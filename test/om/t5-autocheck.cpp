// #include <stdlib>
#include <cmath>
#include <csignal>
#include <string>
#include <iostream>
#include <memory>
// #include "sx.h"
// #include "oxclass.hpp"
#include "object.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "ortmgr.hpp"
#include "oapi.h"
#include "odev.h"

/* ===================================================================
          Script Deployable Objects/Object Model SDO/SDOM
   __step_1__  osrt system initialization/termination, general object
               construction, destruction methods.
   __step_2__  d-derivation
   __step_3__  object trees
   =================================================================== */

#define INTVAL(a) _PINT(a)->getValue()
#define VAL1 24
#define VAL2 3

struct TestResult {
  bool passed;
  bool cInvalidOperation;
  bool hardException;
  bool noResult;
  bool exceptionSettled;
};

TestResult applyCAndCheck(ORef result, ORef a, ORef b, const std::string& op);
TestResult checkResult(ORef result, const std::string& op);

void setValue(ORef o, int value);

void _delete(ORef id, Session *m)
{ if (id != 0) id->getClass()->deleteObject(id, m); }

ORef& ref_(OVariable& var) {return var.objectRef;}

std::ostream& operator<<(std::ostream& os, const OVariable& var) {
  std::vector<OArg> noargs{};
  ref_(const_cast<OVariable&>(var))->applyMethod("print", noargs, nullptr);
  return os;
}

auto displayMessage = [](const std::string& title, const std::string& msg) {
  std::cout << title;
  if (title.back() != ':') std::cout << ':';
  std::cout << ' ' << msg;
  if (msg.back() != '\n') {
    if (msg.back() != '.') std::cout << '.';
    std::cout << '\n';
  }
};

/*===================================================================
  Rules for osObject-derived classes
  ===================================================================
1. All object instances are created on the heap. Create instances on
   the stack only for temporary usage (within a method).
2. When the return value (RV) is immaterial (soft copied) the self
   ptr may be returned. When the RV is essential, ie this is what
   matters when the method is called, a new instance must be created,
   which encapsulates the RV and returned. Temporary RV's are
   automatically put in the trashcan when a DOV-Machine is present.
  ===================================================================*/

using namespace std;

int main(int argc, const char *argv[])
{
  // Convert FPE-signals to C++ exceptions (compile with -fnon-call-exceptions)
  std::shared_ptr<void(int)> handler(
      signal(SIGFPE, [](int signum) {cout << "throw" << endl; throw std::logic_error("FPE");}),
      [](__sighandler_t f) {signal(SIGFPE, f);}
  );

#ifdef _DEBUG
  cout << "_DEBUG defined" << endl;
#endif // _DEBUG

  // Script Extensible Object system initialization
  Session *m = sdoInitialize();
  if (m == nullptr)
  {
    cout << "could not initialize run-time object" << endl;
    return 0;
  }

  m->stringOutput = [](const string& msg) {cout << msg;};
  m->displayMessage = displayMessage;

  vector<string> classes{"unsigned long", "long", "unsigned", "int",
      "short", "char", "bool", "pointer", "double", "float"
  };
  vector<string> methods{"copy:",
    "incBy:", "decBy:", "mulBy:", "divBy:", "modBy:", "expBy:", "powBy:",
    "add:", "sub:", "mul:", "div:", "mod:", "exp:", "pow:",
  };
  vector<string> ops{"=", "+=", "-=", "*=", "/=", "%=",
    "expBy:", "powBy:", "+", "-", "*", "/", "%", "exp:", "pow:",
  };

  cout << "Test 1: Checking operations";
  for (const auto& op : ops) cout << " " << op;
  cout << " on the following types:" << endl;
  for (const auto& cls : classes)
    cout << (cls == classes.front() ? "" : ", ") << cls;
  cout << endl;
  cout << "Options: " << endl;
  cout << "  -c Check for C++ compatibility" << endl;
  cout << "  -v Validate results" << endl;
  cout << "  -d (default) Display results" << endl;

  const bool compatibility = (argc > 1 && string(argv[1]) == "-c");
  const bool validation = (argc > 1 && string(argv[1]) == "-v");
  const bool displayResults = !compatibility && !validation;

  if (argc > 2) cout << "Extra arguments were ignored." << endl;

  OVariable k, j, result;
  vector<OArg> noargs{};
  vector<OArg> arg{&j};

  int numTests = 0;
  int numHardExceptions = 0;
  int numSoftExceptions = 0;
  int numSoftUnthrownExceptions = 0;
  int numPassedTests = 0;
  int numNoTestResults = 0;

  for (const auto& className : classes) {
    OClass *c = m->classMgr->getClass(className);
    ref_(k) = c->createObject(m);
    for (const auto& srcName : classes) {
      OClass *srcClass = m->classMgr->getClass(srcName);
      ref_(j) = srcClass->createObject(m);
      setValue(ref_(j), 3);
      for (const auto& method : methods) {
        setValue(ref_(k), 24);

        if (displayResults) {
          cout << "(" << className << ") ";
          // ref_(k)->applyMethod("print", noargs, m);
          cout << k;
          cout << " " << method << " (" << srcName << ") ";
          ref_(j)->applyMethod("print", noargs, m);
          cout << " --> ";
        }

        bool exceptionResolved = false;
        try {
          result = ref_(k)->applyMethod(method, arg, m);
        } catch (const std::logic_error& e) {
          if (e.what() == (string)"FPE") {
            exceptionResolved = true;
          }
        }

        if (validation)
        {
          TestResult testResult = checkResult(ref_(result), method);
          if (testResult.cInvalidOperation){
            if (m->exceptionPending()) {
              ++numPassedTests;
              ++numSoftExceptions;
            } else {
              cout << "Test reported uncaught invalid operation: (" << className
                   << ") " << method << " (" << srcName << ")" << endl;
            }
          } else if (testResult.hardException) {
            if (m->exceptionPending()) {
              ++numPassedTests;
              ++numHardExceptions;
            } else {
              cout << "Test reported uncaught hard exception: (" << className
                   << ") " << method << " (" << srcName << ")" << endl;
            }
          } else if (testResult.noResult) {
            if (ref_(result) != nullptr)
              cout << "Test error!" << endl;
            else if (m->exceptionPending()) {
              ++numSoftExceptions;
              ++numPassedTests;
            } else {
              ++numSoftUnthrownExceptions;
              ++numPassedTests;
            }
          } else if (testResult.passed) {
            if (m->exceptionPending())
              ++numSoftExceptions;
            ++numPassedTests;
          } else {
            if (m->exceptionPending()) {
              ++numSoftExceptions;
              ++numPassedTests;
              cout << "Exception: (" << className << ") "
                   << method << " (" << srcName << ")" << endl;
            } else if (ref_(result) == nullptr) {
              ++numSoftUnthrownExceptions;
              ++numPassedTests;
              cout << "Unthrown exception: (" << className << ") "
                   << method << " (" << srcName << ")" << endl;
            } else {
              ++numNoTestResults;
              cout << "Unknown result: (" << className << ") "
                   << method << " (" << srcName << ")" << endl;
            }
          }

          if (m->exceptionPending())
            m->clearException();
          if (result.disposable()) {
            _delete(ref_(result), m);
            result.usage = 0;
          }
        }
        else if (compatibility)
        {
          TestResult testResult = applyCAndCheck(ref_(result), ref_(k),
              ref_(j), method);
          if (testResult.cInvalidOperation) {
            if (m->exceptionPending()) {
              ++numPassedTests;
              ++numSoftExceptions;
            } else {
              cout << "Test reported uncaught invalid operation: (" << className
                   << ") " << method << " (" << srcName << ")" << endl;
            }
          } else if (testResult.hardException) {
            if (m->exceptionPending()) {
              ++numPassedTests;
              ++numHardExceptions;
            } else {
              cout << "Test reported uncaught hard exception: (" << className
                   << ") " << method << " (" << srcName << ")" << endl;
            }
          } else if (testResult.passed) {
            if (m->exceptionPending())
              ++numSoftExceptions;
            ++numPassedTests;
          } else {
            cout << "Unknown test result: (" << className << ") "
                 << method << " (" << srcName << ")" << endl;
          }

          if (m->exceptionPending())
            m->clearException();
          if (result.disposable()) {
            _delete(ref_(result), m);
            result.usage = 0;
          }
        }
        else
        {
          if (m->exceptionPending()) {
            cout << " exception.\n";
            m->clearException();
          } else if (ref_(result) == nullptr) {
            cout << " unthrown exception.\n";
          } else {
            ref_(result)->applyMethod("print", noargs, m);
            const bool b = result.disposable();
            cout << " (" << (b ? "" : "non-") << "disposable)\n";
            if (b) {
              _delete(ref_(result), m);
              result.usage = 0;
            }
          }
        }
        ++numTests;
      }
    }
  }

  _delete(ref_(k), m);
  _delete(ref_(j), m);

  m->terminate();

  if (validation || compatibility) {
    cout << "Test statistics:" << endl;
    cout <<
        "Number of tests performed:" << numTests << endl <<
        "Number of passed tests:" << numPassedTests << endl <<
        "Number of hard exceptions:" << numHardExceptions << endl <<
        "Number of soft exceptions:" << numSoftExceptions << endl <<
        "Number of soft unthrown exceptions:" << numSoftUnthrownExceptions << endl <<
        "Number of unknown results:" << numNoTestResults << endl;
  }

  return 0;
}

#if 0
struct TestResult {
  bool passed;
  bool cInvalidOperation;
  bool hardException;
  bool noResult;
  bool exceptionSettled;
};
#endif

#define PASS(b) testResult.passed = (b); break;

TestResult checkResult(ORef result, const string& op) {
  TestResult testResult{false, false, false, false};

  if (result == nullptr) {
    testResult.noResult = true;
    return testResult;
  }

  if (op == "copy:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL2);
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL2);
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL2);
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL2);
      case CLSID_INT: PASS(_PINT(result)->getValue() == VAL2);
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL2);
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL2);
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL2);
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL2);
    }
  } else if (op == "incBy:" || op == "add:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL1+VAL2);
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL1+VAL2);
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL1+VAL2);
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL1+VAL2);
      case CLSID_INT: PASS(_PINT(result)->getValue() == VAL1+VAL2);
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL1+VAL2);
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL1+VAL2);
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL1+VAL2);
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL1+VAL2);
    }
  } else if (op == "decBy:" || op == "sub:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL1-VAL2);
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);;
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL1-VAL2);
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL1-VAL2);
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL1-VAL2);
      case CLSID_INT: PASS(_PINT(result)->getValue() == VAL1-VAL2);
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL1-VAL2);
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL1-VAL2);
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL1-VAL2);
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL1-VAL2);
    }
  } else if (op == "mulBy:" || op == "mul:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL1*VAL2);
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL1*VAL2);
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL1*VAL2);
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL1*VAL2);
      case CLSID_INT: PASS(_PINT(result)->getValue() == VAL1*VAL2);
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL1*VAL2);
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL1*VAL2);
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL1*VAL2);
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL1*VAL2);
    }
  } else if (op == "divBy:" || op == "div:") {
    try
    {
      auto val2 = VAL2;
      switch (result->getClassId()) {
        case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL1/val2);
        case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
        case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL1/val2);
        case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL1/val2);
        case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL1/val2);
        case CLSID_INT: PASS(_PINT(result)->getValue() == VAL1/val2);
        case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL1/val2);
        case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL1/val2);
        case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL1/val2);
        case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL1/val2);
      }
    }
    catch (const logic_error& e)
    {
      testResult.hardException = true;
      testResult.passed = false;
    }
  } else if (op == "modBy:" || op == "mod:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == VAL1%VAL2);
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == VAL1%VAL2);
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == VAL1%VAL2);
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == VAL1%VAL2);
      case CLSID_INT: PASS(_PINT(result)->getValue() == VAL1%VAL2);
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == VAL1%VAL2);
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == VAL1%VAL2);
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == VAL1%VAL2);
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == VAL1%VAL2);
    }
  } else if (op == "expBy:" || op == "exp:") {
    switch (result->getClassId()) {
      case CLSID_BOOL: PASS(_PBOOL(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_POINTER: testResult.cInvalidOperation = true; PASS(false);
      case CLSID_ULONG: PASS(_PULONG(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_LONG: PASS(_PLONG(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_UINT: PASS(_PUINT(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_INT: PASS(_PINT(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_SHORT: PASS(_PSHORT(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_CHAR: PASS(_PCHAR(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_DOUBLE: PASS(_PDBL(result)->getValue() == pow(VAL1, VAL2));
      case CLSID_FLOAT: PASS(_PFLT(result)->getValue() == pow(VAL1, VAL2));
    }
  } else if (op == "powBy:" || op == "pow:") {
    testResult.hardException = true;
  }
  return testResult;
}

// template<const char* _OP_>
// struct Operation {
  // template<typename t, typename s>
  // t operator(const t a, const s b) {
    // return a + b;
  // }
// };

template<typename t>
TestResult compareResults(ORef result, t calculatedResult) {
  TestResult testResult{false, false, false, false};
  switch (result->getClassId()) {
    case CLSID_BOOL:
      testResult.passed = (_PBOOL(result)->getValue() == *(bool *)&calculatedResult);
      break;
    case CLSID_POINTER:
      if (std::is_convertible<t, void *>::value)
        testResult.passed = (_PPTR(result)->getValue() == *(void **)&calculatedResult);
      else
        testResult.cInvalidOperation = true;
      break;
    case CLSID_ULONG:
      testResult.passed = _PULONG(result)->getValue() == *(unsigned long *)&calculatedResult;
      break;
    case CLSID_LONG:
      testResult.passed = _PLONG(result)->getValue() == *(long *)&calculatedResult;
      break;
    case CLSID_UINT:
      testResult.passed = _PUINT(result)->getValue() == *(unsigned *)&calculatedResult;
      break;
    case CLSID_INT:
      testResult.passed = _PINT(result)->getValue() == *(int *)&calculatedResult;
      break;
    case CLSID_SHORT:
      testResult.passed = _PSHORT(result)->getValue() == *(short *)&calculatedResult;
      break;
    case CLSID_CHAR:
      testResult.passed = _PCHAR(result)->getValue() == *(char *)&calculatedResult;
      break;
    case CLSID_DOUBLE:
      testResult.passed = _PDBL(result)->getValue() == *(double *)&calculatedResult;
      break;
    case CLSID_FLOAT:
      testResult.passed = _PFLT(result)->getValue() == *(float *)&calculatedResult;
      break;
  }
  return testResult;
}

void* add(void* const a, void* const b) {return nullptr;}
// template<typename s> t add(void* const a, const s b) {return (void*)(((char*)a) + b);}
template<typename s> void* add(void* const a, const s b) {return nullptr;}
template<typename t> void* add(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t add(const t a, const s b) {return a + b;}
void* sub(void* const a, void* const b) {return nullptr;}
template<typename s> void* sub(void* const a, const s b) {return nullptr;}
template<typename t> void* sub(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t sub(const t a, const s b) {return a - b;}
void* mul(void* const a, void* const b) {return nullptr;}
template<typename s> void* mul(void* const a, const s b) {return nullptr;}
template<typename t> void* mul(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t mul(const t a, const s b) {return a * b;}
void* Div(void* const a, void* const b) {return nullptr;}
template<typename s> void* Div(void* const a, const s b) {return nullptr;}
template<typename t> void* Div(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t Div(const t a, const s b) {return a / b;}
void* Mod(void* const a, void* const b) {return nullptr;}
template<typename s> void* Mod(void* const a, const s b) {return nullptr;}
template<typename t> void* Mod(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t Mod(const t a, const s b) {
  long r = Div(a, b);
  return a - b*r;
}
void* Pow(void* const a, void* const b) {return nullptr;}
template<typename s> void* Pow(void* const a, const s b) {return nullptr;}
template<typename t> void* Pow(const t a, void* const b) {return nullptr;}
template<typename t, typename s> t Pow(const t a, const s b) {return pow(a, b);}

void*& incBy(void*& a, void* const b) {static void *z = nullptr; return z;}
template<typename t> t& incBy(t& a, const t b) {return a += b;}
void*& decBy(void*& a, void* const b) {static void *z = nullptr; return z;}
template<typename t> t& decBy(t& a, const t b) {return a -= b;}
void*& mulBy(void*& a, void* const b) {static void *z = nullptr; return z;}
template<typename t> t& mulBy(t& a, const t b) {return a *= b;}
void*& divBy(void*& a, void* const b) {static void *z = nullptr; return z;}
template<typename t> t& divBy(t& a, const t b) {return a /= b;}
void*& modBy(void*& a, void* const b) {static void *z = nullptr; return z;}
template<typename t> t& modBy(t& a, const t b) {return a = Mod(a, b);}

template<typename t, typename s>
TestResult copCheck(ORef result, t a, s b, const string& op) {
  TestResult testResult{false, false, false, false};
  // decltype(b) bc = std::is_convertible<s, t>::value ? (t)b : (t)0;
  if (op == "copy:") {
    if (std::is_convertible<s, t>::value)
      testResult = compareResults(result, a = *(t *)&b);
    else testResult.cInvalidOperation = true;
  } else if (op == "incBy:") {
    if (std::is_convertible<s, t>::value) {
      auto& r = incBy(a, *(t *)&b);
      testResult = compareResults(result, r);
    }
    else testResult.cInvalidOperation = true;
  } else if (op == "decBy:") {
    if (std::is_convertible<s, t>::value) {
      auto& r = decBy(a, *(t *)&b);
      testResult = compareResults(result, r);
    }
    else testResult.cInvalidOperation = true;
  } else if (op == "mulBy:") {
    if (std::is_convertible<s, t>::value) {
      auto& r = mulBy(a, *(t *)&b);
      testResult = compareResults(result, r);
    }
    else testResult.cInvalidOperation = true;
  } else if (op == "divBy:") {
    try
    {
      if (std::is_convertible<s, t>::value) {
        auto& r = divBy(a, *(t *)&b);
        testResult = compareResults(result, r);
      }
      else testResult.cInvalidOperation = true;
    }
    catch (const logic_error& e)
    {
      testResult.hardException = true;
      if ((string)e.what() == "FPE" && (b == 0))
        testResult.exceptionSettled = true;
    }
  } else if (op == "modBy:") {
    if (std::is_convertible<s, t>::value) {
      cout << "it's here!" << endl;
      try
      {
        auto& r = modBy(a, *(t *)&b);
        testResult = compareResults(result, r);
      }
      catch (const logic_error& e)
      {
        testResult.hardException = true;
        if ((string)e.what() == "FPE" && (b == 0))
          testResult.exceptionSettled = true;
      }
    }
    else testResult.cInvalidOperation = true;
  } else if (op == "expBy:") {
    testResult.cInvalidOperation = true;
  } else if (op == "powBy:") {
    testResult.cInvalidOperation = true;
  } else if (op == "add:") {
    testResult = compareResults(result, add(a, b));
  } else if (op == "sub:") {
    testResult = compareResults(result, sub(a, b));
  } else if (op == "mul:") {
    testResult = compareResults(result, mul(a, b));
  } else if (op == "div:") {
    try
    {
      testResult = compareResults(result, Div(a, b));
    }
    catch (const logic_error& e)
    {
      testResult.hardException = true;
      if ((string)e.what() == "FPE" && (b == 0))
        testResult.exceptionSettled = true;
    }
  } else if (op == "mod:") {
    cout << "now it's here!" << endl;
    try
    {
      testResult = compareResults(result, Mod(a, b));
    }
    catch (const logic_error& e)
    {
      testResult.hardException = true;
      if ((string)e.what() == "FPE" && (b == 0))
        testResult.exceptionSettled = true;
    }
    catch (...)
    {
      testResult.hardException = true;
      cout << "caught!" << endl;
    }
  } else if (op == "exp:") {
    testResult = compareResults(result, Pow(a, b));
  } else if (op == "pow:") {
    testResult.hardException = true;
  }
  return testResult;
}

#define SWITCH(b) \
switch (b->getClassId()) {                                        \
  case CLSID_BOOL:                                                \
    return copCheck(result, r, _PBOOL(result)->getValue(), op);   \
  case CLSID_POINTER:                                             \
    return copCheck(result, r, _PPTR(result)->getValue(), op);    \
  case CLSID_ULONG:                                               \
    return copCheck(result, r, _PULONG(result)->getValue(), op);  \
  case CLSID_LONG:                                                \
    return copCheck(result, r, _PLONG(result)->getValue(), op);   \
  case CLSID_UINT:                                                \
    return copCheck(result, r, _PUINT(result)->getValue(), op);   \
  case CLSID_INT:                                                 \
    return copCheck(result, r, _PINT(result)->getValue(), op);    \
  case CLSID_SHORT:                                               \
    return copCheck(result, r, _PSHORT(result)->getValue(), op);  \
  case CLSID_CHAR:                                                \
    return copCheck(result, r, _PCHAR(result)->getValue(), op);   \
  case CLSID_DOUBLE:                                              \
    return copCheck(result, r, _PDOUBLE(result)->getValue(), op); \
  case CLSID_FLOAT:                                               \
    return copCheck(result, r, _PFLOAT(result)->getValue(), op);  \
}

TestResult applyCAndCheck(ORef result, ORef a, ORef b, const string& op) {

  if (result == nullptr) {
    TestResult testResult{false, false, false, false, false};
    testResult.noResult = true;
    return testResult;
  }

  switch (a->getClassId()) {
    case CLSID_BOOL: {
      bool r = _PBOOL(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_POINTER: {
      void *r = _PPTR(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_ULONG: {
      unsigned long r = _PULONG(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_LONG: {
      long r = _PLONG(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_UINT: {
      unsigned r = _PUINT(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_INT: {
      int r = _PINT(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_SHORT: {
      short r = _PSHORT(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_CHAR: {
      char r = _PCHAR(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_DOUBLE: {
      double r = _PDBL(a)->getValue();
      SWITCH (b)
      break;
    }
    case CLSID_FLOAT: {
      float r = _PFLT(a)->getValue();
      SWITCH (b)
      break;
    }
  }
  return {false, false, false, false};
}

void setValue(ORef o, int value) {
  switch (o->getClassId()) {
    case CLSID_ULONG: *_P(unsigned long, o) = (unsigned long)value; break;
    case CLSID_LONG : *_P(long,     o) = (long    )value; break;
    case CLSID_UINT : *_P(unsigned, o) = (unsigned)value; break;
    case CLSID_INT  : *_P(int,      o) = (int     )value; break;
    case CLSID_SHORT: *_P(short,    o) = (short   )value; break;
    case CLSID_CHAR : *_P(char,     o) = (char    )value; break;
    case CLSID_DBL  : *_P(double,   o) = (double  )value; break;
    case CLSID_FLT  : *_P(float,    o) = (float   )value; break;
    case CLSID_BOOL : *_P(bool,     o) = (bool    )value; break;
    case CLSID_POINTER: *_P(void*,  o) = (void*   )&value; break;
  }
}
