#ifndef __AUTOTEST_HPP__
#define __AUTOTEST_HPP__

#include <string>
#include <cmath>
#include <cstring>

namespace autotest {

//::::::::::::::::::::::::::::::::  getType  ::::::::::::::::::::::::::::::::://

struct NoType {};

#define RET_IF_VALUE_IS_TYPE(val, t) \
  if (std::is_same<decltype(val), t>::value) return #t
#define RETURN_TYPE_OF(val) \
  RET_IF_VALUE_IS_TYPE(val, ulong); \
  RET_IF_VALUE_IS_TYPE(val, long); \
  RET_IF_VALUE_IS_TYPE(val, unsigned); \
  RET_IF_VALUE_IS_TYPE(val, int); \
  RET_IF_VALUE_IS_TYPE(val, unsigned short); \
  RET_IF_VALUE_IS_TYPE(val, short); \
  RET_IF_VALUE_IS_TYPE(val, unsigned char); \
  RET_IF_VALUE_IS_TYPE(val, char); \
  RET_IF_VALUE_IS_TYPE(val, bool); \
  RET_IF_VALUE_IS_TYPE(val, double); \
  RET_IF_VALUE_IS_TYPE(val, float); \
  RET_IF_VALUE_IS_TYPE(val, void*); \
  if (std::is_same<decltype(val), std::string>::value) return "string";

template<typename t>
std::string getType(t value) {
  RETURN_TYPE_OF(value);
  return "";
}
#undef RETURN_TYPE_OF
#undef RET_IF_VALUE_IS_TYPE

//::::::::::::::::::::::::::::::::  Variant  ::::::::::::::::::::::::::::::::://

enum {
  TOLERATE_FP_INACC = 1,
};

struct Variant {
  unsigned flags{0};
  std::string typeName;
  bool conversionError{false};
  char data[64];

  ~Variant() {
    if (typeName == "string") delete *(std::string**)(void*)data;
  }

  template<typename t>
  t getValue() const {
    if (typeName == "string") return **(t**)(void*)data;
    else return *(t*)(void*)data;
  }

  template<typename t>
  inline void setValue(const t value) {
    *(t*)(void*)data = value;
    typeName = getType(value);
  }

  void setValue(const std::string& value) {
    if (typeName != "string") {
      *(std::string**)(void*)data = new std::string;
      typeName = "string";
    }
    **(std::string**)(void*)data = value;
  }

  std::string toString() const {
    if (typeName == "ulong"   ) return std::to_string(getValue<ulong   >());
    if (typeName == "long"    ) return std::to_string(getValue<long    >());
    if (typeName == "unsigned") return std::to_string(getValue<unsigned>());
    if (typeName == "int"     ) return std::to_string(getValue<int     >());
    if (typeName == "short"   ) return std::to_string(getValue<short   >());
    if (typeName == "char"    ) return std::to_string(getValue<char    >());
    if (typeName == "double"  ) return std::to_string(getValue<double  >());
    if (typeName == "float"   ) return std::to_string(getValue<float   >());
    if (typeName == "bool"    ) return std::to_string(getValue<bool    >());
    if (typeName == "void*"   ) return std::to_string(getValue<size_t  >());
    if (typeName == "string"  ) return getValue<std::string>();
    return "";
  }

  template<typename t>
  bool operator==(t var) const {
    if (getType(var) != typeName) return false;
    if ((typeName == "double" || typeName == "float") &&
        (flags & TOLERATE_FP_INACC) != 0)
      return std::fabs(getValue<t>() - var) < 1.e-30;
    return getValue<t>() == var;
  }

  bool operator==(const Variant& var) const {
    if (typeName != var.typeName) return false;
    // if (getType(var) != typeName) return false;
    if ((typeName == "double" || typeName == "float") &&
        (flags & TOLERATE_FP_INACC) != 0)
      return compareFPData(var);
    return compareData(var);
  }

  template<typename t>
  inline bool operator!=(const Variant& var) const {return !(*this == var);}
  template<typename t>
  inline bool operator!=(t var) const {return !(*this == var);}

  bool compareData(const Variant& var) const {
    if (typeName == "ulong"   ) return getValue<ulong   >() == var.getValue<ulong   >();
    if (typeName == "long"    ) return getValue<long    >() == var.getValue<long    >();
    if (typeName == "unsigned") return getValue<unsigned>() == var.getValue<unsigned>();
    if (typeName == "int"     ) return getValue<int     >() == var.getValue<int     >();
    if (typeName == "short"   ) return getValue<short   >() == var.getValue<short   >();
    if (typeName == "char"    ) return getValue<char    >() == var.getValue<char    >();
    if (typeName == "double"  ) return getValue<double  >() == var.getValue<double  >();
    if (typeName == "float"   ) return getValue<float   >() == var.getValue<float   >();
    if (typeName == "bool"    ) return getValue<bool    >() == var.getValue<bool    >();
    if (typeName == "void*"   ) return getValue<void*   >() == var.getValue<void*   >();
    if (typeName == "string"  ) return getValue<std::string>() == var.getValue<std::string>();
    return false;
  }

  bool compareFPData(const Variant& var) const {
    if (typeName == "double")
      return std::fabs(getValue<double>() - var.getValue<double>()) < 1.e-30;
    if (typeName == "float" )
      return std::fabs(getValue<float >() - var.getValue<float >()) < 1.e-30;
    return false;
  }

  template<typename t>
  bool areNAN(const t& value) const {
    return std::isnan(value) && std::isnan(this->getValue<t>());
  }

  template<typename t>
  bool areINF(const t& value) const {
    return std::isinf(value) && std::isinf(this->getValue<t>());
  }

  template<typename t>
  bool areZero(const t& value) const {
    return std::fpclassify(value) == FP_ZERO &&
           std::fpclassify(this->getValue<t>()) == FP_ZERO;
  }

  template<typename t>
  bool areSubnormal(const t& value) const {
    return std::fpclassify(value) == FP_SUBNORMAL &&
           std::fpclassify(this->getValue<t>()) == FP_SUBNORMAL;
  }

  
  bool specialFP(const std::string& resType,
                 const std::pair<float, double>& resVal, std::string& msg) {
    const float& f = resVal.first;
    const double& d = resVal.second;
    std::string rType = resType;
    if (rType == OBasicTypeInfo::getTypeProxyClass<float>()->getClassName())
      rType = OBasicTypeInfo::getTypeClass<float>()->getClassName();
    else if (rType == OBasicTypeInfo::getTypeProxyClass<double>()->getClassName())
      rType = OBasicTypeInfo::getTypeClass<double>()->getClassName();
  
    if (typeName == "float" && rType == "float" && areNAN(f))
      msg = "float NAN (OK)";
    else if (typeName == "double" && rType == "double" && areNAN(d))
      msg = "double NAN (OK)";
    else if (typeName == "float" && rType == "float" && areINF(f))
      msg = "float INF (OK)";
    else if (typeName == "double" && rType == "double" && areINF(d))
      msg = "double INF (OK)";
    else if (typeName == "float" && rType == "float" && areZero(f))
      msg = "float zero (OK)";
    else if (typeName == "double" && rType == "double" && areZero(d))
      msg = "double zero (OK)";
    else if (typeName == "float" && rType == "float" && areSubnormal(f))
      msg = "float subnormal (OK)";
    else if (typeName == "double" && rType == "double" && areSubnormal(d))
      msg = "double subnormal (OK)";
    else
      return false;
    return true;
  }

};

//::::::::::::::::::::::::::::::::::  Cast  :::::::::::::::::::::::::::::::::://

/// \brief Set pointer value
/// \details This function does not provide a correct copy of the
/// source value. However, it provides a consistenct copy for the
/// calculation and checking functions.
template<typename t>
void setPointerValue(void*& p, t value) {
  std::memset(&p, 0, sizeof(void*));
  std::memcpy(&p, &value, std::min(sizeof(t), sizeof(void*)));
}

template<typename t>
void setValueFromP(t& val, void* p) {
  val = 0;
  std::memcpy(&val, &p, std::min(sizeof(t), sizeof(void*)));
}

template<> void setValueFromP(bool& val, void* p) {val = p != nullptr;}

/// \brief Interface between user supplied values and values used in a test
/// \details Accuracy of the cast values does not matter. What matters
/// is consistency between the values used in the calculation tests
/// and the ckecking functions.
template<typename t, typename s>
t cast(s val, t, bool&) {return (t)val;}
template<typename t>
t cast(std::nullptr_t, t, bool&) {return (t)0;}
unsigned cast(void* p, unsigned, bool&) {unsigned v; setValueFromP(v, p); return v;}
int      cast(void* p, int,      bool&) {int      v; setValueFromP(v, p); return v;}
short    cast(void* p, short,    bool&) {short    v; setValueFromP(v, p); return v;}
char     cast(void* p, char,     bool&) {char     v; setValueFromP(v, p); return v;}
bool     cast(void* p, bool,     bool&) {bool     v; setValueFromP(v, p); return v;}
double   cast(void* p, double,   bool&) {double   v; setValueFromP(v, p); return v;}
float    cast(void* p, float,    bool&) {float    v; setValueFromP(v, p); return v;}
void* cast(unsigned val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(int      val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(short    val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(char     val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(bool     val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(double   val, void*, bool&) {void* r; setPointerValue(r, val); return r;}
void* cast(float    val, void*, bool&) {void* r; setPointerValue(r, val); return r;}

//:::::::::::::::::::::::::::  Pointer Operations  ::::::::::::::::::::::::::://

#define DEFINE_PTROP(opname, opsym) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<LV_TYPE>::value, NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<LV_TYPE>::value && \
                          std::is_integral<RV_TYPE>::value, void *>::type \
  apply(LV_TYPE a, RV_TYPE b) \
  { return (void*)(((char*)a) opsym (ulong)b); } \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<LV_TYPE>::value && \
                          !std::is_integral<RV_TYPE>::value, NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
};
#define DEFINE_PTRIOP(opname) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  NoType apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  void *apply(void *a, void *b) \
  { a = b; return a; } \
};
#define DEFINE_PTRCMPOP(opname, opsym) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  NoType apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  bool apply(void *a, void *b) {return a opsym b;} \
};
#define DEFINE_NOPTROP(opname, opsym) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  NoType apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
};

DEFINE_PTRIOP(Cpy)
DEFINE_PTRIOP(Set)
DEFINE_PTRIOP(Ini)
DEFINE_NOPTROP(Add, +)
DEFINE_NOPTROP(Sub, -)
DEFINE_NOPTROP(Mul, *)
DEFINE_NOPTROP(Div, /)
DEFINE_NOPTROP(Mod, %)
DEFINE_NOPTROP(Exp, **)
DEFINE_NOPTROP(Pow, **)
DEFINE_NOPTROP(Incb, +)
DEFINE_NOPTROP(Decb, -)
DEFINE_NOPTROP(Mulb, *=)
DEFINE_NOPTROP(Divb, /=)
DEFINE_NOPTROP(Modb, %=)
DEFINE_NOPTROP(Expb, **=)
DEFINE_NOPTROP(Powb, **=)
DEFINE_PTRCMPOP(Eq, ==)
DEFINE_PTRCMPOP(Ne, !=)
DEFINE_PTRCMPOP(Gt,  >)
DEFINE_PTRCMPOP(Lt,  <)
DEFINE_PTRCMPOP(Ge, >=)
DEFINE_PTRCMPOP(Le, <=)
DEFINE_NOPTROP(Bab, &=)
DEFINE_NOPTROP(Bob, |=)
DEFINE_NOPTROP(Bxb, ^=)
DEFINE_NOPTROP(Shrb, >>=)
DEFINE_NOPTROP(Shlb, <<=)
DEFINE_NOPTROP(Band, &)
DEFINE_NOPTROP(Bor,  |)
DEFINE_NOPTROP(Bxor, ^)
DEFINE_NOPTROP(Shr, >>)
DEFINE_NOPTROP(Shl, <<)
#undef DEFINE_NOPTROP
#undef DEFINE_PTROP
// Special cases
struct PtrAnd {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a && (bool)b;}
};
struct PtrOr {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a || (bool)b;}
};
struct PtrXor {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a != (bool)b;}
};

//:::::::::::::::::::::::::::::::  Operations  ::::::::::::::::::::::::::::::://

#define DEFINE_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value, \
                          decltype((LV_TYPE)0 opsym (RV_TYPE)0)>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#define DEFINE_COPY_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value && \
                          !std::is_pointer<LV_TYPE>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value && \
                          std::is_pointer<LV_TYPE>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {a = (void*)(((char*)a) opsym b); return a;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

/// \brief Previous implementation of \c DEFINE_ASSIGN_OPERATION
#define DEFINE_ASSIGN_OPERATION_PREV(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value && \
                          std::is_integral<LV_TYPE>::value && \
                          sizeof(LV_TYPE) == sizeof(void*), LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym (LV_TYPE)b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value && \
                          (!std::is_integral<LV_TYPE>::value || \
                           sizeof(LV_TYPE) != sizeof(void*)), NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#define DEFINE_ASSIGN_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value && \
                          std::is_same<LV_TYPE, bool>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym (LV_TYPE)b;} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_pointer<RV_TYPE>::value && \
                          (!std::is_same<LV_TYPE, bool>::value), NoType>::type \
  apply(LV_TYPE a, RV_TYPE b) {return NoType();} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#define DEFINE_BIT_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_floating_point<LV_TYPE>::value || \
                          std::is_floating_point<RV_TYPE>::value || \
                          std::is_pointer<LV_TYPE>::value        || \
                          std::is_pointer<RV_TYPE>::value, NoType>::type \
  apply(LV_TYPE, RV_TYPE) {return NoType();} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_integral<LV_TYPE>::value && \
                          std::is_integral<RV_TYPE>::value, \
                          decltype((LV_TYPE)0 opsym (RV_TYPE)0)>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#define DEFINE_BIT_COPY_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_floating_point<LV_TYPE>::value || \
                          std::is_floating_point<RV_TYPE>::value || \
                          std::is_pointer<LV_TYPE>::value        || \
                          std::is_pointer<RV_TYPE>::value, NoType>::type \
  apply(LV_TYPE, RV_TYPE) {return NoType();} \
  template<typename LV_TYPE, typename RV_TYPE> \
  typename std::enable_if<std::is_integral<LV_TYPE>::value && \
                          std::is_integral<RV_TYPE>::value, LV_TYPE>::type \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#define DEFINE_CMP_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  bool apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  NoType apply(ulong,    void*) {return NoType();} \
  NoType apply(long,     void*) {return NoType();} \
  NoType apply(unsigned, void*) {return NoType();} \
  NoType apply(int,      void*) {return NoType();} \
  NoType apply(short,    void*) {return NoType();} \
  NoType apply(char,     void*) {return NoType();} \
  NoType apply(bool,     void*) {return NoType();} \
  NoType apply(double,   void*) {return NoType();} \
  NoType apply(float,    void*) {return NoType();} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};

#if 0
struct Cpy {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_same<LV_TYPE, RV_TYPE>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {return a = b;}

  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_same<LV_TYPE, RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}

  PtrCpy pointerOperation() {return PtrCpy();}
};
#endif // 0

DEFINE_ASSIGN_OPERATION(Cpy, =)
DEFINE_ASSIGN_OPERATION(Set, =)
DEFINE_ASSIGN_OPERATION(Ini, =)
DEFINE_OPERATION(Add,  +)
DEFINE_OPERATION(Sub,  -)
DEFINE_OPERATION(Mul,  *)
struct Div {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value,
                          decltype((LV_TYPE)0 / (RV_TYPE)0)>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (std::is_floating_point<RV_TYPE>::value && !std::isnormal(b))
      throw std::string{"abnormal floating point"};
    if (std::is_same<RV_TYPE, bool>::value && !b)
      throw std::string{"abnormal divisor"};
    if (std::is_integral_v<RV_TYPE> && b == 0)
      throw std::string{"abnormal divisor"};
    return a / b;
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrDiv pointerOperation() {return PtrDiv();}
};
struct Mod {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_integral<LV_TYPE>::value &&
                          std::is_integral<RV_TYPE>::value,
                          decltype((LV_TYPE)0 % (RV_TYPE)0)>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (std::is_same<RV_TYPE, bool>::value && !b)
      throw std::string{"abnormal divisor"};
    if (b == 0)
      throw std::string{"abnormal divisor"};
    return a % b;
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<(std::is_floating_point<LV_TYPE>::value ||
                           std::is_floating_point<RV_TYPE>::value) &&
                          !std::is_pointer<RV_TYPE>::value,
                          decltype(std::fmod((LV_TYPE)0, (RV_TYPE)0))>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (!std::isnormal(b))
      throw std::string{"abnormal floating point"};
    return std::fmod(a, b);
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrMod pointerOperation() {return PtrMod();}
};
struct Exp {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value,
                          decltype(std::pow((LV_TYPE)0, (RV_TYPE)0))>::type
  apply(LV_TYPE a, RV_TYPE b) {return std::pow(a, b);}
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrExp pointerOperation() {return PtrExp();}
};
struct Pow {
  template<typename LV, typename RV> NoType apply(LV a, RV b) {return NoType();}
  PtrPow pointerOperation() {return PtrPow();} \
};
DEFINE_COPY_OPERATION(Incb,  +)
DEFINE_COPY_OPERATION(Decb,  -);
DEFINE_COPY_OPERATION(Mulb,  *);
struct Divb {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value &&
                          !std::is_pointer<LV_TYPE>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (std::is_floating_point<RV_TYPE>::value && !std::isnormal(b))
      throw std::string{"abnormal floating point"};
    if (std::is_same<RV_TYPE, bool>::value && !b)
      throw std::string{"abnormal divisor"};
    if (std::is_integral_v<RV_TYPE> && b == 0)
      throw std::string{"abnormal divisor"};
    return a / b;
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value &&
                          std::is_pointer<LV_TYPE>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {a = (void*)(((char*)a) / b); return a;}
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrDivb pointerOperation() {return PtrDivb();}
};
struct Modb {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_integral<LV_TYPE>::value &&
                          std::is_integral<RV_TYPE>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (std::is_same<RV_TYPE, bool>::value && !b)
      throw std::string{"abnormal divisor"};
    if (b == 0)
      throw std::string{"abnormal divisor"};
    return a %= b;
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<(std::is_floating_point<LV_TYPE>::value ||
                           std::is_floating_point<RV_TYPE>::value) &&
                          !std::is_pointer<RV_TYPE>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {
    if (!std::isnormal(b))
      throw std::string{"abnormal floating point"};
    return a = std::fmod(a, b);
  }
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrModb pointerOperation() {return PtrModb();}
};
struct Expb {
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<!std::is_pointer<RV_TYPE>::value &&
                          !std::is_same<LV_TYPE, bool>::value, LV_TYPE>::type
  apply(LV_TYPE a, RV_TYPE b) {a = std::pow(a, b); return a;}
  template<typename LV_TYPE, typename RV_TYPE>
  typename std::enable_if<std::is_pointer<RV_TYPE>::value ||
                          std::is_same<LV_TYPE, bool>::value, NoType>::type
  apply(LV_TYPE a, RV_TYPE b) {return NoType();}
  PtrExpb pointerOperation() {return PtrExpb();}
};
struct Powb {
  template<typename LV, typename RV> NoType apply(LV a, RV b) {return NoType();}
  PtrPowb pointerOperation() {return PtrPowb();} \
};
DEFINE_CMP_OPERATION(Eq,  ==)
DEFINE_CMP_OPERATION(Ne,  !=)
DEFINE_CMP_OPERATION(Gt,   >)
DEFINE_CMP_OPERATION(Lt,   <)
DEFINE_CMP_OPERATION(Ge,  >=)
DEFINE_CMP_OPERATION(Le,  <=)
DEFINE_BIT_COPY_OPERATION(Bab,  &=)
DEFINE_BIT_COPY_OPERATION(Bob,  |=)
DEFINE_BIT_COPY_OPERATION(Bxb,  ^=)
DEFINE_BIT_COPY_OPERATION(Shrb, >>=)
DEFINE_BIT_COPY_OPERATION(Shlb, <<=)
DEFINE_BIT_OPERATION(Band,  &)
DEFINE_BIT_OPERATION(Bor,   |)
DEFINE_BIT_OPERATION(Bxor,  ^)
DEFINE_BIT_OPERATION(Shr,  >>)
DEFINE_BIT_OPERATION(Shl,  <<)

#undef DEFINE_BIT_OPERATION
#undef DEFINE_BIT_COPY_OPERATION
#undef DEFINE_COPY_OPERATION
#undef DEFINE_CMP_OPERATION
#undef DEFINE_OPERATION
// Special cases
struct And {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a && (bool)b;}
  PtrAnd pointerOperation() {return PtrAnd();}
};
struct Or {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a || (bool)b;}
  PtrOr pointerOperation() {return PtrOr();}
};
struct Xor {
  template<typename LV_TYPE, typename RV_TYPE>
  bool apply(LV_TYPE a, RV_TYPE b) {return (bool)a != (bool)b;}
  PtrXor pointerOperation() {return PtrXor();}
};

//::::::::::::::::::::::::::::  Unary Operations  :::::::::::::::::::::::::::://

#define DEFINE_PREFIX_PTR_UNARY_OPERATION(opname, opsym) \
struct Ptr##opname {void *apply(void*& p) { return (void*)(((char*)p) opsym 1);}};
#define DEFINE_POSTFIX_PTR_UNARY_OPERATION(opname, opsym) \
struct Ptr##opname {void *apply(void*& p) { return p;}};
#define DEFINE_NO_PTR_UNARY_OPERATION(opname, opsym) \
struct Ptr##opname {NoType apply(void*& p) {return NoType{};}};

#define DEFINE_PREFIX_UNARY_OPERATION(opname, opsym) \
struct opname { \
  template<typename VAL_TYPE> \
  VAL_TYPE apply(VAL_TYPE& a) {return opsym a;} \
  NoType apply(bool& a) {return NoType{};} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};
#define DEFINE_POSTFIX_UNARY_OPERATION(opname, opsym) \
struct opname { \
  template<typename VAL_TYPE> \
  VAL_TYPE apply(VAL_TYPE& a) {return a opsym;} \
  NoType apply(bool& a) {return NoType{};} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};
#define DEFINE_CAST_OPERATION(opname, opsym) \
struct opname { \
  template<typename VAL_TYPE> \
  opsym apply(VAL_TYPE a) {return (opsym) a;} \
  Ptr##opname pointerOperation() {return Ptr##opname();} \
};
// NoType apply(bool& a) {return NoType{};}
// Ptr##opname pointerOperation() {return Ptr##opname();}

DEFINE_PREFIX_PTR_UNARY_OPERATION(Inc,  +)
DEFINE_PREFIX_PTR_UNARY_OPERATION(Dec,  -)
DEFINE_POSTFIX_PTR_UNARY_OPERATION(Pinc, +)
DEFINE_POSTFIX_PTR_UNARY_OPERATION(Pdec, -)
DEFINE_NO_PTR_UNARY_OPERATION(Neg,  -)
DEFINE_NO_PTR_UNARY_OPERATION(Binv, ~)
struct PtrNot {bool apply(void*& p) { return !p;}};
struct PtrAddr {void *apply(void*& p) { return (void*)&p;}};

DEFINE_PREFIX_UNARY_OPERATION(Inc,  ++)
DEFINE_PREFIX_UNARY_OPERATION(Dec,  --)
DEFINE_POSTFIX_UNARY_OPERATION(Pinc, ++)
DEFINE_POSTFIX_UNARY_OPERATION(Pdec, --)
DEFINE_PREFIX_UNARY_OPERATION(Neg,  -)
// DEFINE_PREFIX_UNARY_OPERATION(Not,  !)
// DEFINE_PREFIX_UNARY_OPERATION(Binv, ~)
struct Not {
  template<typename VAL_TYPE>
  bool apply(VAL_TYPE& a) {return !a;}
  PtrNot pointerOperation() {return PtrNot();}
};
struct Binv {
  template<typename VAL_TYPE>
  VAL_TYPE apply(VAL_TYPE& a) {return ~a;}
  NoType apply(bool& a) {return NoType{};}
  NoType apply(double& a) {return NoType{};}
  NoType apply(float& a) {return NoType{};}
  PtrBinv pointerOperation() {return PtrBinv();}
};
struct Addr {
  template<typename VAL_TYPE>
  void *apply(VAL_TYPE& a) { return (void *)&a;}
  PtrAddr pointerOperation() {return PtrAddr();}
};
struct PtrUlng {NoType apply(void* a) {return NoType();}};
struct PtrLng  {NoType apply(void* a) {return NoType();}};
struct PtrUns  {NoType apply(void* a) {return NoType();}};
struct PtrInt  {NoType apply(void* a) {return NoType();}};
struct PtrSht  {NoType apply(void* a) {return NoType();}};
struct PtrChr  {NoType apply(void* a) {return NoType();}};
struct PtrBool {bool   apply(void* p) {return p != nullptr;}};
struct PtrDbl  {NoType apply(void* a) {return NoType();}};
struct PtrFlt  {NoType apply(void* a) {return NoType();}};
struct PtrTostr {
  std::string apply(void *p) {
    char buf[64]; sprintf(buf, "%p", p);
    std::string r(buf);
    const auto pos = r.find("0x");
    if (pos != std::string::npos) r = r.substr(pos+2);
    else if (r == "(nil)") r = "";
    if (r.length() != 16) r = std::string(16 - r.length(), '0') + r;
    return r;
  }
};
DEFINE_CAST_OPERATION(Ulng, ulong)
DEFINE_CAST_OPERATION(Lng, long)
DEFINE_CAST_OPERATION(Uns, unsigned)
DEFINE_CAST_OPERATION(Int, int)
DEFINE_CAST_OPERATION(Sht, short)
DEFINE_CAST_OPERATION(Chr, char)
DEFINE_CAST_OPERATION(Bool, bool)
DEFINE_CAST_OPERATION(Dbl, double)
DEFINE_CAST_OPERATION(Flt, float)
struct Tostr {
  template<typename VAL_TYPE>
  std::string apply(VAL_TYPE a) {return std::to_string(a);}
  PtrTostr pointerOperation() {return PtrTostr();}
};

#undef DEFINE_CAST_OPERATION
#undef DEFINE_PREFIX_PTR_UNARY_OPERATION
#undef DEFINE_POSTFIX_PTR_UNARY_OPERATION
#undef DEFINE_NO_PTR_UNARY_OPERATION
#undef DEFINE_PREFIX_UNARY_OPERATION
#undef DEFINE_POSTFIX_UNARY_OPERATION

//::::::::::::::::::::::::::::::  Conversions  ::::::::::::::::::::::::::::::://

#define CAST(val, t) cast(val, (t)0, conversionError)

#define DO_OP_IF_RV_IS_TYPE(t, s) \
if (rvType == #s) { \
  try { \
    auto _result = OP().apply(CAST(lvAny, t), CAST(rvAny, s)); \
    *(decltype(_result) *)pResAny = _result; \
    result.typeName = getType(_result); \
  } catch (const std::string& msg) { \
    *(NoType*)pResAny = NoType{}; \
    result.typeName = ""; \
  } \
}

#define DO_PTROP_IF_RV_IS_TYPE(s) \
if (rvType == #s) { \
  auto _result = OP().pointerOperation().apply(CAST(lvAny, void*), CAST(rvAny, s)); \
  *(decltype(_result) *)pResAny = _result; \
  result.typeName = getType(_result); \
}

#define DO_OP_IF_LV_IS_TYPE(t) \
if (lvType == #t) { \
  DO_OP_IF_RV_IS_TYPE(t, ulong) \
  else DO_OP_IF_RV_IS_TYPE(t, long) \
  else DO_OP_IF_RV_IS_TYPE(t, unsigned) \
  else DO_OP_IF_RV_IS_TYPE(t, int) \
  else DO_OP_IF_RV_IS_TYPE(t, short) \
  else DO_OP_IF_RV_IS_TYPE(t, char) \
  else DO_OP_IF_RV_IS_TYPE(t, bool) \
  else DO_OP_IF_RV_IS_TYPE(t, double) \
  else DO_OP_IF_RV_IS_TYPE(t, float) \
  else DO_OP_IF_RV_IS_TYPE(t, void*) \
}

#define DO_OP_IF_LV_IS_PTR() \
if (lvType == "void*") { \
  DO_PTROP_IF_RV_IS_TYPE(ulong) \
  else DO_PTROP_IF_RV_IS_TYPE(long) \
  else DO_PTROP_IF_RV_IS_TYPE(unsigned) \
  else DO_PTROP_IF_RV_IS_TYPE(int) \
  else DO_PTROP_IF_RV_IS_TYPE(short) \
  else DO_PTROP_IF_RV_IS_TYPE(char) \
  else DO_PTROP_IF_RV_IS_TYPE(bool) \
  else DO_PTROP_IF_RV_IS_TYPE(double) \
  else DO_PTROP_IF_RV_IS_TYPE(float) \
  else DO_PTROP_IF_RV_IS_TYPE(void*) \
}

#define DO_UOP_IF_VAL_IS_TYPE(t) \
if (valType == #t) { \
  t val = CAST(valAny, t); \
  if (!conversionError) {\
    auto _result = OP().apply(val); \
    result.setValue(_result); \
  } \
}

#define DO_UOP_IF_VAL_IS_PTR() \
if (valType == "void*") { \
  void *val = CAST(valAny, void*); \
  if (!conversionError) {\
    auto _result = OP().pointerOperation().apply(val); \
    result.setValue(_result); \
  } \
}

//:::::::::::::::::::::::::::::  applyOperation  ::::::::::::::::::::::::::::://

template<typename OP, typename LV_TYPE, typename RV_TYPE>
const std::string& applyOperation(
    const std::string& lvOMType, LV_TYPE lvAny,
    const std::string& rvOMType, RV_TYPE rvAny,
    Variant& result) {
  static const std::string voidPtr{"void*"};
  const std::string &lvType = lvOMType == "pointer" ? voidPtr : lvOMType;
  const std::string &rvType = rvOMType == "pointer" ? voidPtr : rvOMType;
  bool& conversionError = result.conversionError;
  void *pResAny = (void *)result.data;
  result.typeName = "";
  DO_OP_IF_LV_IS_TYPE(ulong)
  /*if (lvType == "ulong") {
    // DO_OP_IF_RV_IS_TYPE(t, ulong)
    if (rvType == "ulong") {
      auto _result = OP().apply(CAST(lvAny, ulong), CAST(rvAny, ulong));
      *(decltype(_result) *)pResAny = _result;
      result.typeName = getType(_result);
    }
    else DO_OP_IF_RV_IS_TYPE(ulong, long)
    else DO_OP_IF_RV_IS_TYPE(ulong, unsigned)
    else DO_OP_IF_RV_IS_TYPE(ulong, int)
    else DO_OP_IF_RV_IS_TYPE(ulong, short)
    else DO_OP_IF_RV_IS_TYPE(ulong, char)
    else DO_OP_IF_RV_IS_TYPE(ulong, bool)
    else DO_OP_IF_RV_IS_TYPE(ulong, double)
    else DO_OP_IF_RV_IS_TYPE(ulong, float)
    else DO_OP_IF_RV_IS_TYPE(ulong, void*)
  }*/
  else DO_OP_IF_LV_IS_TYPE(long)
  else DO_OP_IF_LV_IS_TYPE(unsigned)
  else DO_OP_IF_LV_IS_TYPE(int)
  else DO_OP_IF_LV_IS_TYPE(short)
  else DO_OP_IF_LV_IS_TYPE(char)
  else DO_OP_IF_LV_IS_TYPE(bool)
  else DO_OP_IF_LV_IS_TYPE(double)
  else DO_OP_IF_LV_IS_TYPE(float)
  // else DO_OP_IF_LV_IS_PTR()
  if (lvType == "void*") {
    DO_PTROP_IF_RV_IS_TYPE(ulong)
    else DO_PTROP_IF_RV_IS_TYPE(long)
    else DO_PTROP_IF_RV_IS_TYPE(unsigned)
    else DO_PTROP_IF_RV_IS_TYPE(int)
    else DO_PTROP_IF_RV_IS_TYPE(short)
    else DO_PTROP_IF_RV_IS_TYPE(char)
    else DO_PTROP_IF_RV_IS_TYPE(bool)
    else DO_PTROP_IF_RV_IS_TYPE(double)
    else DO_PTROP_IF_RV_IS_TYPE(float)
    else DO_PTROP_IF_RV_IS_TYPE(void*)
  }
  return result.typeName;
}

template<typename OP, typename VAL_TYPE>
const std::string& applyOperation(
    const std::string& valOMType, VAL_TYPE valAny, Variant& result) {
  static const std::string voidPtr{"void*"};
  const std::string &valType = valOMType == "pointer" ? voidPtr : valOMType;
  bool& conversionError = result.conversionError;
  // void *pResAny = (void *)result.data;
  result.typeName = "";
  DO_UOP_IF_VAL_IS_TYPE(ulong)
  else DO_UOP_IF_VAL_IS_TYPE(long)
  else DO_UOP_IF_VAL_IS_TYPE(unsigned)
  else DO_UOP_IF_VAL_IS_TYPE(int)
  else DO_UOP_IF_VAL_IS_TYPE(short)
  else DO_UOP_IF_VAL_IS_TYPE(char)
  else DO_UOP_IF_VAL_IS_TYPE(bool)
  else DO_UOP_IF_VAL_IS_TYPE(double)
  else DO_UOP_IF_VAL_IS_TYPE(float)
  else DO_UOP_IF_VAL_IS_PTR()
  return result.typeName;
}

#undef DO_UOP_IF_VAL_IS_TYPE
#undef DO_UOP_IF_VAL_IS_PTR
#undef CAST
#undef DO_OP_IF_LV_IS_PTR
#undef DO_OP_IF_LV_IS_TYPE
#undef DO_PTROP_IF_RV_IS_TYPE
#undef DO_OP_IF_RV_IS_TYPE

//:::::::::::::::::::::::::::::  Operation Data  ::::::::::::::::::::::::::::://

struct BinaryOperation {
  std::string rec, method, arg;
  bool operator==(const BinaryOperation& r) const {
    return rec == r.rec && method == r.method && arg == r.arg;
  }
};

struct UnaryOperation {
  std::string rec, method;
  bool operator==(const BinaryOperation& r) const {
    return rec == r.rec && method == r.method;
  }
};

} // namespace autotest

#endif // __AUTOTEST_HPP__
