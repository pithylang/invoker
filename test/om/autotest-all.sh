#!/bin/env bash

lhsValues=(
true
true
true
true
true
true
true
true
false
false
true
false
8
8ul
8l
8u
"short(8)"
"char(8)"
8.0f
8.0d
7
7
7
7
7
7
7ul
7u
7l
"short(7)"
"char(7)"
7ul
7ul
7l
7l
7
7
7ul
7ul
7u
7u
7l
7l
"short(7)"
"short(7)"
"char(7)"
"char(7)"
24.6f
24.6f
24.6d
24.6d
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
3.2f
3.2d
true
false
7
7ul
7u
7l
"short(7)"
"char(7)"
3.2d
3.2f
true
false
-1.2f
-1.2d
7
7ul
7u
7l
"short(7)"
"char(7)"
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
)

rhsValues=(
8
8ul
8l
8u
"short(8)"
"char(8)"
8.0f
8.0d
8
8.0
false
true
true
true
true
true
true
true
true
true
8
8ul
8u
8l
"short(8)"
"char(8)"
8
8
8
8
8
8l
"char(8)"
8ul
8l
-1.2f
-1.2d
-1.2f
-1.2d
-1.2f
-1.2d
-1.2f
-1.2d
-1.2f
-1.2d
-1.2f
-1.2d
3.2f
3.2d
3.2f
3.2d
3.2f
3.2d
true
false
7
7ul
7u
7l
"short(7)"
"char(7)"
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
nullptr
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
ptrValue
3.2f
3.2d
true
false
-1.2f
-1.2d
7
7ul
7u
7l
"short(7)"
"char(7)"
)

uopValues=(
true
false
92345
8
8ul
8l
8u
"short(8)"
"char(8)"
8.0f
8.0d
7
7ul
7u
7l
"short(7)"
"char(7)"
24.6f
24.6d
nullptr
3.2f
3.2d
-1.2f
-1.2d
ptrValue
)

getResponse() {
  while true;
  __response_=""
  do
    read -n 1 -p "$1" __response_; echo;
    case $__response_ in
      "y"|"Y") return 0;
        ;;
      "n"|"N") return 1;
        ;;
      *) echo "Incorrect answer."
        ;;
    esac
  done
}

NUM_TESTS=${#lhsValues[@]}

for i in {5,6,7,8,11,12,13,14}; do
#  break;
  src=t$i
#  getResponse "proceed with test $src? (y/n) "
#  if  (( $? != 0 ))  ; then
#    exit 0
#  fi
  for ((k=0; k<${NUM_TESTS}; k++)); do
    echo "compiling $src with values ${lhsValues[k]} ${rhsValues[k]}"
    sed -r \
      -e 's%^#define[ \t]+VAL1[ \t]+(0x[0-9a-fA-F]+|[+-]?[0-9]?\.[0-9]+|[+-]?[0-9]+\.[0-9]?|([1-9][0-9]*)|true|false)[ \t]+\/\/%#define VAL1 '${lhsValues[k]}' //%' \
      -e 's%^#define[ \t]+VAL2[ \t]+(0x[0-9a-fA-F]+|[+-]?[0-9]?\.[0-9]+|[+-]?[0-9]+\.[0-9]?|([1-9][0-9]*)|true|false)[ \t]+\/\/%#define VAL2 '${rhsValues[k]}' //%' \
      "${src}.cpp" > "tmp.cpp"
    source compile.sh tmp
    if  (( $? == 0 ))  ; then
      echo "running ${src} with values ${lhsValues[k]} ${rhsValues[k]}"
    else
      echo "compilation failed"
      exit 22
    fi
    ./tmp -a
    if  (( $? != 0 ))  ; then
      exit 23
    fi
  done
done

NUM_UOP_TESTS=${#uopValues[@]}

for i in {9,10,15,16}; do
#  break;
  src=t$i
#  getResponse "proceed with test $src? (y/n) "
#  if  (( $? != 0 ))  ; then
#    exit 0
#  fi
  for ((k=0; k<${NUM_UOP_TESTS}; k++)); do
    echo "compiling $src with value ${uopValues[k]}"
    sed -r \
      -e 's%^#define[ \t]+VAL1[ \t]+(0x[0-9a-fA-F]+|[+-]?[0-9]?\.[0-9]+|[+-]?[0-9]+\.[0-9]?|([1-9][0-9]*)|true|false)[ \t]+\/\/%#define VAL1 '${uopValues[k]}' //%' \
      "${src}.cpp" > "tmp.cpp"
    source compile.sh tmp
    if  (( $? == 0 ))  ; then
      echo "running ${src} with value ${uopValues[k]}"
    else
      echo "compilation failed"
      exit 24
    fi
    ./tmp -n
    if  (( $? != 0 ))  ; then
      exit 25
    fi
  done
done
