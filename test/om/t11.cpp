#include <iostream>
#include <map>
#include "object.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "odev.h"
#include "autotest.hpp"

//============================================================================//
//:::::::::::   Script Deployable Objects/Object Model SDO/SDOM   :::::::::::://
//============================================================================//

#define VAL1 24.6 // Redefine for a different set of tests
#define VAL2 3.2  // Redefine for a different set of tests
// Tested pairs:
// (true, 8), (7, -1.2), (24.6, 3.2)

static Session *thisSession = nullptr;
static bool noSpecialFPInfo = false;

int i_ = 0;
void* ptrValue = &i_;

autotest::Variant getValue(ORef o);
template<typename t> void setValue(ORef o, t value);
template<typename t> void setProxyValue(ORef o, t value);
std::string makeProxyClassName(const std::string& typeName);
void handleResult(OVariable& result, Session *m);
int handleResult(OVariable& result, const autotest::BinaryOperation& bo,
    bool recIsProxy, bool argIsProxy, Session *m);
void handleResultNoDisplay(OVariable& result, Session *m);
void _delete(ORef id, Session *m) {if (id) id->getClass()->deleteObject(id, m);}
ORef& ref_(OVariable& var) {return var.objectRef;}
static std::pair<ORet, int>
execMethod(Session* m, const std::string& method, ulong, ORef, int, OArg*);

std::ostream& operator<<(std::ostream& os, const OVariable& var) {
  std::vector<OArg> noargs{};
  ref_(const_cast<OVariable&>(var))->applyMethod("print", noargs, thisSession);
  return os;
}

using namespace std;

// functions required by the compiler
std::pair<OVar*, const string&>
g_GetVariable(VirtualMachine*, MSymbolReference*, int) {return {nullptr, ""};}
ORet g_ExecReturn(VirtualMachine*, OArg, bool) {return OVar::nullResult();}
ORet g_ExecTryCatch(VirtualMachine*, OArg, OArg, OArg) {return OVar::nullResult();}
MReqValue g_GetMachineValue(VirtualMachine* vm, MParam) {return MReqValue{};}
vector<string> split(const string&, const string&) {return vector<string>{};}
// end required functions

int main(int argc, const char *argv[]) {
  bool autoTest = false;
  map<autotest::BinaryOperation, autotest::Variant> expectedResults;

  vector<string> classes{"ulong", "long", "unsigned", "int",
      "short", "char", "bool", "pointer", "double", "float"
  };
  vector<string> proxyClasses{"ulong", "long", "unsigned", "int",
      "short", "char", "bool", "pointer", "double", "float"
  };
  vector<string> methods{"copy:", "value:", "init:",
    "incBy:", "decBy:", "mulBy:", "divBy:", "modBy:", "expBy:", "powBy:",
    "add:", "sub:", "mul:", "div:", "mod:", "exp:", "pow:",
  };
  vector<string> ops{"=", "value:", "init:",
    "+=", "-=", "*=", "/=", "%=", "expBy:", "powBy:",
    "+", "-", "*", "/", "%", "exp:", "pow:",
  };

  cout << "Test 7: Checking operations";
  for (const auto& op : ops) cout << " " << op;
  cout << " between proxy and the following types:" << endl;
  for (const auto& cls : classes)
    cout << (cls == classes.front() ? "" : ", ") << cls;
  cout << endl;

  // Check command-line arguments
  if (argc >= 2) {
    if (string(argv[1]) == "-a" || string(argv[1]) == "--autotest")
      autoTest = true;
    else if (string(argv[1]) == "-r")
      autoTest = false;
    else if (string(argv[1]) == "-n")
      noSpecialFPInfo = autoTest = true;
    else if (string(argv[1]) == "-h" || string(argv[1]) == "--help") {
      cout << "Command-line options:" << endl
           << "  -a, --autotest   Perform autotest" << endl
           << "  -r               (default) Display results" << endl
           << "  -n               Autotest without special FP info" << endl
           << "  -h, --help       Display usage info" << endl;
      return 0;
    }
    else
      cout << "Ignoring extraneous command-line option "
           << argv[1] << "." << endl;
  }
  if (argc > 2)
    cout << "Ignoring extra command-line options." << endl;

  // Script Deployable Object system initialization
  Session *m = sdoInitialize();
  if (m == nullptr) {
    cout << "could not initialize run-time object" << endl;
    return 0;
  }

  m->stringOutput = [](const string& msg) {cout << msg;};
  thisSession = m;
  m->setExecMethodProc([m](const string& method, ulong flags,
      ORef rec, int argc, OArg* argv) {
        return execMethod(m, method, flags, rec, argc, argv);
      });

  OVariable k, j, result;
  vector<OArg> noargs{};
  vector<OArg> arg{&j};
  int numTests = 0;

  // type - proxy
  for (const auto& recType : classes) {
    OClass *c = m->classMgr->getClass(recType);
    ref_(k) = c->createObject(m);
    for (const auto& argType : classes) {
      const string proxyType = makeProxyClassName(argType);
      OClass *proxyClass = m->classMgr->getClass(proxyType);
      ref_(j) = proxyClass->createObject(m);
      setProxyValue(ref_(j), VAL2);
      for (const auto& method : methods) {
        setValue(ref_(k), VAL1);
        if (!autoTest)
          cout << "(" << recType     << ") " << k << " " << method << " "
               << "(" << proxyType   << ") " << j << " --> ";

        result = ref_(k)->applyMethod(method, arg, m);

        if (autoTest) {
          int rc = handleResult(result, {recType, method, argType},
              false, true, m);
          if (rc != 0) return rc;
        } else
          handleResult(result, m);
        ++numTests;
      }
    }
  }

  // proxy - type
  for (const auto& recType : classes) {
    const string proxyName = makeProxyClassName(recType);
    OClass *c = m->classMgr->getClass(proxyName);
    ref_(k) = c->createObject(m);
    for (const auto& argType : classes) {
      OClass *argClass = m->classMgr->getClass(argType);
      ref_(j) = argClass->createObject(m);
      setValue(ref_(j), VAL2);
      for (const auto& method : methods) {
        setProxyValue(ref_(k), VAL1);
        if (!autoTest)
          cout << "(" << proxyName << ") " << k << " " << method << " "
               << "(" << argType   << ") " << j << " --> ";

        result = ref_(k)->applyMethod(method, arg, m);

        if (autoTest) {
          int rc = handleResult(result, {recType, method, argType},
              true, false, m);
          if (rc != 0) return rc;
        } else
          handleResult(result, m);
        ++numTests;
      }
    }
  }

  // proxy - proxy
  for (const auto& recType : classes) {
    const string recProxy = makeProxyClassName(recType);
    OClass *c = m->classMgr->getClass(recProxy);
    ref_(k) = c->createObject(m);
    for (const auto& argType : classes) {
      const string argProxy = makeProxyClassName(argType);
      OClass *argClass = m->classMgr->getClass(argProxy);
      ref_(j) = argClass->createObject(m);
      setProxyValue(ref_(j), VAL2);
      for (const auto& method : methods) {
        setProxyValue(ref_(k), VAL1);
        if (!autoTest)
          cout << "(" << recProxy << ") " << k << " " << method << " "
               << "(" << argProxy << ") " << j << " --> ";

        result = ref_(k)->applyMethod(method, arg, m);

        if (autoTest) {
          int rc = handleResult(result, {recType, method, argType},
              true, true, m);
          if (rc != 0) return rc;
        } else
          handleResult(result, m);
        ++numTests;
      }
    }
  }

  _delete(ref_(k), m);
  _delete(ref_(j), m);

  m->terminate();
  if (autoTest)
    cout << numTests << " tests were completed successfully." << endl;

  return 0;
}

#define APPLY_METHOD_IF(name, sym) \
if (method == name) \
  autotest::applyOperation<autotest::sym>(recType, VAL1, argType, VAL2, cResult)

int handleResult(OVariable& result, const autotest::BinaryOperation& bo,
    bool recIsProxy, bool argIsProxy, Session *m) {
  const string& method  = bo.method;
  const string& recType = bo.rec;
  const string& argType = bo.arg;
  const string  recClass = recIsProxy ? makeProxyClassName(bo.rec) : bo.rec;
  const string  argClass = argIsProxy ? makeProxyClassName(bo.arg) : bo.arg;
  autotest::Variant cResult;

  APPLY_METHOD_IF("copy:",       Cpy);
  else APPLY_METHOD_IF("value:", Set);
  else APPLY_METHOD_IF("init:",  Ini);
  else APPLY_METHOD_IF("incBy:", Incb);
  else APPLY_METHOD_IF("decBy:", Decb);
  else APPLY_METHOD_IF("mulBy:", Mulb);
  else APPLY_METHOD_IF("divBy:", Divb);
  else APPLY_METHOD_IF("modBy:", Modb);
  else APPLY_METHOD_IF("expBy:", Expb);
  else APPLY_METHOD_IF("powBy:", Powb);
  else APPLY_METHOD_IF("add:",   Add);
  else APPLY_METHOD_IF("sub:",   Sub);
  else APPLY_METHOD_IF("mul:",   Mul);
  else APPLY_METHOD_IF("div:",   Div);
  else APPLY_METHOD_IF("mod:",   Mod);
  else APPLY_METHOD_IF("exp:",   Exp);
  else APPLY_METHOD_IF("pow:",   Pow);

  pair<float, double> fp{0., 0.};
  if (ref_(result) && !m->exceptionPending())
    switch (ref_(result)->getClassId()) {
      case CLSID_FLT: fp.first  = _PFLT(ref_(result))->getValue(); break;
      case CLSID_DBL: fp.second = _PDBL(ref_(result))->getValue(); break;
      case CLSID_FLTVECELEM: fp.first  = _PTypeProxy(float, ref_(result))->getValue(); break;
      case CLSID_DBLVECELEM: fp.second = _PTypeProxy(double, ref_(result))->getValue(); break;
    }
  string msg;

  if (cResult.conversionError) {
    cout << "Conversion error: " << recClass << " "
         << method << " " << argClass << endl;
    if (!m->exceptionPending() && ref_(result) != nullptr) return 3;
  } else if (cResult.typeName.empty()) {
    if (!m->exceptionPending() && ref_(result) != nullptr) {
      cout << "Failed on: " << recClass << " "
           << method << " " << argClass << " (expected exception)" << endl;
      return 4;
    }
  } else if (m->exceptionPending() || ref_(result) == nullptr) {
    cout << "Failed on: " << recClass << " "
         << method << " " << argClass << " (unexpected "
         << (m->exceptionPending() ? "exception" : "null result")
         << ")" << endl;
    return 6;
  } else if (cResult.specialFP(ref_(result)->getClassName(), fp, msg)) {
    if (!noSpecialFPInfo)
      cout << msg << endl;
  } else if (cResult != getValue(ref_(result))) {
    cout << "Failed on: " << recClass << " " << method << " " << argClass << " ";
    if (ref_(result)->getClassName() == cResult.typeName)
      cout << "(expected " << cResult.toString()
           << " found " << result << ")" << endl;
    else
      cout << "(expected type " << cResult.typeName
           << ", found type " << ref_(result)->getClassName() << ")" << endl;
    return 2;
  }
  handleResultNoDisplay(result, m);
  return 0;
}

void handleResult(OVariable& result, Session *m) {
  if (m->exceptionPending()) {
    cout << " exception.\n";
    m->clearException();
  } else if (ref_(result) == nullptr) {
    cout << " unthrown exception.\n";
  } else {
    const bool b = result.disposable();
    cout << result << " (" << ref_(result)->getClassName() << ") "
         << "(" << (b ? "" : "non-") << "disposable)" << endl;
    if (b) {
      _delete(ref_(result), m);
      result.usage = 0;
    }
  }
}

void handleResultNoDisplay(OVariable& result, Session *m) {
  if (m->exceptionPending()) {
    m->clearException();
  } else if (result.disposable()) {
    _delete(ref_(result), m);
    result.usage = 0;
  }
}

string makeProxyClassName(const string& typeName) {
  string proxyName = typeName;
  const auto pos = proxyName.find(' ');
  if (pos != string::npos) proxyName.replace(pos, 1, 1, '_');
  return "SDOM_"+proxyName+"_type_proxy";
}

#define CAST(val, t) autotest::cast(val, (t)0, conversionError)

template<typename t>
void setValue(ORef o, t value) {
  bool conversionError = false;
  switch (o->getClassId()) {
    case CLSID_ULONG: *_P(ulong,    o) = CAST(value, ulong   ); break;
    case CLSID_LONG : *_P(long,     o) = CAST(value, long    ); break;
    case CLSID_UINT : *_P(unsigned, o) = CAST(value, unsigned); break;
    case CLSID_INT  : *_P(int,      o) = CAST(value, int     ); break;
    case CLSID_SHORT: *_P(short,    o) = CAST(value, short   ); break;
    case CLSID_CHAR : *_P(char,     o) = CAST(value, char    ); break;
    case CLSID_DBL  : *_P(double,   o) = CAST(value, double  ); break;
    case CLSID_FLT  : *_P(float,    o) = CAST(value, float   ); break;
    case CLSID_BOOL : *_P(bool,     o) = CAST(value, bool    ); break;
    case CLSID_PTR  : *_P(void*,    o) = CAST(value, void*   ); break;
  }
}

static void *proxyData = nullptr;
static std::vector<bool> bv{true, false, false, true};
static std::vector<bool>::reference proxyDataBool = bv[0];

template<typename proxy_type, typename t>
requires std::is_same<t, nullptr_t>::value
void setProxyTypeValue(ORef o, nullptr_t value) {
  _PTypeProxy(proxy_type, o)->setElemPtr((proxy_type*)(void*)&proxyData);
  *(CType<proxy_type> *)_PTypeProxy(proxy_type, o) = 0;
}
template<typename proxy_type, typename t>
requires (!std::is_same<t, nullptr_t>::value)
void setProxyTypeValue(ORef o, t value) {
  bool conversionError = false;
  _PTypeProxy(proxy_type, o)->setElemPtr((proxy_type*)(void*)&proxyData);
  *(CType<proxy_type> *)_PTypeProxy(proxy_type, o) = CAST(value, proxy_type);
}
template<typename t>
void setProxyPtrValue(ORef o, t value) {
  bool conversionError = false;
  _PTypeProxy(void*, o)->setElemPtr(&proxyData);
  _PTypeProxy(void*, o)->setValue(CAST(value, void*));
  _PTypeProxy(void*, o)->updateValue();
}
template<typename t>
void setProxyBoolValue(ORef o, t value) {
  _PTypeProxy(bool, o)->setElemRef(proxyDataBool);
  ((OTypeProxy<bool> *)o)->setValue((bool) value);
}

template<typename t>
void setProxyValue(ORef o, t value) {
  int id = o->getClassId();
  switch (id) {
    case CLSID_ULONGVECELEM: setProxyTypeValue<ulong,    t>(o, value); break;
    case CLSID_LONGVECELEM : setProxyTypeValue<long,     t>(o, value); break;
    case CLSID_UINTVECELEM : setProxyTypeValue<unsigned, t>(o, value); break;
    case CLSID_INTVECELEM  : setProxyTypeValue<int,      t>(o, value); break;
    case CLSID_SHORTVECELEM: setProxyTypeValue<short,    t>(o, value); break;
    case CLSID_CHARVECELEM : setProxyTypeValue<char,     t>(o, value); break;
    case CLSID_DBLVECELEM  : setProxyTypeValue<double,   t>(o, value); break;
    case CLSID_FLTVECELEM  : setProxyTypeValue<float,    t>(o, value); break;
    case CLSID_BOOLVECELEM : setProxyBoolValue<t>          (o, value); break;
    case CLSID_PTRVECELEM  : setProxyPtrValue<t>           (o, value); break;
  }
}

autotest::Variant getValue(ORef o) {
  autotest::Variant var;
  switch (o->getClassId()) {
    case CLSID_ULONG       : var.setValue(_P(unsigned long, o)->getValue()); break;
    case CLSID_LONG        : var.setValue(_P(long,     o)->getValue()); break;
    case CLSID_UINT        : var.setValue(_P(unsigned, o)->getValue()); break;
    case CLSID_INT         : var.setValue(_P(int,      o)->getValue()); break;
    case CLSID_SHORT       : var.setValue(_P(short,    o)->getValue()); break;
    case CLSID_CHAR        : var.setValue(_P(char,     o)->getValue()); break;
    case CLSID_DBL         : var.setValue(_P(double,   o)->getValue()); break;
    case CLSID_FLT         : var.setValue(_P(float,    o)->getValue()); break;
    case CLSID_BOOL        : var.setValue(_P(bool,     o)->getValue()); break;
    case CLSID_POINTER     : var.setValue(_P(void*,    o)->getValue()); break;
    case CLSID_STRING      : var.setValue(*(string *)_PStr(o)); break;
    case CLSID_ULONGVECELEM: var.setValue(_PTypeProxy(unsigned long, o)->getValue()); break;
    case CLSID_LONGVECELEM : var.setValue(_PTypeProxy(long    , o)->getValue()); break;
    case CLSID_UINTVECELEM : var.setValue(_PTypeProxy(unsigned, o)->getValue()); break;
    case CLSID_INTVECELEM  : var.setValue(_PTypeProxy(int     , o)->getValue()); break;
    case CLSID_SHORTVECELEM: var.setValue(_PTypeProxy(short   , o)->getValue()); break;
    case CLSID_CHARVECELEM : var.setValue(_PTypeProxy(char    , o)->getValue()); break;
    case CLSID_DBLVECELEM  : var.setValue(_PTypeProxy(double  , o)->getValue()); break;
    case CLSID_FLTVECELEM  : var.setValue(_PTypeProxy(float   , o)->getValue()); break;
    case CLSID_BOOLVECELEM : var.setValue(_PTypeProxy(bool    , o)->getValue()); break;
    case CLSID_PTRVECELEM  : var.setValue(_PTypeProxy(void*   , o)->getValue()); break;
  }
  return var;
}

std::pair<ORet, int> execMethod(Session* m, const string& method, ulong flags,
                                ORef rec, int argc, OArg* argv) {
  // get address of method
  OAddr methodAddress;
  OClass* c = rec->getClass();
  auto result = c->getMethodAccessAddress(method, nullptr);
  if (result.second.notValid()) {
    if (flags & EMF_NO_THROW_UNTIL_EXEC)
      return {OVar::nullResult(), EMR_METHOD_NOT_RESPONDING};
    else
      m->throwRTE(method, "class '"+c->name+
                          "' does not respond to method '"+method+"'");
  }
  else if (!result.first) {
    if (flags & EMF_NO_THROW_UNTIL_EXEC)
      return {OVar::nullResult(), EMR_METHOD_NOT_ACCESSIBLE};
    else
      m->throwRTE(method, "method "+method+" is not accessible");
  }
  else
    methodAddress = result.second;

  if (m->exceptionPending())
    return {OVar::nullResult(), 0};

  // execute method
  OVar retObj;
  if (methodAddress.scriptMethod) { // script method
    m->throwRTE(method, "method "+method+" is not accessible");
    return {OVar::nullResult(), 0};
  }
  else { // core method
    const auto& f = methodAddress.coreMethodAddress;
    retObj = (rec->*f)(argc, argv, m);
    if (retObj.isNull() && !m->exceptionPending())
      m->throwRTE(method, "error executing method "+method);
  }
  if (m->exceptionPending())
    return {OVar{m->rethrow(method, __FILE__, __LINE__)}, 0};

  return {retObj, EMR_OK};
}
