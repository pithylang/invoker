#include <iostream>
#include <cmath>
#include "autotest.hpp"

#define VAL1 24.3
#define VAL2 6.1

using namespace std;

int main()
{
  autotest::Variant result;
  // result.flags = autotest::TOLERATE_FP_INACC;
  int i = 0;
  // bool conversionError = false;

  const auto& resultType = autotest::applyOperation<autotest::Add>("int",
      /*(void*)&i*/VAL1, "float", VAL2, result/*, conversionError*/);
  // result.typeName = resultType;
  cout << "result type: " << resultType << endl;
  if (result.conversionError) cout << "conversion error" << endl;
  if (resultType.empty()) cout << "exception" << endl;
  if (!result.conversionError && !resultType.empty()) {
    bool b = (result == (int)VAL1 + (float)VAL2);
    cout << (b ? "success" : "failed") << endl;
  }
  // cout << "result: " << result.getValue<double>()
       // << " (" << (int)VAL1 + VAL2 << ")" << endl;

  return 0;
}
