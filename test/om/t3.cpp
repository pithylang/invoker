#include <string>
#include <iostream>
#include "object.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "oapi.h"
#include "odev.h"

/* ===================================================================
          Script Deployable Objects/Object Model SDO/SDOM
   -------------------------------------------------------------------
   Test 3: script class extension
   =================================================================== */

#define INTVAL(a) _PINT(a)->getValue()

static Session* thisSession = nullptr;

void handleException(ORef SO, Session* m);
void _delete(ORef id, Session *m) {if (id) id->getClass()->deleteObject(id, m);}
ORef& ref_(OVariable& var) noexcept {return var.objectRef;}

std::ostream& operator<<(std::ostream& os, const OVariable& var) {
  std::vector<OArg> noargs{};
  ref_(const_cast<OVariable&>(var))->applyMethod("print", noargs, thisSession);
  return os;
}

template<typename t>
void setValue(ORef o, t value) {*_P(t, o) = value;}
template<typename t>
void setValue(OVar& var, t value) {*_P(t, ref_(var)) = value;}

/*auto displayMessage = [](const std::string& title, const std::string& msg) {
  std::cout << title;
  if (title.back() != ':') std::cout << ':';
  std::cout << ' ' << msg;
  if (msg.back() != '\n') {
    if (msg.back() != '.') std::cout << '.';
    std::cout << '\n';
  }
};*/

using namespace std;

int main()
{
#ifdef _DEBUG
  cout << "_DEBUG defined" << endl;
#endif // _DEBUG

  cout << "Test 3: Simulating the extention of a base class" << endl;

  Session* m = sdoInitialize();
  if (m == nullptr)
  {
    cout << "could not initialize run-time object" << endl;
    return 0;
  }

  m->stringOutput = [](const string& msg) {cout << msg;};
  thisSession = m;
  ORef SO = (ORef)(ORTMgr*)m;

  OClass *stringClass = m->classMgr->getClass("string");
  if (stringClass == nullptr)
  {
    cout << "Could not find class 'string'\n";
    return 2;
  }

  vector<OArg> nullArgList;
  OVariable b, j, k, result;

  // create bool's
  ref_(b) = m->classMgr->getClass("bool")->createObject(m);
  setValue(b, true);
  cout << "b (bool) = " << b << endl;
//   ref_(b)->applyMethod("println", nullArgList, m);

  // create integers
  ref_(j) = m->classMgr->getClass("int")->createObject(m);
  setValue(j, 34);
  cout << "j (int) = " << j << endl;

  // Simulate script extension
  OVar className, baseClass, block;
  vector<OArg> argv{&className, &baseClass, &block};
  _ARG(0) = stringClass->createObject(m);
  _ARG(1) = stringClass->createObject(m);
  ref_(block) = m->classMgr->getClass("pointer")->createObject(m);
  setValue(block, (void*)nullptr);
  *_PStr(_ARG(0)) = "special int";
  *_PStr(_ARG(1)) = "int";
//   *_PPTR(_ARG(2)) = (void*)nullptr;

  // Create extension
//   result = SO->applyMethod("class:", argv, m);
  result = static_cast<ORTMgr*>(SO)->createClassObjectNoVM(3, argv.data(), m);
  if (m->exceptionPending()) {
    handleException(SO, m);
    return 2;
  }
  if (result.null()) {
    cout << "Could not override class" << endl;
    return 2;
  }
  cout << "result usage: 0x" << hex << result.usage << dec << ", type: "
       << (result.type == VARTYPE_CLASS
           ? "class"
           : (result.type == VARTYPE_OBJECT ? "object" : "unknown"))
       << endl;

  OClass* newClass = result.objectClass;

  // Add two members
  if (newClass->addProperty(m, "m_fpValue", "double", M_PROPERTY) != 0)
  {
    if (m->exceptionPending()) handleException(SO, m);
    cout << "Could not add property 'm_fpValue'" << endl;
    return 2;
  }
  if (newClass->addProperty(m, "m_extraInfo", "string", M_PROPERTY) != 0)
  {
    if (m->exceptionPending()) handleException(SO, m);
    cout << "Could not add property 'm_extraInfo'" << endl;
    return 2;
  }
  newClass->finalize(m);

  if (newClass->addProperty(m, "failure", "bool", M_PROPERTY) != 0)
  {
    if (m->exceptionPending()) handleException(SO, m);
    cout << "Could not add property 'failure' after finalization" << endl;
  }

  // Check consistency
  if (m->classMgr->getClass("special int") != newClass)
  {
    cout << "Inconsistent class creation" << endl;
    return 2;
  }

  // Create special int
  m->displayMessage("Part 1", "create 'special int'");

  ref_(k) = newClass->createObject(m);
  setValue(k, (int)43);
  cout << "special int k = " << k << endl;

  // Copy from int to special int
  m->displayMessage("Part 2", "copy from int to 'special int'");
//   auto rc = newClass->copyObject(ref_(k), ref_(j), m);
  {
    vector<OArg> args{&j};
    auto rc = ref_(k)->applyMethod("copy:", args, m);
    if (rc.null()) cout << "error copying object" << endl;
  }
  if (m->exceptionPending())
  {
    OVar z;
    vector<OArg> argv{&z};
    _ASSERT(_ARG(0) == nullptr, m);
    auto e = SO->applyMethod("getException:", argv, m);
    _ASSERT(ref_(e) != nullptr, m);
    cout << "exception: " << ref_(e)->getClassName() << endl;
    // m->clearException();
    _delete(ref_(e), m);
  }

  cout << "'special int' k = " << k << " (should be " << j << ")" << endl;

  // Copy from int to special int
  m->displayMessage("Part 3", "copy from 'special int' to int");
  setValue(ref_(k), 671);
  {
    auto rc = ref_(j)->getClass()->copyObject(ref_(j), ref_(k), m);
    if (rc.null())
      cout << "error copying object" << endl;
  }
  if (m->exceptionPending())
    handleException(SO, m);

  cout << "int j = " << j << " (should be: " << k << ")" << endl;

  // print classes
  int scratch;
  string printout;
  StringPrinter sp = [&printout](const std::string& s) {printout += s;};
  cout << "Press any key to continue..."; cin >> scratch;
  ref_(j)->getClass()->print(sp, true);
  cout << printout << endl;
  printout = "";
  cout << "Press any key to continue..."; cin >> scratch;
  ref_(k)->getClass()->print(sp, false);
  cout << printout << endl;
  printout = "";
  cout << "Press any key to continue..."; cin >> scratch;

  // clean-up
  for (int ii = 0; ii < 3; ++ii)
    if (_ARG(ii) != nullptr) _delete(_ARG(ii), m);

  _delete(ref_(j), m);
  _delete(ref_(k), m);
  _delete(ref_(b), m);

//   delete newClass;

  m->terminate();

  return 0;
}

void handleException(ORef SO, Session* m)
{
  vector<OArg> argv(1);
  _ASSERT(_ARG(0) == nullptr, m);
  auto e = SO->applyMethod("getException:", argv, m);
  _ASSERT(ref_(e) != nullptr, m);
  cout << "exception: " << ref_(e)->getClassName() << endl;
  _delete(ref_(e), m);
}
