// #include <stdlib>
// #include <stdio>
#include <string>
#include <iostream>
// #include "sx.h"
// #include "oxclass.hpp"
#include "object.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "ortmgr.hpp"
#include "oapi.h"
#include "odev.h"

/* ===================================================================
          Script Deployable Objects/Object Model SDO/SDOM
   __step_1__  osrt system initialization/termination, general object
               construction, destruction methods.
   __step_2__  d-derivation
   __step_3__  object trees
   =================================================================== */

#define __step_1__

#define INTVAL(a) _PINT(a)->getValue()

// extern const char _OM_LINK *bsel[];

// OClassMgr *initSEOSystem(Session **m);

void _delete(ORef id, Session *m)
{ if (id != 0) id->getClass()->deleteObject(id, m); }

ORef& _ref(OVariable& var) {return var.objectRef;}

auto displayMessage = [](const std::string& title, const std::string& msg) {
  std::cout << title;
  if (title.back() != ':') std::cout << ':';
  std::cout << ' ' << msg;
  if (msg.back() != '\n') {
    if (msg.back() != '.') std::cout << '.';
    std::cout << '\n';
  }
};

/*===================================================================
  Rules for osObject-derived classes
  ===================================================================
1. All object instances are created on the heap. Create instances on
   the stack only for temporary usage (within a method).
2. When the return value (RV) is immaterial (soft copied) the self
   ptr may be returned. When the RV is essential, ie this is what
   matters when the method is called, a new instance must be created,
   which encapsulates the RV and returned. Temporary RV's are
   automatically put in the trashcan when a DOV-Machine is present.
  ===================================================================*/

extern "C" {
void exit_fn();
};

using namespace std;

#if defined(                      __step_1__                      )

int main()
{
   atexit(exit_fn);

   cout << "step 1" << endl;

#ifdef _DEBUG
   cout << "_DEBUG defined" << endl;
#endif // _DEBUG

   // Script Extensible Object system initialization
   Session *m = sdoInitialize();
   if (m == nullptr)
   {
     cout << "could not initialize run-time object" << endl;
     return 0;
   }

   m->stringOutput = [](const string& msg) {cout << msg;};
   m->displayMessage = displayMessage;
   // [](const string& title, const string& msg) {
     // cout << title;
     // if (title.back() != ':') cout << ':';
     // cout << ' ' << msg;
     // if (msg.back() != '\n') {
       // if (msg.back() != '.') cout << '.';
       // cout << '\n';
     // }
   // };

   OVariable k, j;
   ORef& k_ = k.objectRef;
   ORef& j_ = j.objectRef;
   OClass *intClass = m->classMgr->getClass("int");

   k_ = intClass->createObject(m);
   j_ = intClass->createObject(m);
   _PINT(k_)->setValue(3);
   _PINT(j_)->setValue(-2);


   // cout << "k = " << INTVAL(k) << endl <<
           // "j = " << INTVAL(j) << endl ;
  k_->applyMethod("println", vector<OArg>(), m);
  j_->applyMethod("println", vector<OArg>(), m);
  // ORet r = j->applyMethod("preDec", {{j, 0}}, m);
  ORet r = j_->applyMethod("inc", vector<OArg>(), m);
  // if (m->exceptionPending()) {
    // cout << "a VM exception was thrown" << endl;
    // return 1;
  // }
  ORet result = j_->applyMethod("add:", {&k}, m);
  result.objectRef->applyMethod("println", vector<OArg>(), m);

  if (r.objectRef && r.disposable()) {
    cout << "r is disposable\n";
    _delete(r.objectRef, m);
  }
  if (result.disposable()) {
    cout << "result is disposable\n";
    _delete(result.objectRef, m);
  }
  _delete(k_, m);
  _delete(j_, m);

  m->terminate();

  return 0;
}

#elif defined(                    __step_2__                    )

#include "ostring.hpp"

int main()
{
  atexit(exit_fn);

  cout << "Testing Script Deployable Objects. Step2: Overriding a base class.\n\n";

  vector<OArg> nullArgList;
  Session *m = sdoInitialize();
  const char *sss = "string";

  OClass *stringClass = m->classMgr->getClass(sss);
  if (stringClass == nullptr)
  {
    cout << "Could not find class '" << sss << "'\n";
    exit(2);
  }

  // create bool's
  ORet b = m->classMgr->getClass("bool")->createObject(m);
  _PBOOL(b)->setValue(true);
  cout << "bool = ";
  b->objectRef->applyMethod("println", 0, nullArgList, m);

  // create integers
  OVariable j, k;
  _ref(j) = m->classMgr->getClass("int")->createObject(m);
  _PINT(j)->setValue(34);

  // Simulate derivation
  oxClass *newClass;
  vector<OArg> argv{nullptr,nullptr,nullptr};
  MMethodAddress classConstructor;
  if (!OClass::getMMethodAddressByName("createClass", &classConstructor))
    goto create_class_error;
  _ARG(0) = stringClass->createObject(m);
  _ARG(1) = stringClass->createObject(m);
  _ARG(2) = NULLID;
  *_PStr(_ARG(0)) = "special int";
  *_PStr(_ARG(1)) = "int";
  newClass = (OClass *)(void *) classConstructor((OClass*)(void*)m, argv);
  if (m->exceptionPending() || newClass == nullptr) goto create_class_error;
  newClass->beginClassDef();
  // add two members
  if (!newClass->addMember("m_fpValue", "double", M_PROPERTY))
    goto create_class_error;
  if (!newClass->addMember("m_extraInfo", "string", M_PROPERTY))
    goto create_class_error;
  newClass->finalize();
  goto create_class_ok;

create_class_error:
  cout << "Could not override class" << endl;
  return 2;

create_class_ok:

  // create special int
  sxMessage("Test no. 00", "create special int");

  k = oxGetClass("special int")->createObject(m);

  _PINT(k)->setValue(43);
  cout << "special int k = ";
  k->applyMethod("println", 0, NULL, m);

  // copy from int to special int
  sxMessage("Test no. 01", "copy from int to special int");
  oxId rc = newClass->copyObject(k, j, m);
//   oxId rc = oxGetClass("int")->copyObject(k, j, m);
  if (rc == nullid)
    cout << "error copying object." << endl;
  if (m->exceptionPending())
  {
    oxId e = ((_Session *)(void*)m)->m_exception;
    cout << "exception: " << e->getClassName() << endl;
//     ((_Session *)(void*)m)->m_exception = NULLID;
    m->clearException();
  }

  cout << "special int k = ";
  k->applyMethod("println", 0, NULL, m);
  cout << "(should be: " << _PINT(j)->getValue() << ")" << endl;

  // copy from int to special int
  sxMessage("Test no. 02", "copy from special int to int");
/*  _PINT(k)->setValue(671);
  rc = j->getClass()->copyObject(j, k, m);
  if (rc == nullid)
    cout << "error copying object." << endl;
*/  if (m->exceptionPending())
  {
    oxId e = ((_Session *)(void*)m)->m_exception;
    cout << "exception: " << e->getClassName() << endl;
  }

  cout << "int j = ";
  j->applyMethod("println", 0, NULL, m);
//  cout << "(should be: " << _PINT(k)->getValue() << ")" << endl;

  // print classes
  system("PAUSE");
  j->getClass()->print(stdout, false);
  system("PAUSE");
//  k->getClass()->print(stdout, false);
//  system("PAUSE");

  // clean-up
  for (int ii = 0; ii < 3; ++ii)
  {
    if (_ARG(ii) != NULLID)
      _delete(_ARG(ii), m);
  }

  j->applyMethod("delete", 0, NULL, m);
  k->applyMethod("delete", 0, NULL, m);
  b->applyMethod("delete", 0, NULL, m);

  delete newClass;

  // terminate
  rtmObject->deleteObject(m);
  classMgr->term();

  return 0;
}

#elif defined(__step_3__)

class ObjectTreeNode;

#include "sxstring.hpp"

int main()
{
   atexit(exit_fn);

   cout << "Testing DynaObjects. Step2: Overriding a base class.\n\n";

   Session *m;
   sxClassMgr *classMgr = initSEOSystem(&m);
   oxId rtmObject = (oxId)(ORTMgr *) m;

   const char *sss = "string";

   oxClass *stringClass = oxGetClass(sss);
   if (stringClass == NULL)
   {
      cout << "Could not find class `" << sss << "'\n";
      system("PAUSE");
      exit(2);
   }

   // create bool's
   oxId b = oxGetClass("bool")->createObject(m);
   _PBOOL(b)->setValue(true);
   cout << "bool = ";
   b->applyMethod("println", 0, NULL, m);

   // create integers
   oxId j, k;
   j = oxGetClass("int")->createObject(m);
   _PINT(j)->setValue(34);

   // Simulate derivation
   oxClass *newClass;
   osArg argv[3] = {NULL,NULL,NULL};
   MMethodAddress classConstructor;
   if (!oxClass::getMMethodAddressByName("createClass", &classConstructor))
      goto create_class_error;
   _ARG(0) = stringClass->createObject(m);
   _ARG(1) = stringClass->createObject(m);
   _ARG(2) = NULLID;
   *_PStr(_ARG(0)) = "special int";
   *_PStr(_ARG(1)) = "int";
   newClass = (oxClass *)(void *) classConstructor(NULL, argv);
   if (newClass == NULL)
      goto create_class_error;
   newClass->beginClassDef();
   // add two members
   if (!newClass->addMember("m_fpValue", "double", M_PROPERTY))
      goto create_class_error;
   if (!newClass->addMember("m_extraInfo", "string", M_PROPERTY))
      goto create_class_error;
   newClass->finalize();
   goto create_class_ok;

create_class_error:
   cout << "Could not override class" << endl;
   return 2;

create_class_ok:

   // create special int
   sxMessage("Test no. 00", "create special int");
   k = oxGetClass("special int")->createObject(m);
   _PINT(k)->setValue(43);
   cout << "special int k = ";
   k->applyMethod("println", 0, NULL, m);

   // Object tree for special int
   sxMessage("Test no. 01", "object tree for special int");
   ObjectTreeNode *node0;
   oxGetClass("special int")->buildObjectTree(k, "a_special_int", &node0);

   // Flat object tree representation for special int
   sxMessage("Test no. 02", "flat object tree for special int");
   ObjectTreeNode *node1;
   oxGetClass("special int")->buildObjectFlatTree(k, "a_special_int", &node1);

   // print classes
   system("PAUSE");


   // clean-up
   for (int ii = 0; ii < 3; ++ii)
   {
      if (_ARG(ii) != NULLID)
         _delete(_ARG(ii), m);
   }

   j->applyMethod("delete", 0, NULL, m);
   k->applyMethod("delete", 0, NULL, m);

   // terminate
   rtmObject->deleteObject(m);
   classMgr->term();

   return 0;
}

#elif defined(__step_4__)

#define F(x, s) \
   x("%s\n", #s);

int main()
{
   F(printf, apostol faliagas ok);
   return 0;
}

#else
#error __step_n__ must be defined
#endif

#if 0
void exit_fn()
{
   if (sxMemMgrQuery(QMM_MEMMGR_PRESENT, NULL, NULL))
   {
      int cbUser, cbSystem;
      bool bDiags;

      sxMemMgrQuery(QMM_VALID, &bDiags, NULL);
      sxMemMgrQuery(QMM_USER_BYTES, &cbUser, NULL);
      sxMemMgrQuery(QMM_SYSTEM_BYTES, &cbSystem, NULL);

      if (bDiags)
         cout << "Diags on allocator passed" << endl;
      else
         cout << "Diags on allocator failed" << endl;

      cout << "User: " << cbUser << endl;
      cout << "Sys:  " << cbSystem << endl;

      sxMemMgrDump("alloc.txt");
   }
   else
      cout << "Memory manager not present" << endl;
}
#endif // 0

void exit_fn() {}

OClassMgr *initSEOSystem(Session **sessionPtr)
{
  OClassMgr *classMgr = (OClassMgr *)(void *) sdoInitialize();

  if (classMgr == nullptr)
  {
    cout << "could not initialize SEO run-time system" << endl;
    exit(2);
  }

  ORef rtmObject = nullptr;
  OClass *rtmClass = classMgr->getClass(OM_RTMgrName);
  if (rtmClass != nullptr)
    rtmObject = rtmClass->createObject(nullptr);
  if (rtmObject == nullptr)
  {
    cout << "could not initialize run-time object" << endl;
    exit(2);
  }

  Session *m = (Session *) _PRTMgr(rtmObject);
  m->stringOutput = [](const string& msg) {cout << msg;};
  m->displayMessage = [](const string& title, const string& msg) {
    cout << title;
    if (title.back() != ':') cout << ':';
    cout << ' ' << msg;
    if (msg.back() != '\n') {
      if (msg.back() != '.') cout << '.';
      cout << '\n';
    }
  };

  m->classMgr = classMgr;

  *sessionPtr = m;
  return classMgr;
}
