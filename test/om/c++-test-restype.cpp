#include <iostream>
#include <cmath>

#define RET_IF_VALUE_IS_TYPE(val, t) \
  if (std::is_same<decltype(val), t>::value) return #t
#define RETURN_ITYPE_OF(val) \
  RET_IF_VALUE_IS_TYPE(val, unsigned long); \
  RET_IF_VALUE_IS_TYPE(val, long); \
  RET_IF_VALUE_IS_TYPE(val, unsigned); \
  RET_IF_VALUE_IS_TYPE(val, int); \
  RET_IF_VALUE_IS_TYPE(val, unsigned short); \
  RET_IF_VALUE_IS_TYPE(val, short); \
  RET_IF_VALUE_IS_TYPE(val, unsigned char); \
  RET_IF_VALUE_IS_TYPE(val, char); \
  RET_IF_VALUE_IS_TYPE(val, bool);

template<typename t>
std::string getIType(t value) {
  RETURN_ITYPE_OF(value);
  return "unknown type";
}

#define SAY_(lv, op, rv) \
  "The resulting type of " #lv " " op " " #rv " is: "
  
using namespace std;

int main()
{
  bool b = true;
  long k = 1;
  unsigned long ul = 1;
  
  cout << SAY_(ulong, "<<", ulong) << getIType(ul << (unsigned long)1) << endl;
  cout << SAY_(long,  "<<", long ) << getIType(k  << (long)1         ) << endl;
  cout << SAY_(bool,  "<<", int  ) << getIType(b  << (int)1          ) << endl;
  cout << SAY_(bool,  "<<", bool ) << getIType(b  << true            ) << endl;
  cout << SAY_(bool,  "<<", ulong) << getIType(b  << (unsigned long)1) << endl;
  cout << SAY_(bool,  "&",  int  ) << getIType(b  &  (int)1          ) << endl;
  cout << SAY_(bool,  "&",  bool ) << getIType(b  &  true            ) << endl;
  cout << SAY_(bool,  "^",  ulong) << getIType(b  ^  (unsigned long)1) << endl;
  cout << SAY_(bool,  "^",  long ) << getIType(b  ^  (long)1         ) << endl;
  cout << SAY_(bool,  "^",  int  ) << getIType(b  ^  (int)1          ) << endl;
  cout << SAY_(bool,  "^",  bool ) << getIType(b  ^  true            ) << endl;
  cout << SAY_(int,   "<<", ulong) << getIType((int)1 & ul) << endl;

  return 0;
}

