#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include "sx.h"
#include "oxclass.hpp"
#include "sxclsmgr.hpp"
#include "oxobject.hpp"
#include "sxtype.hpp"
#include "sxdev.h"
#include "sxrtmgr.hpp"

/* ===================================================================
   __step_1__  osrt system initialization/termination, general object
               construction, destruction methods.
   __step_2__  d-derivation
   __step_3__  object trees
   =================================================================== */

#define __step_2__

#define INTVAL(a) _PINT(a)->getValue()

extern const char _OX_LINK *bsel[];

sxClassMgr *InitOSSystem(Session **rtm);

void _delete(oxId id, Session *m)
{  id->getClass()->deleteObject(id, m); }

void DisplayMsg(const char *title, const char *msg)
{
   printf("%s: %s\n", title, msg);
}

   struct _Session
   {
      RTEnvironment     m_Environment;
      oxId              m_exception;
   };

/*===================================================================
  Rules for osObject-derived classes
  ===================================================================
1. All object instances are created on the heap. Create instances on
   the stack only for temporary usage (within a method).
2. When the return value (RV) is immaterial (soft copied) the self
   ptr may be returned. When the RV is essential, ie this is what
   matters when the method is called, a new instance must be created,
   which encapsulates the RV and returned. Temporary RV's are
   automatically put in the trashcan when a DOV-Machine is present.
  ===================================================================*/

extern "C" {
void exit_fn();
};

#if defined(__step_1__)
int main()
{
   atexit(exit_fn);
   int i;                      
   
   cout << "step 1" << endl;

#ifdef _DEBUG
   cout << "_DEBUG defined" << endl ;
#endif // _DEBUG

   sxSetDisplayMsgProc(DisplayMsg);
    
   // SXC system initialization
   sxClassMgr *m = (sxClassMgr *)(void *) CilInitialize();
   
   oxId rtm_id = nullid;
   oxClass *rtm_cls = oxGetClass(SX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject(NULL);
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      return 0;
   }
   
   Session *rtm = (Session *) _PRTMgr(rtm_id);

   oxId k, j;
   
   k = m->findClass("int")->createObject(rtm);
   _PINT(k)->setValue(3);
   j = oxGetClass("int")->createObject(rtm);
   _PINT(j)->setValue(-2);

   cout << "k = " << INTVAL(k) << endl <<
           "j = " << INTVAL(j) << endl ;

   _delete(k, rtm);
   _delete(j, rtm);
   
   rtm_id->deleteObject(rtm);

   m->term();

   system("PAUSE");

   return 0;
}

#elif defined(__step_2__)
#include "sxstring.hpp"

int main()
{
   atexit(exit_fn);

   cout << "Testing DynaObjects. Step2: Overriding a base class.\n\n";
   
   Session *rtm;
   sxClassMgr *m = InitOSSystem(&rtm);
   oxId rtm_id = (oxId)(ORTMgr *) rtm;
   
   const char *sss = "string";
   
   oxClass *string_class = oxGetClass(sss);
   if (string_class == NULL)
   {
      cout << "Could not find class `" << sss << "'\n";
      system("PAUSE");
      exit(2);
   }                
   
   // create bool's
   oxId b = oxGetClass("bool")->createObject(rtm);
   _PBOOL(b)->setValue(true);
   oxGetClass("bool")->print(stdout, true);
      system("PAUSE");
   oxGetClass("int")->print(stdout, true);
      system("PAUSE");
   cout << "bool = ";
   b->applyMethod("println", 0, NULL, rtm);
   
   // create integers
   oxId j, k;
   j = oxGetClass("int")->createObject(rtm);
   _PINT(j)->setValue(34);

   // Simulate derivation
   oxClass *new_class;
   sxArg argv[3] = {NULL,NULL,NULL};
   MMethodAddress class_constructor;
   if (!oxClass::getMMethodAddressByName("createClass", &class_constructor))
      goto create_class_error;
   _ARG(0) = string_class->createObject(rtm);
   _ARG(1) = string_class->createObject(rtm);
   _ARG(2) = NULLID;
   *_PStr(_ARG(0)) = "special int";
   *_PStr(_ARG(1)) = "int";
   new_class = (oxClass *)(void *) class_constructor((oxClass*)(void*)rtm, argv);
   if (rtm->exceptionPending() || new_class == NULL)
      goto create_class_error;
   new_class->beginClassDef();
   // add two members
   if (!new_class->addMember("m_fpValue", "double", M_PROPERTY))
      goto create_class_error;
   if (!new_class->addMember("m_extraInfo", "string", M_PROPERTY))
      goto create_class_error;
   new_class->finalize();
   goto create_class_ok;
    
create_class_error:
   cout << "Could not override class" << endl;
   return 2;
   
create_class_ok:
                     
   // create special int
   sxMessage("Test no. 00", "create special int");
   k = oxGetClass("special int")->createObject(rtm);
   _PINT(k)->setValue(43);
   cout << "special int k = ";
   k->applyMethod("println", 0, NULL, rtm);
   
   // copy from int to special int
   sxMessage("Test no. 01", "copy from int to special int");
   oxId rc = new_class->copyObject(k, j, rtm);
   if (rc == nullid)
      cout << "error copying object." << endl;
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
//      ((_Session *)(void*)rtm)->m_exception = NULLID;
      rtm->clearException();
   }
   
   cout << "special int k = ";
   k->applyMethod("println", 0, NULL, rtm);
   cout << "(should be: " << _PINT(j)->getValue() << ")" << endl;

   // copy from int to special int
   sxMessage("Test no. 02", "copy from special int to int");
   _PINT(k)->setValue(671);
   rc = j->getClass()->copyObject(j, k, rtm);
   if (rc == nullid)
      cout << "error copying object." << endl;
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
   }
   
   cout << "int j = ";
   j->applyMethod("println", 0, NULL, rtm);
   cout << "(should be: " << _PINT(k)->getValue() << ")" << endl;
   
   // print classes
   system("PAUSE");
   j->getClass()->print(stdout, false);
   system("PAUSE");
   k->getClass()->print(stdout, false);
   system("PAUSE");

   // clean-up   
   for (int ii = 0; ii < 3; ++ii)
   {
      if (_ARG(ii) != NULLID)
         _delete(_ARG(ii), rtm);
   }
              
   j->applyMethod("delete", 0, NULL, rtm);
   k->applyMethod("delete", 0, NULL, rtm);
   b->applyMethod("delete", 0, NULL, rtm);
   
   delete new_class;

   // terminate
   rtm_id->deleteObject(rtm);
   m->term();

   return 0;
}

#elif defined(__step_3__)

class ObjectTreeNode;

#include "sxstring.hpp"

int main()
{
   atexit(exit_fn);

   cout << "Testing DynaObjects. Step2: Overriding a base class.\n\n";
   
   Session *rtm;
   sxClassMgr *m = InitOSSystem(&rtm);
   oxId rtm_id = (oxId)(ORTMgr *) rtm;
   
   const char *sss = "string";
   
   oxClass *string_class = oxGetClass(sss);
   if (string_class == NULL)
   {
      cout << "Could not find class `" << sss << "'\n";
      system("PAUSE");
      exit(2);
   }                
   
   // create bool's
   oxId b = oxGetClass("bool")->createObject(rtm);
   _PBOOL(b)->setValue(true);
   cout << "bool = ";
   b->applyMethod("println", 0, NULL, rtm);
   
   // create integers
   oxId j, k;
   j = oxGetClass("int")->createObject(rtm);
   _PINT(j)->setValue(34);

   // Simulate derivation
   oxClass *new_class;
   osArg argv[3] = {NULL,NULL,NULL};
   MMethodAddress class_constructor;
   if (!oxClass::getMMethodAddressByName("createClass", &class_constructor))
      goto create_class_error;
   _ARG(0) = string_class->createObject(rtm);
   _ARG(1) = string_class->createObject(rtm);
   _ARG(2) = NULLID;
   *_PStr(_ARG(0)) = "special int";
   *_PStr(_ARG(1)) = "int";
   new_class = (oxClass *)(void *) class_constructor(NULL, argv);
   if (new_class == NULL)
      goto create_class_error;
   new_class->beginClassDef();
   // add two members
   if (!new_class->addMember("m_fpValue", "double", M_PROPERTY))
      goto create_class_error;
   if (!new_class->addMember("m_extraInfo", "string", M_PROPERTY))
      goto create_class_error;
   new_class->finalize();
   goto create_class_ok;
    
create_class_error:
   cout << "Could not override class" << endl;
   return 2;
   
create_class_ok:
                     
   // create special int
   sxMessage("Test no. 00", "create special int");
   k = oxGetClass("special int")->createObject(rtm);
   _PINT(k)->setValue(43);
   cout << "special int k = ";
   k->applyMethod("println", 0, NULL, rtm);
   
   // Object tree for special int
   sxMessage("Test no. 01", "object tree for special int");
   ObjectTreeNode *node0;
   oxGetClass("special int")->buildObjectTree(k, "a_special_int", &node0);

   // Flat object tree representation for special int
   sxMessage("Test no. 02", "flat object tree for special int");
   ObjectTreeNode *node1;
   oxGetClass("special int")->buildObjectFlatTree(k, "a_special_int", &node1);
   
   // print classes
   system("PAUSE");


   // clean-up   
   for (int ii = 0; ii < 3; ++ii)
   {
      if (_ARG(ii) != NULLID)
         _delete(_ARG(ii), rtm);
   }
              
   j->applyMethod("delete", 0, NULL, rtm);
   k->applyMethod("delete", 0, NULL, rtm);
   
   // terminate
   rtm_id->deleteObject(rtm);
   m->term();

   return 0;
}

#else
#error __step_n__ must be defined
#endif

void exit_fn()
{
   if (sxMemMgrQuery(QMM_MEMMGR_PRESENT, NULL, NULL))
   {
      int cbUser, cbSystem;
      bool bDiags;

      sxMemMgrQuery(QMM_VALID, &bDiags, NULL);
      sxMemMgrQuery(QMM_USER_BYTES, &cbUser, NULL);
      sxMemMgrQuery(QMM_SYSTEM_BYTES, &cbSystem, NULL);

      if (bDiags)
         cout << "Diags on allocator passed" << endl;
      else
         cout << "Diags on allocator failed" << endl;

      cout << "User: " << cbUser << endl;
      cout << "Sys:  " << cbSystem << endl;
   
      sxMemMgrDump("alloc.txt");
   }
   else
      cout << "Memory manager not present" << endl;
}

sxClassMgr *InitOSSystem(Session **rtm)
{
   sxSetDisplayMsgProc(DisplayMsg);
   
   sxClassMgr *m = (sxClassMgr *)(void *) CilInitialize();
   
   if (m == NULL)
   {
      cout << "could not initialize OS run-time system" << endl;
      exit(2);
   }
   
   oxId rtm_id = NULLID;
   oxClass *rtm_cls = m->findClass(SX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject(NULL);
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      exit(2);
   }
   
   *rtm = (Session *) _PRTMgr(rtm_id);
   return m;
}

