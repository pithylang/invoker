#include <csignal>
#include <iostream>
#include <memory>

/* * * * * *  Compile with -fnon-call-exceptions  * * * * * */

using namespace std;

int main()
{
  std::shared_ptr<void(int)> handler(
      signal(SIGFPE, [](int signum) {throw std::logic_error("FPE");}),
      [](__sighandler_t f) {signal(SIGFPE, f);}
  );

  int i, j = 10, k;
  cout << "input a number: "; cin >> i;
  try {
    k = j / i;
  } catch (const string& msg) {
    cout << "exception occurred: " << msg << endl;
    return 1;
  } catch (const logic_error& e) {
    cout << "exception occurred: " << e.what() << endl;
    return 1;
  } catch (...) {
    cout << "exception occurred" << endl;
    return 1;
  }
  cout << "result: " << k << endl;

  return 0;
}
