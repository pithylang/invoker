#include <iostream>
#include <cmath>

static bool globalError = false;

template<typename t, typename s>
t cast(s val, t) {return (t)val;}
unsigned cast(void *val, unsigned) {globalError = true; return 0;}
int cast(void *val, int) {globalError = true; return 0;}
short cast(void *val, short) {globalError = true; return 0;}
char cast(void *val, char) {globalError = true; return 0;}
bool cast(void *val, bool) {globalError = true; return 0;}
double cast(void *val, double) {globalError = true; return 0;}
float cast(void *val, float) {globalError = true; return 0;}

#define RET_IF_VALUE_IS_TYPE(val, t) \
  if (std::is_same<decltype(val), t>::value) return #t
#define RETURN_TYPE_OF(val) \
  RET_IF_VALUE_IS_TYPE(val, unsigned long); \
  RET_IF_VALUE_IS_TYPE(val, long); \
  RET_IF_VALUE_IS_TYPE(val, unsigned); \
  RET_IF_VALUE_IS_TYPE(val, int); \
  RET_IF_VALUE_IS_TYPE(val, unsigned short); \
  RET_IF_VALUE_IS_TYPE(val, short); \
  RET_IF_VALUE_IS_TYPE(val, unsigned char); \
  RET_IF_VALUE_IS_TYPE(val, char); \
  RET_IF_VALUE_IS_TYPE(val, bool); \
  RET_IF_VALUE_IS_TYPE(val, double); \
  RET_IF_VALUE_IS_TYPE(val, float); \
  RET_IF_VALUE_IS_TYPE(val, void*);

template<typename t>
std::string getType(t value) {
  RETURN_TYPE_OF(value);
  return "";
}
#undef RETURN_TYPE_OF
#undef RET_IF_VALUE_IS_TYPE

#define DEFINE_PTROP(opname, opsym) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  void *apply(LV_TYPE a, RV_TYPE b) {return nullptr;} \
  template<typename RV_TYPE> \
  void *apply(void *a, RV_TYPE b) \
  { return (void*)(((char*)a)+(unsigned long)b); } \
  void *apply(void *a, double b) {return nullptr;} \
  void *apply(void *a, float b) {return nullptr;} \
};
#define DEFINE_NOPTROP(opname, opsym) \
struct Ptr##opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  void *apply(LV_TYPE a, RV_TYPE b) {return nullptr;} \
};

DEFINE_PTROP(Add, +)
DEFINE_PTROP(Sub, -)
DEFINE_NOPTROP(Mul, *)
DEFINE_NOPTROP(Div, /)

#define DEFINE_OPERATION(opname, opsym) \
struct opname { \
  template<typename LV_TYPE, typename RV_TYPE> \
  decltype((LV_TYPE)0 opsym (RV_TYPE)0) \
  apply(LV_TYPE a, RV_TYPE b) {return a opsym b;} \
  Ptr##opname operator()(int) {return Ptr##opname();} \
};

DEFINE_OPERATION(Add, +)
DEFINE_OPERATION(Sub, -)
DEFINE_OPERATION(Mul, *)
DEFINE_OPERATION(Div, /)

#undef DEFINE_OPERATION

#define DO_OP_IF_RV_IS_TYPE(t, s) \
if (rvType == #s) { \
  auto result = OP().apply(cast(lvAny, (t)0), cast(rvAny, (s)0)); \
  *(decltype(result) *)pResAny = result; \
  return getType(result); \
}

#define DO_PTROP_IF_RV_IS_TYPE(OP, s) \
if (rvType == #s) { \
  auto result = OP()(1).apply(lvAny, cast(rvAny, (s)0)); \
  *(decltype(result) *)pResAny = result; \
  return getType(result); \
}

#define DO_OP_IF_LV_IS_TYPE(t) \
if (lvType == #t) { \
  DO_OP_IF_RV_IS_TYPE(t, unsigned long) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, long) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, unsigned) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, int) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, short) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, char) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, bool) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, double) \
  else \
  DO_OP_IF_RV_IS_TYPE(t, float) \
}

#define DO_OP_IF_LV_IS_PTR(OP) \
if (lvType == "void*") { \
  DO_PTROP_IF_RV_IS_TYPE(OP, unsigned long) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, long) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, unsigned) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, int) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, short) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, char) \
  else \
  DO_PTROP_IF_RV_IS_TYPE(OP, bool) \
}

#if 0
if (lvType == #t) {
  if (rvType == "unsigned long") {
    auto result = OP().apply((t)lvAny, (unsigned long)rvAny);
    *(decltype(result) *)pResAny = result;
    return getType(result);
  } else if (rvType == "long") {
  } else if (rvType == "unsigned") {
  } else if (rvType == "int") {
  } else if (rvType == "short") {
  } else if (rvType == "char") {
  } else if (rvType == "bool") {
  } else if (rvType == "pointer") {
  } else if (rvType == "double") {
  } else if (rvType == "float") {
  }
}
#endif // 0

template<typename OP, typename LV_TYPE, typename RV_TYPE>
std::string applyOperation(
    const std::string& lvType, LV_TYPE lvAny,
    const std::string& rvType, RV_TYPE rvAny, void *pResAny) {
  DO_OP_IF_LV_IS_TYPE(unsigned long)
  else
  DO_OP_IF_LV_IS_TYPE(long)
  else
  DO_OP_IF_LV_IS_TYPE(unsigned)
  else
  DO_OP_IF_LV_IS_TYPE(int)
  else
  DO_OP_IF_LV_IS_TYPE(short)
  else
  DO_OP_IF_LV_IS_TYPE(char)
  else
  DO_OP_IF_LV_IS_TYPE(bool)
  else
  DO_OP_IF_LV_IS_TYPE(double)
  else
  DO_OP_IF_LV_IS_TYPE(float)
  else
  DO_OP_IF_LV_IS_PTR(OP)
  return "";
}

#define VAL1 24.3
#define VAL2 6

using namespace std;

int main()
{
  cout<<"Hello World\n";
  char data[64];
  int i = 0;
  
  auto resultType = applyOperation<Add>("int", VAL2, "void*", (void*)&i, (void *)data);
  cout << resultType << endl;
  if (globalError) cout << "error" << endl;
  if (resultType.empty()) cout << "exception" << endl;

  return 0;
}

