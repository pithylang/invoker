#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include "sx.h"
#include "oxclass.hpp"
#include "sxclsmgr.hpp"
#include "oxobject.hpp"
#include "sxtype.hpp"
#include "sxdev.h"
#include "sxrtmgr.hpp"

/* ===================================================================
   __STEP_1__  rt system initialization/termination, general object
               construction, destruction methods.
   __STEP_2__  object vector
   __STEP_3__  application object
   =================================================================== */

#define __STEP_4__

#define INTVAL(a) _PINT(a)->getValue()

const char * _OX_LINK bsel[];

oxClassMgr *InitSxcSystem(Session **rtm);

void _delete(oxId id, Session *m)
{  id->getClass()->deleteObject(id, m); }

void DisplayMsg(const char *title, const char *msg)
{
   printf("%s: %s\n", title, msg);
}

   struct _Session
   {
      RTEnvironment     m_Environment;
      oxId              m_exception;
   };

/*===================================================================
  Rules for oxObject-derived classes
  ===================================================================
1. All object instances are created on the heap. Create instances on
   the stack only for temporary usage (within a method).
2. When the return value (RV) is immaterial (soft copied) the self
   ptr may be returned. When the RV is essential, ie this is what
   matters when the method is called, a new instance must be created,
   which encapsulates the RV and returned. Temporary RV's are
   automatically put in the trashcan when a DOV-Machine is present.
  ===================================================================*/

extern "C" {
void exit_fn();
};

#if defined(__STEP_1__)
int main()
{
   atexit(exit_fn);
   int i;                      
   
   cout << "step 1" << endl;

#ifdef _DEBUG
   cout << "_DEBUG defined" << endl ;
#endif // _DEBUG

   oxSetDisplayMsgProc(DisplayMsg);
    
   // System initialization
   oxClassMgr *m = (oxClassMgr *) oxInitialize();
   
   oxId rtm_id = NULLID;
   oxClass *rtm_cls = m->findClass(SX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject(NULL);
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      return 0;
   }
   
   Session *rtm = (Session *) _PRTMgr(rtm_id);

   oxId k, j;
   
   k = m->findClass("int")->createObject(rtm);
   _PINT(k)->setValue(3);
   j = oxGetClass("int")->createObject(rtm);
   _PINT(j)->setValue(-2);

   cout << "k = " << INTVAL(k) << endl <<
           "j = " << INTVAL(j) << endl ;

   _delete(k, rtm);
   _delete(j, rtm);
   
   rtm_id->deleteObject(rtm);

   m->term();

   system("PAUSE");

   return 0;
}

#elif defined(__STEP_2__)
#include "sxstring.hpp"

int main()
{
   atexit(exit_fn);

   cout << "Testing OX-Objects. Step2: object vector class.\n\n";
   
   Session *rtm;
   oxClassMgr *m = InitSxcSystem(&rtm);
   oxId session = (oxId)(ORTMgr *) rtm;
   
   oxClass *object_vector_class = oxGetClass("vector");
   if (object_vector_class == NULL)
   {
      cout << "Could not find class `vector'\n";
      system("PAUSE");
      exit(2);
   }
   
   // create object vector
   oxId v = object_vector_class->createObject(rtm);
   
   // copy from int to special int
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
   }
   
   sxArg argv[8];
   
   _ARG(0) = oxGetClass("string")->createObject(rtm);
   _ARG(1) = oxGetClass("int")->createObject(rtm);
   
   *_PStr(_ARG(0)) = "int";
   _PINT(_ARG(1))->setValue(16);
   
   v->applyMethod("type:size:", 2, argv, rtm);
   
   _PINT(_ARG(1))->setValue(3);
   oxId e = v->applyMethod("component:", 1, argv + 1, rtm);
   _PINT(e)->setValue(-9);
   e = v->applyMethod("component:", 1, argv + 1, rtm);
   e->applyMethod("println", 0, NULL, rtm);
   e = v->applyMethod("size", 0, NULL, rtm);
   e->applyMethod("println", 0, NULL, rtm);
   
   // clean-up   
   for (int k = 0; k < 8; ++k)
   {
      if (_ARG(k) != NULLID)
         _ARG(k)->deleteObject(rtm);
   }
              
   v->applyMethod("delete", 0, NULL, rtm);
   
   // terminate
   session->deleteObject(rtm);
   m->term();

   return 0;
}

#elif defined(__STEP_3__)
#include "sxstring.hpp"

char *envp[] = {"apostol", "faliagas", "gxk", NULL};

int main(int _argc, const char **_argv)
{
   atexit(exit_fn);

   cout << "Testing OX-Objects. Step3: application class.\n\n";
   
   Session *rtm;
   oxClassMgr *m = InitSxcSystem(&rtm);
   oxId session = (oxId)(ORTMgr *) rtm;
   
   oxClass *app_class = oxGetClass("OSApp");
   if (app_class == NULL)
   {
      cout << "Could not find class `OSApp'\n";
      system("PAUSE");
      exit(2);
   }
   
   // create application
   oxId a = app_class->createObject(rtm);
   
   // check for errors
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
   }
   
   struct _Args {int argc; const char **argv;} args = {
      _argc, _argv  };
   
   sxArg argv[8];
   
   _ARG(0) = oxGetClass("pointer")->createObject(rtm);
   _ARG(1) = oxGetClass("pointer")->createObject(rtm);
   
   _PPTR(_ARG(0))->setValue((void *) envp);
   _PPTR(_ARG(1))->setValue((void *) &args);
   
   a->applyMethod("env:args:", 2, argv, rtm);
   
   // check for errors
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
   }
   
   _ARG(2) = oxGetClass("int")->createObject(rtm);
   _PINT(_ARG(2))->setValue(1);
   oxId e = a->applyMethod("arg:", 1, argv + 2, rtm);
   e->applyMethod("println", 0, NULL, rtm);
   
   // check for errors
   if (rtm->exceptionPending())
   {
      oxId e = ((_Session *)(void*)rtm)->m_exception;
      cout << "exception: " << e->getClassName() << endl;
   }

/*   _PINT(_ARG(1))->setValue(3);
   oxId e = v->applyMethod("component:", 1, argv + 1, rtm);
   _PINT(e)->setValue(-9);
   e = v->applyMethod("component:", 1, argv + 1, rtm);
   e->applyMethod("println", 0, NULL, rtm);
   e = v->applyMethod("size", 0, NULL, rtm);
   e->applyMethod("println", 0, NULL, rtm);*/
   
   // clean-up   
   for (int k = 0; k < 8; ++k)
   {
      if (_ARG(k) != NULLID)
         _ARG(k)->deleteObject(rtm);
   }
              
   a->applyMethod("delete", 0, NULL, rtm);
   
   // terminate
   session->deleteObject(rtm);
   m->term();

   return 0;
}

#elif defined(__STEP_4__)

int main()
{
   atexit(exit_fn);
   int i;                      
   
   cout << "step 1" << endl;

#ifdef _DEBUG
   cout << "_DEBUG defined" << endl ;
#endif // _DEBUG

   oxSetDisplayMsgProc(DisplayMsg);
    
   // System initialization
   oxClassMgr *m = (oxClassMgr *) oxInitialize();
   
   oxId rtm_id = NULLID;
   oxClass *rtm_cls = m->findClass(OX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject(NULL);
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      return 0;
   }
   
   Session *rtm = (Session *) _PRTMgr(rtm_id);
   
   oxType<int> a(m->findClass("int"));
   oxType<int> b(m->findClass("int"));

   a.setValue(12);
   b.setValue(7);
   
   oxArg argv[3];
   argv[0].objectId = &b;

   argv[1].objectId = oxApplyMethod(&a, "add:", 1, argv, rtm);
   oxApplyMethod(argv[1].objectId, "println", 0, NULL, rtm);
   
   _delete(argv[1].objectId, rtm);
   
   rtm_id->deleteObject(rtm);

   m->term();

   system("PAUSE");

   return 0;
}

#else
#error __STEP_n__ must be defined
#endif

void exit_fn()
{
   if (oxMemMgrQuery(QMM_MEMMGR_PRESENT, NULL, NULL))
   {
      int cbUser, cbSystem;
      bool bDiags;

      oxMemMgrQuery(QMM_VALID, &bDiags, NULL);
      oxMemMgrQuery(QMM_USER_BYTES, &cbUser, NULL);
      oxMemMgrQuery(QMM_SYSTEM_BYTES, &cbSystem, NULL);

      if (bDiags)
         cout << "Diags on allocator passed" << endl;
      else
         cout << "Diags on allocator failed" << endl;

      cout << "User: " << cbUser << endl;
      cout << "Sys:  " << cbSystem << endl;
   
      oxMemMgrDump("alloc.txt");
   }
   else
      cout << "Memory manager not present" << endl;
}

oxClassMgr *InitSxcSystem(Session **rtm)
{
   oxSetDisplayMsgProc(DisplayMsg);
   
   oxClassMgr *m = (oxClassMgr *)(void *) oxInitialize();
   
   if (m == NULL)
   {
      cout << "could not initialize OS run-time system" << endl;
      exit(2);
   }
   
   oxId rtm_id = NULLID;
   oxClass *rtm_cls = m->findClass(OX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject(NULL);
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      exit(2);
   }
   
   *rtm = (Session *) _PRTMgr(rtm_id);
   return m;
}

