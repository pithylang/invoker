#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>
#include "osx.h"
#include "osutils.hpp"
#include "oscls.hpp"
#include "osclsmgr.hpp"
#include "osobject.hpp"
#include "ostype.hpp"
#include "osdev.h"
#include "osrtmgr.hpp"

/* ===================================================================
   __step_1__  osrt system initialization/termination, general object
               construction, destruction methods.
   __step_2__  osType operations
   __step_3__  osAny
   =================================================================== */

//extern IdGenerator *_IdGenerator;
extern const char OS_LINK *bsel[];

osClassMgr *InitOSSystem(CRTMgr **rtm);

#define __step_3__

void DisplayMsg(const char *title, const char *msg)
{
   printf("%s: %s\n", title, msg);
}

/*===================================================================
  Rules for osObject-derived classes
  ===================================================================
1. All object instances are created on the heap. Create instances on
   the stack only for temporary usage (within a method).
2. When the return value (RV) is immaterial (soft copied) the self
   ptr may be returned. When the RV is essential, ie this is what
   matters when the method is called, a new instance must be created,
   which encapsulates the RV and returned. Temporary RV's are
   automatically put in the trashcan when a DOV-Machine is present.
  ===================================================================*/

extern "C" {
void exit_fn();
};

#if defined(__step_1__)
int main()
{
   atexit(exit_fn);
   int i;                      
   
   cout << "step 1" << endl;

#ifdef _DEBUG
   cout << "_DEBUG defined" << endl ;
#endif // _DEBUG

   osSetDisplayMsgProc(DisplayMsg);
    
   // OS system initialization
   osClassMgr *m = osInitialize();
   
   osId rtm_id = NULLID;
   osClass *rtm_cls = m->findClass(SX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject();
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      return 0;
   }
   
   CRTMgr *rtm = (CRTMgr *) _PRTMgr(rtm_id);

   osId k, j;
   
   k = m->findClass("int")->createObject();
   _PINT(k)->setValue(3);
   j = osGetClass("int")->createObject();
   _PINT(j)->setValue(-2);

   cout << "k = " << INTVAL(k) << endl <<
           "j = " << INTVAL(j) << endl ;

   k->deleteObject();
   j->deleteObject();
   
   rtm_id->deleteObject();

   m->term();

   system("PAUSE");

   return 0;
}

#elif defined(__step_2__)
#include "osstr.hpp"

const char *selector[] = 
{
   "add:",         // 0
   "sub:",
   "mul:",
   "div:",
   "mod:",         
   
   "incBy:",       // 5
   "decBy:",
   "mulBy:",
   "divBy:",
   "modBy:",
   "getThenInc",
   "getThenDec",
   
   "bitAndBy:",    // 12
   "bitOrBy:",
   "bitXorBy:",
   "shiftRBy:",
   "shiftLBy:",
   "bitAnd:",      // 17
   "bitOr:",
   "bitXor:",
   "shiftR:",
   "shiftL:",
   "eq:",          // 22 
   "gt:",
   "lt:",
   "ge:",
   "le:",
   "ne:",

   "int",          // 28
   "short",
   "char",
   "double",
   "float",
   "bool",
   "pointer",
   "neg"           // 35
};

int main()
{
   atexit(exit_fn);
   
   CRTMgr *rtm;
   osClassMgr *m = InitOSSystem(&rtm);

   osId x, y, i, b;
   // Invoke constructor -- opcode cobj; stor x
   x = m->getClass("double")->createObject();
   _PDOUBLE(x)->setValue(1.23);
   i = m->getClass("int")->createObject();
   _PINT(i)->setValue(8);
   b = m->getClass("bool")->createObject();
   
   cout << "Testing operations..." << endl;
   system("PAUSE");
   
   // Test operations
   osId result; 
   osArg arg(i);
   int k;
   for (k = 0; k < 5; ++k)
   {                                  
      // (x op: k) println;
      x->applyMethod("copy:", 1, &arg, rtm);
      result = x->applyMethod(selector[k], 1, &arg, rtm);
      result->applyMethod("print", 0, NULL, rtm);
      cout << " : ";
      x->applyMethod("println", 0, NULL, rtm);
      result->getClass()->deleteObject(result, rtm);
   }

   cout << "Testing copy operations..." << endl;
   system("PAUSE");
   
   // Test copy operations
   for (k = 5; k < 10; ++k)
   {                                  
      // (x op: k) println;
      result = x->applyMethod(selector[k], 1, &arg, rtm);
      result->applyMethod("print", 0, NULL, rtm);
      cout << " : ";
      x->applyMethod("println", 0, NULL, rtm);
   }

   cout << "Testing post inc/dec operations..." << endl;
   system("PAUSE");
   
   // Test get operations
   for (k = 10; k < 12; ++k)
   {                                  
      // (x op) println;
      result = x->applyMethod(selector[k], 0, NULL, rtm);
      result->applyMethod("print", 0, NULL, rtm);
      cout << " : ";
      x->applyMethod("println", 0, NULL, rtm);
      osDisplayMsg("App note", result->getClass()->c_str());
      result->deleteObject();
   }              
   
   cout << "Testing bit operations..." << endl;
   system("PAUSE");
   
   // Test bit operations
   for (k = 12; k < 17; ++k)
   {                                  
      // (x op: k) println;
      result = x->applyMethod(selector[k], 1, &arg, rtm);
      result->applyMethod("print", 0, NULL, rtm);
      cout << " : ";
      x->applyMethod("println", 0, NULL, rtm);
      if (result != x)
         cout << "result != x" << endl;
   }

   cout << "Testing bit copy operations..." << endl;
   system("PAUSE");
   
   // Test bit copy operations
   for (k = 17; k < 22; ++k)
   {                                  
      // (x op: k) println;
      result = x->applyMethod(selector[k], 1, &arg, rtm);
      if (result != NULL)
      {
         result->applyMethod("print", 0, NULL, rtm);
         cout << " : ";
         x->applyMethod("println", 0, NULL, rtm);
         result->getClass()->deleteObject(result, rtm);
      }
      else
      {
         cout << "result = NULL" << endl;
      }

   }

   cout << "Testing comparison operations..." << endl;
   system("PAUSE");
   
   // Test comparison operations
   for (k = 22; k < 28; ++k)
   {                                  
      // (x op: k) println;
      result = x->applyMethod(selector[k], 1, &arg, rtm);
      result->applyMethod("print", 0, NULL, rtm);
      cout << " : ";
      x->applyMethod("println", 0, NULL, rtm);
      result->getClass()->deleteObject(result, rtm);
   }

   cout << "Testing cast operations..." << endl;
   system("PAUSE");
   
   _PDOUBLE(x)->setValue(3.45);
   // Test cast operations
   for (k = 28; k < 35; ++k)
   {                                  
      // (x op: k) println;
      cout << selector[k] << " ";
      result = x->applyMethod(selector[k], 0, NULL, rtm);
      result->applyMethod("println", 0, NULL, rtm);
      result->getClass()->deleteObject(result, rtm);
   }

   cout << "Testing neg..." << endl;
   system("PAUSE");
   
   // Test neg
   result = x->applyMethod(selector[35], 1, &arg, rtm);
   result->applyMethod("print", 0, NULL, rtm);
   cout << " : ";
   x->applyMethod("println", 0, NULL, rtm);
   result->getClass()->deleteObject(result, rtm);
      
   x->getClass()->deleteObject(x, rtm);
   i->getClass()->deleteObject(i, rtm);
   b->getClass()->deleteObject(b, rtm);

   ((ORTMgr *)rtm)->getClass()->deleteObject((ORTMgr *)rtm, NULL);
      
   m->term();

   return 0;
}

#elif defined(__step_3__)

int main()
{
   atexit(exit_fn);
   
   CRTMgr *rtm;
   osClassMgr *m = InitOSSystem(&rtm);

   osId x, y, i, b, a;
   // Invoke constructor -- opcode cobj; stor x
   x = m->getClass("double")->createObject();
   _PDOUBLE(x)->setValue(1.23);
   i = m->getClass("int")->createObject();
   _PINT(i)->setValue(8);
   a = m->getClass("Any")->createObject();
   
   // Test
   osId result; 
   osArg arg(i);
   
   a->applyMethod("copy:", 1, &arg, rtm);
   a->applyMethod("println", 0, NULL, rtm);
              
   arg.objectId = a;
   result = x->applyMethod("modBy:", 1, &arg, rtm);
   cout << "result = ";
   x->applyMethod("println", 0, NULL, rtm);
//   result->deleteObject();
   
   ((ORTMgr *)rtm)->getClass()->deleteObject((ORTMgr *)rtm, NULL);
      
   m->term();

   return 0;
}

#else
#error __step_n__ must be defined
#endif

void exit_fn()
{
   //delete _IdGenerator;
   osAllocator *a = osGetAllocator();
   if (!a->diags()) cout << "Diags on allocator failed" << endl;
   cout << "User: " << a->cbUser() << endl;
   cout << "Sys:  " << a->cbSys() << endl;
   ///////
   FILE *f = fopen("alloc.dat", "w");
   if (f == 0)
      cout << "could not open dump file\n";
   else
   {
      a->print(f);
      fclose(f);
   }
}

osClassMgr *InitOSSystem(CRTMgr **rtm)
{
   osSetDisplayMsgProc(DisplayMsg);
   
   osClassMgr *m = osInitialize();
   
   osId rtm_id = NULLID;
   osClass *rtm_cls = m->findClass(SX_RTMgrName);
   if (rtm_cls != NULL)
      rtm_id = rtm_cls->createObject();
   
   if (rtm_id == NULLID)
   {
      cout << "could not initialize run-time object" << endl;
      return 0;
   }
   
   *rtm = (CRTMgr *) _PRTMgr(rtm_id);
   return m;
}

