__src_=$1
if [ -z $1 ]; then
  __src_="t5";
fi
echo "c++ -std=c++14 -fconcepts -O0 -g -I../include -D__DEBUG__ -L. -o ${__src_} ${__src_}.cpp -lisom"
c++ -std=c++14 -fconcepts -O0 -g -I../include -D__DEBUG__ -L. -o ${__src_} ${__src_}.cpp -lisom
