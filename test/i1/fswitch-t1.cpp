#include <iostream>
#include "fswitch.h"

struct X
{
  void f(int);
  void g(int);
};

void X::f(int i)
{
  SWITCH_TABLE_CHK(0, 0, 3)
    DEFINE_CASE(0,0)
    DEFINE_CASE(0,1)
    DEFINE_CASE(0,2)
    DEFINE_CASE(0,3)
  END_SWITCH_TABLE(0)

  SWITCH_CHK (0, i)
    CASE(0, 0):
      std::cout << 0 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 1):
      std::cout << 1 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 2):
      std::cout << 2 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 3):
      std::cout << 3 << ": " << i << std::endl;
      BREAK(0);

    DEFAULT(0):
      std::cout << "default\n";
  END_SWITCH(0);
}

void X::g(int i)
{
  SWITCH_TABLE_CHK(0, 0, 3)
    DEFINE_CASE(0,0)
    DEFINE_CASE(0,1)
    DEFINE_CASE(0,2)
    DEFINE_CASE(0,3)
  END_SWITCH_TABLE(0)

  if (i < 0 || i > 3) {
    std::cout << "case " << i << "does not exist" << std::endl;
    return;
  }

  SWITCH (0, i)
    CASE(0, 0):
      std::cout << 0 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 1):
      std::cout << 1 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 2):
      std::cout << 2 << ": " << i << std::endl;
      BREAK(0);

    CASE(0, 3):
      std::cout << 3 << ": " << i << std::endl;
      BREAK(0);
  END_SWITCH(0);
}

int main() {
  X x;
  for (int i = -2; i < 5; ++i) {
    std::cout << "i = " << i << ": ";
    x.f(i);
  }
  return 0;
}