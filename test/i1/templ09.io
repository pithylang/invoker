$class: "A" definition: {
  .property: "a" type: "string";
  .method: "println" definition: {
    a println;
  };
};

$class: "MyString" definition: {
  .method: "string" definition: {
    $return: "test";
    // alternative possibility:
    //"test" dup;
  };
};

("MyString"i string) println;
s = "MyString"i;
x = "A"i{s};
x println;
