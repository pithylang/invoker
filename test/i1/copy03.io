$class: A definition: {
  .property: x type: "int";
  .method: "value:" definition: {
    x = value;
    $returnThis;
  };
};

$class: B definition: {
  .property: y type: "double";
  .method: A definition: {
    $return: ("A"i => value: 32);
//    $auto: aa type: "A" => value: 32;
//    $return: aa;
  };
};

"" println;
//$auto: a type: "A" => value: 76;
//$auto: z type: "A" => value: 61;
$auto: b type: "B";
//"before copy: z.x = " print; z.x println;
//z = a;
//"after copy : z.x = " print; z.x println;
//a = b;
//"after copy : a.x = " print; a.x println;
//a value: 61;
r = (b A);
"" println;
