(x := (($auto: int) => value: 88)) println; $check: "88";
($isObject: x) println;                     $check: "false";
(($auto: int) => value: 88) println;        $check: "88";
($auto: int) => value: 88 => println;       $check: "88";
"int"i => value: 88 => println;             $check: "88";

$expectedLeftovers: 7;

/**
 *  @brief Comment
 *  @details The statement in line 1 does not work as intended
 *  for the following reason: "$auto: int" creates a \c temporary
 *  object, which is then assigned the value 88. Then, `x` becomes
 *  a reference to the result of this operation, which is again
 *  the original object, and finally, the last part of the
 *  statement prints it. Upon completion, all temporaries used
 *  in this line statement, including that referred to by `x`,
 *  are destroyed, and the next one reports that it is no longer
 *  in the system.
 *  @par The statement on line 3 works fine as it does not use or
 *  refer to the involved object.
 *  @par More elegant and possibly more efficient alternatives
 *  appear on lines 4 and 5.
 */
