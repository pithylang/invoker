#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
// #include <forward_list>
#include "tree.hpp"

using namespace i1;

struct PopAct0 {
  using node_t = TreeNode<int>;
  int i{0}, j{0};
  std::vector<int> terminals{5,9,12,20,24,27};
  int data() noexcept {return i++;}
  bool terminal(node_t *p) const {
    if (std::find(terminals.begin(), terminals.end(), p->data()) == terminals.end())
      return false;
    return p->data() > 30;
  }
  bool more(node_t *p) noexcept {
    switch (p->data()) {
      case  0: return (++j <=  5);
      case  1: return (++j <=  8);
      case  2: return (++j <= 11);
      case  3: return (++j <= 13);
      case  4: return (++j <= 16);
      case  6: return (++j <= 19);
      case  7: return (++j <= 23);
      case  8: return (++j <= 25);
      case 10: return (++j <= 29);
    }
    return false;
  }
};

struct PopAct {
  using node_t = TreeNode<int>;

  size_t i{0};
  node_t* currentNode;
  const std::vector<std::vector<int>>& tab;

  PopAct(const std::vector<std::vector<int>>& table) : tab(table) {}

  void operator()(node_t *p) {
    currentNode = p;
    i = 0;
  }

  int data() noexcept {return tab[currentNode->data()][i++];}
  bool more() const noexcept {return i < tab[currentNode->data()].size();}
};

struct OutputNodeAction {
  std::ofstream &os;
  OutputNodeAction(std::ofstream &os) : os(os) {}
  void operator()(TreeNode<int> *p) {
    if (p->empty()) return;
    // os << "  " << "node" << p->data() << std::endl;
#if USE_DOUBLE_LIST_TREE
    for (auto q : p->branches())
#else // !USE_DOUBLE_LIST_TREE
    std::forward_list<TreeNode<int>*> v;
    for (auto e : p->branches()) v.push_front(e);
    for (auto q : v)
#endif // USE_DOUBLE_LIST_TREE
      os << "  " << "node" << p->data() << " -> node" << q->data() << std::endl;
    os << std::endl;
  }
};

int main() {
  const std::vector<std::vector<int>> tab{
    {1, 2, 3, 4},
    {5, 6},
    {14, 16},
    {8, 9},
    {10},
    {},
    {7, 11, 12, 13, 15},
    {17, 18, 19},
    {20},
    {},
    {21, 22, 23},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  };
  Tree<int> tree;
  PopAct popAct(tab);
  tree.populate(0, popAct);

  std::ofstream os1("tree-graph-1.dot");
  OutputNodeAction outputNode1(os1);
  os1 << "digraph {\n";
  tree.walkDown(outputNode1);
  os1 << "}\n";

  tree.relink(19, 5);
  tree.relink(5, 0);
  tree.relink(4, 19);

  std::ofstream os("tree-graph.dot");
  OutputNodeAction outputNode(os);
  os << "digraph {\n";
  tree.walkDown(outputNode);
  os << "}\n";

  int i = 0;
  auto f = [&i](TreeNode<int>*) {++i;};
  tree.walkDown(f);
  std::cout << "number of nodes: " << i << std::endl;
  tree.clear();
  i = 0;
  tree.walkDown(f);
  std::cout << "number of nodes: " << i << std::endl;

  return 0;
}
