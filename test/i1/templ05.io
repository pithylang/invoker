$class: "A" definition: {
  .property: "a";
  .method: "println" definition: {
    a println;
  };
};

"int"i {23} println;
k = ("int"i value: 23);
"k = "print; k println;
(k < 7) println;
x = "A"i{<k < 7>};
//x.a println;
x println;
("A<int>"C valid) println;
("A<ulong>"C valid) println;
("A<bool>"C valid) println;
