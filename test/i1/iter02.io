/**
 *  @brief Random access iterator for vectors
 *  @details This class is an example of a random access iterator
 *  for vectors completely implemented in script.
 */
$class: "vectorIterator" extends: "random access iterator" definition: {
  .protected: "count_" type: "long";
  .ref: "vector";
  .method: "init: v" definition: {
    vector := v;
    $returnRef: this;
  };
  .method: "value" definition: {
    $returnRef: vector[count_];
  };
  .method: "incBy: r" definition: {
    count_ += r;
    $returnRef: this;
  };
  .method: "decBy: r" definition: {
    count_ -= r;
    $returnRef: this;
  };
  .method: "inc" definition: {
    ++count_;
    $returnRef: this;
  };
  .method: "dec" definition: {
    --count_;
    $returnRef: this;
  };
  .method: "sub: rhs" definition: {
    $if: (rhs typeof: "random access iterator") then: {
      $return: count_ - rhs.count_;
    };
    count_ -= rhs;
    $returnRef: this;
  };
};

v = [1, "a string", 2.3];
$auto: end type: "vectorIterator" => init: v => incBy: 3;

$for: {$auto: it type: "vectorIterator"; it init: v;} while: {it != end;}
                                                      step: {++it;} do: {
  (it value) println;
};
