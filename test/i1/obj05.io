s = "hello world!";
o = {};
o member: "text" value: s
  member: "print" value: {
    text println;
  }
  member: "print: a val: v" value: {
    text print; " - " print; a print; " - " print; v println;
  };

o print;
o print: "a test" val: 32;
x = o;
x.text = "test string";
x print;
