# Modify paths as needed

dir="$(pwd)"
rootDir="${dir%'/test'}"
if [ ! "${rootDir}/test" == "${dir}" ]; then
  echo "you must run this script from the test subdirectory"
  return
fi

c++ -std=c++17 -fPIC -fconcepts -O0 -g -c -D__DEBUG__ -DPKG_EXPORTING \
    -I../../pithy/include -I. testCorePack.cpp
if [ "$?" == "0" ]; then
  c++ -shared -fPIC -Wl,-soname,libtestCorePack.so \
      -Wl,-rpath,${rootDir}/lib -L../lib \
      -o libtestCorePack.so testCorePack.o -lisom -lc
  if [ "$?" == "0" ]; then
    mv -v libtestCorePack.so ../lib
  fi
fi

