#include <iostream>
#include <forward_list>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::forward_list<T>& l) {
  for (const auto& x : l) std::cout << x << " ";
  return os;
}

template<typename T>
size_t sizeOf(const std::forward_list<T>& l) {
  size_t size = 0;
  for (const auto& x : l) ++size;
  return size;
}

template<typename T>
const T& at(const std::forward_list<T>& l, size_t i) {
  size_t skip = sizeOf(l) - i - 1;
  auto it = l.begin();
  for (size_t n = 0; n < skip; ++n) ++it;
  return *it;
}

using namespace std;

int main()
{
  cout<<"Hello World\n"s;

  forward_list<int> list;
  list.push_front(3);
  list.push_front(-2);
  list.push_front(8);
  list.push_front(13);

  cout << sizeOf(list) << endl;
  cout << list << endl;
  // auto it = list.begin();
  // it = ++it;
  // cout << *++it << endl;
  for (int i = 0; i < (int) sizeOf(list); ++i)
    cout << at(list, i) << " ";
  cout << endl;
  try {
    cout << "1\n";
    throw "skata"s;
    cout << "2\n";
  }
  catch (const string& msg) {
    cout << msg << endl;
    return 1;
  }
  auto str = "skata"s;

  return 0;
}
