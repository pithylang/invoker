$class: "A" definition: {
  .property: "a" type: "int";
};

$class: "B" extends: "A" definition: {
  .property: "t";
};

("A"C template) println;
("B"C template) println;
