$class: "A" definition: {
  .property: "a" type: "int";
};

$class: "B" extends: "A" definition: {
  .property: "t";
};

x = "A"i{ 12 };
"x.a = "print; x.a println;
y = "B"i { < 15 > };
"y.a = " print; y.a println;
"y.t = " print; y.t println;
// throws runtime error excessive template data
//z = "B"i {<21, 76>};
z = "B"i { 21, 76 };
"z.a = " print; z.a println;
"z.t = " print; z.t println;

w = "B"i {529};
"w.a = " print; w.a println;
"w.t = " print; w.t println;
