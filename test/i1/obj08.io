s = "hello world!";
o = {
  text: s
  print: {
    text println;
  }
  "print: a with: b": {
    text print;
    " - " print; a print;
    " - " print; b println;
  }
  "z": 23
  "test:": {
    test print; " -- " print;
    "OK" println;
  }
};

o print;
o print: "aaa" with: "bbb";
o test: 2;
o.z println;
x = o;
x.text = "test string";
x print;
x print: "1" with: "2";
