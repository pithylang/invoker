n = 2;
$ switch: n + 1
  case: 1 do: {(1 + n) println;}
  case: 2 do: {(2 + n) println;}
  case: 3 do: {(3 + n) println;}
  case: 4 do: {(4 + n) println;}
  case: 5 do: {(5 + n) println;}
  case: 6 do: {(6 + n) println;}
  case: 7 do: {(7 + n) println;}
  case: 8 do: {(8 + n) println;}
;
"finished!\n" print;

