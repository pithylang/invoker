$class: "A" definition: {
  .property: "a";
  .method: "println" definition: {
    a println;
  };
};

k = "long"i {23};
"A"i{<k++>} println;
k println;
