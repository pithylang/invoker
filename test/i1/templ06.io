$class: "A" definition: {
  .property: "a";
  .method: "println" definition: {
    a println;
  };
};

k = ("ulong"i value: 23);
b = "A"i{<k < 7>};
"b = " print; b println;
i = "A"i{<12>};
"i = " print; i println;
u = "A"i{<k>};
"u = " print; u println;
o = "A"i{<"object"i>};
"o = " print; o println;
