#include <fstream>
#include "iparseTools.hpp"

char *readScript(const std::string& fileName);

int main(int argc, const char *argv[]) {
  const char *theScript = argc > 1 ? readScript(argv[1]) :
R"(// Lex test 1
x = 2; /*
/*    787 y*/

*/ x ameth: 1 arg: y other: z;
$ exit;
)";

  if (!theScript) {
    std::cout << "could not read file " << argv[1] << std::endl;
    return 2;
  }

  IParser parser(theScript);

  std::cout << theScript << std::endl;
  while (true) {
    yy::parser::symbol_type symbol(yylex(parser));
    if (symbol.kind() == yy::parser::symbol_kind_type::S_YYEOF) break;
    std::cout << symbol.name();
    if (symbol.kind() == yy::parser::symbol_kind_type::S_ID ||
        symbol.kind() == yy::parser::symbol_kind_type::S_SELID) {
      std::cout << " (" << symbol.value.as<ISymbol>().name << " @ "
                << symbol.location.begin.line << ":"
                << symbol.location.begin.column << "-"
                << symbol.location.end.column
                << ")";
    }
    std::cout << std::endl;
  }

  return 0;
}

char *readScript(const std::string& fileName)
{
  std::ifstream is(fileName, std::ifstream::binary);
  if (!is) return nullptr;
  // get length of file:
  is.seekg(0, is.end);
  long length = is.tellg();
  is.seekg(0, is.beg);
  // get space to store data
  char *fileContent = new char[length+1];
  // read data as a block
  is.read(fileContent,length);
  if (!is)
  {
    delete[] fileContent;
    fileContent = nullptr;
  }
  else if (fileContent[length - 1] != 0)
  {
    std::cout << "null terminating...\n";
    fileContent[length] = 0;
  }
  is.close();
  return fileContent;
}
