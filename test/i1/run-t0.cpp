#include <fstream>
#include "ortmgr.hpp"
#include "itypes.hpp"
#include "iparser.hpp"
#include "icodegen.hpp"
#include "iparseTools.hpp"
#include "mtypes.hpp"
#include "lvector.hpp"
#include "vm.hpp"
#include "oapi.h"

using std::string;

char* readScript(const string& fileName);

int main(int argc, const char* argv[]) {
  const string fileName(argc > 1 ? argv[1] : "");
  const char* theScript = argc > 1 ? readScript(fileName) :
R"(// Lex test 1
x = 2; /*
/*    787 y*/

*/ x ameth: 1 arg: y other: z;;
$ exit;
)";

  if (!theScript) {
    std::cout << "could not read file " << argv[1] << std::endl;
    return 2;
  }

  IParser parseServices(theScript);
  parseServices.location.initialize(&fileName);

  std::cout << theScript << std::endl;

  yy::parser parser(parseServices);
  i1::vector<MBlock> blockTable;
  ICodeGenerator codeGenerator(parseServices, blockTable);
  int rc;

  try {
    rc = parser.parse();
    if (rc != 0) throw "parser failed with exit code "+std::to_string(rc);
    IBlock program = std::move(parseServices.parsedProgram);
    MOperand result = codeGenerator.generateCode(program, false/*immaterial*/);
    if (result != 0) {
      std::cout << "code generator returned " << result << std::endl;
      return 2;
    }

    if (argc > 1) {
      const auto extensionPos = fileName.rfind(".io");
      string outputFile;
      if (extensionPos == string::npos) outputFile = fileName + ".aio";
      else outputFile = fileName.substr(0, extensionPos) + ".aio";
      std::ofstream os(outputFile);
      os << codeGenerator.disassemble();
    }
    else {
      std::cout << "\ndisassembly:\n\n" << codeGenerator.disassemble()
                << std::endl;
    }
  }
  catch (string& msg) {
    if (argc > 1) delete[] theScript;
    std::cout << msg << std::endl;
    return rc;
  }
  /*catch (const IException& e)
  {
    std::cout << e.title << ": " << e.descr << std::endl;
    return 1;
  }*/

  // initialize runtime manager (system) object
  Session* m;
  std::cout << "program output:" << std::endl;
  try {
    m = sdoInitialize();
  }
  catch (string& msg) {
    if (argc > 1) delete[] theScript;
    std::cout << msg << std::endl;
    return rc;
  }
  if (m == nullptr) {
    std::cout << "could not initialize run-time object" << std::endl;
    return 1;
  }
  m->flags |= RTFL_KEEP_OBJECT_REGISTER;
  m->stringOutput = [](const string& msg) {std::cout << msg;};
  m->setExecLoopProc([m](const std::vector<int>& args, bool whileLoop,
      bool beginCond) {
        m->getCurrentMachine()->execLoop(args, whileLoop, beginCond);
      });
  m->setExecIfProc([m](OArg ifCondBlock, OArg thenBlock,
      const std::vector<OArg>& elseIfBlocks, OArg elseBlock) {
        return m->getCurrentMachine()->execIf(ifCondBlock, thenBlock,
            elseIfBlocks, elseBlock);
      });
  m->setExecSwitchProc([m](OArg switchVar, const std::vector<OArg>& vals,
      const std::vector<OArg>& blocks, OArg defBlock) {
        m->getCurrentMachine()->execSwitch(switchVar, vals, blocks, defBlock);
      });
  m->setControlProc([m](int n) {m->execFlowControl(n);});
  m->setExecClassDefProc([m](OClass* c, int classDefBlockId) {
        return m->getCurrentMachine()->execDefineClass(c, classDefBlockId);
      });
  m->setMethodArgNamesProc([m](int blockId, std::vector<string>& argNames) {
        m->getMainMachine()->setMethodArgNames(blockId, argNames);
      });
  m->setExecMethodProc([m](OClass* baseClass, const string& method, ulong flags,
      ORef rec, int argc, OArg* argv) {
        return m->getCurrentMachine()->execMethod(baseClass, method, flags,
                                                  rec, argc, argv);
      });
  m->setExecScriptProc([m](const string& method, int blockId, ulong flags,
      ORef rec, int argc, OArg* argv) {
        VirtualMachine vm(*m->getCurrentMachine());
        vm.init(blockId);
        return vm.execScriptMethod(method, blockId, flags, rec, argc, argv);
      });
  // Run program
  MVIASArgTable tableVIAASA{
    // {"class:definition:", {0}},
    // {"class:extends:definition:", {0, 1}},
    {"getClass:",  {0}},
    {"auto:",      {0}},
    {"const:",     {0}},
    {"private:",   {0}},
    {"property:",  {0}},
    {"protected:", {0}},
    {"public:",    {0}},
    {"private:type:",   {0,1}},
    {"property:type:",  {0,1}},
    {"protected:type:", {0,1}},
    {"public:type:",    {0,1}},
    {"reference:",           {0}},
    {"ref:",                 {0}},
    {"reference:attribute:", {0}},
    {"ref:attr:",            {0}},
    {"protectedReference:",  {0}},
    {"privateReference:",    {0}},
    {"constThis:definition:", {0}},
    {"method:definition:",    {0}},
    {"private:definition:",   {0}},
    {"protected:definition:", {0}},
    {"public:definition:",    {0}},
    {"method:attribute:definition:", {0}},
  };
  VirtualMachine vm(m, &blockTable, tableVIAASA);
  if (!m->exceptionPending())
    vm.runStdMode();

  if (argc > 1) delete[] theScript;

  if (m->exceptionPending()) {
    vm.defExceptionHandler(0);
    return 99;
  }

  const int exitCode = vm.defExitHandler();

  const auto undelObjects = m->getInfoRegisteredObjects();
  std::cout << "there are " << undelObjects.size()
            << " undeleted objects" << std::endl;
//   for (const auto& info : undelObjects)
    // std::cout << info << std::endl;

  return exitCode;
}

char* readScript(const string& fileName) {
  std::ifstream is(fileName, std::ifstream::binary);
  if (!is) return nullptr;
  // get length of file:
//   is.seekg(0, is.end);
  is.seekg(0, std::ios::end);
  long length = is.tellg();
//   is.seekg(0, is.beg);
  is.seekg(0, std::ios::beg);
  // get space to store data
  char* fileContent = new char[length+1];
  // read data as a block
  is.read(fileContent, length);
  if (!is) {
    delete[] fileContent;
    fileContent = nullptr;
  }
  else if (fileContent[length - 1] != 0) {
    std::cout << "null terminating...\n";
    fileContent[length] = 0;
  }
  is.close();
  return fileContent;
}
