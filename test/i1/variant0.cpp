#include <iostream>
#include <mapbox/variant.hpp>

namespace mapbox {
  using util::variant;
  using util::get;
}

int main() {
  mapbox::variant<std::string, int, double> var;
  var = "skata";
//  std::cout << var.get<std::string>() << std::endl;
  std::cout << mapbox::get<std::string>(var) << std::endl;

  var = 3.14;
  auto result =
  var.match([](std::string s) {return s;},
            [](int i) {return std::to_string(i);},
            [](double d) {return std::to_string(d);}
  );
  std::cout << result << std::endl;

  return 0;
}
