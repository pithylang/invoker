#include <iostream>
#include "lvector.hpp"

int main() {
  i1::vector<std::string> v;
  v.push_back(std::string("skata"));
  v.emplace_back("ole");
  v.emplace_back(1, '@');
  std::string s("skatoules");
  v.emplace_back(s.begin() + 4, s.end());
  for (size_t i = 0; i < v.size(); ++i) std::cout << v[i] << " ";
  std::cout << std::endl;
  return 0;
}
