#include <fstream>
#include <memory>
#include <charconv>
#include <filesystem>
#include "util.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "iparser.hpp"
#include "icodegen.hpp"
#include "iparseTools.hpp"
#include "vm.hpp"
#include "oapi.h"
#include "clparser.hpp"

std::unique_ptr<char[]> g_readScript(const std::string& fileName);
std::string replaceExtension(const std::string& fileName,
    const char* newExt, const char* defExt = nullptr);
std::vector<std::string> tokenize(const std::string&s, char sep);
std::unordered_map<std::string, ObjectUsage> getObjectCacheConfig(
    const std::string& configFile);

using std::string;
using std::vector;
namespace fs = std::filesystem;

const char *argp_program_version = "i1 1.0";
const char *argp_program_bug_address = "<apostol.faliagas@gmail.com>";

int main(int argc, const char* argv[]) {
  bool testMode = false;
  bool verbose = false;
  bool moreVerbose = false;
  bool generateAssembly = false;
  bool collectUsageData = false;
  bool noInlineConditionals = false;
  ulong parseOptions = PO_DEFAULT;
  string fileName;
  string parseExceptionToIgnore;
  string cacheConfigFile;
  vector<string> pgmArgs;
  std::unordered_map<std::string, ObjectUsage> objectCacheConfigData;

  // parse command line arguments
  try {
    CmdLineArgs cl(argc, const_cast<char**>(argv));
    cl.set_doc("i1 1.0 - Invoker Script (IS) Executor");
    cl.set_args_doc("SCRIPT [its_options]");
    cl.option("testing", "testing mode", &testMode, 't');
    cl.option_uses_no_arg();
    cl.option("verbose", "print script and extra information", &verbose, 'v');
    cl.option_uses_no_arg();
    cl.option("more-verbose", "print additional information", &moreVerbose, 'V');
    cl.option_uses_no_arg();
    cl.option("assembly", "generate virtual machine assembly",
        &generateAssembly, 'a');
    cl.option_uses_no_arg();
    cl.option("usage-stat", "collect object usage data",
        &collectUsageData, 'u');
    cl.option_uses_no_arg();
    cl.option("ignore-parse-exception", "ignore parse exception",
        &parseExceptionToIgnore);
    cl.option("no-inline-conditionals", "no inline conditionals",
        &noInlineConditionals, 'C');
    cl.option_uses_no_arg();
    cl.option("cache-config", "configure object caches from usage data file"
        "$[FILE|default|d]", &cacheConfigFile);
    cl.set_on_arg([&pgmArgs](argp_state* state, char* arg, int* rc) -> bool {
      ASSERT(state->arg_num == 0);
      for (size_t i = state->next; i < state->argc; ++i)
        pgmArgs.emplace_back(string{state->argv[i]});
      state->next = state->argc;
      *rc = 0;
      return /* handled? */ false; // it passes control to default handler
    });
    cl.parse_cmd_line(ARGP_IN_ORDER);
    if (cl.sources().size() != 1) {
      cl.display_usage();
      return 4;
    }
    fileName = std::move(cl.sources()[0]);
    if (testMode) {
    //   verbose = moreVerbose = collectUsageData = generateAssembly = false;
      verbose = moreVerbose = false;
      parseOptions |= PO_TESTING_MODE;
    }
    else if (moreVerbose) {
      verbose = true;
    }
    if (noInlineConditionals)
      parseOptions &= ~PO_INLINE_CONDITIONAL_OPERATOR;
    if (!cacheConfigFile.empty()) {
      collectUsageData = false;
      bool defaultCacheConfigFile = false;
      if (cacheConfigFile == "default" || cacheConfigFile == "d") {
        cacheConfigFile = replaceExtension(fileName, ".ous", ".is");
        defaultCacheConfigFile = true;
      }
      if (!fs::exists(cacheConfigFile)) {
        const string d{defaultCacheConfigFile ? "default " : ""};
        std::cout << "error: the " << d << "configuration file "
                  << cacheConfigFile << " does not exist" << std::endl;
        return 8;
      }
      try {
        objectCacheConfigData = getObjectCacheConfig(cacheConfigFile);
      }
      catch(const std::logic_error& except) {
        std::cout << "error: " << except.what() << std::endl;
        return 9;
      }
    }
  }
  catch (const string& errorMsg) {
    std::cout << "error: " << errorMsg << std::endl;
    return 5;
  }

  // read script
  auto theScript{g_readScript(fileName)};

  if (!theScript) {
    std::cout << "could not read file " << fileName << std::endl;
    return 3;
  }

  IParser parseServices(theScript.get(), parseOptions);
  parseServices.location.initialize(&fileName);

  if (verbose)
    std::cout << theScript.get() << std::endl;

  yy::parser parser(parseServices);
  i1::vector<MBlock> blockTable;
  ICodeGenerator codeGenerator(parseServices, blockTable);
  int rc;

  try {
    rc = parser.parse();
    if (rc != 0) throw "parser failed with exit code "+std::to_string(rc);
    IBlock program = std::move(parseServices.parsedProgram);
    MOperand result = codeGenerator.generateCode(program, false/*immaterial*/);
    if (result != 0) {
      std::cout << "code generator returned " << result << std::endl;
      return 6;
    }

    if (generateAssembly)
      std::ofstream{replaceExtension(fileName, ".isa", ".is")}
          << codeGenerator.disassemble();
  }
  catch (const string& msg) {
    if (testMode && msg == parseExceptionToIgnore)
      return 0;
    if (verbose)
      std::cout << msg << std::endl;
    // the parser returns 0 or 1
    rc = 2;
    return rc;
  }

  // initialize runtime manager (system) object
  Session* m;
  if (verbose)
    std::cout << "program output:" << std::endl;
  try {
    m = omInitialize();
  }
  catch (string& msg) {
    std::cout << msg << std::endl;
    return rc;
  }
  if (m == nullptr) {
    std::cout << "could not initialize run-time object" << std::endl;
    return 7;
  }
  m->flags |= RTFL_KEEP_OBJECT_REGISTER;
  m->parseOptions = parseOptions;
  if (testMode) {
    m->setTestingMode();
    m->stringOutput = [m](const string& msg) {m->checkpointText(msg);};
    m->setParseExceptionToIgnore(parseExceptionToIgnore);
  }
  else {
    m->stringOutput = [](const string& msg) {std::cout << msg;};
  }
  m->setGenerateAssembly(generateAssembly);
  m->setVerboseOutput(verbose);
  m->setCollectUsageData(collectUsageData);
  if (collectUsageData)
    m->collectUsageData();
  if (!objectCacheConfigData.empty())
    try {
      m->configObjectCaches(std::move(objectCacheConfigData));
    }
    catch (const std::logic_error& e) {
      std::cout << e.what() << std::endl;
      return 10;
    }
  m->setExecLoopProc([m](const std::vector<int>& args, bool whileLoop,
      bool beginCond) {
        m->getCurrentMachine()->execLoop(args, whileLoop, beginCond);
      });
  m->setExecIfProc([m](OArg ifCondBlock, OArg thenBlock,
      const std::vector<OArg>& elseIfBlocks, OArg elseBlock) {
        return m->getCurrentMachine()->execIf(ifCondBlock, thenBlock,
            elseIfBlocks, elseBlock);
      });
  m->setExecSwitchProc([m](OArg switchVar, const std::vector<OArg>& vals,
      const std::vector<OArg>& blocks, OArg defBlock) {
        m->getCurrentMachine()->execSwitch(switchVar, vals, blocks, defBlock);
      });
  m->setControlProc([m](int n) {m->getCurrentMachine()->execFlowControl(n);});
  m->setExecClassDefProc([m](OClass* c, int classDefBlockId) {
        return m->getCurrentMachine()->execDefineClass(c, classDefBlockId);
      });
  m->setMethodArgNamesProc([m](int blockId, std::vector<string>& argNames) {
        m->getMainMachine()->setMethodArgNames(blockId, argNames);
      });
  m->setExecMethodProc([m](OClass* baseClass, const string& method, ulong flags,
      OArg rec, int argc, OArg* argv) {
        return m->getCurrentMachine()->execMethod(baseClass, method, flags,
                                                  rec, argc, argv);
      });
  m->setExecScriptProc([m](const string& method, int blockId, ulong flags,
      OArg rec, int argc, OArg* argv) {
        VirtualMachine vm(*m->getCurrentMachine());
        vm.init(blockId);
        return vm.execScriptMethod(method, blockId, flags, rec, argc, argv);
      });
  m->setExecBlockProc([m](int blockId) {
        auto& currentMachine = *m->getCurrentMachine();
        VirtualMachine vm{currentMachine};
        vm.init(blockId);
        vm.execBlock(blockId);
        // transfer flags to parent machine
        if (vm.returning()) {
          currentMachine.enterReturnCondition();
          currentMachine.RES = vm.RES; // TODO ?
        }
        return vm.RES;
      });
  // Run program
  MVIASArgTable tableVIAASA{
    // {"class:definition:", {0}},
    // {"class:extends:definition:", {0, 1}},
    {"getClass:",  {0}},
    {"auto:",      {0}},
    {"const:",     {0}},
    {"noauto:",    {0}},
    {"private:",   {0}},
    {"prop:",      {0}},
    {"property:",  {0}},
    {"protected:", {0}},
    {"public:",    {0}},
    {"private:type:",   {0,1}},
    {"property:type:",  {0,1}},
    {"prop:type:",      {0,1}},
    {"protected:type:", {0,1}},
    {"prop:type:attr:", {0,1}},
    {"property:type:attribute:", {0,1}},
    {"public:type:",    {0,1}},
    {"reference:",           {0}},
    {"ref:",                 {0}},
    {"reference:attribute:", {0}},
    {"ref:attr:",            {0}},
    {"protectedReference:",  {0}},
    {"privateReference:",    {0}},
    {"constThis:definition:", {0}},
    {"method:definition:",    {0}},
    {"private:definition:",   {0}},
    {"protected:definition:", {0}},
    {"public:definition:",    {0}},
    {"method:def:",    {0}},
    {"private:def:",   {0}},
    {"protected:def:", {0}},
    {"public:def:",    {0}},
    {"method:attribute:definition:", {0}},
    {"method:attr:def:", {0}},
  };
  VirtualMachine vm(m, &blockTable, tableVIAASA);
  if (!m->exceptionPending())
    vm.runStdMode();

  theScript.reset(nullptr);

  if (m->exceptionPending()) {
    vm.defExceptionHandler(0);
    return m->testingMode() ? 0 : VirtualMachine::DEFAULT_ERROR_EXIT_CODE;
  }
  else if (m->testingMode() && m->expectExceptions()) {
    return VirtualMachine::EXCEPTION_TEST_FAILED;
  }

  const int exitCode = vm.defExitHandler();

  const auto leftoverObjects = m->getInfoRegisteredObjects();
  if (m->testingMode()) {
    if (leftoverObjects.size() > m->expectedLeftovers())
      exit(VirtualMachine::LEFTOVERS_TEST_FAILED);
  }
  else if (verbose) {
    std::cout << "there are " << leftoverObjects.size()
              << " undeleted objects" << std::endl;
    if (moreVerbose)
      for (const auto& info : leftoverObjects) {
        const auto pos = info.find(' ');
        ulong ul;
        std::from_chars(info.data() + 2, info.data() + pos, ul, 16);
        ORef o = reinterpret_cast<ORef>(ul);
        std::cout << info;
        switch (o->getRootClassId()) {
          case CLSID_STRING: std::cout << " -> '" << *_PString(o) << "'";
            break;
          case CLSID_ULONG: std::cout << " -> " << _P(ulong, o)->getValue();
            break;
          case CLSID_UINT: std::cout << " -> " << _P(uint, o)->getValue();
            break;
          case CLSID_INT: std::cout << " -> " << _P(int, o)->getValue();
            break;
        }
        std::cout << std::endl;
      }
  }

  if (collectUsageData)
    std::ofstream{replaceExtension(fileName, ".ous", ".is")}
        << m->getObjectUsageInfo();

  omTerminate(m, true);

  return exitCode > 255 ? 255 : exitCode;
}

//::::::::::::::::::::::::::   Utility Functions   ::::::::::::::::::::::::::://

string replaceExtension(const string& fileName,
    const char* newExt, const char* defExt) {
  const auto extensionPos = defExt ? fileName.rfind(defExt) : string::npos;
  return extensionPos == string::npos ?
      fileName + newExt : fileName.substr(0, extensionPos) + newExt;
}

std::unique_ptr<char[]> g_readScript(const string& fileName) {
  std::ifstream is(fileName, std::ifstream::binary);
  if (!is) return std::unique_ptr<char[]>{};
  // get length of file:
  is.seekg(0, std::ios::end);
  long length = is.tellg();
  is.seekg(0, std::ios::beg);
  // get space to store data
  std::unique_ptr<char[]> fileContent{new char[length+1]};
  // read data as a block
  is.read(fileContent.get(), length);
  if (!is)
    return std::unique_ptr<char[]>{};
  fileContent[length] = 0;
  return fileContent;
}

/**
 *  @brief Tokenize string
 *  @details Characters enclosed in single quotation marks
 *  are considered tokens even if they contain separator
 *  characters. Consequtive separator characters are merged;
 *  thus, the return vector contains no empty tokens. A
 *  return vector of one empty string denotes a quotation
 *  error.
 */
std::vector<std::string> tokenize(const std::string&s, char sep) {
  std::vector<std::string> tokens;
  bool quoted = false;
  size_t begin = 0;
  size_t i = 0;
  while (true) {
    if (i == s.size()) {
      if (quoted)
        tokens = std::vector<std::string>{""};
      else if (i != begin)
        tokens.emplace_back(s.substr(begin, i - begin));
      break;
    }
    else if (s[i] == '\'') {
      if (quoted) {
        quoted = false;
        tokens.emplace_back(s.substr(begin, i - begin));
        begin = ++i;
      }
      else {
        quoted = true;
        begin = ++i;
      }
    }
    else if (s[i] == sep) {
      if (quoted)
        ++i;
      else if (i == begin)
        begin = ++i;
      else {
        tokens.emplace_back(s.substr(begin, i - begin));
        begin = ++i;
      }
    }
    else
      ++i;
  }
  return tokens;
}

/**
 *  @brief Get configuration data for object caches
 *  @details It throws a `std::logic_error` if an error occurs.
 *  @param configFile `std::string` Configuration data file
 *  @return `std::unordered_map<string, ObjectUsage>`
 *      map of object type name and object usage
 */
std::unordered_map<string, ObjectUsage> getObjectCacheConfig(
    const string& configFile) {
  using std::to_string;
  std::ifstream is{configFile};
  static const string b{"bad "}, cfg{" cache config file"};
  std::unordered_map<string, ObjectUsage> typeUsage;
  size_t k = 1;
  for (string s; std::getline(is, s); ++k) {
    auto line = tokenize(s, ' ');
    if (line.size() != 3) {
      const string msg{line.size() == 1 && line[0].empty() ?
        b+"quotation in"+cfg :
        b+"format of"+cfg+": line "+to_string(k)+": expected 3 items, got "
                                   +to_string(line.size())
      };
      throw std::logic_error{msg};
    }
    ObjectUsage objectUsage;
    for (int i : {1, 2}) {
      size_t& r = i == 1 ? objectUsage.limitup : objectUsage.count;
      const char* begin = line[i].data();
      const char* end = begin + line[i].size();
      auto [p, _] = std::from_chars(begin, end, r);
      if (p != end) {
        string msg = b+"format of"+cfg+": line "+to_string(k)+": not a number";
        throw std::logic_error{msg};
      }
    }
    typeUsage[line[0]] = std::move(objectUsage);
  }
  return typeUsage;
}
