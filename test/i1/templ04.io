$class: "A" definition: {
  .property: "a";
};

k = "ulong"i{12};
$auto: i type: "ulong" => value: 12;
i println;
x = "A"i{k};
x.a println;
("A<int>"C valid) println;
("A<ulong>"C valid) println;
