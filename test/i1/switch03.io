n = 2;
i = 0;
$ switch: n
  case: 1 do: {n println;}
  case: 2 do: {
    $if: i == 0 then: {
      "this will execute a break" println;
      $break;
    };
    n println;
  }
  case: 3 do: {n println;}
;
"finished!\n" print;

