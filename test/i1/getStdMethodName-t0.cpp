#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template<typename t>
ostream& operator<<(ostream& os, const std::vector<t>& v) {
  for (const auto& item : v) os << "'" << item << "' ";
  return os;
}

vector<string> split(const string& str, const string& delim)
{
  constexpr auto EOS = string::npos;
  vector<string> result;
  if (delim.empty()) return result;
  std::string::size_type start = 0, end = 0;
  while (end != string::npos)
  {
    end = str.find(delim, start);
    result.push_back(str.substr(start, end == EOS ? EOS : end - start));
    start = end > (EOS - delim.size()) ? EOS : end + delim.size();
  }
  return result;
}

string getStdMethodName(const string& methodName)
{
  vector<string> keys = split(methodName, ":");
  if (keys.empty())
    throw "'"+methodName+"' is not a proper method name";
  if (!keys.back().empty())
    throw "'"+methodName+"' is not a proper method name";
  keys.pop_back();
  auto firstOccurence = std::adjacent_find(keys.begin(), keys.end());
  if (firstOccurence == keys.end()) return methodName;
  // auto last = firstOccurence + 1;
  // while (last + 1 != keys.end() && *last == *(last + 1)) ++last;
  // if (last < keys.end() - 2) return methodName;
  auto last = firstOccurence + 2;
  while (last != keys.end() && *last == *firstOccurence) ++last;
  if (last < keys.end() - 1) return methodName;
  --last;
  auto back = std::move(keys.back());
  keys.pop_back();
  while (last-- != firstOccurence) keys.pop_back();
  keys.push_back(std::move(back));
  string result;
  std::for_each(keys.begin(), keys.end(), [&result](const string item) {
    result += item + ":";
  });
  return result;
}

int main()
{
  const string name = "1:3:3:x:";
  auto keys = split(name, ":");
  keys.pop_back();
  cout << keys << endl;
  cout << "std name: " << getStdMethodName(name) << endl;

  return 0;
}

