n = 2;
i = 0;
$for: {k = 0;} while: {k < 3;} step: {++k;} do: {
  $switch: n
    case: 1 do: {n println;}
    case: 2 do: {
      $if: i == 0 && k == 1 then: {
        "this will execute a break" println;
//        $break: 2;
        $break;
      };
      n println;
    }
    case: 3 do: {n println;}
  ;
  "just got out of switch" println;
};
"finished!\n" print;
