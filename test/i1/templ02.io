$class: "A" definition: {
  .property: "a" type: "int";
};

$class: "B" extends: "A" definition: {
  .property: "t";
};

x = "A"i {12};
x.a println;
y = "B"i {
  <15>
};
y.a println;
y.t println;
