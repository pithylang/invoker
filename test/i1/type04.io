$auto: ptrValue type: "pointer";
$auto: types type: "vector";
types pushBack: "ulong";
types pushBack: "long";
types pushBack: "unsigned";
types pushBack: "int";
types pushBack: "short";
types pushBack: "char";
types pushBack: "bool";
types pushBack: "double";
types pushBack: "float";
types pushBack: "pointer";
"size = " print; (types size) println;
$auto: values type: "vector";
values pushBack: ULONG_VAL;
values pushBack: LONG_VAL;
values pushBack: UNSIGNED_VAL;
values pushBack: INT_VAL;
values pushBack: SHORT_VAL;
values pushBack: CHAR_VAL;
values pushBack: BOOL_VAL;
values pushBack: DOUBLE_VAL;
values pushBack: FLOAT_VAL;
values pushBack: ptrValue;

$class: Xulong    extends: ulong    definition: { . public: msg type: string; };
$class: Xlong     extends: long     definition: { . public: msg type: string; };
$class: Xunsigned extends: unsigned definition: { . public: msg type: string; };
$class: Xint      extends: int      definition: { . public: msg type: string; };
$class: Xshort    extends: short    definition: { . public: msg type: string; };
$class: Xchar     extends: char     definition: { . public: msg type: string; };
$class: Xbool     extends: bool     definition: { . public: msg type: string; };
$class: Xdouble   extends: double   definition: { . public: msg type: string; };
$class: Xfloat    extends: float    definition: { . public: msg type: string; };
$class: Xpointer  extends: pointer  definition: { . public: msg type: string; };

// type - extension
$for: {i = 0;} while: {i < (types size);} step: {++i;} do: {
  $auto: lhs type: types[i];
  lhs value: values[i];
  types[i] print; ":" println;
  $for: {j = 0;} while: {j < (types size);} step: {++j;} do: {
    $auto: rhs type: ("X"+types[j]);
    rhs value: values[j];
    $try: {
      lhs value: rhs;
      "  " print; lhs print; " from " print;
      rhs print; " (" print; types[j] print; ")" println;
    }
    catch: {
      e = ($ getException);
      "  from " print; types[j] print; ":" println;
      "    " print; (e message) println;
    };
  };
};
"" println;

// extension - type
$for: {i = 0;} while: {i < (types size);} step: {++i;} do: {
  $auto: lhs type: ("X"+types[i]);
  lhs value: values[i];
  types[i] print; ":" println;
  $for: {j = 0;} while: {j < (types size);} step: {++j;} do: {
    $auto: rhs type: types[j];
    rhs value: values[j];
    $try: {
      lhs value: rhs;
      "  " print; lhs print; " from " print;
      rhs print; " (" print; types[j] print; ")" println;
    }
    catch: {
      e = ($ getException);
      "  from " print; types[j] print; ":" println;
      "    " print; (e message) println;
    };
  };
};
"" println;

// extension - extension
$for: {i = 0;} while: {i < (types size);} step: {++i;} do: {
  $auto: lhs type: ("X"+types[i]);
  lhs value: values[i];
  types[i] print; ":" println;
  $for: {j = 0;} while: {j < (types size);} step: {++j;} do: {
    $auto: rhs type: ("X"+types[j]);
    rhs value: values[j];
    $try: {
      lhs value: rhs;
      "  " print; lhs print; " from " print;
      rhs print; " (" print; types[j] print; ")" println;
    }
    catch: {
      e = ($ getException);
      "  from " print; types[j] print; ":" println;
      "    " print; (e message) println;
    };
  };
};
"" println;

