#include <fstream>
#include "itypes.hpp"
#include "iparser.hpp"
#include "icodegen.hpp"
#include "iparseTools.hpp"
#include "mtypes.hpp"
#include "lvector.hpp"

char *readScript(const std::string& fileName);

int main(int argc, const char *argv[]) {
  const std::string fileName(argc > 1 ? argv[1] : "");
  const char *theScript = argc > 1 ? readScript(fileName) :
R"(// Lex test 1
x = 2; /*
/*    787 y*/

*/ x ameth: 1 arg: y other: z;;
$ exit;
)";

  if (!theScript) {
    std::cout << "could not read file " << argv[1] << std::endl;
    return 2;
  }

  // std::cout << "size of IItem  = "      << sizeof(IItem)       << std::endl
            // << "size of IUnit  = "      << sizeof(IUnit)       << std::endl
            // << "size of IBlock = "      << sizeof(IBlock)      << std::endl
            // << "size of ISymbol = "     << sizeof(ISymbol)     << std::endl
            // << "size of IReceivable = " << sizeof(IReceivable) << std::endl
            // << "size of ISetExpression   = " << sizeof(ISetExpression)   << std::endl
            // << "size of unique_ptr<IUnit>= " << sizeof(std::unique_ptr<IUnit>) << std::endl
            // << "size of IArrayExpression = " << sizeof(IArrayExpression) << std::endl;

  IParser parseServices(theScript);
  parseServices.location.initialize(&fileName);

  std::cout << theScript << std::endl;

  yy::parser parser(parseServices);
  i1::vector<MBlock> blockTable;
  ICodeGenerator codeGenerator(parseServices, blockTable);
  int rc;

  try
  {
    rc = parser.parse();
    if (rc != 0) throw "parser failed with exit code "+std::to_string(rc);
    IBlock program = std::move(parseServices.parsedProgram);
    MOperand result = codeGenerator.generateCode(program);
    std::cout << "result = " << result << std::endl;

    if (argc > 1)
    {
      const auto extensionPos = fileName.rfind(".io");
      std::string outputFile;
      if (extensionPos == std::string::npos) outputFile = fileName + ".aio";
      else outputFile = fileName.substr(0, extensionPos) + ".aio";
      std::ofstream os(outputFile);
      os << codeGenerator.disassemble();
    }
    else
    {
      std::cout << "\ndisassembly:\n\n" << codeGenerator.disassemble()
                << std::endl;
    }
  }
  catch (std::string& msg)
  {
    if (argc > 1) delete[] theScript;
    std::cout << msg << std::endl;
    return rc;
  }
  /*catch (const IException& e)
  {
    std::cout << e.title << ": " << e.descr << std::endl;
    return 1;
  }*/

  if (argc > 1) delete[] theScript;
  return 0;
}

char *readScript(const std::string& fileName)
{
  std::ifstream is(fileName, std::ifstream::binary);
  if (!is) return nullptr;
  // get length of file:
  is.seekg(0, is.end);
  long length = is.tellg();
  is.seekg(0, is.beg);
  // get space to store data
  char *fileContent = new char[length+1];
  // read data as a block
  is.read(fileContent,length);
  if (!is)
  {
    delete[] fileContent;
    fileContent = nullptr;
  }
  else if (fileContent[length - 1] != 0)
  {
    std::cout << "null terminating...\n";
    fileContent[length] = 0;
  }
  is.close();
  return fileContent;
}
