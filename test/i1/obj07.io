$class: A definition: {
  .property: "text" type: "string";
  .method: print definition: {
    text println;
  };
};

o = "A"i;
o.text = "hello world!";
o print;

x = (
  $base: o
    member: "otherText" value: "test string"
    member: "print" value: {
      (text + " - " + otherText) println;
    }
);
x print;
