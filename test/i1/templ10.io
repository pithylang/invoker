$class: "A" definition: {
  .ref: r;
  .property: "a" type: "int";
  .public: "print" definition: {
    "r = '" print;
    $if: (r isNull) then: {
      "null" print;
    } else: {
      r print;
    };
    "', a = " print; a println;
  };
};

$class: "B" extends: "A" definition: {
  .property: "t";
  .ref: p;
};

s = "string"i{"test text"};
x = "A"i{ s, 12 };
"x:" println;
x print;

y = "B"i { < 15 > };
d = 3.14;
y.p := d;
"y:" println;
y print;
"t = " print; y.t println;
"p = " print; y.p println;

// throws runtime error excessive template data
//z = "B"i {<21, 76>};
/*z = "B"i { 21, 76 };
"z.a = " print; z.a println;
"z.t = " print; z.t println;

w = "B"i {529};
"w.a = " print; w.a println;
"w.t = " print; w.t println;
*/
