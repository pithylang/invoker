#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template<typename t>
ostream& operator<<(ostream& os, const std::vector<t>& v) {
  for (const auto& item : v) os << "'" << item << "' ";
  return os;
}

vector<string> split(const string& str, const string& delim)
{
  constexpr auto EOS = string::npos;
  vector<string> result;
  if (delim.empty()) return result;
  std::string::size_type start = 0, end = 0;
  while (end != string::npos)
  {
    end = str.find(delim, start);
    result.push_back(str.substr(start, end == EOS ? EOS : end - start));
    start = end > (EOS - delim.size()) ? EOS : end + delim.size();
  }
  return result;
}

///////////
void checkClassOverride(const string& name)
{
  vector<string> keys = split(name, "::");
  if (keys.empty() || !keys.back().empty())
    throw string("error");
  keys.pop_back();
  for (const string& item : keys)
    if (item.find(':') != string::npos)
      throw "'"+item+"' is not a valid class identifier/namespace";
}
///////////

string getStdMethodName1(const string& methodName)
{
  vector<string> keys = split(methodName, ":");
  if (keys.empty())
    throw "'"+methodName+"' is not a proper method name";
  if (!keys.back().empty())
    throw "'"+methodName+"' is not a proper method name";
  keys.pop_back();
  auto firstOccurence = std::adjacent_find(keys.begin(), keys.end());
  if (firstOccurence == keys.end()) return methodName;
  // auto last = firstOccurence + 1;
  // while (last + 1 != keys.end() && *last == *(last + 1)) ++last;
  // if (last < keys.end() - 2) return methodName;
  auto last = firstOccurence + 2;
  while (last != keys.end() && *last == *firstOccurence) ++last;
  if (last < keys.end() - 1) return methodName;
  --last;
  auto back = std::move(keys.back());
  keys.pop_back();
  while (last-- != firstOccurence) keys.pop_back();
  keys.push_back(std::move(back));
  string result;
  std::for_each(keys.begin(), keys.end(), [&result](const string item) {
    result += item + ":";
  });
  return result;
}

const string& getStdMethodName0(const string& originalMethodName, string& methodName)
{
  // Strip extra colons at end; they correspond to variadic argument lists.
  const auto pos = originalMethodName.find_last_not_of(":");
  if (pos == string::npos)
    throw "'"+originalMethodName+"' is not a proper method name";
  if (pos < originalMethodName.length() - 2)
  {
    methodName = originalMethodName.substr(0, pos + 2);
    return methodName;
  }
  return originalMethodName;
}

#if 0
string stGetStdMethodName_(const string& originalMethodName)
{
  // Split into class/namespace override part and name part
  auto p = originalMethodName.find("::");
  auto namePos = p;
  while (p != string::npos)
  {
    namePos = p + 2;
    p = originalMethodName.find("::", namePos);
  }
  const auto prefix = namePos == string::npos ? "" :
      originalMethodName.substr(0, namePos);
  const auto name = namePos == string::npos ? originalMethodName :
      originalMethodName.substr(namePos);
  // Strip extra colons at end; they correspond to variadic argument lists.
  string methodName = getStdMethodName0(name);
  // Merge consecutive identical keys at end or berore the last key
  return prefix + getStdMethodName1(methodName);
}
#endif // 0

string getStdMethodName(const string& originalMethodName)
{
  string methodName;
  // Strip extra colons at end; they correspond to variadic argument lists.
  const auto& r = getStdMethodName0(originalMethodName, methodName);
  // -- r points at either originalMethodName or methodName
  // -- which holds the stripped originalMethodName
  // Split into class/namespace override part and name part
  auto p = r.find("::");
  auto namePos = p;
  while (p != string::npos)
  {
    namePos = p + 2;
    p = r.find("::", namePos);
  }

  if (namePos == string::npos) return getStdMethodName1(r);

  // Merge consecutive identical keys at end or before the last key
  const auto prefix = r.substr(0, namePos);
  // checkClassOverride(prefix);
  const auto name = r.substr(namePos);
  return prefix + getStdMethodName1(name);
}

int main()
{
  const string name = "A::B::x:z:x:x:l:";
  cout << "name: " << name << endl;
  try
  {
    cout << "std name: " << getStdMethodName(name) << endl;
  }
  catch (const string& msg)
  {
    cout << "error: " << msg << endl;
    return 3;
  }

  return 0;
}

