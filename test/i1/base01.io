$class: A definition: {
  . property: x type: int;
  . method: f definition: {
    "x from A: " print; x println;
  };
};

$class: B extends: A definition: {
  . method: f definition: {
    "x from B: " print; x println;
  };
};

b = "B"i;
b A::f;
b f;
b B::f;
