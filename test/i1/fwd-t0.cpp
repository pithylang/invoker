#include <iostream>
#include <forward_list>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::forward_list<T>& l) {
  for (const auto& x : l) std::cout << x << " ";
  return os;
}

template<typename T>
size_t sizeOf(const std::forward_list<T>& l) {
  size_t size = 0;
  for (const auto& x : l) ++size;
  return size;
}

template<typename T>
const T& at(const std::forward_list<T>& l, size_t i) {
  size_t skip = sizeOf(l) - i - 1;
  auto it = l.begin();
  for (size_t n = 0; n < skip; ++n) ++it;
  return *it;
}

template<typename T, typename... Args>
void addElement(std::forward_list<T>& l, Args&& ...args) {
  l.emplace_front(std::forward<Args>(args)...);
}

template<typename T, typename... Args>
void addElement1(std::forward_list<T>& l, Args& ...args) {
  l.emplace_front(std::forward<Args&>(args)...);
}

using namespace std;

struct A {
  string name;
  int age;
  double salary;
  
  A(string&& name, int&& age, double&& salary) : name(std::move(name)), age(age), salary(salary) {}
  A(const string& name, int age, double salary) : name(name), age(age), salary(salary) {}
  A() : name("papares"), age(0), salary(1) {}
};

std::ostream& operator<<(std::ostream& os, const A& a) {
  os << a.name << ": " << a.age << " (" << a.salary << ")";
  return os;
}

int main()
{
  cout<<"Hello World\n";
  
  forward_list<A> list;
  string n1("skata");
  // addElement1(list, std::move(n1), 23, 12.3);
  addElement(list, std::move(n1), 23, 12.3);
  addElement(list, string("ole"), 38, 212.3);
  addElement(list, string("cabriole"), 58, 2012.3);
  addElement(list);

  cout << sizeOf(list) << endl;
  cout << list << endl;
  cout << "'" << n1 << "'" << endl;

  return 0;
}
