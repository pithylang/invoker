#include <iostream>
#include "lstack.hpp"

int main() {
  i1::stack<std::string> s;
  s.push(std::string("skata"));
  s.emplace("ole");
  s.emplace(1, '@');
  std::string a("skatoules");
  s.emplace(a.begin() + 4, a.end());
  for (const auto& x : s) std::cout << x << " ";
  std::cout << std::endl;
  std::cout << s.top() << std::endl;
  s.pop();
  std::cout << s.top() << std::endl;
  std::cout << "stack after pop:" << std::endl;
  for (const auto& x : s) std::cout << x << " ";
  std::cout << std::endl;
  return 0;
}
