#include <iostream>
//#define __NO_FAST_SWITCH__
#include "fswitch.h"

void _error_handler(int i) {
  if (i == 3 || i == 6)
    std::cout << "error " << i << " case does not exist" << std::endl;
  else
    std::cout << "error " << i << std::endl;
}

void f(const std::string& self, int i)
{
  SWITCH_TABLE_CHK(0, 0, 4)
    DEFINE_CASE(0, 0)
    DEFINE_CASE(0, 1)
    DEFINE_CASE(0, 2)
    DEFINE_CASE(0, 3)
    DEFINE_CASE(0, 4)
  END_SWITCH_TABLE(0)

  SWITCH_CHK (0, i)
    CASE(0, 0):
      std::cout << self << std::endl;
      BREAK(0);

    CASE(0, 3):
      f(self, 0);
      BREAK(0);

    CASE(0, 4):
    {
      SWITCH_TABLE_CHK(1, 0, 2)
        DEFINE_CASE(1,0)
        DEFINE_CASE(1,1)
        DEFINE_CASE(1,2)
      END_SWITCH_TABLE(1)

      SWITCH_CHK (1, self.length())
        CASE(1, 0):
          f(self, 2*i);
          BREAK(1);
        CASE(1, 1):
          _error_handler(1);
          BREAK(1);
        CASE(1, 2):
          _error_handler(2);
          BREAK(1);
        DEFAULT(1):
          _error_handler(6);
      END_SWITCH(1)
      BREAK(0);
    }

  CASE(0, 1):
  CASE(0, 2):
  DEFAULT(0):
    _error_handler(3);

  END_SWITCH(0)

  return;
}

int main() {
  int i;
  std::string text;
  std::cout << "input a number: "; std::cin >> i;
  std::cout << "input some text: "; std::cin >> text;
  f(text, i);
  return 0;
}