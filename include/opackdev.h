#ifndef __OPACKDEV_H__
#define __OPACKDEV_H__

#include "object.hpp"
#include <unordered_map>

enum : int {
  PKG_ERROR_NONE = 0,
  PKG_ERROR_CTOR_FAILED,
  PKG_ERROR_IMPORT_NOT_FOUND,
  PKG_ERROR_CVT_CONST_NONCONST,
  PKG_ERROR_NO_EXPORTER,
  PKG_ERROR_INITIALIZATION,
  PKG_ERROR_UNEXPECTED,
  PKG_ERROR_UNINIT_VAR,
  PKG_ERROR_INIT_TERM_IMPORT,
};

using PObjectInitializer = std::function<void(Session*, ORef)>;
using PTypeGetter = std::function<OClass*(Session*)>;

struct PVarConstructInfo {
  OClass* type;
  ORef objectInstance;
  PTypeGetter typeGet;
  PObjectInitializer initializer;
};

using PTypeConstructor = OClass* (*)(Session*, const std::string&);

/**
 *  \class PackageExportInfo
 *  \brief Package export information
 *  \details Information exported by the package exporter
 *  \sa getPackageExportInfo(PackageExportInfo*)
 */
struct PackageExportInfo {
  const std::unordered_map<std::string, PTypeConstructor>* pTypeExports;
  const std::unordered_map<std::string, PVarConstructInfo>* pVarExports;
  std::vector<std::string>* pRegistrationTable;
  std::string packageName;
  std::string packageVersion;
};

extern "C" {
  using PackageInfoImporter = void (*)(PackageExportInfo*);
  /**
   *  \brief Primary exporting function
   *  \details The method \sa Package::importCorePackage(Session*,
   *  const std::vector<std::string>&) uses the information exported
   *  by this function to get to know how to import the requested
   *  objects (types and variables).
   */
  void getPackageExportInfo(PackageExportInfo*);
}

#ifdef PKG_EXPORTING

/**
 *  Export variable of a specified type
 */
# define OM_EXPORT_VAR_TYPE(ID, CLASSPTR, INIT) \
    { ID, { CLASSPTR, nullptr, nullptr, INIT} },
/**
 *  \brief Export a basic type variable
 *  \details Example:
 *  \code{.cpp}
 *  OM_EXPORT_VAR_BASIC_TYPE("pi", double, 3.14)
 *  \endcode
 */
#define OM_EXPORT_VAR_BASIC_TYPE(ID, T, VALUE) \
    { ID, { BTI::getTypeClass<T>(), nullptr, nullptr, \
            [](Session*, ORef o) {_P(T, o)->setValue(VALUE);}} },
/**
 *  Export preconstructed object instance
 */
# define OM_EXPORT_INSTANCE(ID, OINSTANCE) \
    { ID, { nullptr, OINSTANCE, nullptr, nullptr} },
/**
 *  \brief Export variable of a computed type
 *  \details The type of the export variable is computed according
 *  to the `INIT` lambda
 */
# define OM_EXPORT_VAR_GET_TYPE(ID, TYPEGET, INIT) \
    { ID, { nullptr, nullptr, TYPEGET, INIT} },
/**
 *  Specifiy package initializer function
 */
# define OM_EXPORT_INITIALIZER(INIT) \
    { "+", { nullptr, nullptr, nullptr, INIT} },
/**
 *  \todo
 */
# define OM_EXPORT_TERMINATOR(TERM) \
    { "-", { nullptr, nullptr, nullptr, TERM} },
/**
 *  Export class (type)
 */
# define OM_EXPORT_TYPE(EXPNAME, MCLASS) { EXPNAME, MCLASS::ctor },

# define OM_BEGIN_PACKAGE_EXPORT(NAME, VER)                    \
    static const std::string s_packageName{NAME};              \
    static const std::string s_packageVersion{VER};            \
    static const std::unordered_map<std::string, PTypeConstructor> s_typeExports {
# define OM_END_PACKAGE_EXPORT() };
# define OM_BEGIN_VAR_EXPORT()                                 \
    static const std::unordered_map<std::string, PVarConstructInfo> s_varExports {
# define OM_END_VAR_EXPORT()                                   \
    };                                                         \
    static std::vector<std::string> s_registrationTable{};
# define OM_DEFINE_PACKAGE_EXPORTER()                          \
    void getPackageExportInfo(PackageExportInfo* exportInfo) { \
      exportInfo->pTypeExports = &s_typeExports;               \
      exportInfo->pVarExports = &s_varExports;                 \
      exportInfo->pRegistrationTable = &s_registrationTable;   \
      exportInfo->packageName = s_packageName;                 \
      exportInfo->packageVersion = s_packageVersion;           \
    }

# define _EXPORT_VAR_TYPE                 OM_EXPORT_VAR_TYPE
# define _EXPORT_VAR_BASIC_TYPE           OM_EXPORT_VAR_BASIC_TYPE
# define _EXPORT_VAR_GET_TYPE             OM_EXPORT_VAR_GET_TYPE
# define _EXPORT_INSTANCE                 OM_EXPORT_INSTANCE
# define _EXPORT_INITIALIZER              OM_EXPORT_INITIALIZER
# define _EXPORT_TERMINATOR               OM_EXPORT_TERMINATOR
# define _EXPORT_TYPE                     OM_EXPORT_TYPE
# define _BEGIN_PACKAGE_EXPORT(NAME, VER) OM_BEGIN_PACKAGE_EXPORT(NAME, VER)
# define _END_PACKAGE_EXPORT()            OM_END_PACKAGE_EXPORT()
# define _BEGIN_VAR_EXPORT()              OM_BEGIN_VAR_EXPORT()
# define _END_VAR_EXPORT()                OM_END_VAR_EXPORT()
# define _DEFINE_PACKAGE_EXPORTER()       OM_DEFINE_PACKAGE_EXPORTER()

#endif // PKG_EXPORTING

#endif // __OPACKDEV_H__
