#ifndef __OSTRITER_HPP__
#define __OSTRITER_HPP__

#include <string>
#include "ostring.hpp"
#include "oiter.hpp"
#include "orangeOperation.hpp"

using StringIterator = String::iterator;
using StringReverseIterator = String::reverse_iterator;

/**
 *  @class OStringIterator
 */
_BEGIN_EXT_TYPE_DECLARATION(StringIterator, ORandomAccessIterator)
  bool const_{false};
  bool reverse_{false};

public:
  template<typename _Op>
    struct Tie : OTie {
      using ROP = RangeOperationBase;
      using OContainer = typename _Op::container;
      using OIter = _O(StringIterator);
      Tie() : OTie{thisType()} {}
      Tie(OClass* itType) : OTie{itType} {}
      Tie(Tie&&) = default;
      virtual ~Tie() {}
      virtual void apply(ROP* op, ORef cont, ORef a, ORef b) override {
        OContainer& c = *static_cast<OContainer*>(cont);
        if (static_cast<OIter*>(a)->isReverse()) {
          auto rbegin = std::make_reverse_iterator(a->castRef<OIter>());
          auto rend   = std::make_reverse_iterator(b->castRef<OIter>());
          static_cast<_Op&>(*op)(c, rbegin, rend);
        }
        else
          static_cast<_Op&>(*op)(c, a->castRef<OIter>(), b->castRef<OIter>());
      }
    };

public:
  void setConst() noexcept {const_ = true;}
  void setReverse(bool b) noexcept {reverse_ = b;}
  bool isReverse() const noexcept {return reverse_;}
  static inline OClass* thisType() noexcept
  { extern OClass* _OXType_class[]; return _OXType_class[CLSID_STRITER]; }

protected:
  /**
   *  @brief Get i-th component
   *  @details Returns disposable result.
   */
  ORet component(difference_type i, Session *m);

  _METHOD (ommGetValue       );
  _METHOD (ommComponent      );
  _METHOD (inc               );
  _METHOD (postinc           );
  _METHOD (dec               );
  _METHOD (postdec           );
  _METHOD (incBy             );
  _METHOD (decBy             );
  _METHOD (add               );
  _METHOD (sub               );
  _METHOD (eq                );
  _METHOD (ne                );
  _METHOD (lt                );
  _METHOD (gt                );
  _METHOD (le                );
  _METHOD (ge                );
_END_CLASS_DECLARATION()

/**
 *  @class OStringReverseIterator
 */
_BEGIN_EXT_TYPE_DECLARATION(StringReverseIterator, ORandomAccessIterator)
  bool const_{false};

public:
  template<typename _Op>
    struct Tie : OTie {
      using ROP = RangeOperationBase;
      using OContainer = typename _Op::container;
      using OIter = _O(StringReverseIterator);
      Tie() : OTie{thisType()} {}
      Tie(OClass* itType) : OTie{itType} {}
      Tie(Tie&&) = default;
      virtual ~Tie() {}
      virtual void apply(ROP* op, ORef cont, ORef a, ORef b) override {
        OContainer& c = *static_cast<OContainer*>(cont);
        static_cast<_Op&>(*op)(c, a->castRef<OIter>(), b->castRef<OIter>());
      }
    };

public:
  void setConst() noexcept {const_ = true;}
  static inline OClass* thisType() noexcept
  { extern OClass* _OXType_class[]; return _OXType_class[CLSID_STRRITER]; }

protected:
  /**
   *  @brief Get i-th component
   *  @details Returns disposable result.
   */
  ORet component(difference_type i, Session *m);

  _METHOD (ommValue          );
  _METHOD (ommBase           );
  _METHOD (ommGetValue       );
  _METHOD (ommComponent      );
  _METHOD (inc               );
  _METHOD (postinc           );
  _METHOD (dec               );
  _METHOD (postdec           );
  _METHOD (add               );
  _METHOD (incBy             );
  _METHOD (sub               );
  _METHOD (decBy             );
  _METHOD (eq                );
  _METHOD (ne                );
  _METHOD (lt                );
  _METHOD (le                );
  _METHOD (gt                );
  _METHOD (ge                );
_END_CLASS_DECLARATION()

/**
 *  @class OStringIterator_Class
 */
_BEGIN_META_TYPE_DECL(StringIterator)
public:
  META(StringIterator)(Session* m, const std::string& typeName);
  META(StringIterator)(Session* m, const std::string& b, const std::string& c)
    : OClass(m, b, c) {}
  META(StringIterator)(Session* m, const OClass* b, const std::string& c)
    : OClass(m, b, c) {}
  _META_TYPE_DECL(StringIterator)
_END_META_TYPE_DECL(StringIterator)

/**
 *  @class OStringReverseIterator_Class
 */
_BEGIN_META_TYPE_DECL(StringReverseIterator)
public:
  META(StringReverseIterator)(Session* m, const std::string& typeName);
  META(StringReverseIterator)(Session* m, const std::string& b,
      const std::string& c) : OClass(m, b, c) {}
  META(StringReverseIterator)(Session* m, const OClass* b, const std::string& c)
    : OClass(m, b, c) {}
  _META_TYPE_DECL(StringReverseIterator)
_END_META_TYPE_DECL(StringReverseIterator)

#endif // __OSTRITER_HPP__
