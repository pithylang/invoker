#ifndef __LVECTOR_HPP__
#define __LVECTOR_HPP__

#include <vector>
#include <forward_list>
#include <memory>

namespace i1 {

template<typename t>
class vector {
protected:
  using vector_type = typename std::vector<std::unique_ptr<t>>;
  using size_type = typename vector_type::size_type;
  using value_type = typename vector_type::value_type;
  using pointer_vector_iterator = typename vector_type::iterator;
  using pointer_vector_const_iterator = typename vector_type::const_iterator;

  std::vector<std::unique_ptr<t>> vector_;

public:
  vector() {}

  template<typename... Args>
  void emplace_back(Args&&... args) {
    vector_.push_back(std::unique_ptr<t>{new t{args...}});
  }

  void push_back(const t& item) {
    vector_.push_back(std::unique_ptr<t>{new t{item}});
  }

  void push_back(t&& item) {
    vector_.push_back(std::unique_ptr<t>{new t{std::move(item)}});
  }

  void push_back(t* p) {
    vector_.push_back(std::unique_ptr<t>{p});
  }

  inline t& operator[](size_type i) {return *vector_[i];}
  inline const t& operator[](size_type i) const {return *vector_[i];}

  inline t* pointer(size_type i) {return vector_[i].get();}
  inline const t* pointer(size_type i) const {return vector_[i].get();}

  inline t& back() {return *vector_.back();}
  inline const t& back() const {return *vector_.back();}

  inline size_type size() const noexcept {return vector_.size();}
  inline void reserve(size_type n) {vector_.reserve(n);}

  pointer_vector_iterator begin() {return vector_.begin();}
  pointer_vector_const_iterator begin() const {return vector_.begin();}
  pointer_vector_iterator end() {return vector_.end();}
  pointer_vector_const_iterator end() const {return vector_.end();}

  long index(const t* item, size_type startFrom = 0) const {
    for (size_type i = startFrom; i < vector_.size(); ++i)
      if (vector_[i].get() == item) return static_cast<long>(i);
    return -1;
  }
};

/**
 *  \deprecated see `i1::vector`
 */
template<typename t>
class lvector {
  std::forward_list<t> list_;
  std::vector<t*> vector_;

  using size_type = typename std::vector<t*>::size_type;
  using iterator = typename std::vector<t*>::iterator;
  using const_iterator = typename std::vector<t*>::const_iterator;

public:
  lvector() {}

  template<typename... Args>
  void emplace_back(Args&&... args) {
    list_.emplace_front(std::forward<Args>(args)...);
    vector_.push_back(&list_.front());
  }

  void push_back(const t& item) {
    list_.push_front(item);
    vector_.push_back(&list_.front());
  }

  void push_back(t&& item) {
    list_.push_front(std::move(item));
    vector_.push_back(&list_.front());
  }

  inline t& operator[](size_t i) {return *vector_[i];}
  inline const t& operator[](size_t i) const {return *vector_[i];}

  inline size_type size() const noexcept {return vector_.size();}

  iterator begin() {return vector_.begin();}
  const_iterator begin() const {return vector_.begin();}
  iterator end() {return vector_.end();}
  const_iterator end() const {return vector_.end();}

  long index(t* item) const {
    for (size_t i = 0; i < vector_.size(); ++i)
      if (vector_[i] == item) return (long)i;
    return -1;
  }
};

} // namespace i1

#endif // __LVECTOR_HPP__
