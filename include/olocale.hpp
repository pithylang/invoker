#ifndef __OLOCALE_HPP__
#define __OLOCALE_HPP__

#include <locale>
#include "odev.h"
#include "object.hpp"
#include <variant>

//============================================================================//
//::::::::::::::::::::::::::::::::   facet   ::::::::::::::::::::::::::::::::://
//============================================================================//

template<typename charT>
class user_facet : public std::locale::facet {
public:
  static std::locale::id id;
  user_facet(size_t refs = 0) : std::locale::facet{refs} {}
};

using AnyFacet = std::variant<
  std::nullptr_t,

  std::num_get<char>*,
  std::num_get<wchar_t>*,
  std::num_put<char>*,
  std::num_put<wchar_t>*,
  std::numpunct<char>*,
  std::numpunct<wchar_t>*,
  std::numpunct_byname<char>*,
  std::numpunct_byname<wchar_t>*,

  std::time_get<char>*,
  std::time_get<wchar_t>*,
  std::time_put<char>*,
  std::time_put<wchar_t>*,
  std::time_get_byname<char>*,
  std::time_get_byname<wchar_t>*,
  std::time_put_byname<char>*,
  std::time_put_byname<wchar_t>*,

  std::money_get<char>*,
  std::money_get<wchar_t>*,
  std::money_put<char>*,
  std::money_put<wchar_t>*,
  std::moneypunct<char>*,
  std::moneypunct<wchar_t>*,
  std::moneypunct<char, true>*,
  std::moneypunct<wchar_t, true>*,
  std::moneypunct_byname<char>*,
  std::moneypunct_byname<wchar_t>*,
  std::moneypunct_byname<char, true>*,
  std::moneypunct_byname<wchar_t, true>*,

  std::ctype<char>*,
  std::ctype<wchar_t>*,
  std::ctype_byname<char>*,
  std::ctype_byname<wchar_t>*,
  std::codecvt<char, char, std::mbstate_t>*,
  std::codecvt<wchar_t, char, std::mbstate_t>*,
  std::codecvt_byname<char, char, std::mbstate_t>*,
  std::codecvt_byname<wchar_t, char, std::mbstate_t>*,

  std::collate<char>*,
  std::collate<wchar_t>*,
  std::collate_byname<char>*,
  std::collate_byname<wchar_t>*,

  std::messages<char>*,
  std::messages<wchar_t>*,
  std::messages_byname<char>*,
  std::messages_byname<wchar_t>*,

  user_facet<char>*,
  user_facet<wchar_t>*
>;

enum AnyFacetId {
  null_facet_id = 0,

  num_get_id,
  num_getw_id,
  num_put_id,
  num_putw_id,
  numpunct_id,
  numpunctw_id,
  numpunct_byname_id,
  numpunctw_byname_id,

  time_get_id,
  time_getw_id,
  time_put_id,
  time_putw_id,
  time_get_byname_id,
  time_getw_byname_id,
  time_put_byname_id,
  time_putw_byname_id,

  money_get_id,
  money_getw_id,
  money_put_id,
  money_putw_id,
  moneypunct_id,
  moneypunctw_id,
  moneypuncti_id,
  moneypunctiw_id,
  moneypunct_byname_id,
  moneypunctw_byname_id,
  moneypuncti_byname_id,
  moneypunctiw_byname_id,

  ctype_id,
  ctypew_id,
  ctype_byname_id,
  ctypew_byname_id,
  codecvt_id,
  codecvtw_id,
  codecvt_byname_id,
  codecvtw_byname_id,

  collate_id,
  collatew_id,
  collate_byname_id,
  collatew_byname_id,

  messages_id,
  messagesw_id,
  messages_byname_id,
  messagesw_byname_id,

  user_facet_id,
  user_facetw_id
};

//============================================================================//
//::::::::::::::::::::::::::::::::   facets   :::::::::::::::::::::::::::::::://
//============================================================================//

struct Facet : AnyFacet {
  static const std::vector<std::string> facetNames;
  Facet() : AnyFacet{nullptr} {}
  Facet(const Facet& r) = delete;
  Facet(Facet&& r);
  Facet& operator=(const Facet& r) {
    throw std::string{"an instance of class 'Facet' was copied"};
    return *this;
  }
  bool setFacet(int which, const std::string& name = "");
  template<typename t>
    bool setFacet(t* facetPtr);
  template<typename t>
    bool setFacet(const t* facetPtr);
  bool setFacet(std::nullptr_t);
};

OM_BEGIN_CLASS_DECLARATION(Facet)
  OM_METHOD (ommType               );
  OM_METHOD (ommTypeName           );
OM_END_CLASS_DECLARATION()

class oxFacet_Class : public OClass {
  typedef oxFacet OT;
  typedef oxFacet_Class M;
public:
  oxFacet_Class(Session* m) : OClass(m, "facet") {}
  oxFacet_Class(Session* m, const std::string& typeName)
    : OClass(m, typeName) {}
  oxFacet_Class(Session* m, const std::string& b, const std::string& c)
    : OClass(m, b, c) {}
  oxFacet_Class(Session* m, const OClass* b, const std::string& c)
    : OClass(m, b, c) {}
  static OClass* ctor(Session*, const std::string& typeName);
  virtual OClass* createClass(Session* m, const std::string& c) const override;
  virtual OClass* createMutableClass(Session* m, const std::string& c)
      const override;
  virtual size_t getAllocSize() const override
  { return sizeof(OT);  }
  virtual OInstance* constructCore(Session* m) override
  { return genericConstructCore<OT>(m); }
  virtual void destructCore(OInstance* self, Session*) override
  { if (!freeCached<OT>(self)) delete static_cast<OT*>(self); }
  virtual ORef copyCore(ORef tgt, const_ORef src, Session*) const override;
  virtual ORef moveCore(ORef tgt, ORef src) const override;
  OM_ASSOCIATE_MCLASS_WITH(oxFacet)
};

//============================================================================//
//::::::::::::::::::::::::::::::::   locale   :::::::::::::::::::::::::::::::://
//============================================================================//

using Locale = std::locale;

OM_BEGIN_CLASS_DECLARATION(Locale)
  Locale combineWith(const Locale& other, Facet& facet);
  Locale combineWith(const Locale& other, size_t facetIndex) const;
  bool hasFacet(size_t facetIndex) const noexcept;
  bool getFacet(Facet& facet, size_t facetIndex) const;
  template<typename CharT>
    bool isCharType(CharT ch, const std::string& type) const;
  std::string toUpper(const std::string& text) const;
  std::string toLower(const std::string& text) const;
  void toUpper(std::wstring& wideText) const;
  void toLower(std::wstring& wideText) const;
  std::wstring toWide(const std::string& text) const;
  std::string toNarrow(const std::wstring& from) const;
  void noConversionError() const;

  /**
   *  \brief Set value
   *
   *  \details The argument can be:
   *  \li another local
   *  \li a `string`, another local's name
   */
  OM_METHOD (ommValue         );
  /**
   *  \brief Set value excluding a category of facets
   *
   *  \details It constructs a copy of `other`, excluding the facets
   *  of the `cats` category; they are taken from the system locale
   *  `stdName` or `one`, whatever is supplied. The resulting locale
   *  has a name if `other` has one.
   *
   *  \li argument 1: `other` type `locale`
   *  \li argument 2: `stdName` type `string`, or an integer type,
   *                  or `one` type `locale`
   *  \li argument 3: `cats` an integer type
   */
  OM_METHOD (ommValueExceptCat);
  /**
   *  \brief Set value replacing a facet
   *
   *  \details It constructs a copy of `other`, replacing the facet
   *  of the argument's type with the supplied facet. If it is a
   *  null facet the constructed locale is a copy of `other`. In
   *  this case, it will have the `other`'s name; otherwise, it will
   *  have no name.
   */
  OM_METHOD (ommValueFacet    );
  /**
   *  \brief Duplicate locale replacing a facet from another locale
   *
   *  \details It constructs a copy of `this` locale, replacing the
   *  facet of the argument's type with the facet of the supplied
   *  locale `other`.
   *
   *  \li argument 1: `other` type `locale`
   *  \li argument 2: `facetType` a `string` or an integer type
   */
  OM_METHOD (ommCombine       );
  /**
   *  \brief Get locale's name
   */
  OM_METHOD (ommGetName       );
  /**
   *  \brief Check if locale implements a facet
   *
   *  \details It checks if `this` locale implements a facet of
   *  the supplied type. The argument is the facet's type specified
   *  as a `string` or integer type.
   */
  OM_METHOD (ommHasFacet      );
  /**
   *  \brief Obtain a facet implemented by `this` locale
   *
   *  \details The argument is the facet's type specified as a
   *  `string` or integer type. It throws a VM runtime exception
   *  if the locale does not implement the provided facet type.
   */
  OM_METHOD (ommUseFacet      );
  /**
   *  \brief Compare two locales
   *
   *  \details Named locales are considered equal if they have the
   *  same name. Unnamed locales are equal if they are copies of
   *  each other.
   */
  OM_METHOD (ommEq            );
  /**
   *  \brief Compare two locales
   *
   *  \details \sa ommEq(int, OArg*, Session*)
   */
  OM_METHOD (ommNe            );
  /**
   *  \brief Lexicographically compare two strings
   *
   *  \details It compares two strings according to the lexicographic
   *  comparison rules defined by `this` locale's `collate` facet. It
   *  is internally used in standard algorithms like `sort`, and
   *  ordered containers like `set`.
   *
   *  \return `true` if the first string is lexicographically less
   *  than the second, `false` otherwise
   */
  OM_METHOD (ommCompare       );
  /**
   *  \brief Replace the global locale
   *
   *  \details It replaces the global `C++` locale with `this`
   *  locale. All subsequent default constructed `locale` objects
   *  will be a copy of it. This method offers a way to modify the
   *  global `C++` locale, which, as a side note, is the `classic`
   *  locale at program startup.
   */
  OM_METHOD (ommGlobal        );
  /**
   *  \brief Check if a character belongs to a category
   *
   *  \details It checks if a character, supplied as the first
   *  argument, belongs to a particular character category, and
   *  returns `true` or `false`. The character category can be
   *  one of the following `string`s:
   *
   *  \li "blank"
   *  \li "cntrl"
   *  \li "upper"
   *  \li "lower"
   *  \li "alpha"
   *  \li "digit"
   *  \li "punct"
   *  \li "xdigit"
   *  \li "alnum"
   *  \li "print"
   *  \li "graph"
   */
  OM_METHOD (ommIsCharType    );
  /**
   *  \brief Convert character or string to uppercase
   *
   *  \details Convers argument to uppercase according to `this`
   *  locale, and returns the result. The argument can be a `char`,
   *  a `wchar_t` (`uint`), or a `string`.
   */
  OM_METHOD (ommToUpper       );
  /**
   *  \brief Convert character or string to lowercase
   *
   *  \details Convers argument to lowercase according to `this`
   *  locale, and returns the result. The argument can be a `char`,
   *  a `wchar_t` (`uint`), or a `string`.
   */
  OM_METHOD (ommToLower       );
  /**
   *  \brief Widen character or string
   *
   *  \details Converts argument to its wide form according to
   *  `this` locale, and returns the result. The argument can be
   *  a `char` or a `string`.
   */
  OM_METHOD (ommToWide        );
  /**
   *  \brief Narrow character or string
   *
   *  \details Converts argument to its narrow form according to
   *  `this` locale, and returns the result. The argument can be
   *  a `uint` (aka `wchar_t`) or a `wstring`.
   */
  OM_METHOD (ommToNarrow      );
OM_END_CLASS_DECLARATION()

OM_DECLARE_MCLASS(Locale, "locale");

#endif // __OLOCALE_HPP__
