#ifndef __ORTMGR_HPP__
#define __ORTMGR_HPP__

#include "object.hpp"
#include "oclsmgr.hpp"
#include <functional>
#include <string>
#include <tuple>
#include <unordered_set>
#include <map>
#include "multiType.hpp"
#include "orangeOperation.hpp"
#include "otest.hpp"
#include "util.hpp"
#include "osessionLocalData.hpp"
#include "otypes.h"
#include "odev.h"

// RCE: ReCursive Extension

struct VirtualMachine;
class Package;

enum {
  RTFL_DEFAULT                       = 0x00000000,
  RTFL_KEEP_OBJECT_REGISTER          = 0x00000001,
  RTFL_EXIT_ON_OBJECT_DEREG_FAIL     = 0x00000002,
  RTFL_EXIT_ON_OBJECT_REG_FAIL       = 0x00000004,

  RTFL_RTE_ON_OBJECT_DEREG_FAIL      = 0x00000020,

  RTFL_WARN_ON_ADD_MEMBER_CLASSREADY = 0x00001000,
  RTFL_WARN_ON_DUPLICATE_MEMBER      = 0x00002000,

  RTFL_POLICY_SWITCH_TYPE_DECIDE_VAR = 0x00010000,

  RTFL_CHECK_LAMBDA_PROTO            = 0x01000000,
  RTFL_MUTABLE_LAMBDA_PROTO          = 0x02000000,
  RTFL_UNDEF_ARGS_DIAGS              = 0x04000000,

  RTFL_RTMGR_TEST_MODE               = 0x10000000,
  RTFL_RTMGR_GENERATE_ASSEMBLY       = 0x20000000,
  RTFL_RTMGR_VERBOSE_OUTPUT          = 0x40000000,
  RTFL_RTMGR_COLLECT_USAGE_DATA      = 0x80000000,
};

struct OMethodArgs;
class VirtualMachine;
class OInstance;
class OClassMgr;
class Requirement;
class Package;

using MExecScript  = std::function<ORet(const std::string&, int, ulong,
                                   OArg, int, OArg*)>;
using MExecMethod  = std::function<std::pair<ORet,int>(OClass*,
                                   const std::string&, ulong, OArg, int, OArg*)>;
using MExecAddress = std::function<ORet(const OMethodAddress&,
                                   ulong, OArg, int, OArg*)>;
using MExecBlock   = std::function<ORet(int)>;
using MExecLoop    = std::function<void(const std::vector<int>&, bool, bool)>;
using MExecIf      = std::function<bool(OArg, OArg,
                                   const std::vector<OArg>&, OArg)>;
using MExecSwitch  = std::function<void(OArg, const std::vector<OArg>&,
                                   const std::vector<OArg>&, OArg)>;
using MControl     = std::function<void(int)>;
using MExecClassDefProc = std::function<ORet(OClass*, int)>;
using MSetArgNamesProc  = std::function<void(int, std::vector<std::string>&)>;

//============================================================================//
//::::::::::::::::::::::   Runtime Manager (Session)   ::::::::::::::::::::::://
//============================================================================//

/**
 *  @class Session
 *  @brief Runtime manager class
 *  @details There is one Session per primary machine (VM). Secondary
 *  machines share the same session with their owner main machine.
 */
class _OM_LINK Session {
  using string_classptr_table = std::vector<std::pair<std::string, OClass*>>;

public:
  struct PackageInfo {
    Package* package;
    std::string typePrefix;
    std::vector<std::pair<std::string, OClass*>> registeredTypes;
    PackageInfo() = default;
    PackageInfo(Package* package, const std::string& typePrefix) :
        package{package}, typePrefix{typePrefix} {}
    PackageInfo(Package* package, std::string&& prefix) :
        package{package}, typePrefix{std::move(prefix)} {}
  };

  using string_usage_table = std::unordered_map<std::string, ObjectUsage>;

  unsigned flags;
  /**
   *  @brief Disable object caching
   *  @details When `true` objects caching is disabled even when
   *  a cache is available.
   */
  bool forceNoCache{false};
  ulong parseOptions;

protected:
  VirtualMachine* mainMachine_;
  VirtualMachine* currentMachine_;
  MExecScript execScriptProc_;
  MExecMethod execMethodProc_;
  MExecBlock execBlockProc_;
  MExecLoop execLoopProc_;
  MExecIf execIfProc_;
  MExecSwitch execSwitchProc_;
  MControl controlProc_;
  MExecClassDefProc execClassDefProc_;
  MSetArgNamesProc setArgNamesProc_;
  ORef exception_;
  ORef savedException_;
  std::vector<ORef> discardedObjects_;
  om::SessionLocalData sessionLocalData_;

public:
  Session* caller{nullptr};
  OClassMgr* classMgr;
  std::function<void(const std::string&)> stringOutput;
  std::function<void(const std::string&, const std::string&)> displayMessage;

protected:
  std::unordered_set<ORef> objectTable_;
  std::forward_list<PackageInfo> packageStack_;
  std::unique_ptr<std::map<int, ObjectUsage>> usageData_;
  std::unique_ptr<string_usage_table> objectCacheConfigData_;
  Checker checker_;

public:
  Session();
  ~Session();

  om::SessionLocalData& getSessionLocalData() noexcept
  { return sessionLocalData_; }
  const om::SessionLocalData& getSessionLocalData() const noexcept
  { return sessionLocalData_; }

  // Receiver info
  inline bool thisConst() const noexcept
  {return false; /* TODO use machine to get value */ }
  inline void setThisConst(bool on = true) noexcept
  { /* TODO use machine to set value */ }

  //:::::::::::::::::::::::::::::::   Flags   :::::::::::::::::::::::::::::::://

  /** Register objects */
  inline bool registerObjects() const noexcept
  { return Flags::is<RTFL_KEEP_OBJECT_REGISTER>(flags); }
  inline void keepObjectRegister(bool on = true) noexcept
  { Flags::set<RTFL_KEEP_OBJECT_REGISTER>(flags, on); }

  inline bool checkLambdaProto() const noexcept
  { return Flags::is<RTFL_CHECK_LAMBDA_PROTO>(flags); }
  inline void setCheckLambdaProto(bool on = true) noexcept
  { Flags::set<RTFL_CHECK_LAMBDA_PROTO>(flags, on); }

  inline bool mutableLambdaProto() const noexcept
  { return Flags::is<RTFL_MUTABLE_LAMBDA_PROTO>(flags); }
  inline void setMutableLambdaProto(bool on = true) noexcept
  { Flags::set<RTFL_MUTABLE_LAMBDA_PROTO>(flags, on); }

  inline bool undefinedArgsDiagnostics() const noexcept
  { return Flags::is<RTFL_UNDEF_ARGS_DIAGS>(flags); }
  inline void setUndefinedArgsDiagnostics(bool on = true) noexcept
  { Flags::set<RTFL_UNDEF_ARGS_DIAGS>(flags, on); }

  inline bool warnOnAddMemberClassReady() const noexcept
  { return (flags & RTFL_WARN_ON_ADD_MEMBER_CLASSREADY) != 0; }
  inline bool warnOnDuplicateMember() const noexcept
  { return (flags & RTFL_WARN_ON_DUPLICATE_MEMBER) != 0; }
  inline bool policySwitchTypeFromVar() const noexcept
  { return (flags & RTFL_POLICY_SWITCH_TYPE_DECIDE_VAR) != 0; }

  inline bool testingMode() const noexcept
  { return (flags & RTFL_RTMGR_TEST_MODE) != 0; }
  inline void setTestingMode() noexcept { flags |= RTFL_RTMGR_TEST_MODE; }

  inline bool generateAssembly() const noexcept
  { return Flags::is<RTFL_RTMGR_GENERATE_ASSEMBLY>(flags); }
  inline void setGenerateAssembly(bool on = true) noexcept
  { Flags::set<RTFL_RTMGR_GENERATE_ASSEMBLY>(flags, on); }

  inline bool verboseOutput() const noexcept
  { return Flags::is<RTFL_RTMGR_VERBOSE_OUTPUT>(flags); }
  inline void setVerboseOutput(bool on = true) noexcept
  { Flags::set<RTFL_RTMGR_VERBOSE_OUTPUT>(flags, on); }

  inline bool collectingUsageData() const noexcept
  { return Flags::is<RTFL_RTMGR_COLLECT_USAGE_DATA>(flags); }
  inline void setCollectUsageData(bool on = true) noexcept
  { Flags::set<RTFL_RTMGR_COLLECT_USAGE_DATA>(flags, on); }

  //::::::::::::::::::::::::::::   Requirements   :::::::::::::::::::::::::::://

  /**
   *  @brief Get a registered @c Requirement object
   *  @param reqId The identifier of the @c Requirement object
   *  @return A pointer to the @c Requirement object or @c nullptr
   *  if there is no @c Requirement with the supplied identifier
   *  in the list
   */
  Requirement* getRequirement(const std::string& reqId) const;
  /**
   *  @brief Register requirement
   *  @details It registers requirements with `reqs_` in a way
   *  that the list remains sorted in the requirement identifiers.
   *  @param req `Requirement&&`
   *  @return `Requirement*` pointer to the registered
   *  requirement or `nullptr` if already registered
   */
  Requirement* registerRequirement(Requirement&& req);
  /**
   *  @brief Set class requirement
   *  @details Associates a class with a requirement. The class
   *  must not be already associated with a requirement. The
   *  requirement should not be already registered with `reqs_`.
   *  @param type `OClass*`
   *  @param req `Requirement&&`
   */
  void addRequirement(OClass* type, Requirement&& req);
  /**
   *  @brief Add pre-registered requirement to class
   *  @details Associates a class with a requirement already
   *  associated with another class or registered with `reqs_`.
   *  The class must not have previously defined a requirement.
   *  @param type `OClass*` The class being registered with the
   *         supplied requirement
   *  @param reqId The unique requirements identifier
   */
  void importRequirement(OClass* type, const std::string& reqId);

  //:::::::::::::::::::::::::::   Set Callbacks   :::::::::::::::::::::::::::://

  void setExecScriptProc(MExecScript callback) { execScriptProc_ = callback; }
  void setExecMethodProc(MExecMethod callback) { execMethodProc_ = callback; }
  void setExecBlockProc (MExecBlock  callback) { execBlockProc_  = callback; }
  void setExecLoopProc  (MExecLoop   callback) { execLoopProc_   = callback; }
  void setExecIfProc    (MExecIf     callback) { execIfProc_     = callback; }
  void setExecSwitchProc(MExecSwitch callback) { execSwitchProc_ = callback; }
  void setControlProc   (MControl    callback) { controlProc_    = callback; }
  void setExecClassDefProc  (MExecClassDefProc cb) { execClassDefProc_ = cb; }
  void setMethodArgNamesProc(MSetArgNamesProc  cb) { setArgNamesProc_  = cb; }

  //:::::::::::::::::::::::::::   Use Callbacks   :::::::::::::::::::::::::::://

  void setMethodArgNames(int blockId, std::vector<std::string>& argNames)
  { setArgNamesProc_(blockId, argNames); }

  ORet execScriptMethod(const SAddr& a, const std::string& methodName,
                        ulong flags, OArg rec, int argc, OArg* argv) const {
    return execScriptProc_(methodName, a.block, flags, rec, argc, argv);
  }

  inline std::pair<ORet, int>
  execMethod(OClass* baseClass, const std::string& name, ulong flags,
             OArg rec, int argc, OArg* argv) {
    return execMethodProc_(baseClass, name, flags, rec, argc, argv);
  }
  inline std::pair<ORet, int>
  execMethod(OClass* baseClass, const std::string& name, ulong flags,
             ORef r, int argc, OArg* argv)
  { OVar rec{r}; return execMethod(baseClass, name, flags, &rec, argc, argv); }

  inline std::pair<ORet, int>
  execMethod(const std::string& name, ulong flags, OArg rec, int argc, OArg* argv)
  { return execMethodProc_(rec->objectRef->getType(), name, flags, rec, argc, argv); }
  inline std::pair<ORet, int>
  execMethod(const std::string& name, ulong flags, ORef r, int argc, OArg* argv) {
    OVar rec{r};
    return execMethodProc_(r->getType(), name, flags, &rec, argc, argv);
  }

  inline std::pair<ORet, int> execMethodNoThrowUntilExec(const std::string& name,
      OArg rec, int argc, OArg* argv) {
    const ulong flags = EMF_NO_THROW_UNTIL_EXEC;
    return execMethodProc_(rec->objectRef->getType(), name, flags, rec, argc, argv);
  }

  ORet execMethodAddress(const OMethodAddress& methodAddress,
                         const std::string& methodName,
                         ulong flags,
                         OArg rec, int argc, OArg* argv);

  inline ORet execBlock(int blockId) {
    return execBlockProc_(blockId);
  }

  //:::::::::::::::::::::::::   Exception handling   ::::::::::::::::::::::::://

  inline bool exceptionPending() const noexcept {
    return exception_ != nullptr;
  }

  inline bool savedException() const noexcept {
    return savedException_ != nullptr;
  }

  inline void setException(ORef e) noexcept {
    if (!exceptionPending())
      exception_ = e;
  }

  ORef popException() noexcept {
    return std::exchange(exception_, nullptr);
  }

  inline void saveException(ORef e) noexcept {
    if (!savedException())
      savedException_ = e;
  }

  void clearException() noexcept;

  void terminate();

  /**
   *  @brief Rethrow exception from a higher frame
   *
   *  Add a call frame to the present exception.
   *  @param method the method in which the exception is re-thrown
   *  @param srcFile the source file in which the exception is re-thrown
   *  @param line the line of the source file
   */
  ORef rethrow(const std::string& method, const std::string& srcFile = "",
               int line = -1);
  /**
   *  @brief Throw object model exception
   *
   *  @param exceptionType @c string, the object model type of the exception
   *  @param initException @c lambda, initializes the exception
   *
   *  The @c initException lambda must cast the correct exception
   *  pointer from the passed argument, initialize the exception
   *  parameters, if any, and finally call @c addFrame.
   */
  void throwException(const std::string& exceptionType,
                      std::function<void(ORef)> initException);
  /**
   *  @brief Throw exception @c bad @c class
   *
   *  @param method  @c string, the method that caused this exception
   *  @param description @c string, a short description of the exception
   *  @param reason  @c string, the reason that caused the exception
   *  @param context @c string, the context within which the exception occurred
   *  @param srcFile @c string, the source file in which the exception occurred
   *  @param line    @c int, the line in source file where the exception
   *                         occurred
   *
   *  The first two parameters form the exception frame; the
   *  remaining parameters are specific to this exception type.
   *  The first parameter signifies where the exception occurred.
   */
  void throwBadClass(const std::string& method,
                     const std::string& description = "",
                     const std::string& context = "",
                     const std::string& reason = "",
                     const std::string& srcFile = "", int line = -1);
  void throwBadArg(const std::string& method, const std::string& message,
                   const std::string& srcFile = "", int line = -1);
  void throwBadArgc(const std::string& method, const std::string& scriptMethod,
                    int expectedArgc = -1, const std::string& srcFile = "",
                    int line = -1);
  void throwBadArgc(const std::string& method, const std::string& scriptMethod,
                    const std::string& message, const std::string& srcFile = "",
                    int line = -1);
  void throwBadCoreCopy(const std::string& method, const std::string& message,
                        const std::string& srcFile = "", int line = -1);
  void throwBadUsage(const std::string& method, const std::string& message,
                     const std::string& scriptMethod,
                     const std::string& srcFile = "", int line = -1);
  void throwRTE(const std::string& method, const std::string& message,
                const std::string& srcFile = "", int line = -1);

  /**
   *  @brief Unconditionally throw a bad argument VM-exception
   *
   *  @details This is a higher-level method than
   *  void throwBadArg(const std::string&, const std::string&,
   *  const std::string&, int line), and it has a clearer
   *  interface.
   *  @param methodName `const std::string&` The method where this
   *         condition occurred
   *  @param i `int` The argument index starting from 0
   *  @param expectedType `const std::string&` The expected type
   *  @param foundType `const std::string&` The type found
   *  @return undefined object `ORet`
   */
  ORet throwBadArg(const std::string& methodName, int i,
                   const std::string& expectedType,
                   const std::string& foundType);
  /**
   *  @brief Unconditionally throw a bad argument VM-exception
   *
   *  @param methodName @c const @c string& The method where this
   *         condition occurred
   *  @param i @c int The argument index starting from 0
   *  @param type @c OClass* The expected type
   *  @param argv @c OArg[] The method's argument list
   *  @return ORet @c undefined
   */
  ORet badArgType(const std::string& methodName, int i, const OClass* type,
                  OArg* argv);
  /**
   *  @brief Unconditionally throw a bad argument VM-exception
   *
   *  @param methodName @c const @c string& The method where this
   *        condition occurred
   *  @param i @c int The argument index starting from 0
   *  @param typeId @c int The expected type identifier
   *  @param argv @c OArg[] The method's argument list
   *  @return ORet undefined
   */
  ORet badArgTypeId(const std::string& methodName, int i, int typeId,
                    OArg* argv);

  /**
   *  @brief Check if pending exception is type `bad argument`
   *
   *  Returns `false` if no exception is pending.
   */
  bool isExceptionBadArg() const;

  //:::::::::::::::::::::::::   Machine Utilities   :::::::::::::::::::::::::://

  inline void setMainMachine(VirtualMachine* vm) { mainMachine_ = vm; }
  inline void setCurrentMachine(VirtualMachine* vm) { currentMachine_ = vm; }
  VirtualMachine* getCurrentMachine() const noexcept { return currentMachine_; }
  VirtualMachine* getMainMachine() const noexcept { return mainMachine_; }
  /**
   *  @brief Get current machine value
   *  @param which machine value to get, @sa MParam
   */
  template<typename T>
    inline T getCurrentMachineValue(MParam which) const noexcept
    { return g_getMachineValue(getCurrentMachine(), which).get<T>(); }

  inline OVar* getReceiver() const noexcept
  { return static_cast<OVar*>(getCurrentMachineValue<void*>(M_RECEIVER)); }

  inline OClass* getCallingClass() const noexcept
  { return static_cast<OClass*>(getCurrentMachineValue<void*>(M_CALLER_CLASS)); }

  /**
   *  Check if current machine is in returning condition
   */
  inline bool isCurrentMachineReturning() const noexcept;
  void machineInitError() const;

  //:::::::::::::::::::::::::   Object Management   :::::::::::::::::::::::::://

  void registerObject(ORef);
  void deregisterObject(ORef);
  std::vector<std::string> getInfoRegisteredObjects();
  std::string getObjectUsageInfo() const;
  void discardObjects(const std::vector<ORef>&);

  //::::::::::::::::::::::::::   Object Checking   ::::::::::::::::::::::::::://

  /**
   *  @brief Check if the supplied argument is an object
   *  @details Use this method only when an object register is
   *  maintained. @sa registerObjects()
   *  @param @c o type @c ORef (@c OInstance* ) the object to check
   */
  bool isObject(ORef o) const;
  /**
   *  @brief Check if object is valid
   *  @details Checks if the object's class is valid. Use this method
   *  after having checked that the supplied argument is actually an
   *  object with @sa isObject(ORef).
   *  @param @c o type @c ORef (@c OInstance* ) the object to check
   */
  inline bool isValidObject(ORef o) const
  { return classMgr->isClass(o->getClass()); }
  /**
   *  @brief Get global instance of `null` object
   *  @details It is used to initialize @c property @c references
   *  as a safer replacement of @c undefined objects.
   */
  inline ORef nullObject() const noexcept {return classMgr->nullObject();}
  /**
   *  @brief Check if the argument is the standard null object
   *  @details Check if the supplied argument is the global instance
   *  of the `null` object.
   */
  inline bool isStdNullObject(ORef o) const noexcept
  { return classMgr->isStdNullObject(o); }

  //::::::::::::::::::::::::::::   Object Usage   :::::::::::::::::::::::::::://

  inline void collectUsageData() {
    if (!usageData_)
      usageData_ = std::make_unique<std::map<int, ObjectUsage>>();
  }
  inline void increaseUsage(int typeId) noexcept
  { if (collectingUsageData()) (*usageData_)[typeId].inc(); }
  inline void increaseUsage(OClass* type) noexcept
  { increaseUsage(type->getTypeId()); }
  inline void reduceUsage(int typeId) noexcept {
    if (!collectingUsageData()) return;
    auto it = usageData_->find(typeId);
    _ASSERT(it != usageData_->end() && it->second.canDelete(), this);
    it->second.dec();
  }
  inline void reduceUsage(OClass* type) noexcept
  { reduceUsage(type->getTypeId()); }
  int checkUsageData() const noexcept {
    if (collectingUsageData()) {
      for (const auto& [typeId, objectUsage] : *usageData_)
        if (!objectUsage.check()) return typeId;
    }
    return -1;
  }
  bool checkUsageData(int typeId) const noexcept {
    if(!collectingUsageData()) return true;
    const auto it = usageData_->find(typeId);
    return it == usageData_->end() || it->second.check();
  }
  inline bool checkUsageData(OClass* type) const noexcept
  { return checkUsageData(type->getTypeId()); }

  void configObjectCaches(string_usage_table&& cfg);
  ObjectUsage getCacheConfigForType(const std::string& typeName);

  //::::::::::::::::::::::::   Object Manipulation   ::::::::::::::::::::::::://

  /**
   *  @brief Delete object
   *  @details Performs validation checks on supplied instance and
   *  then it invokes the object's class destruction method to
   *  delete the object.
   *  @param @c o type @c ORef (@c OInstance* ) the object to delete
   */
  void deleteObject(ORef o);
  /**
   *  @brief Duplicate object
   *  @details It is a utility method combining the standard copy
   *  and delete methods on objects.
   */
  ORef duplicateObject(const_ORef);
  /**
   *  @brief Copy object performing cast or user specified copy operations
   *
   *  @param `lhsVar` The target object
   *  @param `rhsVar` The source object
   *
   *  @details It uses internally the method
   *  @sa ORet setValueWithCast(ORef, OVar*, bool, bool)
   *  with `applyValue = false` and `init = false`.
   */
  ORet copyFrom(OArg lhsVar, OArg rhsVar);
  /**
   *  @brief Set value with possible cast
   *
   *  @param `lhsVar` The target object variable
   *  @param `rhsVar` The source object variable
   *
   *  @details This method adds a layer on top of
   *  OClass::copyObject(ORef, const_ORef, Session*).
   *  If the @em RHS object is not the type of the @em LHS object,
   *  it applies the following algorithm to get a meaningful value
   *  for `lhs`:
   *  @li 1. If `applyValue` is `true`, it attempts to apply the
   *         user supplied method `value:` to set the value of `lhsVar`
   *  @li 2. Then it checks if `lhsVar` has a user supplied method
   *         `copy:`, and if it has, it is used to copy the @em RHS
   *         object to `lhsVar`
   *  @li 3. Finally, if `rhsVar` has a conversion method to the
   *  class of `lhsVar`, it is applied to convert `rhsVar` to an
   *  object of this type, and then it moves this object to
   *  `lhsVar`.
   *
   *  If none of these methods is available, it throws an exception.
   *  If the @em RHS object is of the type of the @em LHS object, it
   *  chains to the standard `OClass` copy method. If any errors
   *  occur during the process, it tries to cure them if possible.
   *
   *  The caller is responsible to check if `lhsVar` is mutable.
   */
  ORet setValueWithCast(OClass* lhsType, OVar* lhsVar, OVar* rhsVar,
                        bool applyValue, bool init);
  inline ORet setValueWithCast(OClass* lhsType, ORef lhs, OVar* rhsVar,
                        bool applyValue, bool init) {
    OVar lhsVar{lhs};
    return setValueWithCast(lhsType, &lhsVar, rhsVar, applyValue, init);
  }
  /**
   *  @brief Set value with possible cast
   *
   *  @details It calls
   *  ORet setValueWithCast(OClass*, OVar*, OVar*, bool, bool)
   *  with `lhsType = lhsVar->objectRef->getClass()`.
   */
  inline ORet setValueWithCast(OVar* lhsVar, OVar* rhsVar,
                               bool applyValue, bool init) {
    return setValueWithCast(lhsVar->objectRef->getClass(), lhsVar, rhsVar,
        applyValue, init);
  }
  /**
   *  @brief Cast object
   *
   *  @param @c type The type, @c OClass*, into which to cast
   *      the source object
   *  @param @c src The source object
   *  @param @c rc Pointer to returned error code; it can be
   *      nullptr. If it is a @c nullptr, it throws a
   *      RT-exception in case of error; otherwise, if the
   *      conversion method does not exist or is inaccessible
   *      `rc` is set. Other exceptions are thrown as usually.
   *  @return A variable holding the result of the conversion,
   *      an object type @c type or a null variable in case
   *      of error
   *
   *  @details It applies the object's conversion method to
   *  convert it into an object type @c type. Conversion
   *  methods have the name of the type to be converted into.
   *
   *  This method uses `execMethodProc_` to cast object `src`
   *  into a newly created object type `type`.
   */
  ORet valueCast(OClass* type, ORef src, int* rc = nullptr);
  /**
   *  @brief Check cast result
   *
   *  @details Checks the result of a cast operation
   *  (ORet valueCast(OClass*, ORef, int*)) and throws a @c bad
   *  @c class exception when an inconsistent result is detected.
   *
   *  Because `valueCast` may have thrown an exception, it is
   *  recommended that exception checking is done before calling
   *  this method.
   */
  bool checkCastResult(ORet cast, const OClass* to, const OClass* from);
  /**
   *  @brief Emplace variable
   *  @details If `var` is disposable, it swaps the object of the
   *  variable `var` into `thisPlace` rendering `var` non-disposable.
   *  If it is not, it duplicates `var`'s object into `thisPlace`.
   */
  void emplace(OVar& thisPlace, OVar& var);
  /**
   *  @brief Move variable to returned value
   *  @details It invokes @sa void emplace(OVar&, OVar&) into a
   *  local `thisPlace`, and then it returns its object.
   */
  ORef moveBack(OVar& var);
  /**
   *  @brief Move object rendering it if it is a proxy object
   *
   *  @details This function does not throw VM exceptions. If a
   *  VM-exception occurs, it returns a `nullptr`.
   *
   *  @param lhs `ORef` the target object
   *  @param var the source object variable
   *  @param m `Session*`
   *  @return `ORef` the modified original object, the rendered
   *  proxy object, or, if an error occured, `nullptr`. The
   *  possible sources of error are (1) the source variable
   *  is not movable, (2) the types of the source object or
   *  its rendered version do not agree, and (3) a VM-exception
   *  happened.
   */
  ORef moveRenderObject(ORef lhs, OArg var);
  /**
   *  @brief Duplicate object rendering it if it is a proxy object
   *
   *  @details This function does not throw VM exceptions. If a VM
   *  exception occurs, it returns a `nullptr`.
   *
   *  @param var the source object variable
   *  @param m `Session*`
   *  @return ORef the output duplicate or rendered object
   */
  inline ORef dupRenderObject(ORef o) {return dupRenderObject(o, false);}
  ORef dupRenderObject(ORef o, bool forceNoCache);
  inline ORef dupRenderObject(OArg a) {return dupRenderObject(a->objectRef);}
  /**
   *  @brief Optimizing copy method
   *  @details If `rhs` is disposable, it swaps the object of the
   *  variable `var` into `thisPlace` rendering `var` non-disposable.
   *  If it is not, it duplicates `var`'s object into `thisPlace`.
   */
  inline void smartCopy(OVar& lhs, OVar& rhs, bool withCast = true)
  { smartCopy(lhs, rhs, std::function<void(Session*, OVar&, OVar&)>{}, withCast); }
  inline void smartCopyNoCast(OVar& l, OVar& r) { smartCopy(l, r, false); }
  void smartCopy(OVar& lhs, OVar& rhs,
      const std::function<void(Session*, OVar&, OVar&)>&, bool withCast = true);
  void smartCopyNoCast(OVar& lhs, OVar& rhs,
      const std::function<void(Session*, OVar&, OVar&)>& f) {
    smartCopy(lhs, rhs, f, false);
  }

  //::::::::::::::::::::::   Create Object From ISON   ::::::::::::::::::::::://

  ORef createObjectFromDefinition(int argc, OArg* argv)
  { return createObjectExtension(nullptr, argc, argv); }
  ORef createObjectExtension(OVar&& baseObjectVar, int argc, OArg* argv);
  ORef createObjectExtension(ORef baseObject, int argc, OArg* argv)
  { return createObjectExtension(OVar{baseObject}, argc, argv); }
  ORef extendObject(ORef baseObject, int argc, OArg* argv);
  ORef extendObject(ORef baseObject,
      const std::vector<std::pair<std::string,ORef>>&, ushort usage = USG_NONE);

  //:::::::::::::::::::::::   Create Template Object   ::::::::::::::::::::::://

  ORef createObjectFromTemplateParams(const std::string& typeName,
                                      int argc, OArg* argv);
  ORef createInitObject(const std::string& typeName, int argc, OArg* argv);

  //::::::::::::::::::::::::::   Class Management   :::::::::::::::::::::::::://

  inline OClass* getClass(int typeId) const noexcept
  { return classMgr->getClass(typeId); }

  inline const OClass* getRootClass(int typeId) const noexcept
  { return getClass(typeId)->getRootClass(); }

  inline int getRootClassId(int typeId) const noexcept
  { return getRootClass(typeId)->getTypeId(); }

  inline bool isBuiltinType(const OClass* type) const noexcept
  { return type->getTypeId() < CLSID_END; }

  inline bool isBuiltinType(const std::string& typeName) const noexcept {
    OClass* const type = classMgr->getClass(typeName);
    return type && isBuiltinType(type);
  }

  /**
   *  @brief Create class object
   *
   *  @details Invokes the root class's method @c createClass
   *  to create the root class part of the class object.
   */
  OClass* createClassObject(const std::string& className,
                            const std::string& baseClass);
  /**
   *  @brief Delete mutable class
   *
   *  @details Release class ID and destroy C++ metaclass instance.
   */
  void deleteMutableClass(OClass* type) const;

  //:::::::::::::::::::::::::   Testing Framework   :::::::::::::::::::::::::://

  void checkpointText(const std::string& text);
  void ignoreCheckpoint() noexcept
  { if (testingMode()) checker_.ignoreCheckpoint(); }
  void performCheck(const std::string& expectedText);
  void mergeCheck() {if (testingMode()) checker_.mergeCheck();}
  void setCheckpoint() {if (testingMode()) checker_.setCheckpoint();}
  bool expectExceptions() const noexcept
  { return !checker_.expectedExceptions.empty(); }
  bool isExpectedException(int line, const std::string& type = "",
      const std::string& loc = "", const std::string& msg = "") const noexcept
  { return checker_.isExpectedException(line, type, loc, msg); }
  size_t expectedLeftovers() const noexcept
  { return checker_.expectedLeftovers; }
  bool expectedRetValue(int retValue) const noexcept {
    return checker_.expectedRetValue == Checker::MIN_RET_VALUE ||
           checker_.expectedRetValue == retValue;
  }
  void setParseExceptionToIgnore(const std::string& except) noexcept
  { checker_.parseExceptionToIgnore = except; }
  std::string getParseExceptionToIgnore() const noexcept
  { return checker_.parseExceptionToIgnore; }

  //::::::::::::::::::::::::::::::   Packages   :::::::::::::::::::::::::::::://

  /**
   *  @brief Get the type prefix of the current package
   */
  std::string getTypePrefix() const;
  std::string getPrefixedTypeName(const std::string& typeName) const;
  /**
   *  @brief Check if a package is being handled
   */
  inline bool importing() const noexcept { return !packageStack_.empty(); }
  /**
   *  @brief Begin new package
   *  @details It pushes a new `PackageInfo` structure on top of
   *  the package stack signifying that a new package is being
   *  handled.
   */
  void beginPackage(Package* ptr, const std::string& prefix)
  { packageStack_.emplace_front(ptr, prefix); }
  /**
   *  @brief Get current package info
   *  @details Get the `PackageInfo` structure of the package
   *  being handled. Use @sa importing() to check if a package
   *  is currently being handled.
   */
  PackageInfo& getPackageInfo() { return packageStack_.front(); }
  /**
   *  @brief End package
   *  @details It pops a `PackageInfo` structure from the top of
   *  the package stack signifying that a package handling was
   *  finished.
   */
  void endPackage() { packageStack_.pop_front(); }

  //::::::::::::::::::::::::::   Range Operations   :::::::::::::::::::::::::://

  std::vector<RangeOperationBase*> publicRangeOperations_;
  void publicOperation(RangeOperationBase* op)
  { publicRangeOperations_.push_back(op); }
  template<typename... _Ops>
    requires (std::conjunction_v<std::is_pointer<_Ops>...> &&
              std::conjunction_v<std::is_base_of<
                  RangeOperationBase, std::remove_pointer_t<_Ops>>...>)
    void publicOperations(_Ops... ops) {
      RangeOperationBase* v[sizeof...(ops)]{ ops... };
      for (RangeOperationBase* op : v) publicOperation(op);
    }
  void forEachRangeOperation(const std::function<bool(RangeOperationBase*)>& f){
    for (RangeOperationBase* op : publicRangeOperations_)
      if (!f(op)) break;
  }
};

//============================================================================//
//::::::::::::::::::::::::   Runtime Manager Object   :::::::::::::::::::::::://
//============================================================================//

class _OM_LINK ORTMgr : public OInstance, public Session {
public:
  ORTMgr(OClass* cls) : OInstance(cls), Session() {}
  ~ORTMgr() {}
  ORTMgr& operator=(const Session& r) {
    throw std::logic_error{"runtime manager object copied"};
    return *this;
  }
  ORTMgr& operator=(const ORTMgr& r) {
    return operator=(static_cast<const Session&>(r));
  }

protected:
  /**
   *  Create an instance of a script extended class
   *
   *  This method is unique to this class.
   *
   *  The difference between this method and @c createClass
   *  is that this method constructs a class defined in a script,
   *  while @c createClass creates a @c Session class. Notice that
   *  neither creates an object, i.e., an instance of @c OInstance.
   */
  ORet ommDefineClass(OArg, int, OArg*, Session*);

public:
  /**
   *  Create an instance of a script extended class without a VM
   *
   *  The constructed class object is @em not finalized so that
   *  members can be added. One can use this method to simulate
   *  a class construction from a C++ program.
   */
  ORet createClassObjectNoVM(OArg, int, OArg*, Session*);

protected:
  _METHOD( ommAlias                   );
  _METHOD( ommGetArg                  );
  _METHOD( ommGetArgPtr               );
  _METHOD( ommGetArgCount             );
  _METHOD( ommBuildArgList            );
  _METHOD( ommDeleteArgList           );
  _METHOD( ommDeleteObject            );
  _METHOD( ommIsUndefined             );
  _METHOD( ommIsObject                );
  _METHOD( ommIsType                  );
  _METHOD( ommIsDisposable            );
  _METHOD( ommIsMovable               );
  /** @brief Check if argument is a local variable */
  _METHOD( ommIsLocalVar              );
  _METHOD( ommIsArg                   );
  _METHOD( ommIsLiteral               );
  _METHOD( ommIsConst                 );
  _METHOD( ommIsRef                   );
  _METHOD( ommCopyFrom                );
  _METHOD( ommGetClass                );
  _METHOD( ommGetException            );
  _METHOD( ommGetCurrentLine          );
  _METHOD( ommFilterException         );
  _METHOD( ommCreateArray             );
  _METHOD( ommCreateSet               );
  _METHOD( ommCreateEmptyArray        );
  _METHOD( ommCreateEmptySet          );
  _METHOD( ommCreateEmptyObj          );
  _METHOD( ommCreateObj               );
  _METHOD( ommCreateObjExt            );
  _METHOD( ommCreateObjFromTemplArgs  );
  _METHOD( ommCreateCObjFromTemplArgs );
  _METHOD( ommCreateNObjFromTemplArgs );
  _METHOD( ommCreateInitObject        );
  _METHOD( ommCreateInitCObject       );
  _METHOD( ommCreateInitNoAutoObject  );
  _METHOD( ommSetException            );
  _METHOD( ommAutoVar                 );
  _METHOD( ommConstAutoVar            );
  _METHOD( ommAutoVarWithValue        );
  _METHOD( ommConstAutoVarWithValue   );
  _METHOD( ommAutoTmp                 );
  _METHOD( ommConstAutoTmp            );
  /**
   *  @brief Create a noauto object instance
   *
   *  @details The created instance is non-disposable and, hence,
   *  not automatically deleted. It can be assigned to a reference
   *  or used and then handed over (through `move:`) to an
   *  object's initializer that expects a disposable argument.
   */
  _METHOD( ommNoAuto                  );
  _METHOD( ommMove                    );
  _METHOD( ommPassLiteral             );
  _METHOD( ommMoveObject              );
  _METHOD( ommStaticVar               );
  _METHOD( ommConstStaticVar          );
  _METHOD( ommRefVar                  );
  _METHOD( ommForLoop                 );
  _METHOD( ommWhileLoop               );
  _METHOD( ommUntilLoop               );
  _METHOD( ommWhileAtEndLoop          );
  _METHOD( ommUntilAtEndLoop          );
  _METHOD( ommIfThen                  );
  _METHOD( ommIfThenElse              );
  _METHOD( ommIfThenElseif            );
  _METHOD( ommIfThenElseifElse        );
  _METHOD( ommSwitchCase              );
  _METHOD( ommSwitchCaseDefault       );
  _METHOD( ommBreak                   );
  _METHOD( ommBreakMany               );
  _METHOD( ommBreakManyIf             );
  _METHOD( ommReturn                  );
  _METHOD( ommReturnIf                );
  _METHOD( ommReturnValue             );
  _METHOD( ommReturnRef               );
  _METHOD( ommReturnThis              );
  _METHOD( ommReturnConstRef          );
  _METHOD( ommTryCatch                );
  _METHOD( ommTryCatchDo              );
  _METHOD( ommThrow                   );
  _METHOD( ommCheck                   );
  _METHOD( ommMergeCheck              );
  _METHOD( ommIgnoreCheck             );
  _METHOD( ommSetCheckpoint           );
  _METHOD( ommExpectException         );
  _METHOD( ommExpectLeftovers         );
  _METHOD( ommExpectRetValue          );
  _METHOD( ommCheckLambdaProtos       );
  _METHOD( ommMutableLambdaProtos     );
  _METHOD( ommUndefArgsDiags          );

  ORet createAutoVar(int argc, OArg* argv, Session* m, bool constVar, bool value);
  ORet createStaticVar(int argc, OArg* argv, Session* m, bool constVar);
  ORet createAutoTmp(int argc, OArg* argv, Session*, bool constVar, bool noauto);

  OM_DECLARE_METHOD_TABLE();
};

#define OM_PRTMgr(ref) static_cast<ORTMgr*>(ref)
#define _PRTMgr(ref) OM_PRTMgr(ref)

template<typename _ObjectType>
  inline _ObjectType* OClass::getObjectFromCache(Session* m) noexcept
  { return m->forceNoCache ? nullptr : getObjectFromCache<_ObjectType>(); }

//============================================================================//
//::::::::::::::::::::::   Runtime Manager Metaclass   ::::::::::::::::::::::://
//============================================================================//

class _OM_LINK ORTMgr_Class : public OClass {
public:
  /** Constructor for this class only */
  ORTMgr_Class(OClassMgr* cm) : OClass(cm, T_runtime_manager) {}
  /** Constructor for script-derived classes */
  ORTMgr_Class(Session* m, const std::string& base, const std::string& clsname)
      : OClass(m, base, clsname) {}
  /** Constructor for mutable classes */
  ORTMgr_Class(Session* m, const OClass* base, const std::string& clsname)
      : OClass(m, base, clsname) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;
  virtual size_t getAllocSize() const override { return sizeof(ORTMgr); }
  /** Create no cache for runtime manager objects */
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual OInstance* constructCore(Session*) override
  { return new ORTMgr{this}; }
  virtual void destructCore(OInstance* o, Session*) override;

  /**
   *  @brief Create runtime manager object
   *
   *  @details We override this method in order to allow invocation
   *  with `NULL` argument.
   */
  virtual ORef createObject(bool, Session*) override;
  /**
   *  @brief Destroy runtime manager instance
   *
   *  @details The destruction of the runtime object requires
   *  special treatment because the argument to be destroyed
   *  is possibly the `Session*` argument. Also, the user might
   *  have customized the standard runtime manager object.
   */
  virtual void deleteObject(ORef, Session*) override;

  OM_ASSOCIATE_MCLASS_WITH(ORTMgr)
};

#endif // __ORTMGR_HPP__
