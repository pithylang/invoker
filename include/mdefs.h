#ifndef __MDEFS_H__
#define __MDEFS_H__

#include <string>

#ifdef __DLL__
#  if defined(__EXPORTING__)
#     define _OX_LINK __declspec(dllexport)
#  elif defined(__IMPORTING__)
#     define _OX_LINK __declspec(dllimport)
#  else
#     define _OX_LINK
#  endif /*__EXPORTING__,__IMPORTING__*/
#else /*EXE*/
#  if defined(__IMPORTING__)
#     define _OX_LINK __declspec(dllimport)
#  else
#     define _OX_LINK
#  endif /*__IMPORTING__*/
#endif /*__DLL__*/

// typedef unsigned int uint;
// typedef unsigned long ulong;

//============================================================================//
//                        Special Symbol-Table Indices                        //
//============================================================================//
enum {
  SYM_INDEX_SYSOBJECT = -2,
  SYM_INDEX_NOT_FOUND = -1,
  SYM_INDEX_NO_RESULT = SYM_INDEX_NOT_FOUND,
  SYM_INDEX_ERROR     = SYM_INDEX_NOT_FOUND,
  SYM_OFS_ERROR       = SYM_INDEX_ERROR,
  SYM_INDEX_THIS      = 0
};

//============================================================================//
//                   MOpcode - VM operation codes (opcodes)                   //
//============================================================================//
enum MOpcode : unsigned {
  nop = 0,
  /** Set current source line */
  setl,
  /** Set current source file */
  setf,
  /**
   *  @brief Push variable on execution stack
   *  @details Push variable on execution stack with symbol table lookup.
   */
  push,
  /**
   *  @brief Push register SO
   *  @details Operand:
   *  @li 0: argument
   *  @li 1: receiver
   */
  pushso,
  /**
   *  @brief Push operand as receiver even if it is null
   *  @details It begins a new frame and pushes the referenced symbol
   *  as the next receiver. This instruction is used in
   *  conjunction with @c setv. For the details see @c setv.
   */
  pushrz,
  /** @brief Execute method unit on stack */
  exec,
  execb,   ///< execute block
  execlb,  ///< execute lambda block
  execgp,  ///< execute get property
  execcb,  ///< execute conditional block
  stor,    ///< store register RES to indicated id (must be local)
  enter,   ///< enter frame; operand is number of locals
  leave,   ///< exit frame
  jmp,     ///< unconditionally jump to specified offset
  dto,     ///< destroy disposed tmp object (destruction is done only when
           ///< the object is disposed)
  dtor,    ///< similar to dto, object is referenced by RES register
  rop,     ///< various operations on registers depending on operand
  /**
   *  @brief Check if operand is an rvalue
   *  @details Check if the operand is an rvalue (tmp) and, if it
   *  is, throw a @em runtime exception.
   */
  chkrv,
  lres,    ///< load RES with indicated variable
  pushra,  ///< push resolved address (for usual selectors use push)
  /** @brief Make indicated argument a reference */
  mref,
  /** @brief Push receiver on execution stack */
  pushrec,
  /** @brief Push selector on execution stack */
  pushsel,
  /** @brief Push code block on execution stack */
  pushb,
  /**
   *  @brief Register block classes
   *  @details Register classes defined locally in this block. The
   *  operand is the block identifier.
   */
  rbc,
  /**
   *  @brief Assign value to variable
   *  @details It sets a value to an uninitialized variable or, if
   *  initialized, it copies the value, making some optimizations.
   *  In that case, it branches to the method `copy:from:`. The
   *  operand is the value to be assigned. Besides, it must have
   *  been pushed previously as an argument onto the execution
   *  stack.
   *  @par This operation does not apply to static variables.
   */
  setv,
  /** Push symbol on execution stack without symbol table lookup */
  pushsym,
  oc_end
};

enum OpcodeEx {
  nopex = 0,
  scop,    ///< set constant operation flag
  lcru,    ///< @deprecated
  zres,    ///< zero RES (load undefined variable)
  stchk,   ///< check stack
  cres     ///< make RES constant
};

//============================================================================//
//                             VΜ Flags Register                              //
//============================================================================//
enum MDebugFlags {
  DBF_STD_MODE                      =  0x00000000,
  DBF_DEBUG_MODE                    =  0x00000001,
  DBF_DIAGNOSTIC_MODE               =  DBF_DEBUG_MODE,
  DBF_DEBUGGER_PRESENT              =  0x00000002,
  DBF_DEBUG_METHOD_NAMES            =  0x00000004,
  DBF_DEBUG_NEXT                    =  0x00000008,
  DBF_DEBUG_STEP                    =  0x00000010,
  DBF_DEBUG_NEXTI                   =  0x00000020,
  DBF_DEBUG_STEPI                   =  0x00000040,
  DBF_DEBUG_GO_WHILE_NOT_EXITED     =  0x00000080,
  DBF_DEBUG_GO_UNTIL_EXITED         =  0x00000100,
  DBF_DEBUG_GO                      =  0x00000200,
  DBF_DEBUG_CONTINUE                =  DBF_DEBUG_GO,
  DBF_DEBUG_TRACE_ALL               =  DBF_DEBUG_NEXT  |
                                       DBF_DEBUG_STEP  |
                                       DBF_DEBUG_NEXTI |
                                       DBF_DEBUG_STEPI |
                                       DBF_DEBUG_GO    |
                         DBF_DEBUG_GO_WHILE_NOT_EXITED |
                         DBF_DEBUG_GO_UNTIL_EXITED,
  DBF_DEBUG_CHECK_STACK_OFFSETS     =  0x00000400,
  DBF_DEBUG_MERGE_GLOBAL_OFFSETS    =  0x00008000,
  DBF_WARN_USE_OF_UNINIT_LOCAL_VAR  =  0x00010000,
  DBF_WARN_MERGE_GLOBAL_OFFSETS     =  0x00020000,
  DBF_EXCEPTION                     =  0x80000000,
  DBF_RECOVERING_FROM_EXCEPTION     =  DBF_EXCEPTION,
  DBF_END
};

#if 0
//============================================================================//
//                                Event Table                                 //
//============================================================================//
enum {
  _RESOLVE_EVENT_TYPE,
  _RESOLVE_ID,
  _RESOLVE_LAST_ID,
  _RESOLVE_PARAM
};
#endif // 0

#endif // __MDEFS_H__
