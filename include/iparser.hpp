// A Bison parser, made by GNU Bison 3.8.2.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018-2021 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


/**
 ** \file include/iparser.hpp
 ** Define the yy::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

// DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
// especially those whose name start with YY_ or yy_.  They are
// private implementation details that can be changed or removed.

#ifndef YY_YY_INCLUDE_IPARSER_HPP_INCLUDED
# define YY_YY_INCLUDE_IPARSER_HPP_INCLUDED
// "%code requires" blocks.
#line 27 "src/bison/i.y"

#include "itypes.hpp"
class IParser;

#line 54 "include/iparser.hpp"

# include <cassert>
# include <cstdlib> // std::abort
# include <iostream>
# include <stdexcept>
# include <string>
# include <vector>

#if defined __cplusplus
# define YY_CPLUSPLUS __cplusplus
#else
# define YY_CPLUSPLUS 199711L
#endif

// Support move semantics when possible.
#if 201103L <= YY_CPLUSPLUS
# define YY_MOVE           std::move
# define YY_MOVE_OR_COPY   move
# define YY_MOVE_REF(Type) Type&&
# define YY_RVREF(Type)    Type&&
# define YY_COPY(Type)     Type
#else
# define YY_MOVE
# define YY_MOVE_OR_COPY   copy
# define YY_MOVE_REF(Type) Type&
# define YY_RVREF(Type)    const Type&
# define YY_COPY(Type)     const Type&
#endif

// Support noexcept when possible.
#if 201103L <= YY_CPLUSPLUS
# define YY_NOEXCEPT noexcept
# define YY_NOTHROW
#else
# define YY_NOEXCEPT
# define YY_NOTHROW throw ()
#endif

// Support constexpr when possible.
#if 201703 <= YY_CPLUSPLUS
# define YY_CONSTEXPR constexpr
#else
# define YY_CONSTEXPR
#endif
# include "../include/location.hpp"
#include <typeinfo>
#ifndef YY_ASSERT
# include <cassert>
# define YY_ASSERT assert
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

namespace yy {
#line 194 "include/iparser.hpp"




  /// A Bison parser.
  class parser
  {
  public:
#ifdef YYSTYPE
# ifdef __GNUC__
#  pragma GCC message "bison: do not #define YYSTYPE in C++, use %define api.value.type"
# endif
    typedef YYSTYPE value_type;
#else
  /// A buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current parser state.
  class value_type
  {
  public:
    /// Type of *this.
    typedef value_type self_type;

    /// Empty construction.
    value_type () YY_NOEXCEPT
      : yyraw_ ()
      , yytypeid_ (YY_NULLPTR)
    {}

    /// Construct and fill.
    template <typename T>
    value_type (YY_RVREF (T) t)
      : yytypeid_ (&typeid (T))
    {
      YY_ASSERT (sizeof (T) <= size);
      new (yyas_<T> ()) T (YY_MOVE (t));
    }

#if 201103L <= YY_CPLUSPLUS
    /// Non copyable.
    value_type (const self_type&) = delete;
    /// Non copyable.
    self_type& operator= (const self_type&) = delete;
#endif

    /// Destruction, allowed only if empty.
    ~value_type () YY_NOEXCEPT
    {
      YY_ASSERT (!yytypeid_);
    }

# if 201103L <= YY_CPLUSPLUS
    /// Instantiate a \a T in here from \a t.
    template <typename T, typename... U>
    T&
    emplace (U&&... u)
    {
      YY_ASSERT (!yytypeid_);
      YY_ASSERT (sizeof (T) <= size);
      yytypeid_ = & typeid (T);
      return *new (yyas_<T> ()) T (std::forward <U>(u)...);
    }
# else
    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    emplace ()
    {
      YY_ASSERT (!yytypeid_);
      YY_ASSERT (sizeof (T) <= size);
      yytypeid_ = & typeid (T);
      return *new (yyas_<T> ()) T ();
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    emplace (const T& t)
    {
      YY_ASSERT (!yytypeid_);
      YY_ASSERT (sizeof (T) <= size);
      yytypeid_ = & typeid (T);
      return *new (yyas_<T> ()) T (t);
    }
# endif

    /// Instantiate an empty \a T in here.
    /// Obsolete, use emplace.
    template <typename T>
    T&
    build ()
    {
      return emplace<T> ();
    }

    /// Instantiate a \a T in here from \a t.
    /// Obsolete, use emplace.
    template <typename T>
    T&
    build (const T& t)
    {
      return emplace<T> (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as () YY_NOEXCEPT
    {
      YY_ASSERT (yytypeid_);
      YY_ASSERT (*yytypeid_ == typeid (T));
      YY_ASSERT (sizeof (T) <= size);
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const YY_NOEXCEPT
    {
      YY_ASSERT (yytypeid_);
      YY_ASSERT (*yytypeid_ == typeid (T));
      YY_ASSERT (sizeof (T) <= size);
      return *yyas_<T> ();
    }

    /// Swap the content with \a that, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsibility.
    /// Swapping between built and (possibly) non-built is done with
    /// self_type::move ().
    template <typename T>
    void
    swap (self_type& that) YY_NOEXCEPT
    {
      YY_ASSERT (yytypeid_);
      YY_ASSERT (*yytypeid_ == *that.yytypeid_);
      std::swap (as<T> (), that.as<T> ());
    }

    /// Move the content of \a that to this.
    ///
    /// Destroys \a that.
    template <typename T>
    void
    move (self_type& that)
    {
# if 201103L <= YY_CPLUSPLUS
      emplace<T> (std::move (that.as<T> ()));
# else
      emplace<T> ();
      swap<T> (that);
# endif
      that.destroy<T> ();
    }

# if 201103L <= YY_CPLUSPLUS
    /// Move the content of \a that to this.
    template <typename T>
    void
    move (self_type&& that)
    {
      emplace<T> (std::move (that.as<T> ()));
      that.destroy<T> ();
    }
#endif

    /// Copy the content of \a that to this.
    template <typename T>
    void
    copy (const self_type& that)
    {
      emplace<T> (that.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
      yytypeid_ = YY_NULLPTR;
    }

  private:
#if YY_CPLUSPLUS < 201103L
    /// Non copyable.
    value_type (const self_type&);
    /// Non copyable.
    self_type& operator= (const self_type&);
#endif

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ () YY_NOEXCEPT
    {
      void *yyp = yyraw_;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const YY_NOEXCEPT
    {
      const void *yyp = yyraw_;
      return static_cast<const T*> (yyp);
     }

    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // block
      char dummy1[sizeof (IBlock)];

      // item
      // itemi
      // itemac
      char dummy2[sizeof (IItem)];

      // recbl
      // jsond
      char dummy3[sizeof (IReceivable)];

      // "string with attributes"
      char dummy4[sizeof (IStringWithAttrs)];

      // "identifier"
      // "selector"
      // "ulong"
      // "long"
      // "uint"
      // "int"
      // "short"
      // "char"
      // "bool"
      // "float"
      // "double"
      // "string"
      // lit
      // ssel
      // sel
      char dummy5[sizeof (ISymbol)];

      // stm
      // unit
      // expr
      char dummy6[sizeof (IUnit)];

      // pgm
      // stlist
      // cexpr
      // dmexpr
      char dummy7[sizeof (std::vector<IUnit>)];
    };

    /// The size of the largest semantic type.
    enum { size = sizeof (union_type) };

    /// A buffer to store semantic values.
    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me_;
      /// A buffer large enough to store any of the semantic values.
      char yyraw_[size];
    };

    /// Whether the content is built: if defined, the name of the stored type.
    const std::type_info *yytypeid_;
  };

#endif
    /// Backward compatibility (Bison 3.8).
    typedef value_type semantic_type;

    /// Symbol locations.
    typedef location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m)
        : std::runtime_error (m)
        , location (l)
      {}

      syntax_error (const syntax_error& s)
        : std::runtime_error (s.what ())
        , location (s.location)
      {}

      ~syntax_error () YY_NOEXCEPT YY_NOTHROW;

      location_type location;
    };

    /// Token kinds.
    struct token
    {
      enum token_kind_type
      {
        TOK_YYEMPTY = -2,
    TOK_END = 0,                   // "end of file"
    TOK_YYerror = 256,             // error
    TOK_YYUNDEF = 257,             // "invalid token"
    TOK_DOTS = 258,                // ".."
    TOK_ELLIPS = 259,              // "..."
    TOK_FWD = 260,                 // "=>"
    TOK_REFSET = 261,              // ":="
    TOK_CREFSET = 262,             // "::="
    TOK_EVAL = 263,                // "@"
    TOK_COMMA = 264,               // ","
    TOK_BXOREQ = 265,              // "^="
    TOK_BOREQ = 266,               // "|="
    TOK_BANDEQ = 267,              // "&="
    TOK_SHLEQ = 268,               // "<<="
    TOK_SHREQ = 269,               // ">>="
    TOK_PLUSEQ = 270,              // "+="
    TOK_MINUSEQ = 271,             // "-="
    TOK_MODEQ = 272,               // "%="
    TOK_DIVEQ = 273,               // "/="
    TOK_MULEQ = 274,               // "*="
    TOK_COPY = 275,                // "="
    TOK_CCOPY = 276,               // "@="
    TOK_QMARK = 277,               // "?"
    TOK_OR = 278,                  // "||"
    TOK_XOR = 279,                 // "^^"
    TOK_AND = 280,                 // "&&"
    TOK_BOR = 281,                 // "|"
    TOK_BXOR = 282,                // "^"
    TOK_BAND = 283,                // "&"
    TOK_DIF = 284,                 // "!="
    TOK_EQ = 285,                  // "=="
    TOK_GE = 286,                  // ">="
    TOK_GT = 287,                  // ">"
    TOK_LE = 288,                  // "<="
    TOK_LT = 289,                  // "<"
    TOK_SHR = 290,                 // ">>"
    TOK_SHL = 291,                 // "<<"
    TOK_PLUS = 292,                // "+"
    TOK_MINUS = 293,               // "-"
    TOK_MUL = 294,                 // "*"
    TOK_DIV = 295,                 // "/"
    TOK_MOD = 296,                 // "%"
    TOK_EXP = 297,                 // "**"
    TOK_UPLUS = 298,               // UPLUS
    TOK_UMINUS = 299,              // UMINUS
    TOK_BNOT = 300,                // "~"
    TOK_LNOT = 301,                // "!"
    TOK_DOT = 302,                 // "."
    TOK_RPLUSPLUS = 303,           // RPLUSPLUS
    TOK_RMINUSMINUS = 304,         // RMINUSMINUS
    TOK_PLUSPLUS = 305,            // "++"
    TOK_MINUSMINUS = 306,          // "--"
    TOK_XOREQ = 307,               // "^^="
    TOK_ANDEQ = 308,               // "&&="
    TOK_OREQ = 309,                // "||="
    TOK_LP = 310,                  // "("
    TOK_RP = 311,                  // ")"
    TOK_LB = 312,                  // "["
    TOK_RB = 313,                  // "]"
    TOK_LCB = 314,                 // "{"
    TOK_RCB = 315,                 // "}"
    TOK_RTI = 316,                 // ">}"
    TOK_COL = 317,                 // ":"
    TOK_DCOL = 318,                // "::"
    TOK_SEMI = 319,                // ";"
    TOK_ID = 320,                  // "identifier"
    TOK_SELID = 321,               // "selector"
    TOK_ULONG = 322,               // "ulong"
    TOK_LONG = 323,                // "long"
    TOK_UINT = 324,                // "uint"
    TOK_INT = 325,                 // "int"
    TOK_SHORT = 326,               // "short"
    TOK_CHAR = 327,                // "char"
    TOK_BOOL = 328,                // "bool"
    TOK_FLT = 329,                 // "float"
    TOK_DBL = 330,                 // "double"
    TOK_STRING = 331,              // "string"
    TOK_STRWATTR = 332             // "string with attributes"
      };
      /// Backward compatibility alias (Bison 3.6).
      typedef token_kind_type yytokentype;
    };

    /// Token kind, as returned by yylex.
    typedef token::token_kind_type token_kind_type;

    /// Backward compatibility alias (Bison 3.6).
    typedef token_kind_type token_type;

    /// Symbol kinds.
    struct symbol_kind
    {
      enum symbol_kind_type
      {
        YYNTOKENS = 78, ///< Number of tokens.
        S_YYEMPTY = -2,
        S_YYEOF = 0,                             // "end of file"
        S_YYerror = 1,                           // error
        S_YYUNDEF = 2,                           // "invalid token"
        S_DOTS = 3,                              // ".."
        S_ELLIPS = 4,                            // "..."
        S_FWD = 5,                               // "=>"
        S_REFSET = 6,                            // ":="
        S_CREFSET = 7,                           // "::="
        S_EVAL = 8,                              // "@"
        S_COMMA = 9,                             // ","
        S_BXOREQ = 10,                           // "^="
        S_BOREQ = 11,                            // "|="
        S_BANDEQ = 12,                           // "&="
        S_SHLEQ = 13,                            // "<<="
        S_SHREQ = 14,                            // ">>="
        S_PLUSEQ = 15,                           // "+="
        S_MINUSEQ = 16,                          // "-="
        S_MODEQ = 17,                            // "%="
        S_DIVEQ = 18,                            // "/="
        S_MULEQ = 19,                            // "*="
        S_COPY = 20,                             // "="
        S_CCOPY = 21,                            // "@="
        S_QMARK = 22,                            // "?"
        S_OR = 23,                               // "||"
        S_XOR = 24,                              // "^^"
        S_AND = 25,                              // "&&"
        S_BOR = 26,                              // "|"
        S_BXOR = 27,                             // "^"
        S_BAND = 28,                             // "&"
        S_DIF = 29,                              // "!="
        S_EQ = 30,                               // "=="
        S_GE = 31,                               // ">="
        S_GT = 32,                               // ">"
        S_LE = 33,                               // "<="
        S_LT = 34,                               // "<"
        S_SHR = 35,                              // ">>"
        S_SHL = 36,                              // "<<"
        S_PLUS = 37,                             // "+"
        S_MINUS = 38,                            // "-"
        S_MUL = 39,                              // "*"
        S_DIV = 40,                              // "/"
        S_MOD = 41,                              // "%"
        S_EXP = 42,                              // "**"
        S_UPLUS = 43,                            // UPLUS
        S_UMINUS = 44,                           // UMINUS
        S_BNOT = 45,                             // "~"
        S_LNOT = 46,                             // "!"
        S_DOT = 47,                              // "."
        S_RPLUSPLUS = 48,                        // RPLUSPLUS
        S_RMINUSMINUS = 49,                      // RMINUSMINUS
        S_PLUSPLUS = 50,                         // "++"
        S_MINUSMINUS = 51,                       // "--"
        S_XOREQ = 52,                            // "^^="
        S_ANDEQ = 53,                            // "&&="
        S_OREQ = 54,                             // "||="
        S_LP = 55,                               // "("
        S_RP = 56,                               // ")"
        S_LB = 57,                               // "["
        S_RB = 58,                               // "]"
        S_LCB = 59,                              // "{"
        S_RCB = 60,                              // "}"
        S_RTI = 61,                              // ">}"
        S_COL = 62,                              // ":"
        S_DCOL = 63,                             // "::"
        S_SEMI = 64,                             // ";"
        S_ID = 65,                               // "identifier"
        S_SELID = 66,                            // "selector"
        S_ULONG = 67,                            // "ulong"
        S_LONG = 68,                             // "long"
        S_UINT = 69,                             // "uint"
        S_INT = 70,                              // "int"
        S_SHORT = 71,                            // "short"
        S_CHAR = 72,                             // "char"
        S_BOOL = 73,                             // "bool"
        S_FLT = 74,                              // "float"
        S_DBL = 75,                              // "double"
        S_STRING = 76,                           // "string"
        S_STRWATTR = 77,                         // "string with attributes"
        S_YYACCEPT = 78,                         // $accept
        S_pgm = 79,                              // pgm
        S_block = 80,                            // block
        S_stlist = 81,                           // stlist
        S_stm = 82,                              // stm
        S_unit = 83,                             // unit
        S_item = 84,                             // item
        S_itemi = 85,                            // itemi
        S_itemac = 86,                           // itemac
        S_lit = 87,                              // lit
        S_cexpr = 88,                            // cexpr
        S_expr = 89,                             // expr
        S_recbl = 90,                            // recbl
        S_jsond = 91,                            // jsond
        S_ssel = 92,                             // ssel
        S_sel = 93,                              // sel
        S_dmexpr = 94                            // dmexpr
      };
    };

    /// (Internal) symbol kind.
    typedef symbol_kind::symbol_kind_type symbol_kind_type;

    /// The number of tokens.
    static const symbol_kind_type YYNTOKENS = symbol_kind::YYNTOKENS;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol kind
    /// via kind ().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol () YY_NOEXCEPT
        : value ()
        , location ()
      {}

#if 201103L <= YY_CPLUSPLUS
      /// Move constructor.
      basic_symbol (basic_symbol&& that)
        : Base (std::move (that))
        , value ()
        , location (std::move (that.location))
      {
        switch (this->kind ())
    {
      case symbol_kind::S_block: // block
        value.move< IBlock > (std::move (that.value));
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.move< IItem > (std::move (that.value));
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.move< IReceivable > (std::move (that.value));
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.move< IStringWithAttrs > (std::move (that.value));
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.move< ISymbol > (std::move (that.value));
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.move< IUnit > (std::move (that.value));
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.move< std::vector<IUnit> > (std::move (that.value));
        break;

      default:
        break;
    }

      }
#endif

      /// Copy constructor.
      basic_symbol (const basic_symbol& that);

      /// Constructors for typed symbols.
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, location_type&& l)
        : Base (t)
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const location_type& l)
        : Base (t)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, IBlock&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const IBlock& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, IItem&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const IItem& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, IReceivable&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const IReceivable& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, IStringWithAttrs&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const IStringWithAttrs& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, ISymbol&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const ISymbol& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, IUnit&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const IUnit& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<IUnit>&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<IUnit>& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

      /// Destroy the symbol.
      ~basic_symbol ()
      {
        clear ();
      }



      /// Destroy contents, and record that is empty.
      void clear () YY_NOEXCEPT
      {
        // User destructor.
        symbol_kind_type yykind = this->kind ();
        basic_symbol<Base>& yysym = *this;
        (void) yysym;
        switch (yykind)
        {
       default:
          break;
        }

        // Value type destructor.
switch (yykind)
    {
      case symbol_kind::S_block: // block
        value.template destroy< IBlock > ();
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.template destroy< IItem > ();
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.template destroy< IReceivable > ();
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.template destroy< IStringWithAttrs > ();
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.template destroy< ISymbol > ();
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.template destroy< IUnit > ();
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.template destroy< std::vector<IUnit> > ();
        break;

      default:
        break;
    }

        Base::clear ();
      }

      /// The user-facing name of this symbol.
      std::string name () const YY_NOEXCEPT
      {
        return parser::symbol_name (this->kind ());
      }

      /// Backward compatibility (Bison 3.6).
      symbol_kind_type type_get () const YY_NOEXCEPT;

      /// Whether empty.
      bool empty () const YY_NOEXCEPT;

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      value_type value;

      /// The location.
      location_type location;

    private:
#if YY_CPLUSPLUS < 201103L
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& that);
#endif
    };

    /// Type access provider for token (enum) based symbols.
    struct by_kind
    {
      /// The symbol kind as needed by the constructor.
      typedef token_kind_type kind_type;

      /// Default constructor.
      by_kind () YY_NOEXCEPT;

#if 201103L <= YY_CPLUSPLUS
      /// Move constructor.
      by_kind (by_kind&& that) YY_NOEXCEPT;
#endif

      /// Copy constructor.
      by_kind (const by_kind& that) YY_NOEXCEPT;

      /// Constructor from (external) token numbers.
      by_kind (kind_type t) YY_NOEXCEPT;



      /// Record that this symbol is empty.
      void clear () YY_NOEXCEPT;

      /// Steal the symbol kind from \a that.
      void move (by_kind& that);

      /// The (internal) type number (corresponding to \a type).
      /// \a empty when empty.
      symbol_kind_type kind () const YY_NOEXCEPT;

      /// Backward compatibility (Bison 3.6).
      symbol_kind_type type_get () const YY_NOEXCEPT;

      /// The symbol kind.
      /// \a S_YYEMPTY when empty.
      symbol_kind_type kind_;
    };

    /// Backward compatibility for a private implementation detail (Bison 3.6).
    typedef by_kind by_type;

    /// "External" symbols: returned by the scanner.
    struct symbol_type : basic_symbol<by_kind>
    {
      /// Superclass.
      typedef basic_symbol<by_kind> super_type;

      /// Empty symbol.
      symbol_type () YY_NOEXCEPT {}

      /// Constructor for valueless symbols, and symbols from each type.
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, location_type l)
        : super_type (token_kind_type (tok), std::move (l))
#else
      symbol_type (int tok, const location_type& l)
        : super_type (token_kind_type (tok), l)
#endif
      {
#if !defined _MSC_VER || defined __clang__
        YY_ASSERT (tok == token::TOK_END
                   || (token::TOK_YYerror <= tok && tok <= token::TOK_SEMI));
#endif
      }
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, IStringWithAttrs v, location_type l)
        : super_type (token_kind_type (tok), std::move (v), std::move (l))
#else
      symbol_type (int tok, const IStringWithAttrs& v, const location_type& l)
        : super_type (token_kind_type (tok), v, l)
#endif
      {
#if !defined _MSC_VER || defined __clang__
        YY_ASSERT (tok == token::TOK_STRWATTR);
#endif
      }
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, ISymbol v, location_type l)
        : super_type (token_kind_type (tok), std::move (v), std::move (l))
#else
      symbol_type (int tok, const ISymbol& v, const location_type& l)
        : super_type (token_kind_type (tok), v, l)
#endif
      {
#if !defined _MSC_VER || defined __clang__
        YY_ASSERT ((token::TOK_ID <= tok && tok <= token::TOK_STRING));
#endif
      }
    };

    /// Build a parser object.
    parser (IParser& parseServices_yyarg);
    virtual ~parser ();

#if 201103L <= YY_CPLUSPLUS
    /// Non copyable.
    parser (const parser&) = delete;
    /// Non copyable.
    parser& operator= (const parser&) = delete;
#endif

    /// Parse.  An alias for parse ().
    /// \returns  0 iff parsing succeeded.
    int operator() ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

    /// The user-facing name of the symbol whose (internal) number is
    /// YYSYMBOL.  No bounds checking.
    static std::string symbol_name (symbol_kind_type yysymbol);

    // Implementation of make_symbol for each token kind.
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_END (location_type l)
      {
        return symbol_type (token::TOK_END, std::move (l));
      }
#else
      static
      symbol_type
      make_END (const location_type& l)
      {
        return symbol_type (token::TOK_END, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_YYerror (location_type l)
      {
        return symbol_type (token::TOK_YYerror, std::move (l));
      }
#else
      static
      symbol_type
      make_YYerror (const location_type& l)
      {
        return symbol_type (token::TOK_YYerror, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_YYUNDEF (location_type l)
      {
        return symbol_type (token::TOK_YYUNDEF, std::move (l));
      }
#else
      static
      symbol_type
      make_YYUNDEF (const location_type& l)
      {
        return symbol_type (token::TOK_YYUNDEF, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DOTS (location_type l)
      {
        return symbol_type (token::TOK_DOTS, std::move (l));
      }
#else
      static
      symbol_type
      make_DOTS (const location_type& l)
      {
        return symbol_type (token::TOK_DOTS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_ELLIPS (location_type l)
      {
        return symbol_type (token::TOK_ELLIPS, std::move (l));
      }
#else
      static
      symbol_type
      make_ELLIPS (const location_type& l)
      {
        return symbol_type (token::TOK_ELLIPS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_FWD (location_type l)
      {
        return symbol_type (token::TOK_FWD, std::move (l));
      }
#else
      static
      symbol_type
      make_FWD (const location_type& l)
      {
        return symbol_type (token::TOK_FWD, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_REFSET (location_type l)
      {
        return symbol_type (token::TOK_REFSET, std::move (l));
      }
#else
      static
      symbol_type
      make_REFSET (const location_type& l)
      {
        return symbol_type (token::TOK_REFSET, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_CREFSET (location_type l)
      {
        return symbol_type (token::TOK_CREFSET, std::move (l));
      }
#else
      static
      symbol_type
      make_CREFSET (const location_type& l)
      {
        return symbol_type (token::TOK_CREFSET, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EVAL (location_type l)
      {
        return symbol_type (token::TOK_EVAL, std::move (l));
      }
#else
      static
      symbol_type
      make_EVAL (const location_type& l)
      {
        return symbol_type (token::TOK_EVAL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_COMMA (location_type l)
      {
        return symbol_type (token::TOK_COMMA, std::move (l));
      }
#else
      static
      symbol_type
      make_COMMA (const location_type& l)
      {
        return symbol_type (token::TOK_COMMA, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BXOREQ (location_type l)
      {
        return symbol_type (token::TOK_BXOREQ, std::move (l));
      }
#else
      static
      symbol_type
      make_BXOREQ (const location_type& l)
      {
        return symbol_type (token::TOK_BXOREQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BOREQ (location_type l)
      {
        return symbol_type (token::TOK_BOREQ, std::move (l));
      }
#else
      static
      symbol_type
      make_BOREQ (const location_type& l)
      {
        return symbol_type (token::TOK_BOREQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BANDEQ (location_type l)
      {
        return symbol_type (token::TOK_BANDEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_BANDEQ (const location_type& l)
      {
        return symbol_type (token::TOK_BANDEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SHLEQ (location_type l)
      {
        return symbol_type (token::TOK_SHLEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_SHLEQ (const location_type& l)
      {
        return symbol_type (token::TOK_SHLEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SHREQ (location_type l)
      {
        return symbol_type (token::TOK_SHREQ, std::move (l));
      }
#else
      static
      symbol_type
      make_SHREQ (const location_type& l)
      {
        return symbol_type (token::TOK_SHREQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PLUSEQ (location_type l)
      {
        return symbol_type (token::TOK_PLUSEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_PLUSEQ (const location_type& l)
      {
        return symbol_type (token::TOK_PLUSEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MINUSEQ (location_type l)
      {
        return symbol_type (token::TOK_MINUSEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_MINUSEQ (const location_type& l)
      {
        return symbol_type (token::TOK_MINUSEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MODEQ (location_type l)
      {
        return symbol_type (token::TOK_MODEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_MODEQ (const location_type& l)
      {
        return symbol_type (token::TOK_MODEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DIVEQ (location_type l)
      {
        return symbol_type (token::TOK_DIVEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_DIVEQ (const location_type& l)
      {
        return symbol_type (token::TOK_DIVEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MULEQ (location_type l)
      {
        return symbol_type (token::TOK_MULEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_MULEQ (const location_type& l)
      {
        return symbol_type (token::TOK_MULEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_COPY (location_type l)
      {
        return symbol_type (token::TOK_COPY, std::move (l));
      }
#else
      static
      symbol_type
      make_COPY (const location_type& l)
      {
        return symbol_type (token::TOK_COPY, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_CCOPY (location_type l)
      {
        return symbol_type (token::TOK_CCOPY, std::move (l));
      }
#else
      static
      symbol_type
      make_CCOPY (const location_type& l)
      {
        return symbol_type (token::TOK_CCOPY, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_QMARK (location_type l)
      {
        return symbol_type (token::TOK_QMARK, std::move (l));
      }
#else
      static
      symbol_type
      make_QMARK (const location_type& l)
      {
        return symbol_type (token::TOK_QMARK, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_OR (location_type l)
      {
        return symbol_type (token::TOK_OR, std::move (l));
      }
#else
      static
      symbol_type
      make_OR (const location_type& l)
      {
        return symbol_type (token::TOK_OR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_XOR (location_type l)
      {
        return symbol_type (token::TOK_XOR, std::move (l));
      }
#else
      static
      symbol_type
      make_XOR (const location_type& l)
      {
        return symbol_type (token::TOK_XOR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_AND (location_type l)
      {
        return symbol_type (token::TOK_AND, std::move (l));
      }
#else
      static
      symbol_type
      make_AND (const location_type& l)
      {
        return symbol_type (token::TOK_AND, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BOR (location_type l)
      {
        return symbol_type (token::TOK_BOR, std::move (l));
      }
#else
      static
      symbol_type
      make_BOR (const location_type& l)
      {
        return symbol_type (token::TOK_BOR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BXOR (location_type l)
      {
        return symbol_type (token::TOK_BXOR, std::move (l));
      }
#else
      static
      symbol_type
      make_BXOR (const location_type& l)
      {
        return symbol_type (token::TOK_BXOR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BAND (location_type l)
      {
        return symbol_type (token::TOK_BAND, std::move (l));
      }
#else
      static
      symbol_type
      make_BAND (const location_type& l)
      {
        return symbol_type (token::TOK_BAND, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DIF (location_type l)
      {
        return symbol_type (token::TOK_DIF, std::move (l));
      }
#else
      static
      symbol_type
      make_DIF (const location_type& l)
      {
        return symbol_type (token::TOK_DIF, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EQ (location_type l)
      {
        return symbol_type (token::TOK_EQ, std::move (l));
      }
#else
      static
      symbol_type
      make_EQ (const location_type& l)
      {
        return symbol_type (token::TOK_EQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_GE (location_type l)
      {
        return symbol_type (token::TOK_GE, std::move (l));
      }
#else
      static
      symbol_type
      make_GE (const location_type& l)
      {
        return symbol_type (token::TOK_GE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_GT (location_type l)
      {
        return symbol_type (token::TOK_GT, std::move (l));
      }
#else
      static
      symbol_type
      make_GT (const location_type& l)
      {
        return symbol_type (token::TOK_GT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LE (location_type l)
      {
        return symbol_type (token::TOK_LE, std::move (l));
      }
#else
      static
      symbol_type
      make_LE (const location_type& l)
      {
        return symbol_type (token::TOK_LE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LT (location_type l)
      {
        return symbol_type (token::TOK_LT, std::move (l));
      }
#else
      static
      symbol_type
      make_LT (const location_type& l)
      {
        return symbol_type (token::TOK_LT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SHR (location_type l)
      {
        return symbol_type (token::TOK_SHR, std::move (l));
      }
#else
      static
      symbol_type
      make_SHR (const location_type& l)
      {
        return symbol_type (token::TOK_SHR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SHL (location_type l)
      {
        return symbol_type (token::TOK_SHL, std::move (l));
      }
#else
      static
      symbol_type
      make_SHL (const location_type& l)
      {
        return symbol_type (token::TOK_SHL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PLUS (location_type l)
      {
        return symbol_type (token::TOK_PLUS, std::move (l));
      }
#else
      static
      symbol_type
      make_PLUS (const location_type& l)
      {
        return symbol_type (token::TOK_PLUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MINUS (location_type l)
      {
        return symbol_type (token::TOK_MINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_MINUS (const location_type& l)
      {
        return symbol_type (token::TOK_MINUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MUL (location_type l)
      {
        return symbol_type (token::TOK_MUL, std::move (l));
      }
#else
      static
      symbol_type
      make_MUL (const location_type& l)
      {
        return symbol_type (token::TOK_MUL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DIV (location_type l)
      {
        return symbol_type (token::TOK_DIV, std::move (l));
      }
#else
      static
      symbol_type
      make_DIV (const location_type& l)
      {
        return symbol_type (token::TOK_DIV, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MOD (location_type l)
      {
        return symbol_type (token::TOK_MOD, std::move (l));
      }
#else
      static
      symbol_type
      make_MOD (const location_type& l)
      {
        return symbol_type (token::TOK_MOD, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EXP (location_type l)
      {
        return symbol_type (token::TOK_EXP, std::move (l));
      }
#else
      static
      symbol_type
      make_EXP (const location_type& l)
      {
        return symbol_type (token::TOK_EXP, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_UPLUS (location_type l)
      {
        return symbol_type (token::TOK_UPLUS, std::move (l));
      }
#else
      static
      symbol_type
      make_UPLUS (const location_type& l)
      {
        return symbol_type (token::TOK_UPLUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_UMINUS (location_type l)
      {
        return symbol_type (token::TOK_UMINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_UMINUS (const location_type& l)
      {
        return symbol_type (token::TOK_UMINUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BNOT (location_type l)
      {
        return symbol_type (token::TOK_BNOT, std::move (l));
      }
#else
      static
      symbol_type
      make_BNOT (const location_type& l)
      {
        return symbol_type (token::TOK_BNOT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LNOT (location_type l)
      {
        return symbol_type (token::TOK_LNOT, std::move (l));
      }
#else
      static
      symbol_type
      make_LNOT (const location_type& l)
      {
        return symbol_type (token::TOK_LNOT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DOT (location_type l)
      {
        return symbol_type (token::TOK_DOT, std::move (l));
      }
#else
      static
      symbol_type
      make_DOT (const location_type& l)
      {
        return symbol_type (token::TOK_DOT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RPLUSPLUS (location_type l)
      {
        return symbol_type (token::TOK_RPLUSPLUS, std::move (l));
      }
#else
      static
      symbol_type
      make_RPLUSPLUS (const location_type& l)
      {
        return symbol_type (token::TOK_RPLUSPLUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RMINUSMINUS (location_type l)
      {
        return symbol_type (token::TOK_RMINUSMINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_RMINUSMINUS (const location_type& l)
      {
        return symbol_type (token::TOK_RMINUSMINUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PLUSPLUS (location_type l)
      {
        return symbol_type (token::TOK_PLUSPLUS, std::move (l));
      }
#else
      static
      symbol_type
      make_PLUSPLUS (const location_type& l)
      {
        return symbol_type (token::TOK_PLUSPLUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MINUSMINUS (location_type l)
      {
        return symbol_type (token::TOK_MINUSMINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_MINUSMINUS (const location_type& l)
      {
        return symbol_type (token::TOK_MINUSMINUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_XOREQ (location_type l)
      {
        return symbol_type (token::TOK_XOREQ, std::move (l));
      }
#else
      static
      symbol_type
      make_XOREQ (const location_type& l)
      {
        return symbol_type (token::TOK_XOREQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_ANDEQ (location_type l)
      {
        return symbol_type (token::TOK_ANDEQ, std::move (l));
      }
#else
      static
      symbol_type
      make_ANDEQ (const location_type& l)
      {
        return symbol_type (token::TOK_ANDEQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_OREQ (location_type l)
      {
        return symbol_type (token::TOK_OREQ, std::move (l));
      }
#else
      static
      symbol_type
      make_OREQ (const location_type& l)
      {
        return symbol_type (token::TOK_OREQ, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LP (location_type l)
      {
        return symbol_type (token::TOK_LP, std::move (l));
      }
#else
      static
      symbol_type
      make_LP (const location_type& l)
      {
        return symbol_type (token::TOK_LP, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RP (location_type l)
      {
        return symbol_type (token::TOK_RP, std::move (l));
      }
#else
      static
      symbol_type
      make_RP (const location_type& l)
      {
        return symbol_type (token::TOK_RP, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LB (location_type l)
      {
        return symbol_type (token::TOK_LB, std::move (l));
      }
#else
      static
      symbol_type
      make_LB (const location_type& l)
      {
        return symbol_type (token::TOK_LB, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RB (location_type l)
      {
        return symbol_type (token::TOK_RB, std::move (l));
      }
#else
      static
      symbol_type
      make_RB (const location_type& l)
      {
        return symbol_type (token::TOK_RB, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LCB (location_type l)
      {
        return symbol_type (token::TOK_LCB, std::move (l));
      }
#else
      static
      symbol_type
      make_LCB (const location_type& l)
      {
        return symbol_type (token::TOK_LCB, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RCB (location_type l)
      {
        return symbol_type (token::TOK_RCB, std::move (l));
      }
#else
      static
      symbol_type
      make_RCB (const location_type& l)
      {
        return symbol_type (token::TOK_RCB, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RTI (location_type l)
      {
        return symbol_type (token::TOK_RTI, std::move (l));
      }
#else
      static
      symbol_type
      make_RTI (const location_type& l)
      {
        return symbol_type (token::TOK_RTI, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_COL (location_type l)
      {
        return symbol_type (token::TOK_COL, std::move (l));
      }
#else
      static
      symbol_type
      make_COL (const location_type& l)
      {
        return symbol_type (token::TOK_COL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DCOL (location_type l)
      {
        return symbol_type (token::TOK_DCOL, std::move (l));
      }
#else
      static
      symbol_type
      make_DCOL (const location_type& l)
      {
        return symbol_type (token::TOK_DCOL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SEMI (location_type l)
      {
        return symbol_type (token::TOK_SEMI, std::move (l));
      }
#else
      static
      symbol_type
      make_SEMI (const location_type& l)
      {
        return symbol_type (token::TOK_SEMI, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_ID (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_ID, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_ID (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_ID, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SELID (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_SELID, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_SELID (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_SELID, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_ULONG (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_ULONG, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_ULONG (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_ULONG, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LONG (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_LONG, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_LONG (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_LONG, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_UINT (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_UINT, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_UINT (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_UINT, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_INT (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_INT, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_INT (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_INT, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SHORT (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_SHORT, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_SHORT (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_SHORT, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_CHAR (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_CHAR, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_CHAR (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_CHAR, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_BOOL (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_BOOL, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_BOOL (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_BOOL, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_FLT (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_FLT, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_FLT (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_FLT, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DBL (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_DBL, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_DBL (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_DBL, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_STRING (ISymbol v, location_type l)
      {
        return symbol_type (token::TOK_STRING, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_STRING (const ISymbol& v, const location_type& l)
      {
        return symbol_type (token::TOK_STRING, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_STRWATTR (IStringWithAttrs v, location_type l)
      {
        return symbol_type (token::TOK_STRWATTR, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_STRWATTR (const IStringWithAttrs& v, const location_type& l)
      {
        return symbol_type (token::TOK_STRWATTR, v, l);
      }
#endif


    class context
    {
    public:
      context (const parser& yyparser, const symbol_type& yyla);
      const symbol_type& lookahead () const YY_NOEXCEPT { return yyla_; }
      symbol_kind_type token () const YY_NOEXCEPT { return yyla_.kind (); }
      const location_type& location () const YY_NOEXCEPT { return yyla_.location; }

      /// Put in YYARG at most YYARGN of the expected tokens, and return the
      /// number of tokens stored in YYARG.  If YYARG is null, return the
      /// number of expected tokens (guaranteed to be less than YYNTOKENS).
      int expected_tokens (symbol_kind_type yyarg[], int yyargn) const;

    private:
      const parser& yyparser_;
      const symbol_type& yyla_;
    };

  private:
#if YY_CPLUSPLUS < 201103L
    /// Non copyable.
    parser (const parser&);
    /// Non copyable.
    parser& operator= (const parser&);
#endif


    /// Stored state numbers (used for stacks).
    typedef unsigned char state_type;

    /// The arguments of the error message.
    int yy_syntax_error_arguments_ (const context& yyctx,
                                    symbol_kind_type yyarg[], int yyargn) const;

    /// Generate an error message.
    /// \param yyctx     the context in which the error occurred.
    virtual std::string yysyntax_error_ (const context& yyctx) const;
    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    static state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue) YY_NOEXCEPT;

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue) YY_NOEXCEPT;

    static const signed char yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token kind \a t to a symbol kind.
    /// In theory \a t should be a token_kind_type, but character literals
    /// are valid, yet not members of the token_kind_type enum.
    static symbol_kind_type yytranslate_ (int t) YY_NOEXCEPT;

    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    static std::string yytnamerr_ (const char *yystr);

    /// For a symbol, its name in clear.
    static const char* const yytname_[];


    // Tables.
    // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
    // STATE-NUM.
    static const short yypact_[];

    // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
    // Performed when YYTABLE does not specify something else to do.  Zero
    // means the default is an error.
    static const signed char yydefact_[];

    // YYPGOTO[NTERM-NUM].
    static const short yypgoto_[];

    // YYDEFGOTO[NTERM-NUM].
    static const unsigned char yydefgoto_[];

    // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
    // positive, shift that token.  If negative, reduce the rule whose
    // number is the opposite.  If YYTABLE_NINF, syntax error.
    static const short yytable_[];

    static const short yycheck_[];

    // YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
    // state STATE-NUM.
    static const signed char yystos_[];

    // YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.
    static const signed char yyr1_[];

    // YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.
    static const signed char yyr2_[];


#if YYDEBUG
    // YYRLINE[YYN] -- Source line where rule number YYN was defined.
    static const short yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r) const;
    /// Print the state stack on the debug stream.
    virtual void yy_stack_print_ () const;

    /// Debugging level.
    int yydebug_;
    /// Debug stream.
    std::ostream* yycdebug_;

    /// \brief Display a symbol kind, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state () YY_NOEXCEPT;

      /// The symbol kind as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s) YY_NOEXCEPT;

      /// Copy constructor.
      by_state (const by_state& that) YY_NOEXCEPT;

      /// Record that this symbol is empty.
      void clear () YY_NOEXCEPT;

      /// Steal the symbol kind from \a that.
      void move (by_state& that);

      /// The symbol kind (corresponding to \a state).
      /// \a symbol_kind::S_YYEMPTY when empty.
      symbol_kind_type kind () const YY_NOEXCEPT;

      /// The state number used to denote an empty symbol.
      /// We use the initial state, as it does not have a value.
      enum { empty_state = 0 };

      /// The state.
      /// \a empty when empty.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Move or copy construction.
      stack_symbol_type (YY_RVREF (stack_symbol_type) that);
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, YY_MOVE_REF (symbol_type) sym);
#if YY_CPLUSPLUS < 201103L
      /// Assignment, needed by push_back by some old implementations.
      /// Moves the contents of that.
      stack_symbol_type& operator= (stack_symbol_type& that);

      /// Assignment, needed by push_back by other implementations.
      /// Needed by some other old implementations.
      stack_symbol_type& operator= (const stack_symbol_type& that);
#endif
    };

    /// A stack with random access from its top.
    template <typename T, typename S = std::vector<T> >
    class stack
    {
    public:
      // Hide our reversed order.
      typedef typename S::iterator iterator;
      typedef typename S::const_iterator const_iterator;
      typedef typename S::size_type size_type;
      typedef typename std::ptrdiff_t index_type;

      stack (size_type n = 200) YY_NOEXCEPT
        : seq_ (n)
      {}

#if 201103L <= YY_CPLUSPLUS
      /// Non copyable.
      stack (const stack&) = delete;
      /// Non copyable.
      stack& operator= (const stack&) = delete;
#endif

      /// Random access.
      ///
      /// Index 0 returns the topmost element.
      const T&
      operator[] (index_type i) const
      {
        return seq_[size_type (size () - 1 - i)];
      }

      /// Random access.
      ///
      /// Index 0 returns the topmost element.
      T&
      operator[] (index_type i)
      {
        return seq_[size_type (size () - 1 - i)];
      }

      /// Steal the contents of \a t.
      ///
      /// Close to move-semantics.
      void
      push (YY_MOVE_REF (T) t)
      {
        seq_.push_back (T ());
        operator[] (0).move (t);
      }

      /// Pop elements from the stack.
      void
      pop (std::ptrdiff_t n = 1) YY_NOEXCEPT
      {
        for (; 0 < n; --n)
          seq_.pop_back ();
      }

      /// Pop all elements from the stack.
      void
      clear () YY_NOEXCEPT
      {
        seq_.clear ();
      }

      /// Number of elements on the stack.
      index_type
      size () const YY_NOEXCEPT
      {
        return index_type (seq_.size ());
      }

      /// Iterator on top of the stack (going downwards).
      const_iterator
      begin () const YY_NOEXCEPT
      {
        return seq_.begin ();
      }

      /// Bottom of the stack.
      const_iterator
      end () const YY_NOEXCEPT
      {
        return seq_.end ();
      }

      /// Present a slice of the top of a stack.
      class slice
      {
      public:
        slice (const stack& stack, index_type range) YY_NOEXCEPT
          : stack_ (stack)
          , range_ (range)
        {}

        const T&
        operator[] (index_type i) const
        {
          return stack_[range_ - i];
        }

      private:
        const stack& stack_;
        index_type range_;
      };

    private:
#if YY_CPLUSPLUS < 201103L
      /// Non copyable.
      stack (const stack&);
      /// Non copyable.
      stack& operator= (const stack&);
#endif
      /// The wrapped container.
      S seq_;
    };


    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param sym  the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, YY_MOVE_REF (stack_symbol_type) sym);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a sym.value is stolen.
    void yypush_ (const char* m, state_type s, YY_MOVE_REF (symbol_type) sym);

    /// Pop \a n symbols from the stack.
    void yypop_ (int n = 1) YY_NOEXCEPT;

    /// Constants.
    enum
    {
      yylast_ = 1409,     ///< Last index in yytable_.
      yynnts_ = 17,  ///< Number of nonterminal symbols.
      yyfinal_ = 65 ///< Termination state number.
    };


    // User arguments.
    IParser& parseServices;

  };

  inline
  parser::symbol_kind_type
  parser::yytranslate_ (int t) YY_NOEXCEPT
  {
    // YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to
    // TOKEN-NUM as returned by yylex.
    static
    const signed char
    translate_table[] =
    {
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77
    };
    // Last valid token kind.
    const int code_max = 332;

    if (t <= 0)
      return symbol_kind::S_YYEOF;
    else if (t <= code_max)
      return static_cast <symbol_kind_type> (translate_table[t]);
    else
      return symbol_kind::S_YYUNDEF;
  }

  // basic_symbol.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (const basic_symbol& that)
    : Base (that)
    , value ()
    , location (that.location)
  {
    switch (this->kind ())
    {
      case symbol_kind::S_block: // block
        value.copy< IBlock > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.copy< IItem > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.copy< IReceivable > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.copy< IStringWithAttrs > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.copy< ISymbol > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.copy< IUnit > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.copy< std::vector<IUnit> > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

  }




  template <typename Base>
  parser::symbol_kind_type
  parser::basic_symbol<Base>::type_get () const YY_NOEXCEPT
  {
    return this->kind ();
  }


  template <typename Base>
  bool
  parser::basic_symbol<Base>::empty () const YY_NOEXCEPT
  {
    return this->kind () == symbol_kind::S_YYEMPTY;
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move (s);
    switch (this->kind ())
    {
      case symbol_kind::S_block: // block
        value.move< IBlock > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.move< IItem > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.move< IReceivable > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.move< IStringWithAttrs > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.move< ISymbol > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.move< IUnit > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.move< std::vector<IUnit> > (YY_MOVE (s.value));
        break;

      default:
        break;
    }

    location = YY_MOVE (s.location);
  }

  // by_kind.
  inline
  parser::by_kind::by_kind () YY_NOEXCEPT
    : kind_ (symbol_kind::S_YYEMPTY)
  {}

#if 201103L <= YY_CPLUSPLUS
  inline
  parser::by_kind::by_kind (by_kind&& that) YY_NOEXCEPT
    : kind_ (that.kind_)
  {
    that.clear ();
  }
#endif

  inline
  parser::by_kind::by_kind (const by_kind& that) YY_NOEXCEPT
    : kind_ (that.kind_)
  {}

  inline
  parser::by_kind::by_kind (token_kind_type t) YY_NOEXCEPT
    : kind_ (yytranslate_ (t))
  {}



  inline
  void
  parser::by_kind::clear () YY_NOEXCEPT
  {
    kind_ = symbol_kind::S_YYEMPTY;
  }

  inline
  void
  parser::by_kind::move (by_kind& that)
  {
    kind_ = that.kind_;
    that.clear ();
  }

  inline
  parser::symbol_kind_type
  parser::by_kind::kind () const YY_NOEXCEPT
  {
    return kind_;
  }


  inline
  parser::symbol_kind_type
  parser::by_kind::type_get () const YY_NOEXCEPT
  {
    return this->kind ();
  }


} // yy
#line 2922 "include/iparser.hpp"




#endif // !YY_YY_INCLUDE_IPARSER_HPP_INCLUDED
