#ifndef __LSTACK_HPP__
#define __LSTACK_HPP__

#include <forward_list>

namespace i1 {

template<typename t>
class stack : public std::forward_list<t> {
  using size_type = typename std::forward_list<t>::size_type;
  using L = typename std::forward_list<t>;

public:
  stack() {}

  template<typename... Args>
  inline void emplace(Args&&... args)
  { L::emplace_front(std::forward<Args>(args)...); }

  inline void push(const t& item)
  { L::push_front(item); }

  inline void push(t&& item)
  { L::push_front(std::move(item)); }

  inline void pop() {L::pop_front();}
  inline typename L::reference top() {return L::front();}
  inline typename L::const_reference top() const {return L::front();}
};

} // namespace i1

#endif // __LSTACK_HPP__
