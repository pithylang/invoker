#ifndef __TREE_HPP__
#define __TREE_HPP__

#if USE_DOUBLE_LIST_TREE
# include <list>
#else // !USE_DOUBLE_LIST_TREE
# include <forward_list>
#endif // USE_DOUBLE_LIST_TREE
#include <functional>

namespace i1 {

#if USE_DOUBLE_LIST_TREE
# define FOR_EACH_BRANCH(e)
    for (auto e : branches_)
#else // !USE_DOUBLE_LIST_TREE
# define FOR_EACH_BRANCH(e) \
    std::forward_list<nodeptr_t> v; \
    for (auto e : branches_) v.push_front(e); \
    for (auto e : v)
#endif // USE_DOUBLE_LIST_TREE

template<typename t>
class Tree;

/**
 *  \brief Tree node
 *
 *  \details TreeNode represents a \c Tree node encapsulating
 *  data type \c t.
 *  \par \c __Act is a structure to be used in various iteration
 *  methods. Its interface is like \c StdAction.
 */
template<typename t>
class TreeNode {
  typedef TreeNode<t> node_t;
  typedef TreeNode<t>* nodeptr_t;
  typedef Tree<t> tree_t;

#if USE_DOUBLE_LIST_TREE
  std::list<nodeptr_t> branches_;
#else // !USE_DOUBLE_LIST_TREE
  std::forward_list<nodeptr_t> branches_;
#endif // USE_DOUBLE_LIST_TREE
  node_t* parent_;
  t data_;

public:
  TreeNode() : parent_(nullptr) {}
  TreeNode(const t& data) : parent_(nullptr), data_(data) {}
  TreeNode(t&& data) : parent_(nullptr), data_(std::move(data)) {}
  ~TreeNode() {}

  static node_t* create() {return new node_t;}

  inline node_t* parent() const noexcept {return parent_;}
  inline node_t* setParent(node_t* p) noexcept {parent_ = p; return this;}
  inline bool isEndNode() const noexcept {return branches_.empty();}
  inline bool terminal() const noexcept {return branches_.empty();}
  inline node_t* link(node_t* node)
#if USE_DOUBLE_LIST_TREE
  { branches_.push_back(node->setParent(this)); return node; }
#else // !USE_DOUBLE_LIST_TREE
  { branches_.push_front(node->setParent(this)); return node; }
#endif // USE_DOUBLE_LIST_TREE
  inline node_t* link(const t& data) {return link(new node_t(data));}
  inline node_t* link(t&& data) {return link(new node_t(std::move(data)));}

  node_t* relink(node_t* thisNode) {
    auto& branches = thisNode->parent()->branches_;
#if USE_DOUBLE_LIST_TREE
    branches.erase(std::find(branches.begin(), branches.end(), thisNode));
#else // !USE_DOUBLE_LIST_TREE
    if (branches.front() == thisNode) {
      branches.pop_front();
    }
    else {
      for (auto it = branches.begin(); it != branches.end(); ++it) {
        auto current = it;
        if (*++current == thisNode) {
          branches.erase_after(it);
          break;
        }
      }
    }
#endif // USE_DOUBLE_LIST_TREE
    return link(thisNode);
  }

  inline t& data() noexcept {return data_;}
  inline const t& data() const noexcept {return data_;}

#if USE_DOUBLE_LIST_TREE
  inline size_t size() const {return branches_.size();}
  inline std::list<nodeptr_t>& branches() noexcept {return branches_;}
#else // !USE_DOUBLE_LIST_TREE
  inline size_t size() const
  { size_t i = 0; for (const auto& _ : branches_) ++i; return i; }
  inline std::forward_list<nodeptr_t>& branches() noexcept {return branches_;}
#endif // USE_DOUBLE_LIST_TREE
  inline bool empty() const {return branches_.empty();}

  node_t* deleteBranches() {
    for (node_t* branch : branches_)
      delete branch->deleteBranches();
    branches_.clear();
    return this;
  }

  template<typename __Act>
  void walkDown(__Act& a) {
    a(this);
    FOR_EACH_BRANCH(e)
      e->walkDown(a);
  }

  template<typename __Act>
  void walkUp(__Act& a) {
    FOR_EACH_BRANCH(e)
      e->walkUp(a);
    a(this);
  }

  template<typename __Act>
  node_t* walkDownWhile(__Act& a) {
    if (!a(this)) return this;
    node_t* r;
    FOR_EACH_BRANCH(e)
      if ((r = e->walkDownWhile(a)) != nullptr) return r;
    return nullptr;
  }

  template<typename __Act>
  node_t* walkDownUntil(__Act& a) {
    if (a(this)) return this;
    node_t* r;
    FOR_EACH_BRANCH(e)
      if ((r = e->walkDownUntil(a)) != nullptr) return r;
    return nullptr;
  }

  template<typename __Act>
  node_t* walkDownUntilNR(__Act& a) {
    if (a(this)) return this;
    node_t* r;
    for (auto e : branches_)
      if ((r = e->walkDownUntil(a)) != nullptr) return r;
    return nullptr;
  }

  template<typename __Act>
  void walkDownEnterExit(__Act& a) {
    a(this);
    if (isEndNode()) return;
    a.enter(this);
    FOR_EACH_BRANCH(e)
      e->walkDownEnterExit(a);
    a.exit(this);
  }

  template<typename __Act>
  void populate(__Act& a) {
    a(this);
    while (a.more()) link(a.data());
    FOR_EACH_BRANCH(e)
      e->populate(a);
  }

  void walkDown(std::function<void(t&)> a) {
    a(this->data_);
    FOR_EACH_BRANCH(e)
      e->walkDown(a);
  }

  void walkUp(std::function<void(t&)> a) {
    FOR_EACH_BRANCH(e)
      e->walkUp(a);
    a(this->data_);
  }

  node_t* walkDownWhile(std::function<bool(t&)> a) {
    if (!a(this->data_)) return this;
    FOR_EACH_BRANCH(e)
      if (node_t* r = e->walkDownWhile(a); r != nullptr) return r;
    return nullptr;
  }

  node_t* walkDownUntil(std::function<bool(t&)> a) {
    if (a(this->data_)) return this;
    FOR_EACH_BRANCH(e)
      if (node_t* r = e->walkDownUntil(a); r != nullptr) return r;
    return nullptr;
  }

  node_t* walkDownUntilNR(std::function<bool(t&)> a) {
    if (a(this->data_)) return this;
    for (auto e : branches_)
      if (node_t* r = e->walkDownUntil(a); r != nullptr) return r;
    return nullptr;
  }
};

/**
 *  \brief Simple multibranch tree
 *
 *  \details \c Tree is a simple multibranch tree representation.
 *  Its nodes are type \c t and there are no special requirements
 *  on them.
 *  Depending on the configuration parameter \c USE_DOUBLE_LIST_TREE
 *  the tree may use a double list for its branches. The default
 *  is a \c forward_list.
 *  \par \c __Act is a structure to be used in various iteration
 *  methods. Its interface is like \c StdAction.
 */
template<typename t>
class Tree {
  typedef Tree<t> tree_t;
  typedef TreeNode<t> node_t;

  node_t* root_;

public:
  Tree() : root_{nullptr} {}
  Tree(Tree&& tree) : root_{nullptr} {std::swap(root_, tree.root_);}
  ~Tree() {clear();}

  bool empty() const noexcept {return root_ == nullptr;}
  node_t* setRoot(const t& data)
  { return root_ = new node_t(data); }
  node_t* setRoot(t&& data)
  { return root_ = new node_t(std::move(data)); }
  node_t* getRoot() noexcept {return root_;}
  const node_t* getRoot() const noexcept {return root_;}

  void clear() {
    if (!root_) return;
    delete root_->deleteBranches();
    root_ = nullptr;
  }

  template<typename __Act>
  void walkDown(__Act& a) {if (root_) root_->walkDown(a);}
  template<typename __Act>
  void walkUp(__Act& a) {if (root_) root_->walkUp(a);}

  template<typename __Act>
  node_t* walkDownWhile(__Act& a)
  { return root_ ? root_->walkDownWhile(a) : nullptr; }

  template<typename __Act>
  node_t* walkDownUntil(__Act& a)
  { return root_ ? root_->walkDownUntil(a) : nullptr; }

  template<typename __Act>
  node_t* walkDownUntilNR(__Act& a)
  { return root_ ? root_->walkDownUntilNR(a) : nullptr; }

  template<typename __Act>
  void populate(const t& rootValue, __Act& a) {
    root_ = new node_t(rootValue);
    root_->populate(a);
  }

  void walkDown(std::function<void(t&)> a) {if (root_) root_->walkDown(a);}
  void walkUp(std::function<void(t&)> a) {if (root_) root_->walkUp(a);}
  node_t* walkDownWhile(std::function<bool(t&)> a)
  { return root_ ? root_->walkDownWhile(a) : nullptr; }
  node_t* walkDownUntil(std::function<bool(t&)> a)
  { return root_ ? root_->walkDownUntil(a) : nullptr; }
  node_t* walkDownUntilNR(std::function<bool(t&)> a)
  { return root_ ? root_->walkDownUntilNR(a) : nullptr; }

  struct FindNodeAction {
    const t& value;
    FindNodeAction(const t& theValue) : value(theValue) {}
    bool operator()(node_t* p) const noexcept {return p->data() == value;}
  };

  inline node_t* getNode(const t& value) {
    FindNodeAction findNode(value);
    return walkDownUntilNR(findNode);
  }

  inline node_t* getNode(const t& value, bool) {
    return walkDownUntilNR([&value](t& x) {return x == value;});
  }

  inline node_t* relink(const t& value, node_t* here) {
    node_t* thisNode = getNode(value);
    return thisNode ? here->relink(thisNode) : nullptr;
  }

  inline node_t* relink(const t& value, const t& tgtValue) {
    node_t* here = getNode(tgtValue);
    return here ? relink(value, here) : nullptr;
  }
};

/**
 *  \brief Action interface skeleton
 *
 *  \details \c StdAction is a skeleton \em action interface
 *  containing empty definitions after which one can model real
 *  actions and conditions.
 */
template<typename t>
struct StdAction {
  typedef TreeNode<t> node_t;
  typedef Tree<t> tree_t;

  /**
   *  \brief Pointer to owner \c Tree structure
   *  \details Optional member. A pointer to the tree object is
   *  not always necessary.
   */
  tree_t *tree_;
  /**
   *  \brief Current data
   *  \details Optional member. It is necessary when the action
   *  is used to populate the tree.
   */
  t tmpData_;

  /**
   *  \brief Constructor
   *  \details The constructor's interface is arbitrary.
   */
  StdAction(tree_t* tree) : tree_(tree) {}
  /**
   *  \brief Action to be performed iteratively
   *  \details Necessary for actions.
   */
  void operator()(node_t*) {/*do nothing at all*/;}
  /**
   *  \brief Condition to be performed iteratively
   *  \details Necessary for conditions (while, until). If the
   *  return value is false (true for \em until iterations) the
   *  iteration is stopped.
   */
  bool operator()(node_t*) const {return false;}
  /**
   *  Node data accessor
   */
  t& data() {return tmpData_;}
  /**
   *  \brief Action to be performed iteratively in enter/exit loops
   *  \details Optional member. Necessary for enter/exit loops.
   */
  void enter(node_t*) {}
  /**
   *  \brief Action to be performed iteratively in enter/exit loops
   *  \details Optional member. Necessary for enter/exit loops.
   */
  void exit(node_t*) {}
};

#undef FOR_EACH_BRANCH

} // namespace i1

#endif // __TREE_HPP__
