#ifndef __OALGO_HPP__
#define __OALGO_HPP__

#include "ortmgr.hpp"
#include "otypeProxy.hpp"

struct OBasicLoop {
  ORet run(OArg* argv, Session* m) noexcept;
  virtual void action(OVar, Session*) noexcept {}
  virtual ORet getResult(Session*) noexcept { return OVar::undefinedResult(); }
};

namespace om {
  ORet runLoop(Session* m, OArg* argv,
               const std::function<void(OVar)>& loopAction,
               const std::function<ORet()>& resultGetter = []{
                 return OVar::undefinedResult();
               }) noexcept;
}

using getmsg_t = std::function<std::pair<std::string, std::string>()>;

template<typename t>
  struct ValueExtractor {
    t extractValue(OVar result, Session* m) noexcept {
      return g_castValue<t>(result.objectRef, m);
    }
  };

template<>
  struct ValueExtractor<ORef> {
    const bool containerOwned;
    getmsg_t getNames;
    ORef extractValue(OVar result, Session* m) noexcept;
  };

#endif // __OALGO_HPP__
