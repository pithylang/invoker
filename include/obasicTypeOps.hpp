#ifndef __OBASICTYPEOPS_HPP__
#define __OBASICTYPEOPS_HPP__

#include <string>
#include <cmath>
#include <cfloat>
#include "selectors.h"

#if __cplusplus < 201703L // C++11, 14

  namespace std {
    template<class T, class U>
    constexpr bool is_same_v = is_same<T, U>::value;
    template<class T>
    constexpr bool is_pointer_v = is_pointer<T>::value;
    template<class T>
    constexpr bool is_integral_v = is_integral<T>::value;
    template<class From, class To>
    constexpr bool is_convertible_v = is_convertible<From, To>::value;
    template<class T>
    constexpr bool is_arithmetic_v = is_arithmetic<T>::value;
  }
# define inline_
# define constexpr_

#else // C++17

# define inline_ inline
# define constexpr_ constexpr

#endif // __cplusplus

namespace ops {

template<typename A, typename B>
concept NoPointer = (!std::is_pointer_v<A> && !std::is_pointer_v<B>);

template<typename A, typename B>
concept Assignment = requires(A a, B b) { a = b; };

//::::::::::::::::::::::::::   Utility Functions   ::::::::::::::::::::::::::://

template<typename T>
inline bool normalDivisor(const T& x) {
  return std::fpclassify(x) == FP_NORMAL;
}

template<class P, class Q>
requires (!(std::is_integral_v<P> && std::is_integral_v<Q>))
inline constexpr auto __mod(P a, Q b) -> decltype(std::fmod((P)1, (Q)1)) {
  return std::fmod(a, b);
}

template<class P, class Q>
requires (std::is_integral_v<P> && std::is_integral_v<Q>)
inline constexpr auto __mod(P a, Q b) -> decltype((P)1 % (Q)1) {
  return a % b;
}


//============================================================================//
//::::::::::::::::::::::::   Unavailable Operation   ::::::::::::::::::::::::://
//============================================================================//

struct UnavailableOperation {};

bool uop(const auto& a) noexcept {
  return std::is_same_v<std::remove_const_t<decltype(a)>, UnavailableOperation>;
}

template<typename T>
constexpr bool uop() noexcept {
  return std::is_same_v<std::remove_const_t<T>, UnavailableOperation>;
}

//============================================================================//
//::::::::::::::::::::::::::   Binary Operations   ::::::::::::::::::::::::::://
//============================================================================//

template<typename OP>
struct Operation {

  template<typename A, typename B>
  static auto apply(const A& a, const B& b) -> decltype(OP::operation(a, b)) {
    return OP::operation(a, b);
  }

  template<typename A, typename B>
  static auto copy(A& a, const B& b) -> decltype(OP::operation(a, b)) {
    return OP::operation(a, b);
  }
};

//::::::::::::::::::::::::::   Generating Macros   ::::::::::::::::::::::::::://
#define BEGIN_BOP(name) \
struct name {                                                             \
  template<typename A, typename B>                                        \
  static UnavailableOperation operation(const A& a, const B& b) {         \
    return UnavailableOperation{};                                        \
  }

#define END_BOP(opname, methname) \
  static const std::string name() noexcept {return #opname;} \
  static const std::string methodName() noexcept {return methname;} \
};

#define GENERATE_BOP(name, opsign, method) \
  BEGIN_BOP(name) \
    template<typename A, typename B>                                        \
    requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)               \
    static auto operation(const A& a, const B& b) -> decltype(a opsign b) { \
      return a opsign b;                                                    \
    }                                                                       \
  END_BOP(opsign, method)

#define GENERATE_CPY(name, opsign, method) \
  struct name {                                                             \
    template<typename A, typename B>                                        \
    static UnavailableOperation operation(const A& a, const B& b) {         \
      return UnavailableOperation{};                                        \
    }                                                                       \
    template<typename A, typename B>                                        \
    requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)               \
    static int operation(A& a, const B& b) {                                \
      a opsign b;                                                           \
      return 0;                                                             \
    }                                                                       \
  END_BOP(opsign, method)

#define GENERATE_CMP(name, opsign, method) \
  struct name {                                                     \
    template<typename A, typename B>                                \
    static UnavailableOperation operation(const A& a, const B& b) { \
      return UnavailableOperation{};                                \
    }                                                               \
    template<typename A, typename B>                                \
    requires (std::is_pointer_v<A> == std::is_pointer_v<B>)         \
    static bool operation(const A& a, const B& b) {                 \
      return a opsign b;                                            \
    }                                                               \
  END_BOP(opsign, method)

#define GENERATE_LOP(name, opsign, method) \
  struct name {                                                     \
    template<typename A, typename B>                                \
    static UnavailableOperation operation(const A& a, const B& b) { \
      return UnavailableOperation{};                                \
    }                                                               \
    template<typename A, typename B>                                \
    requires (std::is_convertible_v<A, bool> &&                     \
              std::is_convertible_v<B, bool>)                       \
    static bool operation(const A& a, const B& b) {                 \
      return (bool)a opsign (bool)b;                                \
    }                                                               \
  END_BOP(opsign, method)

#define GENERATE_BITOP(name, opsign, method) \
  struct name {                                                             \
    template<typename A, typename B>                                        \
    static UnavailableOperation operation(const A& a, const B& b) {         \
      return UnavailableOperation{};                                        \
    }                                                                       \
    template<typename A, typename B>                                        \
    requires (std::is_integral_v<A> && std::is_integral_v<B>)               \
    static auto operation(const A& a, const B& b) -> decltype(a opsign b) { \
      return a opsign b;                                                    \
    }                                                                       \
  END_BOP(opsign, method)

#define GENERATE_BCPY(name, opsign, method) \
  struct name {                                                             \
    template<typename A, typename B>                                        \
    static UnavailableOperation operation(const A& a, const B& b) {         \
      return UnavailableOperation{};                                        \
    }                                                                       \
    template<typename A, typename B>                                        \
    requires (std::is_integral_v<A> && std::is_integral_v<B>)               \
    static int operation(A& a, const B& b) {                                \
      a opsign b;                                                           \
      return 0;                                                             \
    }                                                                       \
  END_BOP(opsign, method)

//::::::::::::::::::::::::::   Binary Operations   ::::::::::::::::::::::::::://
BEGIN_BOP(Nop)
END_BOP(Nop, "")

GENERATE_BOP(Add, +, S_add);
// GENERATE_BOP(Sub, -);
struct Sub {
  static UnavailableOperation operation(const auto& a, const auto& b) {
    return UnavailableOperation{};
  }
  static auto operation(const auto& a, const auto& b) -> decltype(-a + b)
  requires (!std::is_pointer_v<decltype(a)> &&
            !std::is_pointer_v<decltype(b)>) {return a - b;}
  static const std::string name() noexcept {return "-";}
  static const std::string methodName() noexcept {return S_sub;}
};
GENERATE_BOP(Mul, *, S_mul);
BEGIN_BOP(Div)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)
  static auto operation(const A& a, const B& b) -> decltype(a / b) {
    if (!normalDivisor(b)) throw std::string{"abnormal division"};
    return a / b;
  }
END_BOP(/, S_div)
BEGIN_BOP(Mod)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)
  static auto operation(const A& a, const B& b) -> decltype(__mod(a, b)) {
    if (!normalDivisor(b)) throw std::string{"abnormal division"};
    return __mod(a, b);
  }
END_BOP(%, S_mod)
BEGIN_BOP(Exp)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)
  static auto operation(const A& a, const B& b) -> decltype(std::pow(a, b)) {
    return pow(a, b);
  }
END_BOP(**, S_exp)

//:::::::::::::::::::::::::::   Copy Operations   :::::::::::::::::::::::::::://
struct Ass {
  static UnavailableOperation operation(const auto& a, const auto& b) {
    return UnavailableOperation{};
  }
  static int operation(auto& a, const auto& b)
  requires Assignment<decltype(a), decltype(b)> { a = b; return 0; }
  static const std::string name() noexcept {return "=";}
  static const std::string methodName() noexcept {return S_value;}
};
GENERATE_CPY(Incby, +=, S_incBy);
GENERATE_CPY(Decby, -=, S_decBy);
GENERATE_CPY(Mulby, *=, S_mulBy);
BEGIN_BOP(Divby)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)
  static int operation(A& a, const B& b) {
    if (!normalDivisor(b)) throw std::string{"abnormal division"};
    a /= b;
    return 0;
  }
END_BOP(/=, S_divBy)
BEGIN_BOP(Modby)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B>)
  static int operation(A& a, const B& b) {
    if (!normalDivisor(b)) throw std::string{"abnormal division"};
    a = __mod(a, b);
    return 0;
  }
END_BOP(%=, S_modBy)
BEGIN_BOP(Expby)
  template<typename A, typename B>
  requires (!std::is_pointer_v<A> && !std::is_pointer_v<B> &&
            !std::is_same_v<A,bool>)
  static int operation(A& a, const B& b) {
    a = pow(a, b);
    return 0;
  }
END_BOP(**=, S_expBy)

//::::::::::::::::::::::::   Comparison Operations   ::::::::::::::::::::::::://
GENERATE_CMP(Eq, ==, S_eq);
GENERATE_CMP(Ne, !=, S_ne);
GENERATE_CMP(Lt,  <, S_lt);
GENERATE_CMP(Gt,  >, S_gt);
GENERATE_CMP(Le, <=, S_le);
GENERATE_CMP(Ge, >=, S_ge);

//::::::::::::::::::::::::::   Logical Operations   :::::::::::::::::::::::::://
GENERATE_LOP(And, &&, S__and);
GENERATE_LOP(Or,  ||, S__or);
GENERATE_LOP(Xor, !=, S__xor);

//::::::::::::::::::::::::::::   Bit Operations   :::::::::::::::::::::::::::://
GENERATE_BITOP(Band, &, S_bitand);
GENERATE_BITOP(Bor,  |, S_bitor);
GENERATE_BITOP(Bxor, ^, S_bitxor);
GENERATE_BITOP(Shl, <<, S_shiftl);
GENERATE_BITOP(Shr, >>, S_shiftr);

//:::::::::::::::::::::::::   Bit Copy Operations   :::::::::::::::::::::::::://
GENERATE_BCPY(Bandby, &=, S_bitandBy);
GENERATE_BCPY(Borby,  |=, S_bitorBy);
GENERATE_BCPY(Bxorby, ^=, S_bitxorBy);
GENERATE_BCPY(Shlby, <<=, S_shiftlBy);
GENERATE_BCPY(Shrby, >>=, S_shiftrBy);

template<typename OP>
concept IsCopy = (std::is_same_v<OP, Ass> ||
                  std::is_same_v<OP, Incby> ||
                  std::is_same_v<OP, Decby> ||
                  std::is_same_v<OP, Mulby> ||
                  std::is_same_v<OP, Divby> ||
                  std::is_same_v<OP, Modby> ||
                  std::is_same_v<OP, Expby> ||
                  std::is_same_v<OP, Bandby> ||
                  std::is_same_v<OP, Borby>  ||
                  std::is_same_v<OP, Bxorby> ||
                  std::is_same_v<OP, Shlby>  ||
                  std::is_same_v<OP, Shrby>);
} // namespace ops

#endif // __OBASICTYPEOPS_HPP__
