#ifndef __OCLASSOBJ_HPP__
#define __OCLASSOBJ_HPP__

#include "object.hpp"
#include "odev.h"

struct ClassObject {
  OClass* classPointer;
  int addProperty(Session* m, const std::string& name, const std::string& type,
      unsigned short pl);
  ORef isNull(Session* m) const;
  ORef valid(Session* m) const;
};

_BEGIN_CLASS_DECLARATION(ClassObject)
  ORet _genericAddTemplate(const std::string& methodName,
                           const std::string& propName,
                           ushort flags, Session* m);
  ORet _genericAddRef     (const std::string& methodName,
                           const std::string& propName,
                           ushort flags, Session* m);
  ORet _genericAddMethod  (const std::string& methodName,
                           const std::string& proto,
                           ushort flags, int bid, Session* m);
  _METHOD (ommAddProperty);
  _METHOD (ommAddPublicProperty);
  _METHOD (ommAddProtectedProperty);
  _METHOD (ommAddPrivateProperty);
  _METHOD (ommAddPublicTemplate);
  _METHOD (ommAddProtectedTemplate);
  _METHOD (ommAddPrivateTemplate);
  _METHOD (ommAddRefAttr);
  _METHOD (ommAddPublicRef);
  _METHOD (ommAddProtectedRef);
  _METHOD (ommAddPrivateRef);
  _METHOD (ommAddMethod);
  _METHOD (ommAddPublicMethod);
  _METHOD (ommAddProtectedMethod);
  _METHOD (ommAddPrivateMethod);
  /**
   *  @brief Add object cache
   *  @details This method enables classes for which the
   *  default cache creation has been deactivated, such as
   *  abstract classes, to create an object cache. Yet, one
   *  can apply it to define an object cache when no cache
   *  configuration from a data file is involved.
   */
  _METHOD (ommAddObjectCache);
  _METHOD (ommCastBool);
  _METHOD (ommGetPrintout);
  _METHOD (ommPrint);
  _METHOD (ommIsNull);
  _METHOD (ommValid);
  _METHOD (ommIsTemplate);
  _METHOD (ommIsMutable);
  _METHOD (ommAlias);
  _METHOD (ommIsAlias);
  _METHOD (ommIsAliasOf);
_END_CLASS_DECLARATION()

_DECLARE_MCLASS(ClassObject, "class object")

_DECLARE_CLASS_INSTALLER(ClassObject)

#endif // __OCLASSOBJ_HPP__
