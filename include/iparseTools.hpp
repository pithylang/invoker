#ifndef __IPARSETOOLS_HPP__
#define __IPARSETOOLS_HPP__

#include <string>
#include <algorithm>
#include "itypes.hpp"
#include "iparser.hpp"
#include "location.hpp"

enum ParseOptions : ulong {
  PO_NONE                                  = 0x00000000,
  PO_INLINE_CONDITIONAL_OPERATOR           = 0x00000001,
  PO_TESTING_MODE                          = 0x00000002,
#if 0
  PO_COMPILE_FOR_DEBUGGING                 = 0x00000002,
  PO_ENABLE_RUNTIME_DIAGNOSTICS            = 0x00000004,
  PO_PERIODS_IN_VA_METHOD_NAMES            = 0x00000008,
  PO_AUTO_BREAK_SWITCH_CASES               = 0x00000010,
  PO_UNDECLARED_SYMBOLS_MEMBERS            = 0x00000020,
  PO_PREFIXED_UNDECLARED_SYMBOLS_MEMBERS   = 0x00000040,
  PO_SHORT_FILE_NAMES                      = 0x00000080,
  PO_WINDOWS_APPLICATION                   = 0x00000100,
  PO_ENUMS_ARE_GLOBAL_VARS                 = 0x00008000,
  PO_TARGET_CLASS_LIBRARY                  = 0x00010000,
#endif // 0
  PO_DEFAULT                               = PO_INLINE_CONDITIONAL_OPERATOR,
  PO_PARSE_OPTIONS_END                     = 0xffffffff
};

// TODO remove
#define _OX_LINK

yy::parser::symbol_type yylex(IParser&);

struct VarCreateMethod {
  std::string name;
  int creationArg;
  bool usableAsArg;
};

class _OX_LINK IParser {
  friend yy::parser::symbol_type yylex(IParser&);
  using strvector = std::vector<std::string>;
  using VCM = VarCreateMethod;

  int                  lineNumber_;
  const char*          script;
  char*                cursor_;

public:
  ulong                parseOptions;
  std::string          sourceFile;
  yy::location         location;
  std::string          systemName;
  char                 systemSymbol;
  strvector            classCreateMethods;
  std::vector<VCM>     varCreateMethods;
  std::vector<IUnit>   parsedProgram;

public:
  IParser(const char *theScript, ulong flags = PO_DEFAULT);
  ~IParser();

  int parseProgram();

  int getLineNumber() const noexcept {return lineNumber_;}
  bool isClassDeclaration(const std::string& method) const noexcept {
    const auto end = classCreateMethods.end();
    return std::find(classCreateMethods.begin(), end, method) != end;
  }

  const VCM* getVarCreateMethod(const std::string& methodName) const;

  bool inlineConditionalOperator() const noexcept
  { return (parseOptions & PO_INLINE_CONDITIONAL_OPERATOR) != 0; }
  bool testingMode() const noexcept
  { return (parseOptions & PO_TESTING_MODE) != 0; }

protected:
  size_t getColumn() const;

  // Lexer
  yy::parser::symbol_type lex();

  // Lexer primitives
  inline bool eof() const noexcept {return *cursor_ == 0;}
  inline void moveForward(int n) noexcept {cursor_ += n; location.columns(n);}
  inline void advance() noexcept {++cursor_; location.columns(1);}
  inline void advance(int& length) noexcept {advance(); ++length;}
  inline void advanceLine() noexcept
  { ++cursor_; ++lineNumber_; location.lines(1); }

  void  skipBlank();
  char  lookAhead() const noexcept {return cursor_[1];}
  char* lookAheadSkip(int c);
  bool  parseComment();
  std::pair<int, bool> translateChar();
  char* parseStringAttrs(size_t& attrs);

  yy::parser::symbol_type parseNumber    ();
  yy::parser::symbol_type parseHexa      ();
  yy::parser::symbol_type parseBinary    ();
  yy::parser::symbol_type parseCharConst ();

  yy::parser::symbol_type parseString    ();
  yy::parser::symbol_type parseId        ();
  yy::parser::symbol_type parseExtendedId();
  yy::parser::symbol_type lexPlus   ();
  yy::parser::symbol_type lexMinus  ();
  yy::parser::symbol_type lexMul    ();
  yy::parser::symbol_type lexDiv    ();
  yy::parser::symbol_type lexMod    ();
  yy::parser::symbol_type lexEq     ();
  yy::parser::symbol_type lexGt     ();
  yy::parser::symbol_type lexLt     ();
  yy::parser::symbol_type lexVBar   ();
  yy::parser::symbol_type lexAnd    ();
  yy::parser::symbol_type lexCarret ();
  yy::parser::symbol_type lexColon  ();
};

#endif //__IPARSETOOLS_HPP__
