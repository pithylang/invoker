#ifndef __IDEFS_H__
#define __IDEFS_H__

#ifdef __DEBUG__
# ifndef ASSERT
#   define ASSERT(x) if(!(x)) { \
        throw "Assertion " #x " failed in " __FILE__ ", line "+ \
            std::to_string(__LINE__)+"."; \
      }
# endif
# ifndef I_ASSERT
#   define I_ASSERT(x,m) \
    if(!(x)) { \
      m->displayMessage("Error", "Assertion failed in " __FILE__ ", line "\
          +std::to_string(__LINE__)+"."); \
      exit(2); \
    }
# endif
# ifndef I_DEBUG
#   define I_DEBUG(statements) statements
# endif
#else
# ifndef ASSERT
#   define ASSERT(x)
# endif
# ifndef I_ASSERT
#   define I_ASSERT(x,m)
# endif
# ifndef I_DEBUG
#   define I_DEBUG(statements)
# endif
#endif /*__DEBUG__*/

#ifndef _ASSERT
# define  _ASSERT(x,m) I_ASSERT(x,m)
#endif
#ifndef _DEBUG
# define  _DEBUG(stm) I_DEBUG(stm)
#endif

namespace i1 {
  template<typename t>
  class vector;
}

struct MBlock;

/**
 *  \class CompilePackageData
 */
struct CompilePackageData {
  i1::vector<MBlock>& blockTable; // in
  unsigned long parseOptions;     // in
  bool generateAssembly;          // in
  bool testingMode;               // in
  bool verbose;                   // in
  std::string ignoreParseExcept;  // in
  /** Package compilation result */
  int rc;                         // out
  std::string sourceCode;         // out
  std::string assemblyCode;       // out
  std::string message;            // out
};

int g_compilePackage(const std::string&, CompilePackageData&) noexcept;
std::unique_ptr<char[]> g_readScript(const std::string& fileName);

/** Package compilation results */
enum : int {
  COMPILE_PACKAGE_OK = 0,
  COMPILE_PACKAGE_PARSER,
  COMPILE_PACKAGE_CODE_GEN,
  COMPILE_PACKAGE_READ_FILE,
  COMPILE_PACKAGE_SOURCES,
  COMPILE_PACKAGE_CMD_LINE,
  COMPILE_PACKAGE_CODE_GEN_FAIL,
  COMPILE_PACKAGE_INIT_RT_OBJ,
};

#endif // __IDEFS_H__
