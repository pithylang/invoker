#ifndef __OSESSIONLOCALDATA_HPP__
#define __OSESSIONLOCALDATA_HPP__

#include <forward_list>
#include <any>

namespace om {

struct SessionLocalData : std::forward_list<std::any> {
  using container_type = std::forward_list<std::any>;

  SessionLocalData() = default;

  size_type size() const noexcept {
    size_type n = 0;
    for (const auto& i : *this) ++n;
    return n;
  }

  template<typename T, typename... Args>
    std::any& emplace(Args&&... args)
    { return emplace_front(std::in_place_type<T>, std::forward<Args>(args)...); }

  void push(const std::any& item) { container_type::push_front(item); }
  void push(std::any&& item) { container_type::push_front(std::move(item)); }

  void pop() { container_type::pop_front(); }

  container_type::iterator before(const std::any& item) {
    for (auto it = before_begin(); true; ++it) {
      auto current = it;
      if (++current == end()) return current;
      if (&*current == &item) return it;
    }
    return end();
  }

  void erase(const std::any& item) {
    if (auto it = before(item); it != end())
      erase_after(it);
  }
};

} // om

#endif // __OSESSIONLOCALDATA_HPP__
