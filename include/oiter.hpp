#ifndef __OITER_HPP__
#define __OITER_HPP__

#include "object.hpp"

//============================================================================//
//::::::::::::::::::::::::::::::   OIterator   ::::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Basic iterator class
 *  @details This is an abstract class. Its scope is to register
 *  the _BasicIterator_ requirement and define the interface for
 *  all basic iterator classes. Script extensions must define the
 *  methods `value`, `inc` and `swap:`.
 */
class OIterator : public OInstance {
public:
  OIterator(OClass* c) : OInstance{c} {}
  OIterator& operator=(const OIterator& r) = default;

protected:
  _METHOD(ommSwap);
  _METHOD(ommGetValue);
  _METHOD(inc);

  _DECLARE_METHOD_TABLE();
};

class OIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  OIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  OIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override { return sizeof(OIterator); }
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual ORef constructCore(Session* m) override
  { return genericConstructCoreAbstract<OIterator>(CLSID_ITER, m); }
  virtual void destructCore(OInstance *self, Session*) override
  { genericDestructCoreAbstract<OIterator>(CLSID_ITER, self); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override
  { return self; }
  virtual ORef moveCore(ORef self, ORef) const override
  { return self; }

  _ASSOCIATE_MCLASS_WITH(OIterator)
};

//::::::::::::::::::::::::::::   OCountIterator   :::::::::::::::::::::::::::://

/**
 *  @brief Count iterator
 *  @details Generates a half-unlimited sequence of integers, by
 *  default starting from `0`. The current value is `count_` and
 *  can be reset with the methods `value:` or `begin:`. When the
 *  method `inc` is called the counter `count_` is incremented by
 *  `step_`; its default value is `1`. To change `step_` use
 *  `step:` or `begin:step:`. To get the current value use the
 *  dereferencing method `value`.
 *  @par In scripts, count iterator instances have the identifier
 *  `count iterator` and satisfy the _InputIterator_ requirement.
 */
class OCountIterator : public OInstance {
  using difference_t = std::ptrdiff_t;
  difference_t count_;
  difference_t step_;

public:
  OCountIterator(OClass* c) : OInstance{c}, count_{0}, step_{1} {}
  OCountIterator& operator=(const OCountIterator& r) = default;

  void setBegin(difference_t begin) noexcept { count_ = begin; }
  void setStep(difference_t step) noexcept { step_ = step; }
  inline difference_t getCount() const noexcept { return count_; }
  inline void advance() noexcept { count_ += step_; }

  _METHOD(ommGetValue);
protected:
  _METHOD(ommSwap);
  _METHOD(ommStep);
  _METHOD(ommBeginStep);
  _METHOD(eq);
  _METHOD(ne);
  _METHOD(inc);

  _DECLARE_METHOD_TABLE();
};

/**
 *  @brief Count iterator metaclass
 */
class OCountIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OCountIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  OCountIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  OCountIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override { return sizeof(OCountIterator); }
  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<OCountIterator>(ou); }
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<OCountIterator>(m); }
  virtual void destructCore(OInstance *o, Session*) override
  { if (!freeCached<OCountIterator>(o)) delete static_cast<OCountIterator*>(o); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override;
  virtual ORef moveCore(ORef self, ORef) const override;

  _ASSOCIATE_MCLASS_WITH(OCountIterator)
};

//============================================================================//
//::::::::::::::::::::::::::::   OInputIterator   :::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Input iterator
 *  @details This is an abstract class. Its scope is to register
 *  the _InputIterator_ requirement and define the respective
 *  interface. Script extensions must define the methods `eq:`
 *  and `component:` (`operator[]`) in addition to the methods
 *  required by _BasicIterator_. They need not define the `neq:`
 *  method if they define `eq:`.
 */
class OInputIterator : public OInstance {
public:
  OInputIterator(OClass* c) : OInstance{c} {}
  OInputIterator& operator=(const OInputIterator& r) = default;

protected:
  _METHOD(ommSwap);
  _METHOD(ommGetValue);
  _METHOD(ommComponent);
  _METHOD(inc);
  _METHOD(eq);
  _METHOD(ne);

  _DECLARE_METHOD_TABLE();
};

class OInputIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OInputIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  OInputIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  OInputIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override {return sizeof(OInputIterator);}
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual ORef constructCore(Session* m) override
  { return genericConstructCoreAbstract<OInputIterator>(CLSID_INPITER, m); }
  virtual void destructCore(OInstance *self, Session*) override
  { genericDestructCoreAbstract<OInputIterator>(CLSID_INPITER, self); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override
  { return self; }
  virtual ORef moveCore(ORef self, ORef) const override
  { return self; }

  _ASSOCIATE_MCLASS_WITH(OInputIterator)
};

//============================================================================//
//:::::::::::::::::::::::::::   OForwardIterator   ::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Forward iterator
 *  @details This is an abstract class. Its scope is to register
 *  the _ForwardIterator_ requirement and define the respective
 *  interface. Script extensions must define the method `postinc`
 *  in addition to the methods required by _InputIterator_.
 */
class OForwardIterator : public OInstance {
public:
  OForwardIterator(OClass* c) : OInstance{c} {}
  OForwardIterator& operator=(const OForwardIterator& r) = default;

protected:
  _METHOD(ommSwap);
  _METHOD(ommGetValue);
  _METHOD(ommComponent);
  _METHOD(inc);
  _METHOD(postinc);
  _METHOD(eq);
  _METHOD(ne);

  _DECLARE_METHOD_TABLE();
};

class OForwardIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OForwardIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  OForwardIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  OForwardIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override {return sizeof(OForwardIterator);}
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual ORef constructCore(Session* m) override
  { return genericConstructCoreAbstract<OForwardIterator>(CLSID_FWDITER, m); }
  virtual void destructCore(OInstance *self, Session*) override
  { genericDestructCoreAbstract<OForwardIterator>(CLSID_FWDITER, self); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override
  { return self; }
  virtual ORef moveCore(ORef self, ORef) const override
  { return self; }

  _ASSOCIATE_MCLASS_WITH(OForwardIterator)
};

//============================================================================//
//::::::::::::::::::::::::   OBidirectionalIterator   :::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Bidirectional iterator
 *  @details This is an abstract class. Its scope is to register
 *  the _BidirectionalIterator_ requirement and define the respective
 *  interface. Script extensions must define the methods `dec` and
 *  `postdec` in addition to the methods required by _ForwardIterator_.
 */
class OBidirectionalIterator : public OInstance {
public:
  OBidirectionalIterator(OClass* c) : OInstance{c} {}
  OBidirectionalIterator& operator=(const OBidirectionalIterator& r) = default;

protected:
  _METHOD(ommSwap);
  _METHOD(ommGetValue);
  _METHOD(ommComponent);
  _METHOD(inc);
  _METHOD(dec);
  _METHOD(postinc);
  _METHOD(postdec);
  _METHOD(eq);
  _METHOD(ne);

  _DECLARE_METHOD_TABLE();
};

class OBidirectionalIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OBidirectionalIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  OBidirectionalIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  OBidirectionalIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(OBidirectionalIterator); }
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual ORef constructCore(Session* m) override
  { return genericConstructCoreAbstract<OBidirectionalIterator>(CLSID_BIDITER, m); }
  virtual void destructCore(OInstance *self, Session*) override
  { genericDestructCoreAbstract<OBidirectionalIterator>(CLSID_BIDITER, self); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override
  { return self; }
  virtual ORef moveCore(ORef self, ORef) const override
  { return self; }

  _ASSOCIATE_MCLASS_WITH(OBidirectionalIterator)
};

//============================================================================//
//::::::::::::::::::::::::   ORandomAccessIterator   ::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Random Access iterator
 *  @details This is an abstract class. Its scope is to register
 *  the _RandomAccessIterator_ requirement and define the
 *  respective interface. Script extensions must define the methods
 *  - `incBy:`
 *  - `decBy:`
 *  - `add: difference_t`
 *  - `sub: RandomAccessIterator`
 *  - `sub: difference_t`
 *  - All comparison operators
 *  in addition to the methods required by _BidirectionalIterator_.
 *  @par The comparison operators need not be implemented if they
 *  can be inferred from `sub: RandomAccessIterator`. Also, `add:`
 *  can be inferred from `sub: difference_t` and `decBy:` from
 *  `incBy:`.
 *  @par The method `sub: RandomAccessIterator` must return a
 *  `difference_t` type and `sub: difference_t` must return a
 *  `RandomAccessIterator` type.
 */
class ORandomAccessIterator : public OInstance {
  using difference_t = std::ptrdiff_t;

public:
  ORandomAccessIterator(OClass* c) : OInstance{c} {}
  ORandomAccessIterator& operator=(const ORandomAccessIterator& r) = default;

protected:
  _METHOD(ommSwap);
  _METHOD(ommGetValue);
  _METHOD(ommComponent);
  _METHOD(add);
  _METHOD(sub);
  _METHOD(incBy);
  _METHOD(decBy);
  _METHOD(inc);
  _METHOD(dec);
  _METHOD(postinc);
  _METHOD(postdec);
  _METHOD(eq);
  _METHOD(ne);
  _METHOD(gt);
  _METHOD(lt);
  _METHOD(ge);
  _METHOD(le);

  _DECLARE_METHOD_TABLE();
};

class ORandomAccessIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  ORandomAccessIterator_Class(Session* m, const std::string& className);
  /** Constructor for script-derived classes */
  ORandomAccessIterator_Class(Session *m, const std::string& baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}
  /** Constructor for mutable classes */
  ORandomAccessIterator_Class(Session *m, const OClass* baseClass,
      const std::string& className) : OClass(m, baseClass, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(ORandomAccessIterator); }
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual ORef constructCore(Session* m) override
  { return genericConstructCoreAbstract<ORandomAccessIterator>(CLSID_RAITER, m); }
  virtual void destructCore(OInstance *self, Session*) override
  { genericDestructCoreAbstract<ORandomAccessIterator>(CLSID_RAITER, self); }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override
  { return self; }
  virtual ORef moveCore(ORef self, ORef) const override
  { return self; }

  _ASSOCIATE_MCLASS_WITH(ORandomAccessIterator)
};

#endif // __OITER_HPP__
