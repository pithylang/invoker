#ifndef __OPROTO_HPP__
#define __OPROTO_HPP__

#include <string>
#include <vector>

class ProtoParser {
  struct StringPair {
    std::string key;
    std::string value;
    StringPair(const std::string& key, const std::string& value) :
        key(key), value(value) {}
    StringPair(std::string&& key, std::string&& value) :
        key(std::move(key)), value(std::move(value)) {}
  };

  const std::string& proto;

public:
  std::vector<StringPair> definitions;
  std::string error;

  ProtoParser(const std::string& proto) : proto(proto) {}
  bool parse();
  bool invalidIdentifier() const noexcept {return !error.empty();}
  std::string errorMessage() const noexcept;
  bool noargs() const noexcept
  { return definitions.size() == 1 && definitions[0].value == "\x01"; }
  std::string getMethodName() const;
  std::vector<std::string> getArgNames() const;

private:
  struct FragmentTokens {std::vector<std::string> tokens; bool success;};
  FragmentTokens parseFragment(const std::string& fragment);
  static bool isTokenOK(const std::string& token);
  static void trim(std::string& s);
};

#endif // __OPROTO_HPP__
