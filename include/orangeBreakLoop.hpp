#ifndef __ORANGEBREAKLOOP_HPP__
#define __ORANGEBREAKLOOP_HPP__

#include "ortmgr.hpp"
#include "orange.hpp"
#include "oveciter.hpp"
#include "osetiter.hpp"

//============================================================================//
//:::::::::::::::::::::::::   NativeLoopTerminator   ::::::::::::::::::::::::://
//============================================================================//

#define VEC_ITER(t, o) \
    *static_cast<typename std::vector<t>::iterator*>(_PVecIter(t, o))
#define SET_ITER(t, o) \
    *static_cast<typename std::set<t>::iterator*>(_PSetIter(t, o))

template<typename t>
struct NativeLoopTerminator {
  ORange& range;
  const PipelineElement& pipelineElement;

  NativeLoopTerminator(ORange& range) :
      range{range}, pipelineElement{range.getPipelineElement(0)} {}

  bool operator()(const typename std::vector<t>::iterator& it) const noexcept {
    return it != VEC_ITER(t, pipelineElement.end);
  }

  bool operator()(const typename std::set<t>::iterator& it) const noexcept
      requires (!std::is_same_v<t, ORef>) {
    return it != SET_ITER(t, pipelineElement.end);
  }

  using osetiter_t = std::set<ORef, ORefSetCompare>::iterator;

  bool operator()(const osetiter_t& it) const noexcept {
    return it != *static_cast<osetiter_t*>(_PSetIter(ORef, pipelineElement.end));
  }

  bool applySentry(ORef value) const noexcept {
    OVar varValue{value};
    return pipelineElement.applySentry(range, &varValue);
  }

  template<typename _It>
  std::function<bool(ORef)> getContinue(_It& it) {
    switch (pipelineElement.type) {
      case ALL: return [this, &it](ORef) { return (*this)(it); };
      case ITERATOR: return [](ORef) { return true; };
      case ITERATOR_PAIR: return [this, &it](ORef) { return (*this)(it); };
      case ITERATOR_COUNT: return [this](ORef)
               { return range.iterationCounter < pipelineElement.size; };
      case ITERATOR_SENTRY: return [this](ORef value)
               { return applySentry(value); };
    }
    return [](ORef) { return true; };
  }
};

#undef SET_ITER
#undef VEC_ITER

//============================================================================//
//::::::::::::::::::::::::::::   LoopTerminator   :::::::::::::::::::::::::::://
//============================================================================//

struct LoopTerminator {
  ORange& range;
  const PipelineElement& pipelineElement;

  LoopTerminator(ORange& range) :
      range{range}, pipelineElement{range.getPipelineElement(0)} {}
  LoopTerminator(ORange& range, size_t stage) :
      range{range}, pipelineElement{range.getPipelineElement(stage)} {}

  bool operator()(ORef it) const noexcept;
  bool applySentry(ORef value) const noexcept;
  std::function<bool(ORef)> getContinue(ORef it) noexcept;
};


#endif // __ORANGEBREAKLOOP_HPP__