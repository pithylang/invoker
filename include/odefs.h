#ifndef __ODEFS_H__
#define __ODEFS_H__

#include "ovariable.hpp"
#include <string>

#define _NUM_BASE_CLASSES 128

// TODO
#define _OM_LINK

class OClass;
class OInstance;
class Session;
class MBlock;

//:::::::::::::::::::::::::::   Method Addresses   ::::::::::::::::::::::::::://

typedef OInstance* ORef;
typedef const OInstance* const_ORef;
typedef ORet (OInstance::*CoreMethodAddress)(OArg, int, OArg*, Session*);

struct ScriptMethodAddress {
  int block;
  inline bool valid() const noexcept {return block != -1;}
  inline bool eq(const ScriptMethodAddress& r) const noexcept
  { return block == r.block; }
};

struct OMethodAddress {
  union {
    ScriptMethodAddress scriptMethodAddress;
    CoreMethodAddress coreMethodAddress;
  };
  bool scriptMethod;

  OMethodAddress() : scriptMethod(true) {scriptMethodAddress.block = -1;}
  OMethodAddress& operator=(const OMethodAddress& a) {
    scriptMethod = a.scriptMethod;
    if (scriptMethod) scriptMethodAddress = a.scriptMethodAddress;
    else coreMethodAddress = a.coreMethodAddress;
    return *this;
  }
  inline bool valid() const noexcept
  { return scriptMethod ? scriptMethodAddress.valid()
                        : !(coreMethodAddress == CoreMethodAddress{0}); }
  inline bool notValid() const noexcept
  { return scriptMethod ? !scriptMethodAddress.valid()
                        : coreMethodAddress == CoreMethodAddress{0}; }
};

//::::::::::::::::::::::::::::   Method Tables   ::::::::::::::::::::::::::::://

struct OCoreMethodInfo {
  CoreMethodAddress f;
  unsigned int protect;
  const char *name;
};

struct oxCInheritanceData {
  oxCInheritanceData* base;
  OCoreMethodInfo*    table;
};

// Shortcuts
typedef ScriptMethodAddress SAddr;
typedef CoreMethodAddress CAddr;
typedef OMethodAddress OAddr;

//::::::::::::::::::::::::::   Common Definitions   :::::::::::::::::::::::::://

/** VirtualMachine execMethod flags */
enum : unsigned long {
  EMF_STD = 0,
  EMF_CONST_REC = 0x10,
  EMF_NO_THROW_UNTIL_EXEC = 0x40000000,
};

/** VirtualMachine execMethod results */
enum {
  EMR_OK = 0,
  EMR_METHOD_NOT_RESPONDING,
  EMR_METHOD_NOT_ACCESSIBLE,
};

/** Package export results */
enum : int {
  EXPORT_PACKAGE_OK = 0,
  EXPORT_PACKAGE_UNINIT_STATIC,
  EXPORT_PACKAGE_UNINIT_LOCAL,
  EXPORT_PACKAGE_REF,
  EXPORT_PACKAGE_VAR_NOT_FOUND,
};

#if 0 // TODO
// =============================================================================
// oxEventTable
// =============================================================================

enum {
   _RESOLVE_EVENT_TYPE,
   _RESOLVE_ID,
   _RESOLVE_LAST_ID,
   _RESOLVE_PARAM
};

// =============================================================================
// oxGetVariableData input and output flags
// =============================================================================

enum {
   _GET_DATA_VAR_READ   =  0x00000001,
   _GET_DATA_GLOBAL     =  0x00000002,
   _GET_DATA_LOCAL      =  0x00000004,
   _GET_DATA_STATIC     =  0x00000008,
   _GET_DATA_MEMBER     =  0x00000010,
   _GET_DATA_THIS       =  0x00000020,
   _GET_DATA_RES_AUTO   =  0x00000040,
   _GET_DATA_RES_NOAUTO =  0x00000080,
   _GET_DATA_RES_CONST  =  0x00000100,
   _GET_DATA_RES_REF    =  0x00000200,
   _GET_DATA_VAR_TYPE   =  0x00000400,
   _GET_DATA_INV_QUERY  =  0x00004000,
   _GET_DATA_INV_VM     =  0x00008000,
   _GET_DATA_VAR_EXISTS =  0x00010000,
   _GET_DATA_VAR_INIT   =  0x00020000,
   _GET_DATA_MODIFY     =  0x00040000
};
#endif // 0

#endif // __ODEFS_H__
