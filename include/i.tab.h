/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_SHREQ = 258,
     TOK_SHLEQ = 259,
     TOK_BANDEQ = 260,
     TOK_BOREQ = 261,
     TOK_BXOREQ = 262,
     TOK_MULEQ = 263,
     TOK_DIVEQ = 264,
     TOK_MODEQ = 265,
     TOK_MINUSEQ = 266,
     TOK_PLUSEQ = 267,
     TOK_OR = 268,
     TOK_AND = 269,
     TOK_DIF = 270,
     TOK_EQ = 271,
     TOK_GE = 272,
     TOK_LE = 273,
     TOK_SHR = 274,
     TOK_SHL = 275,
     TOK_EXP = 276,
     UPLUS = 277,
     UMINUS = 278,
     BNOT = 279,
     LNOT = 280,
     TOK_MINUSMINUS = 281,
     TOK_PLUSPLUS = 282,
     TOK_RMINUSMINUS = 283,
     TOK_RPLUSPLUS = 284,
     TOK_OREQ = 285,
     TOK_ANDEQ = 286,
     TOK_XOREQ = 287,
     TOK_THIS = 288,
     TOK_INFO = 289,
     TOK_EXTENDS = 290,
     TOK_CLS = 291,
     TOK_IFACE = 292,
     TOK_IMPLEM = 293,
     TOK_MTH = 294,
     TOK_NEW = 295,
     TOK_RESULT = 296,
     TOK_IF = 297,
     TOK_ELSEIF = 298,
     TOK_ELSE = 299,
     TOK_SWITCH = 300,
     TOK_CASE = 301,
     TOK_DO = 302,
     TOK_DEFAULT = 303,
     TOK_WHILE = 304,
     TOK_UNTIL = 305,
     TOK_BREAK = 306,
     TOK_FOR = 307,
     TOK_STEP = 308,
     TOK_CONTINUE = 309,
     TOK_GOTO = 310,
     TOK_LABEL = 311,
     TOK_SET = 312,
     TOK_XOR = 313,
     TOK_ID = 314,
     TOK_SELID = 315,
     TOK_INT = 316,
     TOK_FP = 317,
     TOK_STRING = 318,
     TOK_PUB = 319,
     TOK_PRIV = 320,
     TOK_PROT = 321,
     TOK_MEMB = 322,
     TOK_PROPERTY = 323,
     TOK_INTERNAL = 324,
     TOK_TRY = 325,
     TOK_CATCH = 326,
     TOK_EXCEPT = 327,
     TOK_DEL = 328,
     TOK_CONST = 329,
     TOK_STATIC = 330,
     TOK_GLOBAL = 331,
     TOK_CONGLOB = 332,
     TOK_CONNEW = 333,
     TOK_CONAUTO = 334,
     TOK_AUTO = 335,
     TOK_ENUM = 336,
     TOK_DEFINE = 337,
     TOK_EXTERN = 338,
     TOK_LRESULT = 339,
     TOK_CSET = 340,
     TOK_ATTRIB = 341,
     TOK_CLSACC = 342,
     TOK_INHERITED = 343
   };
#endif
#define TOK_SHREQ 258
#define TOK_SHLEQ 259
#define TOK_BANDEQ 260
#define TOK_BOREQ 261
#define TOK_BXOREQ 262
#define TOK_MULEQ 263
#define TOK_DIVEQ 264
#define TOK_MODEQ 265
#define TOK_MINUSEQ 266
#define TOK_PLUSEQ 267
#define TOK_OR 268
#define TOK_AND 269
#define TOK_DIF 270
#define TOK_EQ 271
#define TOK_GE 272
#define TOK_LE 273
#define TOK_SHR 274
#define TOK_SHL 275
#define TOK_EXP 276
#define UPLUS 277
#define UMINUS 278
#define BNOT 279
#define LNOT 280
#define TOK_MINUSMINUS 281
#define TOK_PLUSPLUS 282
#define TOK_RMINUSMINUS 283
#define TOK_RPLUSPLUS 284
#define TOK_OREQ 285
#define TOK_ANDEQ 286
#define TOK_XOREQ 287
#define TOK_THIS 288
#define TOK_INFO 289
#define TOK_EXTENDS 290
#define TOK_CLS 291
#define TOK_IFACE 292
#define TOK_IMPLEM 293
#define TOK_MTH 294
#define TOK_NEW 295
#define TOK_RESULT 296
#define TOK_IF 297
#define TOK_ELSEIF 298
#define TOK_ELSE 299
#define TOK_SWITCH 300
#define TOK_CASE 301
#define TOK_DO 302
#define TOK_DEFAULT 303
#define TOK_WHILE 304
#define TOK_UNTIL 305
#define TOK_BREAK 306
#define TOK_FOR 307
#define TOK_STEP 308
#define TOK_CONTINUE 309
#define TOK_GOTO 310
#define TOK_LABEL 311
#define TOK_SET 312
#define TOK_XOR 313
#define TOK_ID 314
#define TOK_SELID 315
#define TOK_INT 316
#define TOK_FP 317
#define TOK_STRING 318
#define TOK_PUB 319
#define TOK_PRIV 320
#define TOK_PROT 321
#define TOK_MEMB 322
#define TOK_PROPERTY 323
#define TOK_INTERNAL 324
#define TOK_TRY 325
#define TOK_CATCH 326
#define TOK_EXCEPT 327
#define TOK_DEL 328
#define TOK_CONST 329
#define TOK_STATIC 330
#define TOK_GLOBAL 331
#define TOK_CONGLOB 332
#define TOK_CONNEW 333
#define TOK_CONAUTO 334
#define TOK_AUTO 335
#define TOK_ENUM 336
#define TOK_DEFINE 337
#define TOK_EXTERN 338
#define TOK_LRESULT 339
#define TOK_CSET 340
#define TOK_ATTRIB 341
#define TOK_CLSACC 342
#define TOK_INHERITED 343




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)

typedef union YYSTYPE {
   int          int_type;
   ISymbol     *sym_type;
   IUnit       *unit_type;
   IBlock      *blk_type;
} YYSTYPE;
/* Line 1248 of yacc.c.  */

# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE yylloc;


