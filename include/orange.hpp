#ifndef __ORANGE_HPP__
#define __ORANGE_HPP__

#include <vector>
#include "object.hpp"
#include "ovector.hpp"
#include "oset.hpp"
#include "otypes.h"

//============================================================================//
//::::::::::::::::::::::::::::   Range Actions   ::::::::::::::::::::::::::::://
//============================================================================//

enum PipelineElementType {
  UNKNOWN,
  // initial range actions
  ALL,
  ITERATOR,
  ITERATOR_PAIR,
  ITERATOR_COUNT,
  ITERATOR_SENTRY,
  IOTA,
  NUM_INIT_PIPELINE_ELEM_TYPES,
  // noninitial range actions
  TAKE = NUM_INIT_PIPELINE_ELEM_TYPES,
  TAKE_WHILE,
  DROP,
  DROP_WHILE,
  FILTER,
  TRANSFORM,
  KEYS,
  VALUES,
  ELEMENTS,
  REVERSE,
  // multiple range actions
  MERGE,
  SPLIT,
  JOIN,
  // execute range
  EXECUTE,
  NUM_PIPELINE_ELEMENT_TYPES,
};

typedef PipelineElementType RangeActionType;

class ORange;

struct PipelineElement {
  PipelineElementType type;
  Session* m;
  OMethodAddress action;
  ORef begin, end;
  size_t size;
  PipelineElement(Session*, PipelineElementType = UNKNOWN);
  PipelineElement(Session*, PipelineElementType, const OMethodAddress& a);
  PipelineElement(Session*, PipelineElementType, int blockId);
  PipelineElement(Session*, PipelineElementType, ORef begin);
  PipelineElement(Session*, PipelineElementType, ORef begin, ORef end);
  void clear();
  void clearIterator();
  void clearIota();
  void clearSplit();
  void clearTake();
  void clearDrop();
  inline void setBlockId(int blockId) noexcept
  { action.scriptMethodAddress.block = blockId; }
  bool isInitial() const noexcept {
    return type == ALL || type == ITERATOR_PAIR ||
           type == ITERATOR_COUNT || type == ITERATOR_SENTRY;
  }
  /**
   *  @brief Pipeline element requires end notification
   *
   *  @details It is relevant for the `transform:`, `process:`
   *      `do:` and `run:` pipeline elements.
   */
  inline bool requiresNotification() const noexcept { return size != 0; }
  inline void requireNotification(bool b) noexcept { size = b ? 1 : 0; }
  void prepareLambda(); // TODO --> prepareSentry
  bool applySentry(ORange& range, OVar* value) const noexcept;
  ORet applyTransform(ORange& range, OVar* value) const noexcept;
};

typedef PipelineElement RangeAction;

//::::::::::::::::::::::::::::   Initial Ranges   :::::::::::::::::::::::::::://
//:::::::::::::::::::::::   Used first in pipelines   :::::::::::::::::::::::://

typedef PipelineElement RangeAll;

/**
 *  @brief Define semi-infinite range from begin iterator
 *
 *  @details Defines a range that starts from a begin iterator and
 *  ends nowhere. It is the responsibility of the pipeline to
 *  terminate the iteration.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  range from: beginIt; // range must be in initial state
 *  // shortcut: NO
 *  // pipeline: NO
 *  @endcode
 */
typedef PipelineElement RangeIterator;

/**
 *  @brief Define range from begin and end iterators
 *
 *  @details Defines a range from a begin and an end iterator.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  range from: beginIt to: endIt; // range must be in initial state
 *  // shortcut
 *  beginIt ... endIt;
 *  // pipeline: NO
 *  @endcode
 */
typedef PipelineElement RangeIteratorPair;

/**
 *  @brief Define range from iterator and number
 *
 *  @details Defines a range from a begin iterator and an iteration
 *  count.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  container from: beginIt count: n;
 *  // shortcut
 *  beginIt ... n;
 *  // pipeline
 *  container | [beginIt, n];
 *  @endcode
 */
typedef PipelineElement RangeIteratorCount;

/**
 *  @brief Define range from iterator and block (*lambda*)
 *
 *  @details Defines a range from a begin iterator and an end
 *  condition, called a *sentinel*.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  container from: beginIt to: {...};
 *  // shortcut
 *  beginIt ... {...};
 *  // pipeline
 *  container | [beginIt, {...}];
 *  @endcode
 */
typedef PipelineElement RangeIteratorSentinel;

typedef PipelineElement RangeIota;

struct RangeSingleItem : RangeAction {};
struct RangeEmpty : RangeAction {};
struct RangeSpan : RangeAction {};

//::::::::::::::::::::::::::   Noninitial Ranges   ::::::::::::::::::::::::::://
//::::::::::::::::   Used in conjunction with other Ranges   ::::::::::::::::://

/**
 *  @brief General count or size range
 *
 *  @details A generic range type that requires a size for its
 *  definition.
 */
typedef PipelineElement RangeSize;

/**
 *  @brief Take range
 *
 *  @details A range that arises from the first @c n elements of
 *  the source range.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange take: 3;
 *  // statement serialization
 *  container range => take: 3;
 *  // pipeline
 *  sourceRange | 3;
 *  @endcode
 */
typedef RangeSize RangeTake;

/**
 *  @brief Take-while range
 *
 *  @details A range that arises from the source range's leading
 *  elements matching the supplied code block ("lambda").
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange takeWhile: {...};
 *  // statement serialization
 *  container range => takeWhile: {...};
 *  // pipeline
 *  sourceRange | [true, {...}];
 *  @endcode
 */
typedef PipelineElement RangeTakeWhile;

/**
 *  @brief Drop elements range
 *
 *  @details A range that arises from the dropping of the first
 *  @c n elements of the source range.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange drop: 3;
 *  // statement serialization
 *  container range => drop: 3;
 *  // pipeline
 *  sourceRange | -3;
 *  @endcode
 */
typedef RangeSize RangeDrop;

/**
 *  @brief Drop-while range
 *
 *  @details A range that arises from the source range by dropping
 *  the leading elements that match the supplied code block
 *  ("lambda").
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange dropWhile: {...};
 *  // statement serialization
 *  container range => dropWhile: {...};
 *  // pipeline
 *  sourceRange | [false, {...}];
 *  @endcode
 */
typedef PipelineElement RangeDropWhile;

/**
 *  @brief Filter range
 *
 *  @details A range that arises from the filtering of
 *  elements matching the supplied code block ("lambda").
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange filter: {...};
 *  // statement serialization
 *  container range => filter: {...};
 *  // pipeline
 *  sourceRange | ["filter", {...}];
 *  @endcode
 */
typedef PipelineElement RangeFilter;

/**
 *  @brief Transform range
 *
 *  @details A range that arises from the transformation of
 *  elements matching the supplied code block ("lambda").
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange transform: {...};
 *  // statement serialization
 *  container range => transform: {...};
 *  // pipeline
 *  sourceRange | ["transform", {...}];
 *  @endcode
 */
typedef PipelineElement RangeTransform;

/**
 *  @brief Keys range
 *
 *  @details A range that results from the first members of each
 *  tuple-like element of the source range.
 *  @par Usage in script:
 *  @code{c++}
 *  // method invocation
 *  sourceRange keys;
 *  // statement serialization
 *  container range => keys;
 *  // pipeline
 *  sourceRange | "keys";
 *  @endcode
 */
typedef RangeAction RangeKeys;

/**
 *  @brief Values range
 *
 *  @details A range that results from the second members of each
 *  tuple-like element of the source range.
 *  @par Usage in script:
 *  @code{.c++}
 *  // method invocation
 *  sourceRange values;
 *  // statement serialization
 *  container range => values;
 *  // pipeline
 *  sourceRange | "values";
 *  @endcode
 */
typedef RangeAction RangeValues;

/**
 *  @brief Elements range
 *
 *  @details A range that results from the `n`-th members of each
 *  tuple-like element of the source range.
 *  @par Usage in script:
 *  @code{.c++}
 *  // method invocation
 *  sourceRange elements: n;
 *  // statement serialization
 *  container range => elements: n;
 *  // pipeline
 *  sourceRange | ["elements", n];
 *  @endcode
 */
typedef RangeSize RangeElements;

/**
 *  @brief Reverse range
 *
 *  @details A range that results from the source range by reversing
 *  the order of its elements.
 *  @par Usage in script:
 *  @code{.c++}
 *  // method invocation
 *  sourceRange reverse;
 *  // statement serialization
 *  container range => reverse;
 *  // pipeline
 *  sourceRange | "reverse";
 *  @endcode
 */
typedef RangeAction RangeReverse;

//:::::::::::::::::::::::::::   Multiple Ranges   :::::::::::::::::::::::::::://

struct Range_ : RangeAction {
  ;
};

//============================================================================//
//:::::::::::::::::::::::::   ORange, ORange_Class   ::::::::::::::::::::::::://
//============================================================================//

template<typename t, typename _C>
  concept SplitBufferReq = (std::is_same_v<_C, OVector<t>> ||
      (std::is_same_v<t, ORef> && std::is_same_v<_C, ObjectVector>));

class ORange : public OInstance {
  template<typename t, typename _C> requires SplitBufferReq<t, _C>
    friend struct SplitBuffer;

  std::vector<PipelineElement> pipeline_;

public:
  /**
   *   @brief Loop iteration counter
   *
   *   @details It is a transitive member to be reset every time
   *       the loop iteration restarts.
   */
  size_t iterationCounter;

  ORange(OClass* cls) : OInstance(cls) {}
  ~ORange();

  PipelineElement& getPipelineElement(size_t stage) noexcept
  { return pipeline_[stage]; }
  const PipelineElement& getPipelineElement(size_t stage) const noexcept
  { return pipeline_[stage]; }

  static inline OClass* getRangeClass() noexcept
  { extern OClass* _OXType_class[]; return _OXType_class[CLSID_RANGE]; }

  /**
   * @brief Add pipeline element "all"
   *
   * @details This method assumes that the caller has created the
   * the iterators `begin` and `end`. It performs no checking on them.
   */
  void addPipelineAll(Session* m, ORef begin, ORef end);

protected:
  void run();

  /**
   *  @brief Run native loop
   *
   *  @tparam t a basic type or ORef or string (for set containers)
   *  @tparam _It an Object Model iterator
   *  @return value indicates if the loop was handled or not
   */
  template<typename t, typename _It>
    bool nativeLoop() noexcept;

  bool runNativeLoop() noexcept;
  bool runGeneralLoop() noexcept;
  bool runStage(size_t stage, ORef value);
  bool signalEnd(size_t stage);
  static bool endSignal(ORef value) noexcept { return !value; }
  /**
   *  @brief Select and run splitter to do range splitting
   *
   *  @param stage The stage of the pipeline, `size_t`
   *  @param value `ORef`, current value
   *  @return `bool`, indicates that the pipeline operation can continue
   */
  bool runSplitter(size_t stage, ORef value) noexcept;
  /**
   *  @brief Split range according to native vector delimiter
   *
   *  @details The vector delimiter is stored in the pipeline
   *      element's field `end`.
   *
   *  @tparam t The type of the delimiter's elements
   *  @tparam _C Type of delimiter vector, default `std::vector<t>`
   *  @param stage The stage of the pipeline, `size_t`
   *  @param delimiter A sequence of elements, type `_C`
   *  @param value `ORef`, current value
   *  @return `bool`, indicates that the pipeline operation can continue
   */
  template<typename t, typename _C = std::vector<t>>
    bool split(size_t stage, const _C& delimiter, ORef value) noexcept;
  bool runMerger(size_t stage, ORef value) noexcept;
  /**
   *  @brief Run native merge loop
   *
   *  @param stage The stage of the pipeline, `size_t`
   *  @param value `ORef`, current value
   *  @return `std::pair<bool, bool>` The first boolean indicates
   *      if the loop can continue and the second if the process
   *      was handled
   */
  std::pair<bool, bool> runNativeMerger(size_t stage, ORef value) noexcept;
  /**
   *  @brief Select and run native merge loop
   *
   *  @param stage The stage of the pipeline, `size_t`
   *  @param value `ORef`, current value
   *  @return `std::pair<bool, bool>` The first boolean indicates
   *      if the loop can continue and the second if the process
   *      was handled
   */
  template<typename t, typename _C>
  requires (!std::is_same_v<t, ORef> && !std::is_same_v<_C, OSet<t>>)
    std::pair<bool, bool> nativeMergeLoop(size_t stage, _C& container) noexcept;
  template<typename t, typename _C>
  requires (!std::is_same_v<t, ORef> && !std::is_same_v<t, std::string> &&
                                         std::is_same_v<_C, OSet<t>>)
    std::pair<bool, bool> nativeMergeLoop(size_t stage, _C& container) noexcept;
  template<typename t, typename _C>
  requires (std::is_same_v<t, ORef>)
    std::pair<bool, bool> nativeMergeLoop(size_t stage, _C& container) noexcept;
  template<typename t, typename _C>
  requires (std::is_same_v<t, std::string>)
    std::pair<bool, bool> nativeMergeLoop(size_t stage, _C& container) noexcept;
  /**
   *  @brief Run general merge loop
   *
   *  @param stage The stage of the pipeline, `size_t`
   *  @param value `ORef`, current value
   *  @return `bool`, indicates if the loop can continue
   */
  bool generalMergeLoop(size_t stage, ORef container) noexcept;

  static bool runUnknown        (ORange*, size_t stage, ORef value);
  static bool runAll            (ORange*, size_t stage, ORef value);
  static bool runIterator       (ORange*, size_t stage, ORef value);
  static bool runInitialLoop    (ORange*, size_t stage, ORef value);
  static bool runIteratorCount  (ORange*, size_t stage, ORef value);
  static bool runIteratorSentry (ORange*, size_t stage, ORef value);
  static bool runIota           (ORange*, size_t stage, ORef value);
  static bool runTake           (ORange*, size_t stage, ORef value);
  static bool runTakeWhile      (ORange*, size_t stage, ORef value);
  static bool runDrop           (ORange*, size_t stage, ORef value);
  static bool runDropWhile      (ORange*, size_t stage, ORef value);
  static bool runFilter         (ORange*, size_t stage, ORef value);
  static bool runTransform      (ORange*, size_t stage, ORef value);
  static bool runKeys           (ORange*, size_t stage, ORef value);
  static bool runValues         (ORange*, size_t stage, ORef value);
  static bool runElements       (ORange*, size_t stage, ORef value);
  static bool runReverse        (ORange*, size_t stage, ORef value);
  static bool runMerge          (ORange*, size_t stage, ORef value);
  static bool runSplit          (ORange*, size_t stage, ORef value);
  static bool runJoin           (ORange*, size_t stage, ORef value);
  static bool runExecute        (ORange*, size_t stage, ORef value);

  static bool runProcess        (ORange*, size_t stage, ORef value);

  bool runKeysOrValues(size_t which, size_t stage, ORef value);

  typedef bool (*PipelineRunner)(ORange*, size_t, ORef);
  PipelineRunner pipelineRunner_[22] {
    &ORange::runUnknown,
    &ORange::runInitialLoop, // ALL
    &ORange::runInitialLoop, // ITERATOR
    &ORange::runInitialLoop, // ITERATOR_PAIR
    &ORange::runInitialLoop, // ITERATOR_COUNT
    &ORange::runInitialLoop, // ITERATOR_SENTRY
    &ORange::runIota,
    &ORange::runTake,
    &ORange::runTakeWhile,
    &ORange::runDrop,
    &ORange::runDropWhile,
    &ORange::runFilter,
    &ORange::runTransform,
    &ORange::runKeys,
    &ORange::runValues,
    &ORange::runElements,
    &ORange::runReverse,
    &ORange::runMerge,
    &ORange::runSplit,
    &ORange::runJoin,
    &ORange::runExecute,
    nullptr,
  };

  _METHOD (ommIterator      );
  _METHOD (ommIteratorPair  );
  _METHOD (ommIteratorCount );
  _METHOD (ommIteratorSentry);
  _METHOD (ommIota          );
  _METHOD (ommIotaEnd       );
  _METHOD (ommTake          );
  _METHOD (ommTakeWhile     );
  _METHOD (ommDrop          );
  _METHOD (ommDropWhile     );
  _METHOD (ommFilter        );
  _METHOD (ommTransform     );
  _METHOD (ommProcess       );
  _METHOD (ommElements      );
  _METHOD (ommKeys          );
  _METHOD (ommValues        );
  _METHOD (ommReverse       );
  _METHOD (ommMerge         );
  _METHOD (ommSplit         );
  _METHOD (ommJoin          );
  _METHOD (ommDo            );
  _METHOD (ommRun           );
  _METHOD (ommRedo          );
  _METHOD (ommRedoAsIs      );
  _METHOD (bor              );

  ORet addDo        (const std::string& method, OArg*, Session*);
  ORet addTransform (const std::string& method, OArg*, Session*);
  ORet defaultIota  (Session* m);

private:
  _DECLARE_METHOD_TABLE();
};

class ORange_Class : public OClass {
public:
  /** Constructor for this class and C-derived classes */
  ORange_Class(Session* m, const std::string& className) :
      OClass(m, className) {}
  /** Constructor for script-derived classes */
  ORange_Class(Session* m, const std::string& base,
      const std::string& className) : OClass(m, base, className) {}
  /** Constructor for mutable classes */
  ORange_Class(Session* m, const OClass* base,
      const std::string& className) : OClass(m, base, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;

  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(ORange); }

  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<ORange>(ou); }

  /** @brief Construct core C++ object */
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<ORange>(m); }

  /** @brief Delete core C++ object */
  virtual void destructCore(OInstance* self, Session* m) override
  { if (!freeCached<ORange>(self)) delete static_cast<ORange*>(self); }

  virtual ORef copyCore(ORef self, const_ORef r, Session* m) const override;
  virtual ORef moveCore(ORef self, ORef r) const override;

  _ASSOCIATE_MCLASS_WITH(ORange);
};

#endif // __ORANGE_HPP__
