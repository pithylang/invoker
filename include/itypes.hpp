#ifndef __ITYPES_HPP__
#define __ITYPES_HPP__

#include <iostream>
#include <vector>
#include <memory>
#include <mapbox/variant.hpp>
// #include <variant>
#include "location.hpp"
#include "idefs.h"

namespace mapbox {
  using util::variant;
  using util::get;
}

struct INull {};

#define I_ALIAS(A, B)                                                          \
struct A : B {                                                                 \
  A() {}                                                                       \
  A(A&& r) {*this = std::move(r);}                                             \
  A(B&& expr) {*static_cast<B*>(this) = std::move(expr);}                      \
  A& operator=(A&& r) {*static_cast<B*>(this) = std::move(r); return *this;}   \
}

//============================================================================//
//:::::::::::::::::::::::::::::::   ISymbol   :::::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @brief Symbol type
 */
enum : ushort {
  SYMTYPE_UNKNOWN = 0,             /** Unknown type symbol           */
  SYMTYPE_VARIABLE,                /** Symbol is a variable          */
  SYMTYPE_SELECTOR,                /** Symbol is a selector          */
  SYMTYPE_BLOCK,                   /** Block or method body          */
  SYMTYPE_CLASS,                   /** Symbol is class               */
  SYMTYPE_METHOD_ADDRESS = SYMTYPE_BLOCK,
  SYMTYPE_CATEGORY_MASK            = 0x000f, /**< Symbol type mask        */
  SYMTYPE_LITERAL_ULONG  = 0x0010, /** Unsigned long literal         */
  SYMTYPE_LITERAL_LONG   = 0x0020, /** Long literal                  */
  SYMTYPE_LITERAL_UINT   = 0x0030, /** Unsigned integer literal      */
  SYMTYPE_LITERAL_INT    = 0x0040, /** Integer literal               */
  SYMTYPE_LITERAL_SHORT  = 0x0050, /** Short integer literal         */
  SYMTYPE_LITERAL_CHAR   = 0x0060, /** Character (8-bit int) literal */
  SYMTYPE_LITERAL_BOOL   = 0x0070, /** Boolean literal               */
  SYMTYPE_LITERAL_DOUBLE = 0x0080, /** Double literal                */
  SYMTYPE_LITERAL_FLOAT  = 0x0090, /** Float literal                 */
  SYMTYPE_LITERAL_STRING = 0x00a0, /** String literal                */
  /**
   *  Selector literal
   *
   *  Valid only in the interpretation phase
   */
  SYMTYPE_LITERAL_SEL    = 0x00b0,
  /**
   *  Variable identifier literal
   *
   *  Valid only in the interpretation phase.
   */
  SYMTYPE_LITERAL_ID     = 0x00c0,
  SYMTYPE_LITERAL_MASK             = 0x00f0, /**< Literal type mask        */
  SYMTYPE_LITERAL_HEXA   = 0x0100, /** Hexadecimal representation     */
  SYMTYPE_LITERAL_BINARY = 0x0200, /** Binary representation          */
  SYMTYPE_FORMAT_MASK              = 0x0300, /**< Format mask              */
  SYMTYPE_REF            = 0x0400, /** @deprecated                    */
  SYMTYPE_CONST          = 0x0800, /** @deprecated                    */
  SYMTYPE_STATIC         = 0x1000, /** Flag for static symbols        */
  SYMTYPE_ARG_REF        = 0x2000, /** Symbol is argument reference   */
  SYMTYPE_PROP_REF       = 0x4000, /** Symbol references object prop  */
  SYMTYPE_SYM_REF        = 0x8000, /** Symbol references other symbol */
  SYMTYPE_USAGE_MASK               = 0xf000, /**< Usage mask               */
};

#define SYMTYPE_STATIC_VAR (SYMTYPE_VARIABLE | SYMTYPE_STATIC)

enum /* VarUsage */ {
  VU_VAR             = 0,
  VU_STATIC          = (SYMTYPE_STATIC   & SYMTYPE_USAGE_MASK) >> 12,
  VU_ARG_REF         = (SYMTYPE_ARG_REF  & SYMTYPE_USAGE_MASK) >> 12,
  VU_STATIC_ARG_REF  = VU_STATIC | VU_ARG_REF,
  VU_PROP_REF        = (SYMTYPE_PROP_REF & SYMTYPE_USAGE_MASK) >> 12,
  VU_STATIC_PROP_REF = (VU_PROP_REF | VU_STATIC),
  _6                 = (VU_PROP_REF | VU_ARG_REF),
  _7                 = (_6 | VU_STATIC),
  VU_SYM_REF         = (SYMTYPE_SYM_REF  & SYMTYPE_USAGE_MASK) >> 12,
  VU_STATIC_SYM_REF  = (VU_SYM_REF + VU_STATIC),
  _10                = (VU_SYM_REF + VU_ARG_REF),
  _11                = (_10 + VU_STATIC),
  _12                = (VU_SYM_REF + VU_PROP_REF),
  _13                = (_12 + VU_STATIC),
  _14                = (_12 + VU_ARG_REF),
  _15                = (_14 + VU_STATIC),
};

#define VAR_USAGE(t) (((t) & SYMTYPE_USAGE_MASK) >> 12)
#define VAR_TYPE(t) ((t) & SYMTYPE_CATEGORY_MASK)

/**
 *  Symbol accessibility
 *
 *  Symbol accessibility is decided during runtime.
 */
enum : ushort {
  SYMACC_ALL = 0,
  SYMACC_NOT_GRANTED,   /**< For object properties only */
  SYMACC_LOCAL,         /**< For variables of current call frame */
};

/**
 *  @brief Interpreter symbol data
 *  @details Representation of symbols and literal values
 *           during the interpretation phase.
 */
struct ISymbol {
  /** Variable identifier or literal value */
  std::string name;
  /** Symbol type */
  ushort type;
  /**
   *  @brief Accessibility of object instance
   *  @details This item, though actually belonging to @c MSymbol,
   *           was placed here to get a better packing of these structures.
   */
  ushort access;
//   int line, pos;

  /**
   *  @brief Default constructor
   *  @details Constructs an invalid, unnamed symbol.
   */
  ISymbol() : type(SYMTYPE_UNKNOWN), access(SYMACC_NOT_GRANTED) {}
  /** Construct symbol with a given name */
  explicit
  ISymbol(const char* name, ushort = SYMTYPE_UNKNOWN);
  /** Construct symbol from a part of a string */
  ISymbol(const char* stringBegin, int length, ushort symbolType);
  /** Construct symbol by moving a given name */
  ISymbol(std::string&& text, ushort symbolType);
  /** Move constructor */
  ISymbol(ISymbol&& r);
  /** Copy constructor */
  ISymbol(const ISymbol& r);
  // ~ISymbol();

  ISymbol& operator=(ISymbol&& r);

  bool isLiteral() const noexcept { return getLiteralType() != 0; }
  decltype(type) getLiteralType() const noexcept
  { return type & SYMTYPE_LITERAL_MASK; }
  bool isLiteralValue() const noexcept {
    const decltype(type) t = (type & SYMTYPE_LITERAL_MASK);
    return t >= SYMTYPE_LITERAL_ULONG && t <= SYMTYPE_LITERAL_STRING;
  }
  bool isLiteralNumber() const noexcept {
    const decltype(type) t = (type & SYMTYPE_LITERAL_MASK);
    return (t >= SYMTYPE_LITERAL_ULONG && t <= SYMTYPE_LITERAL_CHAR) ||
           (t == SYMTYPE_LITERAL_DOUBLE || t == SYMTYPE_LITERAL_FLOAT);
  }
  bool isLiteralInteger() const noexcept {
    const decltype(type) t = (type & SYMTYPE_LITERAL_MASK);
    return t >= SYMTYPE_LITERAL_ULONG && t <= SYMTYPE_LITERAL_CHAR;
  }
  int getIntegerBase() const noexcept;
  bool isId() const noexcept { return getLiteralType() == SYMTYPE_LITERAL_ID; }
  int  getBlockId() const noexcept;
  bool isBlockId() const noexcept {return getBlockId() != -1;}

  //:::::::::::::::::::::   Inline Operation Processing   :::::::::::::::::::://
  struct BOPResult {
    std::string value;
    ushort symbolType;
    bool success;
  };

  std::string applyUnaryOperation(int op, const yy::location& loc) const;
  template<typename t>
    std::pair<std::string, bool> unaryOperation(int op, int base) const;
  ISymbol applyBinaryOperation(int op, const ISymbol& rhs,
                               const yy::location& location) const;
  template<typename t>
    BOPResult applyBOPWithLHSType(int op, const ISymbol& rhs) const;
  template<typename t, typename s>
    BOPResult applyReverseBOP(int op, t value1, int base1) const;
};

std::ostream& operator<<(std::ostream& os, const ISymbol& r);

//============================================================================//
//::::::::::::::::::::::::::::::::   IItem   ::::::::::::::::::::::::::::::::://
//============================================================================//
struct IUnit;
struct IBlock;
struct IReceivable;
struct IArrayExpression;
struct ISetExpression;

using IVariant = mapbox::variant<
  INull,
  ISymbol,
  std::unique_ptr<IUnit>,
  IArrayExpression,
  ISetExpression,
  IReceivable,
  IBlock
>;

struct IItemType {
//   typename std::aligned_storage<64, alignof(void*)>::type buffer[1];
  alignas(void*) std::byte buffer[72];
  IItemType();
  ~IItemType();
  inline operator IVariant&();
  inline operator const IVariant&() const;
  template<typename T>
    inline bool is() const noexcept;
  template<typename T>
    inline T& get() noexcept;
  template<typename T>
    inline const T& get() const noexcept;
  inline IItemType& operator=(IItemType&& r);
};

class IParser;

/**
 * @struct IItem
 * @brief Parse tree item
 */
struct IItem : IItemType {
  /** @brief Default constructor */
  IItem() = default;
  /** @brief Move constructor */
  IItem(IItem&& item);
  /**
   *  @brief Construct item from symbol or literal
   *  @details This constructor is used in the reductions
   *  @c item: "." | @c ID | @c lit
   */
  IItem(ISymbol&& simpleItem) {*this = std::move(simpleItem);}
  /**
   *  @brief Construct item from unit
   *  @details This constructor is used in the reduction
   *  @c item: "(" @c unit ")"
   */
  IItem(IUnit&& unit) {*this = std::move(unit);}
  /**
   *  @brief Construct item from json definition
   *  @details This constructor is used in the reduction
   *  @c item: "{" @c jsond "}"
   */
  IItem(IReceivable&& r) {*this = std::move(r);}
  /** @brief Copy operator */
  IItem& operator=(IItem&& r);
  /**
   *  @brief Set symbol
   *  @details This operator is used in the reductions
   *  @c item: "." | @c ID | @c lit
   */
  IItem& operator=(ISymbol&& simpleItem);
  /**
   *  @brief Set unit
   *  @details This operator is used in the reduction
   *  @c item: "(" @c unit ")"
   */
  IItem& operator=(IUnit&& unit);
  /**
   *  @brief Set block
   *  @details This operator is used in the reduction
   *  @c item: "(" @c block ")"
   */
  IItem& operator=(IBlock&& block);
  /**
   *  @brief Set object definition
   *  @details This operator is used in the reduction
   *  @c item: "{" @c jsond "}"
   */
  IItem& operator=(IReceivable&& r);

  IItem& operator=(IArrayExpression&& r);
  IItem& operator=(ISetExpression&& r);

  inline bool isEncapsulatedUnit() const;
  inline IUnit& getEncapsulatedUnit() const;

  /**
   *  @brief Set member access expression
   *  @details This method is used in the reduction
   *  @c item: @c item "." @c ID
   */
  void setMemberAccessExpression(IItem&& item, ISymbol&& id,
                                 const yy::location& location);
  /**
   *  @brief Set subscripted expression
   *  @details This method is used in the reduction
   *  @c item: @c item "[" @c expr "]"
   */
  void setSubscriptedExpression(IItem&& item, IUnit&& expr,
                                const yy::location& location);
  /**
   *  @brief Make item from string with attributes
   *  @details This method is used in the reduction
   *  @c item: STRWATTR
   *  It creates a unit out of the rhs and then makes into an item
   *  as in the reduction
   *  @c item: "(" unit ")"
   */
  void fromStringWithAttrs (const IParser& parseServices,
                            std::string&& id, size_t attrs,
                            const yy::location& location);
  void fromTemplateInitExpr(const IParser& parseServices,
                            std::string&& id, size_t attrs,
                            std::vector<IUnit>&& cexpr,
                            const yy::location& location);
  void fromInitListExpr    (const IParser& parseServices,
                            std::string&& id, size_t attrs,
                            std::vector<IUnit>&& cexpr,
                            const yy::location& location);
  void setArrayWithItemExpansion(std::vector<IUnit>&& cexpr, IItem&& a,
                                 const yy::location& location, bool atEnd);
  void setSetWithItemExpansion  (std::vector<IUnit>&& cexpr, IItem&& s,
                                 const yy::location& location, bool atEnd);
  /**
   *  @brief Throw parser exception if it is a block
   *  @details Use this method to ban block items used as receivers.
   */
  void throwIfBlock(const yy::location&) const;
};

std::ostream& operator<<(std::ostream& os, const IItem& r);

//============================================================================//
//:::::::::::::::::::::::::::::   IReceivable   :::::::::::::::::::::::::::::://
//============================================================================//
struct IReceivable {
  ISymbol method;
  std::vector<IUnit> args;

  /** @brief Default constructor */
  IReceivable();
  /** @brief Construct receivable from selector without arguments */
  IReceivable(ISymbol&& selector);
  /** @brief Construct receivable from selector and argument */
  IReceivable(ISymbol&& selector, IUnit&& expr);
  /** @brief Construct receivable from selector name and argument list */
  IReceivable(std::string&& selectorName, std::vector<IUnit>&& argList);
  /** @brief Move constructor */
  IReceivable(IReceivable&& r);

  /** @brief Move operator */
  IReceivable& operator=(IReceivable&& r);
  /**
   *  @brief Convert symbol to receivable
   *  @details This operator is used in the reduction
   *  @c recbl: @c ID
   */
  IReceivable& operator=(ISymbol&& symbol);

  inline bool isNull() const noexcept
  { return method.name.empty() && args.empty(); }

  IReceivable& renderArgList(ISymbol&& newSelector);

  std::vector<std::string> splitSelector(const std::string& delim) const {
    std::vector<std::string> split(const std::string&, const std::string&);
    return split(method.name, delim);
  }
};

IReceivable operator+(IReceivable&& a, IReceivable&& b);
std::ostream& operator<<(std::ostream& os, const IReceivable& r);

//============================================================================//
//::::::::::::::::::::::::::::::::   IUnit   ::::::::::::::::::::::::::::::::://
//============================================================================//
class IParser;

/**
 * @struct IItem
 * @brief Representation of a statement unit
 */
struct IUnit {
  IItem receiver;
  IReceivable receivable;
  yy::location loc;

  /** @brief Default constructor*/
  IUnit() {}
  /** @brief Copy constructor*/
  IUnit(IUnit&& unit);
  /**
   *  @brief Construct unit from item
   *  @details This constructor is used in the reduction
   *  @c expr: @c item
   */
  IUnit(IItem&& item);
  /**
   *  @brief Construct unit from its parts
   *  @details This constructor is used in the reduction
   *  @c unit: @c item @c recbl
   */
  IUnit(IItem&&, IReceivable&&);

  /**
   *  @brief Convert item to expression
   *  @details This operator is used in the reduction
   *  @c expr: @c item
   *  The result is a unit-encapsulated @em individual item
   */
  IUnit& operator=(IItem&& r);
  /** @brief Move operator */
  IUnit& operator=(IUnit&& r);

  inline bool isNull() const noexcept;
  /**
   *  @brief Check if this unit is an individual item
   *  @details A unit-encapsulated item occurs through the reductions
   *  @c expr: @c item
   *  @c unit: @c expr
   *  When unit-encapsulated items are combined to form true
   *  expressions, as for example through the reductions
   *  @c expr: @c expr @em op @c expr
   *  the first expression is implicitly converted into an ordinary
   *  item.
   */
  inline bool isIndividualItem() const noexcept;
  /**
   *  @brief Indicates if VCM can be used as argument
   *  @details Variable Creation Methods are used exclusively by
   *  the system object and can or cannot be used as arguments to
   *  other methods.
   *  @param parser `IParser`
   *  @return true If it is usable as argument
   *  @return false If it is not
   */
  bool isVCMNotUsableAsArg(const IParser& parser) const noexcept;
  /** @brief Check if it is a simple item expression */
  bool isItem() const noexcept {return isIndividualItem();}
  inline bool isOperation(const char* op) const noexcept
  { return receivable.method.name == op; }
  inline bool maybeSpecialOperation() const noexcept {
    const auto& id = receivable.method.name;
    return id.size() == 3 && id[0] == '#';
  }
  bool isAssignmentOperation()     const noexcept {return isOperation("#ai");}
  bool isConstAssignOperation()    const noexcept {return isOperation("#ca");}
  bool isDecompAssignOperation()   const noexcept {return isOperation("#da");}
  bool isCDecompAssignOperation()  const noexcept {return isOperation("#Da");}
  bool isCopyOperation()           const noexcept {return isOperation("#cp");}
  bool isPropAccessOperation()     const noexcept {return isOperation("#gp");}
  bool isReferenceOperation()      const noexcept {return isOperation("#rf");}
  bool isConstReferenceOperation() const noexcept {return isOperation("#cr");}
  bool isDecompRefOperation()      const noexcept {return isOperation("#dr");}
  bool isConstDecompRefOperation() const noexcept {return isOperation("#cd");}
  bool isBlockInvocation()         const noexcept {return isOperation("#bl");}
  bool isConditionalOperation()    const noexcept {return isOperation("#cb");}
  bool isEncapsulatedBlock() const noexcept;
  bool isBlockInvocationWithParams() const noexcept {
    return receiver.isEncapsulatedUnit() && !receivable.isNull() &&
        receiver.getEncapsulatedUnit().isBlockInvocation();
  }
  inline bool isBlockEvaluation() const noexcept;
  bool isLiteralNumber() const noexcept;
  bool isLiteralInteger() const noexcept;

  bool isClassDeclaration(const IParser&) const noexcept;

  /**
   *  @brief Throw exception if receiver is block
   *  @details It throws a parser exception if the expression's
   *  receiver is a @em bare block, ie. a block not obtained from
   *  a call to IUnit::makeBlockInvocation(IItem&&, const yy::location&).
   */
  void throwIfReceiverIsBlock(const yy::location&) const;

  /**
   *  @brief Set binary operation
   *  @details This is used in the reduction
   *  @c expr: @c expr OP @c expr
   *  where OP is an operation. The first expression is implicitly
   *  converted into an item by means of the reductions:
   *  @c unit: @c expr
   *  @c item: "(" @c unit ")"
   *  The operation and the second expression are implicitly
   *  converted into a receivable:
   *  @c jsond: @c SELID @c expr
   *  @c recbl: @c jsond
   *  Finally, the item and the receivable produce a unit:
   *  @c unit: @c item @c recbl
   *  This @c unit is @c this object.
   */
  void setBinaryOperation(IUnit&& expr1, const char *op, IUnit&& expr2);
  bool handleLiteralBOP(const char *op, const
      IUnit& expr1, const IUnit& expr2, const yy::location& location);
  /**
   *  @brief Set unary operation
   *  @details This is used in the reduction
   *  @c expr: OP @c expr
   *  where OP is an operation. The expression is implicitly
   *  converted into an item by means of the reductions:
   *  @c unit: @c expr
   *  @c item: "(" @c unit ")"
   *  The operation is implicitly converted into a receivable:
   *  @c recbl: @c ID
   *  Finally, the item and the receivable produce a unit:
   *  @c unit: @c item @c recbl
   *  This @c unit is @c this object.
   */
  void setUnaryOperation(const char *op, IUnit&& expr);
  /**
   *  @brief Handle internally unary operation
   *  @details This method is used to internally handle a unary
   *  operation when the receiver is a literal number.
   */
  bool handleLiteralUOP(const char *op, IUnit&& expr, const yy::location& loc);
  /**
   *  @brief Set assignment operation
   *  @details This is used in the reduction
   *  @c expr: "=" @c expr
   */
  void setAssignmentOperation(IUnit&& expr1, IUnit&& expr2,
                              const yy::location& location, bool isConst);
  /**
   *  @brief Set reference operation
   *  @details This is used in the reductions
   *  @c expr: ":=" @c expr
   *  @c expr: "::=" @c expr
   */
  void setReferenceOperation(IUnit&& expr1, IUnit&& expr2,
                             const yy::location& location, bool isConst);
  /**
   *  @brief Make block invocation
   *  @details This method is used in the reduction
   *  @c expr: "@" @c item
   *  If the item is a @c block it encpsulates it into @em this
   *  @c unit and sets the `evaluate` flag to `true`; Otherwise,
   *  it creates a receivable with selector `#bl` and then it
   *  reduces the @c item and the receivable into @em this @c unit.
   */
  void makeBlockInvocation(IItem&& item, const yy::location& location);
  void setConditionalOperation(bool inlineConditionalOperator, IUnit&& expr1,
      IUnit&& expr2, IUnit&& expr3, const yy::location& location);

  static std::string assignError(const char* what = nullptr);
};

std::ostream& operator<<(std::ostream& os, const IUnit& r);
std::ostream& operator<<(std::ostream& os, const std::vector<IUnit>& r);

I_ALIAS(IArrayExpression, std::vector<IUnit>);
I_ALIAS(ISetExpression, std::vector<IUnit>);

//============================================================================//
//::::::::::::::::::::::::::::::::   IBlock   :::::::::::::::::::::::::::::::://
//============================================================================//

struct IBlock : std::vector<IUnit> {
  bool evaluate; ///< Evaluation explicitly demanded by the user
  bool execute;  ///< Implicitly deduced by the parser

  IBlock() : evaluate{false}, execute{false} {}
  IBlock(IBlock&& r);
  IBlock(std::vector<IUnit>&& r) : evaluate{false}, execute{false}
  { *this = std::move(r); }

  IBlock& operator=(std::vector<IUnit>&& r);
  IBlock& operator=(IBlock&& r);
};

//============================================================================//
//:::::::::::::::::::::::::::   IStringWithAttrs   ::::::::::::::::::::::::::://
//============================================================================//

enum : ulong {
  SAT_NONE = 0,
  SAT_AUTOVAR,
  SAT_CONSTVAR,
  SAT_NOAUTOVAR,
  SAT_CLASSOBJ,
  SAT_PACKAGE,
  SAT_ID,
  SAT_SELID,
};

struct IStringWithAttrs {
  std::string name;
  size_t attrs;

  IStringWithAttrs() : attrs{0} {}
  IStringWithAttrs(IStringWithAttrs&& r) :
      name{std::move(r.name)}, attrs{r.attrs} {}
  IStringWithAttrs(std::string&& name, size_t attrs) :
      name{std::move(name)}, attrs{attrs} {}

  IStringWithAttrs& operator=(IStringWithAttrs&& r);
};

std::ostream& operator<<(std::ostream& os, const IStringWithAttrs& r);

//============================================================================//
//::::::::::::::::::::::::   Inline Implementation   ::::::::::::::::::::::::://
//============================================================================//

inline IItemType::operator IVariant&()
{ return *reinterpret_cast<IVariant*>(buffer); }

inline IItemType::operator const IVariant&() const
{ return *reinterpret_cast<const IVariant*>(buffer); }

inline IItemType& IItemType::operator=(IItemType&& r) {
  IVariant& rhs = static_cast<IVariant&>(r);
  static_cast<IVariant&>(*this) = std::move(rhs);
  return *this;
}

template<typename T>
inline bool IItemType::is() const noexcept
{ return static_cast<const IVariant&>(*this).is<T>(); }

template<typename T>
inline T& IItemType::get() noexcept
{ return static_cast<IVariant&>(*this).get<T>(); }

template<typename T>
inline const T& IItemType::get() const noexcept
{ return static_cast<const IVariant&>(*this).get<T>(); }

inline bool IItem::isEncapsulatedUnit() const
{ return static_cast<const IVariant&>(*this).is<std::unique_ptr<IUnit>>(); }
inline IUnit& IItem::getEncapsulatedUnit() const
{ return *static_cast<const IVariant&>(*this).get<std::unique_ptr<IUnit>>(); }

inline bool IUnit::isNull() const noexcept
{ return receiver.is<INull>() && receivable.isNull(); }

inline bool IUnit::isIndividualItem() const noexcept
{ return !receiver.is<INull>() && receivable.isNull(); }

inline bool IUnit::isEncapsulatedBlock() const noexcept {
  return isItem() && receiver.is<IBlock>() &&
      !receiver.get<IBlock>().evaluate &&
      !receiver.get<IBlock>().execute;
}

inline bool IUnit::isBlockEvaluation() const noexcept {
  return receiver.is<IBlock>() && receiver.get<IBlock>().evaluate &&
      !receiver.get<IBlock>().execute;
}

#endif // __ITYPES_HPP__
