#ifndef __OVECITER_HPP__
#define __OVECITER_HPP__

#include <vector>
#include "oiter.hpp"
#include "object.hpp"
#include "obasicTypeInfo.hpp"
#include "orangeOperation.hpp"

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::   OVectorIterator   :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

template<typename t>
class OVectorIterator : public ORandomAccessIterator,
                        public std::vector<t>::iterator {
  using difference_t = typename std::vector<t>::iterator::difference_type;

  bool const_{false};
  bool reverse_{false};

public:
  using base_class = typename std::vector<t>::iterator;

  OVectorIterator(OClass* cls) : ORandomAccessIterator(cls) {}
  OVectorIterator& operator=(const OVectorIterator& r) = default;
  OVectorIterator& operator=(const typename std::vector<t>::iterator& r)
  { *static_cast<base_class*>(this) = r; return *this; }

  _DEFINE_TIE(_O(VectorIterator), BTI::getVectorIteratorClass<t>());

  void setConst() noexcept {const_ = true;}
  void setReverse(bool b) noexcept {reverse_ = b;}
  bool isReverse() const noexcept {return reverse_;}

  _METHOD(ommGetValue);
protected:
  _METHOD(ommComponent);
  _METHOD(ommSwap);
  _METHOD(add);
  _METHOD(sub);
  _METHOD(inc);
  _METHOD(dec);
  _METHOD(postinc);
  _METHOD(postdec);
  _METHOD(incBy);
  _METHOD(decBy);
  _METHOD(lt);
  _METHOD(gt);
  _METHOD(le);
  _METHOD(ge);
  _METHOD(eq);
  _METHOD(ne);

  static std::string getFullMethodName(const std::string& methodName) noexcept;
  ORet packElementValue(t& x, const char* methodName, Session* m) const
      requires (!std::is_same_v<t, bool> && !std::is_same_v<t, ORef>);
  ORet packElementValue(const std::vector<bool>::reference& x,
      const char* methodName, Session* m) const
      requires std::is_same_v<t, bool>;
  ORet getResult(base_class&& value, Session* m) const noexcept;

  _DECLARE_METHOD_TABLE();
};

template<typename t>
class OVectorIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OVectorIterator_Class(Session* m, const std::string& typeName);
  /** Constructor for script-derived classes */
  OVectorIterator_Class(Session *m, const std::string& baseClass,
      const std::string& typeName) : OClass(m, baseClass, typeName) {}
  /** Constructor for mutable classes */
  OVectorIterator_Class(Session *m, const OClass* baseClass,
      const std::string& typeName) : OClass(m, baseClass, typeName) {}

  virtual OClass* createClass(Session* m, const std::string& typeName)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& typeName)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(OVectorIterator<t>); }
  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<OVectorIterator<t>>(ou); }
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<OVectorIterator<t>>(m); }
  virtual void destructCore(OInstance* self, Session*) override {
    if (!freeCached<OVectorIterator<t>>(self))
      delete static_cast<OVectorIterator<t>*>(self);
  }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override;
  virtual ORef moveCore(ORef self, ORef) const override;

  _ASSOCIATE_MCLASS_WITH(OVectorIterator<t>)
};

#define _PVecIter(t, o) static_cast<OVectorIterator<t>*>(o)
#define _PCVecIter(t, o) static_cast<const OVectorIterator<t>*>(o)

#endif // __OVECITER_HPP__
