#ifndef __OTYPELESS_HPP__
#define __OTYPELESS_HPP__

#include <algorithm>
#include "oclass.hpp"

class OClass;

/**
 * Typeless property
 */
struct TypelessProperty {
  OClass* owner;
  std::string typeName;
  int ofs;

  inline bool isNull() const noexcept
  { return owner == nullptr && typeName.empty() && ofs == - 1; }
  inline void setNull() noexcept
  { owner = nullptr; typeName = std::string(""); ofs = - 1; }
};

/**
 * Typeless property list
 */
struct TypelessPropertyList : std::vector<TypelessProperty> {
  /** Add typeless member to list */
  void add(OClass* owner, const std::string& type, int ofs)
  { push_back({owner, type, ofs}); }
  /** Remove null entries from list */
  void pack() {
    auto e = end();
    for (auto i = rbegin(); i < rend(); ++i)
      if (i->isNull()) std::swap(*i, *--e);
    erase(e, end());
  }
};

#endif // __OTYPELESS_HPP__
