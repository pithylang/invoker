#ifndef __OATTRS_HPP__
#define __OATTRS_HPP__

#include <string>

struct Attr {
  std::string key;
  ushort flag;
  bool operator==(const Attr& r) const noexcept {return key == r.key;}
};

class Session;

struct AttrParser {
  const std::string& attrs;
  std::string error;
  ushort flags;
  AttrParser(const std::string& attrs) : attrs(attrs), flags(0) {}
  void reset() {error = ""; flags = 0;}
  /** Parse attribute string */
  bool parse();
  /** Parse attributes and throw exception in the occasion of error */
  void parse(Session* m, const char* methodName);
  bool incorrectAttribute() const noexcept {return !error.empty();}
  static void trim(std::string&);
};

#endif // __OATTRS_HPP__
