#ifndef __OBJECTCACHE_HPP__
#define __OBJECTCACHE_HPP__

#include <memory>
#include <vector>
#include <stdexcept>
#include <functional>
#include "oclass.hpp"

struct ObjectCacheBase {
    size_t sizeAvailable;
    std::unique_ptr<size_t[]> available;
    ObjectCacheBase() = delete;
    ObjectCacheBase(size_t size) :
        sizeAvailable{size}, available{new size_t[size]} {}
    virtual ~ObjectCacheBase() {}
    virtual bool contains(OInstance*) const noexcept {return false;}
};

template<typename T>
  requires std::is_base_of_v<OInstance, T>
  struct ObjectCache : ObjectCacheBase {
    std::vector<T> cache;

    ObjectCache() = delete;
    ObjectCache(size_t size, OClass* type) : ObjectCacheBase{size} {
      cache.reserve(size);
      for (size_t i = 0; i < size; ++i) {
        available.get()[size - 1 - i] = i;
        cache.emplace_back(type);
      }
    }

    virtual bool contains(OInstance* o) const noexcept override
    { return o < (cache.data() + cache.size()) && o >= cache.data(); }
    inline bool isCached(T* o) const noexcept
    { return o < cache.data() + cache.size() && o >= cache.data(); }
    T* reserve() {
      if (sizeAvailable == 0) return nullptr;
      const size_t nextAvailable = available[--sizeAvailable];
      return &cache[nextAvailable];
    }
    bool release(T* o) {
      auto d = std::distance(cache.data(), o);
      if (d < 0 || d > cache.size() - 1) return false;
#ifdef __DEBUG__
      if (sizeAvailable == cache.size())
      	throw std::logic_error{o->getType()->name+" cache error"};
#endif // __DEBUG__
      available[sizeAvailable++] = d;
      o->~T();
      new(cache.data() + d) T{o->getType()};
      return true;
    }
    void forEachAvailable(const std::function<void(T&)>& f) {
      for (size_t i = 0; i < sizeAvailable; ++i) {
        const auto ofs = available[i];
        f(cache[ofs]);
      }
    }
  };

enum : int { // Cache creation policy
  CCP_NONE,
  CCP_MAX,
  CCP_MIN,
  CCP_PC_OF_MIN,
  CCP_PC_OF_MAX,
  CCP_UNDERCACHE, // size = min + (max - min)*pc/100
  CCP_OVERCACHE = CCP_PC_OF_MAX,
  CCP_DEFAULT = CCP_MAX
};

struct ObjectUsage {
  size_t count;
  size_t limitup;
  int policy;
  int pc;
  ObjectUsage() : count{0}, limitup{0}, policy{CCP_DEFAULT}, pc{0} {}
  bool check() const noexcept {return count >= 0 && limitup >= count;}
  bool canDelete() const noexcept {return count > 0;}
  inline void inc() noexcept {if (++count > limitup) limitup = count;}
  inline void dec() noexcept {--count;}
  ObjectUsage& operator++() noexcept {inc(); return *this;}
  ObjectUsage operator++(int) noexcept {auto r = *this; inc(); return r;}
  ObjectUsage& operator--() noexcept {dec(); return *this;}
  ObjectUsage operator--(int) noexcept {auto r = *this; dec(); return r;}
  size_t getCacheSize() const noexcept;
};

#endif // __OBJECTCACHE_HPP__
