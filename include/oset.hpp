#ifndef __OSET_HPP__
#define __OSET_HPP__

#include <set>
#include "object.hpp"
#include "otypes.h"

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::   OSet, OSet_Class   ::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

/**
 *  @class OSet<t>
 *  @brief Basic type Set container class
 *
 *  @details **OSet**s are associative containers of sorted objects
 *  of the same type. Associative means that elements are referenced
 *  by key and not by position in the container. OSet objects are
 *  of a basic type, @c ulong, @c long, @c double and `std::string`.
 *  Element values are also their keys. The elements are sorted
 *  according to the default comparison method `std::less`. For
 *  another sorting method, use the @ref OSetContainer
 *  "object set container" or @ref OSet<ORef> "object reference set
 *  container" instead.
 *
 *  Since in a set, the value of an element also identifies it (the
 *  value is itself the key), each value is unique. Set element
 *  values are not modifiable.
 *
 *  Sets are containers that store unique elements following
 *  a specific order.
 *
 *  Internally, set elements are sorted according to a strict
 *  weak ordering criterion indicated by an internal comparison
 *  object of type @c Compare.
 *
 *  Sets are typically implemented as binary search ("red-black")
 *  trees.
 *
 *  Search, removal, and insertion operations have logarithmic
 *  complexity.
 *
 *  @tparam t Basic type, one of @c ulong, @c long, @c double and
 *  `std::string`
 */
template<typename t>
class OSet : public OInstance, public std::set<t> {
  using set_t = typename std::set<t>;

public:
  using base_class = typename std::set<t>;
  using value_type = typename base_class::value_type;
  /**
   *  @brief OM-default constructor
   */
  OSet(OClass* cls) : OInstance(cls) {}

  bool isOwner() const noexcept { return false; }

  /**
   *  @brief Initialize literal set
   *
   *  @details This is not a public method. Instead it is used
   *  internally. More precisely, it is called from
   *  ORTMgr::createSet() to initialize a newly created set.
   *  @par A script defines a literal set with the code
   *  `{item0, item1, ...}`, for example,
   *  @code {.c++}
   *  v = {12, x + y, 32};
   *  @endcode
   *  where @c x, @c y are @c int variables. This will create a
   *  set with @c int elements.
   *  @return ORet Object reference of created set
   */
  _METHOD (ommInit         );

protected:
  _METHOD (ommEmpty        );
  _METHOD (ommCompareInfo  );
  _METHOD (ommCount        );
  _METHOD (ommFind         );
  _METHOD (ommSize         );
  _METHOD (ommErase        );
  _METHOD (ommEraseAt      );
  _METHOD (ommEraseRange   );
  _METHOD (ommClear        );
  _METHOD (ommInsert       );
  _METHOD (ommInsertHint   );
  _METHOD (ommInsertRange  );
  _METHOD (ommSwap         );
  _METHOD (ommBegin        );
  _METHOD (ommRBegin       );
  _METHOD (ommEnd          );
  _METHOD (ommREnd         );
  _METHOD (ommSetType      );
  /**
   *  @brief Range of all set elements
   *  @details It returns a range of all set elements in
   *  ascending order. There is no `call` method because
   *  set elements are not modifiable.
   */
  _METHOD (ommAll          );
  /**
   *  @brief Range of all set elements
   *  @details It returns a range of all set elements in
   *  descending order. There is no `crall` method because
   *  set elements are not modifiable.
   */
  _METHOD (ommRAll         );

  ORet packInsertResult(const std::string& methodName,
                        std::pair<typename std::set<t>::iterator, bool>&,
                        Session*);
  bool insertTypeRange(OArg* argv);

private:
  _DECLARE_METHOD_TABLE();
};

template<typename t>
class OSet_Class : public OClass {
public:
  /** Constructor for this class and C-derived classes */
  OSet_Class(Session* m, const std::string& className) :
      OClass(m, className) {}
  /** Constructor for script-derived classes */
  OSet_Class(Session* m, const std::string& base,
      const std::string& className) : OClass(m, base, className) {}
  /** Constructor for mutable classes */
  OSet_Class(Session* m, const OClass* base,
      const std::string& className) : OClass(m, base, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<OSet<t>>(m); }
  virtual void destructCore(OInstance* self, Session* m) override
  { if (!freeCached<OSet<t>>(self)) delete static_cast<OSet<t>*>(self); }
  virtual ORef copyCore(ORef self, const_ORef r, Session* m) const override;
  virtual ORef moveCore(ORef self, ORef r) const override;
  virtual size_t getAllocSize() const override
  { return sizeof(OSet<t>); }
  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<OSet<t>>(ou); }

  _ASSOCIATE_MCLASS_WITH(OSet<t>)
};

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::   ORefSetCompare   :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

struct ORefSetCompare : std::less<ORef> {
  Session* currentSession;
  OMethodAddress compare;
  bool initialized;
  ORefSetCompare();
  bool operator()(const ORef& lhs, const ORef& rhs) const;
};

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::::   ORefSetPrepare   :::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

struct ORefSetPrepare {
  enum {
    NO_ERROR = 0,
    NO_COMPARE_METHOD,
    TYPELESS_OBJECT_SET,
    TYPE_MISMATCH,
  };

  Session* m;
  ORefSetCompare* comparison;
  int& typeId;
  int errorNumber;

  bool insert(size_t i, const ORef& value);
  void insertError(const std::string& methodName, const ORef& val);
  /**
   *  @brief Prepare copy operation
   *  @details Call this method only on the first element that
   *      is copied and inserted into the container. It does not
   *      throw any VM exceptions.
   *
   *  @param setType The base type of the elements to be copied.
   *      It is the type of the source (rhs) container.
   *  @param compare The source comparison method.
   *  @return Success indicator.
   */
  bool copy(OClass* setType, const OMethodAddress& rhsCompare);
  void find() noexcept { if (comparison) comparison->currentSession = m; }
  ORet badArgType(const std::string& methodName, int argn, OArg* argv) const;
};

//----------------------------------------------------------------------------//
//::::::::::::::::::::::   ObjectSet, ObjectSet_Class   :::::::::::::::::::::://
//----------------------------------------------------------------------------//

/**
 *  @class ObjectSet
 *  @brief Object set container class
 *
 *  @details An **ObjectSet** is a sorted, associative container of
 *  objects of the same type. Associative means elements are
 *  referenced by key, not by position in the container. The object
 *  type can be anyone that supports the method `compare:`. As with
 *  basic type sets, the values are also the keys. The container
 *  sorts elements according to the supplied element comparison
 *  method `compare:`.
 *
 *  Since in a set, the value of an element is also its identifier
 *  (it is also the key), each value is unique. Element values are
 *  not modifiable once in the set.
 *
 *  **ObjectSet**s are containers that store unique elements
 *  following a specific order. The order is determined by the
 *  method `compare:`. There is no default comparison for object
 *  sets. Elements must provide a `compare:` method.
 *
 *  When the type of elements is not explicitly specified by a
 *  call to the method `type:`, it is deduced during the first
 *  element insertion. The type of elements is stored in the
 *  `typeId_` field of the container.
 *
 *  Object sets may own or not own their elements:
 *
 *  @code{.c++}
 *  set1 = {a, b, c}; // set1 owns its elements
 *  set2 = {& a, b, c}; // set2 does not own its elements
 *  $auto: set3 type: "set"; // owns its elements
 *  $auto: set4 type: "reference set"; // does not own its elements
 *  @endcode
 *
 *  Sets are typically implemented as binary search ("red-black")
 *  trees.
 *
 *  Search, removal, and insertion operations have logarithmic
 *  complexity.
 */
class ObjectSet : public OInstance, public std::set<ORef, ORefSetCompare> {
  friend class ObjectSet_Class;

  /**
   * @brief Pointer to the internal comparison object
   */
  ORefSetCompare* compare_;
  int typeId_;

public:
  using base_class = std::set<ORef, ORefSetCompare>;
  using value_type = typename base_class::value_type;
  /**
   *  @brief OM-default constructor
   */
  ObjectSet(OClass* cls);

  bool isOwner() const noexcept { return getRootClassId() == CLSID_OBJSET; }
  /**
   *  @brief Swap sets
   */
  void swap(ObjectSet& rhs);

  /**
   *  @brief Initialize literal object set
   *
   *  @details This is not a public method. Instead it is used
   *  internally. More precisely, it is called from
   *  ORTMgr::createSet() to initialize a newly created set.
   *  @par A script defines a literal set with the code
   *  `{item0, item1, ...}`, for example,
   *  @code{.c++}
   *  s = {x, y, z};
   *  @endcode
   *  where @c x, @c y, @c z are variables of the same type @c T.
   *  This will create a set type @c T.
   *  @return ORet Object reference of created set
   */
  _METHOD (ommInit        );

protected:
  _METHOD (ommEmpty       );
  _METHOD (ommCompareInfo );
  _METHOD (ommCount       );
  _METHOD (ommSize        );
  _METHOD (ommErase       );
  _METHOD (ommEraseAt     );
  _METHOD (ommEraseRange  );
  _METHOD (ommFind        );
  _METHOD (ommClear       );
  _METHOD (ommInsert      );
  _METHOD (ommInsertHint  );
  _METHOD (ommInsertRange );
  _METHOD (ommSetType     );
  _METHOD (ommSwap        );
  _METHOD (ommBegin       );
  _METHOD (ommRBegin      );
  _METHOD (ommEnd         );
  _METHOD (ommREnd        );
  // ranges
  _METHOD (ommAll         );
  _METHOD (ommRAll        );

  void clearContainer(Session* m);
  ORet copyItems(const ObjectSet& rhs, Session* m);
  bool insertRange(OArg* argv, Session* m);

private:
  std::string getFullMethodName(const std::string& methodName) const;

  _DECLARE_METHOD_TABLE();
};

class ObjectSet_Class : public OClass {
public:
  /** Constructor for this class and C-derived classes */
  ObjectSet_Class(Session* m, const std::string& className) :
      OClass(m, className) {}
  /** Constructor for script-derived classes */
  ObjectSet_Class(Session* m, const std::string& base,
      const std::string& className) : OClass(m, base, className) {}
  /** Constructor for mutable classes */
  ObjectSet_Class(Session* m, const OClass* base,
      const std::string& className) : OClass(m, base, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(ObjectSet); }

  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<ObjectSet>(ou); }

  /** @brief Construct core C++ object */
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<ObjectSet>(m); }

  /**
   *  @brief Delete core C++ object
   *  @details We override this method to delete all elements
   *      besides the container.
   */
  virtual void destructCore(OInstance* self, Session* m) override;

  // This argument is necessary to invoke copy
  virtual ORef copyCore(ORef self, const_ORef r, Session* m) const override;
  virtual ORef moveCore(ORef self, ORef r) const override;

  _ASSOCIATE_MCLASS_WITH(ObjectSet);
};

#define _PSet(t, a)  static_cast<OSet<t>*>(a)
#define _PCSet(t, a) static_cast<const OSet<t>*>(a)
#define _PObjSet(a)  static_cast<ObjectSet*>(a)
#define _PULongSet(a)     _PSet(ulong, a)
#define _PLongSet(a)      _PSet(long, a)
#define _PUIntSet(a)      _PSet(unsigned, a)
#define _PIntSet(a)       _PSet(int, a)
#define _PShortSet(a)     _PSet(short, a)
#define _PCharSet(a)      _PSet(char, a)
#define _PDblSet(a)       _PSet(double, a)
#define _PFloatSet(a)     _PSet(float, a)
#define _PBoolSet(a)      _PSet(bool, a)
#define _PPtrSet(a)       _PSet(void*, a)
#define _PStrSet(a)       _PSet(std::string, a)

#endif // __OSET_HPP__
