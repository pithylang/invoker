#ifndef __SELECTORS_H__
#define __SELECTORS_H__

#include "otypes.h"

//============================================================================//
//::::::::::::::::::::::::::::   Selector names   :::::::::::::::::::::::::::://
//============================================================================//

#define S_constructor "constructor"
#define S_destructor  "destructor"
#define S_alloc       "alloc" // TODO any use?
#define S_free        "free"  // TODO any use?
#define S_component   "component:"
#define S_copy        "copy:"
#define S_inc         "inc"
#define S_dec         "dec"
#define S_postinc     "postinc"
#define S_postdec     "postdec"
#define S_incBy       "incBy:"
#define S_decBy       "decBy:"
#define S_mulBy       "mulBy:"
#define S_divBy       "divBy:"
#define S_modBy       "modBy:"
#define S_expBy       "expBy:"
#define S_bitandBy    "bitandBy:"
#define S_bitorBy     "bitorBy:"
#define S_bitxorBy    "bitxorBy:"
#define S_shiftrBy    "shiftrBy:"
#define S_shiftlBy    "shiftlBy:"
#define S_add         "add:"
#define S_sub         "sub:"
#define S_mul         "mul:"
#define S_div         "div:"
#define S_mod         "mod:"
#define S_exp         "exp:"
#define S_bitand      "bitand:"
#define S_bitor       "bitor:"
#define S_bitxor      "bitxor:"
#define S_bitinv      "bitinv"
#define S_shiftr      "shiftr:"
#define S_shiftl      "shiftl:"

#define S_eq          "eq:"
#define S_gt          "gt:"
#define S_lt          "lt:"
#define S_ge          "ge:"
#define S_le          "le:"
#define S_ne          "ne:"
#define S__and        "and:"
#define S__or         "or:"
#define S__xor        "xor:"
#define S__not        "not"
#define S_andBy       "andBy:"
#define S_orBy        "orBy:"
#define S_xorBy       "xorBy:"

// Reverse operators
#define S_rincBy      "r" S_incBy
#define S_rdecBy      "r" S_decBy
#define S_rmulBy      "r" S_mulBy
#define S_rdivBy      "r" S_divBy
#define S_rmodBy      "r" S_modBy
#define S_rexpBy      "r" S_expBy
#define S_rbitandBy   "r" S_bitandBy
#define S_rbitorBy    "r" S_bitorBy
#define S_rbitxorBy   "r" S_bitxorBy
#define S_rshiftrBy   "r" S_shiftrBy
#define S_rshiftlBy   "r" S_shiftlBy
#define S_radd        "r" S_add
#define S_rsub        "r" S_sub
#define S_rmul        "r" S_mul
#define S_rdiv        "r" S_div
#define S_rmod        "r" S_mod
#define S_rexp        "r" S_exp
#define S_rbitand     "r" S_bitand
#define S_rbitor      "r" S_bitor
#define S_rbitxor     "r" S_bitxor
#define S_rshiftr     "r" S_shiftr
#define S_rshiftl     "r" S_shiftl

#define S_req         "r" S_eq
#define S_rgt         "r" S_gt
#define S_rlt         "r" S_lt
#define S_rge         "r" S_ge
#define S_rle         "r" S_le
#define S_rne         "r" S_ne
#define S_r_and       "r" S__and
#define S_r_or        "r" S__or
#define S_r_xor       "r" S__xor
#define S_randBy      "r" S_andBy
#define S_rorBy       "r" S_orBy
#define S_rxorBy      "r" S_xorBy
// End of Reverse Operators

#define S_ulong       T_ulong
#define S_long        T_long
#define S_unsigned    T_uint
#define S_uint        T_uint
#define S_int         T_int
#define S_short       T_short
#define S_char        T_char
#define S_double      T_double
#define S_float       T_float
#define S_bool        T_bool
#define S_pointer     T_pointer
#define S_string      "string"
#define S_tostr       S_string

#define S_address     "address"
#define S_value       "value:"
#define S_neg         "neg"
#define S_print       "print"
#define S_println     "println"
#define S_init        "init:"

#define S_cast_ulong  S_ulong
#define S_cast_long   S_long
#define S_cast_uint   S_unsigned
#define S_cast_int    S_int
#define S_cast_short  S_short
#define S_cast_char   S_char
#define S_cast_double S_double
#define S_cast_float  S_float
#define S_cast_bool   S_bool

#endif // __SELECTORS_H__
