#ifndef __OMTYPES_H__
#define __OMTYPES_H__

enum {
  CLSID_NOCLASS  =  0,
  CLSID_RTMGR    =  1,
  CLSID_OCLASS       ,

  CLSID_ULONG        ,
  CLSID_LONG         ,
  CLSID_UINT         ,
  CLSID_INT          ,
  CLSID_SHORT        ,
  CLSID_CHAR         ,
  CLSID_DOUBLE       ,
  CLSID_FLOAT        ,
  CLSID_BOOL         ,
  CLSID_POINTER      , // 12

  CLSID_STRING       ,

  // CLSID_SEL          ,
  // CLSID_APP          ,

  CLSID_ULONGVECELEM ,
  CLSID_LONGVECELEM  ,
  CLSID_UINTVECELEM  ,
  CLSID_INTVECELEM   ,
  CLSID_SHORTVECELEM ,
  CLSID_CHARVECELEM  ,
  CLSID_DBLVECELEM   ,
  CLSID_FLTVECELEM   ,
  CLSID_BOOLVECELEM  ,
  CLSID_PTRVECELEM   , // 23

  CLSID_ULONGVECTOR  ,
  CLSID_LONGVECTOR   ,
  CLSID_UINTVECTOR   ,
  CLSID_INTVECTOR    ,
  CLSID_SHORTVECTOR  ,
  CLSID_CHARVECTOR   ,
  CLSID_DBLVECTOR    ,
  CLSID_FLTVECTOR    ,
  CLSID_BOOLVECTOR   ,
  CLSID_PTRVECTOR    ,
  CLSID_REFVECTOR    ,
  CLSID_OBJVECTOR    , // 35

  CLSID_ITER         ,
  CLSID_INPITER      ,
  CLSID_FWDITER      ,
  CLSID_BIDITER      ,
  CLSID_RAITER       ,
  CLSID_COUNTITER    , // 41
  CLSID_ULONGVECITER ,
  CLSID_LONGVECITER  ,
  CLSID_UINTVECITER  ,
  CLSID_INTVECITER   ,
  CLSID_SHORTVECITER ,
  CLSID_CHARVECITER  ,
  CLSID_DBLVECITER   ,
  CLSID_FLTVECITER   ,
  CLSID_BOOLVECITER  ,
  CLSID_PTRVECITER   ,
  CLSID_OBJVECITER   , // 52

  CLSID_WSTRING      ,

  CLSID_EXCEPT       ,
  CLSID_BAD_CLASS    ,
  CLSID_BAD_ARG      ,
  CLSID_BAD_ARGC     ,
  CLSID_BAD_USAGE    ,
  CLSID_BAD_CORECOPY ,
  CLSID_RTE          ,

  CLSID_STRITER      ,
  CLSID_STRRITER     ,

  CLSID_BLOCK        ,
  CLSID_NULLOBJ      ,

  CLSID_ULONGSET     , // 65
  CLSID_LONGSET      ,
  CLSID_DBLSET       ,
  CLSID_STRSET       ,
  CLSID_REFSET       ,
  CLSID_OBJSET       , // 70

  CLSID_ULONGSETITER ,
  CLSID_LONGSETITER  ,
  CLSID_DBLSETITER   ,
  CLSID_STRSETITER   ,
  CLSID_OBJSETITER   , // 75

  CLSID_RANGE        ,

  CLSID_PACKAGE      ,

#if 0
  CLSID_STREAMBUF    ,
  CLSID_FILEBUF      ,
  CLSID_STRINGBUF    ,
  CLSID_MANIP        ,
  CLSID_IMANIP       ,
  CLSID_OMANIP       ,
  CLSID_IOS_BASE     ,
  CLSID_IOS          ,
  CLSID_ISTREAM      ,
  CLSID_OSTREAM      ,
  CLSID_IOSTREAM     ,
  CLSID_IFSTREAM     ,
  CLSID_OFSTREAM     ,
  CLSID_FSTREAM      ,
  CLSID_ISSTREAM     ,
  CLSID_OSSTREAM     ,
  CLSID_SSTREAM      ,
  CLSID_ISTREAMITER  ,
  CLSID_OSTREAMITER  ,
  CLSID_ISBITER      ,
  CLSID_OSBITER      ,

  CLSID_RITER        ,
#endif // 0

  CLSID_FACET        ,
  CLSID_LOCALE       ,

  CLSID_LAST        =  CLSID_LOCALE,
  CLSID_END         =  CLSID_LAST+1,

  CLSID_OBJECT      =  CLSID_OCLASS,
  CLSID_DBL         =  CLSID_DOUBLE,
  CLSID_FLT         =  CLSID_FLOAT,
  CLSID_PTR         =  CLSID_POINTER,
  CLSID_VECTOR      =  CLSID_OBJVECTOR,
  CLSID_VECITER     =  CLSID_OBJVECITER,

  CLSID_ULONG_PROXY = CLSID_ULONGVECELEM,
  CLSID_LONG_PROXY  = CLSID_LONGVECELEM,
  CLSID_UINT_PROXY  = CLSID_UINTVECELEM,
  CLSID_INT_PROXY   = CLSID_INTVECELEM,
  CLSID_SHORT_PROXY = CLSID_SHORTVECELEM,
  CLSID_CHAR_PROXY  = CLSID_CHARVECELEM,
  CLSID_DBL_PROXY   = CLSID_DBLVECELEM,
  CLSID_FLT_PROXY   = CLSID_FLTVECELEM,
  CLSID_BOOL_PROXY  = CLSID_BOOLVECELEM,
  CLSID_PTR_PROXY   = CLSID_PTRVECELEM,
};

#define T_NOCLASS            ""
#define T_runtime_manager    "runtime manager"
#define T_object             "object"
#define T_OCLASS             T_OBJECT

#define T_ulong              "ulong"
#define T_long               "long"
#define T_uint               "uint"
#define T_int                "int"
#define T_short              "short"
#define T_char               "char"
#define T_double             "double"
#define T_float              "float"
#define T_bool               "bool"
#define T_pointer            "pointer"

#define T_string             "string"

#define T_ulong_type_proxy   "SDOM_ulong_type_proxy"
#define T_long_type_proxy    "SDOM_long_type_proxy"
#define T_uint_type_proxy    "SDOM_uint_type_proxy"
#define T_int_type_proxy     "SDOM_int_type_proxy"
#define T_short_type_proxy   "SDOM_short_type_proxy"
#define T_char_type_proxy    "SDOM_char_type_proxy"
#define T_double_type_proxy  "SDOM_double_type_proxy"
#define T_float_type_proxy   "SDOM_float_type_proxy"
#define T_bool_type_proxy    "SDOM_bool_type_proxy"
#define T_pointer_type_proxy "SDOM_pointer_type_proxy"

#define T_RTMGR              T_runtime_manager
#define T_OBJECT             T_object

#define T_ULONG              T_ulong
#define T_LONG               T_long
#define T_UINT               T_uint
#define T_INT                T_int
#define T_SHORT              T_short
#define T_CHAR               T_char
#define T_DOUBLE             T_double
#define T_FLOAT              T_float
#define T_BOOL               T_bool
#define T_POINTER            T_pointer

#define T_STRING             T_string

#define T_ULONG_PROXY        T_ulong_type_proxy
#define T_LONG_PROXY         T_long_type_proxy
#define T_UINT_PROXY         T_uint_type_proxy
#define T_INT_PROXY          T_int_type_proxy
#define T_SHORT_PROXY        T_short_type_proxy
#define T_CHAR_PROXY         T_char_type_proxy
#define T_DBL_PROXY          T_double_type_proxy
#define T_FLT_PROXY          T_float_type_proxy
#define T_BOOL_PROXY         T_bool_type_proxy
#define T_PTR_PROXY          T_pointer_type_proxy

enum {
  oxmanip_boolalpha = 0,
  oxmanip_noboolalpha,
  oxmanip_showbase,
  oxmanip_noshowbase,
  oxmanip_showpoint,
  oxmanip_noshowpoint,
  oxmanip_showpos,
  oxmanip_noshowpos,
  oxmanip_skipws,
  oxmanip_noskipws,
  oxmanip_uppercase,
  oxmanip_nouppercase,
  oxmanip_unitbuf,
  oxmanip_nounitbuf,
  oxmanip_internal,
  oxmanip_left,
  oxmanip_right,
  oxmanip_dec,
  oxmanip_hex,
  oxmanip_oct,
  oxmanip_fixed,
  oxmanip_scientific,

  oxmanip_ws,

  oxmanip_endl,
  oxmanip_ends,
  oxmanip_flush
};

#endif /* __OMTYPES_H__ */
