#ifndef __OTYPE_HPP__
#define __OTYPE_HPP__

#include "object.hpp"
#include "obasicTypeInfo.hpp"
#include "obasicTypeOps.hpp"

#if __cplusplus <= 201703L // C++14, 17

  template<class T>
  concept IsThrowing =
      std::is_same<T, ops::Div>::value   || std::is_same<T, ops::Mod>::value ||
      std::is_same<T, ops::Divby>::value || std::is_same<T, ops::Modby>::value;

#else // C++20

  template<class T>
  concept IsThrowing =
      std::is_same_v<T, ops::Div>   || std::is_same_v<T, ops::Mod> ||
      std::is_same_v<T, ops::Divby> || std::is_same_v<T, ops::Modby>;

#endif // __cplusplus


//============================================================================//
//::::::::::::::::::::::::::::::::   CType   ::::::::::::::::::::::::::::::::://
//============================================================================//
template<class t>
class CType {
  t value;

public:
  CType() : value(0) {}
  CType(t aValue) : value(aValue) {}
  // ~CType() {}

  std::string toString() const;

  CType<t>& operator=(const CType<t>& r) {
    value = r.value;
    return *this;
  }

  t& refValue() noexcept {return value;}
  t getValue() noexcept {return value;}
  const t getValue() const noexcept {return value;}
  void setValue(t r) noexcept {value = r;}

  inline void incValue() noexcept {++value;}
  inline void decValue() noexcept {--value;}
};

template<>
  inline void CType<void*>::incValue() noexcept {++*(char**)&value;}
template<>
  inline void CType<void*>::decValue() noexcept {--*(char**)&value;}

//============================================================================//
//::::::::::::::::::::::::::::::::   oxType   :::::::::::::::::::::::::::::::://
//============================================================================//

template<typename t>
class oxType : public OInstance, public CType<t> {
  template<typename s> friend class OTypeProxy;

public:
  oxType() = delete;
  oxType(OClass* cls) : OInstance(cls), CType<t>() {}

  oxType<t>& operator=(const oxType<t>& r) = default;
  oxType<t>& operator=(const CType<t>& r)
  { *static_cast<CType<t>*>(this) = r; return *this; }
  oxType<t>& operator=(const t& r)
  { *static_cast<CType<t>*>(this) = r; return *this; }

  ORet assignValue(OArg* argv, Session* m);

protected:
  template<typename _OP>
  const std::string getMethodName() const {
    return getClassName()+"::operator"+_OP::name();
  }

private:
  inline static ORef createWithValue(t value, Session* m) {
    ORef r = OBasicTypeInfo::getTypeClass<t>()->createObject(m);
    _P(t, r)->setValue(value);
    return r;
  }

  /**
   *  @brief Get operation result
   *
   *  @tparam OP the operation to be applied, @sa ops::Add etc
   *  @tparam T, basic type (long, bool etc)
   *  @param rhs, the RHS operand
   *  @return ORet, the result of the operation or @c undefined
   *          in case of error
   *
   *  @details Applies the low level static function
   *  _applyOperation(oxType<t>&, T, Session*)
   *  to obtain the result of the operation @c OP. If the
   *  operation throws an exception, it converts it to a
   *  VM run-time exception.
   */
  template<typename OP, typename T>
    requires IsThrowing<OP>
    ORet getResult(T rhs, Session*);

  /**
   *  @brief Get operation result
   *
   *  @tparam OP the operation to be applied, @sa ops::Add etc
   *  @tparam T, basic type (long, bool etc)
   *  @param rhs, the RHS operand
   *  @return ORet, the result of the operation or @c undefined
   *          in case of error
   *
   *  @details Applies the low level static function
   *  _applyOperation(oxType<t>&, T, Session*)
   *  to obtain the result of the operation @c OP. The operation
   *  @c OP should not throw any exceptions.
   */
  template<typename OP, typename T>
    requires (!IsThrowing<OP>)
    ORet getResult(T rhs, Session*);

  /**
   *  @brief Execute operation
   *
   *  @tparam OP the operation to be applied, @sa ops::Add etc
   *  @param o, object reference of the RHS operand
   *  @return ORet the result of the operation or undefined
   *          in case of error
   *
   *  @details Uses a switch table to get the type of the
   *  supplied argument @c o, and then properly applies
   *  oxType<t>::getResult(T, Session*).
   */
  template<typename OP>
    ORet execOperation(ORef o, Session*);

  /**
   *  @brief Apply operation
   *
   *  @tparam OP the operation to be applied, @sa ops::Add etc
   *  @tparam ROP reverse operation, e.g. ROP_sub etc
   *  @param argv, @c OArg[]: @c argv[0] points to the @c OVariable
   *          of the RHS operand
   *  @return ORet, the result of the operation or @c undefined
   *          in case of error
   *
   *  @details Applies oxType<t>::execOperation(T, Session*)
   *  taking care to cast the operand into its basic type if it
   *  is a basic type extension.
   */
  template<typename OP, typename ROP>
    ORet applyOperation(OArg* argv, Session*);

  /** Generic binary operation */
  template<typename OP, typename ROP>
    ORet genericBinaryOperation(OArg* argv, Session* m);
  /** Generic binary operation for throwing operations */
  template<typename OP, typename ROP>
    ORet genericBinaryOperationThrow(OArg* argv, Session* m);
  /** Generic binary operation without RHS cast */
  template<typename OP, typename ROP>
    ORet genericBinaryOperationNoCast(OArg* argv, Session* m);
  /** Generic pointer comparison operation */
  template<typename OP, typename ROP>
    ORet genericPointerCmpOperation(OArg* argv, Session* m);
  /** Generic cast operation */
  template<typename OP>
    ORet genericCastOperation(Session* m);

public:
  ORef copy(const_ORef r) {
    this->setValue(static_cast<const oxType*>(r)->getValue());
    return this;
  }

  ORef swapValue(ORef r) {
    std::swap(this->refValue(), static_cast<oxType*>(r)->refValue());
    return this;
  }

  _METHOD (incBy        );
  _METHOD (decBy        );
  _METHOD (mulBy        );
  _METHOD (divBy        );
  _METHOD (modBy        );
  _METHOD (expBy        );
  _METHOD (bandw        );
  _METHOD (borw         );
  _METHOD (bxorw        );
  _METHOD (shrw         );
  _METHOD (shlw         );
  _METHOD (add          );
  _METHOD (sub          );
  _METHOD (mul          );
  _METHOD (div          );
  _METHOD (mod          );
  _METHOD (exp          );
  _METHOD (bor          );
  _METHOD (bxor         );
  _METHOD (band         );
  _METHOD (shr          );
  _METHOD (shl          );
  _METHOD (eq           );
  _METHOD (gt           );
  _METHOD (lt           );
  _METHOD (ge           );
  _METHOD (le           );
  _METHOD (ne           );
  _METHOD (neg          );
  _METHOD (inc          );
  _METHOD (pinc         );
  _METHOD (dec          );
  _METHOD (pdec         );
  _METHOD (_and         );
  _METHOD (_or          );
  _METHOD (_xor         );
  _METHOD (_not         );
  _METHOD (addr         );
  _METHOD (bitinv       );
  _METHOD (ommValue     );
  _METHOD (cast_ulong   );
  _METHOD (cast_long    );
  _METHOD (cast_uint    );
  _METHOD (cast_int     );
  _METHOD (cast_short   );
  _METHOD (cast_char    );
  _METHOD (cast_double  );
  _METHOD (cast_float   );
  _METHOD (cast_bool    );
  _METHOD (cast_pointer );
  _METHOD (print        );
  _METHOD (println      );
  _METHOD (ommToString  );

private:
  _DECLARE_METHOD_TABLE();
};

#if defined(__clang__)
_TEMPLATE_DEFINITION(oxType, OInstance);
#endif // defined(__clang__)

//============================================================================//
//:::::::::::::::::::::::::::::   oxType_Class   ::::::::::::::::::::::::::::://
//============================================================================//
template<typename t>
class oxType_Class : public OClass {
public:
  /**
   *  @brief Constructor for this and C-derived classes
   */
  oxType_Class(Session* m, const std::string& clsname) :
    OClass(m, clsname) {}
  /**
   *  @brief Constructor for script-derived classes
   */
  oxType_Class(Session* m, const std::string& base, const std::string& clsname) :
    OClass(m, base, clsname) {}
  /**
   *  @brief Constructor for mutable classes
   */
  oxType_Class(Session* m, const OClass* base, const std::string& clsname) :
      OClass(m, base, clsname) {}

  virtual ~oxType_Class() override {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(oxType<t>); }

  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<oxType<t>>(ou); }

  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<oxType<t>>(m); }

  virtual void destructCore(OInstance* o, Session*) override
  { if (!freeCached<oxType<t>>(o)) delete static_cast<oxType<t>*>(o); }

  virtual ORef copyCore(ORef tgt, const_ORef src, Session*) const override
  { return static_cast<oxType<t>*>(tgt)->copy(src); }

  virtual ORef moveCore(ORef tgt, ORef src) const override
  { return static_cast<oxType<t>*>(tgt)->swapValue(src); }

  virtual bool finalize(Session* m) override {
    if (!OClass::finalize(m)) return false;
    cacheCoreMethodAddressCopy();
    return true;
  }

  _ASSOCIATE_MCLASS_WITH(oxType<t>)
};

//============================================================================//
//:::::::::::::::::::::::::::::::   oxBlock   :::::::::::::::::::::::::::::::://
//============================================================================//
typedef oxType<int> oxBlock;
_BEGIN_MCLASS_DECLARATION(Block, "block")
_END_MCLASS_DECLARATION(Block)

inline int g_blockId(ORef r) {
  return static_cast<oxBlock*>(r)->getValue();
}

inline int& g_refBlockId(ORef r) {
  return static_cast<oxBlock*>(r)->refValue();
}

//============================================================================//
//::::::::::::::::::::::   Global Functions & Macros   ::::::::::::::::::::::://
//============================================================================//

template<typename T>
  T g_typeValue(ORef r) noexcept
  { return static_cast<oxType<T>*>(r)->getValue(); }

template<typename t>
  t g_castValue(OArg* argv, Session* m) noexcept {
    oxType<t> thisTypeInstance(OBasicTypeInfo::getTypeClass<t>());
    // set value to this instance (it knows how to cast)
    thisTypeInstance.assignValue(argv, m);
    return thisTypeInstance.getValue();
  }

template<typename t>
  t g_castValue(ORef o, Session* m) noexcept {
    OVariable var{o};
    OArg argv[1]{&var};
    return g_castValue<t>(argv, m);
  }

#define _PULONG(a)     _P(unsigned long, a)
#define _PLONG(a)      _P(long, a)
#define _PUINT(a)      _P(unsigned, a)
#define _PINT(a)       _P(int, a)
#define _PSHORT(a)     _P(short, a)
#define _PCHAR(a)      _P(char, a)
#define _PDOUBLE(a)    _P(double, a)
#define _PFLOAT(a)     _P(float, a)
#define _PBOOL(a)      _P(bool, a)
#define _PPTR(a)       _P(void*, a)

#endif // __OTYPE_HPP__
