#ifndef __OPACKAGE_HPP__
#define __OPACKAGE_HPP__

#include "object.hpp"
#include "opackdev.h"
#include "odev.h"
#include "ortmgr.hpp"

/**
 *  @class Package type
 */
enum : unsigned {
  PKG_UNKNOWN = 0,
  PKG_CORE,
  PKG_INCLUDE,
  PKG_PACKAGE,
  PKG_PLUGIN = PKG_CORE,
};

struct PackageImportResult {
  int rc;
  std::string itemNotFound;
};

/**
 *  @class Package
 *  @brief Package object representation
 */
class Package {
protected:

  using string_vector = std::vector<std::string>;
  using string_pclass_assoc = std::vector<std::pair<std::string, OClass*>>;
  using string_object_assoc = std::vector<std::pair<std::string, ORef>>;

private:
  /**
   *  @brief Table of top-level packages
   *  @details They are local packages or scripts imported from the
   *  main program. A local package can import child packages, the
   *  "importees," but not export anything. Child packages can do
   *  both importing <u>and</u> exporting.
   */
  static std::vector<Package*> s_topLevelPackages_;

public:
  /**
   *  @brief Unique global identifier of package
   */
  std::string name;
  /**
   *  @brief Package version
   */
  std::string version;
  /**
   *  @brief Local identifier of package
   *  @details It defines a namespace for the imported types
   *  (classes). Each class is accessed by prepending the package
   *  `id` followed by a dot. Package variables are accessed as
   *  members of the variable representing the package object.
   */
  std::string id;
  /**
   *  @brief Package file
   *  @details It can be an absolute or relative path. Paths are
   *  relative to the current directory or the `lib` subdirectory
   *  of it.
   */
  std::string fileName;
  /**
   *  @brief OS handle of the shared library containing the package
   */
  void* hlib{nullptr};
  /**
   *  @brief Index of script package
   */
  int packageBlock{-1};
  /**
   *  @brief Function used to import package information
   *
   *  @details This function is found in the shared (dynamic
   *  link) library of a core package. It is used to load the
   *  information (exported types and variables, and other
   *  package data) exported by the library.
   */
  PackageInfoImporter importPackageInfo{nullptr};
  /**
   *  @brief Package type
   *
   *  @details It is deterimined from the file's extension.
   *  When it has no extension the standard extension `.is`
   *  of a script is assumed.
   */
  unsigned type{PKG_UNKNOWN};
  /**
   *  @brief Parent package
   *  @details The package that imported this package
   */
  Package* importer_{nullptr};
  /**
   *  @brief List of child packages
   *  @details The list of packages imported by this package
   */
  std::vector<Package*> importees_;
  /**
   *  @brief Data for the types (classes) imported from the package
   */
  string_pclass_assoc typeImports_;
  /**
   *  @brief Data for the variables imported from the package
   *  @details The imported variables become
   */
  string_object_assoc varImports_;
  /**
   *  User requested imports
   */
  string_vector requestedImports_;
  /**
   *  User specifed exports
   */
  string_vector specifiedExports_;
  /**
   *  @brief Data for the types (classes) exported by the package
   */
  string_pclass_assoc typeExports_;
  /**
   *  @brief Data for the variables exported by the package
   */
  string_object_assoc varExports_;

  /**
   *  Check if package is a core library
   */
  bool isCoreLibrary() const;
  /**
   *  Check if package is a script library
   */
  bool isScriptPackage() const;
  /**
   *  Check if package is a script include
   */
  bool isScriptInclude() const;
  /**
   *  Determine package type and set `type` field
   */
  void setPackageType();
  /**
   *  @brief Load and import a package
   *
   *  @details This nethod combines void load(Session*) with
   *  void importFromCorePackage(Session*, const string_vector&)
   *  or void importFromScriptPackage(Session*) to load and
   *  import a package. It assumes that the data structure
   *  requestedImports_ is filled in with @em raw type and
   *  variable identifiers, and the package data fileName and
   *  id specified.
   */
  void importPackage(Session* m);
  /**
   *  @brief Load package and set package type
   *
   *  @details Load core library, script package, or plain
   *  script. The package type is determined from the file
   *  extension. For core packages, is uses the <em>Operating
   *  System</em>'s loader to load the package. For packages
   *  or scripts, it selects a suitable loader:
   *  loadScriptPackage(Session*, const std::string&)
   */
  void load(Session*);
  /**
   *  @brief Load script package
   *
   *  @details It invokes int g_loadPackage(Session*,
   *  const std::string&, const std::string&) which in turn
   *  calls VirtualMachine::loadPackage(const std::string&,
   *  const std::string&) to compile and embbed the package
   *  script into the main script. It will be used by the
   *  importing methods to import types (classes) and objects.
   */
  void loadScriptPackage(Session* m, const std::string& filePath);
  /**
   *  @brief Import classes and variables from core package
   *
   *  @details This method does the following jobs:
   *  @li It calls the method importCorePackage(Session*) to
   *      import from the package the types (classes) and
   *      variables specified in `requestedImports_` or all if
   *      it is empty
   *  @li Checks for errors
   *  @li Calls extend(Session*) to make the imported variables
   *      properties of the package object.
   */
  void importFromCorePackage(Session*);
  /**
   *  @brief Import classes and variables from script package
   *
   *  @details This method does the following jobs:
   *  @li It calls the method importScriptPackage(Session*)
   *      to import types (classes) and variables from the
   *      package as specified in requestedImports_
   *  @li Checks for errors
   *  @li Calls extend(Session*) to make the imported variables
   *      properties of the package object.
   */
  void importFromScriptPackage(Session*);
  /**
   *  @brief Export requested items
   *
   *  @details This method uses the information contained in
   *  `requestedImports_` and the supplied `exports` list to
   *  export the requested items, types and variables. The
   *  `exports` list, as it is the case with `requestedImports_`,
   *  contains @em raw type and/or variable identifiers.
   *  @par It does the following jobs:
   *  @li it builds the list (`vector<string>`) of export types
   *      (classes) using the `registeredTypes` list of the
   *      `PackageInfo` data structure maintained by `Session`,
   *      the <em>runtime manager</em>
   *  @li it builds the list of export variables. Export items
   *      that were not classified as types are assumed to be
   *      export variables
   *  @li it uses the method classifyImports(Session*) to
   *      classify the `requestedImports_` as types or variables
   *  @li it makes @em inaccessible all registered types that
   *      were not imported
   *  @li it invokes the `VirtualMachine::exportVars` method
   *      through g_exportVars(Session*, int,
   *      Package::string_object_assoc) to do the actual
   *      exportation of the requested variables, and finally,
   *  @li it checks for errors
   */
  void exportRequestedItems(Session* m);
  /**
   *  @brief Get exportable items
   *
   *  @details The exported items are the types (classes) and
   *  objects specified by the script command `export:`. They
   *  are subsets of the sets of all types and objects that can
   *  be exported, the sets of <em>exportable types</em> and
   *  <em>objects</em>.
   *  @par The set of <em>exportable types</em> consists of
   *  @li the types (classes) defined in this package
   *  @li the types @e imported from @e importees (packages
   *      imported by this package) through `import:`
   *      declaratives
   *  @par The set of <em>exportable variables</em> consists
   *  of
   *  @li the variables defined and instantiated in this
   *      package
   *  @li the variables @e imported from @e importees (packages
   *      imported by this package) through `import:`
   *      declaratives
   *  @par This method does the following jobs:
   *  @li it builds the list of exportable types and stores it
   *      in `typeExports_`
   *  @li it builds the list of exportable variables and stores
   *      it in `varExports_`
   *  @par The non-exported items will be subsequently removed
   *  from these lists.
   */
  void getExportableItems(Session* m);
  /**
   *  @brief Export nothing
   *
   *  @details It makes all registered types (classes) inaccessible.
   */
  void exportNothing(Session* m);
  /**
   *  @brief Classify imports according to lists of exports
   *
   *  @details It classifies the items contained in
   *  `requestedImports_` as @em types (classes) or
   *  @em variables. Consequently, the set of items classified
   *  as types are (<em>must be</em>) a subset of `typeExports_`
   *  and that of variables are a subset of `varExports_`. If
   *  an import (`requestedImports_`'s item) does not match any
   *  of the exported types and variables, a runtime VM-exception
   *  is thrown.
   *  @par It fills in `varImports_` with pairs of strings
   *  taken from `varExports_`, and `typeImports_` with pairs
   *  of strings taken from `typeExports_`. An empty
   *  `requestedImports_` list signifies that all exports must
   *  be imported.
   */
  void classifyImports(Session* m);
  /**
   *  Add variables to the package object making them its properties
   */
  void extend(Session*);
  /**
   *  @brief Import classes and variables from core package
   *  @details The imported classes (types) are registered in
   *  the namespace defined by `id`. The imported variables are
   *  accessed as properties of the package.
   *  @par The syntax
   *  @code
   *  var = "package-file"P;
   *  @endcode
   *  is required to import variables, otherwise a runtime error
   *  is thrown. Packages containing only classes can be imported
   *  with both syntaxes:
   *  @code
   *  $auto: var type: "package" => file: "package-file"
   *                             => importAll;
   *  @endcode
   */
  PackageImportResult importCorePackage(Session* m);
  /**
   *  @brief Import script package
   *
   *  @details It invokes the top level block of the package as
   *  a method of this package to import the requested items.
   *  The name of the method is `importFromScriptPackage:` and
   *  its argument is the package's `id`. This will invoke the
   *  script's `exports:` or `exportAll` declarative, which
   *  will finish the exportation/importation transaction.
   */
  void importScriptPackage(Session*);
  /**
   *  @brief Get importing function address
   *  @details Loads the function that will be used to import
   *  the package's export information from shared (dynamic
   *  link) library. The function address is stored in the
   *  property `importPackageInfo`.
   */
  bool getImportingFunction();
  /**
   *  @brief Import script
   *
   *  @details It invokes the top level block of the script as
   *  a method of this package to import all types. No objects
   *  are imported. The name of the method is `importScript:`
   *  and its argument is the package's `id`. Also, no exports
   *  are allowed.
   */
  void importScript(Session*);

protected:
  /**
   *  @brief Check if a package imports a package with the supplied id
   *  @details If the parameter `importer` is a `nullptr` it
   *  looks into the global package list, otherwise the
   *  importees list is searched for the supplied id.
   */
  static bool s_importsPackageWithId(const Package* importer,
                                     const std::string& theId);
  /**
   *  @brief Register package
   */
  bool registerPackage(Session* m);
  void setFilePath(Package* importer, const std::string& path);
  const std::string& getFilePath() const noexcept { return fileName; }

private:
  static Package* s_findMainPackage(std::function<bool(Package*)> pred);
  static Package* s_scanPackagesUntil(std::function<bool(Package*)>);
  std::string getPathInSubdir(const std::string& path) const;
  int getScriptPackageType() const;
  OClass* getImportType(const std::string& typeName) const;
  Package* findImportee(std::function<bool(const Package*)> pred) const;
  bool isTopLevel() const noexcept { return importer_ == nullptr; }
  bool isImportee() const noexcept { return !isTopLevel(); }
  Package* findNonameImporterWithPath(const std::string& path) const;
  Package* scanImporteesUntil(std::function<bool(Package*)>);
  void importCorePackageVar(Session* m, const std::string& varName,
    const PVarConstructInfo& exportItem);
};

#if 0
OM_BEGIN_CLASS_DECLARATION(Package)
#endif // 0
OM_BEGIN_TYPE_DECLARATION(Package)
  static std::pair<int, std::string>
      s_getUserSpec(int argc, OArg* argv, std::vector<std::string>& userSpec,
          std::function<bool(const std::string&)> isUnacceptableItem);

  OM_METHOD(ommAs);
  OM_METHOD(ommAsImport);
  OM_METHOD(ommImport);
  OM_METHOD(ommImportAll);
  /**
   *  @brief Set file name
   *
   *  @details It is used internally by the @em Invoker to set
   *  the file containing the package. Beside this, it implements
   *  the script method `file:`.
   */
  OM_METHOD(ommSetValue);
  /**
   *  @brief Export script package types and variables
   *
   *  @details Exports a subset of a script-package's types and
   *  variables. It can be specified in a number of ways:
   *  @li As a set of strings
   *  @li As a vector of strings
   *  @li As a variable argument list
   *  @li As a single string
   *  @li As a star string to import all types and variables
   *
   *  @par This method can only be called from within a
   *  @em script package and it must be the very last
   *  declarative in the script. A runtime error condition is
   *  thrown if it is called elsewhere.
   */
  OM_METHOD(ommExport);
  /**
   *  @brief Export all of a script package's types and variables
   *
   *  @details This method can only be called from within a
   *  @em script package and it must be the very last declarative
   *  in the script. A runtime error condition is thrown if it
   *  is called elsewhere.
   */
  OM_METHOD(ommExportAll);
  /**
   *  @brief Export objects
   *
   *  @details This method is used to export objects rather
   *  than variables and set their variable names. The exported
   *  objects will be accessible from the calling module as
   *  members of the package object as usual.
   *  @par Example:
   *  @code{.cpp}
   *  . objectExports: "key1" : "its value" :
   *                   "key2" : x + 2;
   *  @endcode
   */
  OM_METHOD(ommExportObjects);
  /**
   *  @brief Export nothing
   *
   *  @details It must be placed as the last declarative of a
   *  scipt package when exporting variables with the script
   *  method `varExports:`
   */
  OM_METHOD(ommExportNothing);
#if 0
OM_END_CLASS_DECLARATION()
#endif // 0
OM_END_TYPE_DECLARATION()

#if 0
OM_DECLARE_MCLASS(Package, "package");
#endif // 0
OM_BEGIN_META_TYPE_DECL(Package)
  OM_META_TYPE_CTORS(Package)
public:
  static OClass* ctor(Session*, const std::string& typeName);
  virtual OClass* createClass(Session* m, const std::string& c)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& c)
      const override;
  virtual size_t getAllocSize() const override { return sizeof(OT); }
  virtual void createObjectCache(const ObjectUsage& ou) override {}
  virtual OInstance* constructCore(Session*) override { return new OT{this}; }
  virtual void destructCore(OInstance* self, Session*) override
  { delete static_cast<OT*>(self); }
  virtual ORef copyCore(ORef l, const_ORef r, Session* m) const override {
    if (r->getRootClass() != getRootClass()) return nullptr;
    *static_cast<OT*>(l) = *static_cast<const OT*>(r);
    return l;
  }
  virtual ORef moveCore(ORef lhs, ORef rhs) const override {
    if (rhs->getRootClass() != getRootClass()) return nullptr;
    *static_cast<OT*>(lhs) = std::move(*static_cast<OT*>(rhs));
    return lhs;
  }
OM_END_META_TYPE_DECL(Package)

#endif // __OPACKAGE_HPP__
