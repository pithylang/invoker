#ifndef __SEOAPI_H_
#define __SEOAPI_H_

extern "C" {

class Session;
class OClass;

Session *omInitialize();
void omTerminate(Session* m, bool destroyClasses);
OClass *omGetClass(Session *m, const char *className);

}

#endif /* __SEOAPI_H_ */
