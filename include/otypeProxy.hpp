#ifndef __OTYPEPROXY_HPP__
#define __OTYPEPROXY_HPP__

#include "ortmgr.hpp"
#include "otype.hpp"

template<class t>
  class OTypeProxy_Class;

/**
 *  @class OTypeProxy
 *  @brief Basic type proxy object
 */
template<class t>
class OTypeProxy : public oxType<t> {
  friend class OTypeProxy_Class<t>;
  t* proxiedData_;

public:
  OTypeProxy(OClass* cls) : oxType<t>{cls}, proxiedData_{nullptr} {}

  /**
   *  @brief Copy operator
   *  @details Override the default copy operator to copy the
   *  value ONLY. The proxy keeps referring to the original
   *  object.
   */
  OTypeProxy<t>& operator=(const OTypeProxy<t>&) = default;
  /** @brief Copy operator */
  OTypeProxy<t>& operator=(const CType<t>& r)
  { *static_cast<CType<t>*>(this) = r; return *this; }

  bool initialized() const noexcept { return proxiedData_; }
  t* getElemPtr() { return proxiedData_; }
  void setElemPtr(t *p) { proxiedData_ = p; }
  void setElemValue(t value) { *proxiedData_ = value; }
  void updateValue() { *proxiedData_ = this->getValue(); }
  void setUpdateValue(t value) { this->setValue(value); updateValue();  }

  /**
   *  @brief Render proxy object into basic type object
   *  @details This function is used by
   *  ORef g_renderProxyObject(ORef, Session*)
   *  @param m @c Session*
   *  @return ORef the output rendered object
   */
  inline ORef render(Session* m) const {return render(false, m);}
  ORef render(bool forceNoCache, Session* m) const {
    ORef r = BTI::getTypeClass<t>()->createObject(forceNoCache, m);
    if (m->exceptionPending()) return nullptr;
    *_P(t, r) = this->getValue();
    return r;
  }

private:
  template<typename _OP>
    ORet genericOperation(oxType<t>&, OArg* argv, Session* m);
  template<typename _OP>
    ORet genericSetOperation(OArg* argv, Session* m);
  template<typename _OP>
    inline ORet genericOperation(OArg* argv, Session* m) {
      oxType<t> typeObj{OBasicTypeInfo::getTypeClass<t>()};
      return genericOperation<_OP>(typeObj, argv, m);
    }
  template<typename _OP>
    ORet genericReverseOperation(OArg* argv, Session* m);

protected:
  _METHOD (ommValue    );
  _METHOD (ommCopy     );
  _METHOD (ommGetValue );
  _METHOD (add         );
  _METHOD (sub         );
  _METHOD (mul         );
  _METHOD (div         );
  _METHOD (mod         );
  _METHOD (exp         );
  _METHOD (band        );
  _METHOD (bor         );
  _METHOD (bxor        );
  _METHOD (shr         );
  _METHOD (shl         );
  _METHOD (incBy       );
  _METHOD (decBy       );
  _METHOD (mulBy       );
  _METHOD (divBy       );
  _METHOD (modBy       );
  _METHOD (expBy       );
  _METHOD (bandw       );
  _METHOD (borw        );
  _METHOD (bxorw       );
  _METHOD (shrw        );
  _METHOD (shlw        );
  _METHOD (eq          );
  _METHOD (lt          );
  _METHOD (gt          );
  _METHOD (le          );
  _METHOD (ge          );
  _METHOD (ne          );
  _METHOD (_and        );
  _METHOD (_or         );
  _METHOD (_xor        );
  _METHOD (_not        );
  _METHOD (inc         );
  _METHOD (dec         );
  _METHOD (pinc        );
  _METHOD (pdec        );
  _METHOD (addr        );
  _METHOD (rsub        );
  _METHOD (rdiv        );
  _METHOD (rmod        );
  _METHOD (rexp        );
  _METHOD (rshr        );
  _METHOD (rshl        );
  _METHOD (rincBy      );
  _METHOD (rdecBy      );
  _METHOD (rmulBy      );
  _METHOD (rdivBy      );
  _METHOD (rmodBy      );
  _METHOD (rexpBy      );
  _METHOD (rbandw      );
  _METHOD (rborw       );
  _METHOD (rbxorw      );
  _METHOD (rshrw       );
  _METHOD (rshlw       );

  _DECLARE_METHOD_TABLE();
};

/**
 *  @class OTypeProxy<bool>
 *  @brief Boolean type proxy object. Specialization of
 *  template @c OTypeProxy.
 */
template<>
class OTypeProxy<bool> : public oxType<bool> {
  friend class OTypeProxy_Class<bool>;
  typedef std::vector<bool>::reference bitreference;

  bitreference proxiedData_;

public:
  OTypeProxy(OClass* cls) : oxType<bool>{cls} {}

  /**
   *  @brief Copy operator
   *  @details Override the default copy operator to copy the value
   *  ONLY. The proxy keeps referring to the original object.
   */
  OTypeProxy<bool>& operator=(const OTypeProxy<bool>& r) = default;
  /** @brief Copy operator */
  OTypeProxy<bool>& operator=(const CType<bool>& r)
  { *static_cast<CType<bool>*>(this) = r; return *this; }

  bool initialized() const noexcept { return proxiedData_._M_p; }
  bitreference& getElemRef() { return proxiedData_; }
  void setElemRef(bitreference b) noexcept // { proxiedData_ = b; } // TODO
  { proxiedData_._M_p = b._M_p; proxiedData_._M_mask = b._M_mask; }
  void setElemPtr(bitreference b) noexcept { setElemRef(b); }
  void setElemValue(bool value) { proxiedData_ = value; }
  void updateValue() { proxiedData_ = this->getValue(); }
  void setUpdateValue(bool value) { this->setValue(value); updateValue(); }

  /**
   *  @brief Render proxy object into boolean object
   *  @details This function is used by
   *  ORef g_renderProxyObject(ORef, Session*)
   *  @param m @c Session*
   *  @return ORef the output rendered object
   */
  inline ORef render(Session* m) const {return render(false, m);}
  ORef render(bool forceNoCache, Session* m) const {
    ORef r = BTI::getTypeClass<bool>()->createObject(m);
    if (m->exceptionPending()) return nullptr;
    *_P(bool, r) = this->getValue();
    return r;
  }

private:
  template<typename _OP>
    ORet genericOperation(oxType<bool>&, OArg* argv, Session* m);
  template<typename _OP>
    ORet genericSetOperation(OArg* argv, Session* m);
  template<typename _OP>
    inline ORet genericOperation(OArg* argv, Session* m) {
      oxType<bool> typeObj{OBasicTypeInfo::getTypeClass<bool>()};
      return genericOperation<_OP>(typeObj, argv, m);
    }
  template<typename _OP>
    ORet genericReverseOperation(OArg* argv, Session* m);

protected:
  _METHOD (ommValue    );
  _METHOD (ommCopy     );
  _METHOD (ommGetValue );
  _METHOD (add         );
  _METHOD (sub         );
  _METHOD (mul         );
  _METHOD (div         );
  _METHOD (mod         );
  _METHOD (exp         );
  _METHOD (band        );
  _METHOD (bor         );
  _METHOD (bxor        );
  _METHOD (shr         );
  _METHOD (shl         );
  _METHOD (incBy       );
  _METHOD (decBy       );
  _METHOD (mulBy       );
  _METHOD (divBy       );
  _METHOD (modBy       );
  _METHOD (bandw       );
  _METHOD (borw        );
  _METHOD (bxorw       );
  _METHOD (shrw        );
  _METHOD (shlw        );
  _METHOD (eq          );
  _METHOD (lt          );
  _METHOD (gt          );
  _METHOD (le          );
  _METHOD (ge          );
  _METHOD (ne          );
  _METHOD (_and        );
  _METHOD (_or         );
  _METHOD (_xor        );
  _METHOD (_not        );
  _METHOD (rmod        );
  _METHOD (rexp        );
  _METHOD (rshr        );
  _METHOD (rshl        );
  _METHOD (rbandw      );
  _METHOD (rborw       );
  _METHOD (rbxorw      );
  _METHOD (rshrw       );
  _METHOD (rshlw       );

  _DECLARE_METHOD_TABLE();
};

template<class t>
class OTypeProxy_Class : public oxType_Class<t> {
  /** This class cannot be extended (no derived classes from this class!) */

public:
  OTypeProxy_Class(Session* m, const std::string& name);
  /** Constructor for this class only */
  OTypeProxy_Class(Session* m, const std::string& clsname, int baseclsid);

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;
  /** Construct Core */
  virtual ORef constructCore(Session* m) override
  { return this->template genericConstructCore<OTypeProxy<t>>(m); }
  /** Destruct Core */
  virtual void destructCore(OInstance* self, Session*) override
  { if (!OClass::freeCached<OTypeProxy<t>>(self)) delete static_cast<OTypeProxy<t>*>(self); }
  /** Get instance size */
  virtual size_t getAllocSize() const override {return sizeof(OTypeProxy<t>);}
  virtual void createObjectCache(const ObjectUsage& ou) override
  { OClass::genericCreateObjectCache<OTypeProxy<t>>(ou); }
  /**
   *  @brief Copy core method
   *  @details This method, if called, throws an exception.
   */
  virtual ORef copyCore(ORef self, const_ORef src, Session *m) const override;
  /**
   *  @brief Move core method
   *  @details This method, if called, throws an exception.
   */
  virtual ORef moveCore(ORef self, ORef src) const override;

  _ASSOCIATE_MCLASS_WITH(OTypeProxy<t>)
};

/**
 *  @brief Render proxy object into same type basic type object
 *  @details If the object is not a proxy object it is returned
 *  without any conversion. To check if the object was actually
 *  rendered use `rendered == o`, where `rendered` is the
 *  returned object.
 *  @par This function is internally used by
 *  ORef Session::dupRenderObject(OArg, Session*) and
 *  ORef g_moveObject(OArg, OArg, Session*)
 *  @param o the source proxy object
 *  @param m @c Session*
 *  @return ORef the output rendered object
 */
ORef g_renderProxyObject(ORef o, bool forceNoCache, Session* m);
inline ORef g_renderProxyObject(ORef o, Session* m)
{ return g_renderProxyObject(o, false, m); }

/**  @deprecated Use ORef Session::dupRenderObject(OArg) */
ORef g_duplicateObject(OArg var, Session* m);

#define _PVectorElem(t,id) static_cast<OTypeProxy<t>*>(id)
#define _PTypeProxy(t,id) static_cast<OTypeProxy<t>*>(id)
#define _PCTypeProxy(t,id) static_cast<const OTypeProxy<t>*>(id)

template<typename t>
  ORef g_convertProxy(ORef o, Session* m) noexcept {
    ORef result = BTI::getTypeClass<t>()->createObject(m);
    if (result)
      _P(t, result)->setValue(_PTypeProxy(t, o)->getValue());
    return result;
  }

#endif // __OTYPEPROXY_HPP__
