#ifndef __MCACHES_HPP__
#define __MCACHES_HPP__

#include <string>
#include <vector>
#include "odefs.h"

class Session;

/**
 *  @brief Selector cache
 *  @details It contains resolved selector addresses for the selector
 *  @c name. It is used when the @c USE_SELECTOR_CACHES is active,
 *  which is the default. It is pushed onto the stack as a variable
 *  type @c VARTYPE_SEL_CACHE. The selector symbol is converted into
 *  a static symbol during runtime, and the cache is permanently
 *  stored in the method's static data segment.
 */
struct SelectorCache {
  struct SelectorCacheRec {
    const OClass* caller;
    OAddr methodAddress;
    SelectorCacheRec() : caller(nullptr) {}
    SelectorCacheRec(const OClass* c) : caller(c) {}
    SelectorCacheRec(const OClass* c, const OAddr& a) : caller(c), methodAddress(a) {}
    bool operator==(const SelectorCacheRec& r) const noexcept
    { return caller == r.caller; }
  };

  /**
   *  @brief Method identifier
   *  @details @sa prefix
   */
  std::string name;
  /**
   *  @brief Method prefix
   *  @details The method prefix is a class, for example, in
   *  @code {.c++}
   *  BaseClass::methodName:extraArg:
   *  @endcode
   *  `BaseClass` is the prefix, and `methodName:extraArg:` is
   *  the method identifier.
   */
  std::string prefix;
  std::vector<SelectorCacheRec> table;

  SelectorCache() = default;
  SelectorCache(Session* m, const std::string& methodName) :
      name{methodName}, prefix{} { adjustName_(m); }

  inline const OAddr& addSelector(OClass* c, const OAddr& a)
  { table.emplace_back(c, a); return table.back().methodAddress; }
  std::tuple<const OAddr&, bool/*not responding*/>
  getSelectorForClass(OClass* c, OClass* caller, Session* m);
  const OAddr* lookup_(const OClass* c) const;
  void adjustName_(Session* m) noexcept;
};

/**
 *  @brief Resolved selector address
 *  @details This structure is used only when the @c USE_SELECTOR_CACHES is
 *  @em not active. It is pushed onto the stack as a variable type
 *  @c VARTYPE_SEL_DATA.
 */
struct SelectorData {
  std::string name;
  OClass* owner;
  OAddr methodAddress;
};

/**
 *  @brief Property cache
 *  @details It contains resolved selector addresses for the selector
 *  @c name. It is used when the @c USE_SELECTOR_CACHES is active,
 *  which is the default. It is pushed onto the stack as a variable
 *  type @c VARTYPE_SEL_CACHE. The selector symbol is converted into
 *  a static symbol during runtime, and the cache is permanently
 *  stored in the method's static data segment.
 */
struct PropertyCache {
  struct PropertyCacheRec {
    const OClass* owner;
    int offset;
    PropertyCacheRec() : owner(nullptr), offset(-1) {}
    PropertyCacheRec(const OClass* c, int ofs) : owner(c), offset(ofs) {}
    bool operator==(const PropertyCacheRec& r) const noexcept
    { return owner == r.owner; }
  };

  std::string name;
  std::vector<PropertyCacheRec> table;

  PropertyCache() = default;
  PropertyCache(const std::string& propName) : name(propName) {}

  inline int addOffset(const OClass* c, int ofs)
  { table.emplace_back(c, ofs); return ofs; }
  std::tuple<int/*member offset*/, bool/*accessible*/>
    getPropertyForClass(OClass* c, OClass* caller);
  int lookup_(const OClass* const c) const noexcept;
};

/**
 *  @brief Resolved selector address
 *  @details This structure is used only when the @c USE_SELECTOR_CACHES is
 *  @em not active. It is pushed onto the stack as a variable type
 *  @c VARTYPE_SEL_DATA.
 */
struct PropertyData {
  std::string name;
  OClass* owner;
  int offset;
};

#if USE_SELECTOR_CACHES
# define SELECTOR_CACHE(var) (var).selectorCache
#else // !USE_SELECTOR_CACHES
# define SELECTOR_CACHE(var) (var).selectorData
#endif // USE_SELECTOR_CACHES

#endif // __MCACHES_HPP__
