#ifndef __OCLASS_HPP__
#define __OCLASS_HPP__

#include <string>
#include <vector>
#include <forward_list>
// #include <functional>
#include "odev.h"
#include "omembers.hpp"
#include "ovariable.hpp"
#include "objectCache.hpp"
#include "util.hpp"
#include "odefs.h"

//============================================================================//
//:::::::::::::::::::::::::::::::::  OClass  ::::::::::::::::::::::::::::::::://
//============================================================================//

class OClassMgr;
class ObjectTreeNode;
class MethodListNode;
class Requirement;
struct ExceptionBase;
struct ObjectUsage;

/**
 *  @class OClass
 *  @brief Base object factory class (metaclass)
 *
 *  @details Each class of objects is an extension of @c OInstance
 *  and it is associated with an instance of @c OClass or one of
 *  its extensions. This class contains object related information,
 *  a pointer to its base class, and defines methods used for the
 *  creation of the associated @c OInstance. This is why we call it
 *  a @c factory @c class.
 *
 *  In script-defined classes forward references are not necessary.
 *  However, members of an undefined type (template members) can be
 *  used. Their class is specified dynamically during run-time at the
 *  time of their initialization. Classes containing template members
 *  are template classes.
 */
class _OM_LINK OClass : public Descriptor {
  friend class ORTMgr_Class;
  using SpecializationList = std::forward_list<const OClass*>;
  enum {
    CLSF_DEFAULT               = 0x0000,
    CLSF_HAS_CONSTRUCTOR       = 0x0001,
    CLSF_HAS_DESTRUCTOR        = 0x0002,
    CLSF_HAS_COPY_METHOD       = 0x0004,
    CLSF_HAS_TEMPLATE_MEMBERS  = 0x0008,

    CLSF_STATUS_CTOR_FAILED    = 0x0020,
    CLSF_STATUS_FAILURE_MASK   = 0x00c0,

    CLSF_STATUS_OPEN           = 0x0100,
    CLSF_STATUS_TEMPLATE       = 0x0200,
    CLSF_STATUS_READY          = 0x0400,
    CLSF_STATUS_MUTABLE        = 0x0800,
    CLSF_STATUS_HIDDEN         = 0x1000,
    CLSF_STATUS_TYPELESS_PROPS = 0x2000,
  };

  int                 classId_;      /**< Unique identifier of class */
  unsigned short      flags_;        /**< Class configuration flags */
  PropertyTable       props_;        /**< Vector of member definitions */
  MethodTable         methods_;      /**< Vector of method definitions */
  OClass*             baseClass_;    /**< Base class */
  const OClass*       rootClass_;    /**< Root class */
  /**
   *  @brief List of specializations
   *  @details List of specialization classes if `this` is a
   *  template class.
   */
  SpecializationList  specializations_;
  ushort              access_[3];    /**< Cached accessibilities */
  OAddr               constructor_;  /**< Cached script method address */
  OAddr               destructor_;   /**< Cached script method address */
  OAddr               copyOperator_; /**< Cached script method address */
  int                 classBlockId_; /**< Class block */
  /**
   *  @brief Instances of this class
   *  @details Applies to mutable classes only. It is 0 for
   *  unmutable classes.
   */
  unsigned            dedicatedUses_;
  std::string         prefix_;
  Requirement*        req_;
  std::unique_ptr<ObjectCacheBase> objectCache_;
//   oxEventTable       *eventTable_;

private:
  /**
   *  Special constructor for @c ORTMgr
   */
  OClass(OClassMgr*, const std::string& className);

public:
  /**
   *  @brief Constructor for core (root) classes
   *
   *  @details Use this constructor for this class and C-defined
   *  classes. Core or C-classes are C-implementations and have
   *  no base class. C-classes C-extending other classes are still
   *  root classes for the IOM and have no base class. However,
   *  C-extensions inherit the method table of their C-base class.
   */
  OClass(Session* m, const std::string& className);
  /**
   *  Constructor for script-defined classes
   */
  OClass(Session* m, const std::string& baseClassName,
         const std::string& className);
  /**
   *  Constructor for mutable classes
   */
  OClass(Session* m, const OClass* baseClass, const std::string& className);

  /**
   *  @brief Destructor
   *
   *  @details The method OClassMgr::deregisterClass(OClass*) must
   *  be called just prior to invoking the destructor.
   */
  virtual ~OClass() NOEXCEPT_IF_NDEBUG;

  virtual OClass* createClass(Session* m, const std::string& className) const;
  /**
   *  @brief Create mutable class
   *
   *  @details Mutable classes are used by a single instance.
   *  When a program requests that an object have additional
   *  instance data (properties) or methods, the IOM system
   *  creates an extension of the object's class and makes it
   *  the new object's class. This instance can subsequently
   *  be added members on the fly.
   *  @par On the fly object extension is a convenience feature
   *  that should not be overused.
   */
  virtual OClass* createMutableClass(Session*, const std::string& className) const;

  //::::::::::::::::::::::::   Class Management   :::::::::::::::::::::::::::://

  virtual bool finalize(Session*);
  OClass* getImmutableBase() const noexcept;
  std::vector<const OClass*> getMutableBases() const noexcept;

  //::::::::::::::::::::::::::   Template Classes   :::::::::::::::::::::::::://

  /**
   *  @brief Count template parameters
   *  @details Count the template parameters defined in a class
   *  hierarchy. Note that `this` class must be a template class.
   */
  size_t countTemplateParams() const NOEXCEPT_IF_NDEBUG;
  /**
   *  @brief Specialize template class
   *  @details Creates a specialization of `this` class according
   *  to the supplied template parameter types.
   *
   *  @param templateTypes vector of template parameter types
   *  @return `OClass*` the specialized type
   */
  OClass* specializeTemplateClass(Session*,
      const std::vector<const OClass*>& templateTypes);
  /**
   *  @brief Build template class name
   *  @details Builds an identifier (name) for a template class
   *  specialization (`this` class) according to the supplied
   *  template parameter types starting from the given base.
   *
   *  @param templateTypes vector of template parameter types
   *  @param base `size_t` the index of the template parameter
   *         type to start from
   *  @return `std::string` the identifier of the specialized type
   */
  std::string buildTemplateClassName(
      const std::vector<const OClass*>& templateTypes, size_t base) const;
  /**
   *  @brief Check if `this` type is a specialization of a template type
   */
  bool isSpecializationOf(const OClass* templateClass) const NOEXCEPT_IF_NDEBUG;
  /**
   *  @brief Get template parameter types
   *
   *  @details Get the template parameter types from a given
   *  script-method argument list. The arguments must correspond
   *  to the members of `this` class starting from the most
   *  derived class and going down to the most basic type.
   *  Non-template arguments of the most basic types in the
   *  hierarchy, not intermixing with template parameters, can
   *  be missing. They are default constructed when an instance
   *  is created.
   *
   *  @param argc number of arguments
   *  @param argv `OArg*` array of arguments
   *  @param m `Session*`
   *  @return `std::vector<const OClass*>` the template parameter types
   */
  std::vector<const OClass*> getTemplateParamTypes(int argc, OArg* argv,
      Session* m) const;
  /**
   *  @brief Get arguments from template parameters
   *
   *  @details Get a complete argument list from an argument list
   *  that contains only those arguments which correspond to the
   *  template parameters. The non-template arguments are set to
   *  `nullptr`, and they are default constructed when an instance
   *  is created.
   *
   *  @param argc number of template parameter arguments
   *  @param argv `OArg*` array of template parameter arguments
   *  @return `std::vector<OArg>` the complete argument list
   */
  std::vector<OArg> getArgsFromTemplateParams(size_t argc, OArg* argv) const;
  /**
   *  @brief Find specialization of `this` template class
   *  @details Finds a specialization of `this` template class,
   *  the template parameters of which match the supplied types.
   *
   *  @param templateTypes vector of template parameter types
   *  @return `OClass*` a specialized type or `nullptr`
   */
  OClass* findSpecialization(const std::vector<const OClass*>&) const;

private:

  OClass* specialize(Session*, const std::vector<const OClass*>&, size_t& base);
  bool matchesTemplateParams(const OClass* specialization,
      const std::vector<const OClass*>& templateTypes) const;

public:

  //::::::::::::::::::::::::   Class Management   :::::::::::::::::::::::::::://

  inline bool ctorFailed() const noexcept
  { return (flags_ & CLSF_STATUS_CTOR_FAILED) != 0; }

  inline bool isCoreClass() const noexcept {return baseClass_ == nullptr;}
  inline bool isClassReady() const noexcept
  { return Flags::is<CLSF_STATUS_READY>(flags_); }
  inline bool isMutable() const noexcept
  { return (flags_ & CLSF_STATUS_MUTABLE) != 0; }
  inline bool isTemplate() const noexcept
  { return (flags_ & CLSF_STATUS_TEMPLATE) != 0; }
  inline bool isTemplateClass() const noexcept { return isTemplate(); }
  inline bool hasTemplateMembers() const noexcept
  { return (flags_ & CLSF_HAS_TEMPLATE_MEMBERS) != 0; }
  inline bool isHidden() const noexcept
  { return Flags::is<CLSF_STATUS_HIDDEN>(flags_); }
  inline bool isOpen() const noexcept
  { return Flags::is<CLSF_STATUS_OPEN>(flags_); }

  inline void hide(bool b = true) noexcept
  { Flags::set<CLSF_STATUS_HIDDEN>(flags_, b); }
  inline void setTypelessProps(bool b = true) noexcept
  { Flags::set<CLSF_STATUS_TYPELESS_PROPS>(flags_, b); }

  inline bool hasTypelessProps() const noexcept
  { return Flags::is<CLSF_STATUS_TYPELESS_PROPS>(flags_); }

  inline void setBlock(int b) noexcept {classBlockId_ = b;}
  inline int getBlock() const noexcept {return classBlockId_;}

  //:::::::::::::::::::::::   Property Management   :::::::::::::::::::::::::://

  const std::vector<OVar>& propArray(ORef self) const noexcept;
  std::vector<OVar>& propArray(ORef self) noexcept;

  inline size_t numProps() const noexcept {return props_.size();}

  inline int getThisPropOffset(const std::string& name) const noexcept
  { return props_.getOffset(name); }
  inline const Property& getThisProperty(const std::string& name) const noexcept
  { return props_.getItem(name); }
  const Property& getProperty(const std::string& name) const noexcept;
  const Property& getProperty(size_t i) const {return props_[i];}
  Property& getProperty(size_t i) {return props_[i];}
  /**
   *  @brief Resolve property by name
   *  @details Get information related to a class's property. The
   *  fields of the returned tuple are as follows:
   *  @li @c 0 the owner class
   *  @li @c 1 the type of the property
   *  @li @c 2 the offset of the property in an instance of this class
   *  @li @c 3 the accessibility of the property
   */
  std::tuple<OClass*, OClass*, int, ushort>
  resolveProperty(const std::string& name) const noexcept;
  /**
   */
  inline int getPropertyOffset(const std::string& name) const noexcept
  { return std::get<2>(resolveProperty(name)); }
  /**
   *  @brief Add property to class
   *  @details Add property with specified protection properties
   *  to class.
   *  @param @c m Session, `Session*`
   *  @param @c name Property name; `std::string`
   *  @param @c type The type of the property (class name); `std::string`.
   *         An empty string indicates a template or reference property.
   *  @param @c pl Protection level (public, protected, private); `int`
   *  @return
   *  @li 0 success
   *  @li 1 class is finalized
   *  @li 2 member was previously added to class
   */
  int addProperty(Session* m, std::string&& name,
                  const std::string& type, unsigned short options);
  /**
   *  @brief Add property to mutable class
   *  @details Add property with specified protection properties
   *  to class.
   *  @param @c m Session, `Session*`
   *  @param @c name Property name; `std::string`
   *  @param @c type The type of the property; `OClass*`
   *  @param @c pl Protection level (public, private); `int`
   *  @return
   *  @li 0 success
   *  @li 2 member was previously added to class
   */
  int addMutableTypeProperty(Session* m, std::string&& name,
                             OClass* type, unsigned short options);
#if 0
  /** Resolve properties with forward type definition */
  static void s_resolvePropTypes(Session*);
#endif // 0
  inline const PropertyTable& getPropTable() const noexcept {return props_;}
  inline PropertyTable& getPropTable() noexcept {return props_;}

  //::::::::::::::::::::::::   Method Management   ::::::::::::::::::::::::::://

  /**
   *  @brief Add script method to class
   *
   *  @details Add script method with specified prototype, implementation
   *           and protection properties to class.
   *           @par This method throws a string error message if an
   *           error occurs during prototype parsing. This exception
   *           must be caught and rethrown as an OM exception.
   *
   *  @param @c m Session pointer
   *  @param @c proto Method prototype, @c std::string. The prototype
   *            is supplied in the form of a sequence of key-value
   *            pairs, for example,
   *            "name: employeName age: num salary: amount" or
   *            "name:age:salary:" or
   *            "name:age: 'employee age' salary:".
   *            The values are used as the argument names. If a value
   *            is not supplied, the name of the key without the
   *            trailing colon is used.
   *  @param @c blockId The identifier of the code block implementing
   *            the method, @c int
   *  @param @c protectionFlags Protection level, public, protected,
   *            private, static, constThis; @c ushort
   *  @return
   *  @li true success
   *  @li false error duplicate method
   */
  bool addScriptMethod(Session* m, const std::string& proto, int blockId,
                                   ushort protectionFlags = 0);

  bool setScriptMethodAddress(const std::string& method, ushort a, int blockId);
  /**
   *  @brief Cache address of ctor, dtor or copy method
   *  @details Applies to newly introduced script methods.
   */
  void cacheMethodAddress(const Method& method, ushort accessibility);
  /**
   *  @brief Cache address of a core class's copy method
   *  @details Has no effect if the class has no copy method.
   */
  void cacheCoreMethodAddressCopy() noexcept;

  const OMethodAddress& getCachedCopyOperator() const noexcept
  { return copyOperator_; }

  bool getMethodAddressByName(const std::string& name, OAddr*, void* = 0) const;
  int getScriptMethodAddress(const std::string& name, OAddr*, OClass**) const;
  OCoreMethodInfo* getCoreMethodAddress(const std::string& name, OAddr* a) const;
  ScriptMethodAddress getNewScriptMethodAddress(const std::string& methodName,
                                                ushort* access = nullptr) const;
  const char* getNewCoreMethodName(const CoreMethodAddress&) const;

  inline int getMethodOffset(const std::string &s) const noexcept
  { return methods_.getOffset(s); }
  inline const Method& getMethod(const std::string &s) const noexcept
  { return methods_.getItem(s); }
  inline int numMethods() const noexcept {return (int) methods_.size();}
  inline const MethodTable& getMethodTable() const {return methods_;}

  //::::::::::::::::::::::::::   Accessibility   ::::::::::::::::::::::::::::://

  std::pair<const Property&, ushort>
    getPropertyAccess(const std::string &name) const noexcept;

private:

  inline bool isPropPublic(int i) const
  { return g_public(props_.protection(i)); }
  inline bool isPropInternal(int i) const
  { return g_internal(props_.protection(i)); }
  inline bool isPropTemplate(int i) const
  { return props_[i].isTemplate(props_.protection(i)); }
  inline bool isPropConst(int i) const
  { return g_const(props_.protection(i)); }
  inline bool isPropReference(int i) const
  { return g_ref(props_.protection(i)); }

  inline bool isMethodPublic(int i) const
  { return g_public(methods_.protection(i)); }
  inline bool isMethodProtected(int i) const
  { return g_protected(methods_.protection(i)); }
  inline bool isMethodPrivate(int i) const
  { return g_private(methods_.protection(i)); }

public:

  /**
   *  @brief Check accessibility from caller
   *
   *  @details Check if a member, property or method, with the
   *  given accessibility is accessible from @c caller.
   *  A null caller indicates the main program.
   *  @param @c accessibility obtained, e.g., from resolveProperty()
   *  @param @c caller the class of the object that accesses this property
   */
  bool isMemberAccessible(ushort accessibility, const OClass* caller)
      const noexcept;
  /**
   *  @brief Check if property is accessible from caller
   *
   *  @details Check if the supplied property is accessible from
   *  @c caller. A null caller indicates the main program.
   *  @param @c name the property's name
   *  @param @c caller the class of the object that accesses this property
   */
  bool isPropertyAccessible(const std::string& name, OClass* caller) const;
  /**
   *  @brief Check if method is accessible from caller
   *
   *  @details Check if the supplied method is accessible from @c caller.
   *  @param name name of method
   *  @param caller the class of the calling object. If the method is
   *                called from a global context it must be @c null.
   */
  bool isMethodAccessible(const std::string& name, OClass* caller) const;
  std::pair<bool, OMethodAddress> getMethodAccessAddress(
      const std::string& methodName, const OClass* caller) const;
  ushort getCachedMethodAccess(int which) const noexcept
  { return access_[which]; }

  //:::::::::::::::::::::::   Instance Management   :::::::::::::::::::::::::://

  /** @brief Get instance size*/
  size_t getInstanceSize() const noexcept;
  /**
   *  @brief Check if @c this extends the supplied class
   *
   *  @details It returns true if this class is a script extension
   *  of the supplied class.
   */
  bool extends(const OClass*) const;
  /**
   *  @brief Check if @c this extends the supplied class
   *
   *  @details An object is the type of another object when
   *  its class extends the class of the latter.
   *  \note It does not check if it extends the C-core class.
   *  Although C-classes can inherit method tables, they are
   *  considered independent \em base (root) classes. Method
   *  table inheritance is a convenience rather than a usual
   *  class inheritance.
   */
  inline bool typeOf(const OClass* c) const {return extends(c);}

  bool respondsTo(const std::string&);
  bool introduces(const std::string& methodName) const;

# define HAS_CONSTRUCTOR (flags_ & CLSF_HAS_CONSTRUCTOR)
# define HAS_DESTRUCTOR  (flags_ & CLSF_HAS_DESTRUCTOR )
# define HAS_COPY_METHOD (flags_ & CLSF_HAS_COPY_METHOD)

  inline bool hasConstructor() const noexcept {return HAS_CONSTRUCTOR;}
  inline bool hasDestructor () const noexcept {return HAS_DESTRUCTOR; }
  inline bool hasCopyMethod () const noexcept {return HAS_COPY_METHOD;}

# undef HAS_CONSTRUCTOR
# undef HAS_DESTRUCTOR
# undef HAS_COPY_METHOD

  // TODO doc needed!
  bool canCopy(ORef) const;
  bool canCreate() const {return !hasTypelessProps();}

  /** @brief Get class name */
  inline const std::string& getClassName() const noexcept {return name;}
  inline const std::string& getTypeName() const noexcept {return name;}
  /** @brief Get type id */
  inline int getTypeId() const noexcept {return classId_;}
  inline int getClassId() const noexcept {return classId_;}
  inline int getRootTypeId() const noexcept {return rootClass_->classId_;}
  /** Get root class */
  static const OClass* s_getRootClass(const std::string& className, Session*);
  /**
   *  Find root class
   *
   *  Finds the root class of this class by traversing the inheritance
   *  list. This method is used in the class constructor, where the
   *  root class is not known yet. In all other cases the method
   *  @c getRootClass should be used for faster access.
   */
  const OClass* findRootClass() const;
  /**
   *  Get root class
   *
   *  Use this method in all cases except in the class constructor.
   */
  inline const OClass* getRootClass() const noexcept {return rootClass_;}
  inline const OClass* getRootType() const noexcept {return rootClass_;}
  /** Get base class */
  inline OClass* getBaseClass() const noexcept {return baseClass_;}

protected:

  /**
   *  @brief Get instance allocation size
   *
   *  @details It must be redefined in all @c OInstance c-derived classes.
   */
  virtual size_t getAllocSize() const;
  /**
   *  @brief Check if @c this class recursively extends the supplied class
   *
   *  @details A class recursively extends another class `c` when it
   *  is an extension (a derived class) of `c` or when some of its
   *  members extend `c`. Reference members are ignored as they are
   *  not (deep) copied, constructed or deleted. Also template
   *  members are ignored. Since their class is unknown until their
   *  instantiation, they are checked for recursive extension when
   *  they are instantiated.
   *
   *  When the supplied argument is `nullptr` `this` class is checked
   *  against itself, i.e. it is checked if `this` type is recursively
   *  defined.
   */
  bool recursivelyExtends(const OClass*) const noexcept;
  /**
   *  @brief Check if @c this class is recursively defined
   *
   *  @details It calls @sa recursivelyExtends(const OClass*) const
   *  noexcept with a `nullptr` argument.
   */
  inline bool isRecursivelyDefined() const noexcept
  { return recursivelyExtends(nullptr); }

public:

  //::::::::::::::::::::::::::::   Requirements   :::::::::::::::::::::::::::://

  /**
   *  @brief Get the @c Requirement object associated with a class
   *  @param c The class of which we inquire about the associated
   *      @c Requirement object
   *  @return A pointer to the @c Requirement object or `nullptr`
   *      if no @c Requirement object is associated with the class
   */
  Requirement* getRequirement() const noexcept { return req_; }
  /**
   *  @brief Add pre-registered requirement to class
   *  @details Associates a class with a requirement already
   *  associated with another class or registered with @c reqs_.
   *  The class must not have previously defined a requirement.
   *  @param type `OClass*` The class being registered with the
   *      supplied requirement
   *  @param req Pointer to registered @c Requirement object
   *  @return false if the class has previously defined requirements
   *  @return true otherwise
   */
  bool importRequirement(Requirement* req) noexcept;

  //::::::::::::::::::::::   Instance Construction   ::::::::::::::::::::::::://

  /**
   *  Create instance of this class
   */
  inline ORef createObject(Session* m) { return createObject(false, m); }
  virtual ORef createObject(bool forceNoCache, Session* m);
  /**
   *  Create and initialize instance of this class
   */
  inline ORef createInitObject(Session* m, int argc, OArg* argv)
  { return createInitObject(false, m, argc, argv); }
  virtual ORef createInitObject(bool forceNoCache, Session*, int, OArg*);
  ORef applyConstructor(ORef self, Session*);

  inline void addInstance() noexcept { ++dedicatedUses_; }
  inline void removeInstance() noexcept { --dedicatedUses_; }

protected:

  /**
   *  Construct core C++ object
   */
  virtual ORef constructCore(Session*);

private:

  /**
   * @brief Construct scripted part of object
   */
  ORef constructScriptedPart(ORef, Session*);
  /**
   * @brief Construct and initialize scripted part of object
   */
  ORef constructInitScriptedPart(ORef, int argc, OArg* argv, size_t base,
                                                             Session* m);
  /**
   *  @brief Default object constructor
   *
   *  @details The default construction behavior is to construct
   *  non-template members and set reference members to @c nullptr.
   *  If a class recursively defines members (has members of the
   *  class's type, or some member's class has members that is an
   *  extension of this class) this construct would enter an endless
   *  loop. These members should be implemented as references rather
   *  than as hard-created members. The default behavior is to throw
   *  an exception when a recursively defined member is encountered.
   */
  ORef defaultConstructor(ORef, Session *);

public:

  //::::::::::::::::::::::   Instance Destruction   :::::::::::::::::::::::::://

  /**
   *  Destroy instance
   */
  virtual void deleteObject(ORef, Session*);
  ORef applyDestructor(ORef self, Session*);

protected:

  /**
   *  @brief Destruct core C++ object
   *  @details This method should not throw any VM exceptions to
   *  allow the destruction of local variables when exiting or
   *  returning from a stack frame.
   */
  virtual void destructCore(ORef, Session*);

private:

  ORef deleteScriptedPart(ORef, Session *);
  ORef defaultDestructor(ORef, Session *);
  ORef deleteProperties(ORef, Session *);

public:

  //:::::::::::::::::::::::::::   Copy Method   :::::::::::::::::::::::::::::://

  /**
   *  @brief Object copy
   *
   *  @details It is a low level copy method which, along with
   *  its sibling @sa OInstance::copyObject(), can be directly
   *  used if
   *  @li the class of `self` is `this`, and
   *  @li the class of `r` is a subclass (extends) the class of `self`
   *
   *  The Virtual Machine uses @sa Session::copyFrom() which
   *  adds a layer on top of @sa Session::extendedCopy(), and
   *  this in turn adds another layer on top of this method.
   *
   *  @par Question: does it really have to be virtual?
   */
  virtual ORet copyObject(ORef self, const_ORef r, Session* m);

protected:

  /**
   *  @brief Copy core C++ object
   *
   *  @details It copies the core C++ part of the object. This method
   *  must be overriden in C-derived classes to call the C++ object's
   *  copy operator.
   */
  virtual ORef copyCore(ORef self, const_ORef, Session*) const {return self;}

private:

  ORet defaultCopy      (ORef, const_ORef, Session*);
  void copyProperties   (std::vector<OVar>& self,
                         const std::vector<OVar>& r, Session *m);
  void copyProperty     (const Property& prop, ushort accessibility,
                         ORef& self, const ORef& r, Session *m);

  //:::::::::::::::::::::::::::   Move Method   :::::::::::::::::::::::::::::://

public:

  /**
   *  @brief Object move
   *
   *  @details Similar to @c objectCopy but moves the object.
   *  The present implementation moves the members of the object
   *  and its c-core. It applies to objects of the same class.
   */
  virtual ORet moveObject(ORef self, ORef r, Session* m);

  /**
   *  @brief Move core C++ object
   *
   *  @details It moves the core C++ part of the object. This method
   *  must be overriden in C-derived classes to call the C++ object's
   *  move operator.
   */
  virtual ORef moveCore(ORef self, ORef rhs) const {return self;}

  //:::::::::::::::::::::::   Object Cache Creation   :::::::::::::::::::::::://

  virtual void createObjectCache(const ObjectUsage&);

  inline bool hasCache() const noexcept {return !!objectCache_;}
  inline ObjectCacheBase* getCache() const noexcept {return objectCache_.get();}

  template<typename _ObjectType>
    void genericCreateObjectCache(const ObjectUsage& configData) {
      if (objectCache_)
        throw std::logic_error{"cache already constructed for objects type "+name};
      if (const size_t n = configData.getCacheSize(); n != 0)
        objectCache_ = std::make_unique<ObjectCache<_ObjectType>>(n, this);
    }

  template<typename _ObjectType>
    inline ObjectCache<_ObjectType>* getObjectCache() noexcept
    { return static_cast<ObjectCache<_ObjectType>*>(objectCache_.get()); }
  template<typename _ObjectType>
    inline const ObjectCache<_ObjectType>* getObjectCache() const noexcept
    { return static_cast<const ObjectCache<_ObjectType>*>(objectCache_.get()); }

  template<typename _ObjectType>
    inline _ObjectType* getObjectFromCache() noexcept {
      auto* const cache = getObjectCache<_ObjectType>();
      return cache ? cache->reserve() : nullptr;
    }

  /**
   *  @brief Get object from cache
   *  @details It returns an object from the cache, or `nullptr`
   *  if no cache is available or caching is disabled;
   *  see Session::forceNoCache.
   *  @par The definition of this function is in `ortmgr.hpp`.
   */
  template<typename _ObjectType>
    inline _ObjectType* getObjectFromCache(Session* m) noexcept;

  /**
   *  @brief Release cached object
   *  @details It frees an object if there is a cache present
   *  and the object is in the cache. Otherwise, it returns
   *  `false`.
   */
  template<typename _ObjectType>
    inline bool freeCached(ORef o) noexcept {
      auto* const cache = getObjectCache<_ObjectType>();
      return cache && cache->release(static_cast<_ObjectType*>(o));
    }

  /**
   *  @brief Check if object is from this type's cache
   */
  template<typename _ObjectType>
    inline bool cached(ORef o) const noexcept {
      auto* const cache = getObjectCache<_ObjectType>();
      return cache && cache->isCached(static_cast<_ObjectType*>(o));
    }

  /**
   *  @brief Generic core constructor
   *  @details To be used only from ORef constructCore(Session*).
   */
  template<typename _ObjectType>
    inline ORef genericConstructCore(Session* m) noexcept {
      _ObjectType* const o = getObjectFromCache<_ObjectType>(m);
      return o ? o : new _ObjectType{this};
    }

  /**
   *  @brief Generic core constructor for abstract classes
   *  @details To be used only from ORef constructCore(Session*).
   */
  template<typename _ObjectType>
    inline ORef genericConstructCoreAbstract(int typeId, Session* m) noexcept {
      if (classId_ != typeId) {
        _ObjectType* const o = getObjectFromCache<_ObjectType>(m);
        if (o) return o;
      }
      return new _ObjectType{this};
    }

  /**
   *  @brief Generic core constructor for abstract classes
   *  @details To be used only from ORef constructCore(Session*).
   *  The `type` argument must have been obtained from
   *  OClass* OClassMgr::getClass(const std::string&) or
   *  something equivalent.
   */
  template<typename _ObjectType>
    inline ORef genericConstructCoreAbstract(OClass* type, Session* m) noexcept
    { return genericConstructCoreAbstract<_ObjectType>(type->classId_, m); }

  /**
   *  @brief Generic core destructor
   *  @details To be used only from void destructCore(OInstance*, Session*).
   */
  template<typename _ObjectType>
    inline void genericDestructCore(ORef self) noexcept {
      if (!freeCached<_ObjectType>(self))
        delete static_cast<_ObjectType*>(self);
    }

  /**
   *  @brief Generic core destructor for abstract classes
   *  @details To be used only from void destructCore(OInstance*, Session*).
   */
  template<typename _ObjectType>
    inline void genericDestructCoreAbstract(int typeId, ORef self) noexcept {
      if (classId_ == typeId || !freeCached<_ObjectType>(self))
        delete static_cast<_ObjectType*>(self);
    }

  /**
   *  @brief Generic core destructor for abstract classes
   *  @details To be used only from void destructCore(OInstance*, Session*).
   *  The `type` argument must have been obtained from
   *  OClass* OClassMgr::getClass(const std::string&) or
   *  something equivalent.
   */
  template<typename _ObjectType>
    inline void genericDestructCoreAbstract(OClass* type, ORef self) noexcept
    { genericDestructCoreAbstract<_ObjectType>(type->classId_, self); }

  //:::::::::::::::::::::::::::   Apply Method   ::::::::::::::::::::::::::::://

  ORet applyMethod(const std::string&, OArg, int argc, OArg argv[], Session*);
  ORet applyMethod(const std::string&, OArg, std::vector<OArg>&, Session*);

  //::::::::::::::::::::::::::   Cast Operators   :::::::::::::::::::::::::::://

  /**
   *  @brief Get core exception pointer
   *
   *  This method must be overriden by all exception classes.
   */
  virtual ExceptionBase* castException(ORef) const noexcept {return nullptr;}

  /** Strip const (immutable) attribute */
  static void s_constCast(OArg*);

  /**
   *  @brief Reinterpret object's class
   *
   *  Forcibly change object's class. After this operation this
   *  class becomes the object's class, which can be an unrelated
   *  type.
   *  @return OClass* The original class
   */
  OClass* reinterpretCast(ORef) noexcept;

  //::::::::::::::::::::::::::   Class Printer   ::::::::::::::::::::::::::::://

  void print(StringPrinter, bool);

  //::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://

  void setPrefix(const std::string& prefix) {prefix_ = prefix;}
  const std::string& getPrefix() const {return prefix_;}

#if 0 // TODO in a separate file
  //:::::::::::::::::::::::::::   Object Tree   :::::::::::::::::::::::::::::://

  // Build tree representations out of a given instance.
  // There should be no need to redefine these methods.
  void buildObjectTree    (ORef, const char *name, int, unsigned, ObjectTreeNode**) const;
  void buildObjectFlatTree(ORef, const char *name, int, unsigned, ObjectTreeNode**) const;
  void buildMethodTree    (int, MethodListNode**) const;
  void buildMethodFlatTree(MethodListNode**) const;

private:
  void addTreeObjectInterface(ORef, int, int, ObjectTreeNode*) const;
  void addTreeBaseClass      (ORef, int, int, ObjectTreeNode*) const;
  void addTreeMember         (ORef, const char *, int, int, ObjectTreeNode*) const;
  void addFlatTreeObjectInterface(ORef, int, int, ObjectTreeNode*) const;
  void addTreeClassMethods   (int, int, MethodListNode*) const;
  void addTreeMethod         (const Method&, MethodListNode*) const;
#endif // 0

#if 0
public:

  //::::::::::::::::::::::   xxxxxxxxxxxxxxxxxxxxx   ::::::::::::::::::::::::://

  // Throw exception bad usage
          void   cvtError       (const ORef, int, Session*) const;

  // Event handling support
  // virtual oxEventTable *getEventTable  () const {return eventTable_;}
  virtual bool   addEvent       (const char *handler,
                                 ORef _type,
                                 ORef _id, ORef _to,
                                 ORef _param);
  const char* getResolveName    (int entryIndex, int type);
  void resolveEventData         (int i, int val, int which);
  void resolveEventParam        (int i, ORef);
#endif // 0

#if 0
  ORef        createShadow     (ORef, OClass* c = NULL) const;
  static void updateFromShadow (ORef, ORef shadow);
  static void deleteShadow     (ORef);
#endif // 0

//   _ASSOCIATE_MCLASS_WITH(OInstance)
protected:
  virtual oxCInheritanceData* getCInheritanceData() const;
};

#endif // __OCLASS_HPP__
