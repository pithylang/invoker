#ifndef __UTIL_HPP__
#define __UTIL_HPP__

#include <type_traits>
#include <string>
#include <initializer_list>
#include <cstring>

//:::::::::::::::::::   Iterator Checking And Validation   ::::::::::::::::::://

template<typename _It>
concept TrueRefValue =
    (std::is_reference<decltype(*std::declval<_It>())>::value);

template<typename _RangeIt, typename _It>
requires (TrueRefValue<_RangeIt> && TrueRefValue<_It>)
bool rangeContains(const _RangeIt& from, const _RangeIt& end, const _It& it) {
  auto i = from;
  while (i != end)
    if (&*i++ == &*it) return true;
  return false;
}

template<typename _RangeIt, typename _It>
requires (!TrueRefValue<_RangeIt> || !TrueRefValue<_It>)
bool rangeContains(const _RangeIt& from, const _RangeIt& end, const _It& it) {
  auto i = from;
  while (i != end)
    if (i++ == it) return true;
  return false;
}

template<typename _Cont>
bool inContainer(const _Cont& c, const typename _Cont::iterator& it) {
  return rangeContains(c.begin(), c.end(), it);
}

template<typename _Cont>
bool validateIterator(const _Cont& c, const typename _Cont::iterator& it) {
  return c.empty() || it == c.end() || inContainer(c, it);
}

template<typename _Cont>
bool validateIterators(const _Cont& c,
    std::initializer_list<const typename _Cont::iterator> l) {
  for (const auto it : l)
    if (!validateIterator(c, it)) return false;
  return true;
}

//:::::::::::::::::::::::::::   Utility Classes   :::::::::::::::::::::::::::://

struct Flags {
  template<unsigned long FLAG, typename t>
    inline static bool is(t flags) noexcept {
      return (flags & static_cast<t>(FLAG)) != 0;
    }
  template<unsigned long FLAG, typename t>
    inline static void set(t& flags, bool on) noexcept {
      if (on) flags |= static_cast<t>(FLAG);
      else flags &= ~static_cast<t>(FLAG);
    }
};


struct StringView {
  const std::string& r;
  StringView() = delete;
  StringView(const std::string& r) : r{r} {}
  StringView& operator=(const StringView&) = delete;
  StringView& operator=(StringView&&) = delete;

  inline bool startsWith(const char* text, size_t n) const noexcept
  { return r.size() >= n ? memcmp(r.data(), text, n) == 0 : false; }
  inline bool startsWith(const char* text) const noexcept
  { return startsWith(std::string{text}); }
  inline bool startsWith(const std::string& text) const noexcept
  { return startsWith(text.c_str(), text.length()); }

  inline bool endsWith(const char* text, size_t n) const noexcept
  { return r.size() >= n ? memcmp(r.data() + r.size() - n, text, n) == 0 : false; }
  inline bool endsWith(const char* text) const noexcept
  { return endsWith(std::string{text}); }
  inline bool endsWith(const std::string& text) const noexcept
  { return endsWith(text.c_str(), text.length()); }
};

struct StringOutput {
  std::string& r;
  StringOutput() = delete;
  explicit StringOutput(std::string& r) : r{r} {}
  StringOutput& operator=(const StringOutput&) = delete;
  StringOutput& operator=(StringOutput&&) = delete;
};

inline StringOutput& operator<<(StringOutput& os, char ch)
{ os.r += ch; return os; }
inline StringOutput& operator<<(StringOutput& os, const std::string& s)
{ os.r += s; return os; }
template<typename t> requires std::is_arithmetic_v<t>
  inline StringOutput& operator<<(StringOutput& os, const t x)
  { return os << std::to_string(x); }
inline StringOutput& operator<<(StringOutput&& os, const char ch)
{ return os << ch; }
inline StringOutput& operator<<(StringOutput&& os, const std::string& s)
{ return os << s; }
template<typename t> requires std::is_arithmetic_v<t>
  inline StringOutput& operator<<(StringOutput&& os, const t x)
  { return os << x; }

//::::::::::::::::::::::::::   Utility Functions   ::::::::::::::::::::::::::://

template<typename...>
  constexpr bool isOneOf(long n) noexcept { return false; }
template<int I1, int... In>
  constexpr bool isOneOf(long n) noexcept { return n == I1 || isOneOf<In...>(n); }

#endif // __UTIL_HPP__
