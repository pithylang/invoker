#ifndef __OVECTOR_HPP__
#define __OVECTOR_HPP__

#include <vector>
#include "object.hpp"

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::   OVector, OVector_Class   :::::::::::::::::::::::://
//----------------------------------------------------------------------------//
template<typename t>
class OVector : public OInstance, public std::vector<t> {
public:
  using base_class = typename std::vector<t>;

  OVector(OClass* cls) : OInstance(cls) {}

  /**
   *  @brief Initialize literal vector
   *
   *  @details This is not a public method. Instead it is used
   *  internally. More precisely, it is called from
   *  ORTMgr::createArray() to initialize a newly created vector.
   *  @par A script defines a literal vector with the code
   *  `[item0, item1, ...]`, for example,
   *  @code {.c++}
   *  v = [12, x + y, 32];
   *  @endcode
   *  where @c x, @c y are @c int variables. This will create a
   *  vector with @c int elements.
   *  @return ORet Object reference of created vector
   */
  _METHOD (ommInit           );

protected:
  _METHOD (ommBack           );
  _METHOD (ommComponent      );
  _METHOD (ommGetItem        );
  _METHOD (ommData           );
  _METHOD (ommEmpty          );
  _METHOD (ommFront          );
  _METHOD (ommValue          );
  _METHOD (ommComponentValue );
  _METHOD (ommResize         );
  _METHOD (ommSize           );
  _METHOD (ommSizeValue      );
  _METHOD (ommPushBack       );
  _METHOD (ommPopBack        );
  _METHOD (ommShrinkToFit    );
  _METHOD (ommErase          );
  _METHOD (ommEraseRange     );
  _METHOD (ommClear          );
  _METHOD (ommInsert         );
  _METHOD (ommInsertSize     );
  _METHOD (ommInsertRange    );
  _METHOD (ommAssignRange    );
  _METHOD (ommSwap           );
  _METHOD (ommBegin          );
  _METHOD (ommCBegin         );
  _METHOD (ommRBegin         );
  _METHOD (ommCRBegin        );
  _METHOD (ommEnd            );
  _METHOD (ommCEnd           );
  _METHOD (ommREnd           );
  _METHOD (ommCREnd          );
  // ranges
  _METHOD (ommAll            );
  _METHOD (ommRAll           );
  _METHOD (ommCAll           );
  _METHOD (ommCRAll          );
  // conversions
  template<typename T>
    _METHOD (ommCast);

  // helper methods
public:
  static ORet packComponentValue(t&, const char* methodName, Session* m)
                                requires (!std::is_same_v<t, bool>);
  static ORet packComponentValue(const std::vector<bool>::reference&,
                                 const char* methodName, Session* m)
                                requires std::is_same_v<t, bool>;
protected:
  typename std::vector<t>::iterator insertTypeRange(
      typename std::vector<t>::iterator pos, OArg* argv, bool& handled);
  bool assignTypeRange(OArg* argv);

private:
  _DECLARE_METHOD_TABLE();
};

template<typename t>
class OVector_Class : public OClass {
public:
  /** Constructor for this class and C-derived classes */
  OVector_Class(Session* m, const std::string& className) :
      OClass(m, className) {}
  /** Constructor for script-derived classes */
  OVector_Class(Session* m, const std::string& base,
      const std::string& className) : OClass(m, base, className) {}
  /** Constructor for mutable classes */
  OVector_Class(Session* m, const OClass* base,
      const std::string& className) : OClass(m, base, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<OVector<t>>(m); }
  virtual void destructCore(OInstance* self, Session* m) override
  { if (!freeCached<OVector<t>>(self)) delete static_cast<OVector<t>*>(self); }
  virtual ORef copyCore(ORef self, const_ORef r, Session* m) const override;
  virtual ORef moveCore(ORef self, ORef r) const override;
  virtual size_t getAllocSize() const override
  { return sizeof(OVector<t>); }
  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<OVector<t>>(ou); }

  _ASSOCIATE_MCLASS_WITH(OVector<t>)
};

//----------------------------------------------------------------------------//
//:::::::::::::::::::   ObjectVector, ObjectVector_Class   ::::::::::::::::::://
//----------------------------------------------------------------------------//
class ObjectVector : public OInstance, public std::vector<ORef> {
  friend class ObjectVector_Class;

public:
  using base_class = std::vector<ORef>;

  ObjectVector(OClass* cls) : OInstance(cls) {}

  /**
   *  @brief Initialize literal object vector
   *
   *  @details See @sa template<typename t> class OVector
   */
  _METHOD (ommInit           );

  _METHOD (ommBack           );
  _METHOD (ommComponent      );
  _METHOD (ommData           );
  _METHOD (ommEmpty          );
  _METHOD (ommFront          );
  _METHOD (ommValue          );
  _METHOD (ommComponentValue );
  _METHOD (ommResize         );
  _METHOD (ommSize           );
  _METHOD (ommSizeValue      );
  _METHOD (ommPushBack       );
  _METHOD (ommPopBack        );
  _METHOD (ommShrinkToFit    );
  _METHOD (ommErase          );
  _METHOD (ommEraseRange     );
  _METHOD (ommClear          );
  _METHOD (ommInsert         );
  _METHOD (ommInsertSize     );
  _METHOD (ommInsertRange    );
  _METHOD (ommAssignRange    );
  _METHOD (ommSwap           );
  _METHOD (ommBegin          );
  _METHOD (ommCBegin         );
  _METHOD (ommRBegin         );
  _METHOD (ommCRBegin        );
  _METHOD (ommEnd            );
  _METHOD (ommCEnd           );
  _METHOD (ommREnd           );
  _METHOD (ommCREnd          );
  // ranges
  _METHOD (ommAll            );
  _METHOD (ommRAll           );
  _METHOD (ommCAll           );
  _METHOD (ommCRAll          );

public:
  int sameBasicTypeItems() const;

protected:
  void clearItems(Session*, bool alsoContainer = true);
  ORet copyItems(const std::vector<ORef>& a, Session* m);
  std::vector<ORef>::iterator
  insertRange(std::vector<ORef>::iterator pos, bool& handled, OArg*, Session*);
  bool assignRange(OArg* argv, Session* m);
  template<typename t>
    void transferItems(const vector<t>& from, Session* m);

private:
  std::string getFullMethodName(const std::string& methodName) const;

  _DECLARE_METHOD_TABLE();
};

class ObjectVector_Class : public OClass {
public:
  /** Constructor for this class and C-derived classes */
  ObjectVector_Class(Session* m, const std::string& className) :
      OClass(m, className) {}
  /** Constructor for script-derived classes */
  ObjectVector_Class(Session* m, const std::string& base,
      const std::string& className) : OClass(m, base, className) {}
  /** Constructor for mutable classes */
  ObjectVector_Class(Session* m, const OClass* base,
      const std::string& className) : OClass(m, base, className) {}

  virtual OClass* createClass(Session* m, const std::string& className)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& className)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(ObjectVector); }

  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<ObjectVector>(ou); }

  /** @brief Construct core C++ object */
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<ObjectVector>(m); }

  /**
   *  @brief Delete core C++ object
   *  @details We override this method to delete all elements
   *  besides the container.
   */
  virtual void destructCore(OInstance* self, Session* m) override;

  // This argument is necessary to invoke copy
  virtual ORef copyCore(ORef self, const_ORef r, Session* m) const override;
  virtual ORef moveCore(ORef self, ORef r) const override;

  _ASSOCIATE_MCLASS_WITH(ObjectVector);
};

#define _PVector(t, a) static_cast<OVector<t>*>(a)
#define _PCVector(t, a) static_cast<const OVector<t>*>(a)
#define _PObjVector(a) static_cast<ObjectVector*>(a)
#define _PCObjVector(a) static_cast<const ObjectVector*>(a)
#define _PULongVector(a)     _PVector(unsigned long, a)
#define _PLongVector(a)      _PVector(long, a)
#define _PUIntVector(a)      _PVector(unsigned, a)
#define _PIntVector(a)       _PVector(int, a)
#define _PShortVector(a)     _PVector(short, a)
#define _PCharVector(a)      _PVector(char, a)
#define _PDblVector(a)       _PVector(double, a)
#define _PFloatVector(a)     _PVector(float, a)
#define _PBoolVector(a)      _PVector(bool, a)
#define _PPtrVector(a)       _PVector(void*, a)

#endif // __OVECTOR_HPP__
