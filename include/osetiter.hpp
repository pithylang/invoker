#ifndef __OSETITER_HPP__
#define __OSETITER_HPP__

#include <set>
#include "oiter.hpp"
#include "oset.hpp"
#include "obasicTypeInfo.hpp"
#include "orangeOperation.hpp"

//----------------------------------------------------------------------------//
//:::::::::::::::::::::::::::::   OSetIterator   ::::::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

template<typename t>
class OSetIterator : public OBidirectionalIterator,
                     public std::set<t>::iterator {
public:
  using base_class = typename std::set<t>::iterator;
  using difference_t = typename base_class::difference_type;

  bool reverse_{false};

  OSetIterator(OClass* cls) : OBidirectionalIterator{cls} {}
  OSetIterator& operator=(const OSetIterator& r) = default;
  OSetIterator& operator=(const typename std::set<t>::iterator& r)
  { *static_cast<base_class*>(this) = r; return *this; }

  _DEFINE_TIE(_O(SetIterator), BTI::getSetIteratorClass<t>());

  void setReverse(bool b) noexcept {reverse_ = b;}
  bool isReverse() const noexcept {return reverse_;}

  _METHOD(ommGetValue);
protected:
  _METHOD(ommComponent);
  _METHOD(inc);
  _METHOD(dec);
  _METHOD(postinc);
  _METHOD(postdec);
  _METHOD(eq);
  _METHOD(ne);
  _METHOD(ommSwap);

  _DECLARE_METHOD_TABLE();
};

template<>
class OSetIterator<ORef> : public OBidirectionalIterator,
                           public std::set<ORef, ORefSetCompare>::iterator {
public:
  using base_class = std::set<ORef, ORefSetCompare>::iterator;
  using difference_t = base_class::difference_type;

  bool reverse_{false};

  OSetIterator(OClass* cls) : OBidirectionalIterator{cls} {}
  OSetIterator& operator=(const OSetIterator& r) = default;
  OSetIterator& operator=(const base_class& r)
  { *static_cast<base_class*>(this) = r; return *this; }

  _DEFINE_TIE(_O(SetIterator)<ORef>, BTI::getSetIteratorClass<ORef>());

  void setReverse(bool b) noexcept {reverse_ = b;}
  bool isReverse() const noexcept {return reverse_;}

  _METHOD(ommGetValue);
protected:
  _METHOD(ommComponent);
  _METHOD(inc);
  _METHOD(dec);
  _METHOD(postinc);
  _METHOD(postdec);
  _METHOD(eq);
  _METHOD(ne);
  _METHOD(ommSwap);

  _DECLARE_METHOD_TABLE();
};

//----------------------------------------------------------------------------//
//::::::::::::::::::::::::::   OSetIterator_Class   :::::::::::::::::::::::::://
//----------------------------------------------------------------------------//

template<typename t>
class OSetIterator_Class : public OClass {
public:
  /** Constructor for this and C-derived classes */
  OSetIterator_Class(Session* m, const std::string& typeName);
  /** Constructor for script-derived classes */
  OSetIterator_Class(Session *m, const std::string& baseClass,
      const std::string& typeName) : OClass(m, baseClass, typeName) {}
  /** Constructor for mutable classes */
  OSetIterator_Class(Session *m, const OClass* baseClass,
      const std::string& typeName) : OClass(m, baseClass, typeName) {}

  virtual OClass* createClass(Session* m, const std::string& typeName)
      const override;
  virtual OClass* createMutableClass(Session* m, const std::string& typeName)
      const override;

  virtual size_t getAllocSize() const override
  { return sizeof(OSetIterator<t>); }
  virtual void createObjectCache(const ObjectUsage& ou) override
  { genericCreateObjectCache<OSetIterator<t>>(ou); }
  virtual ORef constructCore(Session* m) override
  { return genericConstructCore<OSetIterator<t>>(m); }
  virtual void destructCore(OInstance* self, Session*) override {
    if (!freeCached<OSetIterator<t>>(self))
      delete static_cast<OSetIterator<t>*>(self);
  }
  virtual ORef copyCore(ORef self, const_ORef, Session*) const override;
  virtual ORef moveCore(ORef self, ORef) const override;

  _ASSOCIATE_MCLASS_WITH(OSetIterator<t>)
};

#define _PSetIter(t, o) static_cast<OSetIterator<t>*>(o)
#define _PCSetIter(t, o) static_cast<const OSetIterator<t>*>(o)

#endif // __OSETITER_HPP__
