#ifndef __MFRAME_HPP__
#define __MFRAME_HPP__

#include <vector>
#include <cinttypes>
#include "lvector.hpp"
#include "odefs.h"
#include "mdefs.h"

class MBlock;
class Session;

enum ReturnCondition : uint8_t {
  M_FRAME,
  M_IP,
  M_OPCODE,
};

/**
 *  @brief Machine call frame
 *  @details @c MCallFrame represents a stack frame of the
 *  @em Virtual @em Machine.
 *  @par The fields of @c MCallFrame are:
 *  @arg @c prev, `MCallFrame*` the parent call frame
 *  @arg @c receiver, `OArg` pointer to variable which is the
 *                   receiver of the present call
 *  @arg @c selector, `OVar`
 *  @arg @c args, `std::vector<OArg>` vector of pointers to variables
 *                   which are the arguments of the present call
 *  @arg @c locals, `std::vector<OVar>` vector of local variables for
 *                   the present call
 *  @arg @c block, `MBlock*` block pointer of this frame. A null
 *                   value indicates a secondary @em blockless call
 *                   frame. Blockless frames have no local variables
 *                   (and no statics). References to locals in these
 *                   frames must be resolved in the nearest parent
 *                   frame having a non-null block
 *  @arg @c referenceResult, `OArg` when it in not null it contains
 *                   a pointer to a property's variable; it is used
 *                   by `execgp` in conjunction with `mref` to set a
 *                   reference property's value
 *  @arg @c BP, `MBlock*`
 *  @arg @c IP, `int`
 *  @arg @c RC, `uint8_t`
 *  @par The last two fields, `BP` and `IP`, are the saved
 *  machine state of the previous call frame.
 */
struct MCallFrame {
  union ReturnPoint {
    MCallFrame* endFrame;
    int         endIP;
    MOpcode     endOpcode;
    ReturnPoint() : endFrame{nullptr} {}
  };

  MCallFrame* prev;
  // Call frame
  OArg receiver;
  OVar selector;
  std::vector<OArg> args;
  std::vector<OVar> locals;
  i1::vector<OVar> extraLocals;
  std::vector<size_t> argsToRestore;
  MBlock* block;
  OArg referenceResult;
  // Saved machine state
  MBlock* BP;
  int IP;
  bool constOperation{false};
  bool restoreRec{false};
  ReturnCondition RC{M_FRAME};
  ReturnPoint returnPoint;

  MCallFrame() : receiver{nullptr}, block{nullptr}, referenceResult{nullptr} {
    args.reserve(2);
    locals.reserve(12);
  }

  void destroy(Session* m);

  inline size_t localSize() const noexcept { return locals.size(); }
  inline bool trivial() const noexcept
  { return !block || locals.empty(); }
  inline bool isInlineBlock() const noexcept
  { return selector.undefinedSelector(); }
  inline bool isLocal(OVar* p) const noexcept
  { return p >= locals.data() && p < locals.data() + locals.size(); }
  bool isArg(OVar* p) const noexcept;
  bool isExtraLocal(OVar* p) const noexcept;

  std::string getMethodName() const noexcept;
  const std::string& methodName() const noexcept;
  std::string getFullMethodName() const noexcept;

  inline void autoCleanTmp(OVar* p) noexcept {
    extraLocals.emplace_back(p->objectRef, USG_DISPOSABLE);
    *p = OVar{};
  }
  inline void autoCleanTmp(OVar& var) noexcept { autoCleanTmp(&var); }

  inline void push(OVar* var, bool asArg) noexcept {
    if (asArg) args.push_back(var);
    else receiver = var;
  }
  inline void push(OVar& var, bool asArg) noexcept { push(&var, asArg); }
  void pushUndisposed(OVar* var, bool asArg) noexcept;
  inline void pushUndisposed(OVar& var, bool asArg) noexcept
  { pushUndisposed(&var, asArg); }
  inline void pushExtraLocal(OVar&& var, bool asArg = true) noexcept {
    extraLocals.emplace_back(std::move(var));
    push(&extraLocals.back(), asArg);
  }

  MCallFrame* searchParentFramesForLocal(OVar* var) const noexcept;
  MCallFrame* searchParentFramesForArg(OVar* var) const noexcept;
};

/**
 *  @brief Machine stack
 *  @details @c MCallStack is a representation of a @em Virtual
 *  @em Machine stack. The only field of @c MCallStack is the
 *  @c top frame of the stack. Other frames are accessed through
 *  their @c prev field.
 */
struct MCallStack {
  MCallFrame* top;
  inline void push() noexcept {
    auto* p = top;
    (top = new MCallFrame)->prev = p;
  }
  inline void pop(Session* m) noexcept {
    auto* p = top;
    top = top->prev;
    p->destroy(m);
  }
};

#endif // __MFRAME_HPP__
