#ifndef __OSTRING_HPP__
#define __OSTRING_HPP__

#include <string>
#include "object.hpp"
#include "orangeOperation.hpp"
#include "oapi.h"

typedef std::string String;
typedef std::wstring WString;
typedef WString::iterator WStringIterator;
typedef WString::reverse_iterator WStringReverseIterator;

#define _PString(id)  static_cast<_ox(String)*>(id)
#define _PStr(id)     static_cast<_ox(String)*>(id)
#define _PWString(id) static_cast<_ox(WString)*>(id)
#define _PWStr(id)    static_cast<_ox(WString)*>(id)

namespace om {
  struct GenericFind {
    using size_type = std::string::size_type;
    const std::string& s;
    const std::string* p;
    size_type pos;
    char ch;
    GenericFind(const std::string& s) :
        s{s}, p{nullptr}, pos{std::string::npos}, ch{0} {}
    virtual size_type operator()() const { return std::string::npos; }
  };
}

_BEGIN_CLASS_DECLARATION(String)
public:
  T& operator=(const String& r)
  { *static_cast<String*>(this) = r; return *this;  }
  T& operator=(const char *s)
  { *static_cast<String*>(this) = s; return *this;  }

private:
  int __compare(size_type __pos1, size_type __n1,
      const basic_string& __str, size_type __pos2, size_type __n2 = npos) const;

protected:
  static ORet packComponentValue(char& c, const char* methodName, Session* m);
  ORet ommGenericFind(om::GenericFind&& f, int argc, OArg* argv, Session* m);
  _METHOD (method_print     );
  _METHOD (method_println   );
  _METHOD (ommValue         );
  _METHOD (ommValueFromSize );
  _METHOD (ommSizeChar      );
  _METHOD (ommBegin         );
  _METHOD (ommRBegin        );
  _METHOD (ommCBegin        );
  _METHOD (ommCRBegin       );
  _METHOD (ommEnd           );
  _METHOD (ommREnd          );
  _METHOD (ommCEnd          );
  _METHOD (ommCREnd         );
  _METHOD (ommSize          );
  _METHOD (ommMaxSize       );
  _METHOD (ommResize        );
  _METHOD (ommCapacity      );
  _METHOD (ommReserve       );
  _METHOD (ommClear         );
  _METHOD (ommEmpty         );
  _METHOD (ommComponent     );
  _METHOD (ommAt            );
  _METHOD (ommFront         );
  _METHOD (ommBack          );
  _METHOD (ommPushBack      );
  _METHOD (ommPopBack       );
  _METHOD (ommAppend        );
  _METHOD (ommAssign        );
  _METHOD (ommInsert        );
  _METHOD (ommErase         );
  _METHOD (ommReplace       );
  _METHOD (ommSwap          );
  _METHOD (ommCStr          );
  _METHOD (ommData          );
  _METHOD (ommStartsWith    );
  _METHOD (ommEndsWith      );
  _METHOD (ommFind          );
  _METHOD (ommRFind         );
  _METHOD (ommFindFirst     );
  _METHOD (ommFindLast      );
  _METHOD (ommFindFirstNotOf);
  _METHOD (ommFindLastNotOf );
  _METHOD (ommNpos          );
  _METHOD (ommSubstring     );
  _METHOD (ommCompare       );
  _METHOD (method_incBy     );
  _METHOD (method_add       );
  _METHOD (method_eq        );
  _METHOD (method_ne        );
  _METHOD (method_lt        );
  _METHOD (method_gt        );
  _METHOD (method_le        );
  _METHOD (method_ge        );
  _METHOD (ommAll           );
  _METHOD (ommRAll          );
  _METHOD (ommCAll          );
  _METHOD (ommCRAll         );
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(WString)
public:
  _ox(WString)& operator=(const wchar_t *s)
  { *static_cast<WString*>(this) = s; return *this;  }
protected:
  _METHOD (method_print);
  _METHOD (method_println);
_END_CLASS_DECLARATION()

// _DECLARE_MCLASS(String, "string");
_BEGIN_MCLASS_DECLARATION(String, "string");
  using StringROP = RangeOperation<OT>;
  static StringROP insertOp;
  static StringROP replaceOp;
  /**
   *  @brief Register range operations
   *  @details This function is called by the class manager
   *  after standard class registration.
   */
  static void registerOperations(Session* m, bool makePublic);
_END_MCLASS_DECLARATION(String);

_DECLARE_MCLASS(WString, "wstring");

#endif // __OSTRING_HPP__
