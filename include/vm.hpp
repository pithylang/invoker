#ifndef __VM_HPP__
#define __VM_HPP__

#include <map>
// #include <string>
#include "odefs.h"
#include "lvector.hpp"
#include "mtypes.hpp"
#include "mcaches.hpp"
#include "mframe.hpp"
#include "msymref.hpp"
#include "mconfig.h"
#include "ortmgr.hpp"

//============================================================================//
//:::::::::::::::::::::::::   Virtual Machine (VM)   ::::::::::::::::::::::::://
//============================================================================//

class MDebugger;
struct Switch;
struct ObjectAutoCleaner;

#ifdef USE_INSTRUCTION_TABLES
class VirtualMachine;
typedef void (VirtualMachine::*vmInstructionHandler)();
#endif // USE_INSTRUCTION_TABLES

// VM configuration values
#ifndef VMCONF_INIT_STACK_SIZE
# define VMCONF_INIT_STACK_SIZE 100
#endif

struct VIASArg {
  std::string methodName;
  std::vector<short> args;
};
using MVIASArgTable = std::vector<VIASArg>;

/**
 *  @brief Virtual machine representation
 */
struct VirtualMachine {
  using MBlockTable = i1::vector<MBlock>;
  using MSwitchTable = std::map<int, std::map<int, Switch*>>;
  using MSwitchTableRef = std::reference_wrapper<MSwitchTable>;
  using MSwitchTablePtr = std::unique_ptr<MSwitchTable>;

  //:::::::::::::::::::::::::::::::   Enums   :::::::::::::::::::::::::::::::://

  enum : int {
    M_INIT_VAR_OK = 0,
    M_INIT_VAR_NO_ACCESS,
    M_INIT_VAR_BLOCK_INACTIVE,
    M_INIT_VAR_UNINIT,
    M_INIT_VAR_NOT_FOUND
  };

  enum ExecutionFlow : uint8_t {
    M_NORMAL,       ///< Normal execution flow
    M_RETURN,       ///< Return from method
    M_EXIT,         ///< Leave machine
    M_RECOVER,      ///< Recover from thrown exception
    M_BREAK_LOOP,   ///< Break loop (not used)
    M_BREAK_SWITCH, ///< Break switch
    M_BREAK_BLOCK,  ///< Break block
  };

  enum SearchRestriction {
    NO_RESTRICTIONS = 0,
    METHOD_BOUNDARY = 1,
    CLASS_BOUNDARY = 2,
    BLOCK_BOUNDARY = 3,
  };

  enum /*ExitCode*/ : int {
    EXIT_CODE_SUCCESS        =  0,
    DEFAULT_ERROR_EXIT_CODE  = 99,
    LEFTOVERS_TEST_FAILED    = 98,
    RETURN_VALUE_TEST_FAILED = 97,
    EXCEPTION_TEST_FAILED    = 96,
  };

  //:::::::::::::::::::::::::::   Static Members   ::::::::::::::::::::::::::://

  static unsigned s_debuggerMutex;
  static std::vector<ORef> s_globalConstants;

  //::::::::::::::::::::::::::::::   Members   ::::::::::::::::::::::::::::::://

  VirtualMachine*       parentMachine;
  MDebugger*            debugger;

  /** Virtual machine stack */
  MCallStack            callStack;

  /**
   *  @brief Virtual machine switch table
   *  @details This table contains all switch statements data used
   *  by this machine. It is allocated only by the main machine.
   *  Never use otherwise this pointer. Instead use the reference
   *  @c switchTableRef.
   */
  MSwitchTablePtr       switchTablePtr;
  MSwitchTableRef       switchTableRef;

  MBlockTable*          BT;  ///< Block table (global)
//   MCallFrame*           FP;  ///< Call Frame pointer (current)
  MBlock*               BP;  ///< Block pointer (current)
  int                   IP;  ///< Instruction pointer (current)
  OVar                  RES; ///< Result
  ORef                  SO;  ///< System RT object
  OVar                  SOVAR; ///< System RT variable
  MSymbol*              SR;  ///< Current local table symbol
#if 0
  vmResolvedDataTable  *RDT; ///< Resolved member data table
  unsigned              DBG; ///< Debug register (flags)
  vmType                XC;  ///< eXception Contexts
  vmType                DO;  ///< Disposed Objects
  vmType                DOS; ///< Local Disposed Objects Stack
#endif // 0
  int                   LR;  ///< Line Register
#if 0
  const char*           FR;  ///< Source File register
  bool                  DR;  ///< Dispose result register
  bool                  CST; ///< Result is constant

#ifdef USE_INSTRUCTION_TABLES
  static vmInstructionHandler sm_instructionHandler[128];
#endif
#endif // 0

  /**
   *  @brief Type of return condition
   *  @details The type of the condition under which the machine
   *  will return.
   */
  ReturnCondition       RC;
  /**
   *  @brief Type of execution flow
   *  @details Indicates the type of execution flow the machine
   *  has to follow.
   *  @sa ExecutionFlow
   */
  ExecutionFlow         XF;
  /**
   *  @brief Current loop break depth
   *  @details This register is pertinent to primary machines.
   *  Since breaks never cross method boundaries, there is no
   *  need to preserve this register through method calls.
   *  \par Do not access this register directly.
   */
  uint8_t               BR;

  /**
   *  @brief Return condition
   *  @details The condition under which the machine will return.
   *  The active member is determined from the field @c RC.
   */
  /*union {
    MCallFrame*  endFrame;
    decltype(IP) endIP;
    MOpcode      endOpcode;
  } */
  MCallFrame::ReturnPoint returnPoint;

  MVIASArgTable& tableVIASArgs;

  /**
   *  @brief Create main machine
   */
  VirtualMachine(Session*, MBlockTable*, MVIASArgTable&);

  /**
   *  @brief Create child machine
   *  @details Child machines run in the same thread with their
   *  parent machine. This means that a parent machine takes
   *  control when its child has finished execution. Each machine
   *  can have at most one child machine.
   */
  VirtualMachine(VirtualMachine& parent);

  void init(int blockId);
  void runStdMode();
  void runDebugMode();

  // adjust machine state
  inline void setSR() noexcept
  { SR = &BP->dataSegment.localSymbolTable[BP->codeSegment[IP].operand]; }

  //::::::::::::::::   Access variables, local, static etc.   :::::::::::::::://

  inline static ORef& _ref(OVariable& var) noexcept {return var.objectRef;}
  inline static const ORef& _ref(const OVariable& var) noexcept
  { return var.objectRef; }
  inline static ORef _ref(OArg& arg) noexcept
  { return arg ? arg->objectRef : nullptr; }
  inline static const ORef _ref(const OArg& arg) noexcept
  { return arg ? arg->objectRef : nullptr; }

  inline OVar& getStaticVar() const // TODO parent()->block ==> BP
  { return parent()->block->dataSegment.staticData[SR->variableOffset]; }
  inline OVar& getStaticVar(MBlock* b, long offset) const
  { return b->dataSegment.staticData[offset]; }
  inline OVar& getLocalVar() const
  { return parent()->locals[SR->variableOffset]; }
  inline OVar& getLocalVar(long offset)
  { return parent()->locals[offset]; }
  inline OVar& getLocalVar(MCallFrame* frame, long offset) const
  { return frame->locals[offset]; }

  MCallFrame* currentFrame() const noexcept;
  MCallFrame* parent() const noexcept;
  std::tuple<MCallFrame*, MCallFrame*> getParentChildPair() const noexcept;
  MCallFrame* getParentMethodFrame() const noexcept;
  MCallFrame* getCurrentMethodFrame() const noexcept;
  inline OArg varThis() noexcept
  { MCallFrame* const p = getParentMethodFrame(); return p ? p->receiver : nullptr; }
  inline ORef refThis() noexcept {return _ref(varThis());}
  inline const OArg varThis() const noexcept
  { MCallFrame* const p = getParentMethodFrame(); return p ? p->receiver : nullptr; }
  inline const ORef refThis() const noexcept {return _ref(varThis());}

  inline const std::string& curMethodName() const
  { return SELECTOR_CACHE(callStack.top->selector)->name; }
  inline uint curArgIndex() const noexcept
  { return callStack.top->args.size(); }

  //::::::::::::::::::::::::::   Control Machine   ::::::::::::::::::::::::::://

  void setReturnCondition(MCallFrame* f) noexcept
  { RC = M_FRAME; returnPoint.endFrame = f; }
  void setReturnCondition(decltype(IP) ip) noexcept
  { RC = M_IP; returnPoint.endIP = ip; }
  void setReturnCondition(MOpcode op) noexcept
  { RC = M_OPCODE; returnPoint.endOpcode = op; }
  inline void setDefaultRC() noexcept
  { RC = M_FRAME; returnPoint.endFrame = nullptr; }
  inline void saveReturnCondition(MCallFrame& f) const
  { f.RC = RC; f.returnPoint = returnPoint; }
  inline void restoreReturnCondition(const MCallFrame& f)
  { RC = f.RC; returnPoint = f.returnPoint; }
  inline void saveRC(MCallFrame* f) const
  { f->RC = RC; f->returnPoint = returnPoint; }
  inline void restoreRC(const MCallFrame* f)
  { RC = f->RC; returnPoint = f->returnPoint; }

  /** Get Break Condition counter */
  decltype(BR)& getBreakCondition() noexcept
  { return _PRTMgr(SO)->getMainMachine()->BR; }
  /** Get Break Condition counter */
  const decltype(BR)& getBreakCondition() const noexcept
  { return _PRTMgr(SO)->getMainMachine()->BR; }
  /** Set Break Condition counter */
  void setBreakCondition(uint n) noexcept {getBreakCondition() = n;}
  /** Break Condition indicator */
  bool inBreakCondition() const noexcept {return getBreakCondition() != 0;}
  /**
   *  @brief Exit break condition
   *  @details Decrement break counter by one.
   */
  void exitBreakCondition() noexcept {--getBreakCondition();}
  /**
   *  @brief Enter return condition
   *  @details The machine enters this condition when it returns
   *  from a method call.
   */
  void enterReturnCondition() noexcept {XF = M_RETURN;}
  /**
   *  @brief Exit return condition
   *  @details The machine exits a return condition when it has finished
   *  returning from a method call.
   */
  void exitReturnCondition() noexcept {XF = M_NORMAL;}
  /**
   *  @brief Return from a method call condition
   *  @details Indicates that the machine is in the process of
   *  returning from a method call.
   */
  bool returning() const noexcept {return XF == M_RETURN;}

  //::::::::::::::::::::::::   Initialize variables   :::::::::::::::::::::::://

  void initGlobalConstants();
  void initGlobalVariables();
  std::tuple<int,OVar*>
    initFromHigherBlock(MSymbol& symbol, SearchRestriction restriction) const;
  void initLiteral(OVar& var) const;

  //:::::::::::::::::::::::::::::   Utilities   :::::::::::::::::::::::::::::://

  bool assignReference(OVar& lhs, const OArg rhs, bool makeConst);
  /**
   *  @brief deleteResult
   *  @details Delete and zero the object contained in the @em Machine
   *  register @c RES if it is disposable and not null. The object is
   *  deregistered and @c RES is undispoded at the end of the operation.
   */
  void deleteResult();

  /**
   *  @brief Accept variable identifier as string argument (VIASA)
   */
  bool acceptVIASA (const std::string& methodName, int argIndex);

  const OClass* getCaller() const;
  int getIndexBP() const noexcept;

  std::string callStackRundown() const;

  std::tuple<OVar*, int, const std::string&>
    resolveSymbolType(int symbolIndex, int what) const;
  /**
   *  @brief Resolve variables type `VARTYPE_INT_DATA`
   *  @details It determines the referenced object of variables
   *  type `VARTYPE_INT_DATA` or constructs them if they are
   *  undefined. An optimization process is followed in which
   *  the `definitionIP` field undergoes the changes
   *  -1 -> IP -> -2. In its final state -2 the symbol is
   *  resolved without any checking.
   *  @param symbol `MSymbol`
   *  @param what `int`
   *  @return std::tuple<OVar*, int, const std::string&>
   */
  std::tuple<OVar*, int, const std::string&>
    resolveSymbolType(MSymbol& symbol, int what) const;
  OVar* resolveSymbol(MSymbol& symbol,
                      SearchRestriction restriction = NO_RESTRICTIONS) const;

  static bool
    s_violates(MBlock::Type blockType, SearchRestriction restriction) noexcept;
  /**
   *  @brief Check if receiver is @em undefined
   *  @details This method is used for debugging purposes.
   */
  bool isUndefindedReceiver(const OVar& var, const std::string& what);
  void error(const std::string& msg);
  void errorNoProp(const std::string& propName, const std::string& type);
  std::string getVariableName(const MBlock& block, ORef o) const;
  /**
   *  @brief Check if argument is a local variable
   *
   *  @param v the variable pointer to check
   *  @param f the call frame to look into or `nullptr` to search
   *           in the parent frame
   */
  bool isLocalVar(OVar* v, const MCallFrame* f) const noexcept;
  /**
   *  @brief Check if argument is an argument of a local frame
   *
   *  @param v the variable pointer to check
   *  @param f the call frame to look into or `nullptr` to search
   *           in the parent frame
   */
  bool isArgument(OVar* v, const MCallFrame* f) const noexcept;
  /**
   *  @brief Get info if argument is receiver's property
   *  @details Get pertaining information if the supplied symbol
   *  name is a property of the receiving object.
   *
   *  @param `symbol` `std::string&` the symbol identifier
   *  @return `std::tuple<MCallFrame*, const OClass*, int, ushort>`
   */
  inline std::tuple<MCallFrame*, const OClass*, int, ushort>
    getDataIfProp(const std::string& symbol) const noexcept {
      MCallFrame* const methodFrame = getParentMethodFrame();
      if (!methodFrame) return {nullptr, nullptr, -1, 0};
      const OClass* const type = methodFrame->receiver->objectRef->getClass();
      const auto [owner, _, offset, access] = type->resolveProperty(symbol);
      if (offset == -1) return {nullptr, nullptr, -1, 0};
      return {methodFrame, owner, offset, access};
    }
  /**
   *  @brief Clean stack frame
   */
  void cleanStackFrame(ObjectAutoCleaner& stackCleaner, MCallFrame* fp);

  //::::::::::::::::::::::::::   Method Execution   :::::::::::::::::::::::::://

  void execBlock(int blockId, MBlock::Type setType = MBlock::PLAIN,
                              MOpcode opcode = nop);
  void execBlockKeepResult(int blockId, MBlock::Type setType = MBlock::PLAIN,
                                        MOpcode opcode = nop);
  bool execBlockGetResult(int blockId, MBlock::Type setType = MBlock::PLAIN);
  void execLoop(const std::vector<int>& args, bool whileLoop, bool beginCond);
  bool execIf(OArg ifCondBlock,
              OArg ifBlock,
              const std::vector<OArg>& elseIfBlocks,
              OArg elseBlock);
  bool execIfThen(const OClass* caller, OArg condBlock, OArg ifBlock);
  void execSwitch(OArg switchVar,
                  const std::vector<OArg>& vals,
                  const std::vector<OArg>& blocks,
                  OArg defBlock);
  /**
   *  @brief Execute flow control
   *  @details It sets the register `BR` of the @em main @em machine
   *  to the specified number.
   *  @param n @c int The number of loops out of which to jump
   */
  void execFlowControl(int n);
  ORet execReturn(OArg arg, bool ref);
  ORet execDefineClass(OClass* c, int blockId);
  ORet execTryCatch(OArg tryBlock, int catchBlockId, int doBlockId);
  ORet execTryCatch(OArg tryBlock, const std::vector<int>& doBlocks,
                                   const std::vector<int>& catchBlocks);

  void beginLoop(const std::vector<int>& args, bool beginCond);
  Switch*& getSwitch(const MBlock* b, int ip) const;
  void initSwitch(Switch*& switchBase, ORef r, const std::vector<OArg>& vals);
  template<typename T>
    T castValue(ORef objectRef);
  template<typename T>
    void initSwitchData(Switch*& switchBase, const std::vector<OArg>& vals);

  /**
   *  @brief Execute method
   *  @details Execute base class method possibly starting a
   *  Virtual Machine if it is a script method.
   *
   *  It is the caller's responsibility to check if `baseClass` is
   *  actually a base class of the receiver's class.
   *
   *  @param baseClass `OClass*` one of receiver's base classes
   *  @param method `const std::string&` method name
   *  @param flags call flags
   *  @param rec receiver object
   *  @param argc number of arguments
   *  @param argv array of arguments
   *  @return std::pair containing
   *    @li first item: @c ORet value returned from method
   *    @li second item: @c int diagnostic value depending on @c flags
   */
  std::pair<ORet, int> execMethod(OClass* baseClass,
                                  const std::string& method,
                                  ulong flags,
                                  OArg rec, int argc, OArg* argv);
  /**
   *  @brief Execute method
   *  @details Execute method possibly starting a Virtual Machine
   *  if it is a script method.
   *
   *  @param method `const std::string&` method name
   *  @param flags call flags
   *  @param rec receiver object
   *  @param argc number of arguments
   *  @param argv array of arguments
   *  @return std::pair containing
   *    @li first item: @c ORet value returned from method
   *    @li second item: @c int diagnostic value depending on @c flags
   */
  inline
  std::pair<ORet, int> execMethod(const std::string& method,
                                  ulong fl,
                                  OArg rec, int argc, OArg* argv)
  { return execMethod(rec->objectRef->getType(), method, fl, rec, argc, argv); }
  /**
   *  @brief Execute method address
   *  @details Execute method address possibly starting a Virtual
   *  Machine if it is a script method address.
   *
   *  @param methodAddress, @c OMethodAddress
   *  @param methodName, @c std::string
   *  @param flags call flags
   *  @param rec receiver object
   *  @param argc number of arguments
   *  @param argv array of arguments
   *  @return std::pair containing
   *    @li first item: @c ORet value returned from method
   *    @li second item: @c int diagnostic value depending on @c flags
   */
  ORet execMethodAddress(const OMethodAddress& methodAddress,
                         const std::string& methodName,
                         ulong flags,
                         OArg rec, int argc, OArg* argv);
  /**
   *  @brief Execute script method
   *  @details This method was designed to be called from
   *  ORet execMethod(const std::string&, ulong, ORef, int, OArg*)
   *  If you want to use it directly, set up a similar environment.
   *
   *  @param method
   *  @param blockIdMethod the method's block index
   *  @param flags call flags
   *  @param rec receiver object
   *  @param argc number of arguments
   *  @param argv array of arguments
   *  @return ORet returned value
   */
  ORet execScriptMethod(const std::string& method,
                        int blockIdMethod,
                        ulong flags,
                        OArg rec, int argc, OArg* argv);

  //::::::::::::::::   Class Registration And Construction   ::::::::::::::::://

  void registerClassesInBlock() const;
  ORet defineClass(OClass* c);
  /**
   *  @brief Set method argument names
   *  @details It checks the argument names with @sa
   *  MDataSegment::checkMethodArgNames(const std::vector<std::string>&) const
   *  and then calls the method
   *  @sa MBlock::setMethodArgNames(std::vector<std::string>&)
   *  to set the argument names of the code block.
   *  \note This method was designed for use by a @em runtime @em manager
   *  lambda to make available the @sa
   *  MBlock::setMethodArgNames(std::vector<std::string>&)
   *  function in the object library.
   *
   *  @param blockId Identifier of method's block
   *  @param argNames Vector of argument names (strings)
   */
  void setMethodArgNames(int blockId, std::vector<std::string>& argNames);

  //::::::::::::::::::::::::::   Package Handling   :::::::::::::::::::::::::://

  /**
   *  @brief Script package loader
   *  @details The script package loader
   *  - compiles the package indicated in `filePath`,
   *  - fixes the block references, localizes the defined classes
   *    (types) using the `typePrefix`, and finally,
   *  - notifies the runtime manager of the new package being handled
   *  \par This method <em>does not</em> run the script to define
   *  the package.
   *  @param filePath `string`
   *  @param typePrefix `string`
   *  @return int The index of the top-level block that is used
   *  to define and export the package
   */
  int loadPackage(const std::string& filePath);
  std::pair<int, std::string>
    exportVars(int, std::vector<std::pair<std::string, ORef>>& varImports);
  std::pair<int, std::string>
    exportAllVars(int, std::vector<std::pair<std::string, ORef>>& varImports);
#if 0
  ORef getPackageObject(int blockId, const std::string& varName);
#endif // 0

  //:::::::::::::::::::::::::   Exception Handling   ::::::::::::::::::::::::://

  /**
   *  @brief Exit call frame as a result of an exception throw
   *  @details This method is recursively called after an exception
   *  throw until a catch block is encountered or the main machine
   *  is exited.
   */
  void exitCallFrame(ObjectAutoCleaner&);
  void defExceptionHandler(int exitCode);
  int defExitHandler();

};

#endif // __VM_HPP__
