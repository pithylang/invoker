#ifndef __MSYMREF_HPP__
#define __MSYMREF_HPP__

#include "mtypes.hpp"
#include "mframe.hpp"

struct MSymbolReference {
  enum Type : ushort {
    RT_STATIC,
    RT_LOCAL,
    RT_REC_REF,
    RT_ARG_REF,
  };

  MBlock* block;
  uint offset;
  const Type refType;

  MSymbolReference() = delete;
  MSymbolReference(MBlock* b, int offset, Type refType) :
      block{b}, offset{static_cast<uint>(offset)}, refType{refType} {}

  /**
   *  @brief Get referenced frame
   *  @details Get parent frame at offset @c offset above a base
   *  frame which is supplied as an argument. It is valid only
   *  for @c RT_LOCAL, @c RT_REC_REF and @c RT_ARG_REF types.
   *  @param MCallFrame* base frame; obtained usually as
   *         @code{parent()->prev} in a virtual machine. Generally,
   *         this depends on how @c frameOffset was counted.
   *  @return MCallFrame*
   */
  MCallFrame* getFrame(MCallFrame* base) const noexcept {
    MCallFrame* frame = base;
    while (frame && frame->block != block)
      frame = frame->prev;
    return frame;
  }
  /**
   *  @brief Dereference local or static variable
   *  @param MCallFrame* base frame; See getFrame(MCallFrame*)
   *         It can be @c nullptr for @c RT_STATIC types.
   *  @return OVar*
   */
  OVar* dereference(MCallFrame* base) const {
    if (refType == RT_STATIC)
      return &block->dataSegment.staticData[offset];
    if (MCallFrame* const frame = getFrame(base); frame) {
      switch (refType) {
        case RT_LOCAL:   return &frame->locals[offset];
        case RT_REC_REF: return frame->receiver;
        case RT_ARG_REF: return frame->args[offset];
      }
    }
    return nullptr;
  }
};

#endif // __MSYMREF_HPP__
