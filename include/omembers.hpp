#ifndef __OMEMBERS_HPP__
#define __OMEMBERS_HPP__

#include <string>
#include <vector>
#include <functional>
// #include <algorithm>

// TODO
#define _OM_LINK

enum : ushort {
  M_PUBLIC        = 0x0000,
  M_PROPERTY      = M_PUBLIC,
  M_PROTECTED     = 0x0001,
  M_INTERNAL      = M_PROTECTED,
  M_PRIVATE       = 0x0002,
  M_INACCESSIBLE  = 0x0004,
  M_ACCESSIBILITY = 0x000f,
  M_CONST         = 0x0010,
  M_READONLY      = M_CONST,
  M_REF           = 0x0020,
  M_STATIC        = 0x0100, // methods only
  M_NOTVALID      = 0x8000
};

inline ushort s_prot(ushort p) noexcept {return (p & M_ACCESSIBILITY);}
inline ushort g_accessibility(ushort p) noexcept {return s_prot(p);}
inline bool g_public    (ushort p) noexcept {return s_prot(p) == M_PUBLIC;}
inline bool g_protected (ushort p) noexcept {return s_prot(p) == M_PROTECTED;}
inline bool g_internal  (ushort p) noexcept {return g_protected(p);         }
inline bool g_private   (ushort p) noexcept {return s_prot(p) == M_PRIVATE;  }
inline bool g_const     (ushort p) noexcept {return (p & M_CONST) != 0; }
inline bool g_noAccess  (ushort p) noexcept {return s_prot(p) == M_INACCESSIBLE;}
inline bool g_accessible(ushort p) noexcept {return !g_noAccess(p);}
inline bool g_ref       (ushort p) noexcept {return (p & M_REF) != 0;}
inline bool g_static    (ushort p) noexcept {return (p & M_STATIC) != 0;}
inline bool g_valid     (ushort p) noexcept {return (p & M_NOTVALID) == 0;}

using StringPrinter = std::function<void(const std::string&)>;

class OClass;

struct _OM_LINK Descriptor {
  /** Descriptor identifier */
  std::string name;
  inline bool valid() const noexcept {return !name.empty();}
  void print(StringPrinter stringPrinter) const {stringPrinter(name);}
};

/**
 *  @class Property
 *  @details Implementation of script-defined properties.
 */
struct _OM_LINK Property : public Descriptor {
  /**
   *  @brief Property class
   *  @details It is a @c nullptr for template and reference properties.
   */
  OClass* type;

  /** Default constructor */
  Property() : type(nullptr) {}
  /** Constructor */
  Property(const std::string& id, OClass *pClass) :
      Descriptor{id}, type(pClass) {}
  Property(std::string&& id, OClass *pClass) :
      type(pClass) {name.swap(id);}

  bool isTemplate(ushort protection) const noexcept {
    return type == nullptr && !g_ref(protection);
  }

  static const Property& null() noexcept {
    static const Property nullMember("", nullptr);
    return nullMember;
  }

  void print(StringPrinter, ushort) const;
};

/**
 *  @class Method
 *  @details Implementation of script-defined methods (SD-methods).
 */
struct _OM_LINK Method : public Descriptor {
  int block;

  /** @brief Default constructor; it creates an invalid \c Method */
  Method() : block(-1) {}
  /** @brief Constructor for SD-methods with known address */
  Method(const std::string& id, int b) : Descriptor{id}, block(b) {}
  /** @brief Constructor for SD-methods with known address */
  Method(std::string&& id, int b) : block(b) {name.swap(id);}

  /** Validate method */
  inline bool valid() const noexcept
  { return Descriptor::valid() && block != -1; }

  /** Compare addresses */
  inline bool sameAddress(const Method& r) const noexcept
  { return block == r.block; }

  /** Return null method */
  static const Method& null() noexcept {
    static const Method nullMethod("", -1);
    return nullMethod;
  }

  void print(StringPrinter, ushort) const;
};

//============================================================================//
//:::::::::::::::::  Script Deployable Object [SDO] Methods  ::::::::::::::::://
//============================================================================//

/**
 *  @class MemberVector
 *  @brief Table of dynamic methods
 *  @details Compiled methods are (compiled) encapsulations of
 *  C or C++ functions. Script-implemented methods are only
 *  available in Invoker scripts.
 */

template<typename T>
class MemberVector : public std::vector<T> {
protected:

  std::vector<ushort> protection_;

public:

  MemberVector() = default;
  MemberVector(const MemberVector&) = default;

  MemberVector& operator=(const MemberVector&) = default;

  MemberVector& operator+=(const MemberVector& b) {
    this->insert(this->end(), b.begin(), b.end());
    auto& bp = b.protection_;
    protection_.insert(protection_.end(), bp.begin(), bp.end());
    return *this;
  }

  MemberVector operator+(const MemberVector& b) {
    MemberVector a = *this;
    return a += b;
  }

  const std::vector<ushort>& protection() const noexcept {return protection_;}
  ushort& protection(int i) noexcept {return protection_[i];}
  ushort protection(int i) const noexcept {return protection_[i];}

  /** Append member to table */
  bool add(const T& m, ushort protection) {
    if (getItem(m).valid()) return false;
    this->push_back(m);
    protection_.push_back(protection);
    return true;
  }

  /** Get offset (index) of member in table */
  int getOffset(const std::string &name) const noexcept {
    for (size_t i = 0; i < this->size(); ++i)
      if ((*this)[i].name == name) return static_cast<int>(i);
    return -1;
  }

  /** Get backwards offset of member in table */
  int getROffset(const std::string &name) const noexcept {
    for (size_t i = this->size(); i; --i)
      if ((*this)[i - 1].name == name) return static_cast<int>(i - 1);
    return -1;
  }

  /** Get offset (index) of member in table */
  inline int getOffset(const Descriptor& d) const noexcept
  { return getOffset(d.name); }

  /** Get member with given name */
  const T &getItem(const std::string &name) const noexcept {
    return const_cast<MemberVector<T>*>(this)->getItem(name);
  }

  /** Get member with given name */
  inline T &getItem(const std::string &name) noexcept {
    const auto i = getOffset(name);
    return i == -1 ? const_cast<T&>(T::null()) : (*this)[i];
  }

  /** Get member with given name */
  inline const T &getItem(const Descriptor& d) const noexcept
  { return getItem(d.name); }

  /** Check if the argument is a valid offset */
  inline bool isOffset(int i) const noexcept
  { return i > -1 && i < (int) this->size(); }
};

/**
 *  @class MethodTable
 *
 *  Table of methods
 */
class _OM_LINK MethodTable : public MemberVector<Method> {
public:
  MethodTable() {}

  MethodTable& operator=(const MethodTable&) = default;

  /** Append method to table */
  bool addMethod(std::string&& name, int b, ushort options);

  void print(StringPrinter) const;
};

/**
 *  @class PropertyTable
 *
 *  Table of properties
 */
class _OM_LINK PropertyTable : public MemberVector<Property> {
public:
  PropertyTable() {}

  PropertyTable& operator=(const PropertyTable&) = default;

  bool isTemplate() const noexcept {
    for (size_t i = 0; i < size(); ++i)
      if ((*this)[i].isTemplate(protection(i))) return true;
    return false;
  }

  size_t countTemplateProps() const noexcept {
    size_t count = 0;
    for (size_t i = 0; i < size(); ++i)
      if ((*this)[i].isTemplate(protection(i))) ++count;
    return count;
  }

#if 0
  bool hasUnknownMembers() const {
    return std::find_if(cbegin(), cend(), [](const Property& m) {
      return m.isUndefType();
    }) != cend();
  }
#endif // 0

  int addPropertyNoCheck(std::string&& name, OClass *c, ushort pl);

  void print(StringPrinter) const;
};

#endif // __OMEMBERS_HPP__
