#ifndef IDGEN_HPP_INCLUDED
#define IDGEN_HPP_INCLUDED

#include <vector>
#include <algorithm>

template<int START_FROM = 0>
class SimpleIdGenerator {
  std::vector<int> repository_;
  int firstAvailable_;

public:
  SimpleIdGenerator() : firstAvailable_{START_FROM} {}
  inline int get()
  { return repository_.empty() ? firstAvailable_++ : getFromRepo(); }
  inline void release(int id) { repository_.push_back(id); }
  inline bool inUse(int id) const {return !inRepo(id) && inBounds(id);}
  inline std::size_t size() const noexcept
  { return firstAvailable_ - START_FROM - repository_.size(); }
  inline bool valid() const noexcept
  { return firstAvailable_ >= START_FROM + repository_.size(); }
  inline std::size_t released() const noexcept { return repository_.size(); }

private:
  inline bool inBounds(int id) const noexcept
  { return id >= START_FROM && id < firstAvailable_; }
  bool inRepo(int val) const
  { return std::find(repository_.begin(), repository_.end(), val) != repository_.end(); }
  int getFromRepo()
  { const int r = repository_.back(); repository_.pop_back(); return r; }
};

template<int START_FROM = 0, bool REUSE = true>
class IdGenerator {
  std::vector<int> repository_;
  std::vector<int> stack_;
  int firstAvailable_;

public:
  IdGenerator() : firstAvailable_(START_FROM) {}
  inline int getId() {const int r = get(); stack_.push_back(r); return r;}
  inline bool inUse(int id) const {return !inRepo(id) && inBounds(id);}
  inline void reset() noexcept
  { firstAvailable_ = START_FROM; repository_.clear(); stack_.clear(); }
  inline int last() const noexcept {return firstAvailable_ - 1;}
  inline int end() const noexcept {return firstAvailable_;}
  inline size_t beginContext() const noexcept {return stack_.size();}
  void resetContext(size_t begin) {
    while (stack_.size() > begin) {
      free(stack_.back());
      stack_.pop_back();
    }
  }
  inline int getContextItem(size_t index) const {return stack_[index];}
  inline size_t contextEnd() const noexcept {return stack_.size();}

private:
  inline int get()
  { return repository_.empty() ? firstAvailable_++ : getFromRepo(); }
  inline void free(int id) {if (REUSE) repository_.push_back(id);}
  inline bool inBounds(int id) const noexcept
  { return id >= START_FROM && id < firstAvailable_; }
  bool inRepo(int val) const
  { return std::find(repository_.begin(), repository_.end(), val) != repository_.end(); }
  int getFromRepo()
  { const int r = repository_.back(); repository_.pop_back(); return r; }
};

#endif // IDGEN_HPP_INCLUDED
