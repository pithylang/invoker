#ifndef __OSWITCHTAB_HPP__
#define __OSWITCHTAB_HPP__

#include <vector>
#include <algorithm>

namespace i1 {

template <class FwdIterator, class T>
static FwdIterator binary_search(FwdIterator first,
                                 FwdIterator last, const T& val) {
  FwdIterator it = lower_bound(first, last, val);
  return (it == last || !(*it == val)) ? last : it;
}

} // namespace i1

struct Switch {
  enum SwitchDataType {
    SWT_UINT,
    SWT_INT,
    SWT_BOOL,
    SWT_STRING,
    SWT_FP
  };
  SwitchDataType dataType;
};

template<typename t, typename IndexType = ushort>
class SwitchData : public Switch {
  using Index_t = IndexType;
  struct SwitchCaseData {
    t caseValue;
    Index_t caseIndex;
    bool operator==(const SwitchCaseData& r) const {return caseValue == r.caseValue;}
    bool operator<(const SwitchCaseData& r) const {return caseValue < r.caseValue;}
  };

  std::vector<SwitchCaseData> entries_;

public:
  using index_t = IndexType;

  SwitchData() {}
  SwitchData(const std::vector<t>& a) {init(a);}

  void init(const std::vector<t>& a) {
    for (size_t i = 0; i < a.size(); ++i)
      entries_.push_back({a[i], static_cast<Index_t>(i)});
    std::sort(entries_.begin(), entries_.end());
  }

  Index_t defaultIndex() const noexcept {return static_cast<Index_t>(size());}
  size_t size() const noexcept {return entries_.size();}

  Index_t index(const t& value) const {
    const SwitchCaseData toFind{value, 0};
    auto it = i1::binary_search(entries_.begin(), entries_.end(), toFind);
    return it == entries_.end() ? defaultIndex() : it->caseIndex;
  }
};

#endif // __OSWITCHTAB_HPP__
