#ifndef __OBASICTYPEINFO_HPP__
#define __OBASICTYPEINFO_HPP__

#include "otypes.h"
#include "object.hpp"

class OClass;

struct OBasicTypeInfo {
  static OClass** getTypeClasses() noexcept
  { extern OClass* _OXType_class[]; return _OXType_class; }
  template<typename t>
  static constexpr int getTypeClassId() noexcept;
  template<typename t>
  static constexpr int getTypeProxyClassId() noexcept;
  template<typename t>
  static constexpr int getVectorClassId() noexcept;
  template<typename t>
  static constexpr int getVectorIteratorClassId() noexcept;
  template<typename t>
  static constexpr int getSetClassId() noexcept;
  template<typename t>
  static constexpr int getSetIteratorClassId() noexcept;

  static constexpr int getObjectVectorClassId() noexcept
  { return CLSID_OBJVECTOR; }
  static constexpr int getObjectSetClassId() noexcept
  { return CLSID_OBJSET; }

  template<typename t>
  inline static OClass* getTypeClass() noexcept
  { return getTypeClasses()[getTypeClassId<t>()]; }
  template<typename t>
  inline static OClass* getTypeProxyClass() noexcept
  { return getTypeClasses()[getTypeProxyClassId<t>()]; }
  template<typename t>
  inline static OClass* getVectorClass() noexcept
  { return getTypeClasses()[getVectorClassId<t>()]; }
  template<typename t>
  inline static OClass* getVectorIteratorClass() noexcept
  { return getTypeClasses()[getVectorIteratorClassId<t>()]; }
  template<typename t>
  inline static OClass* getSetClass() noexcept
  { return getTypeClasses()[getSetClassId<t>()]; }
  template<typename t>
  inline static OClass* getSetIteratorClass() noexcept
  { return getTypeClasses()[getSetIteratorClassId<t>()]; }

  inline static OClass* getObjectVectorClass() noexcept
  { return getTypeClasses()[getObjectVectorClassId()]; }
  inline static OClass* getObjectSetClass() noexcept
  { return getTypeClasses()[getObjectSetClassId()]; }

  template<int _TID> requires (_TID >= 0 && _TID < CLSID_END)
    inline static OClass* getBuiltinType() noexcept
    { return getTypeClasses()[_TID]; }
  template<int _TID> requires (_TID >= 0 && _TID < CLSID_END)
    inline static void setBuiltinType(OClass* type) noexcept
    { return getTypeClasses()[_TID] = type; }

  inline static bool isBasicType(int classId) noexcept
  { return classId <= CLSID_PTR && classId >= CLSID_ULONG; }
  inline static bool isBasicType(ORef o) noexcept
  { return isBasicType(o->getClassId()); }
  inline static bool extendsBasicType(ORef o) noexcept {
    const auto rootId = o->getRootClassId();
    return isBasicType(rootId) && rootId != o->getClassId();
  }

  inline static bool isBasicTypeProxy(int classId) noexcept
  { return classId <= CLSID_PTR_PROXY && classId >= CLSID_ULONG_PROXY; }
  inline static bool isBasicTypeProxy(ORef o) noexcept
  { return isBasicTypeProxy(o->getClassId()); }

  template<typename t>
  inline static bool extendsType(ORef o) noexcept {
    constexpr auto id = getTypeClassId<t>();
    return o->getClassId() != id && o->getRootClassId() == id;
  }
};

typedef OBasicTypeInfo BTI;

template<> constexpr int OBasicTypeInfo::getTypeClassId<ulong>() noexcept {return CLSID_ULONG;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<ulong>() noexcept {return CLSID_ULONG_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<ulong>() noexcept {return CLSID_ULONGVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<ulong>() noexcept {return CLSID_ULONGVECITER;}
template<> constexpr int OBasicTypeInfo::getSetClassId<ulong>() noexcept {return CLSID_ULONGSET;}
template<> constexpr int OBasicTypeInfo::getSetIteratorClassId<ulong>() noexcept {return CLSID_ULONGSETITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<long>() noexcept {return CLSID_LONG;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<long>() noexcept {return CLSID_LONG_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<long>() noexcept {return CLSID_LONGVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<long>() noexcept {return CLSID_LONGVECITER;}
template<> constexpr int OBasicTypeInfo::getSetClassId<long>() noexcept {return CLSID_LONGSET;}
template<> constexpr int OBasicTypeInfo::getSetIteratorClassId<long>() noexcept {return CLSID_LONGSETITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<unsigned>() noexcept {return CLSID_UINT;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<unsigned>() noexcept {return CLSID_UINT_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<unsigned>() noexcept {return CLSID_UINTVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<unsigned>() noexcept {return CLSID_UINTVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<int>() noexcept {return CLSID_INT;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<int>() noexcept {return CLSID_INT_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<int>() noexcept {return CLSID_INTVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<int>() noexcept {return CLSID_INTVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<short>() noexcept {return CLSID_SHORT;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<short>() noexcept {return CLSID_SHORT_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<short>() noexcept {return CLSID_SHORTVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<short>() noexcept {return CLSID_SHORTVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<char>() noexcept {return CLSID_CHAR;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<char>() noexcept {return CLSID_CHAR_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<char>() noexcept {return CLSID_CHARVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<char>() noexcept {return CLSID_CHARVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<double>() noexcept {return CLSID_DBL;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<double>() noexcept {return CLSID_DBL_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<double>() noexcept {return CLSID_DBLVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<double>() noexcept {return CLSID_DBLVECITER;}
template<> constexpr int OBasicTypeInfo::getSetClassId<double>() noexcept {return CLSID_DBLSET;}
template<> constexpr int OBasicTypeInfo::getSetIteratorClassId<double>() noexcept {return CLSID_DBLSETITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<float>() noexcept {return CLSID_FLT;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<float>() noexcept {return CLSID_FLT_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<float>() noexcept {return CLSID_FLTVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<float>() noexcept {return CLSID_FLTVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<bool>() noexcept {return CLSID_BOOL;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<bool>() noexcept {return CLSID_BOOL_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<bool>() noexcept {return CLSID_BOOLVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<bool>() noexcept {return CLSID_BOOLVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<void*>() noexcept {return CLSID_PTR;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<void*>() noexcept {return CLSID_PTR_PROXY;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<void*>() noexcept {return CLSID_PTRVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<void*>() noexcept {return CLSID_PTRVECITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<ORef>() noexcept {return CLSID_OCLASS;}
template<> constexpr int OBasicTypeInfo::getTypeProxyClassId<ORef>() noexcept {return CLSID_NOCLASS;}
template<> constexpr int OBasicTypeInfo::getVectorClassId<ORef>() noexcept {return CLSID_REFVECTOR;}
template<> constexpr int OBasicTypeInfo::getVectorIteratorClassId<ORef>() noexcept {return CLSID_OBJVECITER;}
template<> constexpr int OBasicTypeInfo::getSetClassId<ORef>() noexcept {return CLSID_REFSET;}
template<> constexpr int OBasicTypeInfo::getSetIteratorClassId<ORef>() noexcept {return CLSID_OBJSETITER;}

template<> constexpr int OBasicTypeInfo::getTypeClassId<std::string>() noexcept {return CLSID_STRING;}
template<> constexpr int OBasicTypeInfo::getSetClassId<std::string>() noexcept {return CLSID_STRSET;}
template<> constexpr int OBasicTypeInfo::getSetIteratorClassId<std::string>() noexcept {return CLSID_STRSETITER;}

#endif // __OBASICTYPEINFO_HPP__
