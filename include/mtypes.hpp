#ifndef __MTYPES_HPP__
#define __MTYPES_HPP__

#include <vector>
#include "ovariable.hpp"
#include "itypes.hpp"
#include "mdefs.h"
#include "lvector.hpp"
#include "tree.hpp"

//============================================================================//
//:::::::::::::::::::::::::::::::   MSymbol   :::::::::::::::::::::::::::::::://
//============================================================================//

#define NOUSAGE(x) ((x) & ~SYMTYPE_USAGE_MASK)

/**
 *  @brief Virtual machine symbol data
 *  @details Basic @em Data @em Segment entry.
 *  Representation of symbols during runtime.
 */
struct MSymbol : ISymbol {
  /**
   *  @brief Offset into a local frame
   *
   *  @details When the symbol is a @em local or @em automatic
   *  variable `variableOffset` is an offset into the @em stack
   *  @em frame. When it is a @em static variable or a @em literal,
   *  @c variableOffset is an offset into the @c staticData field
   *  of its @c MDataSegment.
   *
   *  When one of the @em usage flags @c SYMTYPE_PROP_REF,
   *  @c SYMTYPE_SYM_REF is set, @c variableOffset is a reference to
   *  the location of the referenced property or symbol.
   */
  int variableOffset;
  /**
   *  @brief The @c IP that defined the symbol's variable or `-1`
   *
   *  @details All symbols have initially this field set to `-1`.
   *  When the method @c static:type:, or its @em constant
   *  equivalent, is applied to define a @em static variable, the
   *  symbol's @c definitionIP is set to the current value of the
   *  machine register @c IP. On the @em second invocation of the
   *  same method, the symbol's @c definitionIP is checked against
   *  the current @c IP. If they are equal, it is set to @c -2,
   *  otherwise a VM-exception is thrown. When the static variable
   *  definition method is called with the symbol's @c definitionIP
   *  value equal to @c -2, the variable's value is returned without
   *  checking.
   *  @par This method helps in checking for doublicate definitions
   *  of static variables without impairing performance.
   */
  int definitionIP;

  MSymbol() : variableOffset{0}, definitionIP{-1} {}
  MSymbol(ISymbol&& symbol) :
      ISymbol{std::move(symbol)},
      variableOffset{0},
      definitionIP{-1} {}
  MSymbol(MSymbol&& symbol) :
      ISymbol{std::move(symbol)},
      variableOffset{symbol.variableOffset},
      definitionIP{symbol.definitionIP} {}
  MSymbol(const ISymbol& symbol) :
      ISymbol{symbol},
      variableOffset{0},
      definitionIP{-1} {}
  MSymbol(std::string&& text, ushort symbolType) :
      ISymbol{std::move(text), symbolType},
      variableOffset{0},
      definitionIP{-1} {}

  MSymbol& operator=(MSymbol&& r);

  inline bool operator==(const MSymbol& r) const noexcept
  { return NOUSAGE(type) == NOUSAGE(r.type) && name == r.name; }

  static void s_convert(ISymbol& symbol);

  inline bool isStatic() const noexcept {return (type & SYMTYPE_STATIC) != 0;}
  inline void setStatic() noexcept {type |= SYMTYPE_STATIC;}
  inline decltype(type) getVariableType() const noexcept
  { return type & SYMTYPE_CATEGORY_MASK; }
  inline bool isVariable() const noexcept {return getVariableType() == SYMTYPE_VARIABLE;}
  inline bool isSelector() const noexcept {return getVariableType() == SYMTYPE_SELECTOR;}
  inline bool isPropRef() const noexcept {return (type & SYMTYPE_PROP_REF) != 0;}
  inline bool isSymRef() const noexcept {return (type & SYMTYPE_SYM_REF) != 0;}
  inline bool isArgRef() const noexcept {return (type & SYMTYPE_ARG_REF) != 0;}
  inline bool isTmp() const noexcept
  { return name.size() > 1 && name[0] == '#' && s_isNumber(name.c_str() + 1); }

  static bool s_isNumber(const char* text) noexcept;

  int getTmpId() const noexcept;

  std::string disassemble() const;
};

#undef NOUSAGE

//============================================================================//
//::::::::::::::::::::::::   MInstruction, MOperand   :::::::::::::::::::::::://
//============================================================================//
typedef int MOperand;

struct MInstruction {
  MOpcode  opcode;
  MOperand operand;

  inline bool operator==(const MInstruction& r) const
  { return opcode == r.opcode && operand == r.operand; }
  std::string disassemble(int spacing) const;
};

//============================================================================//
//:::::::::::::::::::::::::::::   MStaticData   :::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @class MStaticData
 *  @brief Static data segment
 *  @details It is the data structure of a block's static data
 *  segment modeled after a @c i1::vector<OVariable> with the
 *  added method MStaticData::provide(size_type n), which
 *  preallocates `n` variables.
 */
struct MStaticData : i1::vector<OVariable> {
  void provide(size_type n) {
    reserve(n);
    for (size_type i = 0; i < n; ++i)
      push_back(new OVariable{});
  }
};

//============================================================================//
//::::::::::::::::::::::::::::   MDataSegment   :::::::::::::::::::::::::::::://
//============================================================================//

struct MDataSegment {
  /**
   *  @brief Local symbol table
   *  @details The local symbol table contains ALL symbols accessed
   *  from a code block. Every symbol (variable, member etc) during
   *  runtime is accessed through the local symbol table.
   *  @par The first entry of the local symbol table is @c this.
   */
  std::vector<MSymbol> localSymbolTable;
  /**
   *  @brief Local static data table
   *  @details The local static data table contains the values of
   *  static variables locally accessible during runtime.
   */
  MStaticData staticData;

  /**
   *  @brief Add symbol to local symbol table
   *
   *  @details If the symbol is already in the local symbol table
   *  it is not added. If it is not, it is added. In both cases
   *  the index of the symbol is returned.
   */
  MOperand addSymbol(const ISymbol& symbol);
  /** @brief Move symbol to local symbol table */
  MOperand addSymbol(ISymbol&& symbol);
  /**
   *  @brief Add @c this symbol
   *
   *  @details Add @c this symbol to local symbol table as
   *  argument reference.
   */
  MOperand addThisSymbol(bool argReference);
  /** @brief Find symbol in local symbol table */
  MOperand findSymbol(const ISymbol& symbol) const noexcept;
  /**
   *  @brief Find symbol in local symbol table
   *  @details Find any symbol with identifier @c symbolName in
   *  local symbol table.
   */
  MOperand findSymbol(const std::string& symbolName) const noexcept;
  /**
   *  @brief Check if argument is a symbol
   *  @details Check if there is a symbol with identifier
   *  @c symbolName in the local symbol table.
   */
  bool isSymbolName(const std::string& symbolName) const noexcept;

  void disassemble(std::string& code, int depth) const;
  /**
   *  @brief Set the method's argument names
   *  @details This method does not throw any exceptions. Use
   *  checkMethodArgNames(const std::vector<std::string>&) const
   *  to have the arguments checked for duplicate or the presence
   *  of `this` identifiers.
   *  @param argNames @c std::vector of argument names
   */
  void setMethodArgNames(std::vector<std::string>& argNames);
  /** @deprecated */
  bool setMethodArgNames_PREV(std::vector<std::string>& argNames);
  /**
   *  @brief Check the method's argument names
   *  @details This method throws a @em string if @c this is used
   *  as an argument name, or when there is a duplicate argument
   *  identifier. The caller must catch it and handle it by
   *  throwing a VM run-time exception (RTE).
   *
   *  @param argNames @c std::vector of argument names
   */
  void checkMethodArgNames(const std::vector<std::string>& argNames) const;
  /**
   *  @brief Refresh lambda's argument names
   *  @details A lambda's prototype is fixed on first call.
   *  However, when the `Session` flag `RTFL_MUTABLE_LAMBDA_PROTO`
   *  is set, subsequent calls can change the prototype. This
   *  method refreshes the lambda's argument variables according
   *  to the supplied argument list.
   *  @par If the `Session` flag `RTFL_UNDEF_ARGS_DIAGS` is set,
   *  it prepares all previously used but now undefined argument
   *  variables for diagnostics by invalidating their variable
   *  offset (it sets it to -1) so that on access, the machine
   *  will throw a runtime exception.
   *  @par This method does not throw any exceptions.
   *
   *  @param argNames @c std::vector of argument names
   *  @param diagnoseUndefinedArgs @c bool `true` if
   *  `RTFL_UNDEF_ARGS_DIAGS` is set; otherwise `false`
   */
  void refreshLambdaArglist(std::vector<std::string>& argNames,
                            bool diagnoseUndefinedArgs);
  void prepDiagsForLambdaNoArgs();
  /**
   *  @brief Check lambda's prototype
   *  @details A lambda's prototype is fixed on first call.
   *  Unless the `Session`'s flag `RTFL_MUTABLE_LAMBDA_PROTO` is
   *  set, subsequent calls must use the same prototype. This
   *  method checks if the supplied argument names will change
   *  the lambda's prototype.
   *  @par This method does not throw any exceptions.
   *
   *  @param argNames @c std::vector of argument names
   */
  bool checkLambdaProto(std::vector<std::string>& argNames);

private:
  inline void add(const ISymbol& symbol)
  { localSymbolTable.push_back(MSymbol(symbol)); }
  inline void add(ISymbol&& symbol)
  { localSymbolTable.push_back(MSymbol{std::move(symbol)}); }
};

//============================================================================//
//:::::::::::::::::::::::::::::::   MBlock   ::::::::::::::::::::::::::::::::://
//============================================================================//

namespace i1 {
  template<typename t>
  struct vector;
}

struct MBlock {
  enum Type : uint8_t {
    PLAIN = 0,
    CLASS,
    METHOD,
    LOOP,
    LOOP_INIT,
    LOOP_COND,
    LOOP_STEP,
  };

  struct InheritanceData {
    std::string name, base;
    InheritanceData(const std::string& name, const std::string& base) :
        name(name), base(base) {}
    InheritanceData(const InheritanceData& r) : name(r.name), base(r.base) {}
    InheritanceData(InheritanceData&& r) :
        name(std::move(r.name)), base(std::move(r.base)) {}
    bool operator==(const InheritanceData& r) const noexcept
    { return name == r.name; }
  };

  MDataSegment dataSegment;
  std::vector<MInstruction> codeSegment;
  i1::Tree<InheritanceData> classInheritanceTree;
  /**
   *  @brief Pointer to object class
   *  @details Pointer to object class defined by this block. When
   *  it is @c nullptr this is not a class block.
   */
  OClass* definedClass{nullptr};
  /**
   *  @brief Parent block index
   */
  int parentIndex{-1};
  bool evaluate; /**< See IBlock */
  bool execute;  /**< See IBlock */
  /**
   *  @brief Block type
   *  @details Block types are determined during runtime. After
   *  the compilation phase all blocks are @c PLAIN.
   */
  Type type;

  MBlock() = default;
  MBlock(MBlock&&);
  inline MOperand addSymbol(const ISymbol& symbol)
  { return dataSegment.addSymbol(symbol); }
  inline MOperand addSymbol(ISymbol&& symbol)
  { return dataSegment.addSymbol(std::move(symbol)); }
  inline int getBlockIndex(MOperand symbolIndex) const
  { return dataSegment.localSymbolTable[symbolIndex].getBlockId(); }
  inline bool isBlock(MOperand symbolIndex) const
  { return getBlockIndex(symbolIndex) != -1; }
  /**
   *  @brief Finalize block
   *  @details This method
   *  @li assigns offsets to local and static variables
   *  @li creates static data segment
   *  @li fixes the @c enter instruction
   */
  void finalize();
  void orderClasses();

  void disassemble(std::string& code, int depth) const;
  void disassembleCodeSegment(std::string& code, int depth) const;

  const MBlock* getParentMethodBlock(const i1::vector<MBlock>&) const;
  const OClass* getOwnerClass(const i1::vector<MBlock>&) const;
  /**
   *  @brief Set method argument names
   *  @details Sets the block type to @c METHOD and calls
   *  MDataSegment::setMethodArgNames(std::vector<std::string>&).
   */
  void setMethodArgNames(std::vector<std::string>& argNames);
};

#endif // __MTYPES_HPP__
