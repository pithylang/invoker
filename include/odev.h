#ifndef __ODEV_H__
#define __ODEV_H__

#ifdef __DEBUG__
# ifndef ASSERT
#   define ASSERT(x) if(!(x)) { throw \
      "Assertion " #x " failed in " __FILE__ ", line "+std::to_string(__LINE__);}
# endif
# ifndef OM_ASSERT
#   define OM_ASSERT(x,m) if(!(x)) { \
      m->displayMessage("Error", "Assertion failed in " __FILE__ ", line "+\
      std::to_string(__LINE__)); exit(2);}
# endif
# ifndef OM_DEBUG
#   define OM_DEBUG(statements) statements
# endif
# define NOEXCEPT_IF_NDEBUG noexcept(false)
#else /*!defined(__DEBUG__)*/
# ifndef ASSERT
#   define ASSERT(x)
# endif
# ifndef OM_ASSERT
#   define OM_ASSERT(x,m)
# endif
# ifndef OM_DEBUG
#   define OM_DEBUG(statements)
# endif
# define NOEXCEPT_IF_NDEBUG noexcept(true)
#endif /*__DEBUG__*/

#ifndef _ASSERT
# define  _ASSERT(x,m) OM_ASSERT(x,m)
#endif
#ifndef _DEBUG
# define  _DEBUG(stm) OM_DEBUG(stm)
#endif

// programming error
#define OM_PROG_ERROR_IF(x) if(x) throw \
  "programming error (file " __FILE__ ", line "+std::to_string(__LINE__)+")";
#define OM_PROG_ERROR_MSG_IF(x, MSG) if(x) throw \
  "programming error (file " __FILE__ ", line "+std::to_string(__LINE__)+"): "+MSG;
#define _PROG_ERROR_IF(x) OM_PROG_ERROR_IF(x)
#define _PROG_ERROR_MSG_IF(x, MSG) OM_PROG_ERROR_MSG_IF(x, MSG)

#define OM_ARG(n) (argv[n]->objectRef)
#define OM_ARG_CLASS(i,CLSPTR) (OM_ARG(i)->getClass() == (CLSPTR))
/**
 *  @brief Test argument's type (class)
 *  @param i @c int The argument's index
 *  @param CLSPTR @c OClass* Pointer to a @c core class
 */
#define OM_ARG_TYPE(i,CLSPTR) (OM_ARG(i)->getRootClass() == (CLSPTR))
/**
 *  @brief Test argument's type identifier
 *  @param i @c int The argument's index
 *  @param CLSID @c int The identifier of a @c core class
 */
#define OM_ARG_TYPEID(i,CLSID) (OM_ARG(i)->getRootClassId() == (CLSID))

#define OM_CAST(T, id) static_cast<T*>(static_cast<ox##T*>(id))
#define OM_VALUE(T, id) *OM_CAST(T, id)
#define OM_UNPACK_VAR(T, var, argno) T var = OM_VALUE(T, OM_ARG(argno))
#define OM_UNPACK_CONST_VAR(T, var, n) const T var = OM_VALUE(T, OM_ARG(n))
#define OM_UNPACK_PTR(T, ptr, argno) T *ptr = OM_CAST(T, OM_ARG(argno))
#define OM_UNPACK_REF(T, ref, argno) T& ref = OM_VALUE(T, OM_ARG(argno))
#define OM_UNPACK_CONST_REF(T, ref, n) const T& ref = OM_VALUE(T, OM_ARG(n))

#define OM_VAR_DECL(T, var, id) T var = OM_VALUE(T, id)
#define OM_CONST_VAR_DECL(T, var, id) const T var = OM_VALUE(T, id)
#define OM_PTR_DECL(T, ptr, id) T *ptr = OM_CAST(T, id)
#define OM_REF_DECL(T, ref, id) T& ref = OM_VALUE(T, id)
#define OM_CONST_REF_DECL(T, ref, id) const T& ref = OM_VALUE(T, id)

#ifdef __MTH_NOCHECK__

# define OM_ENTER_NOCHK(mth)
# define OM_ENTER(mth,n)
# define OM_ENTER_METHOD_NOCHK(mth)
# define OM_ENTER_METHOD(mth,n)
# define OM_LEAVE()
# define OM_CHECK_ARGC(n)
# define OM_CHECK_ARG_TYPE(i,CLSPTR)

#else // this is the default

# define new_OM_ENTER_NOCHK(mth) \
    const auto& _om_cmethod_addr=&mth
# define OM_DECL_METHOD_NAME(mth) \
    const char* _ox_method_name=(mth)
# define OM_ENTER_NOCHK(mth) \
    static const char* _ox_method_name=mth
# define OM_ENTER(mth,n) \
    OM_ENTER_NOCHK(mth); \
    OM_CHECK_ARGC(n)
# define OM_ENTER_METHOD_NOCHK(mth) \
    static const std::string _ox_method_name{mth}
# define OM_ENTER_METHOD(mth,n) \
    OM_ENTER_METHOD_NOCHK(mth); \
    OM_CHECK_ARGC(n)
# define OM_LEAVE() return this->makeResult()

#endif /*__MTH_NOCHECK__*/

//::::::::::::::::::::::::::::   size checking   ::::::::::::::::::::::::::::://

#define OM_CHECK_ARGC_METHOD(mth,n)                                     \
    if (argc != (n)) {                                                  \
      m->throwBadArgc(__PRETTY_FUNCTION__, mth, n, __FILE__, __LINE__); \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARGC(n) OM_CHECK_ARGC_METHOD(_ox_method_name, n)

#define OM_CHECK_ARGC_2(a,b)                                                   \
    if (argc != (a) && argc != (b)) {                                          \
      const std::string msg = "unexpected number of arguments: expected "+     \
          std::to_string(a)+" or "+std::to_string(b)+" but found "+            \
          std::to_string(argc);                                                \
      m->throwBadArgc(__PRETTY_FUNCTION__, _ox_method_name, msg);              \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARGC_RANGE(a, b)                                              \
    if (argc < (a) || argc > (b)) {                                            \
      const std::string msg = "unexpected number of arguments: expected "+     \
          std::to_string(a)+" to "+std::to_string(b)+" but found "+            \
          std::to_string(argc);                                                \
      m->throwBadArgc(__PRETTY_FUNCTION__, _ox_method_name, msg);              \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARG_SIZE_METHOD(mth,sz,V,n)                                   \
    if (const auto msg = OInstance::sizeCheckMessage(                          \
          sz,(V),(n)+1); !msg.empty()) {                                       \
      m->throwBadUsage(__PRETTY_FUNCTION__, msg, mth, __FILE__, __LINE__);     \
      return OVariable::undefinedResult(); }

#define OM_CHECK_VAR_SIZE_METHOD(mth,sz,V)                                     \
    if (const auto msg = OInstance::sizeCheckMessage(sz,V,0,1);                \
          !msg.empty()) {                                                      \
      m->throwBadUsage(__PRETTY_FUNCTION__, msg, mth, __FILE__, __LINE__);     \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARG_SIZE(SZ,V,N) OM_CHECK_ARG_SIZE_METHOD(_ox_method_name,SZ,V,N)
#define OM_CHECK_VAR_SIZE(SZ,V) OM_CHECK_VAR_SIZE_METHOD(_ox_method_name,SZ,V)

//::::::::::::::::::::::::::::   type checking   ::::::::::::::::::::::::::::://

#define OM_CHECK_TYPEID(o, _I, W, ARGN)                                        \
    if (auto msg = (o)->typeCheckMessage(m, (_I), (W), ARGN); !msg.empty()) {  \
      m->throwBadArg(_ox_method_name, msg);                                    \
      return OVariable::undefinedResult(); }

#define OM_CHECK_TYPE(o, _T, W, ARGN)                                          \
    if (auto msg = (o)->typeCheckMessage((_T), (W), (ARGN)); !msg.empty()) {   \
      m->throwBadArg(_ox_method_name, msg);                                    \
      return OVariable::undefinedResult(); }

/**
 *  @brief Check argument type
 *
 *  @details Checks if an argument has type id `ID` and throws a
 *  `bad arg` exception if it has not.
 *  @param i `int` The argument number starting from 0
 */
#define OM_CHECK_ARG_TYPEID(i,CLSID)                                    \
    OM_ASSERT(m->getClass(CLSID)->isCoreClass(), m);                    \
    if (!OM_ARG_TYPEID(i, CLSID)) {                                     \
      const std::string& expected = m->getClass(CLSID)->name;           \
      const std::string msg = "unexpected type for argument "+          \
          std::to_string(i+1)+": expected '"+expected+"' but found '"+  \
          OM_ARG(i)->getClassName()+"'";                                \
      m->throwBadArg(_ox_method_name, msg, __FILE__, __LINE__);         \
      return OVariable::undefinedResult(); }

/**
 *  @brief Check if argument is one of the supplied types
 *
 *  @details Checks if an argument is of type `_T` and throws a
 *  `bad arg` exception if it is not. `_T` can be a single type,
 *  `const OClass*`, or a vector of types.
 *  @par It uses Session::typeCheckMessage(ORef,const OClass*,
 *  int,int) or one of its variants.
 *  @param i `int` The argument number starting from 0
 */
#define OM_CHECK_ARG_TYPES(i, _T) OM_CHECK_TYPE(OM_ARG(i), (_T), (i)+1, 0)
#define OM_CHECK_ARG_TYPEIDS(i, _I) OM_CHECK_TYPEID(OM_ARG(i), (_I), (i)+1, 0)
/**
 *  @brief Check argument type
 *
 *  @details Checks if an argument is of type `_T` and throws a
 *  `bad arg` exception if it is not. `_T` is a `const OClass*`.
 *  @param i `int` The argument number starting from 0
 */
#define OM_CHECK_ARG_TYPE(i, _T)                                            \
    OM_ASSERT((_T)->isCoreClass(), m);                                      \
    if (!OM_ARG_TYPE(i, _T)) {                                              \
      m->throwBadArg(_ox_method_name, "unexpected type for argument "+      \
          std::to_string(i + 1)+": expected '"+(_T)->name+"' but found '"+  \
          OM_ARG(i)->getClassName()+"'");                                   \
      return OVariable::undefinedResult(); }
/**
 *  @brief Check argument element type
 *
 *  @details Checks if an element of a vector argument has type
 *  id `ID` and throws a `bad arg` exception if it has not.
 *  @par It uses Session::typeCheckMessage(ORef,const OClass*,
 *  int,int) or one of its variants.
 *  @param argn `int` The argument number starting from 0
 *  @param i `int` The element index starting from 0
 */
#define OM_CHECK_ARG_ELEM_TYPEID(o, ID, argn, i) \
    OM_CHECK_TYPEID(o, ID, -1-(i), (argn)+1)
/**
 *  @brief Check argument element type
 *
 *  @details Checks if an element of a vector argument is of type
 *  `TYPE` and throws a `bad arg` exception if it is not.
 *  @par It uses Session::typeCheckMessage(ORef,const OClass*,
 *  int,int) or one of its variants.
 *  @param argn `int` The argument number starting from 0
 *  @param i `int` The element index starting from 0
 */
#define OM_CHECK_ARG_ELEM_TYPE(o, TYPE, argn, i) \
    OM_CHECK_TYPE(o, TYPE, -1-(i), (argn)+1)
/**
 *  @brief Check object type
 *
 *  @details Checks if an object is of type `_T` and throws a
 *  `bad usage` exception if it is not. `_T` can be a single
 *  type, `const OClass*`, or a vector of types.
 *  @par It uses Session::typeCheckMessage(ORef,const OClass*,
 *  int,int) or one of its variants.
 */
#define OM_CHECK_VAR_TYPE(o, _T)                                               \
    if (auto msg = (o)->typeCheckMessage((_T), 0, 0); !msg.empty()) {          \
      m->throwBadUsage(__PRETTY_FUNCTION__, msg, _ox_method_name);             \
      return OVariable::undefinedResult(); }
/**
 *  @brief Check object type
 *
 *  @details Checks if an object has type id `ID` and throws a
 *  `bad usage` exception if it is not.
 *  @par It uses Session::typeCheckMessage(ORef,const OClass*,
 *  int,int) or one of its variants.
 */
#define OM_CHECK_VAR_TYPEID(o, ID)                                             \
    if (auto msg = (o)->typeCheckMessage(m, ID, 0, 0); !msg.empty()) {         \
      m->throwBadUsage(__PRETTY_FUNCTION__, msg, _ox_method_name);             \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARG_CLASS_METHOD(i,_T,METHOD)                                 \
    if (const bool test = OM_ARG_CLASS(i, _T); !test) {                        \
      const std::string msg = OInstance::typeCheckMessage(test,                \
          OM_ARG(i)->getClassName(), (_T)->getClassName(), (i)+1);             \
      m->throwBadArg(METHOD, msg, __FILE__, __LINE__);                         \
      return OVariable::undefinedResult(); }

#define OM_CHECK_ARG_CLASS(i,P) OM_CHECK_ARG_CLASS_METHOD(i,P,_ox_method_name)

//::::::::::::::::::::::::::::::   mutability   :::::::::::::::::::::::::::::://

#define OM_MUTABLE_REC_MSG(msg) if (!rec->isMutable())                         \
      OM_THROW_BAD_USAGE(msg)
#define OM_MUTABLE_REC() if (!rec->isMutable())                                \
      OM_THROW_BAD_USAGE("cannot modify constant receiver")
#define OM_MUTABLE_VAR(var, msg) if (!(var)->isMutable())                      \
      OM_THROW_BAD_USAGE(msg)
#define OM_MUTABLE_ARG_MSG(n, msg) if (!argv[n]->isMutable())                  \
      OM_THROW_BAD_USAGE(msg)
#define OM_MUTABLE_ARG_METHOD(n, method) if (!argv[n]->isMutable()) {          \
      std::string msg = "cannot modify constant argument "+std::to_string(n+1);\
      m->throwBadUsage(__PRETTY_FUNCTION__, msg, method);                      \
      return OVariable::undefinedResult(); }
#define OM_MUTABLE_ARG(n) OM_MUTABLE_ARG_METHOD(n, _ox_method_name)
#define OM_MUTABLE_REC_METHOD(method) if (!rec->isMutable()) {                 \
      m->throwBadUsage(__PRETTY_FUNCTION__,                                    \
          "cannot modify constant receiver", method);                          \
      return OVariable::undefinedResult(); }

//::::::::::::::::::::::::::::::   exceptions   :::::::::::::::::::::::::::::://

#define OM_THROW_RTE_FULL(msg) {                                   \
            m->throwRTE(_ox_method_name, msg, __FILE__, __LINE__); \
            return OVariable::undefinedResult(); }
#define OM_THROW_RTE(msg) {                             \
            m->throwRTE(_ox_method_name, msg);          \
            return OVariable::undefinedResult(); }
#define OM_THROW_RTE_NORET(msg) {                       \
            m->throwRTE(_ox_method_name, msg);          \
            return; }
#define OM_THROW_RTE_RETURN(msg, __RV) {                \
            m->throwRTE(_ox_method_name, msg);          \
            return __RV; }
#define OM_THROW_RTE_FULL_METHOD(meth, msg) {           \
            m->throwRTE(meth, msg, __FILE__, __LINE__); \
            return OVariable::undefinedResult(); }
#define OM_THROW_RTE_METHOD(meth, msg) {                \
            m->throwRTE(meth, msg);                     \
            return OVariable::undefinedResult(); }

#define OM_THROW_BAD_ARG(msg) {                         \
            m->throwBadArg(_ox_method_name, msg);       \
            return OVariable::undefinedResult(); }
#define OM_THROW_BAD_USAGE(msg) {                                       \
            m->throwBadUsage(__PRETTY_FUNCTION__, msg, _ox_method_name);\
            return OVariable::undefinedResult(); }
#define OM_THROW_BAD_USAGE_RETURN(msg, rv) {                            \
            m->throwBadUsage(__PRETTY_FUNCTION__, msg, _ox_method_name);\
            return (rv); }
#define OM_THROW_BAD_ARG_METHOD(methnam, msg) {         \
            m->throwBadArg(methnam, msg);               \
            return OVariable::undefinedResult(); }
#define OM_THROW_BAD_USAGE_METHOD(methnam, MSG) {                       \
            m->throwBadUsage(__PRETTY_FUNCTION__, MSG, methnam);        \
            return OVariable::undefinedResult(); }

#define OM_RETHROW_RETURN(rv, method) if (m->exceptionPending())        \
            return m->rethrow(method, __FILE__, __LINE__),(rv)
#define OM_RETHROW_HERE_RETURN(rv) if (m->exceptionPending())           \
            return m->rethrow(_ox_method_name, __FILE__, __LINE__),(rv)
#define OM_RETHROW(method) if (m->exceptionPending())                   \
            return OVariable(m->rethrow(method, __FILE__, __LINE__))
#define OM_RETHROW_HERE() if (m->exceptionPending())                    \
            return OVariable(m->rethrow(_ox_method_name, __FILE__, __LINE__))
#define OM_RETHROW_NORET(method) if (m->exceptionPending())             \
            {m->rethrow(method, __FILE__, __LINE__); return;}
#define OM_RETHROW_HERE_NORET() if (m->exceptionPending())              \
            {m->rethrow(_ox_method_name, __FILE__, __LINE__); return;}
#define OM_RETHROW_IF(condition, method) if (condition)                 \
            return OVariable(m->rethrow(method, __FILE__, __LINE__))
#define OM_RETHROW_HERE_IF(condition) if (condition)                    \
            return OVariable(m->rethrow(_ox_method_name, __FILE__, __LINE__))

#define OM_CHECK_EXCEPTION() OM_RETHROW_HERE()
#define OM_CHECK_EXCEPTION_NO_RETHROW() \
    if (m->exceptionPending()) return OVariable::undefinedResult()
#define OM_CHECK_EXCEPTION_RETURN(val) \
    if (m->exceptionPending()) return (val)

#define OM_RETURN_THIS                                     \
    return makeResult();
#define OM_RETURN_INT(i) {                                 \
    ORef __r = BTI::getTypeClass<int>()->createObject(m);  \
    OM_CHECK_EXCEPTION();                                  \
    static_cast<oxType<int>*>(__r)->setValue(i);           \
    return __r->disposableResult(); }
#define OM_RETURN_BOOL(b) {                                \
    ORef __r = BTI::getTypeClass<bool>()->createObject(m); \
    OM_CHECK_EXCEPTION();                                  \
    static_cast<oxType<bool>*>(__r)->setValue(b);          \
    return __r->disposableResult(); }
#define OM_RETURN_TYPE(t, CLASS_PTR, value) {              \
    ORef __r = CLASS_PTR->createObject(m);                 \
    OM_CHECK_EXCEPTION();                                  \
    *static_cast<t*>(__r) = value;                         \
    return __r->disposableResult(); }
#define OM_RETURN(__r)                                     \
    return (__r)->makeResult();
#define OM_RETURN_CHECK(__r)                               \
    return m->exceptionPending() ?                         \
        OVariable{m->rethrow(_ox_method_name,              \
            __FILE__, __LINE__)} : (__r)->makeResult();
#define OM_RETURN_DISPOSABLE(__r)                          \
    return (__r)->disposableResult();

// Shorter versions
#define _ARG(n)                         OM_ARG(n)
#define _ARG_TYPE(i,CLSPTR)             OM_ARG_TYPE(i,CLSPTR)
#define _ARG_CLASS(i,CLSPTR)            OM_ARG_CLASS(i,CLSPTR)
#define _ARG_TYPEID(i,CLSID)            OM_ARG_TYPEID(i,CLSID)
#define _ENTER_NOCHK(mth)               OM_ENTER_NOCHK(mth)
#define _ENTER(mth,n)                   OM_ENTER(mth,n)
#define _ENTER_METHOD_NOCHK(mth)        OM_ENTER_METHOD_NOCHK(mth)
#define _ENTER_METHOD(mth,n)            OM_ENTER_METHOD(mth,n)
#define _DECL_METHOD_NAME               OM_DECL_METHOD_NAME
#define _LEAVE()                        OM_LEAVE()
#define _CHECK_ARGC(n)                  OM_CHECK_ARGC(n)
#define _CHECK_ARGC_METHOD(mth,n)       OM_CHECK_ARGC_METHOD(mth,n)
#define _CHECK_ARGC_2(n1,n2)            OM_CHECK_ARGC_2(n1,n2)
#define _CHECK_ARGC_RANGE(beg, end)     OM_CHECK_ARGC_RANGE(beg, end)
#define _CHECK_ARG_SIZE_METHOD(M,S,V,N) OM_CHECK_ARG_SIZE_METHOD(M,S,V,N)
#define _CHECK_VAR_SIZE_METHOD(MN,SZ,V) OM_CHECK_VAR_SIZE_METHOD(MN,SZ,V)
#define _CHECK_ARG_SIZE(SZ,V,N)         OM_CHECK_ARG_SIZE(SZ,V,N)
#define _CHECK_VAR_SIZE(SZ,V)           OM_CHECK_VAR_SIZE(SZ,V)
#define _CHECK_TYPEID(o,CLSPTR,w,m)     OM_CHECK_TYPEID(o,CLSPTR,w,m)
#define _CHECK_ARG_TYPEID(i,ID)         OM_CHECK_ARG_TYPEID(i,ID)
#define _CHECK_ARG_TYPEIDS              OM_CHECK_ARG_TYPEIDS
#define _CHECK_VAR_TYPEID(o,ID)         OM_CHECK_VAR_TYPEID(o,ID)
#define _CHECK_ARG_ELEM_TYPEID(o,I,n,i) OM_CHECK_ARG_ELEM_TYPEID(o,I,n,i)
#define _CHECK_TYPE(o,CLSPTR,w,m)       OM_CHECK_TYPE(o,CLSPTR,w,m)
#define _CHECK_ARG_TYPE(i,CLSPTR)       OM_CHECK_ARG_TYPE(i,CLSPTR)
#define _CHECK_VAR_TYPE(o,T)            OM_CHECK_VAR_TYPE(o,T)
#define _CHECK_ARG_ELEM_TYPE(o,T,n,i)   OM_CHECK_ARG_ELEM_TYPE(o,T,n,i)
#define _CHECK_ARG_CLASS(i,CLSPTR)      OM_CHECK_ARG_CLASS(i,CLSPTR)
#define _CHECK_ARG_CLASS_METHOD(i,CP,m) OM_CHECK_ARG_CLASS_METHOD(i,CP,m)
#define _MUTABLE_REC_MSG(msg)           OM_MUTABLE_REC_MSG(msg)
#define _MUTABLE_REC()                  OM_MUTABLE_REC()
#define _MUTABLE_VAR(var, msg)          OM_MUTABLE_VAR(var, msg)
#define _MUTABLE_ARG_MSG(n, msg)        OM_MUTABLE_ARG_MSG(n, msg)
#define _MUTABLE_ARG(n)                 OM_MUTABLE_ARG(n)
#define _MUTABLE_REC_METHOD(mth)        OM_MUTABLE_REC_METHOD(mth)
#define _MUTABLE_ARG_METHOD(n, mth)     OM_MUTABLE_ARG_METHOD(n, mth)
#define _CHECK_EXCEPTION_RETURN(v)      OM_CHECK_EXCEPTION_RETURN(v)
#define _CHECK_EXCEPTION()              OM_CHECK_EXCEPTION()
#define _CHECK_EXCEPTION_NO_RETHROW()   OM_CHECK_EXCEPTION_NO_RETHROW()
#define _THROW_RTE_FULL(msg)            OM_THROW_RTE_FULL(msg)
#define _THROW_RTE(msg)                 OM_THROW_RTE(msg)
#define _THROW_RTE_NORET(msg)           OM_THROW_RTE_NORET(msg)
#define _THROW_RTE_RETURN(msg,__RV)     OM_THROW_RTE_RETURN(msg,__RV)
#define _THROW_RTE_FULL_METHOD(m,msg)   OM_THROW_RTE_FULL_METHOD(m,msg)
#define _THROW_RTE_METHOD(meth,msg)     OM_THROW_RTE_METHOD(meth,msg)
#define _THROW_BAD_ARG(msg)             OM_THROW_BAD_ARG(msg)
#define _THROW_BAD_ARG_METHOD(msg,M)    OM_THROW_BAD_ARG_METHOD(msg,M)
#define _THROW_BAD_USAGE(msg)           OM_THROW_BAD_USAGE(msg)
#define _THROW_BAD_USAGE_RETURN(msg,RV) OM_THROW_BAD_USAGE_RETURN(msg,RV)
#define _THROW_BAD_USAGE_METHOD(msg,M)  OM_THROW_BAD_USAGE_METHOD(msg,M)
#define _RETHROW_RETURN(rv,M)           OM_RETHROW_RETURN(rv,M)
#define _RETHROW_HERE_RETURN(rv)        OM_RETHROW_HERE_RETURN(rv)
#define _RETHROW(method)                OM_RETHROW(method)
#define _RETHROW_HERE()                 OM_RETHROW_HERE()
#define _RETHROW_NORET(method)          OM_RETHROW_NORET(method)
#define _RETHROW_HERE_NORET()           OM_RETHROW_HERE_NORET()
#define _RETHROW_IF(cond, meth)         OM_RETHROW_IF(cond, meth)
#define _RETHROW_HERE_IF(cond)          OM_RETHROW_HERE_IF(cond)
#define _UNPACK_VAR(T, var, argno)      OM_UNPACK_VAR(T, var, argno)
#define _UNPACK_CONST_VAR(T, var, n)    OM_UNPACK_CONST_VAR(T, var, n)
#define _UNPACK_PTR(T, ptr, argno)      OM_UNPACK_PTR(T, ptr, argno)
#define _UNPACK_REF(T, ref, argno)      OM_UNPACK_REF(T, ref, argno)
#define _UNPACK_CONST_REF(T, ref, n)    OM_UNPACK_CONST_REF(T, ref, n)
#define _CAST(T, id)                    OM_CAST(T, id)
#define _VALUE(T, id)                   OM_VALUE(T, id)

#define _VAR_DECL(T, var, id)           OM_VAR_DECL(T, var, id)
#define _CONST_VAR_DECL(T, var, id)     OM_CONST_VAR_DECL(T, var, id)
#define _PTR_DECL(T, ptr, id)           OM_PTR_DECL(T, ptr, id)
#define _REF_DECL(T, ref, id)           OM_REF_DECL(T, ref, id)
#define _CONST_REF_DECL(T, ref, id)     OM_CONST_REF_DECL(T, ref, id)

#define _RETURN_THIS                    OM_RETURN_THIS
#define _RETURN_INT(i)                  OM_RETURN_INT(i)
#define _RETURN_BOOL(b)                 OM_RETURN_BOOL(b)
#define _RETURN_TYPE(t,CLASS_PTR,value) OM_RETURN_TYPE(t,CLASS_PTR,value)
#define _RETURN(r)                      OM_RETURN(r)
#define _RETURN_CHECK(r)                OM_RETURN_CHECK(r)
#define _RETURN_DISPOSABLE(r)           OM_RETURN_DISPOSABLE(r)

//============================================================================//
//:::::::::::::::::::::::::::::   Class Macros   ::::::::::::::::::::::::::::://
//============================================================================//

#define OM_T(CSTEM) ox##CSTEM
#define _ox(CSTEM) ox##CSTEM
#define OM_O(CSTEM) O##CSTEM
#define _O(CSTEM) OM_O(CSTEM)

#define OM_DECLARE_METHOD_TABLE()                                              \
    private:                                                                   \
      static OCoreMethodInfo           sm_SMethodTable[];                      \
    public:                                                                    \
      static oxCInheritanceData        sm_mtab;                                \
      static oxCInheritanceData       *getCInheritanceData()                   \
      { return &sm_mtab; }

#define OM_BEGIN_METHOD_TABLE(CLS, BASECLS)                                    \
    oxCInheritanceData CLS::sm_mtab =                                          \
    { &(BASECLS::sm_mtab), CLS::sm_SMethodTable };                             \
    OCoreMethodInfo CLS::sm_SMethodTable[] = {

#define OM_BEGIN_EXPORTED_METHOD_TABLE(CLS, BASECLS)                           \
    oxCInheritanceData _OM_LINK CLS::sm_mtab =                                 \
    { &(BASECLS::sm_mtab), CLS::sm_SMethodTable };                             \
    OCoreMethodInfo _OM_LINK CLS::sm_SMethodTable[] = {

#define OM_END_METHOD_TABLE()                                                  \
      { (CAddr)0, 0, nullptr }                                                 \
    };

#define PUBLIC(m,a) {(CAddr)&m,0,a},

#define OM_BEGIN_CLASS_DECLARATION(CSTEM)                                      \
    class ox##CSTEM : public OInstance, public CSTEM {                         \
      typedef ox##CSTEM T;                                                     \
    public:                                                                    \
      typedef CSTEM base_class;                                                \
      ox##CSTEM(OClass *cls) : OInstance(cls) {}                               \
      ~ox##CSTEM() {}                                                          \
      T& operator=(const T& r)                                                 \
      { *static_cast<CSTEM*>(this) = r; return *this; }                        \
    protected:

  // Here come user method declarations, e.g.
  // ORef my_om_exported_method(int, OArg*, Session*);

#define OM_METHOD(method) ORet method(OArg, int, OArg*, Session*)

#define OM_END_CLASS_DECLARATION()                                             \
      OM_DECLARE_METHOD_TABLE()                                                \
    };
#define OM_END_TYPE_DECLARATION OM_END_CLASS_DECLARATION

#define OM_BEGIN_NOCOPY_CLASS_DECLARATION(CSTEM)                               \
    class ox##CSTEM : public OInstance, public CSTEM {                         \
      typedef ox##CSTEM T;                                                     \
    public:                                                                    \
      typedef CSTEM base_class;                                                \
      ox##CSTEM(OClass* cls) : OInstance(cls) {}                               \
      ~ox##CSTEM() {}                                                          \
    protected:

// Similar to OM_BEGIN_CLASS_DECLARATION,
#define OM_BEGIN_EXPORTED_CLASS_DECLARATION(CSTEM)                             \
    class _OM_LINK ox##CSTEM : public OInstance, public CSTEM {                \
      typedef ox##CSTEM oxT;                                                   \
    public:                                                                    \
      typedef CSTEM base_class;                                                \
      ox##CSTEM(OClass* cls) : OInstance(cls) {}                               \
      ~ox##CSTEM() {}                                                          \
      oxT& operator=(const oxT& r)                                             \
      { *static_cast<CSTEM*>(this) = r; return *this; }                        \
    protected:

#define OM_BEGIN_EXT_TYPE_DECLARATION(CSTEM,ROOT_TYPE)                         \
    class OM_O(CSTEM) : public ROOT_TYPE, public CSTEM {                       \
      typedef OM_O(CSTEM) T;                                                   \
    public:                                                                    \
      typedef CSTEM base_class;                                                \
      OM_O(CSTEM)(OClass* cls) : ROOT_TYPE(cls) {}                             \
      T& operator=(const T& r) = default;                                      \
      T& operator=(const base_class& r)                                        \
      { *static_cast<base_class*>(this) = r; return *this; }                   \
    protected:

#define OM_BEGIN_TYPE_DECLARATION(CSTEM)                                       \
    OM_BEGIN_EXT_TYPE_DECLARATION(CSTEM,OInstance)

//============================================================================//
//:::::::::::::::::::::::::::   Metaclass Macros   ::::::::::::::::::::::::::://
//============================================================================//

#define _oxC(CSTEM) ox##CSTEM##_Class
#define META(CSTEM) O##CSTEM##_Class
#define META_NAME(CSTEM) "O" #CSTEM "_Class"
#define _oxC_NAME(CSTEM) "ox" #CSTEM "_Class"

#define OM_ASSOCIATE_MCLASS_WITH(OBJCLS)                                       \
    protected:                                                                 \
      virtual oxCInheritanceData *getCInheritanceData() const override         \
      { return OBJCLS::getCInheritanceData(); }

#define OM_DECLARE_MCLASS(CSTEM,NAME)                                          \
    class _oxC(CSTEM) : public OClass {                                        \
      typedef _ox(CSTEM) OT;                                                   \
      typedef _oxC(CSTEM) M;                                                   \
    public:                                                                    \
      _oxC(CSTEM)(Session* m) : OClass(m, NAME) {}                             \
      _oxC(CSTEM)(Session* m, const std::string& typeName)                     \
        : OClass(m, typeName) {}                                               \
      _oxC(CSTEM)(Session* m, const std::string& b, const std::string& c)      \
        : OClass(m, b, c) {}                                                   \
      _oxC(CSTEM)(Session* m, const OClass* b, const std::string& c)           \
        : OClass(m, b, c) {}                                                   \
      static OClass* ctor(Session*, const std::string& typeName);              \
      virtual OClass* createClass(Session* m, const std::string& c)            \
          const override;                                                      \
      virtual OClass* createMutableClass(Session* m, const std::string& c)     \
          const override;                                                      \
      virtual size_t getAllocSize() const override                             \
      { return sizeof(OT);  }                                                  \
      virtual void createObjectCache(const ObjectUsage& ou) override           \
      { genericCreateObjectCache<OT>(ou); }                                    \
      virtual OInstance* constructCore(Session* m) override                    \
      { return genericConstructCore<OT>(m); }                                  \
      virtual void destructCore(OInstance* self, Session*) override            \
      { if (!freeCached<OT>(self)) delete static_cast<OT*>(self); }            \
      virtual ORef copyCore(ORef l, const_ORef r, Session*) const override {   \
        if (r->getRootClass() != getRootClass()) return nullptr;               \
        *static_cast<OT*>(l) = *static_cast<const OT*>(r);                     \
        return l;                                                              \
      }                                                                        \
      virtual ORef moveCore(ORef lhs, ORef rhs) const override {               \
        if (rhs->getRootClass() != getRootClass()) return nullptr;             \
        *static_cast<OT*>(lhs) = std::move(*static_cast<OT*>(rhs));            \
        return lhs;                                                            \
      }                                                                        \
      OM_ASSOCIATE_MCLASS_WITH(_ox(CSTEM))                                     \
    };

#define OM_BEGIN_META_TYPE_DECL(CSTEM)                                         \
    class META(CSTEM) : public OClass {                                        \
      typedef OM_O(CSTEM) OT;                                                  \
      typedef META(CSTEM) M;

#define OM_END_META_TYPE_DECL(CSTEM)                                           \
      OM_ASSOCIATE_MCLASS_WITH(OM_O(CSTEM))                                    \
    };

#define OM_META_TYPE_CTORS(CSTEM)                                              \
    public:                                                                    \
      META(CSTEM)(Session* m, const std::string& typeName)                     \
        : OClass(m, typeName) {}                                               \
      META(CSTEM)(Session* m, const std::string& b, const std::string& c)      \
        : OClass(m, b, c) {}                                                   \
      META(CSTEM)(Session* m, const OClass* b, const std::string& c)           \
        : OClass(m, b, c) {}

#define OM_META_TYPE_DECL(CSTEM)                                               \
    public:                                                                    \
      static OClass* ctor(Session*, const std::string& typeName);              \
      virtual OClass* createClass(Session* m, const std::string& c)            \
          const override;                                                      \
      virtual OClass* createMutableClass(Session* m, const std::string& c)     \
          const override;                                                      \
      virtual size_t getAllocSize() const override                             \
      { return sizeof(OT); }                                                   \
      virtual void createObjectCache(const ObjectUsage& ou) override           \
      { genericCreateObjectCache<OT>(ou); }                                    \
      virtual OInstance* constructCore(Session* m) override                    \
      { return genericConstructCore<OT>(m); }                                  \
      virtual void destructCore(OInstance* self, Session*) override            \
      { if (!freeCached<OT>(self)) delete static_cast<OT*>(self); }            \
      virtual ORef copyCore(ORef l, const_ORef r, Session* m) const override { \
        if (r->getRootClass() != getRootClass()) return nullptr;               \
        *static_cast<OT*>(l) = *static_cast<const OT*>(r);                     \
        return l;                                                              \
      }                                                                        \
      virtual ORef moveCore(ORef lhs, ORef rhs) const override {               \
        if (rhs->getRootClass() != getRootClass()) return nullptr;             \
        *static_cast<OT*>(lhs) = std::move(*static_cast<OT*>(rhs));            \
        return lhs;                                                            \
      }

#define OM_DECLARE_META_TYPE(CSTEM)                                            \
    OM_BEGIN_META_TYPE_DECL(CSTEM)                                             \
    OM_META_TYPE_CTORS(CSTEM)                                                  \
    OM_META_TYPE_DECL(CSTEM)                                                   \
    OM_END_META_TYPE_DECL(CSTEM)

#define OM_BEGIN_TEMPLATE_METHOD_TABLE_FOR(CLS)                                \
    template<> OCoreMethodInfo CLS::sm_SMethodTable[] = {

#define OM_BEGIN_SPECIALIZED_TEMPLATE_METHOD_TABLE_FOR(CLS)                    \
    OCoreMethodInfo CLS::sm_SMethodTable[] = {

#define OM_TEMPLATE_DEFINITION(CLS,BASECLS)                                    \
    template<typename t>                                                       \
      oxCInheritanceData CLS<t>::sm_mtab =                                     \
          { &(BASECLS::sm_mtab), CLS<t>::sm_SMethodTable };                    \
    template<typename t>                                                       \
      OCoreMethodInfo CLS<t>::sm_SMethodTable[] = {                            \
          { (CAddr)0, 0, nullptr }                                             \
      };

#define OM_BEGIN_MCLASS_DECLARATION(CSTEM,NAME)                                \
    class _oxC(CSTEM) : public OClass {                                        \
      typedef ox##CSTEM OT;                                                    \
      typedef _oxC(CSTEM) M;                                                   \
    public:                                                                    \
      _oxC(CSTEM)(Session* m) : OClass(m, NAME) {}                             \
      _oxC(CSTEM)(Session* m, const std::string& typeName)                     \
        : OClass(m, typeName) {}                                               \
      _oxC(CSTEM)(Session* m, const std::string& b, const std::string& c)      \
        : OClass(m, b, c) {}                                                   \
      _oxC(CSTEM)(Session* m, const OClass* b, const std::string& c)           \
        : OClass(m, b, c) {}                                                   \
      static OClass* ctor(Session*, const std::string& typeName);              \
      virtual OClass* createClass(Session* m, const std::string& c)            \
          const override;                                                      \
      virtual OClass* createMutableClass(Session* m, const std::string& c)     \
          const override;                                                      \
      virtual size_t getAllocSize() const override                             \
      { return sizeof(OT);  }                                                  \
      virtual void createObjectCache(const ObjectUsage& ou) override           \
      { genericCreateObjectCache<OT>(ou); }                                    \
      virtual OInstance* constructCore(Session* m) override                    \
      { return genericConstructCore<OT>(m); }                                  \
      virtual void destructCore(OInstance* self, Session*) override            \
      { if (!freeCached<OT>(self)) delete static_cast<OT*>(self); }            \
      virtual ORef copyCore(ORef l, const_ORef r, Session* m) const override { \
        if (r->getRootClass() != getRootClass()) return nullptr;               \
        *static_cast<OT*>(l) = *static_cast<const OT*>(r);                     \
        return l;                                                              \
      }                                                                        \
      virtual ORef moveCore(ORef lhs, ORef rhs) const override {               \
        if (rhs->getRootClass() != getRootClass()) return nullptr;             \
        *static_cast<OT*>(lhs) = std::move(*static_cast<OT*>(rhs));            \
        return lhs;                                                            \
      }

#define OM_END_MCLASS_DECLARATION(CSTEM)                                       \
       OM_ASSOCIATE_MCLASS_WITH(ox##CSTEM)                                     \
    };

#define OM_BEGIN_CUSTOM_MCLASS_DECLARATION(CSTEM,NAME)                         \
    class _oxC(CSTEM) : public OClass {                                        \
      typedef ox##CSTEM OT;                                                    \
      typedef _oxC(CSTEM) M;                                                   \
    public:                                                                    \
      _oxC(CSTEM)(Session*);                                                   \
      _oxC(CSTEM)(Session*, const std::string& typeName);                      \
      _oxC(CSTEM)(Session*, const std::string& b, const std::string& c);       \
      _oxC(CSTEM)(Session*, const OClass* b, const std::string& c);            \
      static OClass* ctor(Session*, const std::string& typeName);              \
      virtual OClass* createClass(Session* m, const std::string& c)            \
          const override;                                                      \
      virtual OClass* createMutableClass(Session* m, const std::string& c)     \
          const override;                                                      \
      virtual size_t getAllocSize() const override { return sizeof(OT); }      \
      virtual void createObjectCache(const ObjectUsage& ou) override           \
      { genericCreateObjectCache<OT>(ou); }                                    \
      virtual OInstance* constructCore(Session*) override;                     \
      virtual void destructCore(OInstance* self, Session*) override;           \
      virtual ORef copyCore(ORef l, const_ORef r, Session*) const override;    \
      virtual ORef moveCore(ORef l, ORef r) const override;

#define OM_DEFINE_MCLASS_CTORS(CSTEM,NAME)                                     \
      _oxC(CSTEM)::_oxC(CSTEM)(Session* m) : OClass(m, NAME) {}                \
      _oxC(CSTEM)::_oxC(CSTEM)(                                                \
          Session* m, const std::string& typeName) : OClass(m, typeName) {}    \
      _oxC(CSTEM)::_oxC(CSTEM)(Session* m,                                     \
          const std::string& b, const std::string& c) : OClass(m, b, c) {}     \
      _oxC(CSTEM)::_oxC(CSTEM)(Session* m,                                     \
        const OClass* b, const std::string& c) : OClass(m, b, c) {}

#define OM_DEFINE_MCLASS(CSTEM)                                                \
    OClass* _oxC(CSTEM)::ctor(Session* m, const std::string& typeName) {       \
      OClass* cls = new _oxC(CSTEM)(m, typeName);                              \
      if (m->exceptionPending())                                               \
        m->rethrow("ox" #CSTEM "_Class::ctor");                                \
      else                                                                     \
        cls->finalize(m);                                                      \
      return cls;                                                              \
    }                                                                          \
    OClass* _oxC(CSTEM)::createClass(Session* m,                               \
        const std::string& c) const {                                          \
      OClass* cls = new _oxC(CSTEM)(m, this->name, c);                         \
      m->rethrow("ox" #CSTEM "_Class::createClass");                           \
      return cls;                                                              \
    }                                                                          \
    OClass* _oxC(CSTEM)::createMutableClass(Session* m,                        \
        const std::string& c) const {                                          \
      OClass* cls = new _oxC(CSTEM)(m, this, c);                               \
      m->rethrow("ox" #CSTEM "_Class::createMutableClass");                    \
      return cls;                                                              \
    }

#define OM_DEFINE_META_TYPE(CSTEM)                                             \
    OClass* META(CSTEM)::ctor(Session* m, const std::string& typeName) {       \
      OClass* cls = new META(CSTEM)(m, typeName);                              \
      if (m->exceptionPending())                                               \
        m->rethrow(META_NAME(CSTEM) "::ctor");                                 \
      else                                                                     \
        cls->finalize(m);                                                      \
      return cls;                                                              \
    }                                                                          \
    OClass* META(CSTEM)::createClass(Session* m, const std::string& c) const { \
      OClass* cls = new META(CSTEM)(m, this->name, c);                         \
      m->rethrow(META_NAME(CSTEM) "::createClass");                            \
      return cls;                                                              \
    }                                                                          \
    OClass* META(CSTEM)::createMutableClass(Session* m,                        \
        const std::string& c) const {                                          \
      OClass* cls = new META(CSTEM)(m, this, c);                               \
      m->rethrow(META_NAME(CSTEM) "::createMutableClass");                     \
      return cls;                                                              \
    }

#define OM_DECLARE_CLASS_INSTALLER(cls) \
    OClass* g_install##cls##Class(Session*);

#define OM_IMPLEMENT_CLASS_INSTALLER(cls)                   \
    OClass* g_install##cls##Class(Session* m) {             \
      OClass* c = new ox##cls##_Class(m);                   \
      if (!c) return nullptr;                               \
      c->finalize(m);                                       \
      return c;                                             \
    }

#define OM_IMPLEMENT_CLASSID_INSTALLER(cls, ID)             \
    OClass* g_install##cls##Class(Session* m) {             \
      extern OClass* _OXType_class[];                       \
      OClass* c = new ox##cls##_Class(m);                   \
      if (!c) return nullptr;                               \
      c->finalize(m);                                       \
      _OXType_class[ID] = c;                                \
      return c;                                             \
    }
#define OM_IMPLEMENT_TYPEID_INSTALLER(cls, NAME, ID)        \
    OClass* g_install##cls##Class(Session* m) {             \
      extern OClass* _OXType_class[];                       \
      OClass* c = new META(cls)(m, NAME);                   \
      if (!c) return nullptr;                               \
      c->finalize(m);                                       \
      _OXType_class[ID] = c;                                \
      return c;                                             \
    }

#define _DECLARE_METHOD_TABLE        OM_DECLARE_METHOD_TABLE
#define _BEGIN_METHOD_TABLE          OM_BEGIN_METHOD_TABLE
#define _END_METHOD_TABLE            OM_END_METHOD_TABLE
#define _ASSOCIATE_MCLASS_WITH       OM_ASSOCIATE_MCLASS_WITH
#define _METHOD                      OM_METHOD
#define _BEGIN_CLASS_DECLARATION     OM_BEGIN_CLASS_DECLARATION
#define _BEGIN_EXT_TYPE_DECLARATION  OM_BEGIN_EXT_TYPE_DECLARATION
#define _BEGIN_TYPE_DECLARATION      OM_BEGIN_TYPE_DECLARATION
#define _BEGIN_NC_CLASS_DECLARATION  OM_BEGIN_NOCOPY_CLASS_DECLARATION
#define _END_CLASS_DECLARATION       OM_END_CLASS_DECLARATION
#define _END_TYPE_DECLARATION        OM_END_TYPE_DECLARATION
#define _BEGIN_EXP_CLASS_DECLARATION OM_BEGIN_EXPORTED_CLASS_DECLARATION
#define _DECLARE_MCLASS              OM_DECLARE_MCLASS
#define _DECLARE_META_TYPE           OM_DECLARE_META_TYPE
#define _BEGIN_META_TYPE_DECL(CSTEM) OM_BEGIN_META_TYPE_DECL(CSTEM)
#define _END_META_TYPE_DECL(CSTEM)   OM_END_META_TYPE_DECL(CSTEM)
#define _META_TYPE_CTORS(CSTEM)      OM_META_TYPE_CTORS(CSTEM)
#define _META_TYPE_DECL(CSTEM)       OM_META_TYPE_DECL(CSTEM)
#define _BEGIN_MCLASS_DECLARATION    OM_BEGIN_MCLASS_DECLARATION
#define _END_MCLASS_DECLARATION      OM_END_MCLASS_DECLARATION
#define _DEFINE_MCLASS               OM_DEFINE_MCLASS
#define _DEFINE_META_TYPE            OM_DEFINE_META_TYPE
#define _BEGIN_EXPORTED_METHOD_TABLE OM_BEGIN_EXPORTED_METHOD_TABLE
#define _DEFINE_MCLASS_CTORS(STEM,N) OM_DEFINE_MCLASS_CTORS(STEM,N)
#define _BEGIN_CUSTOM_MCLASS_DECLARATION(STEM,N) \
                                     OM_BEGIN_CUSTOM_MCLASS_DECLARATION(STEM,N)
#define _DECLARE_CLASS_INSTALLER(cls) \
                                     OM_DECLARE_CLASS_INSTALLER(cls)
#define _IMPLEMENT_CLASS_INSTALLER(cls) \
                                     OM_IMPLEMENT_CLASS_INSTALLER(cls)
#define _IMPLEMENT_CLASSID_INSTALLER(cls, ID) \
                                     OM_IMPLEMENT_CLASSID_INSTALLER(cls, ID)
#define _IMPLEMENT_TYPEID_INSTALLER  OM_IMPLEMENT_TYPEID_INSTALLER
#define _TEMPLATE_DEFINITION(CLS,BASECLS) \
                                     OM_TEMPLATE_DEFINITION(CLS,BASECLS)
#define _BEGIN_TEMPLATE_METHOD_TABLE_FOR(CLS) \
                                     OM_BEGIN_TEMPLATE_METHOD_TABLE_FOR(CLS)
#define _BEGIN_SPECIALIZED_TEMPLATE_METHOD_TABLE_FOR(CLS) \
    OM_BEGIN_SPECIALIZED_TEMPLATE_METHOD_TABLE_FOR(CLS)

//============================================================================//
//::::::::::::::::::::::::::::   Iterator Ties   ::::::::::::::::::::::::::::://
//============================================================================//

#define OM_DEFINE_TIE(_OIt, _DefCtorInit)                                      \
  template<typename _Op>                                                       \
    struct Tie : OTie {                                                        \
      using OContainer = typename _Op::container;                              \
      using OIter = _OIt;                                                      \
      Tie() : OTie{_DefCtorInit} {}                                            \
      Tie(OClass* itType) : OTie{itType} {}                                    \
      Tie(Tie&&) = default;                                                    \
      virtual ~Tie() {}                                                        \
      virtual void apply(RangeOperationBase* op, ORef cont,                    \
          ORef a, ORef b) override {                                           \
        auto& c = *static_cast<typename _Op::container*>(cont);                \
        if (static_cast<OIter*>(a)->isReverse()) {                             \
          auto rbegin = std::make_reverse_iterator(a->castRef<OIter>());       \
          auto rend   = std::make_reverse_iterator(b->castRef<OIter>());       \
          static_cast<_Op&>(*op)(c, rbegin, rend);                             \
        }                                                                      \
        else                                                                   \
          static_cast<_Op&>(*op)(c, a->castRef<OIter>(), b->castRef<OIter>()); \
      }                                                                        \
    };

#define _DEFINE_TIE OM_DEFINE_TIE

#define _P(t, a) static_cast<oxType<t>*>(a)
#define OM_TYPE_VAL(t,b)  (_P(t, b)->getValue())
#define _TYPE_VAL(t,b)  OM_TYPE_VAL(t,b)
#define _INT_VAL(b)     (_P(int, b)->getValue())
#define _SHORT_VAL(b)   (_P(short, b)->getValue())
#define _CHAR_VAL(b)    (_P(char, b)->getValue())
#define _DOUBLE_VAL(b)  (_P(double, b)->getValue())
#define _DBL_VAL(b)     _DOUBLE_VAL(b)
#define _FLOAT_VAL(b)   (_P(float, b)->getValue())
#define _FLT_VAL(b)     _FLOAT_VAL(b)
#define _BOOL_VAL(b)    (_P(bool, b)->getValue())
#define _POINTER_VAL(b) (_P(void*, b)->getValue())
#define _PTR_VAL(b)     (_P(void*, b)->getValue())
#define _PULONG(a)     _P(unsigned long, a)
#define _PLONG(a)      _P(long, a)
#define _PUINT(a)      _P(unsigned, a)
#define _PINT(a)       _P(int, a)
#define _PSHORT(a)     _P(short, a)
#define _PCHAR(a)      _P(char, a)
#define _PDBL(a)       _P(double, a)
#define _PFLOAT(a)     _P(float, a)
#define _PFLT(a)       _P(float, a)
#define _PBOOL(a)      _P(bool, a)
#define _PPTR(a)       _P(void*, a)

#endif /*__ODEV_H__*/
