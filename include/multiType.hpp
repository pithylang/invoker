#ifndef __MULTITYPE_HPP__
#define __MULTITYPE_HPP__

#include <type_traits>
#include "ovariable.hpp"

template<int _SIZE = sizeof(void*)>
struct MultiType {
  using ulong = unsigned long;
  using uint = unsigned int;
  using ushort = unsigned short;
  using uchar = unsigned char;
  using ptr = void*;

  enum {
    MT_UNKNOWN,
    MT_ULONG,
    MT_LONG,
    MT_UINT,
    MT_INT,
    MT_USHORT,
    MT_SHORT,
    MT_UCHAR,
    MT_CHAR,
    MT_BOOL,
    MT_DOUBLE,
    MT_FLOAT,
    MT_POINTER,
    MT_VAR,
  };

  char buf_[_SIZE];
  int type_;

  #define DEFINE_IS(_t, _id) \
    template<typename _T> \
    requires (std::is_same<_T, _t>::value) \
      bool is() const noexcept {return type_ == _id;}

  #define DEFINE_SET_TYPE(_t, _id) \
    template<typename _T> \
    requires (std::is_same<_T, _t>::value) \
      void setType() noexcept {type_ = _id;}

  DEFINE_IS(ulong, MT_ULONG)
  DEFINE_IS(long, MT_LONG)
  DEFINE_IS(uint, MT_UINT)
  DEFINE_IS(int, MT_INT)
  DEFINE_IS(ushort, MT_USHORT)
  DEFINE_IS(short, MT_SHORT)
  DEFINE_IS(uchar, MT_UCHAR)
  DEFINE_IS(char, MT_CHAR)
  DEFINE_IS(bool, MT_BOOL)
  DEFINE_IS(double, MT_DOUBLE)
  DEFINE_IS(float, MT_FLOAT)
  DEFINE_IS(ptr, MT_POINTER)
  DEFINE_IS(OVariable, MT_VAR)

  DEFINE_SET_TYPE(ulong, MT_ULONG)
  DEFINE_SET_TYPE(long, MT_LONG)
  DEFINE_SET_TYPE(uint, MT_UINT)
  DEFINE_SET_TYPE(int, MT_INT)
  DEFINE_SET_TYPE(ushort, MT_USHORT)
  DEFINE_SET_TYPE(short, MT_SHORT)
  DEFINE_SET_TYPE(uchar, MT_UCHAR)
  DEFINE_SET_TYPE(char, MT_CHAR)
  DEFINE_SET_TYPE(bool, MT_BOOL)
  DEFINE_SET_TYPE(double, MT_DOUBLE)
  DEFINE_SET_TYPE(float, MT_FLOAT)
  DEFINE_SET_TYPE(ptr, MT_POINTER)
  DEFINE_SET_TYPE(OVariable, MT_VAR)

  #undef DEFINE_SET_TYPE
  #undef DEFINE_IS

  template<typename _T>
  _T* pointer() {return reinterpret_cast<_T*>(buf_);}
  template<typename _T>
  _T& reference() {return *pointer<_T>();}
  template<typename _T>
  const _T& reference() const {return *pointer<_T>();}

  template<typename _T>
  void set(const _T& value) {setType<_T>(); reference<_T>() = value;}
  template<typename _T>
  _T& get() {return reference<_T>();}
  template<typename _T>
  const _T& get() const {return reference<_T>();}
};

typedef MultiType<> MultiTypeVar;
typedef MultiType<sizeof(OVariable)> MReqValue;

/**
 *  Virtual Machine Parameters
 */
enum MParam : int {
  M_REGISTER_RES,
  M_IS_RETURNING,
  M_CALLER_CLASS,
  M_THIS,
  M_RECEIVER,
  M_RECEIVER_VAR_NAME,
  M_METHOD_NAME,
  M_REF_RESULT,
  M_ARG_0,
  M_REGISTER_IP,
  M_REGISTER_LR,
  M_REGISTER_3
};

class VirtualMachine;
MReqValue g_getMachineValue(VirtualMachine* vm, MParam which);

#endif // __MULTITYPE_HPP__
