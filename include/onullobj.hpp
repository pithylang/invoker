#ifndef __ONULLOBJ_HPP__
#define __ONULLOBJ_HPP__

#include "object.hpp"
#include "odev.h"

struct NullObject {};

_BEGIN_CLASS_DECLARATION(NullObject)
  _METHOD (ommIsNull);
_END_CLASS_DECLARATION()

class oxNullObject_Class : public OClass {
  typedef oxNullObject OT;
  typedef oxNullObject_Class M;
public:
  oxNullObject_Class(Session *m) : OClass(m, "null object") {}
  oxNullObject_Class(Session *m, const std::string& b, const std::string& c)
    : OClass(m, b, c) {}
  oxNullObject_Class(Session *m, const OClass* b, const std::string& c)
    : OClass(m, b, c) {}
  virtual OClass* createClass(Session* m, const std::string& c) const override;
  virtual OClass* createMutableClass(Session* m, const std::string& c)
      const override;
  virtual size_t getAllocSize() const override { return sizeof(OT);  }
  virtual void createObjectCache(const ObjectUsage&) override {}
  virtual OInstance *constructCore(Session*) override { return new OT{this}; }
  virtual void destructCore(OInstance* self, Session*) override;
  virtual ORef copyCore(ORef tgt, const_ORef src, Session*) const override;
  virtual ORef moveCore(ORef tgt, ORef src) const override;
  _ASSOCIATE_MCLASS_WITH(oxNullObject)
};

_DECLARE_CLASS_INSTALLER(NullObject)

#endif // __ONULLOBJ_HPP__
