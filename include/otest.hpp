#ifndef __OTEST_HPP__
#define __OTEST_HPP__

#include <string>
#include <vector>

enum CheckResult {
  CHECK_PASSED,
  CHECKPOINT_DEFINED_BUT_IGNORED,
  CHECK_FAILED
};

struct ExpectedException {
  int line;
  std::string type;
  std::string location;
  std::string message;

  bool valid() const noexcept;
  bool matches(const ExpectedException& e) const noexcept;
};

struct Checker {
  static const size_t MAX_SIZE_LEFTOVERS;
  static const int MIN_RET_VALUE;
  std::string checkpoint;
  std::string parseExceptionToIgnore;
  std::vector<ExpectedException> expectedExceptions;
  size_t expectedLeftovers;
  int expectedRetValue;

  void init() noexcept;
  bool checkpointComplete() const noexcept
  { return !checkpoint.empty() && checkpoint.back() == '\n'; }
  bool expandCheckpoint(const std::string &text);
  void mergeCheck();
  void setCheckpoint();
  void ignoreCheckpoint() noexcept { checkpoint.clear(); }
  CheckResult performCheck(const std::string &expectedText);
  bool expectException(ExpectedException&& expectedException);
  bool expectException(int line, const std::string& type = "",
      const std::string& location = "", const std::string& message = "");
  bool isExpectedException(const ExpectedException& e) const noexcept;
  bool isExpectedException(int line, const std::string& type = "",
      const std::string& loc = "", const std::string& msg = "") const noexcept;
};

#endif // __OTEST_HPP__
