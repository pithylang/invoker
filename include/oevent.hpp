#ifndef __OEVENT_HPP__
#define __OEVENT_HPP__

#include <string>
#include <vector>
#include <list>
#include <functional>
#include <algorithm>
#include "object.hpp"
#include "ovariable.hpp"
#include "oapi.h"

// =========================================================
// oxEventTable
// =========================================================

enum { _UNRESOLVED_INIT = 0x00000001 };

/// Support for event handling
struct oxEventTableEntry
{
  int              m_id;
  int              m_lastId;
  int              m_eventType;
  const char      *m_handler;
  ORef             m_param;
  unsigned         m_flags;
  OMethodAddress  m_address;

  oxEventTableEntry (const char *handler) :
        m_id       (-1),
        m_lastId   (-1),
        m_eventType(-1),
        m_handler  (handler),
        m_param    (0),
        m_flags    (_UNRESOLVED_INIT) {}

  inline bool flags(unsigned fl) const
  { return (m_flags & fl) == fl; }
  inline void set(unsigned fl)
  { m_flags |= fl; }
  inline void cancel(unsigned fl)
  { m_flags &= ~fl; }
};

struct ResolveEntry
{
  unsigned    m_type;
  int         m_entryIndex;
  const char *m_stringId;
};

class oxEventTable
{
  enum
  {
     _UNRESOLVED_METHOD      = _UNRESOLVED_INIT,
     _UNRESOLVED_EVENT_TYPE  = 0x00000002,
     _UNRESOLVED_ID          = 0x00000004,
     _UNRESOLVED_LAST_ID     = 0x00000008,
     _UNRESOLVED_PARAM       = 0x00000010,
     _UNRESOLVED_ALL         = 0x0000001f
 };

  typedef std::vector<oxEventTableEntry> EventArray;
  typedef std::list<ResolveEntry> ResolveInfo;

protected:
  EventArray  m_entries;
  ResolveInfo m_resolveInfo;

public:
  oxEventTable() {}
  ~oxEventTable() {}

  inline const oxEventTableEntry& getEntry(int i) const
  { return m_entries[i]; }

  inline int eventTableSize() const
  { return m_entries.size(); }

  inline unsigned flags(int i) const
  { return m_entries[i].m_flags; }

  inline bool isEntryResolved(int i) const
  { return !m_entries[i].flags(_UNRESOLVED_METHOD); }

  inline bool isIdResolved(int i) const
  { return !m_entries[i].flags(_UNRESOLVED_ID); }

  inline bool isLastIdResolved(int i) const
  { return !m_entries[i].flags(_UNRESOLVED_LAST_ID); }

  inline bool isEventTypeResolved(int i) const
  { return !m_entries[i].flags(_UNRESOLVED_EVENT_TYPE); }

  inline bool isParamResolved(int i) const
  { return !m_entries[i].flags(_UNRESOLVED_PARAM); }

  inline void setEntryResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_METHOD); }

  inline void setIdResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_ID); }

  inline void setLastIdResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_LAST_ID); }

  inline void setEventTypeResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_EVENT_TYPE); }

  inline void setParamResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_PARAM); }

  inline void setMethodResolved(int i)
  { m_entries[i].cancel(_UNRESOLVED_METHOD); }

  inline int getEventType(int i) const
  { return m_entries[i].m_eventType; }

  inline int getId(int i) const
  { return m_entries[i].m_id; }

  inline int getLastId(int i) const
  { return m_entries[i].m_lastId; }

  inline ORef getParam(int i) const
  { return m_entries[i].m_param; }

  inline OMethodAddress *getEntryMethodAddress(int i)
  { return &m_entries[i].m_address; }

  ResolveInfo::Element *getResolveEntry(int, int type,
                           ResolveInfo::Element **ppPrev);
  void removeResolveInfo  (ResolveInfo::Element *p,
                           ResolveInfo::Element *prev);

  int  addEvent           (int type, const char *handler);
  int  addEvent           (const char *strEvType,
                           const char *handler);
  void setEventType       (int i, int type);
  void setEventType       (int i, const char *strEvType);
  void setId              (int i, int eventId);
  void setId              (int i, const char *strEventId);
  void setLastId          (int i, int eventId);
  void setLastId          (int i, const char *strEventId);
  void setParam           (int i, ORef param, int);
  void setParam           (int i, const char *strParam);
  void setMethodAddress   (int i, OMethodAddress *);
};

#endif // __OEVENT_HPP__
