#ifndef __OBJECT_HPP__
#define __OBJECT_HPP__

#include <vector>
#include "oclass.hpp"
#include "odev.h"
#include "otypes.h"
#include "ovariable.hpp"

class _OM_LINK OInstance {
  friend class OClass;

  OClass* class_;
  std::vector<OVar> props_;

public:

  OInstance(OClass* objectType = nullptr) : class_(objectType) {}
  // ~OInstance() {;}

  inline void deleteObject(Session* m) { class_->deleteObject(this, m); }

  inline ORet copyObject(const_ORef rhs, Session *m)
  { return class_->copyObject(this, rhs, m); }

  inline int getTypeId() const { return class_->getTypeId(); }
  inline int getClassId() const { return class_->getClassId(); }
  inline OClass* getClass() const noexcept { return class_; }
  inline OClass* getType() const noexcept { return class_; }
  inline void setClass(OClass *thisType) noexcept { class_ = thisType; }

  inline const OClass* getRootClass() const { return class_->getRootClass(); }
  inline const OClass* getRootType() const { return class_->getRootType(); }
  inline int getRootClassId() const { return getRootClass()->getClassId(); }
  inline int getRootTypeId() const { return class_->getRootTypeId(); }

  std::string getClassName() const noexcept
  { return class_ == nullptr ? "unknown" : class_->name; }
  std::string className() const noexcept { return class_->name; }

  bool typeOf(const OClass* c) const noexcept { return class_->typeOf(c); }
  bool isTypeProxy() const noexcept {
    const auto typeId = class_->getTypeId();
    return typeId <= CLSID_PTR_PROXY && typeId >= CLSID_ULONG_PROXY;
  }

  template<typename T>
    auto castPtr() noexcept -> typename T::base_class*
    { return static_cast<typename T::base_class*>(static_cast<T*>(this)); }
  template<typename T>
    auto castRef() noexcept -> typename T::base_class&
    { return *static_cast<typename T::base_class*>(static_cast<T*>(this)); }
  template<typename T>
    auto castConstRef() const noexcept -> typename T::base_class const&
    { return const_cast<OInstance*>(this)->castRef<T>(); }

  const OVar* propArray() const { return props_.data(); }
  OVar* propArray() { return props_.data(); }
  const std::vector<OVar>& props() const { return props_; }
  std::vector<OVar>& props() { return props_; }

  bool validMemberOffset(int ofs) const noexcept
  { return ofs >= 0 && ofs < props_.size(); }
  const OVar& property(int ofs) const { return props_[ofs]; }
  OVar& property(int ofs) { return props_[ofs]; }

  OVar& property(const std::string& name) {
    static OVar nullVar;
    const int ofs = class_->getPropertyOffset(name);
    return ofs == -1 ? nullVar : property(ofs);
  }

  OVar* propertyPtr(int ofs) { return props_.data() + ofs; }
  OVar* propertyPtr(const std::string& name) {
    const int ofs = class_->getPropertyOffset(name);
    return ofs == -1 ? nullptr : propertyPtr(ofs);
  }

  /**
   *  @brief Swap content
   *  @details It works like OClass::moveObject(ORef,ORef,Session*)
   *  with the difference that it bypasses all requirement
   *  checks. If you are sure that the type requirements are
   *  met, go with this method.
   */
  bool swapContent(ORef r) noexcept;

  inline const char *getNewCoreMethodName(const CoreMethodAddress& f) const
  { return class_ ? class_->getNewCoreMethodName(f) : "?"; }

  inline ORet applyMethod(const std::string& name, int argc, OArg argv[], Session* m)
  { OVar rec{this}; return class_->applyMethod(name, &rec, argc, argv, m); }
  inline ORet applyMethodUsage(const std::string& name, ushort usage,
                               int argc, OArg a[], Session* m)
  { OVar rec{this, usage}; return class_->applyMethod(name, &rec, argc, a, m); }
  inline ORet applyMethodConst(const std::string& name,
                               int argc, OArg argv[], Session* m)
  { return applyMethodUsage(name, USG_CONST, argc, argv, m); }
  inline ORet applyMethod(const std::string& name,
                          std::vector<OArg>& args, Session* m)
  { return applyMethod(name, args.size(), args.data(), m); }
  inline static ORet s_applyCoreMethod(const CAddr& f,
      OVar& rec, int argc, OArg argv[], Session* m) noexcept
  { return (rec.objectRef->*f)(&rec, argc, argv, m); }

  //::::::::::::::::::::::::   diagnostic messages   ::::::::::::::::::::::::://

  /**
   *  @brief Check type of object and build error message
   *
   *  @details It checks if `this` object has type `type`, and if
   *  it hasn't it builds an error message suitable for use as the
   *  message argument of the method
   *  @sa Session::throwBadArg(const std::string&,
   *      const std::string&, const std::string&, int)
   *  @param `types` `const std::vector<const OClass*>&` The types
   *         against which the check is done
   *  @param `which` `int` @sa typeCheckMessage(bool test,
   *         const std::string&, const std::string&, int, int)
   *  @param `argn` `int` @sa typeCheckMessage(bool test,
   *         const std::string&, const std::string&, int, int)
   *  @return The formatted error message or an empty string if the
   *  check was successful.
   */
  std::string typeCheckMessage(const std::vector<const OClass*>& types,
                               int which = 0, int argn = 0) const;
  /**
   *  @brief Check type of object and build error message
   *
   *  @details It works like @sa typeCheckMessage(
   *    const std::vector<const OClass*>&, int, int)
   *  but with one type.
   */
  std::string typeCheckMessage(const OClass* type,
                               int which = 0, int argn = 0) const;
  /**
   *  @brief Check type and build error message
   *  @details @sa typeCheckMessage(const std::vector<const OClass*>&,
   *  int which = 0, int argn = 0)
   */
  std::string typeCheckMessage(Session* m, const std::vector<int>& typeIds,
                               int which = 0, int argn = 0) const;
  /**
   *  @brief Check type and build error message
   *  @details @sa typeCheckMessage(const std::vector<const OClass*>&,
   *  int which = 0, int argn = 0)
   */
  std::string typeCheckMessage(Session* m, int typeId,
                               int which = 0, int argn = 0) const
  { return typeCheckMessage(m, std::vector{ typeId }, which, argn); }
  /**
   *  @brief Build type-check error message
   *
   *  @details It builds an error message suitable to be used as the
   *  message argument of the method
   *  @sa Session::throwBadArg(const std::string&, const std::string&,
   *                           const std::string&, int)
   *  @param `test` `bool` The test to be considered
   *  @param `suppliedTypeName` `string` The supplied type name type
   *  @param `expectedTypeName` `string` The expected type name type
   *  @param `which` `int` According to the value of `which`, the
   *         checked object is:
   *         @li 0: a variable
   *         @li a positive integer: an argument; `which` is the
   *             argument's serial number starting from 1
   *         @li a negative integer: a vector element; `-which` is
   *             the element's index starting from 1.
   *  @param `argn` `int` Valid when `which` is negative and ignored
   *         otherwise. It is the serial number of the vector-type
   *         argument starting from 1.
   *  @return The formatted error message or an empty string if the
   *  test was successful.
   */
  static std::string typeCheckMessage(bool test,
                                      const std::string& suppliedTypeName,
                                      const std::string& expectedTypeName,
                                      int which = 0, int n = 0);
  /**
   *  @brief Build size-check error message
   *
   *  @details It builds an error message suitable to be used as the
   *  message argument of the method
   *  @sa Session::throwBadArgc(const std::string&, const std::string&,
   *      const std::string&, const std::string& = "", int = -1)
   *  or
   *  @sa throwBadUsage(const string&, const string&, const string&,
   *      const string&, int)
   *  @param `suppliedSize` `int` The supplied size
   *  @param `expectedSizes` `const std::vector<int>&` The expected
   *         sizes. We can handle ranges with the following special
   *         values for `expectedSizes`:
   *         @li {a, -b}, -b being a negative integer: the expected
   *         sizes are in the closed interval [a, b].
   *         @li {-a}, -a being a negative integer: the expected
   *         sizes are in the semi-infinite interval [a, infinity].
   *  @param `which` `int` Specifies the object on which the size
   *         will be checked.
   *         @li 0: the checked size is the number of arguments if
   *             `argn` is `0`; otherwise, it is a variable.
   *         @li a positive integer: the checked object is an
   *             argument. In this case, `which` is the argument's
   *             serial number starting from 1. The parameter `argn`
   *             is ignored.
   *         @li a negative integer: the checked object is an
   *             argument's vector element; `-which` is the element's
   *             index starting from 1, and `argn` is the argument's
   *             index starting from 1.
   *         This parameter is used in the formatting of the error
   *         message.
   *  @param `argn` `int` Valid when `which` is negative or `0`.
   *         When `which` is a negative integer, it is the argument
   *         index starting from 1. When `which` is `0`, the checked
   *         object is the argument list or a variable according to
   *         whether it is a zero or non-zero number. This parameter
   *         is used in the formatting of the error message.
   *  @return The formatted error message or an empty string if the
   *  test was successful.
   */
  static std::string sizeCheckMessage(int suppliedSize,
                                      const std::vector<int>& expectedSizes,
                                      int which = 0, int argn = 0);
  /**
   *  @brief Check size and build error message
   *  @details @sa sizeCheckMessage(int suppliedSize,
   *    const std::vector<int>& expectedSizes,int which = 0, int n = 0)
   */
  static std::string sizeCheckMessage(int suppliedSize, int expectedSize,
                                      int which = 0, int n = 0) {
    return sizeCheckMessage(suppliedSize, std::vector<int>{expectedSize}, which, n);
  }

  //::::::::::::::::::::::::::::::   results   ::::::::::::::::::::::::::::::://

  inline ORet disposableResult() noexcept {return OVar::disposableResult(this);}
  inline ORet movableResult() noexcept {return OVariable::movableResult(this);}
  inline ORet makeResult() noexcept {return OVariable{this};}
  inline ORet makeConstResult(bool isConst) noexcept
  { return OVariable{this, isConst ? USG_CONST : USG_NONE}; }
  inline static ORet undefinedResult() noexcept {return OVar::undefinedResult();}

  //:::::::::::::::::::::::::::   object methods   ::::::::::::::::::::::::::://

//   ORet defaultCopy         (int, OArg*, Session*);
  _METHOD (inheritedCopy      );
  _METHOD (ommGetProp         );
  _METHOD (ommType            );
  _METHOD (ommTypeName        );
  _METHOD (ommTypeOf          );
  _METHOD (ommSameType        );
  _METHOD (ommIsNull          );
  _METHOD (ommSame            );
  _METHOD (ommGetClassId      );
  _METHOD (ommGetRootClassId  );
  _METHOD (ommGetClassObject  );
  _METHOD (ommGetObjectRef    );
  /**
   *  Return an undisposable version of `this` object
   *
   *  It is the implementation of the script method `nomove`.
   *  @par This method simply returns a new @c undisposable
   *  @c temporary variable of `this` object. The original
   *  variable remains disposable.
   */
  _METHOD (ommNoMove          );
  /**
   *  Render object undisposable
   *
   *  It is the implementation of the script method `noauto`.
   *  @par This method renders `this` object @c undisposable
   *  and returns a @c temporary variable of it.
   */
  _METHOD (ommNoAuto          );
  /**
   *  Return a constant (unmutable) version of `this` object
   *
   *  It is the implementation of the script method `const`.
   *  @par This method simply returns a new @c unmutable (const)
   *  @c temporary variable of `this` object.
   */
  _METHOD (ommConst           );
  /**
   *  @brief Return a movable version of the receiver
   *
   *  It is the implementation of the script method `move`.
   *
   *  @par If `this` object is a constant (or a literal),
   *  it throws an exception. If it is the `null` object,
   *  it returns it as a non-disposable variable without
   *  modification. On all other occasions, it returns a
   *  @em movable version of `this`. The original variable
   *  remains unchanged by this operation. Yet the contained
   *  object may subsequently be modified if the variable
   *  returned by `move` is passed to the `move:` method.
   *
   *  @par The `move` method is safe and should be the first
   *  choice over `remit`, whenever possible.
   *
   *  @sa OInstance::ommMove(int, OArg*, Session*)
   *  @sa OInstance::ommPassLiteral(int, OArg*, Session*)
   *
   *  @par In contrast to `remit`, `move` applies directly
   *  to the object of the receiver variable, thus affecting
   *  any variables it may come from. For example, in the
   *  code snippet
   *  @code
   *  "int"a{1} => println => move;
   *  @endcode
   *  `move` applies to @em both the temporary variable
   *  returned by the method `println` @and the originally
   *  constructed integer. That code has no effect; however,
   *  if the `move`d object is passed to the `move:` method,
   *  both varables will be affected:
   *  @code
   *  obj methodHandlingMovableArgs: ("int"a{1} => println => move);
   *  @endcode
   *
   *  @par Usually, methods handling movable argumets swap
   *  the supplied argument with another no longer needed
   *  object of the same type. When the execution of the
   *  statement has finished, both the disposable temporary
   *  variable `"int"a{1}` @em and the non-disposable
   *  temporary variable returned by `println` will contain
   *  the needless object that `methodHandlingMovableArgs:`
   *  swapped for theirs. Ultimately, the needless object
   *  gets destroyed, making for a valuable optimization.
   *
   *  @par `move` works much like the `C++ std::move`
   *  function. It does not move anything; it just opens up
   *  the possibility of moving something for methods
   *  making that optimization.
   */
  _METHOD (ommMove            );
  /**
   *  Render temporary variable disposable
   *
   *  It is the implementation of the script method `remit`.
   *
   *  @par If `this` object is a constant (or a literal) it
   *  throws an exception. If it is the `null` object it returns
   *  it as a non-disposable variable without modification.
   *  In all other occasions, it returns a disposable version
   *  of `this`, rendering the original variable unusable
   *  (`undefined`).
   *
   *  @par Warning! This is an unsafe method. Use with caution.
   *  Whenever possible, use the safer `move` method instead.
   *
   *  @sa OInstance::ommMove(int, OArg*, Session*)
   *  @sa OInstance::ommPassLiteral(int, OArg*, Session*)
   *
   *  @par Note that the method applies only to the direct
   *  receiver variable leaving untouched any variables it
   *  may indirectly come from. For example, in the code
   *  snippet
   *  @code
   *  "int"n{1} => println => remit; // noauto instance, OK
   *  @endcode
   *  `remit` applies to the temporary variable returned by
   *  the method `println` rather than to the originally
   *  constructed integer. That code works, but
   *  @code
   *  i = 5;
   *  i => println => remit;
   *  @endcode
   *  is a bug because it produces a disposable temporary
   *  variable of the object `i`, which the executor will
   *  destroy as soon as it has finished with the 2nd
   *  statement leaving `i` with a non-existing object
   *  (@em dangling reference). As a result, an exception
   *  will be thrown when the invoker will attempt to
   *  destroy the local variable `i`. Use the following
   *  code instead:
   *  @code
   *  i println; i remit;
   *  @endcode
   *
   *  @par Also the statement
   *  @code
   *  "int"a{1} => println => remit; // auto (disposable)
   *                                 // instance
   *  @endcode
   *  produces a bug because it creates two disposable
   *  versions of the same object.
   *
   *  @par Examples
   *  @code
   *  1 => println => remit; // error, it destroys the object of
   *                         // constant "1" leaving it with a
   *                         // dangling reference
   *  i = 1;
   *  i => remit;           // destroys i; after that, i is undefined
   *  i => dup => remit;    // OK, it destroys the duplicate object
   *  i => dup => println   // error, the duplicate object
   *           => remit;    // is destroyed twice
   *  i + 1 => println      // same error, the temporary object i+1
   *        => remit;       // is destroyed twice
   *  j = (i + 1 => remit); // OK but does nothing more than j = i+1;
   *                        // the VM itself would optimize it
   *  j = (i + 1 => println // error, the temporary object i+1
   *             => remit); // is destroyed twice
   *  @endcode
   */
  _METHOD (ommRemit           );
  /**
   *  Pass literal value as disposable argument
   *
   *  It is the implementation of the script method `pass`.
   *
   *  @par It returns a disposable variable of a literal-value
   *  object shifting it to the uninitialized state by rendering
   *  it undefined. Nothing is done if the receiver is not a
   *  literal value.
   *
   *  @par Note that the method applies only to the direct
   *  receiver variable leaving untouched any variables it
   *  may indirectly come from. Here are some examples:
   *  @code
   *  "hello world" pass;          // reset literal to undefined
   *  s = ("hello world" pass);    // move literal string to
   *                               // variable s and reset the
   *                               // original literal
   *  s = "hello world";           // VM optimizes it to a pass
   *  z = (s pass);                // copy s to z leaving s
   *                               // intact like in
   *  z = s;
   *  z = (s remit);               // (unsafe) move string s to
   *                               // z and render s undefined
   *  myObj myMeth: ("hello" pass) // bad usage! use instead
   *  myObj myMeth: "hello"        // and in the code of myMeth
   *                               // use pass when needed, e.g.
   *  . method: "myMeth: x" def: {
   *   y = (x size) > 8 ? (x pass) : x;
   *   y println;
   *  };
   *  $result: ("hello" pass);     // error!?
   *  @endcode
   */
  _METHOD (ommPassLiteral     );
  _METHOD (ommApply           );
  _METHOD (ommParentInvoke    );
  _METHOD (ommInitTemplateProp);
  _METHOD (ommDelete          );
  _METHOD (ommDuplicate       );
  _METHOD (ommMoveObject      );
  _METHOD (ommExtend          );
  _METHOD (ommExtendBy        );
  _METHOD (ommCreateExtension );
  _METHOD (ommConditional     );

  _DECLARE_METHOD_TABLE();
};

#endif // __OBJECT_HPP__
