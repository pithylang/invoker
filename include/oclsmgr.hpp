#ifndef __OMCLSMGR_HPP__
#define __OMCLSMGR_HPP__

#include <vector>
#include <unordered_map>
#include <functional>
#include <memory>
#include <limits>
#include "idgen.hpp"
#include "oreqs.hpp"
#include "object.hpp"
#include "onullobj.hpp"

//============================================================================//
//::::::::::::::::::::::::::::::   OClassMgr   ::::::::::::::::::::::::::::::://
//============================================================================//

#ifndef MAX_NUM_CLASSES
# define MAX_NUM_CLASSES std::numeric_limits<int>::max()/2
#endif

class _OM_LINK OClassMgr {
  using type_bool_assoc = std::pair<OClass*, bool>;
  /** Table of registered classes */
  std::unordered_map<std::string, type_bool_assoc> classTable_;
  /** List of classes */
  std::vector<OClass*> classList_;
  /** Table of mutable classes */
  std::unordered_map<int, OClass*> mutableClasses_;
  /** Class identifiers */
  int currentClassId_;
  /** Mutable class identifiers */
  SimpleIdGenerator<MAX_NUM_CLASSES> mutableTypeIdGenerator_;
  /** Global null object instance */
  const oxNullObject nullObject_;

public:
  /** Requirement manager */
  ReqMgr reqMgr;

  OClassMgr() : classList_{nullptr}, currentClassId_{1}, nullObject_{nullptr} {}

  /** Grant id to new class */
  inline int issueClassId() noexcept {return currentClassId_++;}
  /**
   *  \brief Issue mutable type identifier
   *  \details Grant identifier to new mutable class and associate
   *  it with the class.
   */
  int issueMutableTypeId(OClass* type, Session* m) noexcept;
  /**
   *  \brief Release mutable type identifier
   *  \details Release the supplied mutable type identifier and
   *  dissociate it from its class pointer.
   */
  void releaseMutableTypeId(int id) noexcept;
  /** Get number of released mutable type identifiers */
  inline std::size_t getReleasedTypeIds() const noexcept
  { return mutableTypeIdGenerator_.released(); }
  /**
   *  \brief Initialize runtime system
   *  \details Initializes base selector names and registers
   *  the runtime class \c ORTMgr_Class.
   *  \return A pointer to \c ORTMgr_Class
   */
  OClass* initRuntimeSystem();
  /** Initialize base classes */
  bool initClasses(Session*);
  /** Terminate OClassMgr session */
  void term();
  /** Check if OClassMgr is valid */
  inline bool valid() const noexcept {
    return mutableTypeIdGenerator_.valid() &&
        classList_.size() == static_cast<std::size_t>(currentClassId_);
  }

  bool registerClass(OClass*);
  void deregClass(OClass*); // TODO

  bool isClass(OClass* type) const;
  OClass* getClass(const std::string& className) const;
  OClass* getClass(int classId) const;
  inline const std::vector<OClass*>& getClasses() const {return classList_;}

  bool aliasClass(OClass* type, const std::string& newTypeName);
  bool isAlias(const std::string& typeName) const;
  bool isAlias(OClass* type) const;

  inline ORef nullObject() const noexcept
  { return const_cast<oxNullObject*>(&nullObject_); }
  inline bool isStdNullObject(ORef o) const noexcept
  { return o == nullObject(); }

private:
  OClass* getMutableClass(int typeId) const;
};

#endif // __OMCLSMGR_HPP__
