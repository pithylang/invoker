#ifndef __FSWITCH_H__
#define __FSWITCH_H__

#if defined(NO_FAST_SWITCH)
# define SWITCH(w,i) switch(i) {
# define SWITCH_CHK(w,i) switch(i) {
# define END_SWITCH(w) }
# define CASE(w,p) case p
# define DEFAULT(w) default
# define BREAK(w) break
# define BRANCH(w,p) goto label_##w##_##p
# define CONNECTION_POINT(w,p) label_##w##_##p: ;
# define SWITCH_TABLE(w,f)
# define SWITCH_TABLE_CHK(w,f,l)
# define END_SWITCH_TABLE(w)
# define DEFINE_CASE(w,i)
#else
# define SWITCH(w,i)                                 \
    goto *array_label_##w[(i)-_label_first_##w];
# define SWITCH_CHK(w,i)                             \
    if ((i) > _label_first_##w + _label_delta_##w || \
        (i) < _label_first_##w)                      \
      goto label_default_##w;                        \
    else                                             \
      goto *array_label_##w[(i)-_label_first_##w];
# define END_SWITCH(w) label_end_##w: ;
# define CASE(w,p) label_##w##_##p
# define DEFAULT(w) label_default_##w
# define BREAK(w) goto label_end_##w
# define BRANCH(w,p) goto label_##w##_##p
# define CONNECTION_POINT(w,p)
# define SWITCH_TABLE(w,f)                              \
    static constexpr unsigned _label_first_##w = f;     \
    static void *array_label_##w []={
# define SWITCH_TABLE_CHK(w,f,l)                        \
    static constexpr unsigned _label_first_##w = f;     \
    static constexpr unsigned _label_delta_##w = l - f; \
    static void *array_label_##w []={
# define END_SWITCH_TABLE(w) \
      && label_end_##w \
    };
# define DEFINE_CASE(w,i) && label_##w##_##i ,
#endif

/* the definitions of VU_* are in itypes.hpp */
#define SWITCH_TABLE_VAR_TYPES(TABLE_ID) \
  SWITCH_TABLE(TABLE_ID, 0)              \
    DEFINE_CASE(TABLE_ID, VU_VAR)        \
    DEFINE_CASE(TABLE_ID, VU_STATIC)     \
    DEFINE_CASE(TABLE_ID, VU_ARG_REF)    \
    DEFINE_CASE(TABLE_ID, VU_STATIC_ARG_REF)  \
    DEFINE_CASE(TABLE_ID, VU_PROP_REF)   \
    DEFINE_CASE(TABLE_ID, VU_STATIC_PROP_REF) \
    DEFINE_CASE(TABLE_ID, _6)            \
    DEFINE_CASE(TABLE_ID, _7)            \
    DEFINE_CASE(TABLE_ID, VU_SYM_REF)    \
    DEFINE_CASE(TABLE_ID, VU_STATIC_SYM_REF)  \
    DEFINE_CASE(TABLE_ID, _10)           \
    DEFINE_CASE(TABLE_ID, _11)           \
    DEFINE_CASE(TABLE_ID, _12)           \
    DEFINE_CASE(TABLE_ID, _13)           \
    DEFINE_CASE(TABLE_ID, _14)           \
    DEFINE_CASE(TABLE_ID, _15)           \
  END_SWITCH_TABLE(TABLE_ID)

/*
  Example:

  enum {none, exec, push, other};

  SWITCH_TABLE_CHK(1,exec,push)
    DEFINE_CASE(1,exec)
    DEFINE_CASE(1,push)
  END_SWITCH_TABLE(1)

  SWITCH_CHK(1, i)
    CASE(1, exec):
      do_exec();
      BREAK(1);
    CASE(1, push):
      do_push();
      BREAK(1);
    DEFAULT(1):
      do_default();
  END_SWITCH(1)

  SWITCH_TABLE(2,exec)
    DEFINE_CASE(2,exec)
    DEFINE_CASE(2,push)
    DEFINE_CASE(2,other)
  END_SWITCH_TABLE(2)

  SWITCH(2, i)
    CASE(2, exec):
      do_exec();
      BREAK(2);
    CASE(2, push):
      do_push();
      BREAK(2);
    CASE(2, other):
      do_other();
      BREAK(2);
    DEFAULT(2):
      do_default();
  END_SWITCH(2)
*/
#endif /*__FSWITCH_H__*/
