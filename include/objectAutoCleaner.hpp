#ifndef __OBJECTAUTOCLEANER_HPP__
#define __OBJECTAUTOCLEANER_HPP__

#include "odefs.h"
#include "ortmgr.hpp"
#include "ovariable.hpp"

struct ObjectAutoCleaner {
  std::vector<ORef> objects;
  Session* m;
  ObjectAutoCleaner() = delete;
  ObjectAutoCleaner(const ObjectAutoCleaner&) = delete;
  ObjectAutoCleaner(ObjectAutoCleaner&&);
  ObjectAutoCleaner(Session* m) : m{m} {}
  ObjectAutoCleaner(Session* m, ORef o) : m{m} { add(o); }
  ObjectAutoCleaner(Session* m, const OVariable& disposableVar);
  ObjectAutoCleaner(Session* m, OVariable&& disposableVar);
  ObjectAutoCleaner(Session* m, const std::vector<ORef>&);
  ObjectAutoCleaner(Session* m, std::vector<ORef>&&);
  ~ObjectAutoCleaner() { cleanup(); }
  ObjectAutoCleaner& operator=(const ObjectAutoCleaner&) = delete;
  ObjectAutoCleaner& operator=(ObjectAutoCleaner&&);
  bool empty() const noexcept;
  void add(ORef o);
  void addDisposable(const OVariable& var);
  void addDisposable(OVariable&& var);
  void cancel() noexcept;
  void cancel(const_ORef o) noexcept;
  void cancelOne(size_t which = 0) { objects[which] = nullptr; }
  void now();
  void cleanup();
  void pack();
};

#endif // __OBJECTAUTOCLEANER_HPP__
