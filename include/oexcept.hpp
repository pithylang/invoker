#ifndef __OEXCEPT_HPP__
#define __OEXCEPT_HPP__

#include <string>
#include <vector>
#include <forward_list>
#include "object.hpp"
#include "oapi.h"

//============================================================================//
//:::::::::::::::::::::::::::::  ExceptionFrame  ::::::::::::::::::::::::::::://
//============================================================================//

struct ExceptionFrame {
  std::string method;
  std::string file;
  int line;

  /** Constructor without position info */
  ExceptionFrame(const std::string& methodName = "") :
    method     ( methodName ),
    line       (     -1     ) {}
  /** Constructor with position info */
  ExceptionFrame(const std::string& method, const std::string& file, int l = -1) :
    method     (   method   ),
    file       (    file    ),
    line       (      l     ) {}
  /** Copy constructor */
  ExceptionFrame(const ExceptionFrame& r) :
    method     (  r.method  ),
    file       (   r.file   ),
    line       (   r.line   ) {}

  ~ExceptionFrame() = default;

  ExceptionFrame& operator=(const ExceptionFrame&);
  inline bool operator==(const ExceptionFrame& r) const
  { return method == r.method && file == r.file && line == r.line; }
  inline bool operator!=(const ExceptionFrame& r) const {return !operator==(r);}
  inline bool isNull() const noexcept
  { return method.empty() && file.empty() && line == -1; }

  std::string getString() const;
};

//============================================================================//
//:::::::::::::::::::::::::::::  ExceptionBase  :::::::::::::::::::::::::::::://
//============================================================================//

struct _OM_LINK ExceptionBase : public std::forward_list<ExceptionFrame> {
  std::string what;
  ExceptionBase() = default;
  ExceptionBase(const std::string& what) : what(what) {}

  inline void addFrame(const std::string& method,
                       const std::string& file = "", int line = -1)
  { emplace_front(method, file, line); }

  /** Build call stack string */
  std::string callStack() const;
  /** Build default message */
  virtual std::string message() const {return "";}
  /** Get the exception's original call frame */
  const ExceptionFrame& getMainFrame() const;
  /** Get the exception's call stack size */
  std::forward_list<ExceptionFrame>::size_type size() const noexcept;
};

//:::::::::::::::::::::::::::  ExceptionBadClass  :::::::::::::::::::::::::::://

/**
 *  Exception caused by a misbehaving class
 */
struct ExceptionBadClass : public ExceptionBase {
  std::string description;
  std::string context;
  std::string reason;

  ExceptionBadClass() : ExceptionBase("bad class") {}
  ExceptionBadClass(const std::string& d,
                    const std::string& c = "",
                    const std::string& r = "") :
      ExceptionBase("bad class"),
      description  (d),
      context      (c),
      reason       (r) {}

  /** Build message for exception \c bad \c class */
  virtual std::string message() const override;
};

//::::::::::::::::::::::::::::  ExceptionBadArg  ::::::::::::::::::::::::::::://

struct ExceptionBadArg : ExceptionBase {
  std::string info;
  ExceptionBadArg() : ExceptionBase("bad argument") {}
  ExceptionBadArg(const std::string& msg) :
      ExceptionBase("bad argument"), info(msg) {}
  /** Build message for exception \c bad \c arg */
  virtual std::string message() const override;
};

//::::::::::::::::::::::::::::  ExceptionBadArgc  :::::::::::::::::::::::::::://

struct ExceptionBadArgc : ExceptionBase {
  std::string scriptMethodName;
  int expectedArgc{-1};
  std::string info;
  ExceptionBadArgc(int n = -1) : ExceptionBase("bad argc"), expectedArgc(n) {}
  /** Build message for exception \c bad \c argc */
  virtual std::string message() const override;
};

//::::::::::::::::::::::::::  ExceptionBadCoreCopy  :::::::::::::::::::::::::://

struct ExceptionBadCoreCopy : ExceptionBase {
  std::string info;
  ExceptionBadCoreCopy() : ExceptionBase("bad core copy") {}
  ExceptionBadCoreCopy(const std::string& msg) :
      ExceptionBase("bad core copy"), info(msg) {}
  /** Build message for exception \c bad \c core \c copy */
  virtual std::string message() const override;
};

//:::::::::::::::::  ExceptionBadUsage -- bad method usage  :::::::::::::::::://

struct ExceptionBadUsage : ExceptionBase {
  std::string scriptMethodName;
  std::string info;

  ExceptionBadUsage() : ExceptionBase("bad usage") {}
  ExceptionBadUsage(const std::string& msg, const std::string& methodName = ""):
      ExceptionBase("bad usage"), scriptMethodName(methodName), info(msg) {}
  /** Build message for exception \c bad \c usage */
  virtual std::string message() const override;
};

//::::::::::::::::::::::::::::::  ExceptionRTE  :::::::::::::::::::::::::::::://

struct ExceptionRTE : ExceptionBase {
  std::string info;
  ExceptionRTE() : ExceptionBase("runtime error") {}
  ExceptionRTE(const std::string& msg) :
      ExceptionBase("runtime error"), info(msg) {}
  /** Build message for exception \c runtime \c error */
  virtual std::string message() const override;
};

//============================================================================//
//:::::::::::::::::  OX encapsulation of exception package  :::::::::::::::::://
//============================================================================//

_BEGIN_EXP_CLASS_DECLARATION(ExceptionBase)
  _METHOD(ommGetMessage);
  _METHOD(ommGetLine);
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionBadClass)
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionBadArg)
OM_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionBadArgc)
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionBadCoreCopy)
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionBadUsage)
_END_CLASS_DECLARATION()

_BEGIN_CLASS_DECLARATION(ExceptionRTE)
_END_CLASS_DECLARATION()

_BEGIN_MCLASS_DECLARATION(ExceptionBase, "exception")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBase)>(); }
_END_MCLASS_DECLARATION(ExceptionBase)

_BEGIN_MCLASS_DECLARATION(ExceptionBadClass, "bad class")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBadClass)>(); }
_END_MCLASS_DECLARATION(ExceptionBadClass)

_BEGIN_MCLASS_DECLARATION(ExceptionBadArg, "bad arg")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBadArg)>(); }
_END_MCLASS_DECLARATION(ExceptionBadArg)

_BEGIN_MCLASS_DECLARATION(ExceptionBadArgc, "bad argc")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBadArgc)>(); }
_END_MCLASS_DECLARATION(ExceptionBadArgc)

_BEGIN_MCLASS_DECLARATION(ExceptionBadCoreCopy, "bad core copy")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBadCoreCopy)>(); }
_END_MCLASS_DECLARATION(ExceptionBadCoreCopy)

_BEGIN_MCLASS_DECLARATION(ExceptionBadUsage, "bad usage")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionBadUsage)>(); }
_END_MCLASS_DECLARATION(ExceptionBadUsage)

_BEGIN_MCLASS_DECLARATION(ExceptionRTE, "runtime error")
  virtual ExceptionBase* castException(ORef o) const noexcept override
  { return o->castPtr<_ox(ExceptionRTE)>(); }
_END_MCLASS_DECLARATION(ExceptionRTE)

#endif // __OEXCEPT_HPP__
