#ifndef __REQS_HPP__
#define __REQS_HPP__

#include <string>
#include <vector>
#include <memory>

class Session;

//============================================================================//
//:::::::::::::::::::::::::::::   Requirement   :::::::::::::::::::::::::::::://
//============================================================================//

/**
 *  @class Requirement
 *  @brief @c Requirement models Object Model Class requirements
 *  @details @c Requirement represents a set of requirements
 *  placed on a class. It is a fast and efficient way to check
 *  if a class supports an interface.
 */
class Requirement {
public:
  enum RequiredItemType {
    IMPORT, METHOD, PROP,
  };

private:
  /** Imported requirements */
  std::vector<Requirement*> imports_;
  /**
   *  @brief Method requirements
   *  @details A method is required by a class when the class
   *  is required to respond to this method.
   */
  std::vector<std::string> methods_;
  /**
   *  @brief Property requirements
   *  @details A property is required by a class when the class
   *  is required to have a property with this name.
   */
  std::vector<std::string> props_;
  /**
   *  @brief Inherited requirements
   *  @details A requirement inherits all items from its parent.
   */
  Requirement* parent_;

public:
  /**
   *  @brief Identifier of this set of requirements
   *  @details Requirement identifiers are unique.
   */
  std::string name;

  Requirement(const Requirement&) = default;
  Requirement(Requirement&&) = default;
  /**
   *  @brief Constructs empty set of requirements
   *  @details A pure interface requirement is an empty set of
   *  requirements with a `null` parent.
   */
  Requirement(const std::string& name, Requirement* parent) :
      parent_{parent}, name{name} {}
  /** Constructor from initializer list */
  Requirement(Session* m, const std::string& name, Requirement* parent,
               std::initializer_list<std::string> imports,
               std::initializer_list<std::string> methods,
               std::initializer_list<std::string> props = {}) :
      Requirement(m, name, parent,
          std::vector<std::string>{imports.begin(), imports.end()},
          std::vector<std::string>{methods.begin(), methods.end()},
          std::vector<std::string>{props.begin(),   props.end()  }) {}
  /** Constructor from string vectors */
  Requirement(Session* m, const std::string& name, Requirement* parent,
               std::vector<std::string>&& imports,
               std::vector<std::string>&& methods,
               std::vector<std::string>&& props);

  bool empty() const noexcept {
    return imports_.empty() && methods_.empty() && props_.empty() &&
           parent_ ? parent_->empty() : true;
  }

  bool requiresMethod(const std::string& methodName) const;
  bool requiresProp(const std::string& propName) const;
  bool requiresInterface(const std::string& reqName) const;
  bool requiresItem(const std::string& itemName, RequiredItemType itemType) const;

private:
  bool setImports(Session* m, std::vector<std::string>&& imports);
  bool setMethods(Session* m, std::vector<std::string>&& methods);
  bool setProps(Session* m, std::vector<std::string>&& props);
};

//============================================================================//
//:::::::::::::::::::::::::   Requirement Manager   :::::::::::::::::::::::::://
//============================================================================//

class OClass;

class ReqMgr {
  /**
   *  @brief Global list of requirements
   *  @details The list or requirements contains pointers to
   *  @c Requirement objects obtained from the @c new operator.
   *  The list owns the stored pointers. It is sorted in the
   *  requirement identifiers. Requirement identifiers are unique.
   *  Requirements stored in this list may be orphan, i.e. not
   *  associated with a class.
   */
  std::vector<std::unique_ptr<Requirement>> reqs_;

public:
  /**
   *  @brief Constructor
   */
  ReqMgr() = default;
  /**
   *  @brief Register requirement
   *  @details It registers requirements with
   *  `std::vector<std::unique_ptr<Requirement>> reqs_` in a
   *  way that the list remains sorted in the requirement
   *  identifiers.
   *  @param req `Requirement&&`
   *  @return `Requirement*` pointer to the registered
   *      requirement or `nullptr` if already registered
   */
  Requirement* registerRequirement(Requirement&& req);
  /**
   *  @brief Get a registered @c Requirement object
   *  @param reqsId The identifier of the @c Requirement object
   *  @return A pointer to the @c Requirement object or
   *      `nullptr` if none was found
   */
  Requirement* getRequirement(const std::string& reqId) const;
  /**
   *  @brief Set requirement on class
   *  @details Associates a class with a requirement. The class must
   *  not be already associated with a requirement. The requirement
   *  should not be already registered with @c reqs_.
   *  @param type `OClass*`
   *  @param req `Requirement&&`
   *  @return @c bool Indicates if the operation was successful
   */
  bool addRequirement(OClass* type, Requirement&& req);
  /**
   *  @brief Add pre-registered requirement to class
   *  @details Associates a class with a requirement already
   *  associated with another class or registered with @c reqs_.
   *  The class must not have previously defined a requirement.
   *  @param type `OClass*` The class being registered with the
   *      supplied requirement
   *  @param reqsId The unique requirements identifier
   *  @return true if the operation completed successfully
   *  @return false if the class has requirements
   */
  bool importRequirement(OClass* type, const std::string& reqId);
};

#endif // __REQS_HPP__
