#ifndef __ORANGEOPERATION_HPP__
#define __ORANGEOPERATION_HPP__

#include <vector>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include "object.hpp"

/** Standard range operation types */
enum RangeOperationType : size_t {
  Nop,
  Insert,
  Replace,
};

struct RangeOperationBase;

/**
 *  @class OTie
 *  @brief Container-iterator tie
 *  @details Ties associate containers with iterators through
 *  an operation class. Thus, instances of `OTie` are owned by
 *  containers through operations on them. They represent a
 *  bond between the owning container and the iterator with
 *  type `itType`.
 *  @par Override the apply(RangeOperationBase*, ORef, ORef, ORef)
 *  method to provide functionality. Here is an example showing
 *  how to customize `OTie` for vector iterators:
 *  @code{.cpp}
 *  template<typename _Op, typename t>
 *    struct OVectorIterator_Tie : OTie {
 *      using OContainer = typename _Op::container;
 *      using OIterator = OVectorIterator<t>;
 *      OVectorIterator_Tie(OClass* type) : OTie{type} {}
 *      OVectorIterator_Tie(OVectorIterator_Tie&&) = default;
 *      virtual ~OVectorIterator_Tie() {}
 *      virtual void apply(RangeOperationBase* op, ORef cont, ORef a, ORef b)
 *          override {
 *        OContainer& c = *static_cast<OContainer*>(cont);
 *        auto& operation = static_cast<_Op&>(*op);
 *        operation(cont, a->castRef<OIterator>(), b->castRef<OIterator>());
 *      }
 *    };
 *  @endcode
 */
struct OTie {
  OClass* itType{nullptr};
  OTie() = default;
  OTie(OClass* itType) : itType{itType} {}
  OTie(OTie&& r) = default;
  virtual ~OTie() {}
  virtual void apply(RangeOperationBase*, ORef, ORef, ORef) {
    throw std::logic_error{"virtual OTie::apply(...) wasn't overridden"};
  }
  inline bool valid() const noexcept {return itType != nullptr;}
  inline int getTypeId() const noexcept {return itType->getTypeId();}
  inline int getRootTypeId() const noexcept {return itType->getRootTypeId();}
};

/**
 *  @class RangeOperationBase
 *  @brief Base class of range operations
 *  @details A range operation is an operation between
 *  two containers, more precisely, between a _destination
 *  container_ and a pair of two iterators pointing at a
 *  range of another container, the _source container_.
 */
struct RangeOperationBase {
  /** The _destination_ container type identifier */
  int containerId;
  /** Table of ties to integer type iterators */
  std::vector<std::unique_ptr<OTie>> intTies;
  /** Table of other ties */
  std::vector<std::unique_ptr<OTie>> ties;

  RangeOperationBase(int id) : containerId{id} {}
  virtual ~RangeOperationBase() {}

  bool empty() const noexcept {return intTies.empty() && ties.empty();}
  /** Get the tie of the indicated type */
  OTie* getTie(ORef o) const noexcept {
    const OClass* const type = o->getType();
    const auto& list = isIntIter(type->getRootTypeId()) ? intTies : ties;
    const auto end = list.cend();
    const auto it = std::find_if(list.cbegin(), end,
        [type](const auto& tie) {return type->extends(tie->itType);});
    return it == end ? nullptr : it->get();
  }

  void addTie(std::unique_ptr<OTie>&& tie) {
    if (!tie->valid()) throw std::logic_error{"invalid tie"};
    (isIntIter(tie->getRootTypeId()) ? intTies : ties).emplace_back(
        std::move(tie));
  }

  void addTie(OTie* tie) { addTie(std::unique_ptr<OTie>(tie)); }

  template<typename _Tie> requires std::is_base_of_v<OTie, _Tie>
    void addTie(_Tie&& tie) { addTie(new _Tie{std::move(tie)}); }

  /** Add ties to a suitable table of ties */
  template<typename... _Ties>
    requires (std::conjunction_v<std::is_base_of<OTie, _Ties>...>)
    void addTies(_Ties&&... ties) {
      OTie* v[sizeof...(ties)]{ new _Ties{std::move(ties)}... };
      for (OTie* tie : v) addTie(tie);
    }

  /**
   *  @brief Apply operation
   *  @details It applies the operation by chaining to the
   *  tie's apply(RangeOperationBase*, ORef, ORef, ORef) method,
   *  which in turn applies the operation's `operator()`.
   *  @return A boolean indicating if the operation was handled
   */
  bool apply(ORef cont, ORef a, ORef b) {
    OTie* tie = getTie(a);
    if (!tie) return false;
    tie->apply(this, cont, a, b);
    return true;
  }

  static inline bool _isIntVecIter(int typeId) noexcept
  { return (CLSID_ULONGVECITER-1) < typeId && typeId < (CLSID_SHORTVECITER+1); }
  static inline bool _isIntSetIter(int typeId) noexcept
  { return typeId == CLSID_ULONGSETITER || typeId == CLSID_LONGSETITER; }
  static inline bool isIntIter(int id) noexcept
  { return _isIntVecIter(id) || _isIntSetIter(id); }
};

/**
 *  @class RangeOperation
 *  @brief General purpose range operation class
 *  @details It is a ready-to-use range operation class
 *  for any container and any iterator type. It supports
 *  all basic operations on containers, insert, assign,
 *  etc. See enum RangeOperationType for details.
 *  @par The _source_ iterators are supplied as arguments
 *  to template<typename _It> void operator()(_It, _It).
 *  @tparam _OContainer The _destination_ container
 */
template<typename _OContainer>
struct RangeOperation : RangeOperationBase {
  using container = _OContainer;
  using iterator = typename _OContainer::iterator;
  /** Range operation type */
  RangeOperationType operationId;
  /**
   *  @brief Positions inside the owning container
   *  @details They depend on the container type and the
   *  operation to be performed. On return they may hold
   *  output information from the operation. They must be
   *  redefined before each run of the operation.
   */
  iterator from, to;

  RangeOperation(RangeOperationType rop, int contId) :
      operationId{rop},
      RangeOperationBase{contId} {}
  RangeOperation(RangeOperationType rop, int contId, iterator a) :
      operationId{rop},
      RangeOperationBase{contId}, from{a} {}
  RangeOperation(RangeOperationType rop, int contId, iterator a, iterator b) :
      operationId{rop},
      RangeOperationBase{contId}, from{a}, to{b} {}
  virtual ~RangeOperation() {}

  /**
   *  @brief Apply the std `C++` operation
   *  @details The arguments define the @em source iterators.
   *  @tparam `_It` The type of the source iterators
   */
  template<typename _It>
    void operator()(container& cont, _It begin, _It end) {
      switch (operationId) {
        case RangeOperationType::Insert:
          from = cont.insert(from, begin, end);
          break;
        case RangeOperationType::Replace:
          cont.replace(from, to, begin, end);
          break;
        default:
          throw std::logic_error{"unknown or unsupported operation"};
      }
    }
};

#endif // __ORANGEOPERATION_HPP__
