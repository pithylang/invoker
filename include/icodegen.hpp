#ifndef __ICODEGEN_HPP__
#define __ICODEGEN_HPP__

#include <iostream>
#include <vector>
#include <functional>
#include "itypes.hpp"
#include "mtypes.hpp"
#include "idgen.hpp"
#include "lvector.hpp"

//============================================================================//
//:::::::::::::::::::::::::::::  ICodeGenerator  ::::::::::::::::::::::::::::://
//============================================================================//
struct MBlock;

struct ICodeGenerator {
  typedef i1::vector<MBlock> MBlockTable;

  const IParser& parser;
  std::reference_wrapper<MBlockTable> blockTable;
  IdGenerator<0> tmpIdGenerator;
  int currentBlockIndex;

  ICodeGenerator(const IParser& parser, MBlockTable& btab) :
      parser(parser), blockTable(btab), currentBlockIndex(-1) {}

  inline MBlock& currentBlock()
  { return blockTable.get()[currentBlockIndex]; }
  inline const MBlock& currentBlock() const
  { return blockTable.get()[currentBlockIndex]; }

  void encode(MOpcode opcode, MOperand operand = 0);
  bool isSystemSymbol(const ISymbol& symbol) const noexcept;
  bool isBlockSymbol(MOperand ofs) const noexcept;
  MOperand addSymbol(const ISymbol& symbol);
  MOperand addSymbol(ISymbol&& symbol);
  MOperand addTmp(int tmpId);
  inline MOperand addTmp() {return addTmp(tmpIdGenerator.getId());}
  MOperand findTmpSymbol(int id) const;
  /** \brief Add symbol for block \c i */
  MOperand addBlock(int i);
  /** \brief Add local class in current block */
  void addClassDeclaration(const IReceivable&);

  void     generateCodeStatements(const IBlock& block);
  MOperand generateDecompAssignOperation(const IUnit& unit, bool isConst);
  MOperand generateDecompAssignOperation(const IArrayExpression& a,
                                         MOperand rhsItem, bool isConst);
  MOperand generateDecompRefOperation(const IUnit& unit, bool isConst);
  MOperand generateDecompRefOperation(const IArrayExpression& a,
                                      MOperand rhsItem, bool isConst);
  MOperand generateAssignmentOperation(const IUnit& unit, bool isConst);
  MOperand generateMakeReferenceOperation(const IUnit& unit, bool isConst);
  MOperand generateCopyOperation(const IUnit& unit);
  MOperand generatePropAccessOperation(const IUnit& unit, bool last = false);
  MOperand generateBlockInvocation(const IUnit& unit, bool last);
  MOperand generateBlockInvocationParams(const IUnit& unit, bool last);
  MOperand generateConditionalOperator(const IUnit& unit, bool last);
  MOperand generateClassDefinition(const IUnit& unit);
  MOperand generateCode(const IUnit& unit, bool* = nullptr, bool last = false);
  MOperand generateCode(const IItem& item, bool last = false);
  /**
   *  \brief Generate code for receivable
   *  \details Generate code for \em true receivable. This method
   *  does not apply to jsond receivables.
   *  \param noVCM `bool` Do not generate code for *variable
   *  creation method*
   */
  void     generateCode(const IReceivable& recbl, bool noVCM);
  MOperand generateCode(const IBlock& block, bool last);
  MOperand generateCode(const ISymbol& symbol);
  MOperand generateCode(ISymbol&& symbol);

  /**
   *  \brief Generate code for JSON definition
   *  \details This method is used in the reduction
   *  \c item : "{" \c jsond "}"
   *  It translates \c recbl into a unit on the \em system object
   *  and encodes its execution.
   */
  MOperand generateCodeISONDefinition(const IReceivable& recbl);
  /**
   *  \brief Generate code for array definition
   *  \details This method is used in the reductions
   *  \c item : "[" \c cexpr "]"
   *  \c item : "[" \c cexpr "," "]"
   *  It uses MOperand generateCodeDefinition(
   *  const std::vector<IUnit>& cexpr, std::string&& name)
   */
  MOperand generateCodeArrayDefinition(const IArrayExpression& cexpr);
  /**
   *  \brief Generate code for set definition
   *  \details This method is used in the reductions
   *  \c item : "{" \c cexpr "}"
   *  \c item : "{" \c cexpr "," "}"
   *  It uses MOperand generateCodeDefinition(
   *  const std::vector<IUnit>& cexpr, std::string&& name)
   */
  MOperand generateCodeSetDefinition(const ISetExpression& cexpr);
  /**
   *  \brief Generate code for array or set definition
   *  \details It translates \c cexpr into a unit on the \em system object
   *  and encodes its execution.
   */
  MOperand generateCodeDefinition(const std::vector<IUnit>& cexpr,
                                  std::string&& name);

  std::string disassemble() const;

  static std::string getStdMethodName(const std::string& originalMethodName);

private:
  void generateUnitTypeBlock(IUnit& unit);
};

#endif // __ICODEGEN_HPP__
