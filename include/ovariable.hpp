#ifndef __OVARIABLE_HPP__
#define __OVARIABLE_HPP__

#include <utility>

class OInstance;
class OClass;
struct SelectorCache;
struct SelectorData;
struct MSymbolReference;

/**
 *  @brief Usage of object instance
 */
enum : unsigned short {
  USG_NONE          = 0x0000,
  USG_LITERAL       = 0x0100,
  USG_MOVABLE       = 0x0200,
  USG_SWAPPABLE     = 0x0400,
  USG_REF           = 0x0800,
  USG_DISPOSABLE    = 0x1000,
  USG_CONST         = 0x2000,
  USG_IGNORE_CONST  = 0x4000,
  USG_NOT_INITABLE  = 0x8000,
};

/**
 *  @brief Variable type
 */
enum : unsigned short {
  /** Variable whose type will be determined on access */
  VARTYPE_UNKNOWN = 0,
  /** Variable is an object reference */
  VARTYPE_OBJECT,
  /** Variable is int data */
  VARTYPE_INT_DATA,
  /** Variable is a reference to a symbol */
  VARTYPE_SYM_REF,
  /** Variable is a reference to a selector cache */
  VARTYPE_SEL_CACHE,
  /** Variable is a reference to selector data */
  VARTYPE_SEL_DATA,
};

/** @brief Object Model runtime variable */
struct OVariable {
  /** Usage of object instance */
  unsigned short usage;
  /** Variable type */
  unsigned short type;
  union {
    /** Active when type is VARTYPE_OBJECT */
    OInstance* objectRef;
    /** Active when type is VARTYPE_SYM_REF */
    MSymbolReference* symRef;
    /** Active when type is VARTYPE_INT_DATA */
    std::size_t intValue;
    /** Active when type is VARTYPE_SEL_CACHE */
    SelectorCache* selectorCache;
    /** Active when type is VARTYPE_SEL_DATA */
    SelectorData* selectorData;
  };

  inline explicit OVariable(OInstance* o = nullptr) noexcept :
      usage(0), type(VARTYPE_OBJECT), objectRef(o) {}
  inline OVariable(OInstance* o, unsigned short usageFlags) noexcept :
      usage(usageFlags), type(VARTYPE_OBJECT), objectRef(o) {}
  inline OVariable(MSymbolReference* sr, unsigned short usageFlags) noexcept :
      usage(usageFlags), type(VARTYPE_SYM_REF), symRef(sr) {}
  inline explicit OVariable(std::size_t i) noexcept :
      usage(0), type(VARTYPE_INT_DATA), intValue{i} {}
  inline explicit OVariable(SelectorCache* c) noexcept :
      usage(0), type(VARTYPE_SEL_CACHE), selectorCache(c) {}
  inline explicit OVariable(SelectorData* data) noexcept :
      usage(0), type(VARTYPE_SEL_DATA), selectorData(data) {}

  inline void dispose() noexcept {usage |= USG_DISPOSABLE;}
  inline void undispose() noexcept {usage &= ~USG_DISPOSABLE;}
  inline bool disposable() const noexcept {return (usage & USG_DISPOSABLE) != 0;}
  inline bool notInitializable() const noexcept {return (usage & USG_NOT_INITABLE);}
  inline bool isConst() const noexcept {return (usage & USG_CONST) != 0;}
  inline void setConst() noexcept {usage |= USG_CONST;}
  inline bool isMutable() const noexcept
  { return (usage & USG_CONST) == 0 || (usage & USG_IGNORE_CONST) != 0; }
  inline bool undefined() const noexcept {return objectRef == nullptr;}
  inline bool undefinedSelector() const noexcept {return !selectorCache;}
  inline bool isRef() const noexcept {return (usage & USG_REF) != 0;}
  inline void setRef() noexcept {usage |= USG_REF;}
  inline bool isLiteral() const noexcept {return (usage & USG_LITERAL) != 0;}
  inline void setLiteral() noexcept {usage |= USG_LITERAL;}
  inline bool movable() const noexcept {return (usage & USG_MOVABLE) != 0;}
  inline bool isMovable() const noexcept {return movable();}
  inline void setMovable() noexcept {usage |= USG_MOVABLE;}
  inline void unmove() noexcept {usage &= ~USG_MOVABLE;}

  inline OVariable result() const noexcept
  { return OVariable(objectRef, usage & USG_CONST); }

  static inline OVariable disposableResult(OInstance *o) noexcept
  { return OVariable{o, USG_DISPOSABLE}; }
  static inline OVariable movableResult(OInstance *o) noexcept
  { return OVariable{o, USG_MOVABLE}; }
  static inline OVariable undefinedResult() noexcept { return OVariable{}; }
};

typedef OVariable ORet, OVar, *OArg;

/**
 *  @brief Get variable parameter
 *  @details See VirtualMachine::resolveSymbolType() @c what argument
 */
enum {
  RSYM_LOCAL_DEF = 0,
  RSYM_STATIC_DEF = 1,
  RSYM_ANY,
};

#define REF(var) (var).objectRef

#endif // __OVARIABLE_HPP__
