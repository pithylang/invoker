#include "objectAutoCleaner.hpp"
#include "odev.h"
#include "olocale.hpp"
#include "obasicTypeInfo.hpp"
#include "ortmgr.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "otypes.h"
#include "fswitch.h"

using std::string;
using std::vector;

#define DEFINE_FACET_SWITCH(w, n)          \
  SWITCH_TABLE(w, n)                       \
    DEFINE_CASE(w, null_facet_id)          \
                                           \
    DEFINE_CASE(w, num_get_id)             \
    DEFINE_CASE(w, num_getw_id)            \
    DEFINE_CASE(w, num_put_id)             \
    DEFINE_CASE(w, num_putw_id)            \
    DEFINE_CASE(w, numpunct_id)            \
    DEFINE_CASE(w, numpunctw_id)           \
    DEFINE_CASE(w, numpunct_byname_id)     \
    DEFINE_CASE(w, numpunctw_byname_id)    \
                                           \
    DEFINE_CASE(w, time_get_id)            \
    DEFINE_CASE(w, time_getw_id)           \
    DEFINE_CASE(w, time_put_id)            \
    DEFINE_CASE(w, time_putw_id)           \
    DEFINE_CASE(w, time_get_byname_id)     \
    DEFINE_CASE(w, time_getw_byname_id)    \
    DEFINE_CASE(w, time_put_byname_id)     \
    DEFINE_CASE(w, time_putw_byname_id)    \
                                           \
    DEFINE_CASE(w, money_get_id)           \
    DEFINE_CASE(w, money_getw_id)          \
    DEFINE_CASE(w, money_put_id)           \
    DEFINE_CASE(w, money_putw_id)          \
    DEFINE_CASE(w, moneypunct_id)          \
    DEFINE_CASE(w, moneypunctw_id)         \
    DEFINE_CASE(w, moneypuncti_id)         \
    DEFINE_CASE(w, moneypunctiw_id)        \
    DEFINE_CASE(w, moneypunct_byname_id)   \
    DEFINE_CASE(w, moneypunctw_byname_id)  \
    DEFINE_CASE(w, moneypuncti_byname_id)  \
    DEFINE_CASE(w, moneypunctiw_byname_id) \
                                           \
    DEFINE_CASE(w, ctype_id)               \
    DEFINE_CASE(w, ctypew_id)              \
    DEFINE_CASE(w, ctype_byname_id)        \
    DEFINE_CASE(w, ctypew_byname_id)       \
    DEFINE_CASE(w, codecvt_id)             \
    DEFINE_CASE(w, codecvtw_id)            \
    DEFINE_CASE(w, codecvt_byname_id)      \
    DEFINE_CASE(w, codecvtw_byname_id)     \
                                           \
    DEFINE_CASE(w, collate_id)             \
    DEFINE_CASE(w, collatew_id)            \
    DEFINE_CASE(w, collate_byname_id)      \
    DEFINE_CASE(w, collatew_byname_id)     \
                                           \
    DEFINE_CASE(w, messages_id)            \
    DEFINE_CASE(w, messagesw_id)           \
    DEFINE_CASE(w, messages_byname_id)     \
    DEFINE_CASE(w, messagesw_byname_id)    \
                                           \
    DEFINE_CASE(w, user_facet_id)          \
    DEFINE_CASE(w, user_facetw_id)         \
  END_SWITCH_TABLE(w)

//============================================================================//
//::::::::::::::::::::::::::::::::   facets   :::::::::::::::::::::::::::::::://
//============================================================================//

const vector<string> Facet::facetNames {
  "null_facet",

  "num_get",
  "num_getw",
  "num_put",
  "num_putw",
  "numpunct",
  "numpunctw",
  "numpunct_byname",
  "numpunctw_byname",

  "time_get",
  "time_getw",
  "time_put",
  "time_putw",
  "time_get_byname",
  "time_getw_byname",
  "time_put_byname",
  "time_putw_byname",

  "money_get",
  "money_getw",
  "money_put",
  "money_putw",
  "moneypunct",
  "moneypunctw",
  "moneypuncti",
  "moneypunctiw",
  "moneypunct_byname",
  "moneypunctw_byname",
  "moneypuncti_byname",
  "moneypunctiw_byname",

  "ctype",
  "ctypew",
  "ctype_byname",
  "ctypew_byname",
  "codecvt",
  "codecvtw",
  "codecvt_byname",
  "codecvtw_byname",

  "collate",
  "collatew",
  "collate_byname",
  "collatew_byname",

  "messages",
  "messagesw",
  "messages_byname",
  "messagesw_byname",

  "user_facet",
  "user_facetw_id"
};

template<typename charT>
  std::locale::id user_facet<charT>::id;

bool Facet::setFacet(std::nullptr_t) {
  if (index() != null_facet_id) return false;
  emplace<std::nullptr_t>(nullptr);
  return true;
}

template<typename t>
bool Facet::setFacet(t* facetPtr) {
  if (index() != null_facet_id) return false;
  emplace<t*>(facetPtr);
  return true;
}

template<typename t>
bool Facet::setFacet(const t* facetPtr) {
  return setFacet(const_cast<t*>(facetPtr));
}

bool Facet::setFacet(int which, const string& name) {
  DEFINE_FACET_SWITCH(setFacet, 0)

  SWITCH(setFacet, which)
    CASE (setFacet, null_facet_id):
      return setFacet(nullptr);

    CASE (setFacet, num_get_id):
      return setFacet(new std::num_get<char>);
    CASE (setFacet, num_getw_id):
      return setFacet(new std::num_get<wchar_t>);
    CASE (setFacet, num_put_id):
      return setFacet(new std::num_put<char>);
    CASE (setFacet, num_putw_id):
      return setFacet(new std::num_put<wchar_t>);
    CASE (setFacet, numpunct_id):
      return setFacet(new std::numpunct<char>);
    CASE (setFacet, numpunctw_id):
      return setFacet(new std::numpunct<wchar_t>);
    CASE (setFacet, numpunct_byname_id):
      return setFacet(new std::numpunct_byname<char>{name});
    CASE (setFacet, numpunctw_byname_id):
      return setFacet(new std::numpunct_byname<wchar_t>{name});

    CASE (setFacet, time_get_id):
      return setFacet(new std::time_get<char>);
    CASE (setFacet, time_getw_id):
      return setFacet(new std::time_get<wchar_t>);
    CASE (setFacet, time_put_id):
      return setFacet(new std::time_put<char>);
    CASE (setFacet, time_putw_id):
      return setFacet(new std::time_put<wchar_t>);
    CASE (setFacet, time_get_byname_id):
      return setFacet(new std::time_get_byname<char>{name});
    CASE (setFacet, time_getw_byname_id):
      return setFacet(new std::time_get_byname<wchar_t>{name});
    CASE (setFacet, time_put_byname_id):
      return setFacet(new std::time_put_byname<char>{name});
    CASE (setFacet, time_putw_byname_id):
      return setFacet(new std::time_put_byname<wchar_t>{name.c_str()});

    CASE (setFacet, money_get_id):
      return setFacet(new std::money_get<char>);
    CASE (setFacet, money_getw_id):
      return setFacet(new std::money_get<wchar_t>);
    CASE (setFacet, money_put_id):
      return setFacet(new std::money_put<char>);
    CASE (setFacet, money_putw_id):
      return setFacet(new std::money_put<wchar_t>);
    CASE (setFacet, moneypunct_id):
      return setFacet(new std::moneypunct<char>);
    CASE (setFacet, moneypunctw_id):
      return setFacet(new std::moneypunct<wchar_t>);
    CASE (setFacet, moneypuncti_id):
      return setFacet(new std::moneypunct<char, true>);
    CASE (setFacet, moneypunctiw_id):
      return setFacet(new std::moneypunct<wchar_t, true>);
    CASE (setFacet, moneypunct_byname_id):
      return setFacet(new std::moneypunct_byname<char>{name});
    CASE (setFacet, moneypunctw_byname_id):
      return setFacet(new std::moneypunct_byname<wchar_t>{name});
    CASE (setFacet, moneypuncti_byname_id):
      return setFacet(new std::moneypunct_byname<char, true>{name});
    CASE (setFacet, moneypunctiw_byname_id):
      return setFacet(new std::moneypunct_byname<wchar_t, true>{name});

    CASE (setFacet, ctype_id):
      return setFacet(new std::ctype<char>);
    CASE (setFacet, ctypew_id):
      return setFacet(new std::ctype<wchar_t>);
    CASE (setFacet, ctype_byname_id):
      return setFacet(new std::ctype_byname<char>{name});
    CASE (setFacet, ctypew_byname_id):
      return setFacet(new std::ctype_byname<wchar_t>{name});
    CASE (setFacet, codecvt_id):
      return setFacet(new std::codecvt<char, char, std::mbstate_t>);
    CASE (setFacet, codecvtw_id):
      return setFacet(new std::codecvt<wchar_t, char, std::mbstate_t>);
    CASE (setFacet, codecvt_byname_id):
      return setFacet(new std::codecvt_byname<char, char, std::mbstate_t>{name});
    CASE (setFacet, codecvtw_byname_id):
      return setFacet(new std::codecvt_byname<wchar_t, char, std::mbstate_t>{name});

    CASE (setFacet, collate_id):
      return setFacet(new std::collate<char>);
    CASE (setFacet, collatew_id):
      return setFacet(new std::collate<wchar_t>);
    CASE (setFacet, collate_byname_id):
      return setFacet(new std::collate_byname<char>{name});
    CASE (setFacet, collatew_byname_id):
      return setFacet(new std::collate_byname<wchar_t>{name});

    CASE (setFacet, messages_id):
      return setFacet(new std::messages<char>);
    CASE (setFacet, messagesw_id):
      return setFacet(new std::messages<wchar_t>);
    CASE (setFacet, messages_byname_id):
      return setFacet(new std::messages_byname<char>{name});
    CASE (setFacet, messagesw_byname_id):
      return setFacet(new std::messages_byname<wchar_t>{name});

    CASE (setFacet, user_facet_id):
      return setFacet(new user_facet<char>);
    CASE (setFacet, user_facetw_id):
      return setFacet(new user_facet<wchar_t>);

    DEFAULT(setFacet):
      throw string{"incorrect facet id: "+std::to_string(which)};
  END_SWITCH(setFacet)
  return false;
}

//:::::::::::::::::::::::::::::   facet object   ::::::::::::::::::::::::::::://

OM_BEGIN_METHOD_TABLE(oxFacet, OInstance)
  PUBLIC (oxFacet::ommType,     "value:")
  PUBLIC (oxFacet::ommType,     "type:")
  PUBLIC (oxFacet::ommTypeName, "type:name:")
OM_END_METHOD_TABLE()

inline OClass* facetClass() noexcept {
  extern OClass* _OXType_class[];
  return _OXType_class[CLSID_FACET];
}

ORet oxFacet::ommType(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("facet::type:", 1);
  ssize_t typeIndex = -1;
  // unpack arguments
  if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    auto it = std::find(facetNames.begin(), facetNames.end(), facetName);
    if (it != facetNames.end())
      typeIndex = std::distance(facetNames.begin(), it);
  }
  else {
    typeIndex = g_castValue<ssize_t>(argv, m);
    OM_CHECK_EXCEPTION();
  }
  // check facet not found
  if (typeIndex == -1 && OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    OM_THROW_RTE("there is no facet '"+facetName+"'")
  }
  // set facet
  try {
    const bool success = setFacet(typeIndex);
    if (!success)
      OM_THROW_RTE("the facet is already defined")
  }
  catch (const string& msg) {
    OM_THROW_RTE(msg)
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE(string{"std::runtime_error: "}+e.what());
  }
  catch (...) {
    OM_THROW_RTE("error creating facet");
  }
  return makeResult();
}

ORet oxFacet::ommTypeName(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("facet::type:name:", 2);
  ssize_t typeIndex = -1;
  // unpack arguments
  if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    auto it = std::find(facetNames.begin(), facetNames.end(), facetName);
    if (it != facetNames.end())
      typeIndex = std::distance(facetNames.begin(), it);
  }
  else {
    typeIndex = g_castValue<ssize_t>(argv, m);
    OM_CHECK_EXCEPTION();
  }
  OM_UNPACK_CONST_REF(String, localeName, 1);
  // check facet not found
  if (typeIndex == -1 && OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    OM_THROW_RTE("there is no facet '"+facetName+"'")
  }
  // set facet
  try {
    const bool success = setFacet(typeIndex, localeName);
    if (!success)
      OM_THROW_RTE("the facet is already defined")
  }
  catch (const string& msg) {
    OM_THROW_RTE(msg)
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE(string{"std::runtime_error: "}+e.what());
  }
  catch (...) {
    OM_THROW_RTE("error creating facet");
  }
  return makeResult();
}

//:::::::::::::::::::::::::::   facet metaclass   :::::::::::::::::::::::::::://

OM_DEFINE_MCLASS(Facet)

ORef oxFacet_Class::copyCore(ORef tgt, const_ORef src, Session* m) const {
  if (src->getRootClass() != getRootClass()) return nullptr;
  try {
    *static_cast<OT*>(tgt) = *static_cast<const OT*>(src);
  }
  catch (const string& msg) {
    m->throwRTE("oxFacet_Class::copyCore", msg);
  }
  return nullptr;
}

ORef oxFacet_Class::moveCore(ORef tgt, ORef src) const {
  if (src->getRootClass() != getRootClass()) return nullptr;
  *static_cast<OT*>(tgt) = std::move(*static_cast<OT*>(src));
  return tgt;
}

OM_IMPLEMENT_CLASSID_INSTALLER(Facet, CLSID_FACET)

//============================================================================//
//::::::::::::::::::::::::::::::::   locale   :::::::::::::::::::::::::::::::://
//============================================================================//

void oxLocale::toUpper(std::wstring& wideText) const {
  if (!std::has_facet<std::ctype<wchar_t>>(*this))
    noConversionError();
  const auto& f = std::use_facet<std::ctype<wchar_t>>(*this);
  f.toupper(&*wideText.begin(), &*wideText.end());
}

void oxLocale::toLower(std::wstring& wideText) const {
  if (!std::has_facet<std::ctype<wchar_t>>(*this))
    noConversionError();
  const auto& f = std::use_facet<std::ctype<wchar_t>>(*this);
  f.tolower(&*wideText.begin(), &*wideText.end());
}

void oxLocale::noConversionError() const {
  string localeName = name();
  if (localeName.size() > 8)
    localeName = localeName.substr(0, 16) + "...";
  throw "the locale '"+localeName+"' does not implement "
        "the requested conversion";
}

static
void s_conversionError(std::codecvt_base::result ec, const string& methodName) {
  const string msg = (ec == std::codecvt_base::partial) ?
    "not enough space in the output buffer "
    "or unexpected end of source buffer" :
    (ec == std::codecvt_base::error) ?
    "character could not be converted" :
    "this facet is non-converting, no output written";
  throw methodName+": "+msg;
}

std::wstring oxLocale::toWide(const string& text) const {
  if (!std::has_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this))
    noConversionError();
  auto const& f =
      std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this);
  std::mbstate_t mb{};
  std::wstring to(text.size(), '\0');
  const char* fromNext;
  wchar_t* toNext;
  const string& from = text;
  const auto ec = f.in(mb, &from[0], &from[from.size()], fromNext,
                           &to[0],   &to[to.size()],     toNext);
  if (ec != std::codecvt_base::ok)
    s_conversionError(ec, "toWide");
  to.resize(toNext - &to[0]);
  return to;
}

  // back to narrow string
string oxLocale::toNarrow(const std::wstring& from) const {
  if (!std::has_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this))
    noConversionError();
  auto const& f =
      std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this);
  std::string to(from.size()*f.max_length(), '\0');
  std::mbstate_t mb{};
  const wchar_t* fromNext;
  char* toNext;
  const auto ec = f.out(mb, &from[0], &from[from.size()], fromNext,
                            &to[0], &to[to.size()], toNext);
  if (ec != std::codecvt_base::ok)
    s_conversionError(ec, "toNarrow");
  to.resize(toNext - &to[0]);
  return to;
}

string oxLocale::toUpper(const string& text) const {
  std::wstring wideText = toWide(text);
  toUpper(wideText);
  return toNarrow(wideText);
}

string oxLocale::toLower(const string& text) const {
  std::wstring wideText = toWide(text);
  toLower(wideText);
  return toNarrow(wideText);
}

Locale oxLocale::combineWith(const Locale& other, Facet& facet) {
  DEFINE_FACET_SWITCH(combine, 0)
  const auto which = facet.index();
  SWITCH(combine, which)
    CASE (combine, null_facet_id):
      throw string{"cannot combine locale with null facet"};

    CASE (combine, num_get_id):
      return Locale(other, std::get<std::num_get<char>*>(facet));
    CASE (combine, num_getw_id):
      return Locale(other, std::get<std::num_get<wchar_t>*>(facet));
    CASE (combine, num_put_id):
      return Locale(other, std::get<std::num_put<char>*>(facet));
    CASE (combine, num_putw_id):
      return Locale(other, std::get<std::num_put<wchar_t>*>(facet));
    CASE (combine, numpunct_id):
      return Locale(other, std::get<std::numpunct<char>*>(facet));
    CASE (combine, numpunctw_id):
      return Locale(other, std::get<std::numpunct<wchar_t>*>(facet));
    CASE (combine, numpunct_byname_id):
      return Locale(other, std::get<std::numpunct_byname<char>*>(facet));
    CASE (combine, numpunctw_byname_id):
      return Locale(other, std::get<std::numpunct_byname<wchar_t>*>(facet));

    CASE (combine, time_get_id):
      return Locale(other, std::get<std::time_get<char>*>(facet));
    CASE (combine, time_getw_id):
      return Locale(other, std::get<std::time_get<wchar_t>*>(facet));
    CASE (combine, time_put_id):
      return Locale(other, std::get<std::time_put<char>*>(facet));
    CASE (combine, time_putw_id):
      return Locale(other, std::get<std::time_put<wchar_t>*>(facet));
    CASE (combine, time_get_byname_id):
      return Locale(other, std::get<std::time_get_byname<char>*>(facet));
    CASE (combine, time_getw_byname_id):
      return Locale(other, std::get<std::time_get_byname<wchar_t>*>(facet));
    CASE (combine, time_put_byname_id):
      return Locale(other, std::get<std::time_put_byname<char>*>(facet));
    CASE (combine, time_putw_byname_id):
      return Locale(other, std::get<std::time_put_byname<wchar_t>*>(facet));

    CASE (combine, money_get_id):
      return Locale(other, std::get<std::money_get<char>*>(facet));
    CASE (combine, money_getw_id):
      return Locale(other, std::get<std::money_get<wchar_t>*>(facet));
    CASE (combine, money_put_id):
      return Locale(other, std::get<std::money_put<char>*>(facet));
    CASE (combine, money_putw_id):
      return Locale(other, std::get<std::money_put<wchar_t>*>(facet));
    CASE (combine, moneypunct_id):
      return Locale(other, std::get<std::moneypunct<char>*>(facet));
    CASE (combine, moneypunctw_id):
      return Locale(other, std::get<std::moneypunct<wchar_t>*>(facet));
    CASE (combine, moneypuncti_id):
      return Locale(other, std::get<std::moneypunct<char, true>*>(facet));
    CASE (combine, moneypunctiw_id):
      return Locale(other, std::get<std::moneypunct<wchar_t, true>*>(facet));
    CASE (combine, moneypunct_byname_id):
      return Locale(other, std::get<std::moneypunct_byname<char>*>(facet));
    CASE (combine, moneypunctw_byname_id):
      return Locale(other, std::get<std::moneypunct_byname<wchar_t>*>(facet));
    CASE (combine, moneypuncti_byname_id):
      return Locale(other, std::get<std::moneypunct_byname<char, true>*>(facet));
    CASE (combine, moneypunctiw_byname_id):
      return Locale(other, std::get<std::moneypunct_byname<wchar_t, true>*>(facet));

    CASE (combine, ctype_id):
      return Locale(other, std::get<std::ctype<char>*>(facet));
    CASE (combine, ctypew_id):
      return Locale(other, std::get<std::ctype<wchar_t>*>(facet));
    CASE (combine, ctype_byname_id):
      return Locale(other, std::get<std::ctype_byname<char>*>(facet));
    CASE (combine, ctypew_byname_id):
      return Locale(other, std::get<std::ctype_byname<wchar_t>*>(facet));
    CASE (combine, codecvt_id):
      return Locale(other, std::get<std::codecvt<char, char, std::mbstate_t>*>(facet));
    CASE (combine, codecvtw_id):
      return Locale(other, std::get<std::codecvt<wchar_t, char, std::mbstate_t>*>(facet));
    CASE (combine, codecvt_byname_id):
      return Locale(other, std::get<std::codecvt_byname<char, char, std::mbstate_t>*>(facet));
    CASE (combine, codecvtw_byname_id):
      return Locale(other, std::get<std::codecvt_byname<wchar_t, char, std::mbstate_t>*>(facet));

    CASE (combine, collate_id):
      return Locale(other, std::get<std::collate<char>*>(facet));
    CASE (combine, collatew_id):
      return Locale(other, std::get<std::collate<wchar_t>*>(facet));
    CASE (combine, collate_byname_id):
      return Locale(other, std::get<std::collate_byname<char>*>(facet));
    CASE (combine, collatew_byname_id):
      return Locale(other, std::get<std::collate_byname<wchar_t>*>(facet));

    CASE (combine, messages_id):
      return Locale(other, std::get<std::messages<char>*>(facet));
    CASE (combine, messagesw_id):
      return Locale(other, std::get<std::messages<wchar_t>*>(facet));
    CASE (combine, messages_byname_id):
      return Locale(other, std::get<std::messages_byname<char>*>(facet));
    CASE (combine, messagesw_byname_id):
      return Locale(other, std::get<std::messages_byname<wchar_t>*>(facet));

    CASE (combine, user_facet_id):
      return Locale(other, std::get<user_facet<char>*>(facet));
    CASE (combine, user_facetw_id):
      return Locale(other, std::get<user_facet<wchar_t>*>(facet));

    DEFAULT(combine):
      throw string{"incorrect facet id: "+std::to_string(which)};
  END_SWITCH(combine)
  return Locale{};
}

Locale oxLocale::combineWith(const Locale& other, size_t facetIndex) const {
  DEFINE_FACET_SWITCH(combine, 0)

  SWITCH(combine, facetIndex)
    CASE (combine, null_facet_id):
      throw string{"cannot combine locale with null facet"};

    CASE (combine, num_get_id):
      return combine<std::num_get<char>>(other);
    CASE (combine, num_getw_id):
      return combine<std::num_get<wchar_t>>(other);
    CASE (combine, num_put_id):
      return combine<std::num_put<char>>(other);
    CASE (combine, num_putw_id):
      return combine<std::num_put<wchar_t>>(other);
    CASE (combine, numpunct_id):
      return combine<std::numpunct<char>>(other);
    CASE (combine, numpunctw_id):
      return combine<std::numpunct<wchar_t>>(other);
    CASE (combine, numpunct_byname_id):
      return combine<std::numpunct_byname<char>>(other);
    CASE (combine, numpunctw_byname_id):
      return combine<std::numpunct_byname<wchar_t>>(other);

    CASE (combine, time_get_id):
      return combine<std::time_get<char>>(other);
    CASE (combine, time_getw_id):
      return combine<std::time_get<wchar_t>>(other);
    CASE (combine, time_put_id):
      return combine<std::time_put<char>>(other);
    CASE (combine, time_putw_id):
      return combine<std::time_put<wchar_t>>(other);
    CASE (combine, time_get_byname_id):
      return combine<std::time_get_byname<char>>(other);
    CASE (combine, time_getw_byname_id):
      return combine<std::time_get_byname<wchar_t>>(other);
    CASE (combine, time_put_byname_id):
      return combine<std::time_put_byname<char>>(other);
    CASE (combine, time_putw_byname_id):
      return combine<std::time_put_byname<wchar_t>>(other);

    CASE (combine, money_get_id):
      return combine<std::money_get<char>>(other);
    CASE (combine, money_getw_id):
      return combine<std::money_get<wchar_t>>(other);
    CASE (combine, money_put_id):
      return combine<std::money_put<char>>(other);
    CASE (combine, money_putw_id):
      return combine<std::money_put<wchar_t>>(other);
    CASE (combine, moneypunct_id):
      return combine<std::moneypunct<char>>(other);
    CASE (combine, moneypunctw_id):
      return combine<std::moneypunct<wchar_t>>(other);
    CASE (combine, moneypuncti_id):
      return combine<std::moneypunct<char, true>>(other);
    CASE (combine, moneypunctiw_id):
      return combine<std::moneypunct<wchar_t, true>>(other);
    CASE (combine, moneypunct_byname_id):
      return combine<std::moneypunct_byname<char>>(other);
    CASE (combine, moneypunctw_byname_id):
      return combine<std::moneypunct_byname<wchar_t>>(other);
    CASE (combine, moneypuncti_byname_id):
      return combine<std::moneypunct_byname<char, true>>(other);
    CASE (combine, moneypunctiw_byname_id):
      return combine<std::moneypunct_byname<wchar_t, true>>(other);

    CASE (combine, ctype_id):
      return combine<std::ctype<char>>(other);
    CASE (combine, ctypew_id):
      return combine<std::ctype<wchar_t>>(other);
    CASE (combine, ctype_byname_id):
      return combine<std::ctype_byname<char>>(other);
    CASE (combine, ctypew_byname_id):
      return combine<std::ctype_byname<wchar_t>>(other);
    CASE (combine, codecvt_id):
      return combine<std::codecvt<char, char, std::mbstate_t>>(other);
    CASE (combine, codecvtw_id):
      return combine<std::codecvt<wchar_t, char, std::mbstate_t>>(other);
    CASE (combine, codecvt_byname_id):
      return combine<std::codecvt_byname<char, char, std::mbstate_t>>(other);
    CASE (combine, codecvtw_byname_id):
      return combine<std::codecvt_byname<wchar_t, char, std::mbstate_t>>(other);

    CASE (combine, collate_id):
      return combine<std::collate<char>>(other);
    CASE (combine, collatew_id):
      return combine<std::collate<wchar_t>>(other);
    CASE (combine, collate_byname_id):
      return combine<std::collate_byname<char>>(other);
    CASE (combine, collatew_byname_id):
      return combine<std::collate_byname<wchar_t>>(other);

    CASE (combine, messages_id):
      return combine<std::messages<char>>(other);
    CASE (combine, messagesw_id):
      return combine<std::messages<wchar_t>>(other);
    CASE (combine, messages_byname_id):
      return combine<std::messages_byname<char>>(other);
    CASE (combine, messagesw_byname_id):
      return combine<std::messages_byname<wchar_t>>(other);

    CASE (combine, user_facet_id):
      return combine<user_facet<char>>(other);
    CASE (combine, user_facetw_id):
      return combine<user_facet<wchar_t>>(other);

    DEFAULT(combine):
      throw string{"incorrect facet id: "+std::to_string(facetIndex)};
  END_SWITCH(combine)
  return Locale{};
}

bool oxLocale::hasFacet(size_t facetIndex) const noexcept {
  DEFINE_FACET_SWITCH(hasFacet, 0)

  SWITCH(hasFacet, facetIndex)
    CASE (hasFacet, null_facet_id):
      return false;

    CASE (hasFacet, num_get_id):
      return std::has_facet<std::num_get<char>>(*this);
    CASE (hasFacet, num_getw_id):
      return std::has_facet<std::num_get<wchar_t>>(*this);
    CASE (hasFacet, num_put_id):
      return std::has_facet<std::num_put<char>>(*this);
    CASE (hasFacet, num_putw_id):
      return std::has_facet<std::num_put<wchar_t>>(*this);
    CASE (hasFacet, numpunct_id):
      return std::has_facet<std::numpunct<char>>(*this);
    CASE (hasFacet, numpunctw_id):
      return std::has_facet<std::numpunct<wchar_t>>(*this);
    CASE (hasFacet, numpunct_byname_id):
      return std::has_facet<std::numpunct_byname<char>>(*this);
    CASE (hasFacet, numpunctw_byname_id):
      return std::has_facet<std::numpunct_byname<wchar_t>>(*this);

    CASE (hasFacet, time_get_id):
      return std::has_facet<std::time_get<char>>(*this);
    CASE (hasFacet, time_getw_id):
      return std::has_facet<std::time_get<wchar_t>>(*this);
    CASE (hasFacet, time_put_id):
      return std::has_facet<std::time_put<char>>(*this);
    CASE (hasFacet, time_putw_id):
      return std::has_facet<std::time_put<wchar_t>>(*this);
    CASE (hasFacet, time_get_byname_id):
      return std::has_facet<std::time_get_byname<char>>(*this);
    CASE (hasFacet, time_getw_byname_id):
      return std::has_facet<std::time_get_byname<wchar_t>>(*this);
    CASE (hasFacet, time_put_byname_id):
      return std::has_facet<std::time_put_byname<char>>(*this);
    CASE (hasFacet, time_putw_byname_id):
      return std::has_facet<std::time_put_byname<wchar_t>>(*this);

    CASE (hasFacet, money_get_id):
      return std::has_facet<std::money_get<char>>(*this);
    CASE (hasFacet, money_getw_id):
      return std::has_facet<std::money_get<wchar_t>>(*this);
    CASE (hasFacet, money_put_id):
      return std::has_facet<std::money_put<char>>(*this);
    CASE (hasFacet, money_putw_id):
      return std::has_facet<std::money_put<wchar_t>>(*this);
    CASE (hasFacet, moneypunct_id):
      return std::has_facet<std::moneypunct<char>>(*this);
    CASE (hasFacet, moneypunctw_id):
      return std::has_facet<std::moneypunct<wchar_t>>(*this);
    CASE (hasFacet, moneypuncti_id):
      return std::has_facet<std::moneypunct<char, true>>(*this);
    CASE (hasFacet, moneypunctiw_id):
      return std::has_facet<std::moneypunct<wchar_t, true>>(*this);
    CASE (hasFacet, moneypunct_byname_id):
      return std::has_facet<std::moneypunct_byname<char>>(*this);
    CASE (hasFacet, moneypunctw_byname_id):
      return std::has_facet<std::moneypunct_byname<wchar_t>>(*this);
    CASE (hasFacet, moneypuncti_byname_id):
      return std::has_facet<std::moneypunct_byname<char, true>>(*this);
    CASE (hasFacet, moneypunctiw_byname_id):
      return std::has_facet<std::moneypunct_byname<wchar_t, true>>(*this);

    CASE (hasFacet, ctype_id):
      return std::has_facet<std::ctype<char>>(*this);
    CASE (hasFacet, ctypew_id):
      return std::has_facet<std::ctype<wchar_t>>(*this);
    CASE (hasFacet, ctype_byname_id):
      return std::has_facet<std::ctype_byname<char>>(*this);
    CASE (hasFacet, ctypew_byname_id):
      return std::has_facet<std::ctype_byname<wchar_t>>(*this);
    CASE (hasFacet, codecvt_id):
      return std::has_facet<std::codecvt<char, char, std::mbstate_t>>(*this);
    CASE (hasFacet, codecvtw_id):
      return std::has_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this);
    CASE (hasFacet, codecvt_byname_id):
      return std::has_facet<std::codecvt_byname<char, char, std::mbstate_t>>(*this);
    CASE (hasFacet, codecvtw_byname_id):
      return std::has_facet<std::codecvt_byname<wchar_t, char, std::mbstate_t>>(*this);

    CASE (hasFacet, collate_id):
      return std::has_facet<std::collate<char>>(*this);
    CASE (hasFacet, collatew_id):
      return std::has_facet<std::collate<wchar_t>>(*this);
    CASE (hasFacet, collate_byname_id):
      return std::has_facet<std::collate_byname<char>>(*this);
    CASE (hasFacet, collatew_byname_id):
      return std::has_facet<std::collate_byname<wchar_t>>(*this);

    CASE (hasFacet, messages_id):
      return std::has_facet<std::messages<char>>(*this);
    CASE (hasFacet, messagesw_id):
      return std::has_facet<std::messages<wchar_t>>(*this);
    CASE (hasFacet, messages_byname_id):
      return std::has_facet<std::messages_byname<char>>(*this);
    CASE (hasFacet, messagesw_byname_id):
      return std::has_facet<std::messages_byname<wchar_t>>(*this);

    CASE (hasFacet, user_facet_id):
      return std::has_facet<user_facet<char>>(*this);
    CASE (hasFacet, user_facetw_id):
      return std::has_facet<user_facet<wchar_t>>(*this);

    DEFAULT(hasFacet):
      return false;
  END_SWITCH(hasFacet)
  return false;
}

bool oxLocale::getFacet(Facet& facet, size_t facetIndex) const {
  DEFINE_FACET_SWITCH(getFacet, 0)

  SWITCH(getFacet, facetIndex)
    CASE (getFacet, null_facet_id):
      return facet.setFacet(nullptr);

    CASE (getFacet, num_get_id):
      return facet.setFacet(&std::use_facet<std::num_get<char>>(*this));
    CASE (getFacet, num_getw_id):
      return facet.setFacet(&std::use_facet<std::num_get<wchar_t>>(*this));
    CASE (getFacet, num_put_id):
      return facet.setFacet(&std::use_facet<std::num_put<char>>(*this));
    CASE (getFacet, num_putw_id):
      return facet.setFacet(&std::use_facet<std::num_put<wchar_t>>(*this));
    CASE (getFacet, numpunct_id):
      return facet.setFacet(&std::use_facet<std::numpunct<char>>(*this));
    CASE (getFacet, numpunctw_id):
      return facet.setFacet(&std::use_facet<std::numpunct<wchar_t>>(*this));
    CASE (getFacet, numpunct_byname_id):
      return facet.setFacet(&std::use_facet<std::numpunct_byname<char>>(*this));
    CASE (getFacet, numpunctw_byname_id):
      return facet.setFacet(&std::use_facet<std::numpunct_byname<wchar_t>>(*this));

    CASE (getFacet, time_get_id):
      return facet.setFacet(&std::use_facet<std::time_get<char>>(*this));
    CASE (getFacet, time_getw_id):
      return facet.setFacet(&std::use_facet<std::time_get<wchar_t>>(*this));
    CASE (getFacet, time_put_id):
      return facet.setFacet(&std::use_facet<std::time_put<char>>(*this));
    CASE (getFacet, time_putw_id):
      return facet.setFacet(&std::use_facet<std::time_put<wchar_t>>(*this));
    CASE (getFacet, time_get_byname_id):
      return facet.setFacet(&std::use_facet<std::time_get_byname<char>>(*this));
    CASE (getFacet, time_getw_byname_id):
      return facet.setFacet(&std::use_facet<std::time_get_byname<wchar_t>>(*this));
    CASE (getFacet, time_put_byname_id):
      return facet.setFacet(&std::use_facet<std::time_put_byname<char>>(*this));
    CASE (getFacet, time_putw_byname_id):
      return facet.setFacet(&std::use_facet<std::time_put_byname<wchar_t>>(*this));

    CASE (getFacet, money_get_id):
      return facet.setFacet(&std::use_facet<std::money_get<char>>(*this));
    CASE (getFacet, money_getw_id):
      return facet.setFacet(&std::use_facet<std::money_get<wchar_t>>(*this));
    CASE (getFacet, money_put_id):
      return facet.setFacet(&std::use_facet<std::money_put<char>>(*this));
    CASE (getFacet, money_putw_id):
      return facet.setFacet(&std::use_facet<std::money_put<wchar_t>>(*this));
    CASE (getFacet, moneypunct_id):
      return facet.setFacet(&std::use_facet<std::moneypunct<char>>(*this));
    CASE (getFacet, moneypunctw_id):
      return facet.setFacet(&std::use_facet<std::moneypunct<wchar_t>>(*this));
    CASE (getFacet, moneypuncti_id):
      return facet.setFacet(&std::use_facet<std::moneypunct<char, true>>(*this));
    CASE (getFacet, moneypunctiw_id):
      return facet.setFacet(&std::use_facet<std::moneypunct<wchar_t, true>>(*this));
    CASE (getFacet, moneypunct_byname_id):
      return facet.setFacet(&std::use_facet<std::moneypunct_byname<char>>(*this));
    CASE (getFacet, moneypunctw_byname_id):
      return facet.setFacet(&std::use_facet<std::moneypunct_byname<wchar_t>>(*this));
    CASE (getFacet, moneypuncti_byname_id):
      return facet.setFacet(&std::use_facet<std::moneypunct_byname<char, true>>(*this));
    CASE (getFacet, moneypunctiw_byname_id):
      return facet.setFacet(&std::use_facet<std::moneypunct_byname<wchar_t, true>>(*this));

    CASE (getFacet, ctype_id):
      return facet.setFacet(&std::use_facet<std::ctype<char>>(*this));
    CASE (getFacet, ctypew_id):
      return facet.setFacet(&std::use_facet<std::ctype<wchar_t>>(*this));
    CASE (getFacet, ctype_byname_id):
      return facet.setFacet(&std::use_facet<std::ctype_byname<char>>(*this));
    CASE (getFacet, ctypew_byname_id):
      return facet.setFacet(&std::use_facet<std::ctype_byname<wchar_t>>(*this));
    CASE (getFacet, codecvt_id):
      return facet.setFacet(&std::use_facet<std::codecvt<char, char, std::mbstate_t>>(*this));
    CASE (getFacet, codecvtw_id):
      return facet.setFacet(&std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(*this));
    CASE (getFacet, codecvt_byname_id):
      return facet.setFacet(&std::use_facet<std::codecvt_byname<char, char, std::mbstate_t>>(*this));
    CASE (getFacet, codecvtw_byname_id):
      return facet.setFacet(&std::use_facet<std::codecvt_byname<wchar_t, char, std::mbstate_t>>(*this));

    CASE (getFacet, collate_id):
      return facet.setFacet(&std::use_facet<std::collate<char>>(*this));
    CASE (getFacet, collatew_id):
      return facet.setFacet(&std::use_facet<std::collate<wchar_t>>(*this));
    CASE (getFacet, collate_byname_id):
      return facet.setFacet(&std::use_facet<std::collate_byname<char>>(*this));
    CASE (getFacet, collatew_byname_id):
      return facet.setFacet(&std::use_facet<std::collate_byname<wchar_t>>(*this));

    CASE (getFacet, messages_id):
      return facet.setFacet(&std::use_facet<std::messages<char>>(*this));
    CASE (getFacet, messagesw_id):
      return facet.setFacet(&std::use_facet<std::messages<wchar_t>>(*this));
    CASE (getFacet, messages_byname_id):
      return facet.setFacet(&std::use_facet<std::messages_byname<char>>(*this));
    CASE (getFacet, messagesw_byname_id):
      return facet.setFacet(&std::use_facet<std::messages_byname<wchar_t>>(*this));

    CASE (getFacet, user_facet_id):
      return facet.setFacet(&std::use_facet<user_facet<char>>(*this));
    CASE (getFacet, user_facetw_id):
      return facet.setFacet(&std::use_facet<user_facet<wchar_t>>(*this));
  END_SWITCH(getFacet)
  return false;
}

//::::::::::::::::::::::::::::   locale object   ::::::::::::::::::::::::::::://

OM_BEGIN_METHOD_TABLE(oxLocale, OInstance)
  PUBLIC (oxLocale::ommValue,          "value:")
  PUBLIC (oxLocale::ommValue,          "name:")
  PUBLIC (oxLocale::ommValueExceptCat, "value:except:category:")
  PUBLIC (oxLocale::ommGetName,        "name")
  PUBLIC (oxLocale::ommHasFacet,       "hasFacet:")
  PUBLIC (oxLocale::ommUseFacet,       "useFacet:")
  PUBLIC (oxLocale::ommEq,             "eq:")
  PUBLIC (oxLocale::ommNe,             "ne:")
  PUBLIC (oxLocale::ommCompare,        "compare:with:")
  PUBLIC (oxLocale::ommGlobal,         "global")
  PUBLIC (oxLocale::ommIsCharType,     "is:type:")
  PUBLIC (oxLocale::ommToUpper,        "toUpper:")
  PUBLIC (oxLocale::ommToLower,        "toLower:")
  PUBLIC (oxLocale::ommToWide,         "toWide:")
  PUBLIC (oxLocale::ommToNarrow,       "toNarrow:")
OM_END_METHOD_TABLE()

OM_DEFINE_MCLASS(Locale);

OM_IMPLEMENT_CLASSID_INSTALLER(Locale, CLSID_LOCALE)

inline OClass* localeClass() noexcept {
  extern OClass* _OXType_class[];
  return _OXType_class[CLSID_LOCALE];
}

//:::::::::::::::::::::   Constructors - Initializers   :::::::::::::::::::::://

//  locale() noexcept;

//  locale(const locale& other) noexcept;
//  explicit locale(const std::string& std_name);
ORet oxLocale::ommValue(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::value:", 1);
  try {
    if (OM_ARG_TYPEID(0, CLSID_STRING)) {
      OM_UNPACK_CONST_REF(String, stdName, 0);
      *static_cast<locale*>(this) = locale(stdName);
    }
    else if (OM_ARG_TYPEID(0, CLSID_LOCALE)) {
      OM_UNPACK_CONST_REF(Locale, other, 0);
      *static_cast<locale*>(this) = locale(other);
    }
    else {
      OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
    }
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE("std::runtime_error: "+string{e.what()});
  }
  catch (...) {
    OM_THROW_RTE("error creating locale");
  }
  return makeResult();
}

//  locale(const locale& other, const std::string& std_name, category cats);
//  locale(const locale& other, const locale& one, category cats);
ORet oxLocale::ommValueExceptCat(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::value:except:category:", 3);
  OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
  OM_UNPACK_CONST_REF(Locale, other, 0);
  const category cats = g_castValue<category>(argv + 2, m);
  OM_CHECK_EXCEPTION();
  try {
    if (OM_ARG_TYPEID(1, CLSID_STRING)) {
      OM_UNPACK_CONST_REF(String, stdName, 1);
      *static_cast<locale*>(this) = locale(other, stdName, cats);
    }
    else if (OM_ARG_TYPEID(1, CLSID_LOCALE)) {
      OM_UNPACK_CONST_REF(Locale, one, 1);
      *static_cast<locale*>(this) = locale(other, one, cats);
    }
    else {
      OM_CHECK_ARG_TYPEID(1, CLSID_LOCALE);
    }
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE(string{"std::runtime_error: "}+e.what());
  }
  catch (...) {
    OM_THROW_RTE("error creating locale");
  }
  return makeResult();
}

// template<typename _Facet>
//    locale(const locale& other, _Facet* f);
ORet oxLocale::ommValueFacet(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::value:facet:", 2);
  OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
  OM_UNPACK_CONST_REF(Locale, other, 0);
  OM_CHECK_ARG_TYPEID(1, CLSID_FACET);
  OM_UNPACK_REF(Facet, facet, 1);
  struct ClearFacet {
    AnyFacet& anyFacet;
    ClearFacet(AnyFacet& anyFacet) : anyFacet{anyFacet} {}
    ~ClearFacet() {anyFacet = nullptr;}
  };
  ClearFacet clearFacet{facet};
  try {
    *static_cast<locale*>(this) = combineWith(other, facet);
  }
  catch (const string& msg) {
    OM_THROW_RTE(msg)
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE(string{"std::runtime_error: "}+e.what());
  }
  catch (...) {
    OM_THROW_RTE("error creating locale");
  }
  return makeResult();
}

//  const locale& operator=(const locale& other) throw();

//::::::::::::::::::::::::::   locale operations   ::::::::::::::::::::::::::://

// template<typename _Facet>
//    locale combine(const locale& other) const;
ORet oxLocale::ommCombine(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::combine:facetType:", 2);
  // unpack arguments
  OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
  OM_UNPACK_CONST_REF(Locale, other, 0);
  OM_CHECK_EXCEPTION();
  ssize_t typeIndex = -1;
  if (OM_ARG_TYPEID(1, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 1);
    auto& facetNames = Facet::facetNames;
    auto it = std::find(facetNames.begin(), facetNames.end(), facetName);
    if (it != facetNames.end())
      typeIndex = std::distance(facetNames.begin(), it);
  }
  else {
    typeIndex = g_castValue<ssize_t>(argv + 1, m);
    OM_CHECK_EXCEPTION();
  }
  // check facet not found
  if (typeIndex == -1 && OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    OM_THROW_RTE("there is no facet '"+facetName+"'")
  }
  // create locale object
  ORef o = localeClass()->createObject(m);
  OM_CHECK_EXCEPTION();
  try {
    auto* p = static_cast<oxLocale*>(o);
    *static_cast<Locale*>(p) = combineWith(other, typeIndex);
  }
  catch (const string& msg) {
    OM_THROW_RTE(msg)
  }
  catch (const std::runtime_error& e) {
    OM_THROW_RTE(string{"std::runtime_error: "}+e.what());
  }
  catch (...) {
    OM_THROW_RTE("error creating locale");
  }
  // return result
  return o->disposableResult();
}

// template<typename Facet>
//   bool has_facet(const locale& loc) noexcept;
ORet oxLocale::ommHasFacet(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::hasFacet:", 1);
  ssize_t typeIndex = -1;
  // unpack arguments
  if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    const auto& facetNames = Facet::facetNames;
    auto it = std::find(facetNames.begin(), facetNames.end(), facetName);
    if (it != facetNames.end())
      typeIndex = std::distance(facetNames.begin(), it);
  }
  else {
    typeIndex = g_castValue<ssize_t>(argv, m);
    OM_CHECK_EXCEPTION();
    if (typeIndex < 0 || typeIndex > user_facetw_id)
      OM_THROW_RTE("incorrect facet id: "+std::to_string(typeIndex))
  }
  // check facet not found
  if (typeIndex == -1 && OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    OM_THROW_RTE("there is no facet '"+facetName+"'")
  }
  // return value
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  OM_CHECK_EXCEPTION();
  _P(bool, r)->setValue(hasFacet(typeIndex));
  return r->disposableResult();
}

// template<typename Facet>
//   const Facet& use_facet(const std::locale& loc);
ORet oxLocale::ommUseFacet(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::useFacet:", 1);
  ssize_t typeIndex = -1;
  // unpack arguments
  if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    const auto& facetNames = Facet::facetNames;
    auto it = std::find(facetNames.begin(), facetNames.end(), facetName);
    if (it != facetNames.end())
      typeIndex = std::distance(facetNames.begin(), it);
  }
  else {
    typeIndex = g_castValue<ssize_t>(argv, m);
    OM_CHECK_EXCEPTION();
    if (typeIndex < 0 || typeIndex > user_facetw_id)
      OM_THROW_RTE("incorrect facet id: "+std::to_string(typeIndex))
  }
  // check facet not found
  if (typeIndex == -1 && OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, facetName, 0);
    OM_THROW_RTE("there is no facet '"+facetName+"'")
  }
  // return value
  ORef r = facetClass()->createObject(m);
  OM_CHECK_EXCEPTION();
  Facet& facet = OM_VALUE(Facet, r);
  try {
#ifdef __DEBUG__
    ObjectAutoCleaner cleaner{m, r};
    const bool success = getFacet(facet, typeIndex);
    if (!success)
      OM_THROW_RTE("the facet is already defined");
    cleaner.cancelOne();
#else // !__DEBUG__
    getFacet(facet, typeIndex);
#endif // !__DEBUG__
  }
  catch(std::bad_cast& e) {
    OM_THROW_RTE("std::bad_cast: "+string{e.what()});
  }
  return r->disposableResult();
}

//  string name() const;
ORet oxLocale::ommGetName(OArg rec, int argc, OArg *argv, Session *m) {
  string localeName{name()};
  ORef r = BTI::getTypeClass<string>()->createObject(m);
  OM_RETHROW("locale::name");
  OM_VALUE(String, r) = std::move(localeName); //name().c_str();
  return r->disposableResult();
}

//  bool operator==(const locale& __other) const throw ();
ORet oxLocale::ommEq(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::eq:", 1);
  OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
  OM_UNPACK_REF(Locale, loc, 0);
  const bool b = *static_cast<Locale*>(this) == loc;
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  OM_CHECK_EXCEPTION();
  _P(bool, r)->setValue(b);
  return r->disposableResult();
}

//  inline bool operator!=(const locale& __other) const throw ()
ORet oxLocale::ommNe(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::ne:", 1);
  OM_CHECK_ARG_TYPEID(0, CLSID_LOCALE);
  OM_UNPACK_REF(Locale, loc, 0);
  const bool b = *static_cast<Locale*>(this) != loc;
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  OM_CHECK_EXCEPTION();
  _P(bool, r)->setValue(b);
  return r->disposableResult();
}

//  template<typename _Char, typename _Traits, typename _Alloc>
//  bool operator()(
//    const basic_string<_Char, _Traits, _Alloc>& __s1,
//    const basic_string<_Char, _Traits, _Alloc>& __s2) const;
ORet oxLocale::ommCompare(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::compare:with:", 2);
  OM_CHECK_ARG_TYPEID(0, CLSID_STRING);
  OM_CHECK_ARG_TYPEID(1, CLSID_STRING);
  OM_UNPACK_REF(String, s1, 0);
  OM_UNPACK_REF(String, s2, 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  OM_CHECK_EXCEPTION();
  _P(bool, r)->setValue((*this)(s1, s2));
  return r->disposableResult();
}

//  static locale global(const locale&);
ORet oxLocale::ommGlobal(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::global", 1);
  ORef r = localeClass()->createObject(m);
  OM_CHECK_EXCEPTION();
  OM_VALUE(Locale, r) = global(*this);
  return r->disposableResult();
}

// template< class CharT >
//   bool isspace( CharT ch, const locale& loc );
//   bool isblank( CharT ch, const locale& loc );
//   bool iscntrl( CharT ch, const locale& loc );
//   bool isupper( CharT ch, const locale& loc );
//   bool islower( CharT ch, const locale& loc );
//   bool isalpha( CharT ch, const locale& loc );
//   bool isdigit( CharT ch, const locale& loc );
//   bool ispunct( CharT ch, const locale& loc );
//   bool isxdigit( CharT ch, const locale& loc );
//   bool isalnum( CharT ch, const locale& loc );
//   bool isprint( CharT ch, const locale& loc );
//   bool isgraph( CharT ch, const locale& loc );
template<typename CharT>
bool oxLocale::isCharType(CharT ch, const std::string& type) const {
  if (type == "space") return isspace(ch, *this);
  else if (type == "blank" ) return isblank (ch, *this);
  else if (type == "cntrl" ) return iscntrl (ch, *this);
  else if (type == "upper" ) return isupper (ch, *this);
  else if (type == "lower" ) return islower (ch, *this);
  else if (type == "alpha" ) return isalpha (ch, *this);
  else if (type == "digit" ) return isdigit (ch, *this);
  else if (type == "punct" ) return ispunct (ch, *this);
  else if (type == "xdigit") return isxdigit(ch, *this);
  else if (type == "alnum" ) return isalnum (ch, *this);
  else if (type == "print" ) return isprint (ch, *this);
  else if (type == "graph" ) return isgraph (ch, *this);
  throw string{"unknown character category"};
}

ORet oxLocale::ommIsCharType(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::is:type:", 2);
  bool result = false;
  // unpack arguments
  OM_CHECK_ARG_TYPEID(1, CLSID_STRING);
  OM_UNPACK_CONST_REF(String, charCategory, 1);
  try {
    if (OM_ARG_TYPEID(0, CLSID_CHAR)) {
      const char ch = _P(char, OM_ARG(0))->getValue();
      result = isCharType(ch, charCategory);
    }
    else if (OM_ARG_TYPEID(0, CLSID_UINT)) {
      const wchar_t wch = _P(uint, OM_ARG(0))->getValue();
      result = isCharType(wch, charCategory);
    }
    else
      return m->badArgType(_ox_method_name, 0, BTI::getTypeClass<char>(), argv);
  }
  catch (const string& msg) {
    OM_THROW_RTE(msg);
  }
  // return result
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  OM_CHECK_EXCEPTION();
  _P(bool, r)->setValue(result);
  return r->disposableResult();
}

// template< class CharT >
//   CharT toupper( CharT ch, const locale& loc );
ORet oxLocale::ommToUpper(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::toUpper:", 1);
  string upperCase;
  if (OM_ARG_TYPEID(0, CLSID_CHAR)) {
    const char ch = _P(char, OM_ARG(0))->getValue();
    try {
      upperCase = toUpper(string(1, ch));
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<char>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(char, r)->setValue(upperCase[0]);
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_UINT)) {
    const wchar_t wch = _P(uint, OM_ARG(0))->getValue();
    ORef r = BTI::getTypeClass<uint>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(uint, r)->setValue(std::toupper(wch, *this));
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, text, 0);
    try {
      upperCase = toUpper(text);
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<string>()->createObject(m);
    OM_CHECK_EXCEPTION();
    OM_VALUE(String, r) = upperCase;
    return r->disposableResult();
  }
  return m->badArgType(_ox_method_name, 0, BTI::getTypeClass<string>(), argv);
}

// template< class CharT >
//   CharT tolower( CharT ch, const locale& loc );
ORet oxLocale::ommToLower(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::toLower:", 1);
  string lowerCase;
  if (OM_ARG_TYPEID(0, CLSID_CHAR)) {
    const char ch = _P(char, OM_ARG(0))->getValue();
    try {
      lowerCase = toLower(string(1, ch));
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<char>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(char, r)->setValue(lowerCase[0]);
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_UINT)) {
    const wchar_t wch = _P(uint, OM_ARG(0))->getValue();
    ORef r = BTI::getTypeClass<uint>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(uint, r)->setValue(std::tolower(wch, *this));
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, text, 0);
    try {
      lowerCase = toLower(text);
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<string>()->createObject(m);
    OM_CHECK_EXCEPTION();
    OM_VALUE(String, r) = lowerCase;
    return r->disposableResult();
  }
  return m->badArgType(_ox_method_name, 0, BTI::getTypeClass<string>(), argv);
}

inline OClass* wstringClass() noexcept {
  extern OClass* _OXType_class[];
  return _OXType_class[CLSID_WSTRING];
}

ORet oxLocale::ommToNarrow(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::toNarrow:", 1);
  string narrowText;
  if (OM_ARG_TYPEID(0, CLSID_UINT)) {
    const uint wch = _P(uint, OM_ARG(0))->getValue();
    try {
      narrowText = toNarrow(std::wstring(1, wch));
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    if (narrowText.size() > 1)
      OM_THROW_RTE("cannot narrow character");
    ORef r = BTI::getTypeClass<char>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(char, r)->setValue(narrowText[0]);
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_WSTRING)) {
    OM_UNPACK_CONST_REF(WString, wideText, 0);
    try {
      narrowText = toNarrow(wideText);
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<string>()->createObject(m);
    OM_CHECK_EXCEPTION();
    OM_VALUE(String, r) = narrowText;
    return r->disposableResult();
  }
  return m->badArgType(_ox_method_name, 0, wstringClass(), argv);
}

ORet oxLocale::ommToWide(OArg rec, int argc, OArg *argv, Session *m) {
  OM_ENTER("locale::toWide:", 1);
  std::wstring wideText;
  if (OM_ARG_TYPEID(0, CLSID_CHAR)) {
    const char ch = _P(char, OM_ARG(0))->getValue();
    try {
      wideText = toWide(string(1, ch));
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = BTI::getTypeClass<uint>()->createObject(m);
    OM_CHECK_EXCEPTION();
    _P(uint, r)->setValue(wideText[0]);
    return r->disposableResult();
  }
  else if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    OM_UNPACK_CONST_REF(String, text, 0);
    try {
      wideText = toWide(text);
    }
    catch (const string& msg) {
      OM_THROW_RTE(msg);
    }
    ORef r = wstringClass()->createObject(m);
    OM_CHECK_EXCEPTION();
    OM_VALUE(WString, r) = wideText;
    return r->disposableResult();
  }
  return m->badArgType(_ox_method_name, 0, BTI::getTypeClass<string>(), argv);
}
