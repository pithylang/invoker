/*====================================================================
 ObjectInvoker Compiler Grammar
 (c) A.Faliagas 1996 - 2004, 2021

 Version: 00.50
 Revised: 15-Ian-2005
 Version: 00.98
 Revised: 20-Nov-2024

 i.y contains 0 shift/reduce conflicts

 Compile with:
    bison -W src/bison/i.y
 ====================================================================*/

/* %skeleton "lalr1.cc" */
%require "3.5.1"
%language "C++"
%output "src/iparser.cpp"
%defines "include/iparser.hpp"

%define api.token.constructor
%define api.value.type variant
%define api.location.file "../include/location.hpp"
%define parse.assert

%code requires {
#include "itypes.hpp"
class IParser;
}

// The parsing context.
%param { IParser& parseServices }

%locations

%define parse.trace
%define parse.error verbose

%code {
#include "itypes.hpp"
#include "selectors.h"
#include "iparseTools.hpp"

#define S_ext "extend:"
#define S_extBy "extendBy:"
}

%define api.token.prefix {TOK_}
%token
  END  0  "end of file"
  DOTS    ".."
  ELLIPS  "..."
  FWD     "=>"
  REFSET  ":="
  CREFSET "::="
  EVAL    "@"
  COMMA   ","
  BXOREQ  "^="
  BOREQ   "|="
  BANDEQ  "&="
  SHLEQ   "<<="
  SHREQ   ">>="
  PLUSEQ  "+="
  MINUSEQ "-="
  MODEQ   "%="
  DIVEQ   "/="
  MULEQ   "*="
  COPY    "="
  CCOPY   "@="
  QMARK   "?"
  OR      "||"
  XOR     "^^"
  AND     "&&"
  BOR     "|"
  BXOR    "^"
  BAND    "&"
  DIF     "!="
  EQ      "=="
  GE      ">="
  GT      ">"
  LE      "<="
  LT      "<"
  SHR     ">>"
  SHL     "<<"
  PLUS    "+"
  MINUS   "-"
  MUL     "*"
  DIV     "/"
  MOD     "%"
  EXP     "**"
  UPLUS
  UMINUS
  BNOT    "~"
  LNOT    "!"
  DOT     "."
  RPLUSPLUS
  RMINUSMINUS
  PLUSPLUS    "++"
  MINUSMINUS  "--"
  XOREQ       "^^="
  ANDEQ       "&&="
  OREQ        "||="
  LP      "("
  RP      ")"
  LB      "["
  RB      "]"
  LCB     "{"
  RCB     "}"
  RTI     ">}"
  COL     ":"
  DCOL    "::"
  SEMI    ";"
;

%token<ISymbol> ID      "identifier"
%token<ISymbol> SELID   "selector"
%token<ISymbol> ULONG   "ulong"
%token<ISymbol> LONG    "long"
%token<ISymbol> UINT    "uint"
%token<ISymbol> INT     "int"
%token<ISymbol> SHORT   "short"
%token<ISymbol> CHAR    "char"
%token<ISymbol> BOOL    "bool"
%token<ISymbol> FLT     "float"
%token<ISymbol> DBL     "double"
%token<ISymbol> STRING  "string"

%token<IStringWithAttrs> STRWATTR "string with attributes"

%nterm<std::vector<IUnit>> pgm
%nterm<std::vector<IUnit>> stlist
%nterm<IBlock> block
%nterm<ISymbol> lit
%nterm<IItem> item
%nterm<IItem> itemi
%nterm<IItem> itemac
%nterm<IUnit> unit
%nterm<IUnit> stm
%nterm<IUnit> expr
%nterm<std::vector<IUnit>> dmexpr  // decision making expression
%nterm<std::vector<IUnit>> cexpr   // comma-separated expression
%nterm<IReceivable> recbl
%nterm<IReceivable> jsond
%nterm<ISymbol> sel
%nterm<ISymbol> ssel

%printer { yyo << $$; } <*>;

%%

%start pgm;
pgm   : %empty        { /* pgm <-- %empty */; }
      | stlist        { parseServices.parsedProgram = std::move($1); }
      ;
block : "{" stlist "}" { $$ = std::move($2); }
      ;
stlist: stm           { if (!$1.isNull()) $$.push_back(std::move($1)); }
      | stlist stm    { $$ = std::move($1);
                        if (!$2.isNull()) $$.push_back(std::move($2)); }
      ;
stm   : ";"           { /* stm: ";" */ }
      | unit ";"      { if ($1.isEncapsulatedBlock()) {
                          $1.receiver.get<IBlock>().evaluate = false;
                          $1.receiver.get<IBlock>().execute = true;
                        }
                        $1.loc = @1;
                        $$ = std::move($1); }
      ;
unit  : expr          { $$ = std::move($1); }
      | item recbl    { $1.throwIfBlock(@1);
                        $$ = IUnit{std::move($1), std::move($2)}; }
      | "@" item recbl{ IUnit u{}; u.makeBlockInvocation(std::move($2), @2);
                        $$ = IUnit{IItem{std::move(u)}, std::move($3)}; }
      | unit "=>" recbl
                      { $$ = IUnit(IItem(std::move($1)), std::move($3)); }
      | item "..." "+=" "{" jsond "}"
                      { $1.throwIfBlock(@1);
                        $5.renderArgList(ISymbol{S_extBy, SYMTYPE_LITERAL_SEL});
                        $$ = IUnit{std::move($1), std::move($5)}; }
      ;
item  : "."           { $$ = ISymbol("this", SYMTYPE_LITERAL_ID); }
      | ID            { $$ = std::move($1); }
      | lit           { $$ = std::move($1); }
      | itemac        {}
      | itemi         { $$ = std::move($1); }
      | item "." ID   { $$.setMemberAccessExpression(std::move($1), std::move($3), @$); }
      | item "[" unit "]"
                      { $$.setSubscriptedExpression(std::move($1), std::move($3), @$); }
      | "[" cexpr "]" { $$ = IArrayExpression(std::move($2)); }
      | "[" cexpr "," "]"
                      { $$ = IArrayExpression(std::move($2)); }
      | "[" item "..." cexpr "]" // TODO
                      { $$.setArrayWithItemExpansion(std::move($4), std::move($2), @$, false); }
      | "[" cexpr "," item "..." "]" // TODO
                      { $$.setArrayWithItemExpansion(std::move($2), std::move($4), @$, true); }
      | "[" "]"       { $$ = IArrayExpression(); }
      | "[" "," "]"   { $$ = IArrayExpression(); }
      | "{" "}"       { $$ = IReceivable(); }
      | "{" jsond "}" { $$ = std::move($2); }
      | "{" cexpr "}" { $$ = ISetExpression(std::move($2)); }
      | "{" cexpr "," "}"
                      { $$ = ISetExpression(std::move($2)); }
      | "{" item "..." cexpr "}"
                      { $$.setSetWithItemExpansion(std::move($4), std::move($2), @$, false); }
      | "{" cexpr "," item "..." "}"
                      { $$.setSetWithItemExpansion(std::move($2), std::move($4), @$, true); }
      | "{" "," "}"   { $$ = ISetExpression(); }
      | "(" unit ")"  { if ($2.isEncapsulatedBlock())
                          $$ = std::move($2.receiver);
                        else
                          $$ = std::move($2); }
      | block         { $$ = std::move($1); }
      ;
itemi : STRWATTR      { $$.fromStringWithAttrs(parseServices,
                            std::move($1.name), $1.attrs, @1); }
      | STRWATTR "{" cexpr "}"
                      { $$.fromInitListExpr(parseServices,
                            std::move($1.name), $1.attrs, std::move($3), @1); }
      | STRWATTR "{" cexpr "," "}"
                      { $$.fromInitListExpr(parseServices,
                            std::move($1.name), $1.attrs, std::move($3), @1); }
      | STRWATTR "{" "<" cexpr RTI //">" "}"
                      { $$.fromTemplateInitExpr(parseServices,
                            std::move($1.name), $1.attrs, std::move($4), @1);}
      | STRWATTR "{" "<" cexpr "," RTI //">" "}"
                      { $$.fromTemplateInitExpr(parseServices,
                            std::move($1.name), $1.attrs, std::move($4), @1);}
      ;
itemac: ID "::" ID    { throw syntax_error(@1, "the symbol '"+$3.name+"' "
                            "cannot be accessed as '"+$1.name+"::"+$3.name+"' "
                            "in this context");}
      | ID "::" itemac
                      {}
      ;
lit   : ULONG         { $$ = std::move($1); }
      | LONG          { $$ = std::move($1); }
      | UINT          { $$ = std::move($1); }
      | INT           { $$ = std::move($1); }
      | SHORT         { $$ = std::move($1); }
      | CHAR          { $$ = std::move($1); }
      | BOOL          { $$ = std::move($1); }
      | FLT           { $$ = std::move($1); }
      | DBL           { $$ = std::move($1); }
      | STRING        { $$ = std::move($1); }
      ;
cexpr : unit          { $$.push_back(std::move($1)); }
      | cexpr "," cexpr
                      { $$.reserve($1.size() + $3.size());
                        $$ = std::move($1);
                        for (auto& i : $3)
                          $$.push_back(std::move(i)); }
      ;

%left  ",";
/* %precedence COLON; */
%right OREQ XOREQ ANDEQ;
%right BOREQ BXOREQ BANDEQ;
%right SHLEQ SHREQ;
%right MULEQ DIVEQ MODEQ;
%right PLUSEQ MINUSEQ;
%right "=";
%right "@=";
%nonassoc ":=";
%nonassoc "::=";
%precedence QMARK;
%left  "||";
%left  "^^";
%left  "&&";
%left  "|";
%left  "^";
%left  "&";
/* %nonassoc ":="; */
/* %nonassoc "::="; */
/* %precedence QMARK; */
%nonassoc "!=";
%nonassoc "==";
%nonassoc ">=";
%nonassoc ">";
%nonassoc "<=";
%nonassoc "<";
%left  "<<" ">>";
%left  "+" "-";
%left  "*" "/" "%";
%right "**";
/* %left  UPLUS; */
/* %left  UMINUS; */
/* %left  BNOT; */
/* %left  LNOT; */
/* %left  "."; */
/* %left  PLUSPLUS MINUSMINUS; */
%precedence UPLUS UMINUS;
%precedence BNOT LNOT;
%precedence PLUSPLUS MINUSMINUS;
/* %precedence EVAL; */

expr  : item          { $$ = std::move($1); }
      | expr "||=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_orBy, std::move($3)); }
      | expr "^^=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_xorBy, std::move($3)); }
      | expr "&&=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_andBy, std::move($3)); }
      | expr "|="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitorBy, std::move($3)); }
      | expr "^="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitxorBy, std::move($3)); }
      | expr "&="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitandBy, std::move($3)); }
      | expr ">>=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_shiftrBy, std::move($3)); }
      | expr "<<=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_shiftlBy, std::move($3)); }
      | expr "-="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_decBy, std::move($3)); }
      | expr "+="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_incBy, std::move($3)); }
      | expr "%="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_modBy, std::move($3)); }
      | expr "/="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_divBy, std::move($3)); }
      | expr "*="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_mulBy, std::move($3)); }
      | expr ":="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setReferenceOperation(std::move($1), std::move($3), @2, false); }
      | expr "::=" expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setReferenceOperation(std::move($1), std::move($3), @2, true); }
      | expr "="   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setAssignmentOperation(std::move($1), std::move($3), @2, false); }
      | expr "@="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setAssignmentOperation(std::move($1), std::move($3), @2, true); }
      | dmexpr ":" expr %prec COMMA
                      { $3.loc = @3;
                        $$.setConditionalOperation(
                            parseServices.inlineConditionalOperator(),
                            std::move($1[0]), std::move($1[1]), std::move($3), @1); }
      | "{" item "..." jsond "}"
                      { $2.throwIfBlock(@2);
                        $4.renderArgList(ISymbol{S_ext, SYMTYPE_LITERAL_SEL});
                        $$ = IUnit{std::move($2), std::move($4)}; }
      | expr "||"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S__or, std::move($3)); }
      | expr "^^"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S__xor, std::move($3)); }
      | expr "&&"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S__and, std::move($3)); }
      | expr "|"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitor, std::move($3)); }
      | expr "^"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitxor, std::move($3)); }
      | expr "&"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_bitand, std::move($3)); }
      | expr "!="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_ne, std::move($3)); }
      | expr "=="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_eq, std::move($3)); }
      | expr ">="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_ge, std::move($3)); }
      | expr ">"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_gt, std::move($3)); }
      | expr "<="  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_le, std::move($3)); }
      | expr "<"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_lt, std::move($3)); }
      | expr ">>"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_shiftr, std::move($3)); }
      | expr "<<"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_shiftl, std::move($3)); }
      | expr "-"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_sub, std::move($3)); }
      | expr "+"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_add, std::move($3)); }
      | expr "%"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_mod, std::move($3)); }
      | expr "/"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_div, std::move($3)); }
      | expr "*"   expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_mul, std::move($3)); }
      | expr "**"  expr
                      { $1.loc = @1; $3.loc = @3;
                        $$.setBinaryOperation(std::move($1), S_exp, std::move($3)); }
      | "+" expr %prec UPLUS
                      { $2.loc = @2;
                        $$.setUnaryOperation("+", std::move($2)); }
      | "-" expr %prec UMINUS
                      { $2.loc = @2;
                        $$.setUnaryOperation(S_neg, std::move($2)); }
      | "!" expr %prec LNOT
                      { $2.loc = @2;
                        $$.setUnaryOperation(S__not, std::move($2)); }
      | "~" expr %prec BNOT
                      { $2.loc = @2;
                        $$.setUnaryOperation(S_bitinv, std::move($2)); }
      | expr PLUSPLUS
                      { $1.loc = @1;
                        $$.setUnaryOperation(S_postinc, std::move($1)); }
      | expr MINUSMINUS
                      { $1.loc = @1;
                        $$.setUnaryOperation(S_postdec, std::move($1)); }
      | PLUSPLUS expr %prec UPLUS
                      { $2.loc = @2;
                        $$.setUnaryOperation(S_inc, std::move($2)); }
      | MINUSMINUS expr %prec UMINUS
                      { $2.loc = @2;
                        $$.setUnaryOperation(S_dec, std::move($2)); }
      | "@" item %prec UMINUS { $$.makeBlockInvocation(std::move($2), @2); }
      ;
recbl : ssel          { $$ = std::move($1); }
      | jsond         { $$ = std::move($1); }
      ;
jsond : sel expr      { $$ = IReceivable(std::move($1), /*TODO*/ IUnit(std::move($2))); }
      | jsond SELID expr
                      { $$ = std::move($1) +
                             IReceivable(std::move($2), IUnit(std::move($3))); }
      | jsond ":" expr
                      { ISymbol sym{":", SYMTYPE_LITERAL_SEL};
                        $$ = std::move($1) +
                             IReceivable(std::move(sym), IUnit(std::move($3))); }
      ;
ssel  : ID            { $$ = std::move($1); }
      | ID "::" ssel  { $$ = std::move($3); $$.name = $1.name+"::"+$$.name; }
      | STRING "::" ssel
                      { $$ = std::move($3); $$.name = $1.name+"::"+$$.name; }
      ;
sel   : SELID         { $$ = std::move($1); }
      | ID "::" sel   { $$ = std::move($3); $$.name = $1.name+"::"+$$.name; }
      | STRING "::" sel
                      { $$ = std::move($3); $$.name = $1.name+"::"+$$.name; }
      ;
dmexpr: expr "?" expr { $1.throwIfReceiverIsBlock(@1);
                        $$.reserve(2);
                        $$.emplace_back(std::move($1));
                        $$.emplace_back(std::move($3));
                        $$[0].loc = @1;
                        $$[1].loc = @3; }
      ;

%%

void yy::parser::error(const location_type& l, const std::string& msg) {
  if (parseServices.testingMode()) return;
  std::cerr << l << ": " << msg << '\n';
}
