#include "icodegen.hpp"
#include "iparseTools.hpp"
#include "mtypes.hpp"
#include "util.hpp"
#include "odefs.h"

using std::string;
using std::vector;

vector<string> split(const string& str, const string& delim);

void ICodeGenerator::encode(MOpcode opcode, MOperand operand) {
  auto& cs = currentBlock().codeSegment;
  if (operand == SYM_INDEX_SYSOBJECT && (opcode == push || opcode == pushrec))
    cs.push_back(MInstruction{pushso, opcode == pushrec ? 1 : 0});
  else if (opcode == push && isBlockSymbol(operand))
    cs.push_back(MInstruction{pushb, operand});
  else
    cs.push_back(MInstruction{opcode, operand});
}

bool ICodeGenerator::isSystemSymbol(const ISymbol& symbol) const noexcept {
  return symbol.name == parser.systemName && symbol.type == SYMTYPE_LITERAL_ID;
}

bool ICodeGenerator::isBlockSymbol(MOperand ofs) const noexcept {
  const ISymbol& symbol = currentBlock().dataSegment.localSymbolTable[ofs];
  return VAR_TYPE(symbol.type) == SYMTYPE_BLOCK;
}

MOperand ICodeGenerator::addSymbol(const ISymbol& symbol) {
  return isSystemSymbol(symbol) ? SYM_INDEX_SYSOBJECT :
      currentBlock().addSymbol(symbol);
}

MOperand ICodeGenerator::addSymbol(ISymbol&& symbol) {
  return isSystemSymbol(symbol) ? SYM_INDEX_SYSOBJECT :
      currentBlock().addSymbol(std::move(symbol));
}

MOperand ICodeGenerator::addBlock(int i) {
  return currentBlock().addSymbol(
      ISymbol('@'+std::to_string(i), SYMTYPE_BLOCK | SYMTYPE_STATIC));
}

MOperand ICodeGenerator::addTmp(int tmpId) {
  return currentBlock().addSymbol(
      ISymbol('#'+std::to_string(tmpId), SYMTYPE_VARIABLE));
}

MOperand ICodeGenerator::findTmpSymbol(int tmpId) const
{ return currentBlock().dataSegment.findSymbol('#'+std::to_string(tmpId)); }

//:::::::::::::::::::::::::::::::::  Block  :::::::::::::::::::::::::::::::::://

// item: block
// expr: item  (execute is false)
// ------------------------------
// unit: expr  (execute is true)
MOperand ICodeGenerator::generateCode(const IBlock& block, bool last) {
  auto& blockTableRef = blockTable.get();
  const int thisBlockIndex = (int) blockTableRef.size();
  // enter block
  blockTableRef.emplace_back();
  blockTableRef[thisBlockIndex].parentIndex = currentBlockIndex;
  blockTableRef[thisBlockIndex].classInheritanceTree.setRoot({"*", ""});
  currentBlockIndex = thisBlockIndex;
  currentBlock().evaluate = block.evaluate;
  currentBlock().execute = block.execute;
  currentBlock().type = MBlock::PLAIN;
  // add 'this' to local symbol table as argument reference
  currentBlock().dataSegment.addThisSymbol(false);
  // generate code inside the block
  encode(enter, 0);
  if (block.empty()) {
    encode(rop, zres);
    encode(leave);
  }
  else {
    // encode(rbc, thisBlockIndex);
    if (thisBlockIndex == 0) { // main block; add global symbols
      currentBlock().addSymbol(ISymbol("true", SYMTYPE_STATIC_VAR));
      currentBlock().addSymbol(ISymbol("false", SYMTYPE_STATIC_VAR));
      currentBlock().addSymbol(ISymbol("null", SYMTYPE_STATIC_VAR));
    }
    generateCodeStatements(block);
  }
  // finish block - set local variable offsets
  currentBlock().finalize();
  // leave block
  ASSERT(currentBlockIndex == thisBlockIndex);
  currentBlockIndex = currentBlock().parentIndex;
  if (currentBlockIndex == -1) return 0;
  // generate code to execute block from surrounding block or caller
  const auto childBlockIndex = thisBlockIndex;
  MOperand operand;
  if (block.evaluate || block.execute) {
    encode(execb, childBlockIndex);
    if (last) { // signal no result
      operand = SYM_INDEX_NO_RESULT;
    }
    else { // store result
      operand = addTmp();
      encode(stor, operand);
    }
  }
  else {
    operand = addBlock(childBlockIndex);
  }
  return operand;
}

void ICodeGenerator::generateCodeStatements(const IBlock& block) {
  ASSERT(!block.empty());

  // block classes registration indicator
  bool registeredClasses = false;
  const size_t context = tmpIdGenerator.beginContext();

  for (size_t i = 0; i < block.size() - 1; ++i) {
    const auto& unit = block[i];
    encode(setl, unit.loc.begin.line);
    generateCode(unit, &registeredClasses);
    for (size_t i = context; i < tmpIdGenerator.contextEnd(); ++i) {
      const int id = tmpIdGenerator.getContextItem(i);
      const auto operand = findTmpSymbol(id);
      ASSERT(operand >= 0);
      encode(dto, operand);
    }
    tmpIdGenerator.resetContext(context);
  }

  const auto& unit = block.back();
  encode(setl, unit.loc.begin.line);
  const auto lastSymbolIndex = generateCode(unit, &registeredClasses, true);
  if (unit.isItem() && lastSymbolIndex != SYM_INDEX_NO_RESULT) {
    encode(pushso, 1);
    encode(lres, lastSymbolIndex);
  }
  for (size_t i = context; i < tmpIdGenerator.contextEnd(); ++i) {
    const int id = tmpIdGenerator.getContextItem(i);
    const auto operand = findTmpSymbol(id);
    // if (operand != lastSymbolIndex)
      encode(dto, operand);
  }
  tmpIdGenerator.resetContext(context);
  encode(leave);
}

//::::::::::::::::::::::::::::::::::  Unit  :::::::::::::::::::::::::::::::::://

// unit -> stm
// [(] unit [)] -> item
// [SELID] expr -> jsond
//
// item [recbl] -> unit
// expr -> unit
// item -> expr -> unit
MOperand ICodeGenerator::generateCode(const IUnit& unit,
    bool* registeredClasses, bool last) {
  if (unit.maybeSpecialOperation()) {
    if (unit.isAssignmentOperation())
      return generateAssignmentOperation(unit, false);
    if (unit.isReferenceOperation())
      return generateMakeReferenceOperation(unit, false);
    if (unit.isConstReferenceOperation())
      return generateMakeReferenceOperation(unit, true);
    if (unit.isCopyOperation())
      return generateCopyOperation(unit);
    if (unit.isPropAccessOperation())
      return generatePropAccessOperation(unit, last);
    if (unit.isConditionalOperation())
      return generateConditionalOperator(unit, last);
    if (unit.isDecompAssignOperation())
      return generateDecompAssignOperation(unit, false);
    if (unit.isDecompRefOperation())
      return generateDecompRefOperation(unit, false);
    if (unit.isConstDecompRefOperation())
      return generateDecompRefOperation(unit, true);
    if (unit.isBlockInvocation())
      return generateBlockInvocation(unit, last);
    if (unit.isConstAssignOperation())
      return generateAssignmentOperation(unit, true);
    if (unit.isCDecompAssignOperation())
      return generateDecompAssignOperation(unit, true);
  }
  else if (unit.isBlockInvocationWithParams())
    return generateBlockInvocationParams(unit, last);
  else if (unit.isClassDeclaration(parser) &&
           isSystemSymbol(unit.receiver.get<ISymbol>())) {
    ASSERT(registeredClasses != nullptr);
    addClassDeclaration(unit.receivable);
    if (!*registeredClasses) {
      encode(rbc, 0);
      *registeredClasses = true;
    }
    return generateClassDefinition(unit); // TODO
  }

  const MOperand itemIndex = generateCode(unit.receiver, unit.isItem() && last);
  if (unit.isItem()) return itemIndex;
  // push receiver
  encode(pushrec, itemIndex);
  // push selector and args (receivable)
  generateCode(unit.receivable, itemIndex != SYM_INDEX_SYSOBJECT); // TODO check
  // execute
  encode(exec, static_cast<MOperand>(unit.receivable.args.size()));
  if (last) return -1;
  // store result
  const MOperand result = addTmp();
  encode(stor, result);
  // return position of result
  return result;
}

MOperand ICodeGenerator::generateClassDefinition(const IUnit& unit) {
  encode(pushso, 1);
  // convert simple non-literal arguments into literal strings
  for (auto& arg : unit.receivable.args) {
    if (arg.isItem() && arg.receiver.is<ISymbol>() &&
        !arg.receiver.get<ISymbol>().isLiteralValue()) {
      auto& r = const_cast<IUnit&>(arg).receiver;
      r.get<ISymbol>().type = SYMTYPE_LITERAL_STRING;
    }
  }
  generateCode(unit.receivable, true); // no VCM
  encode(exec, static_cast<MOperand>(unit.receivable.args.size()));
  return SYM_INDEX_SYSOBJECT;
}

MOperand ICodeGenerator::generateAssignmentOperation(const IUnit& unit,
                                                     bool isConst) {
  ASSERT(unit.receiver.is<ISymbol>());
  ASSERT(unit.receivable.args.size() == 1);
  ISymbol var{std::move(unit.receiver.get<ISymbol>())};
  var.type = SYMTYPE_VARIABLE;
  MOperand lhsOperand = generateCode(std::move(var));
  encode(pushrz, lhsOperand);
  MOperand rhsOperand = generateCode(unit.receivable.args[0]);
  encode(push, rhsOperand);
  if (isConst)
    encode(rop, scop);
  encode(setv, rhsOperand);
  return lhsOperand;
}

MOperand ICodeGenerator::generateDecompAssignOperation(const IUnit& unit,
                                                       bool isConst) {
  ASSERT(unit.receiver.is<IArrayExpression>());
  ASSERT(unit.receivable.args.size() == 1);
  const auto& a = unit.receiver.get<IArrayExpression>();
  MOperand rhs = generateCode(unit.receivable.args[0]);
  return generateDecompAssignOperation(a, rhs, isConst);
}

// Borrowed from include/location.hpp
static StringOutput& operator<<(StringOutput& sv, const yy::position& pos) {
  if (pos.filename) sv << *pos.filename << ':';
  return sv << pos.line << '.' << pos.column;
}

static StringOutput& operator<<(StringOutput& sv, const yy::location& loc) {
  auto end_col = 0 < loc.end.column ? loc.end.column - 1 : 0;
  sv << loc.begin;
  const string* const beginFile = loc.begin.filename;
  if (loc.end.filename && (!beginFile || *beginFile != *loc.end.filename))
    sv << '-' << *loc.end.filename << ':' << loc.end.line << '.' << end_col;
  else if (loc.begin.line < loc.end.line)
    sv << '-' << loc.end.line << '.' << end_col;
  else if (loc.begin.column < end_col)
    sv << '-' << end_col;
  return sv;
}

static StringOutput& operator<<(StringOutput&& sv, const yy::location& loc)
{ return sv << loc; }

MOperand ICodeGenerator::generateDecompAssignOperation(
    const IArrayExpression& a, MOperand rhs, bool isConst) {
  MOperand r{-1};
  for (size_t i = 0; i < a.size(); ++i) {
    bool simple = false;
    bool isArrayExpr = false;
    // similar to IUnit::setAssignmentOperation
    const auto& lhsItem = a[i].receiver;
    if (a[i].isItem()) {
      static const string msg{"assignment to "}, hint{" (did you mean '=='?)"};
      if (lhsItem.is<ISymbol>() && lhsItem.get<ISymbol>().isLiteralValue())
        throw msg+"literal"+hint;
      else if (lhsItem.is<ISymbol>())
        simple = true;
      else if (lhsItem.is<IBlock>() && !lhsItem.get<IBlock>().evaluate)
        throw msg+"code block"+hint;
      else if (lhsItem.is<IArrayExpression>())
        isArrayExpr = true;
      else if (isConst && lhsItem.isEncapsulatedUnit()) {
        auto loc = lhsItem.getEncapsulatedUnit().loc;
        string msg; StringOutput{msg} << loc << ": "+IUnit::assignError()
                                      << " in decomposition assignment";
        throw msg;
      }
    }
    // get rhs array item
    encode(pushrec, rhs);
    auto operand = generateCode(ISymbol{"getItem:", SYMTYPE_LITERAL_SEL});
    encode(pushsel, operand);
    operand = generateCode(ISymbol{std::to_string(i), SYMTYPE_LITERAL_INT});
    encode(push, operand);
    encode(exec, 1);
    MOperand rhsArrayElement = addTmp();
    encode(stor, rhsArrayElement);
    // encode assignment operation
    const MOperand lhs = isArrayExpr ? -1 : generateCode(a[i]);
    if (simple) {
      encode(pushrz, lhs);
      encode(push, rhsArrayElement);
      if (isConst) encode(rop, scop);
      encode(setv, rhsArrayElement);
    }
    else if (isArrayExpr) {
      const auto& lhsArrayElemExpr = lhsItem.get<IArrayExpression>();
      r = generateDecompAssignOperation(lhsArrayElemExpr,
          rhsArrayElement, isConst);
    }
    else {
      ASSERT(!isConst);
      encode(pushso, 1);
      operand = generateCode(ISymbol{"copy:from:", SYMTYPE_LITERAL_SEL});
      encode(pushsel, operand);
      encode(push, lhs);
      encode(push, rhsArrayElement);
      encode(exec, 2);
      r = addTmp();
      encode(stor, r);
    }
  }
  return r;
}

MOperand ICodeGenerator::generateCopyOperation(const IUnit& unit) {
  ASSERT(unit.receivable.args.size() == 1);
  encode(pushso, 1);
  // add selector to symbol table and encode instruction
  ISymbol selector("copy:from:", SYMTYPE_LITERAL_SEL);
  const MOperand operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // push args
  const MOperand lhs = generateCode(unit.receiver);
  encode(push, lhs);
  const MOperand rhs = generateCode(unit.receivable.args[0]);
  encode(push, rhs);
  encode(exec, 2);
  return lhs;
}

MOperand ICodeGenerator::generateMakeReferenceOperation(const IUnit& unit,
                                                        bool isConst) {
  ASSERT(unit.receivable.args.size() == 1);
  // here we generate code in reverse order, RHS first
  MOperand rhs = generateCode(unit.receivable.args[0]);
  encode(pushrec, rhs);
  const MOperand lhs = generateCode(unit.receiver);
  if (isConst)
    // encode(rop, lcru);
    encode(rop, scop);
  encode(mref, lhs);
  return lhs;
}

MOperand ICodeGenerator::generateDecompRefOperation(const IUnit& unit,
                                                    bool isConst) {
  ASSERT(unit.receiver.is<IArrayExpression>());
  ASSERT(unit.receivable.args.size() == 1);
  const auto& a = unit.receiver.get<IArrayExpression>();
  MOperand rhs = generateCode(unit.receivable.args[0]);
  encode(chkrv, rhs);
  return generateDecompRefOperation(a, rhs, isConst);
}

MOperand ICodeGenerator::generateDecompRefOperation(const IArrayExpression& a,
                                                    MOperand rhs,
                                                    bool isConst) {
  MOperand r{-1};
  for (size_t i = 0; i < a.size(); ++i) {
    bool isArrayExpr = false;
    const auto& lhsItem = a[i].receiver;
    if (a[i].isItem()) {
      static const string msg{" cannot be references (did you mean '==' ?)"};
      if (lhsItem.is<ISymbol>() && lhsItem.get<ISymbol>().isLiteralValue())
        throw "literals"+msg;
      else if (lhsItem.is<IBlock>() && !lhsItem.get<IBlock>().evaluate)
        throw "code blocks"+msg;
      else if (lhsItem.is<IArrayExpression>())
        isArrayExpr = true;
    }
    // get rhs array item
    encode(pushrec, rhs);
    auto operand = generateCode(ISymbol{"component:", SYMTYPE_LITERAL_SEL});
    encode(pushsel, operand);
    operand = generateCode(ISymbol{std::to_string(i), SYMTYPE_LITERAL_INT});
    encode(push, operand);
    encode(exec, 1);
    MOperand rhsArrayElement = addTmp();
    encode(stor, rhsArrayElement);
    // encode reference operation
    if (isArrayExpr) {
      const auto& lhsArrayElemExpr = lhsItem.get<IArrayExpression>();
      r = generateDecompRefOperation(lhsArrayElemExpr, rhsArrayElement, isConst);
    }
    else {
      // here we generate code in reverse order, RHS first
      encode(pushrec, rhsArrayElement);
      const MOperand lhs = generateCode(a[i]);
      if (isConst)
        // encode(rop, lcru);
        encode(rop, scop);
      encode(mref, lhs);
      r = lhs;
    }
  }
  return r;
}

MOperand ICodeGenerator::generatePropAccessOperation(const IUnit& unit,
                                                     bool last) {
  const MOperand itemIndex = generateCode(unit.receiver);
  // push receiver
  encode(pushrec, itemIndex);
  // push arg, no selector!
  const auto operand = generateCode(unit.receivable.args[0]);
  encode(push, operand);
  // execute
  encode(execgp, 1);
  if (last) return -1;
  // store result
  const MOperand result = addTmp();
  encode(stor, result);
  // return position of result
  return result;
}

MOperand ICodeGenerator::generateBlockInvocation(const IUnit& unit, bool last) {
  const MOperand itemIndex = generateCode(unit.receiver);
  encode(pushrec, itemIndex);
  // push selector
  ISymbol selector = ISymbol("lambda_", SYMTYPE_SELECTOR);
  const auto operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // execute block invocation
  encode(execlb, 0);
  if (last) return -1;
  // store result
  const MOperand result = addTmp();
  encode(stor, result);
  // return position of result
  return result;
}

MOperand ICodeGenerator::generateBlockInvocationParams(const IUnit& unit, bool last) {
  const auto& embeddedUnit = unit.receiver.getEncapsulatedUnit();
  const auto& paramSpec = unit.receivable.method.name;
  // get parameters
  vector<string> keys = unit.receivable.splitSelector(":");
  // validate parameters
  if (keys.empty())
    throw "incorrect lambda parameter specification: '"+paramSpec+"'";
  ASSERT(keys.back().empty());
  keys.pop_back();
  ASSERT(!keys.empty());
  vector<string> k = keys;
  std::sort(k.begin(), k.end());
  if (std::adjacent_find(k.begin(), k.end()) != k.end())
    throw "duplicate lambda parameter: '"+paramSpec+"'";
  // PART 1: set method parameters
  const MOperand itemIndex = generateCode(embeddedUnit.receiver);
  encode(pushrec, itemIndex);
  // push args
  for (auto& key : keys) {
    ISymbol sym(std::move(key), SYMTYPE_LITERAL_STRING);
    const auto operand = generateCode(std::move(sym));
    encode(push, operand);
  }
  // execute set block parameters
  encode(execlb, static_cast<MOperand>(keys.size()));
  // PART 2: proceed to block execution
  encode(pushrec, itemIndex);
  // push selector
  ISymbol selector = ISymbol(paramSpec.c_str(), SYMTYPE_SELECTOR);
  const auto operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // push args
  for (const auto& arg : unit.receivable.args) {
    const auto operand = generateCode(arg);
    encode(push, operand);
  }
  // execute block invocation
  encode(execlb, 0);
  if (last) return -1;
  // store result
  const MOperand result = addTmp();
  encode(stor, result);
  // return position of result
  return result;
}

MOperand ICodeGenerator::generateConditionalOperator(const IUnit& unit, bool last) {
  const MOperand itemIndex = generateCode(unit.receiver);
  encode(pushrec, itemIndex);
  // push args
  for (const auto& arg : unit.receivable.args) {
    const auto operand = generateCode(arg);
    encode(push, operand);
  }
  // execute block invocation
  encode(execcb, 0);
  if (last) return -1;
  // store result
  const MOperand result = addTmp();
  encode(stor, result);
  // return position of result
  return result;
}

void ICodeGenerator::addClassDeclaration(const IReceivable& recbl) {
  if (!recbl.args[0].isItem() || !recbl.args[0].receiver.is<ISymbol>())
    throw string("class names must be identifiers or strings");
  if (recbl.args.size() == 3 &&
      (!recbl.args[1].isItem() || !recbl.args[1].receiver.is<ISymbol>()))
    throw string("class names must be identifiers or strings");
  const string& className = recbl.args[0].receiver.get<ISymbol>().name;
  string baseClass = (recbl.args.size() == 2) ? string("object") :
      recbl.args[1].receiver.get<ISymbol>().name;
  auto& classTree = currentBlock().classInheritanceTree;
  const MBlock::InheritanceData base{baseClass, ""};
  auto* node = classTree.getNode(base) ?: classTree.getRoot();
  node->link(MBlock::InheritanceData{className, baseClass});
}

//::::::::::::::::::::::::::::::::::  Item  :::::::::::::::::::::::::::::::::://

// item -> expr -> unit -> stm
// item [recbl] -> unit -> stm
MOperand ICodeGenerator::generateCode(const IItem& item, bool last) {
  const IVariant& itemType = static_cast<const IVariant&>(item);
  return itemType.match(
    [](const INull&) {throw string("null item"); return -1;},
    [this](const ISymbol& symbol) {return generateCode(symbol);},
    [this](const std::unique_ptr<IUnit>& unit) {return generateCode(*unit);},
    [this](const IArrayExpression& v) {return generateCodeArrayDefinition(v);},
    [this](const ISetExpression& set) {return generateCodeSetDefinition(set);},
    [this](const IReceivable& recbl) {return generateCodeISONDefinition(recbl);},
    [this, last](const IBlock& block) {return generateCode(block, last);}
  );
}

//:::::::::::::::::::::::::::::::::  Symbol  ::::::::::::::::::::::::::::::::://

MOperand ICodeGenerator::generateCode(const ISymbol& symbol)
{ return generateCode(ISymbol(symbol)); }

MOperand ICodeGenerator::generateCode(ISymbol&& symbol) {
  if (isSystemSymbol(symbol)) return SYM_INDEX_SYSOBJECT;
  ISymbol var(std::move(symbol));
  MSymbol::s_convert(var);
  return addSymbol(std::move(var));
}

//:::::::::::::::::::::::::::::::  Receivable  ::::::::::::::::::::::::::::::://

void g_replaceAll(string& s, const string& from, const string& to);

// [item] recbl -> unit
void ICodeGenerator::generateCode(const IReceivable& recbl, bool noVCM) {
  auto origMethodName = recbl.method.name;
  g_replaceAll(origMethodName, "\x1f", ":");
  // Standardize method name
  auto methodName = recbl.args.empty() ? origMethodName :
                                         getStdMethodName(origMethodName);
  const auto creationMethod = parser.getVarCreateMethod(methodName);
  // Add selector to symbol table and encode instruction
  ISymbol selector{std::move(methodName), SYMTYPE_SELECTOR};
  const auto operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // Push args
  int argIndex = 0;
  for (const auto& arg : recbl.args) {
    if (arg.isVCMNotUsableAsArg(parser) || (arg.isItem() &&
          arg.receiver.isEncapsulatedUnit() &&
          arg.receiver.getEncapsulatedUnit().isVCMNotUsableAsArg(parser)))
      throw string{"improper use of variable creation method"};
    const auto operand = generateCode(arg);
    const auto operation = (!noVCM && creationMethod &&
        creationMethod->creationArg == argIndex &&
        arg.isIndividualItem() && arg.receiver.is<ISymbol>() &&
        arg.receiver.get<ISymbol>().isId()) ? pushsym : push;
    encode(operation, operand);
    ++argIndex;
  }
}

void g_replaceAll(string& s, const string& from, const string& to) {
  const size_t k = from.length(), l = to.length();
  for (size_t p = 0; (p = s.find(from, p)) != string::npos; p += l)
    s.replace(p, k, to);
}

static const string& s_stripExtraColonsAtEnd(const string& originalMethodName,
                                             string& methodName) {
  const auto pos = originalMethodName.find_last_not_of(":");
  if (pos == string::npos)
    throw "'"+originalMethodName+"' is not a proper method name";
  if (pos < originalMethodName.length() - 2) {
    methodName = originalMethodName.substr(0, pos + 2);
    return methodName;
  }
  return originalMethodName;
}

static string s_mergeVariadicKeyPairs(const string& methodName,
                                      const vector<string>& keys) {
  if (keys.size() < 4) return methodName;
  string repeatingPair = keys[keys.size() - 2]+":"+keys.back()+":";
  auto pos = methodName.find(repeatingPair);
  ASSERT(pos != string::npos);
  if (pos + repeatingPair.size() != methodName.size())
    return methodName.substr(0, pos + repeatingPair.size());
  if (keys.size() < 5) return methodName;
  repeatingPair = keys[keys.size() - 3]+":"+keys[keys.size() - 2]+":";
  pos = methodName.find(repeatingPair);
  if (pos + repeatingPair.size() + keys.back().size() + 1 != methodName.size())
    return methodName.substr(0, pos + repeatingPair.size()) + keys.back() +':';
  return methodName;
}

static string s_mergeVariadicKeys(const string& methodName) {
  vector<string> keys = split(methodName, ":");
  if (keys.empty())
    throw "'"+methodName+"' is not a proper method name";
  ASSERT(keys.back().empty());
  keys.pop_back();
  auto firstOccurence = std::adjacent_find(keys.begin(), keys.end());
  if (firstOccurence == keys.end())
    return s_mergeVariadicKeyPairs(methodName, keys);
  auto last = firstOccurence + 2;
  while (last != keys.end() && *last == *firstOccurence) ++last;
  if (last < keys.end() - 1)
    return s_mergeVariadicKeyPairs(methodName, keys);
  --last;
  auto back = std::move(keys.back());
  keys.pop_back();
  while (last-- != firstOccurence) keys.pop_back();
  keys.push_back(std::move(back));
  string result;
  std::for_each(keys.begin(), keys.end(), [&result](const string item) {
    result += item + ":";
  });
  return result;
}

/*static*/
string ICodeGenerator::getStdMethodName(const string& originalMethodName) {
#if 0
  // Split into class/namespace override part and name part
  const auto namePos = originalMethodName.find_last_of("::");

  // Strip extra colons at end; they correspond to variadic argument lists.
  const auto pos = originalMethodName.find_last_not_of(":");
  if (pos == string::npos)
    throw "'"+originalMethodName+"' is not a proper method name";
  string methodName = (pos < originalMethodName.length() - 2) ?
      originalMethodName.substr(0, pos + 2) : originalMethodName;
  // Merge consecutive identical keys at end or berore the last key
  vector<string> keys = split(methodName, ":");
  if (keys.empty())
    throw "'"+methodName+"' is not a proper method name";
  ASSERT(keys.back().empty());
  keys.pop_back();
  auto firstOccurence = std::adjacent_find(keys.begin(), keys.end());
  if (firstOccurence == keys.end()) return methodName;
  auto last = firstOccurence + 2;
  while (last != keys.end() && *last == *firstOccurence) ++last;
  if (last < keys.end() - 1) return methodName;
  --last;
  auto back = std::move(keys.back());
  keys.pop_back();
  while (last-- != firstOccurence) keys.pop_back();
  keys.push_back(std::move(back));
  string result;
  std::for_each(keys.begin(), keys.end(), [&result](const string item) {
    result += item + ":";
  });
  return result;
}
#endif // 0
  ASSERT(originalMethodName.back() == ':');
  // Stripped originalMethodName -- referenced by r
  string methodName;
  // Strip extra colons at end; they correspond to variadic argument lists.
  const auto& r = s_stripExtraColonsAtEnd(originalMethodName, methodName);
  // -- r points at either originalMethodName or methodName
  // Split into class/namespace override part and name part
  auto p = r.find("::");
  auto startOfName = p;
  while (p != string::npos) {
    startOfName = p + 2;
    p = r.find("::", startOfName);
  }

  if (startOfName == string::npos) return s_mergeVariadicKeys(r);

  // Merge consecutive identical keys at end or before the last key
  const auto prefix = r.substr(0, startOfName);
  // checkClassOverride(prefix);
  const auto name = r.substr(startOfName);
  return prefix + s_mergeVariadicKeys(name);
}

string g_getStdMethodName(const string& originalMethodName) {
  return ICodeGenerator::getStdMethodName(originalMethodName);
}

//:::::::::::::::::::::::::  ISON Object Definition  ::::::::::::::::::::::::://

// "{" jsond "}" -> item
// "{" "}" -> item
MOperand ICodeGenerator::generateCodeISONDefinition(const IReceivable& recbl) {
  // Create new selector name
  string name(recbl.args.empty() ? "createObject" : "createObject:");
  // Push receiver
  encode(pushso, 1);
  // Push receivable
  ISymbol selector{std::move(name), SYMTYPE_LITERAL_SEL};
  const auto operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // Push args (key-value pairs)
  vector<string> keys = recbl.splitSelector(":");
  ASSERT(!keys.empty());
  if (!keys.back().empty())
    throw string("improper ISON definition");
  ASSERT(keys.size() == recbl.args.size() + 1);
  vector<string>::size_type i = 0;
  for (const auto& arg : recbl.args) {
    // Add key argument
    string key = std::move(keys[i]);
    if (key.empty()) {
      if (!arg.isItem() || !arg.receiver.is<ISymbol>())
        throw string{"improper ISON definition"};
      else
        key = arg.receiver.get<ISymbol>().name;
    }
    g_replaceAll(key, "\x1f", ":");
    ISymbol keySymbol = ISymbol{std::move(key), SYMTYPE_LITERAL_STRING};
    const auto keyIndex = generateCode(std::move(keySymbol));
    encode(push, keyIndex);
    // Add value argument
    const auto operand = generateCode(arg);
    encode(push, operand);
    ++i;
  }
  // Execute statement
  encode(exec, 2*recbl.args.size());
  // Store result
  const MOperand result = addTmp();
  encode(stor, result);
  // Return position of result
  return result;
}

vector<string> split(const string& str, const string& delim) {
  constexpr auto EOS = string::npos;
  vector<string> result;
  if (delim.empty()) return result;
  string::size_type start = 0, end = 0;
  while (end != string::npos) {
    end = str.find(delim, start);
    result.push_back(str.substr(start, end == EOS ? EOS : end - start));
    start = end > (EOS - delim.size()) ? EOS : end + delim.size();
  }
  return result;
}

//:::::::::::::::::::::::::::::  Set Definition  ::::::::::::::::::::::::::::://

// "{" cexpr "}" -> item
// "{" cexpr "," "}" -> item
MOperand ICodeGenerator::generateCodeSetDefinition(const ISetExpression& cexpr) {
  // Create new selector name
  string name(cexpr.empty() ? "createSet" : "createSet:");
  // Encode new unit
  return generateCodeDefinition(cexpr, std::move(name));
}

//::::::::::::::::::::::::::::  Array Definition  :::::::::::::::::::::::::::://

// "[" cexpr "]" -> item
// "[" cexpr "," "]" -> item
// "[" "," "]" -> item
MOperand ICodeGenerator::generateCodeArrayDefinition(
    const IArrayExpression& cexpr) {
  // Create new selector name
  string name(cexpr.empty() ? "createArray" : "createArray:");
  // Encode new unit
  return generateCodeDefinition(cexpr, std::move(name));
}

//::::::::::::::::::::::::  Set Or Array Definition  ::::::::::::::::::::::::://

MOperand ICodeGenerator::generateCodeDefinition(const vector<IUnit>& cexpr,
                                                string&& name) {
  // Push receiver
  encode(pushso, 1);
  // Push receivable
  const ISymbol selector = ISymbol(std::move(name), SYMTYPE_LITERAL_SEL);
  const auto operand = generateCode(std::move(selector));
  encode(pushsel, operand);
  // Push args (single value pairs)
  for (const auto& expr : cexpr) {
    const auto operand = generateCode(expr);
    encode(push, operand);
  }
  // Execute statement
  encode(exec, cexpr.size());
  // Store result
  const MOperand result = addTmp();
  encode(stor, result);
  // Return position of result
  return result;
}

//:::::::::::::::::::::::::::  Code Disassembler  :::::::::::::::::::::::::::://

string ICodeGenerator::disassemble() const {
  const auto& blocks = blockTable.get();
  string code;
  for (int i = 0; i < (int) blockTable.get().size(); ++i) {
    if (i == 0) {
      code += ".Program\n";
    }
    else {
      const string type = (blocks[i].execute && blocks[i].evaluate) ? "???" :
          (!blocks[i].execute ? "LAMBDA" : "CODEBLOCK");
      const string howToExecute = (!blocks[i].execute && blocks[i].evaluate) ?
          " execute INPLACE" : "";
      code += ".Block " + std::to_string(i);
      code += " parent " + std::to_string(blocks[i].parentIndex) +
              " type " + type + howToExecute + "\n";
    }
    blocks[i].disassemble(code, 0);
    // code += "\n";
  }
  return code;
}

//:::::::::::::::::::::::::::   Package Compiler   ::::::::::::::::::::::::::://

int g_compilePackage(const string& fileName,
                     CompilePackageData& compilationData) noexcept {
  std::unique_ptr<char[]> theScript{g_readScript(fileName)};
  if (!theScript) {
    compilationData.message = "could not read file " + fileName;
    return COMPILE_PACKAGE_READ_FILE;
  }

  const ulong parseOptions = compilationData.parseOptions;
  const bool generateAssembly = compilationData.generateAssembly;
  const bool testingMode = compilationData.testingMode;
  const bool verbose = compilationData.verbose;
  const string& parseExceptionToIgnore = compilationData.ignoreParseExcept;

  const string packageScript = string{"{"}+theScript.get()+string{"};"};
  IParser parseServices(packageScript.data(), parseOptions);
  parseServices.location.initialize(&fileName);

  if (verbose)
    compilationData.sourceCode = theScript.get();

  yy::parser parser(parseServices);
  ICodeGenerator codeGenerator{parseServices, compilationData.blockTable};

  try {
    int rc = parser.parse();
    if (rc != 0) {
      compilationData.message = "parser failed with exit code "+
          std::to_string(rc);
      return COMPILE_PACKAGE_PARSER;
    }
    IBlock program = std::move(parseServices.parsedProgram);
    MOperand result = codeGenerator.generateCode(program, false/*immaterial*/);
    if (result != 0) {
      compilationData.message = "code generator returned "+
          std::to_string(result);
      return COMPILE_PACKAGE_CODE_GEN_FAIL;
    }
    if (generateAssembly)
      compilationData.assemblyCode = codeGenerator.disassemble();
  }
  catch (const string& msg) {
    if (testingMode && msg == parseExceptionToIgnore)
      exit(0);
    compilationData.message = msg;
    return COMPILE_PACKAGE_CODE_GEN; // 2; the parser returns 0 or 1
  }

  return COMPILE_PACKAGE_OK;
}
