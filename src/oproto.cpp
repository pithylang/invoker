#include "oproto.hpp"

using std::string;
using std::vector;

vector<string> split(const string& str, const string& delim);

bool ProtoParser::parse() {
  const string EMPTY;
  if (proto.find(" :") != string::npos || proto.find("\t:") != string::npos)
    return false;
  auto fragments = split(proto, ":");
  string curKey = EMPTY;
  for (size_t i = 0; i < fragments.size(); ++i) {
    auto fragmentTokens = parseFragment(fragments[i]);
    if (!fragmentTokens.success)
      return false;
    auto& tokens = fragmentTokens.tokens;
    if (i == 0) {
      if (tokens.size() != 1)
        return false;
      curKey = std::move(tokens[0]);
    }
    else if (i == fragments.size() - 1) {
      if (tokens.size() == 0)
        definitions.emplace_back(std::move(curKey), string(""));
      else if (tokens.size() == 1)
        definitions.emplace_back(std::move(curKey), std::move(tokens[0]));
      else
        return false;
    }
    else {
      if (tokens.size() == 1) {
        definitions.emplace_back(std::move(curKey), string(""));
        curKey = std::move(tokens[0]);
      }
      else if (tokens.size() == 2) {
        definitions.emplace_back(std::move(curKey), std::move(tokens[0]));
        curKey = std::move(tokens[1]);
      }
      else
        return false;
    }
  }
  if (!curKey.empty())
    definitions.emplace_back(std::move(curKey), string("\x01"));
  return true;
}

ProtoParser::FragmentTokens ProtoParser::parseFragment(const string& fragment) {
  auto subFragments = split(fragment, "'");
  const auto sz = subFragments.size();
  if (sz > 1 && sz != 3 && sz != 5)
    return {{}, false};
  vector<string> tokens;
  for (size_t i = 0; i < sz; ++i) {
    if (i % 2 == 0) { // outside ''
      trim(subFragments[i]);
      if (!subFragments[i].empty()) {
        auto subFragmentTokens = split(subFragments[i], " ");
        for (size_t i = 0; i < subFragmentTokens.size(); ++i) {
          if (!subFragmentTokens[i].empty()) {
            if (!isTokenOK(subFragmentTokens[i])) {
              error = subFragmentTokens[i];
              return {{}, false};
            }
            tokens.push_back(std::move(subFragmentTokens[i]));
          }
        }
      }
    }
    else { // inside '' everything is a token
      tokens.push_back(std::move(subFragments[i]));
    }
  }
  return {tokens, tokens.size() < 3};
}

string ProtoParser::getMethodName() const {
  if (noargs()) return definitions[0].key;
  string methodName;
  for (const auto& definition : definitions) {
    if (definition.key.find(':') != string::npos)
      return "";
    methodName += definition.key + ':';
  }
  return methodName;
}

vector<string> ProtoParser::getArgNames() const {
  vector<string> argNames;
  if (noargs()) return argNames;
  for (auto& definition : definitions) {
    if (definition.value.empty())
      argNames.push_back(definition.key);
    else
      argNames.push_back(std::move(definition.value));
  }
  return argNames;
}

string ProtoParser::errorMessage() const noexcept {
  if (invalidIdentifier())
    return "incorrect prototype identifier "+error;
  else if (proto.find_first_not_of(" \t") == string::npos)
    return "empty prototype";
  return "syntax error in prototype";
}

bool ProtoParser::isTokenOK(const string& token) {
  if (!isalpha(token[0]) && token[0] != '_') return false;
  for (string::size_type i = 1; i < token.size(); ++i)
    if (!isalnum(token[i]) && token[i] != '_') return false;
  return true;
}

void g_Trim(string& s) {
  const auto p = s.find_first_not_of(" \t");
  if (p == string::npos) {s = ""; return;}
  const auto q = s.find_last_not_of(" \t");
  s = s.substr(p, q == string::npos ? string::npos : q - p + 1);
}

void ProtoParser::trim(string& s)
{ g_Trim(s); }
