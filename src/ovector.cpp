#include <memory>
#include <algorithm>
#include <vector>
#include "objectAutoCleaner.hpp"
#include "odev.h"
#include "otypes.h"
#include "ovector.hpp"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "ostring.hpp"
#include "ortmgr.hpp"
#include "obasicTypeInfo.hpp"
#include "oveciter.hpp"
#include "osetiter.hpp"
#include "orange.hpp"
#include "oalgo.hpp"

_TEMPLATE_DEFINITION(OVector, OInstance);

using std::string;
using std::vector;

#define _CHECK_DISPOSABLE(n) \
  if (argv[n]->disposable()) \
    _THROW_RTE(s_errorMsg())
#define _CHECK_DISPOSABLE_METHOD(n, meth) \
  if (argv[n]->disposable())              \
    _THROW_RTE_METHOD(meth, s_errorMsg())

#define _CHECK_BAD_ARG(i,TYPEID,METHNAM)             \
  _ASSERT(m->getClass(TYPEID)->isCoreClass(), m);    \
  if (!OM_ARG_TYPEID(i, TYPEID))                     \
    return m->badArgTypeId(METHNAM, i, TYPEID, argv)

template<typename t>
  static string s_getFullMethodName(const string& methodName) noexcept
  { return OBasicTypeInfo::getVectorClass<t>()->name+"::"+methodName; }
template<>
  string s_getFullMethodName<ORef>(const string& methodName) noexcept
  { return OBasicTypeInfo::getObjectVectorClass()->name+"::"+methodName; }

static string s_errorMsg() noexcept {
    return "cannot add temporary objects to '"+
       BTI::getVectorClass<ORef>()->name+"'";
  }

template<typename t>
  t s_castValue(OArg* argv, Session* m) noexcept
  { return g_castValue<t>(argv, m); }
template<>
  ORef s_castValue<ORef>(OArg* argv, Session*) noexcept
  { return _ARG(0); }

//::::::::::::::::::::::::::::   Helper Methods   :::::::::::::::::::::::::::://

template<typename t>
  ORet OVector<t>::packComponentValue(t& x, const char* methodName, Session* m)
      requires (!std::is_same_v<t, bool>) {
    ORef r = BTI::getTypeProxyClass<t>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>(methodName));
    _PTypeProxy(t, r)->setValue(x);
    _PTypeProxy(t, r)->setElemPtr(&x);
    const bool constRec = m->getReceiver()->isConst();
    return OVar{r, ushort(USG_DISPOSABLE | (constRec ? USG_CONST : 0))};
  }

template<typename t>
  ORet OVector<t>::packComponentValue(const vector<bool>::reference& x,
      const char* methodName, Session* m) requires std::is_same_v<t, bool> {
    ORef r = BTI::getTypeProxyClass<bool>()->createObject(m);
    _RETHROW(s_getFullMethodName<bool>(methodName));
    _PTypeProxy(bool, r)->setValue(x);
    _PTypeProxy(bool, r)->setElemRef(x);
    const bool constRec = m->getReceiver()->isConst();
    return OVar{r, ushort(USG_DISPOSABLE | (constRec ? USG_CONST : 0))};
  }

//============================================================================//
//::::::::::::::::::::::::   Template Instantiation   :::::::::::::::::::::::://
//============================================================================//

template class OVector_Class<ulong>;
template class OVector_Class<long>;
template class OVector_Class<unsigned>;
template class OVector_Class<int>;
template class OVector_Class<short>;
template class OVector_Class<char>;
template class OVector_Class<double>;
template class OVector_Class<float>;
template class OVector_Class<bool>;
template class OVector_Class<void*>;
template class OVector_Class<ORef>;

//============================================================================//
//:::::::::::::::::::::::::::::   type vector   :::::::::::::::::::::::::::::://
//============================================================================//

//:::::::::::::   vector -- component: access vector component   ::::::::::::://
template<typename t>
ORet OVector<t>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  // unpack arguments
  const size_t i = s_castValue<size_t>(argv, m);
  _RETHROW(s_getFullMethodName<t>(S_component));
  if (i >= this->size()) {
    const auto methodName = s_getFullMethodName<t>(S_component);
    _THROW_RTE_METHOD(methodName, "argument out of range");
  }
  auto& x = (*this)[i];
  return packComponentValue(x, S_component, m);
}

template<>
ORet OVector<bool>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  // unpack arguments
  const size_t i = s_castValue<size_t>(argv, m);
  _RETHROW("bool vector::" S_component);
  if (i >= size())
    _THROW_RTE_METHOD("bool vector::" S_component, "argument out of range");
  auto x = (*this)[i];
  return packComponentValue(x, S_component, m);
}

template<>
ORet OVector<ORef>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  // unpack arguments
  const size_t i = s_castValue<size_t>(argv, m);
  _RETHROW(s_getFullMethodName<ORef>(S_component));
  if (i >= this->size()) {
    const auto methodName = s_getFullMethodName<ORef>(S_component);
    _THROW_RTE_METHOD(methodName, "argument out of range");
  }
  // do the call and return value
  const ushort usage = USG_DISPOSABLE | USG_REF |
      (m->getReceiver()->isConst() ? USG_CONST : 0);
  return OVariable{(*this)[i], usage};
}

//:::::::::::   vector -- getItem: get value of vector component   ::::::::::://

template<typename t>
ORet OVector<t>::ommGetItem(OArg rec, int argc, OArg* argv, Session* m) {
  // unpack arguments
  const size_t i = s_castValue<size_t>(argv, m);
  _RETHROW(s_getFullMethodName<t>("getItem:"));
  if (i >= this->size()) {
    const auto methodName = s_getFullMethodName<t>("getItem:");
    _THROW_RTE_METHOD(methodName, "argument out of range");
  }
  // do the call
  const t x = (*this)[i];
  // pack return value
  const ushort usage = USG_DISPOSABLE |
      (m->getReceiver()->isConst() ? USG_CONST : 0);
  OVar r{BTI::getTypeClass<t>()->createObject(m), usage};
  _RETHROW(s_getFullMethodName<t>("getItem:"));
  _P(t, REF(r))->setValue(x);
  return r;
}

template<>
ORet OVector<ORef>::ommGetItem(OArg rec, int argc, OArg* argv, Session* m) {
  ORet result = ommComponent(rec, argc, argv, m);
  _RETHROW(s_getFullMethodName<ORef>("getItem:"));
  return result;
}

//:::::::::::::::::::::::::::   vector -- value:   ::::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getVectorClass<t>()->name+"::" S_value);
  constexpr auto thisTypeId = BTI::getVectorClassId<t>();
  if (_ARG(0)->getClassId() == thisTypeId && (argv[0]->movable() ||
                                              argv[0]->disposable()))
    this->swap(_ARG(0)->castRef<OVector<t>>());
  else if (_ARG_TYPEID(0, thisTypeId))
    *static_cast<vector<t>*>(this) = _ARG(0)->castRef<OVector<t>>();
  else {
    const int itemTypeId = _ARG_TYPEID(0, BTI::getObjectVectorClassId()) ?
        static_cast<ObjectVector*>(_ARG(0))->sameBasicTypeItems() : 0;
    if (itemTypeId == BTI::getTypeClassId<t>()) {
      const auto& r = _ARG(0)->castConstRef<ObjectVector>();
      vector<t> newContent;
      for (const auto o : r)
        newContent.push_back(g_typeValue<t>(o));
      *static_cast<vector<t>*>(this) = std::move(newContent);
    }
    else {
      const t value = s_castValue<t>(argv, m);
      _RETHROW(s_getFullMethodName<t>(S_value));
      if (this->empty())
        this->push_back(value);
      else
        this->assign(this->size(), value);
    }
  }
  return makeResult();
}

template<>
ORet OVector<ORef>::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<ORef>(S_value));
  constexpr auto thisTypeId = BTI::getVectorClassId<ORef>();
  if (_ARG(0)->getClassId() == thisTypeId && (argv[0]->movable() ||
                                              argv[0]->disposable()))
    this->swap(_ARG(0)->castRef<OVector<ORef>>());
  else if (_ARG_TYPEID(0, thisTypeId))
    *static_cast<base_class*>(this) = _ARG(0)->castRef<OVector<ORef>>();
  else {
    _CHECK_DISPOSABLE_METHOD(0, s_getFullMethodName<ORef>(S_value))
    if (_ARG_TYPEID(0, BTI::getObjectVectorClassId()))
      *static_cast<base_class*>(this) = _ARG(0)->castConstRef<ObjectVector>();
    else if (empty())
      push_back(_ARG(0));
    else
      assign(size(), _ARG(0));
  }
  return makeResult();
}

//::::::::::::::::   vector -- init: initialize components   ::::::::::::::::://

template<typename t>
ORet OVector<t>::ommInit(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(this->empty(), m);
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>(S_init));
  this->reserve(argc);
  for (size_t i = 0; i < static_cast<size_t>(argc); ++i) {
    if constexpr_ (std::is_same<t, ORef>::value) {
      _CHECK_DISPOSABLE_METHOD(i, s_getFullMethodName<ORef>(S_init))
      this->emplace_back(_ARG(i));
    }
    else {
      t value;
      if (_ARG_TYPEID(i, BTI::getTypeClassId<t>()))
        value = _P(t, _ARG(i))->getValue();
      else {
        value = g_castValue<t>(argv + i, m);
        _RETHROW(s_getFullMethodName<t>("init:"));
      }
      this->emplace_back(value);
    }
  }
  return makeResult();
}

//:::::::   vector -- size:value: set size and initialize components   ::::::://

//void assign (size_type n, const value_type& val);
// rec size: arg0 value: arg1;
template<typename t>
ORet OVector<t>::ommSizeValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(s_getFullMethodName<t>("size:value:"), 2);
  _MUTABLE_REC();
  // unpack arguments
  const size_t size = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  const t value = s_castValue<t>(argv + 1, m);
  if constexpr_ (std::is_same<t, ORef>::value) {
    _CHECK_DISPOSABLE(1)
  }
  else {
    _CHECK_EXCEPTION();
  }
  // do the call
  this->assign(size, value);
  // return reference to this
  return makeResult();
}

//:::::   vector -- component:value: set value of specified component   :::::://

template<typename t>
ORet OVector<t>::ommComponentValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(s_getFullMethodName<t>("component:value:"), 2);
  _MUTABLE_REC();
  // unpack arguments
  const size_t i = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  if (i >= this->size())
    _THROW_RTE("argument out of range");
  const t value = s_castValue<t>(argv + 1, m);
  if constexpr_ (std::is_same<t, ORef>::value) {
    _CHECK_DISPOSABLE(1)
  }
  else {
    _CHECK_EXCEPTION();
  }
  // do the call
  (*this)[i] = value;
  // return reference to this
  return makeResult();
}

//:::::::::::::::::::::::::::   vector -- size:   :::::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommResize(OArg rec, int argc, OArg* argv, Session* m) {
  if (argc != 1) {
    m->throwBadArgc(__PRETTY_FUNCTION__, s_getFullMethodName<t>("size:"), 1,
                    __FILE__, __LINE__);
    return OInstance::undefinedResult();
  }
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>("size:"));
  const size_t size = s_castValue<size_t>(argv, m);
  _RETHROW(s_getFullMethodName<t>("size:"));
  this->resize(size);
  return makeResult();
}

//::::::::::::::::::::::::::::   vector -- size   :::::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommSize(OArg rec, int, OArg*, Session* m) {
  ORef r = OBasicTypeInfo::getTypeClass<size_t>()->createObject(m);
  _RETHROW(s_getFullMethodName<t>("size"));
  _P(size_t, r)->setValue(this->size());
  return r->disposableResult();
}

//:::::::::::::::::::::::::   vector -- push, pop   :::::::::::::::::::::::::://

// rec pushBack: arg;
template<typename t>
ORet OVector<t>::ommPushBack(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>("pushBack:"));
  const t value = s_castValue<t>(argv, m);
  if constexpr_ (std::is_same<t, ORef>::value) {
    if (argv[0]->disposable()) {
      m->throwRTE(s_getFullMethodName<t>("pushBack:"), s_errorMsg());
      return undefinedResult();
    }
  }
  else
    _RETHROW(s_getFullMethodName<t>("pushBack:"));
  this->push_back(value);
  return makeResult();
}

// rec popBack;
template<typename t>
ORet OVector<t>::ommPopBack(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>("popBack"));
  this->pop_back();
  return makeResult();
}

//:::::::::::::::::::::::::::   vector -- empty   :::::::::::::::::::::::::::://

// rec empty;
template<typename t>
ORet OVector<t>::ommEmpty(OArg rec, int, OArg*, Session* m) {
  ORef r = OBasicTypeInfo::getTypeClass<bool>()->createObject(m);
  _RETHROW(s_getFullMethodName<t>("empty"));
  _P(bool, r)->setValue(this->empty());
  return r->disposableResult();
}

//::::::::::::::::::::::::   vector -- front, back   ::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommFront(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<t>("front");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  // auto& x = this->front();
  // return packComponentValue(x, "front", m);
  return packComponentValue(this->front(), "front", m);
}

template<>
ORet OVector<bool>::ommFront(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<bool>("front");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  // auto x = this->front();
  // return packComponentValue(x, "front", m);
  return packComponentValue(this->front(), "front", m);
}

template<>
ORet OVector<ORef>::ommFront(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<ORef>("front");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  return OVariable{this->front(), USG_REF};
}

template<typename t>
ORet OVector<t>::ommBack(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<t>("back");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  // auto& x = this->back();
  // return packComponentValue(x, "back", m);
  return packComponentValue(this->back(), "back", m);
}

template<>
ORet OVector<bool>::ommBack(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<bool>("back");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  // auto x = this->back();
  // return packComponentValue(x, "back", m);
  return packComponentValue(this->back(), "back", m);
}

template<>
ORet OVector<ORef>::ommBack(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) {
    const auto methodName = s_getFullMethodName<ORef>("back");
    _THROW_RTE_METHOD(methodName, "method was incorrectly called");
  }
  return OVariable{this->back(), USG_REF};
}

//::::::::::::::::::::::::   vector -- shrinkToFit   ::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommShrinkToFit(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>("shrinkToFit"));
  this->shrink_to_fit();
  return makeResult();
}

//::::::::::::::::::::::::::::   vector -- data   :::::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommData(OArg rec, int, OArg*, Session* m) {
  ORef r = OBasicTypeInfo::getTypeClass<void*>()->createObject(m);
  _RETHROW(s_getFullMethodName<t>("data"));
  _P(void*, r)->setValue(static_cast<void*>(this->data()));
  return r->disposableResult();
}

template<>
ORet OVector<bool>::ommData(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(s_getFullMethodName<bool>("data"),
      "inapplicable method");
}

//:::::::::::::::::::::::::::   vector -- erase   :::::::::::::::::::::::::::://

//iterator erase(const_iterator position);
template<typename t>
ORet OVector<t>::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("erase:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<t>(), _ox_method_name);
  const auto& it = _ARG(0)->castConstRef<OVectorIterator<t>>();
  // do the call
  auto retValue = this->erase(it);
  // pack return value
  ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<t>>() = std::move(retValue);
  return r->disposableResult();
}

//iterator erase(const_iterator first, const_iterator last);
template<typename t>
ORet OVector<t>::ommEraseRange(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("eraseFrom:to:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<t>(), _ox_method_name);
  _CHECK_BAD_ARG(1, BTI::getVectorIteratorClassId<t>(), _ox_method_name);
  auto& from = _ARG(0)->castConstRef<OVectorIterator<t>>();
  auto& to   = _ARG(1)->castConstRef<OVectorIterator<t>>();
  // do the call
  auto retValue = this->erase(from, to);
  // pack return value
  ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<t>>() = std::move(retValue);
  return r->disposableResult();
}

//:::::::::::::::::::::::::::   vector -- clear   :::::::::::::::::::::::::::://
template<typename t>
ORet OVector<t>::ommClear(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>("clear"));
  this->clear();
  return makeResult();
}

//:::::::::::::::::::::::::::   vector -- insert   ::::::::::::::::::::::::::://

template<typename t, typename _Cont>
  struct OInsertLoop : OBasicLoop, ValueExtractor<t> {
    using iter_t = typename _Cont::iterator;
    _Cont& container;
    iter_t pos;
    iter_t lastPos;

    OInsertLoop(_Cont& c, iter_t pos) : container{c}, pos{pos}, lastPos{pos} {}
    OInsertLoop(_Cont& c, iter_t pos, bool contOwns, getmsg_t f)
        requires std::is_same_v<t, ORef> :
        container{c}, pos{pos}, lastPos{pos}, ValueExtractor<t>{contOwns, f} {}

    virtual void action(OVar result, Session* m) noexcept override {
      // extract value
      const t value = this->extractValue(result, m);
      if (m->exceptionPending()) return;
      // insert value into this set
      lastPos = container.insert(pos, value);
      // advance to next position
      pos = lastPos + 1;
    }

    virtual ORet getResult(Session* m) noexcept override {
      ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
      if (m->exceptionPending()) return OVariable::undefinedResult();
      r->castRef<OVectorIterator<t>>() = std::move(lastPos);
      return r->disposableResult();
    }
  };

#define VEC_ITER(t, n) *_PVecIter(t, _ARG(n))
#define SET_ITER(t, n) *_PSetIter(t, _ARG(n))

template<typename t>
  typename vector<t>::iterator OVector<t>::insertTypeRange(
      typename vector<t>::iterator pos, OArg* argv, bool& handled) {
    const auto argTypeId = _ARG(0)->getClassId();
    handled = true;
    switch (argTypeId) {
      case CLSID_ULONGVECITER:
      // case BTI::getVectorIteratorClassId<ulong>():
        return this->insert(pos, VEC_ITER(ulong, 0),  VEC_ITER(ulong, 1));
      case CLSID_LONGVECITER:
        return this->insert(pos, VEC_ITER(long, 0),   VEC_ITER(long, 1));
      case CLSID_UINTVECITER:
        return this->insert(pos, VEC_ITER(uint, 0),   VEC_ITER(uint, 1));
      case CLSID_INTVECITER:
        return this->insert(pos, VEC_ITER(int, 0),    VEC_ITER(int, 1));
      case CLSID_SHORTVECITER:
        return this->insert(pos, VEC_ITER(short, 0),  VEC_ITER(short, 1));
      case CLSID_CHARVECITER:
        return this->insert(pos, VEC_ITER(char, 0),   VEC_ITER(char, 1));
      case CLSID_DBLVECITER:
        return this->insert(pos, VEC_ITER(double, 0), VEC_ITER(double, 1));
      case CLSID_FLTVECITER:
        return this->insert(pos, VEC_ITER(float, 0),  VEC_ITER(float, 1));
      case CLSID_BOOLVECITER:
        return this->insert(pos, VEC_ITER(bool, 0),   VEC_ITER(bool, 1));
      case CLSID_ULONGSETITER:
        return this->insert(pos, SET_ITER(ulong, 0),  SET_ITER(ulong, 1));
      case CLSID_LONGSETITER:
        return this->insert(pos, SET_ITER(long, 0),   SET_ITER(long, 1));
      case CLSID_DBLSETITER:
        return this->insert(pos, SET_ITER(double, 0), SET_ITER(double, 1));
    }
    handled = false;
    return this->end();
  }

template<>
  vector<void*>::iterator OVector<void*>::insertTypeRange(
      vector<void*>::iterator pos, OArg* argv, bool& handled) {
    handled = false;
    return this->end();
  }

template<>
  vector<ORef>::iterator OVector<ORef>::insertTypeRange(
      vector<ORef>::iterator pos, OArg* argv, bool& handled) {
    const auto argTypeId = _ARG(0)->getClassId();
    handled = true;
    switch (argTypeId) {
      case CLSID_OBJVECITER:
        return this->insert(pos, VEC_ITER(ORef, 0), VEC_ITER(ORef, 1));
      case CLSID_OBJSETITER:
        return this->insert(pos, SET_ITER(ORef, 0), SET_ITER(ORef, 1));
    }
    handled = false;
    return this->end();
  }

#undef SET_ITER
#undef VEC_ITER

// iterator insert (const_iterator position, const value_type& val);
// vec insert: val at: position;
template<typename t>
  ORet OVector<t>::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("insert:at:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(1, BTI::getVectorIteratorClassId<t>(), _ox_method_name);
    // unpack arguments
    const t value = s_castValue<t>(argv, m);
    if constexpr_ (std::is_same<t, ORef>::value) {
      _CHECK_DISPOSABLE(0)
    }
    else {
      _CHECK_EXCEPTION();
    }
    const auto& pos = _ARG(1)->castRef<OVectorIterator<t>>();
    // do the call
    auto retValue = this->insert(pos, value);
    // pack result
    ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<OVectorIterator<t>>() = std::move(retValue);
    return r->disposableResult();
  }

template<typename t>
  ORet OVector<t>::ommInsertSize(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("insert:size:at:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(2, BTI::getVectorIteratorClassId<t>(), _ox_method_name);
    // unpack arguments
    const t value = s_castValue<t>(argv, m);
    if constexpr_ (std::is_same<t, ORef>::value) {
      _CHECK_DISPOSABLE(0)
    }
    else {
      _CHECK_EXCEPTION();
    }
    const size_t size = s_castValue<size_t>(argv + 1, m);
    _CHECK_EXCEPTION();
    const auto& pos = _ARG(2)->castRef<OVectorIterator<t>>();
    // do the call
    auto retValue = this->insert(pos, size, value);
    // pack result
    ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<OVectorIterator<t>>() = std::move(retValue);
    return r->disposableResult();
  }

// iterator insert(const_iterator pos, InputIterator first, InputIterator last)
template<typename t>
  ORet OVector<t>::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<t>("insertAt:from:to:"));
    _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<t>(),
        s_getFullMethodName<t>("insertAt:from:to:"));
    auto pos = _ARG(0)->castRef<OVectorIterator<t>>();
    _DEBUG(                                                            \
      if (!validateIterator(*static_cast<vector<t>*>(this), pos))      \
        _THROW_RTE_METHOD(s_getFullMethodName<t>("insertAt:from:to:"), \
            "iterator does not belong to vector");                     \
    )
    if (_ARG(1)->getClassId() != _ARG(2)->getClassId())
      _THROW_RTE_METHOD(s_getFullMethodName<t>("insertAt:from:to:"),
          "iterator type mismatch; expected '"+_ARG(1)->getClassName()+
          "' found '"+_ARG(2)->getClassName()+"'");
    // try to handle compatible basic type
    bool handled;
    auto it = insertTypeRange(pos, argv + 1, handled);
    if (handled) {
      // pack result
      ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
      _RETHROW(s_getFullMethodName<t>("insertAt:from:to:"));
      r->castRef<OVectorIterator<t>>() = std::move(it);
      return r->disposableResult();
    }
    // run iteration loop
    OInsertLoop<t, vector<t>> loop{*this, pos};
    ORet result = loop.run(argv + 1, m);
    _RETHROW(s_getFullMethodName<t>("insertAt:from:to:"));
    return result;
  }

template<>
  ORet OVector<ORef>::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<ORef>("insertAt:from:to:"));
    _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<ORef>(),
        s_getFullMethodName<ORef>("insertAt:from:to:"));
    auto pos = _ARG(0)->castRef<OVectorIterator<ORef>>();
    _DEBUG(                                                               \
      if (!validateIterator(*static_cast<vector<ORef>*>(this), pos))      \
        _THROW_RTE_METHOD(s_getFullMethodName<ORef>("insertAt:from:to:"), \
            "iterator does not belong to vector");                        \
    )
    if (_ARG(1)->getClassId() != _ARG(2)->getClassId())
      _THROW_RTE_METHOD(s_getFullMethodName<ORef>("insertAt:from:to:"),
          "iterator type mismatch; expected '"+_ARG(1)->getClassName()+
          "' found '"+_ARG(2)->getClassName()+"'");
    // try to handle compatible basic type
    bool handled;
    auto it = insertTypeRange(pos, argv + 1, handled);
    if (handled) {
      // pack result
      ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
      _RETHROW(s_getFullMethodName<ORef>("insertAt:from:to:"));
      r->castRef<OVectorIterator<ORef>>() = std::move(it);
      return r->disposableResult();
    }
    // run iteration loop
    OInsertLoop<ORef, vector<ORef>> loop(*this, pos, false,
        []() -> std::pair<string, string> {
          string methodName = s_getFullMethodName<ORef>("insertAt:from:to:");
          return {BTI::getVectorClass<ORef>()->name, methodName};
        });
    ORet result = loop.run(argv + 1, m);
    _RETHROW(s_getFullMethodName<ORef>("insertAt:from:to:"));
    return result;
  }

//:::::::::::::::::::::::::::   vector -- assign   ::::::::::::::::::::::::::://

template<typename t, typename _Cont>
struct OAssignLoop : OBasicLoop, ValueExtractor<t> {
  _Cont& container;

  OAssignLoop(_Cont& c) : container{c} {}
  OAssignLoop(_Cont& c, bool contOwns, getmsg_t f) :
      container{c}, ValueExtractor<t>{contOwns, f} {}

  virtual void action(OVar result, Session* m) noexcept override {
    const t value = this->extractValue(result, m);
    if (m->exceptionPending()) return;
    container.push_back(value);
  }
};

#define VEC_ITER(t, n) *_PVecIter(t, _ARG(n))
#define SET_ITER(t, n) *_PSetIter(t, _ARG(n))

template<typename t>
bool OVector<t>::assignTypeRange(OArg* argv) {
  const auto argTypeId = _ARG(0)->getClassId();
  switch (argTypeId) {
    case CLSID_ULONGVECITER:
      this->assign(VEC_ITER(ulong, 0),  VEC_ITER(ulong, 1)); break;
    case CLSID_LONGVECITER:
      this->assign(VEC_ITER(long, 0),   VEC_ITER(long, 1)); break;
    case CLSID_UINTVECITER:
      this->assign(VEC_ITER(uint, 0),   VEC_ITER(uint, 1)); break;
    case CLSID_INTVECITER:
      this->assign(VEC_ITER(int, 0),    VEC_ITER(int, 1)); break;
    case CLSID_SHORTVECITER:
      this->assign(VEC_ITER(short, 0),  VEC_ITER(short, 1)); break;
    case CLSID_CHARVECITER:
      this->assign(VEC_ITER(char, 0),   VEC_ITER(char, 1)); break;
    case CLSID_DBLVECITER:
      this->assign(VEC_ITER(double, 0), VEC_ITER(double, 1)); break;
    case CLSID_FLTVECITER:
      this->assign(VEC_ITER(float, 0),  VEC_ITER(float, 1)); break;
    case CLSID_BOOLVECITER:
      this->assign(VEC_ITER(bool, 0),   VEC_ITER(bool, 1)); break;
    case CLSID_ULONGSETITER:
      this->assign(SET_ITER(ulong, 0),  SET_ITER(ulong, 1)); break;
    case CLSID_LONGSETITER:
      this->assign(SET_ITER(long, 0),   SET_ITER(long, 1)); break;
    case CLSID_DBLSETITER:
      this->assign(SET_ITER(double, 0), SET_ITER(double, 1)); break;
    default:
      return false;
  }
  return true;
}

template<>
bool OVector<void*>::assignTypeRange(OArg* argv) { return false; }

template<>
bool OVector<ORef>::assignTypeRange(OArg* argv) {
  const auto argTypeId = _ARG(0)->getClassId();
  switch (argTypeId) {
    case CLSID_OBJVECITER:
      this->assign(VEC_ITER(ORef, 0), VEC_ITER(ORef, 1)); break;
    case CLSID_OBJSETITER:
      this->assign(SET_ITER(ORef, 0), SET_ITER(ORef, 1)); break;
    default:
      return false;
  }
  return true;
}

#undef SET_ITER
#undef VEC_ITER

// void assign(InputIterator first, InputIterator last)
template<typename t>
ORet OVector<t>::ommAssignRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<t>(""));
  if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
    _THROW_RTE_METHOD(s_getFullMethodName<t>("assignFrom:to:assignFrom:to:"),
        "iterator type mismatch; expected '"+_ARG(0)->getClassName()+
        "' found '"+_ARG(1)->getClassName()+"'");
  // try to handle compatible basic type
  const bool handled = assignTypeRange(argv);
  if (handled) return makeResult();
  // run iteration loop
  OAssignLoop<t, vector<t>> loop{*this};
  loop.run(argv, m);
  _RETHROW(s_getFullMethodName<t>("assignFrom:to:"));
  return makeResult();
}

template<>
ORet OVector<ORef>::ommAssignRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getFullMethodName<ORef>("assignFrom:to:"));
  if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
    _THROW_RTE_METHOD(s_getFullMethodName<ORef>("assignFrom:to:"),
        "iterator type mismatch; expected '"+_ARG(0)->getClassName()+
        "' found '"+_ARG(1)->getClassName()+"'");
  // try to handle compatible basic type
  const bool handled = assignTypeRange(argv);
  if (handled) return makeResult();
  // run iteration loop
  OAssignLoop<ORef, vector<ORef>> loop{*this, false,
      []() -> std::pair<string, string> {
        string methodName = s_getFullMethodName<ORef>("assignFrom:to:");
        return {BTI::getVectorClass<ORef>()->name, methodName};
      }
  };
  loop.run(argv, m);
  _RETHROW(s_getFullMethodName<ORef>("assignFrom:to:"));
  return makeResult();
}

//:::::::::::::::::::::::::::   vector -- swap:   :::::::::::::::::::::::::::://

template<typename t>
ORet OVector<t>::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(s_getFullMethodName<t>("swap:"), 1);
  _MUTABLE_REC();
  _MUTABLE_ARG(0);
  _CHECK_ARG_CLASS(0, BTI::getVectorClass<t>());
  vector<t>& r = _ARG(0)->castRef<OVector<t>>();
  this->swap(r);
  return makeResult();
}

// TODO const all

//:::::::::::::::::::::::::   vector -- begin, end   ::::::::::::::::::::::::://

static const char* prefix[]{"", "c", "r", "cr"};

template<typename t, int _End, int _Const, int _Rev>
  ORet _genericIterator(typename std::vector<t>* _this, Session* m) {
    ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>(prefix[_Const+2*_Rev]+
        string{_End ? "end" : "begin"}));
    auto& it = r->castRef<_O(VectorIterator)<t>>();
    if constexpr (_Rev) {
      if constexpr (_End)
        it = _this->begin();
      else
        it = _this->end();
    }
    else {
      if constexpr (_End)
        it = _this->end();
      else
        it = _this->begin();
    }
    if constexpr (_Const)
      static_cast<OVectorIterator<t>*>(r)->setConst();
    static_cast<OVectorIterator<t>*>(r)->setReverse(_Rev);
    return r->disposableResult();
  }

template<typename t>
  ORet OVector<t>::ommBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 0, 0, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommCBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 0, 1, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommRBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 0, 0, 1>(this, m); }
template<typename t>
  ORet OVector<t>::ommCRBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 0, 1, 1>(this, m); }

template<typename t>
  ORet OVector<t>::ommEnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 1, 0, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommCEnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 1, 1, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommREnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 1, 0, 1>(this, m); }
template<typename t>
  ORet OVector<t>::ommCREnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<t, 1, 1, 1>(this, m); }

//::::::::::::::::::::::::::::::::   Ranges   :::::::::::::::::::::::::::::::://

template<typename t, int _Const, int _Rev>
  ORet _genericAll(typename std::vector<t>* _this, Session* m) {
    ORef b, e, r;
    b = BTI::getVectorIteratorClass<t>()->createObject(m);
    if (!m->exceptionPending()) {
      e = BTI::getVectorIteratorClass<t>()->createObject(m);
      if (!m->exceptionPending())
        r = ORange::getRangeClass()->createObject(m);
    }
    _RETHROW(s_getFullMethodName<t>(prefix[_Const+2*_Rev]+string{"all"}));
    if constexpr (_Rev) {
      static_cast<_O(VectorIterator)<t>*>(b)->setReverse(true);
      static_cast<_O(VectorIterator)<t>*>(e)->setReverse(true);
      b->castRef<_O(VectorIterator)<t>>() = _this->rbegin().base();
      e->castRef<_O(VectorIterator)<t>>() = _this->rend().base();
    }
    else {
      b->castRef<_O(VectorIterator)<t>>() = _this->begin();
      e->castRef<_O(VectorIterator)<t>>() = _this->end();
    }
    if constexpr (_Const) {
      static_cast<_O(VectorIterator)<t>*>(b)->setConst();
      static_cast<_O(VectorIterator)<t>*>(e)->setConst();
    }
    static_cast<ORange*>(r)->addPipelineAll(m, b, e);
    return r->disposableResult();
  }

template<typename t>
  ORet OVector<t>::ommAll(OArg, int, OArg*, Session* m)
  { return _genericAll<t, 0, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommRAll(OArg, int, OArg*, Session* m)
  { return _genericAll<t, 0, 1>(this, m); }
template<typename t>
  ORet OVector<t>::ommCAll(OArg, int, OArg*, Session* m)
  { return _genericAll<t, 1, 0>(this, m); }
template<typename t>
  ORet OVector<t>::ommCRAll(OArg, int, OArg*, Session* m)
  { return _genericAll<t, 1, 1>(this, m); }

//::::::::::::::::::::::::::::::::   Casts   ::::::::::::::::::::::::::::::::://

template<typename t>
  template<typename T>
    ORet OVector<t>::ommCast(OArg, int, OArg*, Session* m) {
      const string methodName = BTI::getVectorClass<T>()->name;
      const string msg = "illegitimate conversion";
      m->throwRTE(s_getFullMethodName<t>(methodName), msg);
      return undefinedResult();
    }

template<>
  template<typename T>
    ORet OVector<char>::ommCast(OArg, int, OArg*, Session* m) {
      if (castRef<OVector<char>>().size() < sizeof(T))
        _THROW_RTE_METHOD(
            s_getFullMethodName<char>(BTI::getVectorClass<T>()->name),
            "illegitimate conversion due to size mismatch")
      char* const p = castRef<OVector<char>>().data();
      ORef r = BTI::getTypeClass<T>()->createObject(m);
      _RETHROW(s_getFullMethodName<char>(BTI::getVectorClass<T>()->name));
      _P(T, r)->setValue(*reinterpret_cast<T*>(p));
      return r->disposableResult();
    }

//::::::::::::::::::::::::::::::  Method Table  :::::::::::::::::::::::::::::://

#define METHOD_TABLE_FOR_vector(t)                             \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(OVector<t>)                   \
  PUBLIC (OVector<t>::ommAssignRange,    "assignFrom:to:"    ) \
  PUBLIC (OVector<t>::ommBack,           "back"              ) \
  PUBLIC (OVector<t>::ommComponent,      "component:"        ) \
  PUBLIC (OVector<t>::ommGetItem,        "getItem:"          ) \
  PUBLIC (OVector<t>::ommComponentValue, "component:value:"  ) \
  PUBLIC (OVector<t>::ommClear,          "clear"             ) \
  PUBLIC (OVector<t>::ommData,           "data"              ) \
  PUBLIC (OVector<t>::ommEmpty,          "empty"             ) \
  PUBLIC (OVector<t>::ommErase,          "erase:"            ) \
  PUBLIC (OVector<t>::ommEraseRange,     "eraseFrom:to:"     ) \
  PUBLIC (OVector<t>::ommFront,          "front"             ) \
  PUBLIC (OVector<t>::ommInsert,         "insert:at:"        ) \
  PUBLIC (OVector<t>::ommInsertSize,     "insert:size:at:"   ) \
  PUBLIC (OVector<t>::ommInsertRange,    "insertAt:from:to:" ) \
  PUBLIC (OVector<t>::ommInit,           S_init              ) \
  PUBLIC (OVector<t>::ommValue,          S_value             ) \
  PUBLIC (OVector<t>::ommSize,           "size"              ) \
  PUBLIC (OVector<t>::ommSizeValue,      "size:value:"       ) \
  PUBLIC (OVector<t>::ommResize,         "size:"             ) \
  PUBLIC (OVector<t>::ommPushBack,       "pushBack:"         ) \
  PUBLIC (OVector<t>::ommPopBack,        "popBack"           ) \
  PUBLIC (OVector<t>::ommShrinkToFit,    "shrinkToFit"       ) \
  PUBLIC (OVector<t>::ommSwap,           "swap:"             ) \
  PUBLIC (OVector<t>::ommBegin,          "begin"             ) \
  PUBLIC (OVector<t>::ommCBegin,         "cbegin"            ) \
  PUBLIC (OVector<t>::ommRBegin,         "rbegin"            ) \
  PUBLIC (OVector<t>::ommCRBegin,        "crbegin"           ) \
  PUBLIC (OVector<t>::ommEnd,            "end"               ) \
  PUBLIC (OVector<t>::ommCEnd,           "cend"              ) \
  PUBLIC (OVector<t>::ommREnd,           "rend"              ) \
  PUBLIC (OVector<t>::ommCREnd,          "crend"             ) \
  PUBLIC (OVector<t>::ommAll,            "all"               ) \
  PUBLIC (OVector<t>::ommRAll,           "rall"              ) \
  PUBLIC (OVector<t>::ommCAll,           "call"              ) \
  PUBLIC (OVector<t>::ommCRAll,          "crall"             ) \
_END_METHOD_TABLE()

METHOD_TABLE_FOR_vector(ulong);
METHOD_TABLE_FOR_vector(long);
METHOD_TABLE_FOR_vector(unsigned);
METHOD_TABLE_FOR_vector(int);
METHOD_TABLE_FOR_vector(short);
// METHOD_TABLE_FOR_vector(char);
METHOD_TABLE_FOR_vector(double);
METHOD_TABLE_FOR_vector(float);
METHOD_TABLE_FOR_vector(bool);
METHOD_TABLE_FOR_vector(void*);
METHOD_TABLE_FOR_vector(ORef);

_BEGIN_TEMPLATE_METHOD_TABLE_FOR(OVector<char>)
  PUBLIC (OVector<char>::ommAssignRange,    "assignFrom:to:"    )
  PUBLIC (OVector<char>::ommBack,           "back"              )
  PUBLIC (OVector<char>::ommComponent,      "component:"        )
  PUBLIC (OVector<char>::ommGetItem,        "getItem:"          )
  PUBLIC (OVector<char>::ommComponentValue, "component:value:"  )
  PUBLIC (OVector<char>::ommClear,          "clear"             )
  PUBLIC (OVector<char>::ommData,           "data"              )
  PUBLIC (OVector<char>::ommEmpty,          "empty"             )
  PUBLIC (OVector<char>::ommErase,          "erase:"            )
  PUBLIC (OVector<char>::ommEraseRange,     "eraseFrom:to:"     )
  PUBLIC (OVector<char>::ommFront,          "front"             )
  PUBLIC (OVector<char>::ommInsert,         "insert:at:"        )
  PUBLIC (OVector<char>::ommInsertSize,     "insert:size:at:"   )
  PUBLIC (OVector<char>::ommInsertRange,    "insertAt:from:to:" )
  PUBLIC (OVector<char>::ommInit,           S_init              )
  PUBLIC (OVector<char>::ommValue,          S_value             )
  PUBLIC (OVector<char>::ommSize,           "size"              )
  PUBLIC (OVector<char>::ommSizeValue,      "size:value:"       )
  PUBLIC (OVector<char>::ommResize,         "size:"             )
  PUBLIC (OVector<char>::ommPushBack,       "pushBack:"         )
  PUBLIC (OVector<char>::ommPopBack,        "popBack"           )
  PUBLIC (OVector<char>::ommShrinkToFit,    "shrinkToFit"       )
  PUBLIC (OVector<char>::ommSwap,           "swap:"             )
  PUBLIC (OVector<char>::ommBegin,          "begin"             )
  PUBLIC (OVector<char>::ommCBegin,         "cbegin"            )
  PUBLIC (OVector<char>::ommRBegin,         "rbegin"            )
  PUBLIC (OVector<char>::ommCRBegin,        "crbegin"           )
  PUBLIC (OVector<char>::ommEnd,            "end"               )
  PUBLIC (OVector<char>::ommCEnd,           "cend"              )
  PUBLIC (OVector<char>::ommREnd,           "rend"              )
  PUBLIC (OVector<char>::ommCREnd,          "crend"             )
  PUBLIC (OVector<char>::ommAll,            "all"               )
  PUBLIC (OVector<char>::ommRAll,           "rall"              )
  PUBLIC (OVector<char>::ommCAll,           "call"              )
  PUBLIC (OVector<char>::ommCRAll,          "crall"             )
  PUBLIC (OVector<char>::ommCast<ulong>,    S_ulong             )
  PUBLIC (OVector<char>::ommCast<long>,     S_long              )
  PUBLIC (OVector<char>::ommCast<uint>,     S_uint              )
  PUBLIC (OVector<char>::ommCast<int>,      S_int               )
  PUBLIC (OVector<char>::ommCast<short>,    S_short             )
  PUBLIC (OVector<char>::ommCast<void*>,    S_pointer           )
  PUBLIC (OVector<char>::ommCast<double>,   S_double            )
  PUBLIC (OVector<char>::ommCast<float>,    S_float             )
_END_METHOD_TABLE()

template class OVector<ulong>;
template class OVector<long>;
template class OVector<unsigned>;
template class OVector<int>;
template class OVector<short>;
template class OVector<char>;
template class OVector<double>;
template class OVector<float>;
template class OVector<bool>;
template class OVector<void*>;
template class OVector<ORef>;

//:::::::::::::::::::::::::::  Metaclass Methods  :::::::::::::::::::::::::::://

template<typename t>
ORef OVector_Class<t>::copyCore(ORef self, const_ORef r, Session* m) const {
  if (r->getRootClass() != getRootClass()) return nullptr;
  *_PVector(t, self) = *_PCVector(t, r);
  return self;
}

template<typename t>
ORef OVector_Class<t>::moveCore(ORef self, ORef r) const {
  if (r->getRootClass() != getRootClass()) return nullptr;
  *_PVector(t, self) = std::move(*_PVector(t, r));
  return self;
}

template<typename t>
OClass* OVector_Class<t>::createClass(Session* m,
    const string& className) const {
  _ENTER_METHOD_NOCHK("OVector_Class<"+BTI::getTypeClass<t>()->name+
                      ">::createClass");
  OClass* cls = new OVector_Class(m, this->name, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

template<typename t>
OClass* OVector_Class<t>::createMutableClass(Session* m,
    const string& className) const {
  _ENTER_METHOD_NOCHK("OVector_Class<"+BTI::getTypeClass<t>()->name+
                      ">::createMutableClass");
  OClass* cls = new OVector_Class(m, this, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

//============================================================================//
//:::::::::::::::::::::::::::::   ObjectVector   ::::::::::::::::::::::::::::://
//============================================================================//

//:::::::::::::::::::::::::::::::  Utilities  :::::::::::::::::::::::::::::::://

string ObjectVector::getFullMethodName(const string& methodName) const {
  return BTI::getObjectVectorClass()->name+"::"+methodName;
}

int ObjectVector::sameBasicTypeItems() const {
  int basicType = 0;
  for (const auto o : *this) {
    int typeId = o ? o->getRootClassId() : 0;
    if (!BTI::isBasicType(typeId))
      return 0;
    if (basicType == 0)
      basicType = typeId;
    else if (typeId != basicType)
      return 0;
  }
  return basicType;
}

void ObjectVector::clearItems(Session* m, bool alsoContainer) {
  ObjectAutoCleaner cleaner{m, *this};
  if (alsoContainer) this->clear();
}

ORet ObjectVector::copyItems(const vector<ORef>& a, Session* m) {
  _ENTER_NOCHK("ObjectVector::copyItems");
  _ASSERT(this->size() == a.size(), m);

  for (size_t i = 0; i < a.size(); ++i) {
    OClass* itemClass = a[i]->getClass();
    ORef item = itemClass->createObject(m);
    _CHECK_EXCEPTION();
    itemClass->copyObject(item, a[i], m);
    _CHECK_EXCEPTION();
    (*this)[i] = item;
  }

  return makeResult();
}

#define VEC_ITER(t, n) *_PVecIter(t, _ARG(n))
#define SET_ITER(t, n) *_PSetIter(t, _ARG(n))

vector<ORef>::iterator
ObjectVector::insertRange(vector<ORef>::iterator pos, bool& handled,
                          OArg* argv, Session* m) {
  const auto argTypeId = _ARG(0)->getClassId();
  const auto d = std::distance(this->begin(), pos);
  switch (argTypeId) {
    case CLSID_ULONGVECITER:
      std::transform(VEC_ITER(ulong, 0), VEC_ITER(ulong, 1),
          std::inserter(*this, pos), [m](const ulong value) {
            ORef o = BTI::getTypeClass<ulong>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(ulong, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_LONGVECITER:
      std::transform(VEC_ITER(long, 0), VEC_ITER(long, 1),
          std::inserter(*this, pos), [m](const long value) {
            ORef o = BTI::getTypeClass<long>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(long, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_UINTVECITER:
      std::transform(VEC_ITER(uint, 0), VEC_ITER(uint, 1),
          std::inserter(*this, pos), [m](const uint value) {
            ORef o = BTI::getTypeClass<uint>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(uint, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_INTVECITER:
      std::transform(VEC_ITER(int, 0), VEC_ITER(int, 1),
          std::inserter(*this, pos), [m](const int value) {
            ORef o = BTI::getTypeClass<int>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(int, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_SHORTVECITER:
      std::transform(VEC_ITER(short, 0), VEC_ITER(short, 1),
          std::inserter(*this, pos), [m](const short value) {
            ORef o = BTI::getTypeClass<short>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(short, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_CHARVECITER:
      std::transform(VEC_ITER(char, 0), VEC_ITER(char, 1),
          std::inserter(*this, pos), [m](const char value) {
            ORef o = BTI::getTypeClass<char>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(char, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_DBLVECITER:
      std::transform(VEC_ITER(double, 0), VEC_ITER(double, 1),
          std::inserter(*this, pos), [m](const double value) {
            ORef o = BTI::getTypeClass<double>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(double, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_FLTVECITER:
      std::transform(VEC_ITER(float, 0), VEC_ITER(float, 1),
          std::inserter(*this, pos), [m](const float value) {
            ORef o = BTI::getTypeClass<float>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(float, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_BOOLVECITER:
      std::transform(VEC_ITER(bool, 0), VEC_ITER(bool, 1),
          std::inserter(*this, pos), [m](const bool value) {
            ORef o = BTI::getTypeClass<bool>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(bool, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_ULONGSETITER:
      std::transform(SET_ITER(ulong, 0), SET_ITER(ulong, 1),
          std::inserter(*this, pos), [m](const ulong value) {
            ORef o = BTI::getTypeClass<ulong>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(ulong, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_LONGSETITER:
      std::transform(SET_ITER(long, 0), SET_ITER(long, 1),
          std::inserter(*this, pos), [m](const long value) {
            ORef o = BTI::getTypeClass<long>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(long, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_DBLSETITER:
      std::transform(SET_ITER(double, 0), SET_ITER(double, 1),
          std::inserter(*this, pos), [m](const double value) {
            ORef o = BTI::getTypeClass<double>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            _P(double, o)->setValue(value);
            return o;
          });
      break;
    case CLSID_STRSETITER:
      std::transform(SET_ITER(string, 0), SET_ITER(string, 1),
          std::inserter(*this, pos), [m](const string& value) {
            ORef o = BTI::getTypeClass<string>()->createObject(m);
            if (m->exceptionPending()) throw string{"error 0"};
            *_PStr(o) = value;
            return o;
          });
      break;

    case CLSID_OBJVECITER:
      std::transform(VEC_ITER(ORef, 0), VEC_ITER(ORef, 1),
          std::inserter(*this, pos), [m](const_ORef e) {
            ORef o = m->duplicateObject(e);
            if (m->exceptionPending()) throw string{"error 2"};
            return o;
          });
      break;
    case CLSID_OBJSETITER:
      std::transform(SET_ITER(ORef, 0), SET_ITER(ORef, 1),
          std::inserter(*this, pos), [m](const_ORef e) {
            ORef o = m->duplicateObject(e);
            if (m->exceptionPending()) throw string{"error 2"};
            return o;
          });
      break;

    default:
      handled = false;
      return this->end();
  }
  handled = true;
  return this->begin() + d;
}

bool ObjectVector::assignRange(OArg* argv, Session* m) {
  _ASSERT(this->empty(), m);
  bool handled;
  insertRange(this->begin(), handled, argv, m);
  return handled;
}

#undef SET_ITER
#undef VEC_ITER

//:::::::::::::::::::::::::::::  Object Methods  ::::::::::::::::::::::::::::://

template<typename t>
  void ObjectVector::transferItems(const vector<t>& from, Session* m) {
    for (const t item : from) {
      ORef o = BTI::getTypeClass<t>()->createObject(m);
      if (m->exceptionPending())
        break;
      _P(t, o)->setValue(item);
      push_back(o);
    }
  }

template<>
  void ObjectVector::transferItems(const vector<ORef>& from, Session* m) {
    for (const ORef item : from) {
      OVar var{item};
      ORef o = m->dupRenderObject(&var);
      if (m->exceptionPending())
        break;
      push_back(o);
    }
  }

ORet ObjectVector::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(getFullMethodName(S_value), 1);
  _MUTABLE_REC();
  constexpr auto thisTypeId = BTI::getObjectVectorClassId();
  if (_ARG(0)->getClassId() == thisTypeId && (argv[0]->movable() ||
                                              argv[0]->disposable()))
    this->swap(_ARG(0)->castRef<ObjectVector>());
  else if (_ARG_TYPEID(0, thisTypeId))
    static_cast<ObjectVector_Class*>(getClass())->copyCore(this, _ARG(0), m);
  else {
    const auto n = size();
    ObjectAutoCleaner itemCleaner{m, std::move(*this)};
    clear();
    switch (_ARG(0)->getRootClassId()) {
      case CLSID_ULONGVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<ulong>>(), m); break;
      case CLSID_LONGVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<long>>(), m); break;
      case CLSID_UINTVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<uint>>(), m); break;
      case CLSID_INTVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<int>>(), m); break;
      case CLSID_SHORTVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<short>>(), m); break;
      case CLSID_CHARVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<char>>(), m); break;
      case CLSID_BOOLVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<bool>>(), m); break;
      case CLSID_PTRVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<void*>>(), m); break;
      case CLSID_DBLVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<double>>(), m); break;
      case CLSID_FLTVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<float>>(), m); break;
      case CLSID_REFVECTOR:
        transferItems(_ARG(0)->castConstRef<OVector<ORef>>(), m); break;
      default:
        transferItems(std::vector<ORef>(n, _ARG(0)), m); break;
    }
    _CHECK_EXCEPTION();
  }
  return makeResult();
}

ORet ObjectVector::ommInit(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName(S_init));
  _ASSERT(this->empty(), m);
  this->resize(argc);
  for (int i = 0; i < argc; ++i) {
    (*this)[i] = m->dupRenderObject(argv[i]);
    _CHECK_EXCEPTION();
  }
  return makeResult();
}

// void assign (size_type n, const value_type& val);
// rec size: arg0 value: arg1;
ORet ObjectVector::ommSizeValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(getFullMethodName("size:value:"), 2);
  _MUTABLE_REC();
  // unpack parameters
  size_t size = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  // clear and resize vector
  clearItems(m, false);
  _CHECK_EXCEPTION();
  this->resize(size);
  // add elements
  for (size_t i = 0; i < size; ++i) {
    (*this)[i] = m->dupRenderObject(argv[1]);
    _CHECK_EXCEPTION();
  }
  return makeResult();
}

// rec component: arg0;
ORet ObjectVector::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName("component:"));
  const size_t i = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  if (i >= this->size())
    _THROW_RTE("argument out of range");
  const ushort usage = (m->getReceiver()->isConst() ? USG_CONST : 0) | USG_REF;
  return OVariable{(*this)[i], usage};
}

// rec component: arg0 value: arg1;
ORet ObjectVector::ommComponentValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(getFullMethodName("component:value:"), 2);
  _MUTABLE_REC();
  const size_t i = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  if (i >= size())
    _THROW_RTE("argument out of range");
  ORef& element = (*this)[i];
  m->deleteObject(element);
  _CHECK_EXCEPTION();
  element = m->dupRenderObject(argv[1]);
  _CHECK_EXCEPTION();
  return makeResult();
}

// rec size: numItems;
ORet ObjectVector::ommResize(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(getFullMethodName("size:"), 1);
  _MUTABLE_REC();
  const size_t size = s_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  if (this->size() > size) {
    for (size_t i = size; i < this->size(); ++i) {
      m->deleteObject((*this)[i]);
      _CHECK_EXCEPTION();
    }
  }
  this->resize(size);
  return makeResult();
}

// rec size;
ORet ObjectVector::ommSize(OArg rec, int, OArg*, Session* m) {
  ORef result = BTI::getTypeClass<size_t>()->createObject(m);
  _RETHROW(getFullMethodName("size"));
  _P(size_t, result)->setValue(this->size());
  return result->disposableResult();
}

// rec pushBack: arg;
ORet ObjectVector::ommPushBack(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("pushBack:"));
  ORef item = m->dupRenderObject(argv[0]);
  _RETHROW(getFullMethodName("pushBack:"));
  this->push_back(item);
  return makeResult();
}

// recv popBack;
ORet ObjectVector::ommPopBack(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("popBack"));
  m->deleteObject(back());
  _RETHROW(getFullMethodName("popBack"));
  this->pop_back();
  return makeResult();
}

// rec empty;
ORet ObjectVector::ommEmpty(OArg rec, int, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW(getFullMethodName("empty"));
  _P(bool, r)->setValue(this->empty());
  return r->disposableResult();
}

ORet ObjectVector::ommFront(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) _THROW_RTE_METHOD(getFullMethodName("front"),
      "method was incorrectly called");
  const ushort usage = (m->getReceiver()->isConst() ? USG_CONST : 0) | USG_REF;
  return OVariable{this->front(), usage};
}

ORet ObjectVector::ommBack(OArg rec, int, OArg*, Session* m) {
  if (this->empty()) _THROW_RTE_METHOD(getFullMethodName("back"),
      "method was incorrectly called");
  const ushort usage = (m->getReceiver()->isConst() ? USG_CONST : 0) | USG_REF;
  return OVariable{this->back(), usage};
}

ORet ObjectVector::ommShrinkToFit(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("shrinkToFit"));
  this->shrink_to_fit();
  return makeResult();
}

ORet ObjectVector::ommData(OArg rec, int, OArg*, Session* m) {
  ORef r = OBasicTypeInfo::getTypeClass<void*>()->createObject(m);
  _RETHROW(getFullMethodName("data"));
  _P(void*, r)->setValue(static_cast<void*>(this->data()));
  return r->disposableResult();
}

//iterator erase(const_iterator position);
ORet ObjectVector::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName("erase:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<ORef>(), _ox_method_name);
  // unpack parameters
  auto& it = _ARG(0)->castConstRef<OVectorIterator<ORef>>();
  // do the call
  m->deleteObject(*it);
  _CHECK_EXCEPTION();
  auto retValue = this->erase(it);
  // pack and return result
  ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<ORef>>() = std::move(retValue);
  return r->disposableResult();
}

//iterator erase(const_iterator first, const_iterator last);
ORet ObjectVector::ommEraseRange(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName("eraseFrom:to:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<ORef>(), _ox_method_name);
  _CHECK_BAD_ARG(1, BTI::getVectorIteratorClassId<ORef>(), _ox_method_name);
  // unpack parameters
  const auto& from = _ARG(0)->castConstRef<OVectorIterator<ORef>>();
  const auto& to   = _ARG(1)->castConstRef<OVectorIterator<ORef>>();
  // do the call
  for (auto it = from; it != to; ++it) {
    m->deleteObject(*it);
    _CHECK_EXCEPTION();
  }
  auto retValue = this->erase(from, to);
  // pack and return result
  ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<ORef>>() = std::move(retValue);
  return r->disposableResult();
}

ORet ObjectVector::ommClear(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("clear"));
  clearItems(m);
  _RETHROW(getFullMethodName("clear"));
  return makeResult();
}

//:::::::::::::::::::::::::::   vector -- insert   ::::::::::::::::::::::::::://
// iterator insert (const_iterator position, const value_type& val);
// vec insert: val at: position;
ORet ObjectVector::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName("insert:at:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(1, BTI::getVectorIteratorClassId<ORef>(), _ox_method_name);
  // unpack parameters
  ORef value = m->dupRenderObject(argv[0]);
  _CHECK_EXCEPTION();
  const auto& pos = _ARG(1)->castConstRef<OVectorIterator<ORef>>();
  // do the call
  auto retValue = this->insert(pos, value);
  // pack and return result
  ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<ORef>>() = std::move(retValue);
  return r->disposableResult();
}

// vec insert: val size: n at: position;
ORet ObjectVector::ommInsertSize(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD_NOCHK(getFullMethodName("insert:size:at:"));
  _MUTABLE_REC();
  _CHECK_BAD_ARG(2, BTI::getVectorIteratorClassId<ORef>(), _ox_method_name);
  // unpack parameters
  const size_t size = s_castValue<size_t>(argv + 1, m);
  _CHECK_EXCEPTION();
  auto& pos = _ARG(2)->castConstRef<OVectorIterator<ORef>>();
  // do the call
  std::unique_ptr<ORef[]> a{new ORef[size]};
  for (size_t i = 0; i < size; ++i) {
    ORef value = m->dupRenderObject(argv[0]);
    _CHECK_EXCEPTION();
    a[i] = value;
  }
  auto retValue = this->insert(pos, a.get(), a.get() + size);
  // pack and return result
  ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OVectorIterator<ORef>>() = std::move(retValue);
  return r->disposableResult();
}

// iterator insert(const_iterator pos, InputIterator first, InputIterator last)
ORet ObjectVector::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("insertAt:from:to:"));
  _CHECK_BAD_ARG(0, BTI::getVectorIteratorClassId<ORef>(),
      getFullMethodName("insertAt:from:to:"));
  auto pos = _ARG(0)->castRef<OVectorIterator<ORef>>();
  _DEBUG(                                                               \
    if (!validateIterator(*static_cast<vector<ORef>*>(this), pos))      \
      _THROW_RTE_METHOD(s_getFullMethodName<ORef>("insertAt:from:to:"), \
          "iterator does not belong to vector");                        \
  )
  if (_ARG(1)->getClassId() != _ARG(2)->getClassId())
    _THROW_RTE_METHOD(s_getFullMethodName<ORef>("insertAt:from:to:"),
        "iterator type mismatch; expected '"+_ARG(1)->getClassName()+
        "' found '"+_ARG(2)->getClassName()+"'");
  // try to handle compatible types
  bool handled;
  vector<ORef>::iterator it;
  try {
    it = insertRange(pos, handled, argv + 1, m);
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("insertAt:from:to:"));
  }
  if (handled) {
    // pack result
    ORef r = BTI::getVectorIteratorClass<ORef>()->createObject(m);
    _RETHROW(getFullMethodName("insertAt:from:to:"));
    r->castRef<OVectorIterator<ORef>>() = std::move(it);
    return r->disposableResult();
  }
  // run iteration loop
  OInsertLoop<ORef, vector<ORef>> loop{*this, pos, true,
      [this]() -> std::pair<string, string> {
        string methodName = getFullMethodName("insertAt:from:to:");
        return {BTI::getObjectVectorClass()->name, methodName};
      }
  };
  ORet result = loop.run(argv + 1, m);
  _RETHROW(getFullMethodName("insertAt:from:to:"));
  return result;
}

//:::::::::::::::::::::::::::   vector -- assign   ::::::::::::::::::::::::::://
// void assign (InputIterator first, InputIterator last);
ORet ObjectVector::ommAssignRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("assignFrom:to:"));
  // clear vector
  this->clearItems(m, true);
  _RETHROW(getFullMethodName("assignFrom:to:"));
  if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
    _THROW_RTE_METHOD(getFullMethodName("assignFrom:to:"),
        "iterator type mismatch; expected '"+_ARG(0)->getClassName()+
        "' found '"+_ARG(1)->getClassName()+"'");
  // try to handle compatible types
  try {
    const bool handled = assignRange(argv, m);
    if (handled) return makeResult();
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("assignFrom:to:"));
  }
  // run iteration loop
  OAssignLoop<ORef, vector<ORef>> loop{*this, true,
      [this]() -> std::pair<string, string> {
        string methodName = getFullMethodName("assignFrom:to:");
        return {BTI::getObjectVectorClass()->name, methodName};
      }
  };
  loop.run(argv, m);
  _RETHROW(getFullMethodName("assignFrom:to:"));
  return makeResult();
}

//:::::::::::::::::::::::::::::::::   swap   ::::::::::::::::::::::::::::::::://
ORet ObjectVector::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD(getFullMethodName("swap:"), 1);
  _MUTABLE_REC();
  _MUTABLE_ARG(0);
  _CHECK_ARG_CLASS(0, getRootClass()); // == BTI::getVectorClass<ORef>()
  vector<ORef>& r = _ARG(0)->castRef<ObjectVector>();
  this->swap(r);
  return makeResult();
}

//:::::::::::::::::::::::::   vector -- begin, end   ::::::::::::::::::::::::://

ORet ObjectVector::ommBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 0, 0, 0>(this, m); }
ORet ObjectVector::ommCBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 0, 1, 0>(this, m); }
ORet ObjectVector::ommRBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 0, 0, 1>(this, m); }
ORet ObjectVector::ommCRBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 0, 1, 1>(this, m); }

ORet ObjectVector::ommEnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 1, 0, 0>(this, m); }
ORet ObjectVector::ommCEnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 1, 1, 0>(this, m); }
ORet ObjectVector::ommREnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 1, 0, 1>(this, m); }
ORet ObjectVector::ommCREnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ORef, 1, 1, 1>(this, m); }

//::::::::::::::::::::::::::::::::   Ranges   :::::::::::::::::::::::::::::::://

ORet ObjectVector::ommAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ORef, 0, 0>(this, m); }
ORet ObjectVector::ommRAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ORef, 0, 1>(this, m); }
ORet ObjectVector::ommCAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ORef, 1, 0>(this, m); }
ORet ObjectVector::ommCRAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ORef, 1, 1>(this, m); }

//::::::::::::::::::::::::::::::  Method Table  :::::::::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(ObjectVector, OInstance)
  PUBLIC (ObjectVector::ommAssignRange,      "assignFrom:to:"    )
  PUBLIC (ObjectVector::ommBack,             "back"              )
  PUBLIC (ObjectVector::ommComponent,        "component:"        )
  PUBLIC (ObjectVector::ommComponent,        "getItem:"          )
  PUBLIC (ObjectVector::ommComponentValue,   "component:value:"  )
  PUBLIC (ObjectVector::ommClear,            "clear"             )
  PUBLIC (ObjectVector::ommData,             "data"              )
  PUBLIC (ObjectVector::ommEmpty,            "empty"             )
  PUBLIC (ObjectVector::ommErase,            "erase:"            )
  PUBLIC (ObjectVector::ommEraseRange,       "eraseFrom:to:"     )
  PUBLIC (ObjectVector::ommFront,            "front"             )
  PUBLIC (ObjectVector::ommInsert,           "insert:at:"        )
  PUBLIC (ObjectVector::ommInsertSize,       "insert:size:at:"   )
  PUBLIC (ObjectVector::ommInsertRange,      "insertAt:from:to:" )
  PUBLIC (ObjectVector::ommInit,             S_init              )
  PUBLIC (ObjectVector::ommValue,            S_value             )
  PUBLIC (ObjectVector::ommSize,             "size"              )
  PUBLIC (ObjectVector::ommSizeValue,        "size:value:"       )
  PUBLIC (ObjectVector::ommResize,           "size:"             )
  PUBLIC (ObjectVector::ommPushBack,         "pushBack:"         )
  PUBLIC (ObjectVector::ommPopBack,          "popBack"           )
  PUBLIC (ObjectVector::ommShrinkToFit,      "shrinkToFit"       )
  PUBLIC (ObjectVector::ommSwap,             "swap:"             )
  PUBLIC (ObjectVector::ommBegin,            "begin"             )
  PUBLIC (ObjectVector::ommCBegin,           "cbegin"            )
  PUBLIC (ObjectVector::ommRBegin,           "rbegin"            )
  PUBLIC (ObjectVector::ommCRBegin,          "crbegin"           )
  PUBLIC (ObjectVector::ommEnd,              "end"               )
  PUBLIC (ObjectVector::ommCEnd,             "cend"              )
  PUBLIC (ObjectVector::ommREnd,             "rend"              )
  PUBLIC (ObjectVector::ommCREnd,            "crend"             )
  PUBLIC (ObjectVector::ommAll,              "all"               )
  PUBLIC (ObjectVector::ommRAll,             "rall"              )
  PUBLIC (ObjectVector::ommCAll,             "call"              )
  PUBLIC (ObjectVector::ommCRAll,            "crall"             )
_END_METHOD_TABLE()

//:::::::::::::::::::::::::::  Metaclass Methods  :::::::::::::::::::::::::::://

void ObjectVector_Class::destructCore(ORef self, Session* m) {
  ObjectVector* _this = static_cast<ObjectVector*>(self);
  _this->clearItems(m, false);
  if (!freeCached<ObjectVector>(self)) delete _this;
}

ORef ObjectVector_Class::copyCore(ORef self, const_ORef r, Session* m) const {
  _ENTER_NOCHK("ObjectVector_Class::copyCore");
  auto* _this = _PObjVector(self);
  const auto& rhs = *_PCObjVector(r);
  _this->clearItems(m, false);
  _RETHROW_HERE_RETURN(nullptr);
  *static_cast<vector<ORef>*>(_this) = rhs;
  _this->copyItems(rhs, m);
  _RETHROW_HERE_RETURN(nullptr);
  return self;
}

ORef ObjectVector_Class::moveCore(ORef self, ORef r) const {
  if (r->getRootClass() != getRootClass()) return nullptr;
  ObjectVector* const _this = static_cast<ObjectVector*>(self);
  _this->swap(*static_cast<ObjectVector*>(r));
  return self;
}

OClass* ObjectVector_Class::createClass(Session* m,
    const string& className) const {
  _ENTER_NOCHK("ObjectVector_Class::createClass");
  OClass* cls = new ObjectVector_Class(m, this->name, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

OClass* ObjectVector_Class::createMutableClass(Session* m,
    const string& className) const {
  _ENTER_METHOD_NOCHK("ObjectVector_Class::createMutableClass");
  OClass* cls = new ObjectVector_Class(m, this, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

//============================================================================//
//:::::::::::::::::::::::::::   Class Installer   :::::::::::::::::::::::::::://
//============================================================================//

bool g_installVectorClasses(Session* m) {
  OClass* cls;
  extern OClass* _OXType_class[];

  // Vector classes
  cls = new OVector_Class<ulong>(m, "ulong vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_ULONGVECTOR);
  _OXType_class[CLSID_ULONGVECTOR] = cls;

  cls = new OVector_Class<long>(m, "long vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_LONGVECTOR);
  _OXType_class[CLSID_LONGVECTOR] = cls;

  cls = new OVector_Class<unsigned>(m, "unsigned vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_UINTVECTOR);
  _OXType_class[CLSID_UINTVECTOR] = cls;

  cls = new OVector_Class<int>(m, "int vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_INTVECTOR);
  _OXType_class[CLSID_INTVECTOR] = cls;

  cls = new OVector_Class<short>(m, "short vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_SHORTVECTOR);
  _OXType_class[CLSID_SHORTVECTOR] = cls;

  cls = new OVector_Class<char>(m, "char vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_CHARVECTOR);
  _OXType_class[CLSID_CHARVECTOR] = cls;

  cls = new OVector_Class<double>(m, "double vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_DBLVECTOR);
  _OXType_class[CLSID_DBLVECTOR] = cls;

  cls = new OVector_Class<float>(m, "float vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_FLTVECTOR);
  _OXType_class[CLSID_FLTVECTOR] = cls;

  cls = new OVector_Class<bool>(m, "bool vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_BOOLVECTOR);
  _OXType_class[CLSID_BOOLVECTOR] = cls;

  cls = new OVector_Class<void*>(m, "pointer vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_PTRVECTOR);
  _OXType_class[CLSID_PTRVECTOR] = cls;

  cls = new OVector_Class<ORef>(m, "reference vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_REFVECTOR);
  _OXType_class[CLSID_REFVECTOR] = cls;

  cls = new ObjectVector_Class(m, "vector");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_OBJVECTOR);
  _OXType_class[CLSID_OBJVECTOR] = cls;

  return true;
}
