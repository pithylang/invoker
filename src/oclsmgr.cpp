// #include <iostream>
#include <algorithm>
#include <utility>
#include "oclass.hpp"
#include "odev.h"
#include "onullobj.hpp"
#include "otypes.h"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "oclsmgr.hpp"
#include "oreqs.hpp"
#include "odefs.h"
#include "selectors.h"

using std::string;
using std::vector;

OClass* _OM_LINK _OXType_class[_NUM_BASE_CLASSES];

bool g_installTypeClasses            (Session*);
bool g_installTypeProxyClasses       (Session*);
bool g_installVectorClasses          (Session*);
bool g_installIteratorClasses        (Session*);
bool g_installVectorIteratorClasses  (Session*);
bool g_installExceptionClasses       (Session*);
bool g_installStringIteratorClasses  (Session*);
_DECLARE_CLASS_INSTALLER             (Block);
_DECLARE_CLASS_INSTALLER             (NullObject);
bool g_installSetClasses             (Session*);
bool g_installSetIteratorClasses     (Session*);
// bool InstallStreamClasses           (Session*);
// bool InstallLocaleClasses           (Session*);
// _DECLARE_CLASS_INSTALLER            (ReverseIterator);
bool g_installRangeClass             (Session*);
_DECLARE_CLASS_INSTALLER             (Package);
_DECLARE_CLASS_INSTALLER             (Facet);
_DECLARE_CLASS_INSTALLER             (Locale);

OClass* OClassMgr::initRuntimeSystem() {
  // Initialize basic classes
  _OXType_class[0] = nullptr;
  // Create RT manager class
  OClass* cls = new ORTMgr_Class{this};
  if (!cls) return nullptr;
  cls->finalize(nullptr);
  ASSERT(cls->getClassId() == CLSID_RTMGR);
  _OXType_class[CLSID_RTMGR] = cls;
  return cls;
}

bool OClassMgr::initClasses(Session* m) {
  OClass* cls;

  // Base class
  cls = new OClass(m, "object");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT (cls->getClassId() == CLSID_OCLASS);
  _OXType_class[CLSID_OCLASS] = cls;

  // Type classes
  if (!g_installTypeClasses(m)) return false;

  cls = new _oxC(String)(m);
  if (!cls) return false;
  cls->finalize(m);
  ASSERT (cls->getClassId() == CLSID_STRING);
  _OXType_class[CLSID_STRING] = cls;

  // Resolved selector object --> removed
/*
  // Application class
  cls = new OApp_Class(m);
  if (!cls) return false;
  cls->finalize(m);
  ASSERT (cls->getClassId() == CLSID_APP);
  _OXType_class[CLSID_APP] = cls;
*/
  // Type proxy classes
  if (!g_installTypeProxyClasses(m)) return false;

  // Vector classes
  if (!g_installVectorClasses(m)) return false;

  // Iterator classes
  if (!g_installIteratorClasses(m)) return false;
  if (!g_installVectorIteratorClasses(m)) return false;

  // Wide character string (deprecated)
  cls = new oxWString_Class(m);
  if (!cls) return false;
  cls->finalize(m);
  ASSERT (cls->getClassId() == CLSID_WSTRING);
  _OXType_class[CLSID_WSTRING] = cls;

  // Exception classes
  if (!g_installExceptionClasses(m)) return false;

  // String iterators
  if (!g_installStringIteratorClasses(m)) return false;

  // Block class (used by VM)
  cls = g_installBlockClass(m);
  if (!cls) return false;
  _ASSERT(cls->getClassId() == CLSID_BLOCK, m);
  _OXType_class[CLSID_BLOCK] = cls;

  // Null object class
  cls = g_installNullObjectClass(m);
  if (!cls) return false;
  _ASSERT(cls->getClassId() == CLSID_NULLOBJ, m);
  _OXType_class[CLSID_NULLOBJ] = cls;

  const_cast<oxNullObject&>(nullObject_).setClass(cls);

  // Set and set iterator classes
  if (!g_installSetClasses(m)) return false;
  if (!g_installSetIteratorClasses(m)) return false;

  // Range class
  if (!g_installRangeClass(m)) return false;

  // Package class
  cls = g_installPackageClass(m);
  if (!cls) return false;
  _ASSERT(cls->getClassId() == CLSID_PACKAGE, m);

  // Stream classes --> separate package
  // Reverse Iterator class --> removed

  // Facet class
  cls = g_installFacetClass(m);
  if (!cls) return false;
  _ASSERT(cls->getClassId() == CLSID_FACET, m);

  // Locale class
  cls = g_installLocaleClass(m);
  if (!cls) return false;
  _ASSERT(cls->getClassId() == CLSID_LOCALE, m);

  // Finally register range operations
  _oxC(String)::registerOperations(m, true);

  return true;
}

void OClassMgr::term() {
  vector<OClass*> types;
  types.reserve(classTable_.size());
  for (auto& [_, typeAlias] : classTable_) {
    auto& [type, alias] = typeAlias;
    if (!alias)
      types.push_back(type);
  }
  classTable_.clear();
  for (OClass* type : types)
    delete type;
}

OClass* OClassMgr::getClass(const std::string& s) const {
  auto it = classTable_.find(s);
  return it == classTable_.end() ? nullptr : it->second.first;
}

OClass* OClassMgr::getClass(int classId) const {
  return classId < MAX_NUM_CLASSES ?
      classList_.at(classId) : getMutableClass(classId);
}

OClass* OClassMgr::getMutableClass(int classId) const {
  auto it = mutableClasses_.find(classId);
  return it == mutableClasses_.end() ? nullptr : it->second;
}

bool OClassMgr::registerClass(OClass* thisClass) {
  ASSERT(!thisClass->isMutable());
  auto& newRegistration = classTable_[thisClass->name];
  if (newRegistration.first != nullptr) return false; // already registered
  newRegistration = std::make_pair(thisClass, false);
  classList_.push_back(thisClass);
  return true;
}

void OClassMgr::deregClass(OClass* thisClass) {
  ASSERT(!thisClass->isMutable());
  const auto begin = classTable_.begin();
  std::for_each(begin, classTable_.end(), [this, thisClass](auto& item) {
    if (item.second.first == thisClass) classTable_.erase(item.first);
  });
  classList_.at(thisClass->getClassId()) = nullptr;
}

bool OClassMgr::isClass(OClass* cls) const {
  return cls &&
      std::find(classList_.begin(), classList_.end(), cls) == classList_.end() ?
          std::find_if(mutableClasses_.begin(), mutableClasses_.end(),
              [cls](const decltype(mutableClasses_)::value_type& item)
              { return item.second == cls; }) != mutableClasses_.end() : true;
}

bool OClassMgr::aliasClass(OClass* type, const string& newTypeName) {
  if (std::find(classList_.begin(), classList_.end(), type) == classList_.end())
    return false;
  auto& newRegistration = classTable_[newTypeName];
  if (newRegistration.first != nullptr) return false; // already registered
  newRegistration = std::make_pair(type, true);
  return true;
}

bool OClassMgr::isAlias(const string& typeName) const {
  const auto it = classTable_.find(typeName);
  return it != classTable_.end() && it->second.second;
}

bool OClassMgr::isAlias(OClass* type) const {
  for (const auto& [_, typeAlias] : classTable_) {
    const auto& [itemType, alias] = typeAlias;
    if (itemType == type && alias) return true;
  }
  return false;
}

int OClassMgr::issueMutableTypeId(OClass* type, Session* m) noexcept {
  _ENTER_NOCHK("OClassMgr::issueMutableTypeId");
  const int id = mutableTypeIdGenerator_.get();
  try {
    auto& value = mutableClasses_[id];
    _PROG_ERROR_IF(value != nullptr);
    value = type;
  }
  catch (const string& msg) {
    m->throwRTE(_ox_method_name, msg);
    return 0;
  }
  catch (...) {
    m->throwRTE(_ox_method_name, "error associating mutable type id with type",
        __FILE__, __LINE__);
    return 0;
  }
  return id;
}

void OClassMgr::releaseMutableTypeId(int id) noexcept {
  mutableTypeIdGenerator_.release(id);
  mutableClasses_.erase(id);
}

//:::::::::::::::::::::::::::::  API functions  :::::::::::::::::::::::::::::://

extern "C" {

Session* omInitialize() {
  ORef rtmObject = nullptr;
  OClassMgr *classMgr = new OClassMgr;
  if (classMgr == nullptr) return nullptr;
  OClass *rtmClass = classMgr->initRuntimeSystem();
  if (rtmClass != nullptr && !rtmClass->ctorFailed())
    rtmObject = rtmClass->createObject(nullptr);
  if (rtmObject != nullptr) {
    Session *m = _PRTMgr(rtmObject);
    m->classMgr = classMgr;
    const bool success = classMgr->initClasses(m);
    if (success) return m;
    classMgr->term();
  }
  delete rtmObject;
  delete rtmClass;
  delete classMgr;
  return nullptr;
}

void omTerminate(Session* m, bool destroyClasses) {
  ORef rtmObject = static_cast<ORTMgr*>(m);
  OClassMgr *classMgr = m->classMgr;
  rtmObject->getClass()->deleteObject(rtmObject, m);
  if (destroyClasses) {
    classMgr->term(); // deletes also runtime manager classs
    delete classMgr;
  }
}

OClass *omGetClass(Session *m, const char *className)
{ return m == nullptr ? nullptr : m->classMgr->getClass(className); }

} // extern "C"
