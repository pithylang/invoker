#include "onullobj.hpp"
#include "ortmgr.hpp"
#include "otype.hpp"

using std::string;

_BEGIN_METHOD_TABLE(oxNullObject, OInstance)
  PUBLIC (oxNullObject::ommIsNull, "isNull")
_END_METHOD_TABLE()

OClass* oxNullObject_Class::createClass(Session* m,
    const std::string& c) const {
  OClass* cls = new oxNullObject_Class(m, this->name, c);
  m->rethrow("oxNullObject_Class::createClass");
  return cls;
}

OClass* oxNullObject_Class::createMutableClass(Session* m,
    const std::string& c) const {
  OClass* cls = new oxNullObject_Class(m, this, c);
  m->rethrow("oxNullObject_Class::createMutableClass");
  return cls;
}

void oxNullObject_Class::destructCore(OInstance* self, Session* m) {
  if (!m->isStdNullObject(self))
    delete static_cast<OT*>(self);
}

ORef oxNullObject_Class::copyCore(ORef tgt, const_ORef src, Session*) const {
  if (src->getRootClass() != getRootClass()) return nullptr;
  *static_cast<OT*>(tgt) = *static_cast<const OT*>(src);
  return tgt;
}

ORef oxNullObject_Class::moveCore(ORef tgt, ORef src) const {
  if (src->getRootClass() != getRootClass()) return nullptr;
  *static_cast<OT*>(tgt) = std::move(*static_cast<OT*>(src));
  return tgt;
}

_IMPLEMENT_CLASS_INSTALLER(NullObject)

//============================================================================//
//:::::::::::::::::::::::::::::   oxNullObject   ::::::::::::::::::::::::::::://
//============================================================================//

ORet oxNullObject::ommIsNull(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("null object::isNull");
  ORef o = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(o)->setValue(true);
  _RETHROW_HERE();
  return o->disposableResult();
}
