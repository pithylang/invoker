#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "oclsmgr.hpp"
#include "odev.h"
#include "otypes.h"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "ostring.hpp"
#include "oexcept.hpp"
#include "ovariable.hpp"
#include "ovector.hpp"
#include "oset.hpp"
#include "oclassobj.hpp"
#include "oreqs.hpp"
#include "obasicTypeInfo.hpp"
#include <cstddef>
#include <functional>
#include <utility>

std::string ul2hexstr(void*); // implemented in oclass.cpp

using std::string;
using std::vector;

// Imported functions
std::tuple<OVar*, int, const string&>
  g_resolveSymbolType(VirtualMachine* vm, int variableOffset, int what);
ORet g_execReturn(VirtualMachine* vm, OArg arg, bool ref);
ORet g_execTryCatch(VirtualMachine* vm, OArg tryBlock,
    const vector<int>& catchBlocks, const vector<int>& doBlocks);

#define _REF(var) (var)->objectRef

//============================================================================//
//::::::::::::::::::::::::::::::::  Session  ::::::::::::::::::::::::::::::::://
//============================================================================//

Session::Session() :
    flags           {    0    },
    parseOptions    {    0    },
    mainMachine_    { nullptr },
    currentMachine_ { nullptr },
    exception_      { nullptr },
    savedException_ { nullptr } {
  checker_.init();
  stringOutput = [](const string& text) {};
  displayMessage = [this](const string& title, const string& msg) {
    string out = title;
    if (!title.empty()) {
      if (title.back() != ':') out += ":";
      out += " ";
    }
    out += msg;
    if (msg.back() != '\n') out += "\n";
    stringOutput(out);
  };
  execScriptProc_ = [this](const std::string&, int, ulong, OArg, int, OArg*) {
    machineInitError();
    return OVar::undefinedResult();
  };
  execMethodProc_ = [this](OClass*, const std::string&, ulong, OArg, int, OArg*)
      -> std::pair<ORet,int> {
    machineInitError();
    return std::pair<ORet,int>{OVar{}, -1};
  };
  execBlockProc_ = [this](int) {
    machineInitError();
    return OVar::undefinedResult();
  };
}

Session::~Session() {
//    m_intTmpStock.clear(this);
//    m_doubleTmpStock.clear(this);
//    m_boolTmpStock.clear(this);
}

void Session::terminate() { if (classMgr != nullptr) classMgr->term(); }

//::::::::::::::::::::::::::   Object Management   ::::::::::::::::::::::::::://

bool Session::isObject(ORef o) const {
  return registerObjects() ? objectTable_.find(o) != objectTable_.end() : true;
}

void Session::deleteObject(ORef o) {
  if (!o) {
    throwBadArg("Session::deleteObject", "null object");
    return;
  }
  OClass* const objectClass = o->getClass();
  if (!objectClass) {
    throwBadArg("Session::deleteObject",
                "cannot delete typeless object at 0x"+ul2hexstr(o));
    return;
  }
  objectClass->deleteObject(o, this);
  if (exceptionPending())
    rethrow("Session::deleteObject");
}

void Session::registerObject(ORef id) {
  if (registerObjects() && !objectTable_.insert(id).second) {
    if (flags & RTFL_EXIT_ON_OBJECT_REG_FAIL) exit(2);
    throwRTE("Session::registerObject",
             "could not register object at 0x"+ul2hexstr(id));
  }
}

void Session::deregisterObject(ORef id) {
  if (registerObjects() && !objectTable_.erase(id) && !isStdNullObject(id)) {
    if (flags & RTFL_EXIT_ON_OBJECT_DEREG_FAIL) exit(2);
    if (flags & RTFL_RTE_ON_OBJECT_DEREG_FAIL) {
#if 0 // TODO
      logMessage("Session::deregisterObject",
                 "could not deregister object at 0x"+ul2hexstr(id));
#endif // 0
    }
  }
}

vector<string> Session::getInfoRegisteredObjects() {
  vector<string> r;
  for (const auto o : objectTable_) {
    const string type = o->getClass() ? o->getClassName() : "unknown";
    r.push_back("0x"+ul2hexstr(o)+" type "+type);
  }
  return r;
}

void Session::discardObjects(const vector<ORef>& from) {
  discardedObjects_.insert(discardedObjects_.end(), from.begin(), from.end());
}

ORef Session::duplicateObject(const_ORef o) {
  ORef replica = o->getClass()->createObject(this);
  if (exceptionPending()) return nullptr;
  replica->copyObject(o, this);
  return replica;
}

void Session::deleteMutableClass(OClass* type) const {
  _ASSERT(type->isMutable(), this);
  const int typeId = type->getClassId();
  _ASSERT(classMgr->getClass(typeId) != nullptr, this);
  delete type;
  classMgr->releaseMutableTypeId(typeId);
}

inline bool Session::isCurrentMachineReturning() const noexcept
{ return g_getMachineValue(getCurrentMachine(), M_IS_RETURNING).get<bool>(); }

OClass* Session::createClassObject(const string& name, const string& baseClass) {
  _ENTER_NOCHK("Session::createClassObject");
  const OClass* b = classMgr->getClass(baseClass);
  if (!b) {
    throwBadClass(
    /* method      */ "Session::createClassObject",
    /* description */ "unable to create class '"+name+"'",
    /* context     */ "class construction",
    /* reason      */ "base class '"+baseClass+"' was not found",
    /* file, line  */ __FILE__, __LINE__);
    return nullptr;
  }
  OClass* c = b->createClass(this, name);
  c->setPrefix(getTypePrefix());
  if (objectCacheConfigData_) {
    ObjectUsage objectUsage = getCacheConfigForType(c->name);
    if (objectUsage.policy != CCP_NONE) {
      try {
        c->createObjectCache(objectUsage);
      }
      catch (const std::logic_error& except) {
        throwBadClass(
        /* method      */ "Session::createClassObject",
        /* description */ "unable to configure class '"+name+"'",
        /* context     */ "class construction",
        /* reason      */ except.what(),
        /* file, line  */ __FILE__, __LINE__);
        return nullptr;
      }
    }
  }
  return c;
}

string Session::getObjectUsageInfo() const {
  string r;
  if (!usageData_) return r;
  for (const auto& [typeId, objectUsage] : *usageData_) {
    OClass* const type = this->getClass(typeId);
    if (!type) continue;
    const string typeName{type->name};
    const string q{typeName.find(' ') == string::npos ? "" : "'"};
    r += q + typeName + q + ' ';
    r += std::to_string(objectUsage.limitup) + ' ';
    r += std::to_string(objectUsage.count) + '\n';
  }
  return r;
}

void Session::configObjectCaches(Session::string_usage_table&& cfg) {
  objectCacheConfigData_ = std::make_unique<string_usage_table>(std::move(cfg));
  auto& objectCacheConfig = *objectCacheConfigData_.get();
  vector<string> toRemove;
  toRemove.reserve(objectCacheConfig.size());
  for (const auto& [typeName, objectUsage] : objectCacheConfig) {
    OClass* const type = classMgr->getClass(typeName);
    if (type && isBuiltinType(type)) {
      type->createObjectCache(objectUsage);
      toRemove.push_back(typeName);
    }
  }
  for (const auto& typeName : toRemove)
    objectCacheConfig.erase(typeName);
  if (objectCacheConfig.empty())
    objectCacheConfigData_.reset();
}

ObjectUsage Session::getCacheConfigForType(const string& typeName) {
  ObjectUsage objectUsage;
  if (objectCacheConfigData_) {
    auto it = objectCacheConfigData_->find(typeName);
    if (it != objectCacheConfigData_->end()) {
      objectUsage = std::move(it->second);
      objectCacheConfigData_->erase(typeName);
      if (objectCacheConfigData_.get()->empty())
        objectCacheConfigData_.reset();
      return objectUsage;
    }
  }
  objectUsage.policy = CCP_NONE;
  return objectUsage;
}

size_t ObjectUsage::getCacheSize() const noexcept {
  switch (policy) {
    case CCP_NONE: return 0;
    case CCP_MAX: return limitup;
    case CCP_MIN: return count;
    case CCP_PC_OF_MIN: return count*(1 + pc/100);
    case CCP_PC_OF_MAX: return limitup*(1 + pc/100);
    case CCP_UNDERCACHE: return count + ((limitup - count)*pc)/100;
  }
  return 0;
}

//:::::::::::::::::::::::::::   Method Execution   ::::::::::::::::::::::::::://

ORet Session::execMethodAddress(const OMethodAddress& methodAddress,
                                const std::string& method, ulong flags,
                                OArg rec, int argc, OArg* argv) {
  _ENTER_NOCHK("Session::execMethodAddress");
  Session* const m = this;
  //const string method{"unknown"};
  OVar r;
  if (methodAddress.scriptMethod) { // script method
    if (!execScriptProc_) {
      m->throwRTE(method, "improperly configured virtual machine");
      return OVar::undefinedResult();
    }
    const auto blockId = methodAddress.scriptMethodAddress.block;
    r = execScriptProc_(method, blockId, flags, rec, argc, argv);
  }
  else { // core method
    const auto& f = methodAddress.coreMethodAddress;
    r = (_REF(rec)->*f)(rec, argc, argv, m);
    if (r.undefined() && !m->exceptionPending()) {
      m->throwRTE(method, "error executing method");
      return OVar::undefinedResult();
    }
  }
  _RETHROW(method);
  return r;
}

//:::::::::::::::::::::::::::::::::   Cast   ::::::::::::::::::::::::::::::::://

ORet Session::valueCast(OClass* type, ORef src, int *prc) {
  _ENTER_NOCHK("Session::valueCast");
  Session* const m = this;
  OVar rec{src, USG_CONST};
  const auto [r, rc] = execMethodNoThrowUntilExec(type->name, &rec, 0, nullptr);
  _RETHROW_HERE();
  if (prc) *prc = rc;
  if (rc == EMR_OK)
    return r;
  if (!prc) {
    const string cv{"from '"+src->getClassName()+"' to '"+type->name+"'"};
    const string msg = (rc == EMR_METHOD_NOT_RESPONDING) ?
      "cannot convert "+cv : "conversion "+cv+" is not accessible";
    throwRTE(_ox_method_name, msg);
  }
  return OVar::undefinedResult();
}

bool Session::checkCastResult(ORet castResult, const OClass* toType,
                                               const OClass* fromType) {
  const bool correctResultType = REF(castResult)->getClass() == toType;
  if (castResult.disposable() && correctResultType) return true;
  const string fromTypeName = fromType->isMutable() ? "mutable object" :
                                                      fromType->name;
  const string description = "conversion method from '"+fromTypeName+"' "
      "to '"+toType->name +"' is improperly implemented";
  const string context = "copy from object type '"+fromTypeName+"' "
      "to object type '"+toType->name+"'";
  const string what[2]{"a disposable object", "an object of the expected type"};
  throwBadClass(
  /*method     */ "Session::checkCastResult",
  /*description*/ description,
  /*context    */ context,
  /*reason     */ "it did not return "+what[correctResultType ? 0 : 1]);
  return false;
}

//:::::::::::::::::::::::::::::::::   Copy   ::::::::::::::::::::::::::::::::://

ORet Session::copyFrom(OArg lhsVar, OArg rhsVar) {
  _ENTER_NOCHK("Session::copyFrom");
  Session* const m = this;
  _ASSERT(!lhsVar->undefined(), m);
  if (rhsVar->undefined())
    _THROW_RTE("cannot copy undefined object");
  if (_REF(lhsVar)->getClassId() == CLSID_NULLOBJ)
    _THROW_RTE("assigned value to null object");
  if (!lhsVar->isMutable())
    _THROW_RTE("cannot modify constant object");
  ORet result = setValueWithCast(lhsVar, rhsVar, false, false);
  _CHECK_EXCEPTION();
  return result;
}

ORet Session::setValueWithCast(OClass* lhsType, OVar* lhsVar, OVar* rhsVar,
                               bool applyValueMethod, bool isInit) {
  _ENTER_NOCHK("Session::setValueWithCast");
  Session* const m = this;
  _ASSERT(!rhsVar->undefined(), m);

  if (!_REF(lhsVar)->typeOf(lhsType))
    _THROW_BAD_USAGE("incorrect lhs object type")

  ORef rhs = _REF(rhsVar);
  // OClass* const lhsType = lhs->getClass();
  OClass* const rhsType = rhs->getClass();

  if (!rhs->typeOf(lhsType)) {
    // the caller is required to check accessibility
    OClass* const caller = getCallingClass();
    ObjectAutoCleaner exceptionCleaner{m};
    ORef valueException = nullptr;
    bool hasValue = false;
    // start off by applying value method
    if (applyValueMethod) {
      // check if lhs has an accessible value method
      const auto r = lhsType->getMethodAccessAddress(S_value, caller);
      hasValue = r.first;
      // try to apply the value method
      if (hasValue) {
        ORet ret = execMethodAddress(r.second, S_value, 0, lhsVar, 1, &rhsVar);
        if (isExceptionBadArg()) {
          valueException = popException();
          exceptionCleaner.add(valueException);
        }
        else if (exceptionPending()) {
          _RETHROW_HERE();
        }
        else if (ret.disposable())
          _THROW_RTE("improper implementation of method '" S_value "' "
                     "(returned disposable result)")
        else // everything was fine
          return _REF(lhsVar)->makeResult();
      }
    }
    // check if lhs has an accessible custom copy method
    const bool hasCopy = lhsType->hasCopyMethod() &&
        lhsType->isMemberAccessible(lhsType->getCachedMethodAccess(2), caller);
    // try to copy
    ORef copyException = nullptr;
    bool nullCopyResult = false;
    if (hasCopy) {
      const auto& a = lhsType->getCachedCopyOperator();
      ORet result = execMethodAddress(a, S_copy, 0, lhsVar, 1, &rhsVar);
      if (isExceptionBadArg()) {
        copyException = popException();
        exceptionCleaner.add(copyException);
      }
      else if (exceptionPending()) {
        _RETHROW_HERE();
      }
      else if (result.undefined())
        nullCopyResult = true;
      else if (result.disposable())
        _THROW_RTE("improper implementation of copy method "
                   "(returned disposable result)")
      else // everything was fine
        return _REF(lhsVar)->makeResult();
    }
    // we can still try to copy a cast value
    // check if rhs has an accessible cast method
    const auto r = rhsType->getMethodAccessAddress(lhsType->name, caller);
    const bool hasCast = r.first;
    if (hasCast) {
      // cast rhs value
      ORet ret = execMethodAddress(r.second, lhsType->name, 0, rhsVar, 0, 0);
      if (!exceptionPending() && checkCastResult(ret, lhsType, rhsType)) {
        _REF(lhsVar)->swapContent(REF(ret));
        deleteObject(REF(ret));
        _CHECK_EXCEPTION();
        return _REF(lhsVar)->makeResult();
      }
      _ASSERT(exceptionPending(), m);
      exceptionCleaner.add(popException());
    }
    else if (!hasValue && !hasCopy) { // no value, no copy and no cast methods
      const string action = isInit ? " initialize " : " copy to ";
      const string message = "cannot "+action+" object type '"+
          lhsType->name+"' from object type '"+rhsType->name+"'";
      throwBadArg(_ox_method_name, message, __FILE__, __LINE__);
      return OVariable::undefinedResult();
    }
    // now handle various error conditions
    const string action = isInit ? "initializing " : "copying to ";
    const string start = action+"object type '"+lhsType->name+"' from "
        "object type '"+rhsType->name+"' failed after";
    auto numFailures = exceptionCleaner.objects.size();
    if (numFailures < 3 && !copyException && nullCopyResult)
      ++numFailures;
    string msg = start;
    if (valueException) {
      msg += " the ineffectual application of method '" S_value "'";
      --numFailures;
      if (numFailures == 2)
        msg += ",";
      else if (numFailures == 1)
        msg += " and";
    }
    if (copyException || nullCopyResult) {
      msg += " an unsuccessful application of the supplied copy method";
      --numFailures;
      if (numFailures == 1)
        msg += " and";
    }
    if (numFailures == 1) {
      msg += " an abortive attempt to copy a cast value";
      --numFailures;
    }
    _THROW_RTE(msg);
  }

  // now rhs is typeof lhs
  ORef lhs = _REF(lhsVar);
  OClass* const c = lhs->getClass();
  return rhsType == c && (rhsVar->movable() || rhsVar->disposable()) ?
      c->moveObject(lhs, rhs, m) : lhsType->copyObject(lhs, rhs, m);
}

//::::::::::::::::::::::::::::::   Exceptions   :::::::::::::::::::::::::::::://

void Session::clearException() noexcept {
  Session* const m = this;
  if (exceptionPending())
    popException()->deleteObject(m);
  if (savedException_)
    std::exchange(savedException_, nullptr)->deleteObject(m);
}

ORef Session::rethrow(const string& method, const string& fileName, int line) {
  if (!exceptionPending()) return nullptr;
  auto e = exception_->getClass()->castException(exception_);
  if (ExceptionFrame{method, fileName, line} == e->front())
    return nullptr;
  e->addFrame(method, fileName, line);
  return nullptr;
}

void Session::throwException(const string& exceptionType,
                             std::function<void(ORef)> initException) {
  if (exceptionPending()) return;
  OClass* exceptionClass = classMgr->getClass(exceptionType);
  if (!exceptionClass) return;
  ORef exception = exceptionClass->createObject(this);
  if (!exception) return;
  initException(exception);
  setException(exception);
}

template<typename T>
auto castExceptionPtr(ORef o) -> typename T::base_class* {
  return static_cast<typename T::base_class*>(static_cast<T*>(o));
}

void Session::throwBadClass(const string& method,
                            const string& description,
                            const string& context,
                            const string& reason,
                            const string& srcFile, int line) {
  throwException("bad class", [&](ORef o) {
    // ExceptionBadClass* e = (ExceptionBadClass*)(oxExceptionBadClass*)o;
    ExceptionBadClass* e = castExceptionPtr<oxExceptionBadClass>(o);
    e->description = description;
    e->context = context;
    e->reason = reason;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwBadArg(const string& method, const string& message,
                          const string& srcFile, int line) {
  throwException("bad arg", [&](ORef o) {
    ExceptionBadArg* e = castExceptionPtr<oxExceptionBadArg>(o);
    e->info = message;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwBadArgc(const string& method,
                           const string& scriptMethod,
                           int expectedArgc,
                           const string& srcFile, int line) {
  throwException("bad argc", [&](ORef o) {
    ExceptionBadArgc* e = castExceptionPtr<oxExceptionBadArgc>(o);
    e->scriptMethodName = scriptMethod;
    e->expectedArgc = expectedArgc;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwBadArgc(const string& method,
                           const string& scriptMethod,
                           const string& message,
                           const string& srcFile, int line) {
  throwException("bad argc", [&](ORef o) {
    ExceptionBadArgc* e = castExceptionPtr<oxExceptionBadArgc>(o);
    e->scriptMethodName = scriptMethod;
    e->info = message;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwBadCoreCopy(const string& method,
                               const string& message,
                               const string& srcFile, int line) {
  throwException("bad core copy", [&](ORef o) {
    auto* e = castExceptionPtr<oxExceptionBadCoreCopy>(o);
    e->info = message;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwBadUsage(const string& method,
                            const string& message,
                            const string& scriptMethod,
                            const string& srcFile, int line) {
  throwException("bad usage", [&](ORef o) {
    ExceptionBadUsage* e = castExceptionPtr<oxExceptionBadUsage>(o);
    e->scriptMethodName = scriptMethod;
    e->info = message;
    e->addFrame(method, srcFile, line);
  });
}

void Session::throwRTE(const string& method, const string& message,
                       const string& srcFile, int line) {
  throwException("runtime error", [&](ORef o) {
    ExceptionRTE* e = castExceptionPtr<oxExceptionRTE>(o);
    e->info = message;
    e->addFrame(method, srcFile, line);
  });
}

ORet Session::throwBadArg(const string& methodName, int i,
                          const string& expectedType,
                          const string& foundType) {
  const string msg = "unexpected type for argument "+std::to_string(i+1)+
                     ": expected "+expectedType+", found '"+foundType+"'";
  throwBadArg(methodName, msg);
  return OVar::undefinedResult();
}

ORet Session::badArgType(const string& methodName, int i,
                         const OClass* type, OArg* argv) {
  return throwBadArg(methodName, i, type->name, _ARG(i)->getClassName());
}

ORet Session::badArgTypeId(const string& methodName, int i,
                           int typeId, OArg* argv) {
  return badArgType(methodName, i, classMgr->getClass(typeId), argv);
}

bool Session::isExceptionBadArg() const {
  if (!exception_) return false;
  OClass* const type = exception_->getClass();
  return type && type->castException(exception_)->what == "bad argument";
}

//:::::::::::::::::::::::::::::   Requirements   ::::::::::::::::::::::::::::://

Requirement* Session::getRequirement(const std::string& reqId) const {
  return classMgr->reqMgr.getRequirement(reqId);
}

Requirement* Session::registerRequirement(Requirement&& req) {
  Requirement* const p = classMgr->reqMgr.registerRequirement(std::move(req));
  if (p) return p;
  const string msg = "requirement '"+req.name+"' is already registered";
  throwRTE("Session::registerRequirement", msg);
  return nullptr;
}

void Session::addRequirement(OClass* c, Requirement&& req) {
  if (classMgr->reqMgr.addRequirement(c, std::move(req))) return;
  const string msg = "requirement '"+req.name+"' is already registered";
  throwRTE("Session::addRequirement", msg);
}

void Session::importRequirement(OClass* c, const std::string& reqId) {
  if (classMgr->reqMgr.importRequirement(c, reqId)) return;
  const string msg = classMgr->reqMgr.getRequirement(reqId) ?
      "type '"+c->name+"' has already a requirement" :
      "undefined requirement '"+reqId+"'";
  throwRTE("Session::importRequirement", msg);
}

//:::::::::::::::::::::::   Create Object From ISON   :::::::::::::::::::::::://

struct ValueDefinition {
  const string* key;
  OVar* varPtr;
  bool isConst, isRef, isPrivate;

  void gatherInfo(Session* m, const char* methodName) {
    isConst = key->at(0) == '!';
    if (isConst && key->length() < 2) {
      const string msg = "'"+*key+"' is not a valid property identifier";
      m->throwRTE(methodName, msg);
    }
    isRef = key->at(key->size() - 1) == '&';
    isPrivate = key->at(isConst ? 1 : 0) == '_';
  }

  void registerProp(Session* m, const char* methodName, OClass* type) const {
    const ushort flags = (isPrivate ? M_PRIVATE : M_PUBLIC) |
                         (isConst ? M_CONST : 0) | (isRef ? M_REF : 0);
    const int rc = type->addMutableTypeProperty(m,
        s_stripConstRef(*key, isConst, isRef),
        isRef ? nullptr : _REF(varPtr)->getClass(), flags);
    if (rc != 0)
      m->throwRTE(methodName, "duplicate property '"+*key+"' for mutable type");
  }

  void assignValue(Session* m, const char* methodName, OVar& lhsVar) const {
    lhsVar.usage = (isConst ? USG_CONST : 0) | (isRef ? USG_REF : 0);
    if (setPropValue(lhsVar, m))
      return;
    if (m->exceptionPending())
      m->rethrow(methodName);
    else
      m->throwRTE(methodName, assignValueErrorMsg());
  }

  string assignValueErrorMsg() const
  { return "cannot assign disposable value to property reference '"+*key+"'"; }

  bool setPropValue(OVar& lhsVar, Session* m) const {
    bool g_deferTmpDeletion(const VirtualMachine*, OVar*);
    if (isRef) {
      if (varPtr->disposable() && !BTI::isBasicTypeProxy(_REF(varPtr)))
        return false; // caller must throw a VM-exception
      REF(lhsVar) = _REF(varPtr);
      if (varPtr->disposable() && BTI::isBasicTypeProxy(_REF(varPtr)))
        if (!g_deferTmpDeletion(m->getCurrentMachine(), varPtr))
          return false;
      return true;
    }
    m->emplace(lhsVar, *varPtr);
    return !m->exceptionPending(); // rethrow pending VM-exception
  }

  static string s_stripConstRef(const string& name, bool isConst, bool isRef) {
    return name.substr(isConst? 1 : 0, name.size() - (int)isConst - (int)isRef);
  }
};

struct MethodDefinition {
  const string* key;
  int blockId;
  bool registerMethod(Session* m, const char* methodName, OClass* type) const {
    const bool isPrivate = key->at(0) == '_';
    const ushort flags = isPrivate ? M_PRIVATE : M_PUBLIC;
    try {
      if (!type->addScriptMethod(m, isPrivate ? key->substr(1) : *key,
          blockId, flags)) {
        const auto name = isPrivate ? key->substr(1) : *key;
        m->throwRTE(methodName, "duplicate method '"+name+"' in mutable type");
        return false;
      }
    }
    catch (const string& msg) {
      const auto name = isPrivate ? key->substr(1) : *key;
      m->throwRTE(methodName, msg+" in method '"+name+"' of mutable type");
      return false;
    }
    return true;
  }
};

struct MutableTypeDefinition {
  vector<ValueDefinition> values;
  vector<MethodDefinition> meths;
  vector<OVariable> vars;
  void recordPropsAndMethods(int argc, OArg* argv) {
    for (size_t i = 0; i < static_cast<size_t>(argc); i += 2) {
      const string& key = *_PString(_ARG(i));
      ORef value = _ARG(i + 1);
      if (value->getClassId() == CLSID_BLOCK) // method
        meths.push_back({&key, g_blockId(value)});
      else // property
        values.push_back({&key, argv[i + 1]});
    }
  }
  void recordPropsAndMethods(const vector<std::pair<string, ORef>>& defs,
                             ushort usage = USG_NONE) {
    vars.reserve(defs.size());
    for (const auto& [key, value] : defs) {
      if (value->getClassId() == CLSID_BLOCK) // method
        meths.push_back({&key, g_blockId(value)});
      else { // property
        vars.emplace_back(value, usage);
        values.push_back({&key, &vars.back()});
      }
    }
  }
  void registerPropsAndMethods(const char* method, OClass* type, Session* m) {
    if (!type->isMutable()) {
      m->throwRTE(method, "cannot extend immutable type");
      return;
    }
    for (auto& value : values) {
      value.gatherInfo(m, method);
      _RETHROW_NORET(method);
      value.registerProp(m, method, type);
      _RETHROW_NORET(method);
    }
    for (const auto& meth : meths) {
      meth.registerMethod(m, method, type);
      _RETHROW_NORET(method);
    }
  }
  ORef extend(ORef base, size_t start, const char* method, Session* m) {
    vector<OVar>& objectProps = base->props();
    objectProps.resize(start + values.size());
    _ASSERT(start + values.size() == base->getClass()->getInstanceSize(), m);
    for (size_t i = start; i < objectProps.size(); ++i) {
      const auto& value = values[i - start];
      OClass* const tentativeType = _REF(value.varPtr)->getClass();
      _ASSERT(objectProps[i].undefined(), m);
      value.assignValue(m, method, objectProps[i]);
      _CHECK_EXCEPTION_RETURN(nullptr);
      // fix property type in the occasion of proxy rendering
      OClass* const setType = REF(objectProps[i])->getClass();
      if (!value.isRef && setType != tentativeType)
        base->getClass()->getProperty(i - start).type = setType;
    }
    return base;
  }
};

ORef Session::createObjectExtension(OVar&& var, int argc, OArg* argv) {
  _ENTER_NOCHK("Session::createObjectExtension");
  Session* const m = this;
  ORef base = REF(var);
  // create derived class
  OClass* const baseType = base ? base->getClass() : BTI::getTypeClass<ORef>();
  // exclude proxy objects
  if (BTI::isBasicTypeProxy(baseType->getClassId()))
    _THROW_BAD_USAGE_RETURN("cannot extend proxy object", nullptr);
  OClass* type = baseType->createMutableClass(m, "_!_mt");
  _RETHROW_HERE_RETURN(nullptr);
  // we can change type name; mutable types aren't registered with class manager
  type->name = "_!_mutable_type_"+std::to_string(type->getClassId());
  // create instance
  ORef result;
  if (base) {
    ObjectCacheBase* const cache = baseType->getCache();
    result = cache && cache->contains(base) ? m->dupRenderObject(base, true)
                                            : m->moveBack(var);
    result->setClass(type);
    if (baseType->isMutable())
      baseType->removeInstance();
    type->addInstance();
  }
  else
    result = type->createObject(m);
  _RETHROW_HERE_RETURN(nullptr);
  result = extendObject(result, argc, argv);
  _RETHROW_HERE_RETURN(nullptr);
  return result;
}

ORef Session::extendObject(ORef base, int argc, OArg* argv) {
  const char* methodName = "Session::extendObject";
  Session* const m = this;
  const size_t start = base->props().size();
  MutableTypeDefinition typeDefinition;
  typeDefinition.recordPropsAndMethods(argc, argv);
  typeDefinition.registerPropsAndMethods(methodName, base->getClass(), m);
  _RETHROW_RETURN(nullptr, methodName);
  return typeDefinition.extend(base, start, methodName, m);
}

ORef Session::extendObject(ORef base,
    const vector<std::pair<string,ORef>>& defs, ushort usage) {
  const char* methodName = "Session::extendObject";
  Session* const m = this;
  const size_t start = base->props().size();
  MutableTypeDefinition typeDefinition;
  typeDefinition.recordPropsAndMethods(defs, usage);
  typeDefinition.registerPropsAndMethods(methodName, base->getClass(), m);
  _RETHROW_RETURN(nullptr, methodName);
  return typeDefinition.extend(base, start, methodName, m);
}

//::::::::::::::::::::::::   Create Template Object   :::::::::::::::::::::::://

ORef Session::createObjectFromTemplateParams(const string& typeName,
                                             int argc, OArg* argv) {
  _ENTER_NOCHK("Session::createObjectFromTemplateParams");
  Session* const m = this;
  OClass* const templateClass = classMgr->getClass(typeName);
  bool hidden{false};
  if (!templateClass || !templateClass->isTemplateClass()
                     || (hidden = templateClass->isHidden())) {
    const string name = "'"+typeName+"'";
    const string msg = templateClass ? name+" is not a template type" :
        string{hidden ? "inaccessible" : "unknown"}+" template type "+name;
    _THROW_RTE_RETURN(msg, nullptr);
  }
  // build parameter type list
  vector<const OClass*> paramTypes;
  paramTypes.reserve(argc);
  for (size_t i = 0; i < static_cast<size_t>(argc); ++i) {
    if (argv[i]->undefined()) {
      throwRTE(_ox_method_name, "undefined parameter "+std::to_string(i + 1));
      return nullptr;
    }
    paramTypes.emplace_back(_ARG(i)->getClass());
  }
  // specialize class
  OClass* c = templateClass->findSpecialization(paramTypes);
  if (!c) {
    c = templateClass->specializeTemplateClass(m, paramTypes);
    _RETHROW_HERE_RETURN(nullptr);
  }
  // create instance
  vector<OArg> values = templateClass->getArgsFromTemplateParams(argc, argv);
  if (values.empty()) {
    const size_t numTemplateParams = templateClass->countTemplateParams();
    const string msg = string{numTemplateParams > argc ? "too few" : "excessive"}
        +" template parameters (expected "+std::to_string(numTemplateParams)
        +" found "+std::to_string(argc)+")";
    m->throwRTE(_ox_method_name, msg);
    return nullptr;
  }
  ORef o = c->createInitObject(m, values.size(), values.data());
  _RETHROW_HERE_RETURN(nullptr);
  return o;
}

ORef Session::createInitObject(const string& typeName, int argc, OArg* argv) {
  _ENTER_NOCHK("Session::createInitObject");
  Session* const m = this;
  OClass* const type = classMgr->getClass(typeName);
  if (!type || type->isHidden()) {
    const string what = type ? "inaccessible" : "unknown";
    _THROW_RTE_RETURN(what + " type '"+typeName+"'", nullptr)
  }
  OClass* c;
  if (type->isTemplateClass()) {
    // build parameter type list
    vector<const OClass*> paramTypes;
    try {
      paramTypes = type->getTemplateParamTypes(argc, argv, m);
    }
    catch (int n) {
      const size_t numTemplateParams = type->countTemplateParams();
      const string msg = "too few template parameters (expected "+
          std::to_string(numTemplateParams)+" found "+std::to_string(n)+")";
      m->throwRTE(_ox_method_name, msg);
      return nullptr;
    }
    // specialize class
    c = type->findSpecialization(paramTypes);
    if (!c) {
      c = type->specializeTemplateClass(m, paramTypes);
      _RETHROW_HERE_RETURN(nullptr);
    }
  }
  else
    c = type;
  // create instance
  ORef o = c->createInitObject(m, argc, argv);
  _RETHROW_HERE_RETURN(nullptr);
  return o;
}

//:::::::::::::::::::::::::::::::   Testing   :::::::::::::::::::::::::::::::://

void Session::checkpointText(const string &text) {
  if (!checker_.expandCheckpoint(text))
    throwRTE("Session::checkpointText", "checkpoint defined but ignored");
}

void Session::performCheck(const string &expectedText) {
  if (!testingMode()) return;
  auto result = checker_.performCheck(expectedText);
  if (result == CHECKPOINT_DEFINED_BUT_IGNORED)
    throwRTE("Session::performTest", "checkpoint defined but ignored");
  else if (result == CHECK_FAILED)
    throwRTE("Session::performTest", "check failed");
}

//:::::::::::::::::::::::::::::::   Packages   ::::::::::::::::::::::::::::::://

string Session::getTypePrefix() const {
  string prefix{};
  for (const auto& packageInfo : packageStack_)
    prefix = packageInfo.typePrefix + prefix;
  return prefix;
}

string Session::getPrefixedTypeName(const string& typeName) const {
  return isBuiltinType(typeName) || typeName.empty() ? typeName :
      getTypePrefix() + typeName;
}

//::::::::::::::::::::::::   Variable Manipulation   ::::::::::::::::::::::::://

void Session::emplace(OVar& thisPlace, OVar& var) {
  Session* const m = this;
  _ASSERT(thisPlace.undefined(), m);
  if (var.undefined()) {
    throwRTE("Session::emplace", "undefined variable");
    return;
  }
  const bool isNotProxy = !BTI::isBasicTypeProxy(REF(var));
  if (var.disposable() && isNotProxy) {
    std::swap(REF(thisPlace), REF(var));
    var.undispose();
    return;
  }
  else if (var.movable() && isNotProxy) {
    OClass* const type = REF(var)->getClass();
    REF(thisPlace) = type->createObject(this);
    if (!exceptionPending())
      type->moveObject(REF(thisPlace), REF(var), m);
  }
  else
    REF(thisPlace) = m->dupRenderObject(&var);
  _RETHROW_NORET("Session::emplace");
}

ORef Session::moveBack(OVariable& var) {
  OVar thisPlace{};
  emplace(thisPlace, var);
  return REF(thisPlace);
}

ORef Session::moveRenderObject(ORef lhs, OArg var) {
  if (!var->movable()) return nullptr;
  Session* const m = this;
  OVar rendered{g_renderProxyObject(var->objectRef, m)};
  if (m->exceptionPending()) return nullptr;
  const bool varWasRendered = REF(rendered) != var->objectRef;
  ObjectAutoCleaner cleanRendered{m, varWasRendered ? REF(rendered) : nullptr};
  if (varWasRendered) var = &rendered;
  if (lhs->getClass() != _REF(var)->getClass()) return nullptr;
  lhs->swapContent(_REF(var));
  return m->exceptionPending() ? nullptr : lhs;
}

ORef Session::dupRenderObject(ORef o, bool forceNoCache) {
  Session* const m = this;
  ORef rendered = g_renderProxyObject(o, forceNoCache, m);
  if (m->exceptionPending()) return nullptr;
  if (rendered == o) { // o wasn't a proxy
    rendered = o->getClass()->createObject(forceNoCache, m);
    if (m->exceptionPending()) return nullptr;
    rendered->copyObject(o, m);
  }
  return rendered;
}

void Session::smartCopy(OVar& lhs, OVar& rhs,
    const std::function<void(Session*, OVar&, OVar&)>& f, bool withCast) {
  if (!lhs.isMutable()) {
    throwBadUsage(__PRETTY_FUNCTION__, "cannot modify constant object",
        "Session::smartCopy");
    return;
  }
  _ASSERT(!rhs.undefined(), this);
  OClass* const lhsType = REF(lhs)->getClass();
  OClass* const rhsType = REF(rhs)->getClass();
  const bool sameType = lhsType == rhsType;
  const bool isNotProxy = !BTI::isBasicTypeProxy(REF(rhs));
  if (!sameType) {
    if (!withCast) {
      const string msg = "cannot copy object type '"+rhsType->name+"' "
                         "to type '"+lhsType->name+"'";
      throwBadUsage(__PRETTY_FUNCTION__, msg, "Session::smartCopy");
      return;
    }
    if (f)
      f(this, lhs, rhs);
    else
      setValueWithCast(&lhs, &rhs, false, false);
  }
  else if ((rhs.disposable() || rhs.movable())/* && isNotProxy*/)
    REF(lhs)->swapContent(REF(rhs));
  else if (f)
    f(this, lhs, rhs);
  else
    // copyFrom(&lhs, &rhs);
    lhsType->copyObject(REF(lhs), REF(rhs), this);
  if (exceptionPending())
    rethrow("Session::smartCopy", __FILE__, __LINE__);
}

//::::::::::::::::::::::::::   Machine Utilities   ::::::::::::::::::::::::::://

void Session::machineInitError() const {
  displayMessage("runtime error", mainMachine_ ?
      "improperly initialized Virtual Machine" :
      "script execution requires the presence of a Virtual Machine");
}

//============================================================================//
//:::::::::::::::::::::::::::::   ORTMgr_Class   ::::::::::::::::::::::::::::://
//============================================================================//

OClass* ORTMgr_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new ORTMgr_Class(m, this->name, className);
  m->rethrow("ORTMgr_Class::createClass");
  return cls;
}

OClass* ORTMgr_Class::createMutableClass(Session* m, const string&) const {
  m->throwBadClass(
      /* method      */ "ORTMgr_Class::createMutableClass",
      /* description */ "mutable type could not be created",
      /* context     */ "mutable class construction",
      /* reason      */ "the system class cannot be auto-extended",
      /* file, line  */ __FILE__, __LINE__);
  return nullptr;
}

OInstance* ORTMgr_Class::createObject(bool, Session* m/*can be null*/) {
  // construct core (C++) object
  OInstance* o = constructCore(nullptr);
  if (!o) return nullptr;
  // construct script extended object
  o = constructScriptedPart(o, nullptr);
  if (!o) return nullptr;
  // do not register rtmgr objects (registry NOT enabled by default!)
  // _PRTMgr(o)->registerObject(o);
  return o;
}

void ORTMgr_Class::destructCore(OInstance* o, Session*) {
  ORTMgr* rtmgr = static_cast<ORTMgr*>(o);
  delete rtmgr;
}

void ORTMgr_Class::deleteObject(ORef o, Session* m) {
  OClass* const type = o->getClass();
  // destroy properties
  deleteScriptedPart(o, m);
  // we defer the destruction of its core object
  _ASSERT(!type->isMutable() || type->dedicatedUses_ > 0, m);
  if (type->isMutable() && --type->dedicatedUses_ == 0)
    m->deleteMutableClass(type);
  // remove from list
  m->deregisterObject(o);
  // destroy C++ object
  destructCore(o, nullptr);
  // no check for errors
}

//============================================================================//
//::::::::::::::::::::::::::::::::   ORTMgr   :::::::::::::::::::::::::::::::://
//============================================================================//

_BEGIN_METHOD_TABLE(ORTMgr, OInstance)
  PUBLIC (ORTMgr::ommCreateArray, "createArray:")
  PUBLIC (ORTMgr::ommCreateEmptyArray, "createArray")
  PUBLIC (ORTMgr::ommCreateEmptySet, "createSet")
  PUBLIC (ORTMgr::ommCreateSet, "createSet:")
  PUBLIC (ORTMgr::ommCreateEmptyObj, "createObject")
  PUBLIC (ORTMgr::ommCreateObj, "createObject:")
  PUBLIC (ORTMgr::ommCreateObjFromTemplArgs, "crofta:")
  PUBLIC (ORTMgr::ommCreateCObjFromTemplArgs, "crcofta:")
  PUBLIC (ORTMgr::ommCreateNObjFromTemplArgs, "crnofta:")
  PUBLIC (ORTMgr::ommCreateInitObject, "crofil:")
  PUBLIC (ORTMgr::ommCreateInitCObject, "crcofil:")
  PUBLIC (ORTMgr::ommCreateInitNoAutoObject, "crnofil:")
  PUBLIC (ORTMgr::ommCreateObjExt, "base:member:value:")
  PUBLIC (ORTMgr::ommCreateObjExt, "base:memb:val:")
  PUBLIC (ORTMgr::ommGetArg, "arg:")
  PUBLIC (ORTMgr::ommGetArgCount,"argc")
  PUBLIC (ORTMgr::ommGetArgPtr, "argList:")
  PUBLIC (ORTMgr::ommDefineClass, "class:")
  PUBLIC (ORTMgr::ommDefineClass, "class:def:")
  PUBLIC (ORTMgr::ommDefineClass, "class:definition:")
  PUBLIC (ORTMgr::ommDefineClass, "class:extends:")
  PUBLIC (ORTMgr::ommDefineClass, "class:extends:def:")
  PUBLIC (ORTMgr::ommDefineClass, "class:extends:definition:")
  PUBLIC (ORTMgr::ommCopyFrom, "copy:from:")
  PUBLIC (ORTMgr::ommBuildArgList, "createArgList:")
  PUBLIC (ORTMgr::ommDeleteArgList, "deleteArgList:")
  PUBLIC (ORTMgr::ommDeleteObject, "delete:")
  PUBLIC (ORTMgr::ommGetClass, "getClass:")
  PUBLIC (ORTMgr::ommAlias, "using:as:")
  PUBLIC (ORTMgr::ommGetCurrentLine, "currentLine")
  PUBLIC (ORTMgr::ommGetException, "getException")
  PUBLIC (ORTMgr::ommFilterException, "getException:")
  PUBLIC (ORTMgr::ommAutoVar, "auto:type:")
  PUBLIC (ORTMgr::ommConstAutoVar, "const:type:")
  PUBLIC (ORTMgr::ommAutoVarWithValue, "auto:value:")
  PUBLIC (ORTMgr::ommConstAutoVarWithValue, "const:value:")
  PUBLIC (ORTMgr::ommStaticVar, "static:type:")
  PUBLIC (ORTMgr::ommConstStaticVar, "staticConst:type:")
  PUBLIC (ORTMgr::ommAutoTmp, "auto:")
  PUBLIC (ORTMgr::ommConstAutoTmp, "const:")
  PUBLIC (ORTMgr::ommNoAuto, "noauto:")
  PUBLIC (ORTMgr::ommRefVar, "ref:")
  PUBLIC (ORTMgr::ommRefVar, "reference:")
  PUBLIC (ORTMgr::ommIsUndefined, "isUndefined:")
  PUBLIC (ORTMgr::ommIsObject, "isObject:")
  PUBLIC (ORTMgr::ommIsType, "isType:")
  PUBLIC (ORTMgr::ommIsDisposable, "disposable:")
  PUBLIC (ORTMgr::ommIsDisposable, "isDisposable:")
  PUBLIC (ORTMgr::ommIsMovable, "movable:")
  PUBLIC (ORTMgr::ommIsMovable, "isMovable:")
  PUBLIC (ORTMgr::ommIsLocalVar, "isLocal:")
  PUBLIC (ORTMgr::ommIsArg, "isArgument:")
  PUBLIC (ORTMgr::ommIsLiteral, "isLiteral:")
  PUBLIC (ORTMgr::ommIsConst, "isConst:")
  PUBLIC (ORTMgr::ommIsRef, "isRef:")
  PUBLIC (ORTMgr::ommIsRef, "isReference:")
  PUBLIC (ORTMgr::ommSetException, "setException:")
  PUBLIC (ORTMgr::ommForLoop, "for:while:step:do:")
  PUBLIC (ORTMgr::ommWhileLoop, "while:do:")
  PUBLIC (ORTMgr::ommUntilLoop, "until:do:")
  PUBLIC (ORTMgr::ommWhileAtEndLoop, "do:while:")
  PUBLIC (ORTMgr::ommUntilAtEndLoop, "do:until:")
  PUBLIC (ORTMgr::ommIfThen, "if:then:")
  PUBLIC (ORTMgr::ommIfThenElse, "if:then:else:")
  PUBLIC (ORTMgr::ommIfThenElseif, "if:then:elseif:then:")
  PUBLIC (ORTMgr::ommIfThenElseifElse, "if:then:elseif:then:else:")
  PUBLIC (ORTMgr::ommSwitchCase, "switch:case:do:")
  PUBLIC (ORTMgr::ommSwitchCaseDefault, "switch:case:do:default:")
  PUBLIC (ORTMgr::ommBreak, "break")
  PUBLIC (ORTMgr::ommBreakMany, "break:")
  PUBLIC (ORTMgr::ommBreakManyIf, "break:if:")
  PUBLIC (ORTMgr::ommReturn, "return")
  PUBLIC (ORTMgr::ommReturnIf, "returnIf:")
  PUBLIC (ORTMgr::ommReturnValue, "return:")
  PUBLIC (ORTMgr::ommReturnRef, "returnRef:")
  PUBLIC (ORTMgr::ommReturnConstRef, "returnConstReference:")
  PUBLIC (ORTMgr::ommReturnConstRef, "returnConstRef:")
  PUBLIC (ORTMgr::ommReturnRef, "returnReference:")
  PUBLIC (ORTMgr::ommReturnThis, "returnThis")
  PUBLIC (ORTMgr::ommTryCatch, "try:catch:")
  PUBLIC (ORTMgr::ommTryCatchDo, "try:catch:do:")
  PUBLIC (ORTMgr::ommThrow, "throw:")
  PUBLIC (ORTMgr::ommCheck, "check:")
  PUBLIC (ORTMgr::ommMergeCheck, "mergeCheck")
  PUBLIC (ORTMgr::ommIgnoreCheck, "ignoreCheckpoint")
  PUBLIC (ORTMgr::ommSetCheckpoint, "checkpoint")
  PUBLIC (ORTMgr::ommExpectException, "expectedException:line:")
  PUBLIC (ORTMgr::ommExpectException, "expectedException:line:location:message:")
  PUBLIC (ORTMgr::ommExpectLeftovers, "expectedLeftovers:")
  PUBLIC (ORTMgr::ommExpectRetValue, "expectedReturnValue:")
  PUBLIC (ORTMgr::ommCheckLambdaProtos, "checkProtos:")
  PUBLIC (ORTMgr::ommMutableLambdaProtos, "mutableProtos:")
  // rarely or inappropriately used methods
  PUBLIC (ORTMgr::ommUndefArgsDiags, "arglistDiagnostics:")
  PUBLIC (ORTMgr::ommMoveObject, "move:")
  PUBLIC (ORTMgr::ommMove, "move")
  PUBLIC (ORTMgr::ommPassLiteral, "pass")
_END_METHOD_TABLE()

ORet ORTMgr::ommGetClass(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  _ENTER("system::getClass:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, origTypeName, 0);

  const string typeName = m->getPrefixedTypeName(origTypeName);
  OClass* cls = classMgr->getClass(typeName);
  _RETHROW_HERE();
  // create class object
  OClass* classObjFactory = classMgr->getClass("class object");
  if (!classObjFactory) {
    classObjFactory = g_installClassObjectClass(m);
    if (!classObjFactory) {
      m->throwBadClass(
      /* method      */ _ox_method_name,
      /* description */ "could not create class 'class object'",
      /* context     */ "ORTMgr::ommGetClass",
      /* reason      */ "");
      return undefinedResult();
    }
  }
  ORef classObj = classObjFactory->createObject(m);
  _RETHROW_HERE();
  _ASSERT(classObj != nullptr, m);
  static_cast<oxClassObject*>(classObj)->classPointer = cls;
  return classObj->disposableResult();
}

ORet ORTMgr::ommDefineClass(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  const char* _ox_method_name = argc == 2 ? "system::class:definition:" :
      "system::class:extends:definition:";
  _CHECK_ARGC_2(2, 3);
  _UNPACK_CONST_REF(String, className, 0);

  const string typeName = m->getTypePrefix() + className;
  OClass* cls = classMgr->getClass(typeName);
  _RETHROW_HERE();
  if (!cls) {
    m->throwBadClass(
    /* method      */ _ox_method_name,
    /* description */ "the class '"+typeName+"' could not be defined",
    /* context     */ "ORTMgr::ommDefineClass",
    /* reason      */ "the class was not found");
    return undefinedResult();
  }

  // Run machine to define class
  const int classDefBlock = g_blockId(_ARG(argc - 1));
  _ASSERT(classDefBlock != 0 && classDefBlock != -1, m);
  ORet ret = execClassDefProc_(cls, classDefBlock);
  _RETHROW_HERE();

  // Finalize class
  cls->finalize(this);

  return ret;
}

ORet ORTMgr::createClassObjectNoVM(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(argc == 2 || argc == 3, m);
  _ASSERT(m == static_cast<Session*>(this), m);
  _UNPACK_CONST_REF(String, name, 0);
  OClass* cls;
  if (argc == 2)
    cls = BTI::getTypeClass<ORef>()->createClass(this, name);
  else
    cls = _ARG(1)->getClass()->createClass(this, name);
  _RETHROW("ORTMgr::ommDefineClassNoVM");
  _ASSERT(cls != nullptr, m);
  return OVar{reinterpret_cast<ORef>(cls), 0};
}

//::::::::::::::::::::::::::   Exception Handling   :::::::::::::::::::::::::://

ORet ORTMgr::ommSetException(OArg rec, int argc, OArg* argv, Session* m) {
  setException(_ARG(0));
  return exception_->makeResult();
}

ORet ORTMgr::ommGetException(OArg rec, int argc, OArg* argv, Session* m) {
  if (savedException_) {
    ORef r = savedException_;
    savedException_ = nullptr;
    return r->disposableResult();
  }
  else if (exception_) {
    ORef r = exception_;
    exception_ = nullptr;
    return r->disposableResult();
  }
  return undefinedResult(); // TODO replace with null object
}

ORet ORTMgr::ommFilterException(OArg rec, int argc, OArg* argv, Session* m) {
  ORef r = exception_;
  if (exception_ != nullptr && _ARG(0) != nullptr) {
    OClass* requested = classMgr->getClass(*_PStr(_ARG(0)));
    if (!exception_->getClass()->extends(requested)) return undefinedResult();
  }
  exception_ = nullptr;
  return r->disposableResult();
}

//:::::::::::::::::::::::   Machine Related Methods   :::::::::::::::::::::::://

ORet ORTMgr::ommGetArg(OArg, int argc, OArg* argv, Session* m) // TODO redefine!
{
  _ASSERT(argc == 1, this);
  const int i = g_typeValue<int>(_ARG(0));
//   ORIGINAL
//   return g_GetArgProc(this, i);  //vm->getArg(i);
  displayMessage("Error", "This method is being redefined");
  return undefinedResult();
}

ORet ORTMgr::ommGetArgPtr(OArg, int argc, OArg* argv, Session* m) // TODO redefine!
{
  _ASSERT (argc == 1, this);
#if 0 // ORIGINAL
  ORef r = BTI::getTypeClass<void*>()->createObject(m);
  int i = _PINT(_ARG(0))->getValue();
  // vm->getArgPtr(i);
  vmType pArg = (vmType) g_GetArgPtrProc(rte->vm, i);
  _PPTR(r)->setValue(pArg);
  disposeResult();
  return r;
#endif // ORIGINAL
  displayMessage("Error", "This method is being redefined");
  return undefinedResult();
}

ORet ORTMgr::ommGetArgCount(OArg, int, OArg* , Session*) // TODO redefine!
{
#if 0 // ORIGINAL
  ORef r = getStockInt();
  _PINT(r)->setValue(g_GetArgCountProc(rte->vm));
  disposeResult();
  return r;
#endif // ORIGINAL
  displayMessage("Error", "This method is being redefined");
  return undefinedResult();
}

ORet ORTMgr::ommGetCurrentLine(OArg, int, OArg* , Session* m) {
  const int line = g_getMachineValue(mainMachine_, M_REGISTER_LR).get<int>();
  ORef r = BTI::getTypeClass<int>()->createObject(m);
  _RETHROW("system::currentLine");
  _P(int, r)->setValue(line);
  return r->disposableResult();
}

ORet ORTMgr::ommBuildArgList(OArg, int argc, OArg* argv, Session* m) {
  auto ptrArgs = new std::vector<OArg>(argv, argv + argc);
  ORef r = BTI::getTypeClass<void*>()->createObject(m);
  _PPTR(r)->setValue(ptrArgs);
  return r->disposableResult();
}

ORet ORTMgr::ommDeleteArgList(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT (argc == 1, m);
  delete (std::vector<OArg>*) _PPTR(_ARG(0))->getValue();
  return makeResult();
}

ORet ORTMgr::ommCheckLambdaProtos(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::checkProtos:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<bool>());
  const bool on = g_typeValue<bool>(_ARG(0));
  m->setCheckLambdaProto(on);
  return makeResult();
}

ORet ORTMgr::ommMutableLambdaProtos(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::mutableProtos:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<bool>());
  const bool on = g_typeValue<bool>(_ARG(0));
  m->setMutableLambdaProto(on);
  return makeResult();
}

ORet ORTMgr::ommUndefArgsDiags(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::arglistDiagnostics:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<bool>());
  const bool on = g_typeValue<bool>(_ARG(0));
  m->setUndefinedArgsDiagnostics(on);
  return makeResult();
}

//::::::::::::::::::::   Object & Variable Checking Out   :::::::::::::::::::://

ORet ORTMgr::ommIsUndefined(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isUndefined:", 1);
  OVar* varPtr = nullptr;
  if (argv[0]->type == VARTYPE_OBJECT) {
    varPtr = argv[0];
  }
  else if (argv[0]->type == VARTYPE_INT_DATA) {
    int symbolOffset = argv[0]->intValue;
    // *argv[0] = OVar{};
    const auto varInfo = g_resolveSymbolType(getCurrentMachine(),
                                             symbolOffset, RSYM_ANY);
    varPtr = std::get<0>(varInfo);
    if (!varPtr) {
      const auto& varName = std::get<2>(varInfo);
      _THROW_RTE("could not determine the type of symbol '"+varName+"'");
      return undefinedResult();
    }
  }
  else {
    throwBadArg(_ox_method_name, "unexpected variable type for argument 1");
    return undefinedResult();
  }
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(r)->setValue(varPtr->undefined());
  return r->disposableResult();
}

ORet ORTMgr::ommIsObject(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isObject:", 1);
  _ASSERT(static_cast<Session*>(this) == m, m);
  OVar* varPtr = nullptr;
  if (argv[0]->type == VARTYPE_OBJECT) {
    varPtr = argv[0];
  }
  else if (argv[0]->type == VARTYPE_INT_DATA) {
    int symbolOffset = argv[0]->intValue;
    // *argv[0] = OVar{};
    const auto varInfo = g_resolveSymbolType(getCurrentMachine(),
                                             symbolOffset, RSYM_ANY);
    varPtr = std::get<0>(varInfo);
    if (!varPtr) {
      _THROW_RTE("unknown type symbol '"+std::get<2>(varInfo)+"'");
      return undefinedResult();
    }
  }
  else {
    throwBadArg(_ox_method_name, "unexpected variable type for argument 1");
    return undefinedResult();
  }
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  const bool val = m->isStdNullObject(_REF(varPtr)) ||
      objectTable_.find(_REF(varPtr)) != objectTable_.end();
  _PBOOL(r)->setValue(val);
  return r->disposableResult();
}

ORet ORTMgr::ommIsType(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isType:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, typeName, 0);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _RETHROW_HERE_IF(!r);
  OClass* type = m->classMgr->getClass(typeName);
  *_PBOOL(r) = type != nullptr;
  return r->disposableResult();
}

ORet ORTMgr::ommIsDisposable(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::disposable:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->disposable();
  return r->disposableResult();
}

ORet ORTMgr::ommIsMovable(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::movable:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->isMovable();
  return r->disposableResult();
}

ORet ORTMgr::ommIsLocalVar(OArg rec, int argc, OArg* argv, Session* m) {
  bool g_isLocalVar(const VirtualMachine*, OVar*) noexcept;
  _ENTER("system::isLocal:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = g_isLocalVar(m->getCurrentMachine(), argv[0]);
  return r->disposableResult();
}

ORet ORTMgr::ommIsArg(OArg rec, int argc, OArg* argv, Session* m) {
  bool g_isArgument(const VirtualMachine*, OVar*);
  _ENTER("system::isArgument:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = g_isArgument(m->getCurrentMachine(), argv[0]);
  return r->disposableResult();
}

ORet ORTMgr::ommIsLiteral(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isLiteral:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->isLiteral();
  return r->disposableResult();
}

ORet ORTMgr::ommIsConst(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isConst:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->isConst();
  return r->disposableResult();
}

ORet ORTMgr::ommIsRef(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("system::isReference:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->isRef();
  return r->disposableResult();
}

//:::::::::::::::::::::::::::::::   Aliases   :::::::::::::::::::::::::::::::://

ORet ORTMgr::ommAlias(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("using:as:");
  _ASSERT(m == this, m);
  const auto n = static_cast<size_t>(argc);
  if (n % 2 == 1) {
    m->throwBadArgc(__PRETTY_FUNCTION__, _ox_method_name, n + 1);
    return undefinedResult();
  }
  for (size_t i = 0; i < n; i += 2) {
    _ASSERT(i + 1 < n, m);
    if (!_ARG_TYPE(i, BTI::getTypeClass<string>()) &&
        !_ARG_TYPE(i, classMgr->getClass("class object"))) {
      m->throwBadArg(_ox_method_name, i, "'string' or 'class object'",
          _ARG(i)->getClassName());
      return undefinedResult();
    }
    _CHECK_ARG_TYPE(i + 1, BTI::getTypeClass<string>());
    _UNPACK_CONST_REF(String, newTypeName, i + 1);
    OClass* p;
    if (_ARG_TYPE(i, BTI::getTypeClass<string>())) {
      _UNPACK_CONST_REF(String, origTypeName, i);
      const string typeName = getPrefixedTypeName(origTypeName);
      p = classMgr->getClass(typeName);
      if (!p)
        _THROW_RTE("'"+origTypeName+"' (aka '"+typeName+"') is not a type name")
    }
    else
      p = static_cast<oxClassObject*>(_ARG(i))->classPointer;
    const string absTypeName = m->getTypePrefix() + newTypeName;
    classMgr->aliasClass(p, absTypeName);
    if (m->importing())
      m->getPackageInfo().registeredTypes.emplace_back(absTypeName, p);
  }
  return makeResult();
}

//:::::::::::::::::::::::::   Object Manipulation   :::::::::::::::::::::::::://

ORet ORTMgr::ommMoveObject(OArg, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME("system::move:");
  _THROW_BAD_USAGE("cannot move the system object")
}

ORet ORTMgr::ommMove(OArg, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME("system::move");
  _THROW_BAD_USAGE("cannot move the system object")
}

ORet ORTMgr::ommPassLiteral(OArg, int, OArg* argv, Session* m) {
  _DECL_METHOD_NAME("system::pass");
  _THROW_BAD_USAGE("the method applies to literals")
}

//::::::::::::::::::::::::::::   Delete Object   ::::::::::::::::::::::::::::://

ORet ORTMgr::ommDeleteObject(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::delete:", 1);
  OVar* varPtr = nullptr;
  ushort varType = VARTYPE_OBJECT;
  if (argv[0]->type == VARTYPE_OBJECT) {
    varPtr = argv[0];
  }
  else if (argv[0]->type == VARTYPE_INT_DATA) {
    int symbolOffset = argv[0]->intValue;
    varType = VARTYPE_INT_DATA;
    *argv[0] = OVar{};
    const auto varInfo = g_resolveSymbolType(getCurrentMachine(),
                                             symbolOffset, RSYM_ANY);
    varPtr = std::get<0>(varInfo);
    if (!varPtr) {
      _THROW_RTE("unknown type symbol '"+std::get<2>(varInfo)+"'");
      return undefinedResult();
    }
  }
  else {
    throwBadArg(_ox_method_name, "unexpected variable type for argument 1");
    return undefinedResult();
  }
  if (varPtr->isRef()) {
    if (m->isStdNullObject(_REF(varPtr)))
      return undefinedResult();
    if (varPtr->isConst()) {
      throwBadArg(_ox_method_name, "cannot delete constant reference");
      return undefinedResult();
    }
    OVar* const arg = (varType == VARTYPE_INT_DATA) ? varPtr :
        static_cast<OVar*>(getCurrentMachineValue<void*>(M_ARG_0));
    _ASSERT(_REF(arg) == _REF(varPtr), m);
    m->deleteObject(_REF(arg));
    _REF(arg) = m->nullObject();
  }
  else
    m->throwBadArg(_ox_method_name, "argument is not a reference "
                                    "(unsafe deletion)");
  return undefinedResult();
}

//:::::::::::::::::::::::::::::   Object Copy   :::::::::::::::::::::::::::::://

ORet ORTMgr::ommCopyFrom(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::copy:from:", 2);
  if (argv[0]->undefined()) {
    m->throwBadArg(_ox_method_name, "cannot copy to undefined object");
    return undefinedResult();
  }
  ORet r = m->copyFrom(argv[0], argv[1]);
  _RETHROW_HERE();
  return r;
}

//::::::::::::::::::::::::::::   Create Object   ::::::::::::::::::::::::::::://

ORet ORTMgr::ommCreateEmptyObj(OArg, int, OArg*, Session* m) {
  _ENTER_NOCHK("system::createObject");
  _ASSERT(static_cast<Session*>(this) == m, m);
  // create derived class
  OClass* type = BTI::getTypeClass<ORef>()->createMutableClass(m, "_!_mt");
  // check exception
  _CHECK_EXCEPTION();
  // change type name - we can do it because mutable classes
  //                    are not registered with the class manager
  type->name = "_!_mutable_type_"+std::to_string(type->getClassId());
  // create instance
  ORef result = type->createObject(m);
  _CHECK_EXCEPTION();
  _ASSERT(result->props().size() == 0, m);
  return result->disposableResult();
}

ORet ORTMgr::ommCreateObj(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::createObject:");
  _ASSERT(static_cast<Session*>(this) == m, m);
  ORef o = createObjectFromDefinition(argc, argv);
  _CHECK_EXCEPTION();
  return o->disposableResult();
}

ORet ORTMgr::ommCreateObjExt(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::base:member:value:");
  _ASSERT(static_cast<Session*>(this) == m, m);
  ORef o = createObjectExtension(std::move(*argv[0]), argc - 1, argv + 1);
  _CHECK_EXCEPTION();
  return o->disposableResult();
}

static ORet s_createObjFromTemplArgs(int, OArg*, Session*, bool, bool);
static ORet s_createInitObject(int, OArg*, Session*, bool, bool);

ORet ORTMgr::ommCreateObjFromTemplArgs(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createObjFromTemplArgs(argc, argv, m, false, false);
}

ORet ORTMgr::ommCreateCObjFromTemplArgs(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createObjFromTemplArgs(argc, argv, m, true, false);
}

ORet ORTMgr::ommCreateNObjFromTemplArgs(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createObjFromTemplArgs(argc, argv, m, false, true);
}

ORet ORTMgr::ommCreateInitObject(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createInitObject(argc, argv, m, false, false);
}

ORet ORTMgr::ommCreateInitCObject(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createInitObject(argc, argv, m, true, false);
}

ORet ORTMgr::ommCreateInitNoAutoObject(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  return s_createInitObject(argc, argv, m, false, true);
}

ORet s_createObjFromTemplArgs(int argc, OArg* argv, Session* m, bool constVar,
                                                                bool noauto) {
  _DECL_METHOD_NAME(noauto   ? "system::crnofta:" :
                    constVar ? "system::crcofta:" :
                               "system::crofta:");
  _CHECK_ARG_TYPE(argc - 1, BTI::getTypeClass<string>());
  if (argc < 2)
    _THROW_RTE("cannot create template object from empty parameter list");
  const string& suppliedTypeName = *_PString(_ARG(argc - 1));
  // add a package prefix if one is being compiled
  const string typeName = m->getPrefixedTypeName(suppliedTypeName);
  ORef o = m->createObjectFromTemplateParams(typeName, argc - 1, argv);
  _CHECK_EXCEPTION();
  const ushort usage = noauto   ? USG_NONE :
                       constVar ? (USG_DISPOSABLE | USG_CONST) :
                                  USG_DISPOSABLE;
  return OVariable{o, usage};
}

ORet s_createInitObject(int argc, OArg* argv, Session* m, bool constVar,
                                                          bool noauto) {
  _DECL_METHOD_NAME(noauto   ? "system::crnofil:" :
                    constVar ? "system::crcofil:" :
                               "system::crofil:");
  _CHECK_ARG_TYPE(argc - 1, BTI::getTypeClass<string>());
  const string& suppliedTypeName = *_PString(_ARG(argc - 1));
  // add a package prefix if one is being compiled
  const string typeName = m->getPrefixedTypeName(suppliedTypeName);
  ORef o = m->createInitObject(typeName, argc - 1, argv);
  _CHECK_EXCEPTION();
  const ushort usage = noauto   ? USG_NONE :
                       constVar ? (USG_DISPOSABLE | USG_CONST) :
                                  USG_DISPOSABLE;
  return OVariable{o, usage};
}

//:::::::::::::::::::::::::::::   Create Array   ::::::::::::::::::::::::::::://

static int s_getCommonBasicType(int argc, OArg* argv) {
  const auto basicTypeId = _ARG(0)->getRootClassId();
  if (!BTI::isBasicType(basicTypeId))
    return 0;
  for (int i = 1; i < argc; ++i)
    if (_ARG(i)->getRootClassId() != basicTypeId)
      return 0;
  return basicTypeId;
}

static bool s_stringItems(int argc, OArg* argv) {
  for (int i = 0; i < argc; ++i)
    if (_ARG(i)->getRootClassId() != CLSID_STRING)
      return false;
  return true;
}

ORet ORTMgr::ommCreateArray(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::createArray:");
  _ASSERT(static_cast<Session*>(this) == m, m);
  _ASSERT(argc > 0, m);
  ORef r;
  switch (s_getCommonBasicType(argc, argv)) {
    case CLSID_ULONG:
      r = BTI::getVectorClass<ulong>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(ulong, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_LONG:
      r = BTI::getVectorClass<long>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(long, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_UINT:
      r = BTI::getVectorClass<unsigned>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(unsigned, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_INT:
      r = BTI::getVectorClass<int>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(int, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_SHORT:
      r = BTI::getVectorClass<short>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(short, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_CHAR:
      r = BTI::getVectorClass<char>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(char, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_BOOL:
      r = BTI::getVectorClass<bool>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(bool, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_DBL:
      r = BTI::getVectorClass<double>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(double, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_FLT:
      r = BTI::getVectorClass<float>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(float, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_PTR:
      r = BTI::getVectorClass<void*>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PVector(void*, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    default:
      r = BTI::getObjectVectorClass()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PObjVector(r)->ommInit(&rec, argc, argv, m);
      }
      break;
  }
  _CHECK_EXCEPTION();
  return r->disposableResult();
}

ORet ORTMgr::ommCreateEmptyArray(OArg, int, OArg*, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  ORef r = BTI::getObjectVectorClass()->createObject(m);
  _RETHROW("system::createArray");
  return r->disposableResult();
}

//::::::::::::::::::::::::::::::   Create Set   :::::::::::::::::::::::::::::://

ORet ORTMgr::ommCreateSet(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::createSet:");
  _ASSERT(static_cast<Session*>(this) == m, m);
  _ASSERT(argc > 0, m);
  ORef r;
  switch (s_getCommonBasicType(argc, argv)) {
    case CLSID_ULONG:
    case CLSID_UINT:
      r = BTI::getSetClass<ulong>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PSet(ulong, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_LONG:
    case CLSID_INT:
    case CLSID_SHORT:
    case CLSID_CHAR:
      r = BTI::getSetClass<long>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PSet(long, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_DBL:
    case CLSID_FLT:
      r = BTI::getSetClass<double>()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PSet(double, r)->ommInit(&rec, argc, argv, m);
      }
      break;
    case CLSID_BOOL:
      _THROW_RTE("there is no boolean set type; use another container type");
    case CLSID_PTR:
      r = BTI::getObjectSetClass()->createObject(m);
      if (!m->exceptionPending()) {
        OVar rec{r}; _PObjSet(r)->ommInit(&rec, argc, argv, m);
      }
      break;
    default:
      if (s_stringItems(argc, argv)) {
        r = BTI::getSetClass<string>()->createObject(m);
        if (!m->exceptionPending()) {
          OVar rec{r}; _PSet(string, r)->ommInit(&rec, argc, argv, m);
        }
      }
      else {
        r = BTI::getObjectSetClass()->createObject(m);
        if (!m->exceptionPending()) {
          OVar rec{r}; _PObjSet(r)->ommInit(&rec, argc, argv, m);
        }
      }
      break;
  }
  _CHECK_EXCEPTION();
  return r->disposableResult();
}

ORet ORTMgr::ommCreateEmptySet(OArg, int, OArg*, Session* m) {
  _ASSERT(static_cast<Session*>(this) == m, m);
  ORef r = BTI::getObjectSetClass()->createObject(m);
  _RETHROW("system::createSet");
  return r->disposableResult();
}

//::::::::::::::::::::::::::   Variable Creation   ::::::::::::::::::::::::::://

ORet ORTMgr::createAutoVar(int argc, OArg* argv, Session* m,
                           bool constVar, bool withValue) {
  _DECL_METHOD_NAME(withValue ?
      (constVar ? "system::const:value:" : "system::auto:value:") :
      (constVar ? "system::const:type:"  : "system::auto:type:"));
  _CHECK_ARGC(2);
  if (argv[0]->type != VARTYPE_INT_DATA) {
    throwBadArg(_ox_method_name, "unexpected variable type for argument 1");
    return undefinedResult();
  }
  const auto [var, n, varName] =
    g_resolveSymbolType(getCurrentMachine(), argv[0]->intValue, RSYM_LOCAL_DEF);
  if (!var || (!var->undefined() && var->type != VARTYPE_INT_DATA) || n != -1) {
    _THROW_RTE("'"+varName+"' was previously defined");
    return undefinedResult();
  }
  if (withValue) {
    *var = OVar{};
    emplace(*var, *argv[1]);
  }
  else {
    _CHECK_ARG_TYPEID(1, CLSID_STRING);
    OClass* const type = classMgr->getClass(_ARG(1)->castRef<oxString>());
    if (!type) {
      _THROW_RTE("'"+_ARG(1)->castRef<oxString>()+"' is not a type name");
      return undefinedResult();
    }
    ORef o = type->createObject(m);
    _RETHROW_HERE();
    *var = OVar{o};
  }
  if (constVar) {
    var->usage |= USG_CONST;
    return OVar(_REF(var), var->usage | USG_IGNORE_CONST);
  }
  return *var;
}

ORet ORTMgr::ommAutoVar(OArg, int argc, OArg* argv, Session* m) {
  return createAutoVar(argc, argv, m, false, false);
}

ORet ORTMgr::ommConstAutoVar(OArg, int argc, OArg* argv, Session* m) {
  return createAutoVar(argc, argv, m, true, false);
}

ORet ORTMgr::ommAutoVarWithValue(OArg, int argc, OArg* argv, Session* m) {
  return createAutoVar(argc, argv, m, false, true);
}

ORet ORTMgr::ommConstAutoVarWithValue(OArg, int argc, OArg* argv, Session* m) {
  return createAutoVar(argc, argv, m, true, true);
}

ORet ORTMgr::createStaticVar(int argc, OArg* argv, Session* m, bool constVar) {
  _DECL_METHOD_NAME(constVar ? "system::staticConst:type:" :
                               "system::static:type:");
  _CHECK_ARGC(2);
  _CHECK_ARG_TYPEID(1, CLSID_STRING);
  _UNPACK_CONST_REF(String, typeName, 1);
  if (argv[0]->type != VARTYPE_INT_DATA) {
    throwBadArg(_ox_method_name, "unexpected variable type for argument 1");
    return undefinedResult();
  }
  const auto varInfo = g_resolveSymbolType(getCurrentMachine(),
                                           argv[0]->intValue, RSYM_STATIC_DEF);
  // *argv[0] = OVar{};
  OVar* const varPtr = std::get<0>(varInfo);
  const int diagnostic = std::get<1>(varInfo);
  if (!varPtr) {
    const string& varName = std::get<2>(varInfo);
    const string shortVarName = varName.size() < 31 ? varName :
        varName.substr(0, 27)+"...";
    const string msg = diagnostic == -1 ? "was previously defined" :
        "is not a variable (please report this error)";
    _THROW_RTE("'"+shortVarName+"' "+msg);
    return undefinedResult();
  }
  if (diagnostic == -2)
    return OVar(_ARG(0), argv[0]->usage | USG_NOT_INITABLE);
  OClass* const typeClass = classMgr->getClass(typeName);
  if (!typeClass) {
    _THROW_RTE("'"+typeName+"' is not a type name");
    return undefinedResult();
  }
  if (diagnostic != -1) {
    const_ORef o = _REF(varPtr);
    if (o->getClass() != typeClass) {
      int line = getCurrentMachineValue<int>(M_REGISTER_LR);
      const string msg{"variable type '"+o->getClassName()+"' was "
          "redefined as '"+typeClass->name+"'"};
      m->throwRTE(_ox_method_name, msg, "", line);
      return undefinedResult();
    }
    return OVar(const_cast<ORef>(o), varPtr->usage | USG_NOT_INITABLE);
  }
  // create the object
  ORef o = typeClass->createObject(m);
  _RETHROW_HERE();
  *varPtr = OVar{o};
  if (constVar) {
    varPtr->usage |= USG_CONST;
    return OVar(o, varPtr->usage | USG_IGNORE_CONST);
  }
  return *varPtr;
}

ORet ORTMgr::ommStaticVar(OArg, int argc, OArg* argv, Session* m) {
  return createStaticVar(argc, argv, m, false);
}

ORet ORTMgr::ommConstStaticVar(OArg, int argc, OArg* argv, Session* m) {
  return createStaticVar(argc, argv, m, true);
}

ORet ORTMgr::createAutoTmp(int argc, OArg* argv, Session* m, bool constVar,
                                                             bool noauto) {
  _DECL_METHOD_NAME(noauto   ? "system::noauto:" :
                    constVar ? "system::const:"  : "system::auto:");
  _CHECK_ARGC(1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, typeName, 0);
  OClass* typeClass = classMgr->getClass(typeName);
  if (!typeClass) {
    throwRTE(_ox_method_name, "'"+typeName+"' is not a type name");
    return undefinedResult();
  }
  ORef o = typeClass->createObject(m);
  _RETHROW_HERE();
  constexpr ushort CONST_VAR = USG_CONST | USG_IGNORE_CONST | USG_DISPOSABLE;
  const ushort usage = noauto   ? USG_NONE :
                       constVar ? CONST_VAR : USG_DISPOSABLE;
  return OVariable{o, usage};
}

ORet ORTMgr::ommAutoTmp(OArg, int argc, OArg* argv, Session* m) {
  return createAutoTmp(argc, argv, m, false, false);
}

ORet ORTMgr::ommConstAutoTmp(OArg, int argc, OArg* argv, Session* m) {
  return createAutoTmp(argc, argv, m, true, false);
}

ORet ORTMgr::ommNoAuto(OArg, int argc, OArg* argv, Session* m) {
  return createAutoTmp(argc, argv, m, false, true);
}

ORet ORTMgr::ommRefVar(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::reference:", 1);
  if (argv[0]->type != VARTYPE_INT_DATA) {
    _THROW_RTE("reference is already declared");
    return undefinedResult();
  }
  const auto varInfo = g_resolveSymbolType(getCurrentMachine(),
                                           argv[0]->intValue, RSYM_LOCAL_DEF);
  // *argv[0] = OVar{};
  OVar* const varPtr = std::get<0>(varInfo);
  if (!varPtr) {
    _THROW_RTE("'"+std::get<2>(varInfo)+"' is not a variable");
    return undefinedResult();
  }
  *varPtr = OVar{m->nullObject(), USG_REF};
  return *varPtr;
}

//::::::::::::::::::::::::::::::::   Loops   ::::::::::::::::::::::::::::::::://

ORet ORTMgr::ommForLoop(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::for:while:step:do:", 4);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(2, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(3, CLSID_BLOCK);
  const int startBlockId = g_blockId(_ARG(0));
  const int whileBlockId = g_blockId(_ARG(1));
  const int stepBlockId  = g_blockId(_ARG(2));
  const int doBlockId    = g_blockId(_ARG(3));
  const vector<int> args{startBlockId, whileBlockId, stepBlockId, doBlockId};
  execLoopProc_(args, true, true);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

ORet ORTMgr::ommWhileLoop(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::while:do:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const int whileBlockId = g_blockId(_ARG(0));
  const int doBlockId    = g_blockId(_ARG(1));
  const vector<int> args{whileBlockId, doBlockId};
  execLoopProc_(args, true, true);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

ORet ORTMgr::ommUntilLoop(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::until:do:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const int untilBlockId = g_blockId(_ARG(0));
  const int doBlockId    = g_blockId(_ARG(1));
  const vector<int> args{untilBlockId, doBlockId};
  execLoopProc_(args, false, true);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

ORet ORTMgr::ommWhileAtEndLoop(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::do:while:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const int doBlockId    = g_blockId(_ARG(0));
  const int whileBlockId = g_blockId(_ARG(1));
  const vector<int> args{whileBlockId, doBlockId};
  execLoopProc_(args, true, false);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

ORet ORTMgr::ommUntilAtEndLoop(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::do:until:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const int doBlockId    = g_blockId(_ARG(0));
  const int untilBlockId = g_blockId(_ARG(1));
  const vector<int> args{untilBlockId, doBlockId};
  execLoopProc_(args, false, false);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

//:::::::::::::::::::::::::   If Then Elseif Else   :::::::::::::::::::::::::://

ORet ORTMgr::ommIfThen(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::if:then:", 2);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  execIfProc_(argv[0], argv[1], vector<OArg>(), nullptr);
  _RETHROW_HERE();
  if (isCurrentMachineReturning())
    return getCurrentMachineValue<OVar>(M_REGISTER_RES);
  return this->makeResult();
}

ORet ORTMgr::ommIfThenElse(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::if:then:else:", 3);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(2, CLSID_BLOCK);
  execIfProc_(argv[0], argv[1], vector<OArg>(), argv[2]);
  _RETHROW_HERE();
  if (isCurrentMachineReturning())
    return getCurrentMachineValue<OVar>(M_REGISTER_RES);
  return this->makeResult();
}

ORet ORTMgr::ommIfThenElseif(OArg, int argc, OArg argv[], Session* m) {
  _ENTER_NOCHK("system::if:then:elseif:then:");
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const vector<OArg> args(argv + 2, argv + argc);
  execIfProc_(argv[0], argv[1], args, nullptr);
  _RETHROW_HERE();
  if (isCurrentMachineReturning())
    return getCurrentMachineValue<OVar>(M_REGISTER_RES);
  return this->makeResult();
}

ORet ORTMgr::ommIfThenElseifElse(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::if:then:elseif:then:else:");
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  const vector<OArg> args(argv + 2, argv + (argc - 1));
  execIfProc_(argv[0], argv[1], args, argv[argc - 1]);
  _RETHROW_HERE();
  if (isCurrentMachineReturning())
    return getCurrentMachineValue<OVar>(M_REGISTER_RES);
  return this->makeResult();
}

//::::::::::::::::::::::::::::::::   Switch   :::::::::::::::::::::::::::::::://

ORet ORTMgr::ommSwitchCase(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::switch:case:do:");
  vector<OArg> vals, blocks;
  for (int i = 1; i < argc; i += 2) {
    vals.push_back(argv[i]);
    _CHECK_ARG_TYPEID(i + 1, CLSID_BLOCK);
    blocks.push_back(argv[i + 1]);
  }
  execSwitchProc_(argv[0], vals, blocks, nullptr);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

ORet ORTMgr::ommSwitchCaseDefault(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::switch:case:do:default:");
  _CHECK_ARG_TYPEID(argc - 1, CLSID_BLOCK);
  vector<OArg> vals, blocks;
  for (int i = 1; i < argc - 1; i += 2) {
    vals.push_back(argv[i]);
    _CHECK_ARG_TYPEID(i + 1, CLSID_BLOCK);
    blocks.push_back(argv[i + 1]);
  }
  execSwitchProc_(argv[0], vals, blocks, argv[argc - 1]);
  _RETHROW_HERE();
  return isCurrentMachineReturning() ?
      getCurrentMachineValue<OVar>(M_REGISTER_RES) :
      this->makeResult();
}

//:::::::::::::::::::::::::::::   Flow Control   ::::::::::::::::::::::::::::://

ORet ORTMgr::ommBreak(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::break", 0);
  controlProc_(1);
  return this->makeResult();
}

ORet ORTMgr::ommBreakMany(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::break:", 1);
  int n;
  switch (_ARG(0)->getClassId()) {
    case CLSID_ULONG: n = (int)g_typeValue<ulong>(_ARG(0)); break;
    case CLSID_LONG:  n = (int)g_typeValue<long> (_ARG(0)); break;
    case CLSID_UINT:  n = (int)g_typeValue<uint> (_ARG(0)); break;
    case CLSID_INT:   n = (int)g_typeValue<int>  (_ARG(0)); break;
    case CLSID_SHORT: n = (int)g_typeValue<short>(_ARG(0)); break;
    case CLSID_CHAR:  n = (int)g_typeValue<char> (_ARG(0)); break;
    case CLSID_BOOL:  n = (int)g_typeValue<bool> (_ARG(0)); break;
    default: m->throwBadArg(_ox_method_name, "unexpected argument type, "
        "expected integer or boolean");
      return undefinedResult();
  }
  if (n != 0) controlProc_(n);
  return this->makeResult();
}

ORet ORTMgr::ommBreakManyIf(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::break:if:", 2);
  int n;
  switch (_ARG(0)->getClassId()) {
    case CLSID_ULONG: n = (int)g_typeValue<ulong>(_ARG(0)); break;
    case CLSID_LONG:  n = (int)g_typeValue<long> (_ARG(0)); break;
    case CLSID_UINT:  n = (int)g_typeValue<uint> (_ARG(0)); break;
    case CLSID_INT:   n = (int)g_typeValue<int>  (_ARG(0)); break;
    case CLSID_SHORT: n = (int)g_typeValue<short>(_ARG(0)); break;
    case CLSID_CHAR:  n = (int)g_typeValue<char> (_ARG(0)); break;
    case CLSID_BOOL:  n = (int)g_typeValue<bool> (_ARG(0)); break;
    default: m->throwBadArg(_ox_method_name, "unexpected argument type, "
        "expected integer or boolean");
      return undefinedResult();
  }
  if (n < 0) {
    m->throwBadArg(_ox_method_name, "argument 1 value must be "
        "a non-negative integer");
    return undefinedResult();
  }
  if (n != 0) {
    const bool exec = execIfProc_(argv[1], nullptr, vector<OArg>(), nullptr);
    if (exec) controlProc_(n);
  }
  return this->makeResult();
}

ORet ORTMgr::ommReturn(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::return");
  ORet rv = g_execReturn(getCurrentMachine(), nullptr, false);
  _RETHROW_HERE();
  return rv;
}

ORet ORTMgr::ommReturnIf(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::returnIf:");
  const bool doReturn = _PBOOL(_ARG(0))->getValue();
  if (doReturn) {
    g_execReturn(getCurrentMachine(), nullptr, false);
    _RETHROW_HERE();
  }
  return undefinedResult();
}

ORet ORTMgr::ommReturnValue(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::return:", 1);
  ORet rv = g_execReturn(getCurrentMachine(), argv[0], false);
  _RETHROW_HERE();
  return rv;
}

ORet ORTMgr::ommReturnRef(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::returnReference:", 1);
  ORet rv = g_execReturn(getCurrentMachine(), argv[0], true);
  _RETHROW_HERE();
  return rv;
}

ORet ORTMgr::ommReturnConstRef(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::returnConstReference:", 1);
  ORet rv = g_execReturn(getCurrentMachine(), argv[0], true);
  _RETHROW_HERE();
  rv.setConst();
  return rv;
}

ORet ORTMgr::ommReturnThis(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::returnThis");
  auto const vm = getCurrentMachine();
  auto arg = static_cast<OArg>(g_getMachineValue(vm, M_THIS).get<void*>());
  ORet rv = g_execReturn(vm, arg, true);
  _RETHROW_HERE();
  return rv;
}

//::::::::::::::::::::::::::::::   Try Catch   ::::::::::::::::::::::::::::::://

ORet ORTMgr::ommTryCatch(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::try:catch:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  vector<int> b{g_blockId(_ARG(1))};
  ORet rv = g_execTryCatch(getCurrentMachine(), argv[0], vector<int>{}, b);
  _RETHROW_HERE();
  return rv.undefined() ? this->makeResult() : rv;
}

ORet ORTMgr::ommTryCatchDo(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("system::try:catch:do:");
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  //if (argc % 2 == 0) throwBadArgc(_ox_method_name, "try:catch:do:", argc);
  vector<int> catchBlocks;
  vector<int> doBlocks;
  for (int i = 1; i < argc; ++i) {
    _CHECK_ARG_TYPEID(i, CLSID_BLOCK);
    catchBlocks.push_back(g_blockId(_ARG(i)));
    ++i;
    _CHECK_ARG_TYPEID(i, CLSID_BLOCK);
    doBlocks.push_back(g_blockId(_ARG(i)));
  }
  ORet rv = g_execTryCatch(getCurrentMachine(), argv[0], catchBlocks, doBlocks);
  _RETHROW_HERE();
  return rv.undefined() ? this->makeResult() : rv;
}

ORet ORTMgr::ommThrow(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::throw:", 1);
  if (m->exceptionPending()) {
    m->rethrow(_ox_method_name);
  }
  else {
    _ASSERT(_ARG(0), m);
    switch (_ARG(0)->getRootClassId()) {
      case CLSID_STRING: {
        _UNPACK_REF(String, message, 0);
        int line = g_getMachineValue(mainMachine_, M_REGISTER_LR).get<int>();
        m->throwRTE(_ox_method_name, message, "", line);
        break;
      }
      case CLSID_EXCEPT:
      case CLSID_BAD_CLASS:
      case CLSID_BAD_ARG:
      case CLSID_BAD_ARGC:
      case CLSID_BAD_USAGE:
      case CLSID_BAD_CORECOPY:
      case CLSID_RTE:
        m->setException(m->duplicateObject(_ARG(0)));
        break;
      default: {
        const string msg = "unexpected type for argument 1\n"
                           "expected exception type or string, "
                           "found '"+_ARG(0)->getClassName()+"'";
        m->throwBadArg(_ox_method_name, msg);
        return undefinedResult();
      }
    }
  }
  return makeResult();
}

ORet ORTMgr::ommCheck(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  _ENTER("system::check:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, expectedText, 0);
  m->performCheck(expectedText);
  _CHECK_EXCEPTION();
  return makeResult();
}

ORet ORTMgr::ommMergeCheck(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  m->mergeCheck();
  return makeResult();
}

ORet ORTMgr::ommIgnoreCheck(OArg, int, OArg*, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  m->ignoreCheckpoint();
  return makeResult();
}

ORet ORTMgr::ommSetCheckpoint(OArg, int, OArg*, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  m->setCheckpoint();
  return makeResult();
}

ORet ORTMgr::ommExpectException(OArg, int argc, OArg* argv, Session* m) {
  _ASSERT(m == static_cast<Session*>(this), m);
  const char* _ox_method_name = argc == 2 ? "system::expectedException:line:" :
      "system::expectedException:line:location:message:";
  _CHECK_ARGC_2(2, 4);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  const int line = g_castValue<int>(argv + 1, m);
  _RETHROW_HERE();
  if (argc == 4) {
    _CHECK_ARG_TYPE(2, BTI::getTypeClass<string>());
    _CHECK_ARG_TYPE(3, BTI::getTypeClass<string>());
  }
  if (testingMode()) {
    _UNPACK_CONST_REF(String, except, 0);
    const string& loc = argc == 4 ? *static_cast<string*>(_PString(_ARG(2))) : "";
    const string& msg = argc == 4 ? *static_cast<string*>(_PString(_ARG(3))) : "";
    if (!checker_.expectException(line, except, loc, msg))
      _THROW_RTE("incorect specification for expected exception");
  }
  return makeResult();
}

ORet ORTMgr::ommExpectLeftovers(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::expectedLeftovers:", 1);
  const size_t leftovers = g_castValue<size_t>(argv, m);
  _RETHROW_HERE();
  if (testingMode())
    checker_.expectedLeftovers = leftovers;
  return makeResult();
}

ORet ORTMgr::ommExpectRetValue(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("system::expectedReturnValue:", 1);
  const int retValue = g_castValue<int>(argv, m);
  _RETHROW_HERE();
  if (testingMode())
    checker_.expectedRetValue = retValue;
  return makeResult();
}
