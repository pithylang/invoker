#include <string>
#include <locale>
#include <charconv>
#include "itypes.hpp"
#include "iparseTools.hpp"
#include "iparser.hpp"

/** See Notes at https://en.cppreference.com/w/cpp/string/byte/isxdigit */
static inline bool isHexa(char c) noexcept
// { return isdigit(c) || (toupper(c) < 'G'  &&  toupper(c) > 'A' - (char)1); }
{ return std::isxdigit(static_cast<unsigned char>(c)); }

using std::string;
using yy::parser;
using symbol_t = yy::parser::symbol_type;

void g_replaceAll(string& s, const string& from, const string& to);

static constexpr parser::token_kind_type END = parser::token_kind_type::TOK_END;

//============================================================================//
//::::::::::::::::::::::::::::::::::  lex  ::::::::::::::::::::::::::::::::::://
//============================================================================//

symbol_t IParser::lex() {
  skipBlank();
  location.step();

  ASSERT(location.end.line == lineNumber_ + 1);
  ASSERT(location.end.column == getColumn() + 1);

  // special handling for system symbol
  if (*cursor_ == systemSymbol) {
    advance();
    return parser::make_ID(ISymbol(string(systemName), SYMTYPE_LITERAL_ID),
        location);
  }

  // parse token
  switch (*cursor_) {
    case END: return parser::make_END(location);
    case '+': return lexPlus();
    case '-': return lexMinus();
    case '*': return lexMul();
    case '/': // division operator or begin of comment
      if (parseComment()) return lex();
      return lexDiv();

    case '%': return lexMod();
    case ':': return lexColon();

    case '@':
      advance();
      if (*cursor_ == '=') {
        advance();
        return parser::make_CCOPY(location);
      }
      return parser::make_EVAL(location);

    case '(': advance(); return parser::make_LP(location);
    case ')': advance(); return parser::make_RP(location);
    case ';': advance(); return parser::make_SEMI(location);
    case '{': advance(); return parser::make_LCB(location);
    case '}': advance(); return parser::make_RCB(location);
    case '[': advance(); return parser::make_LB(location);
    case ']': advance(); return parser::make_RB(location);
    case ',': advance(); return parser::make_COMMA(location);

    case '=': return lexEq();
    case '!':
      advance();
      if (*cursor_ == '=') {
        advance();
        return parser::make_DIF(location);
      }
#if 0
      else if (*cursor_ == '!' && cursor_[1] == '!') {
        cursor_ += 2;
        location.columns(2);
        return parser::make_UNUL(location);
      }
#endif // 0
      else
        return parser::make_LNOT(location);

    case '>': return lexGt();
    case '<': return lexLt();
    case '|': return lexVBar();
    case '&': return lexAnd();
    case '^': return lexCarret();
    case '~': advance(); return parser::make_BNOT(location);

    case '?': advance(); return parser::make_QMARK(location);
#if 0
      if (*cursor_ == '?') {
        advance();
        return parser::make_UNNUL(location);
      }
      else
        return parser::make_SELID(ISymbol("if:", SYMTYPE_LITERAL_SEL), location);
#endif // 0

    case '`':
      return parseExtendedId();

    case '0':
      switch (toupper(cursor_[1])) {
        case 'X': return parseHexa();
        case 'B': return parseBinary();
      }
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9': // integer or FP number
      return parseNumber();

    case '.':
      if (isdigit(cursor_[1])) return parseNumber();
      advance();
      if (*cursor_ != '.') return parser::make_DOT(location);
      advance();
      if (*cursor_ != '.') return parser::make_DOTS(location);
      advance();
      return parser::make_ELLIPS(location);

    case '\"':
      return parseString();

    case '\'':
      return parseCharConst();

    // case '#':
    //   switchSourceFile();
    //   return lex();

    default:
      ;
  }

  return parseId();
}

void IParser::skipBlank() {
  // skip leading space
  while (*cursor_ == ' ' || *cursor_ == '\t' || *cursor_ == '\n' || *cursor_ == '\r') {
    if (*cursor_ == '\n') advanceLine();
    else advance();
  }
}

char* IParser::lookAheadSkip(int c) {
  skipBlank();
  // skip comment
  if (*cursor_ && *cursor_ == '/' && (cursor_[1] == '/' || cursor_[1] == '*')) {
    parseComment();
    return lookAheadSkip(c);
  }
  // return position of cursor_ if the supplied token is matched
  return *cursor_ == c ? cursor_ + 1 : nullptr;
}

symbol_t IParser::lexPlus() {
  advance();
  switch (*cursor_) {
    case '+':
      advance();
      return parser::make_PLUSPLUS(location);
    case '=':
      advance();
#if 0
      if (*cursor_ == ':') {
        advance();
        return parser::make_XTOBY(location);
      }
#endif // 0
      return parser::make_PLUSEQ(location);
#if 0
    case ':':
      advance();
      return parser::make_XTO(location);
#endif // 0
  }
  return parser::make_PLUS(location);
}

symbol_t IParser::lexMinus() {
  advance();
  switch (*cursor_) {
    case '-': advance(); return parser::make_MINUSMINUS(location);
    case '=': advance(); return parser::make_MINUSEQ(location);
  }
  return parser::make_MINUS(location);
}

symbol_t IParser::lexMul() {
  advance();
  switch (*cursor_) {
    case '*': advance(); return parser::make_EXP(location);
    case '=': advance(); return parser::make_MULEQ(location);
  }
  return parser::make_MUL(location);
}

symbol_t IParser::lexDiv() {
  advance();
  if (*cursor_ == '=') {
    advance();
    return parser::make_DIVEQ(location);
  }
  return parser::make_DIV(location);
}

symbol_t IParser::lexMod() {
  advance();
  if (*cursor_ == '=') {
    advance();
    return parser::make_MODEQ(location);
  }
  return parser::make_MOD(location);
}

symbol_t IParser::lexEq() {
  advance();
  if (*cursor_ == '=') {
    advance();
    return parser::make_EQ(location);
  }
  else if (*cursor_ == '>') {
    advance();
    return parser::make_FWD(location);
  }
  return parser::make_COPY(location);
}

symbol_t IParser::lexGt() {
  switch (*++cursor_) {
    case '=':
      ++cursor_;
      location.columns(2);
      return parser::make_GE(location);
    case '>':
      switch (*++cursor_) {
        case '=':
          ++cursor_;
          location.columns(3);
          return parser::make_SHREQ(location);
      }
      location.columns(2);
      return parser::make_SHR(location);
  }
  location.columns(1);
  // check if next token is '}'
  char* backupCursor = cursor_;
  auto backupLineNumber = lineNumber_;
  yy::location backupLocation = location;
  char *p = lookAheadSkip('}');
  if (!p) {
    cursor_ = backupCursor;
    location = backupLocation;
    lineNumber_ = backupLineNumber;
    return parser::make_GT(location);
  }
  cursor_ = p;
  location.columns(1);
  return parser::make_RTI(location);
}

symbol_t IParser::lexLt() {
  switch (*++cursor_) {
    case '=':
      ++cursor_;
      location.columns(2);
      return parser::make_LE(location);
    case '<':
      switch (*++cursor_) {
        case '=':
          ++cursor_;
          location.columns(3);
          return parser::make_SHLEQ(location);
      }
      location.columns(2);
      return parser::make_SHL(location);
  }
  location.columns(1);
  return parser::make_LT(location);
}

symbol_t IParser::lexVBar() {
  switch (*++cursor_) {
    case '|':
      ++cursor_;
      if (*cursor_ == '=')
      {
        ++cursor_;
        location.columns(3);
        return parser::make_OREQ(location);
      }
      location.columns(2);
      return parser::make_OR(location);
    case '=':
      ++cursor_;
      location.columns(2);
      return parser::make_BOREQ(location);
  }
  location.columns(1);
  return parser::make_BOR(location);
}

symbol_t IParser::lexAnd() {
  switch (*++cursor_) {
    case '&':
      ++cursor_;
      if (*cursor_ == '=') {
        ++cursor_;
        location.columns(3);
        return parser::make_ANDEQ(location);
      }
      location.columns(2);
      return parser::make_AND(location);
    case '=':
      ++cursor_;
      location.columns(2);
      return parser::make_BANDEQ(location);
  }
  location.columns(1);
  return parser::make_BAND(location);
}

symbol_t IParser::lexCarret() {
  switch (*++cursor_) {
    case '^':
      ++cursor_;
      if (*cursor_ == '=') {
        ++cursor_;
        location.columns(3);
        return parser::make_XOREQ(location);
      }
      location.columns(2);
      return parser::make_XOR(location);
    case '=':
      ++cursor_;
      location.columns(2);
      return parser::make_BXOREQ(location);
  }
  location.columns(1);
  return parser::make_BXOR(location);
}

symbol_t IParser::lexColon() {
  advance();
  switch (*cursor_) {
    case '=': advance(); return parser::make_REFSET(location);
    case ':': advance();
      if (*cursor_ == '=') {
        advance();
        return parser::make_CREFSET(location);
      }
      return parser::make_DCOL(location);
  }
  return parser::make_COL(location);
}

symbol_t IParser::parseNumber() {
  bool isInteger = true;
  // Mark start of token
  const char *token = cursor_;
  // Token length counter
  int len = 0;
  // Start
  if (*cursor_ != '.') {
    while (isdigit(*cursor_)) advance(len);
  }
  if (*cursor_ == '.') {
    // FP constant
    advance(len);
    while (isdigit(*cursor_)) advance(len);
    isInteger = false;
  }
  if (toupper(*cursor_) == 'E') {
    // Skip 'e'
    advance(len);
    // Parse exponent
    if (*cursor_ == '+' || *cursor_ == '-') advance(len);
    if (!isdigit(*cursor_)) { // error, incomplete fp
      cursor_ = const_cast<char*>(token);
      string description{"incomplete floating point constant"};
      throw parser::syntax_error{location, description};
    }
    // Numerical part of exponent
    while (isdigit(*cursor_)) advance(len);
    isInteger = false;
  }

  if (isInteger) {
    switch (toupper(*cursor_)) {
      case 'U':
        advance();
        if (toupper(*cursor_) == 'L') {
          advance();
          return parser::make_ULONG(ISymbol(token, len, SYMTYPE_LITERAL_ULONG), location);
        }
        return parser::make_UINT(ISymbol(token, len, SYMTYPE_LITERAL_UINT), location);
      case 'L':
        advance();
        return parser::make_LONG(ISymbol(token, len, SYMTYPE_LITERAL_LONG), location);
      case 'S':
        advance();
        return parser::make_SHORT(ISymbol(token, len, SYMTYPE_LITERAL_SHORT), location);
      case 'C':
        advance();
        return parser::make_CHAR(ISymbol(token, len, SYMTYPE_LITERAL_CHAR), location);
      default:
        return parser::make_INT(ISymbol(token, len, SYMTYPE_LITERAL_INT), location);
    }
  }
  // Floating point number
  switch (toupper(*cursor_)) {
    case 'F':
      advance();
      return parser::make_FLT(ISymbol(token, len, SYMTYPE_LITERAL_FLOAT), location);
    case 'D':
      advance();
  }
  return parser::make_DBL(ISymbol(token, len, SYMTYPE_LITERAL_DOUBLE), location);
}

symbol_t IParser::parseHexa() {
  moveForward(2);
  // mark start of new token
  const char* token = cursor_;
  ulong value = strtoul(token, &cursor_, 16);
  // token length counter
  auto len = cursor_ - token;
  location.columns(len);
  if (len == 0 || len > 16) {
    cursor_ = const_cast<char*>(token) - 2;
    const string description(len == 0 ? "incomplete hexadecimal integer"
                                      : "hexadecimal integer too long");
    throw parser::syntax_error(location, description);
  }
  constexpr ushort fl = SYMTYPE_LITERAL_HEXA;
  if (len > 8)
    return parser::make_ULONG(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_ULONG), location);
  else if (len > 4)
    return parser::make_UINT(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_UINT), location);
  else
    return parser::make_SHORT(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_SHORT), location);
}

symbol_t IParser::parseBinary() {
  moveForward(2);
  // mark start of new token
  const char* token = cursor_;
  ulong value = strtoul(token, &cursor_, 2);
  // token length counter
  auto len = cursor_ - token;
  location.columns(len);
  if (len == 0 || len > 64) {
    cursor_ = const_cast<char*>(token) - 2;
    const string description(len == 0 ? "incomplete binary integer"
                                      : "binary integer too long");
    throw parser::syntax_error(location, description);
  }
  constexpr ushort fl = SYMTYPE_LITERAL_BINARY;
  if (len > 64)
    return parser::make_ULONG(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_ULONG), location);
  else if (len > 32)
    return parser::make_UINT(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_UINT), location);
  else
    return parser::make_SHORT(
        ISymbol(token, len, fl | SYMTYPE_LITERAL_SHORT), location);
}

std::pair<int, bool> IParser::translateChar() {
  if (*cursor_ != '\\')
    return {*cursor_, false};
  advance(); // skip '\\'
  switch (*cursor_) { // translate character
    case 'a':  return {'\a', false};
    case 'b':  return {'\b', false};
    case 'f':  return {'\f', false};
    case 'n':  return {'\n', false};
    case 'r':  return {'\r', false};
    case 't':  return {'\t', false};
    case 'v':  return {'\v', false};
    case '\'': return {'\'', false};
    case '\"': return {'\"', false};
    case '\\': return {'\\', false};
    case '?':  return {'?',  false};
    case '0':  return {0,    false};
    case 'x':  {
      advance();
      int result;
      auto [ptr, ec] = std::from_chars(cursor_, cursor_ + 4, result, 16);
      if (ec == std::errc{}) {
        auto d = ptr - cursor_;
        moveForward(d);
        return {result, true};
      }
      return {-1, true};
    }
  }
  return {*cursor_, false};
}

static std::wstring s_toWide(const std::locale& loc, const string& text);

symbol_t IParser::parseCharConst() {
  // skip '\'' character
  advance();
  // check for empty character
  if (*cursor_ == '\'')
    throw parser::syntax_error(location, "empty character specification");
  // backup position
  char* backupCursor = cursor_;
  auto backupLineNumber = lineNumber_;
  yy::location backupLocation = location;
  // try to parse and possibly translate a character
  const auto [value, hex] = translateChar();
  if (!hex) {
    advance();
    // skip trailing '\'' character
    if (*cursor_ == '\'') {
      advance();
      return parser::make_CHAR(
          ISymbol(string(1, value), SYMTYPE_LITERAL_CHAR), location);
    }
  }
  else if (value != -1 && *cursor_ == '\'') {
    const auto d = cursor_ - (backupCursor + 2);
    string str(backupCursor + 2, d);
    // skip trailing '\'' character
    advance();
    constexpr ushort flags = SYMTYPE_LITERAL_CHAR | SYMTYPE_LITERAL_HEXA;
    return parser::make_CHAR(ISymbol{std::move(str), flags}, location);
  }
  else
    throw parser::syntax_error(location, "incorrect character specification");
  // not a std character - restore position
  cursor_ = backupCursor;
  location = backupLocation;
  lineNumber_ = backupLineNumber;
  // try to parse a wide (4-bytes long) character
  size_t i = 0;
  while (i < sizeof(wchar_t) && cursor_[i] && cursor_[i] != '\'') ++i;
  if (cursor_[i] != '\'')
    throw parser::syntax_error(location, "incomplete character constant");
  std::wstring wideCharString;
  try {
    std::locale loc{"C.utf8"};
    string wideChar(cursor_, i);
    wideCharString = s_toWide(loc, wideChar);
  }
  catch (const std::runtime_error&) {
    throw parser::syntax_error(location, "incorrect or wide character; "
        "unable to read it due to missing classic unicode locale");
  }
  catch (const string& msg) {
    throw parser::syntax_error(location, msg);
  }
  if (wideCharString.size() != 1)
    throw parser::syntax_error(location, "incorrect wide character");
  wchar_t wch = wideCharString[0];
  moveForward(i + 1);
  const auto ui = static_cast<unsigned>(wch);
  return parser::make_UINT(ISymbol(std::to_string(ui), SYMTYPE_LITERAL_UINT),
      location);
}

char* IParser::parseStringAttrs(size_t& attrs) {
  if (!*cursor_) return nullptr;
  switch (*cursor_) {
    case 'a':
    case 'i': attrs = SAT_AUTOVAR; break;
    case 'n': attrs = SAT_NOAUTOVAR; break;
    case 'c': attrs = SAT_CONSTVAR; break;
    case 'C': attrs = SAT_CLASSOBJ; break;
    case 'P': attrs = SAT_PACKAGE; break;
    case ':':
      if (cursor_[1] == ':') return nullptr;
      attrs = SAT_SELID; break;
    case 'v': attrs = SAT_ID; break;
    default: return nullptr;
  }
  advance();
  if (attrs == SAT_SELID)
    return cursor_;
  const char c = toupper(*cursor_);
  if (c == '_' || (c > 'A' - 1 && c < 'Z' + 1) || (c > '0' - 1 && c < '9' + 1))
    return nullptr;
  return cursor_;
}

symbol_t IParser::parseString() {
  // Remember current cursor position skipping leading '"' character
  char *oldCursorPos = cursor_;
  bool ended = false;
  string tok;

  advance();
  while (*cursor_) {
    if (*cursor_ == '\"') { // merge consecutive strings
      advance();
      char* backupCursor = cursor_;
      auto backupLineNumber = lineNumber_;
      yy::location backupLocation = location;
      char *p = lookAheadSkip('\"');
      if (!p) {
        cursor_ = backupCursor;
        location = backupLocation;
        lineNumber_ = backupLineNumber;
        size_t attrs;
        if (tok.empty() || !parseStringAttrs(attrs)) {
          ended = true;
          break;
        }
        // string with attributes
        if (attrs == SAT_SELID) {
          g_replaceAll(tok, ":", "\x1f");
          tok.push_back(':');
          return parser::make_SELID(
              ISymbol(std::move(tok), SYMTYPE_LITERAL_SEL), location);
        }
        if (attrs == SAT_ID) return parser::make_ID(
            ISymbol(std::move(tok), SYMTYPE_LITERAL_ID), location);
        return parser::make_STRWATTR(
            IStringWithAttrs{std::move(tok), attrs}, location);
      }
      cursor_ = p;
      location.columns(1);
    }
    else if (*cursor_ == '\n') { // ended is false
      break;
    }
    else {
      const auto [ch, hex] = translateChar();
      if (ch == -1)
        throw parser::syntax_error(location,
            "incorrect character specification");
      tok.push_back(ch);
      if (!hex || (hex && *cursor_ == ' '))
        advance();
    }
  }

  if (!ended) {
    cursor_ = oldCursorPos;
    throw parser::syntax_error(location, "incomplete string");
  }

  return parser::make_STRING(
      ISymbol(std::move(tok), SYMTYPE_LITERAL_STRING), location);
}

bool IParser::parseComment() {
  // Remember current cursor_ position
  char *oldCursorPos = cursor_;

  if (cursor_[0] == '/' && cursor_[1] == '/') {
    moveForward(2);
    int i = 2;
    while (!eof() && *cursor_ != '\n') advance(i);
    if (*cursor_ == '\n') {
      ++lineNumber_;
      ++cursor_;
      location.lines(1);
    }
    else
      location.columns(i);
    return true;
  }

  int iNestLevel = 0;
  while (*cursor_) {
    if (*cursor_ == '/' && cursor_[1] == '*') {
      ++iNestLevel;
      moveForward(2);
    }
    else if (*cursor_ == '*' && cursor_[1] == '/') {
      --iNestLevel;
      moveForward(2);
      if (iNestLevel == 0) return true;
    }
    else if (iNestLevel == 0) {
      ASSERT(oldCursorPos == cursor_);
      return false;
    }
    else {
      if (*cursor_ == '\n') advanceLine();
      else advance();
    }
  }

  // Error: file ended before comment
  const string description("missing end of comment '*/'");
  throw parser::syntax_error(location, description);

  return false;
}

symbol_t IParser::parseId() {
  // Mark token begin
  const auto token = cursor_;
  // Token length counter
  int len = 0;
  // Start - first letter must be alpha
  const int c = toupper(*cursor_);
  if (c == '_' || (c > 'A' - 1 && c < 'Z' + 1)) {
    advance(len);
  }
  else {
    const string description = string("invalid character ") + (isprint(*cursor_)
        ? string("'")+*cursor_+string("'")
        : std::to_string((int)*cursor_)) + " in token";
    throw yy::parser::syntax_error(location, description);
  }

  // Subsequent letters can be alphanumeric
  bool isSelector = false;
  while (*cursor_) {
    const int c = toupper(*cursor_);
    if (c == '_' || (c > 'A' - 1 && c < 'Z' + 1) ||
                    (c > '0' - 1 && c < '9' + 1)) {
      advance(len);
    }
    else if (c == ':') {
      if (cursor_[1] == ':') break;
      advance(len);
      isSelector = true;
      break;
    }
    else {
      break;
    }
  }

  return isSelector
      ? parser::make_SELID(ISymbol(token, len, SYMTYPE_LITERAL_SEL), location)
      : parser::make_ID(ISymbol(token, len, SYMTYPE_LITERAL_ID), location);
}

static bool _invalidExtendedIdChar(char ch) {
  return ch == ':' || ch == '.' || ch == '\n';
}

static bool _isExtendedIdOK(const string& token) {
  if (token.empty()) return false;
  if (token[0] != '#') return true;
  for (string::size_type i = 1; i < token.length(); ++i)
    if (!isdigit(token[i])) return true;
  return false;
}

symbol_t IParser::parseExtendedId() {
  // Remember current cursor_ position skipping leading ` character
  char *oldCursorPos = cursor_;
  bool isSelector = false;
  string tok;

  advance();
  while (*cursor_ && *cursor_ != '\'' && !_invalidExtendedIdChar(*cursor_)) {
    tok.push_back(*cursor_);
    advance();
  }

  if (!*cursor_ || _invalidExtendedIdChar(*cursor_)) {
    cursor_ = oldCursorPos;
    string description;
    switch (*cursor_) {
      case ':' : description = "missing ' before ':'"; break;
      case '.' : description = "incorrect character in identifier: '.'"; break;
      case '\n': description = "missing '"; break;
    }
    throw parser::syntax_error(location, description);
  }

  advance();
  if (*cursor_ == ':' && cursor_[1] != ':') {
    tok.push_back(*cursor_);
    advance();
    isSelector = true;
  }

  if ((!isSelector && !_isExtendedIdOK(tok)) ||
      (isSelector && tok.length() == 1)) {
    cursor_ = oldCursorPos;
    const string description = "'"+
        tok.substr(0, 64)+(tok.size() > 64 ? "..." : "")+
        string("' is not an acceptable identifier");
    throw parser::syntax_error(location, description);
  }

  return isSelector
    ? parser::make_SELID(ISymbol(std::move(tok), SYMTYPE_LITERAL_SEL), location)
    : parser::make_ID(ISymbol(std::move(tok), SYMTYPE_LITERAL_ID), location);
}

size_t IParser::getColumn() const {
  char *p = const_cast<char *>(script);
  string::size_type pos = 0;
  while (p + pos < cursor_) {
    auto next = string(p).find('\n', pos);
    if (next == string::npos || p + next >= cursor_)
      return cursor_ - (p + pos);
    pos = next + 1;
  }
  return 0;
}

//:::::::::::::::::::::::   locale related utilities   ::::::::::::::::::::::://

static void s_noConversionError(const std::locale& loc) {
  string localeName = loc.name();
  if (localeName.size() > 8)
    localeName = localeName.substr(0, 16) + "...";
  throw "the locale '"+localeName+"' does not implement "
        "the requested conversion";
}

static
void s_conversionError(std::codecvt_base::result ec, const string& funcName) {
  const string msg = (ec == std::codecvt_base::partial) ?
      "not enough space in the output buffer "
      "or unexpected end of source buffer" :
    (ec == std::codecvt_base::error) ?
      "character could not be converted" :
      "this facet is non-converting, no output written";
  throw funcName+": "+msg;
}

std::wstring s_toWide(const std::locale& loc, const string& text) {
  if (!std::has_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(loc))
    s_noConversionError(loc);
  auto const& f =
      std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(loc);
  std::mbstate_t mb{};
  std::wstring to(text.size(), '\0');
  const char* fromNext;
  wchar_t* toNext;
  const string& from = text;
  const auto ec = f.in(mb, &from[0], &from[from.size()], fromNext,
                           &to[0],   &to[to.size()],     toNext);
  if (ec != std::codecvt_base::ok)
    s_conversionError(ec, "toWide");
  to.resize(toNext - &to[0]);
  return to;
}
