#include "object.hpp"
#include "multiType.hpp"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "ortmgr.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "oclassobj.hpp"
#include "otypes.h"
// #include "oexcept.hpp"
#include "obasicTypeInfo.hpp"
#include "odev.h"
#include "ovariable.hpp"

using std::string;
using std::vector;

#define _REF(var) (var)->objectRef

//============================================================================//
//:::::::::::::::::::::::   OInstance Implementation   ::::::::::::::::::::::://
//============================================================================//

bool OInstance::swapContent(ORef r) noexcept {
  props_.swap(r->props_);
  return class_->moveCore(this, r) != nullptr;
}

// _BEGIN_METHOD_TABLE(OInstance, BASE_CLASS)
oxCInheritanceData _OM_LINK OInstance::sm_mtab = {
  nullptr, OInstance::sm_SMethodTable
};
OCoreMethodInfo _OM_LINK OInstance::sm_SMethodTable[] = {
  PUBLIC (OInstance::ommConst,            "const")
  PUBLIC (OInstance::ommNoMove,           "nomove")
  PUBLIC (OInstance::ommNoAuto,           "noauto")
  PUBLIC (OInstance::ommMove,             "move")
  PUBLIC (OInstance::ommRemit,            "remit")
  PUBLIC (OInstance::ommPassLiteral,      "pass")
  PUBLIC (OInstance::ommApply,            "apply:")
  PUBLIC (OInstance::ommApply,            "apply:args:")
  PUBLIC (OInstance::ommDuplicate,        "dup")
  PUBLIC (OInstance::ommDuplicate,        "duplicate")
  PUBLIC (OInstance::ommMoveObject,       "move:")
  PUBLIC (OInstance::ommExtend,           "member:value:")
  PUBLIC (OInstance::ommExtend,           "memb:val:")
  PUBLIC (OInstance::ommExtendBy,         "extendBy:")
  PUBLIC (OInstance::ommCreateExtension,  "extend:")
  PUBLIC (OInstance::ommInitTemplateProp, "prop:type:")
  PUBLIC (OInstance::ommInitTemplateProp, "property:type:")
  PUBLIC (OInstance::ommIsNull,           "isNull")
  PUBLIC (OInstance::ommGetRootClassId,   "ctypeid")
//   PUBLIC (OInstance::defaultCopy,       "defaultCopy:")
  PUBLIC (OInstance::ommDelete,           "delete")
  PUBLIC (OInstance::ommGetProp,          "property:")
  PUBLIC (OInstance::ommGetProp,          "prop:")
  PUBLIC (OInstance::inheritedCopy,       "inheritedCopy:")
  PUBLIC (OInstance::ommGetObjectRef,     "oref")
  PUBLIC (OInstance::ommGetClassObject,   "class")
  PUBLIC (OInstance::ommGetClassId,       "typeid")
  PUBLIC (OInstance::ommTypeName,         "type")
  PUBLIC (OInstance::ommTypeName,         "typename")
  PUBLIC (OInstance::ommType,             "type:")
  PUBLIC (OInstance::ommTypeOf,           "typeof:")
  PUBLIC (OInstance::ommSame,             "same:")
  PUBLIC (OInstance::ommConditional,      "optFor:other:")
// _END_METHOD_TABLE()
  {(CAddr)0, 0, nullptr}
};

//:::::::::::::::::::::::::   diagnostic messages   :::::::::::::::::::::::::://

static string s_getExpected(const vector<const OClass*>& types);
static string s_getStringExpected(const vector<int>& es);

string OInstance::typeCheckMessage(const vector<const OClass*>& types,
                                   int which, int n) const {
#ifdef __DEBUG__
  for (auto* type : types)
    ASSERT(type->isCoreClass())
#endif // __DEBUG__
  const OClass* const c = getRootClass();
  const auto e = types.end();
  const auto b = types.begin();
  const bool r = !types.empty() &&
      std::find_if(b, e, [c](const OClass* p) {return c == p;}) != e;
  return r ? "" :
      typeCheckMessage(r, getClassName(), s_getExpected(types), which, n);
}

string OInstance::typeCheckMessage(const OClass* type, int which, int n) const {
  if (!type)
    return typeCheckMessage(vector<const OClass*>{}, which, n);
  ASSERT(type->isCoreClass());
  if (getRootClass() == type) return "";
  return typeCheckMessage(false, getClassName(), type->name, which, n);
}

string OInstance::typeCheckMessage(Session* m, const vector<int>& typeIds,
                                   int which, int argn) const {
  vector<const OClass*> types(typeIds.size());
  for (size_t i = 0; i < typeIds.size(); ++i)
    types[i] = m->getClass(typeIds[i]);
  return typeCheckMessage(types, which, argn);
}

/* static */
string OInstance::typeCheckMessage(bool test,
                                   const std::string& suppliedTypeName,
                                   const std::string& expectedTypeName,
                                   int which, int n) {
  if (test) return "";
  const string msg =
      (which == 0) ? "variable" :
      (which  > 0) ? "argument "+std::to_string(which) :
                     "element "+std::to_string(-which-1)+" "
                     "of argument "+std::to_string(n);
  const string& es = expectedTypeName;
  const string& ss = suppliedTypeName;
  const string expected = es.empty() ? "" : ": expected '"+es;
  const string found = ss.empty() ? "" :
      (es.empty() ? ": '" : "' but found '")+ss+"'";
  return "unexpected type for "+msg+expected+found;
}

/* static */
string OInstance::sizeCheckMessage(
    int suppliedSize, const vector<int>& expectedSizes, int which, int n) {
  const auto& es = expectedSizes;
  const bool isRange = es.size() == 2 && es[1] < 0;
  _PROG_ERROR_MSG_IF(isRange && es[0] < 0, "incorrect arg range: "
      "{"+std::to_string(es[0])+", "+std::to_string(es[1])+"}");
  const bool isInfRange = es.size() == 1 && es[0] < 0;
  const bool test =
      expectedSizes.empty() ? false :
      isRange ? (suppliedSize >= es[0] && suppliedSize <= -es[1]) :
      isInfRange ? (suppliedSize >= -es[0]) :
      std::find(es.begin(), es.end(), suppliedSize) != es.end();
  if (test) return "";
  string extra = which < 0 && n > 0 ? " of argument "+std::to_string(n) : "";
  string s = which == 0 ? (n == 0 ? "arguments: " : "variable: ") :
             which  > 0 ? "argument "+std::to_string(which)+": " :
                          "element "+std::to_string(-which-1)+extra+": ";
  string msg = "unexpected "+
               string{ which == 0 && n == 0 ? "number of " : "size for " }+s;
  string expected = es.empty() ? "" : "expected "+(isInfRange ?
      "at least "+std::to_string(-es.at(0)) : std::to_string(es.at(0)));
  if (isRange)
    expected += " to "+std::to_string(-es[1])+", but";
  else if (es.size() == 1)
    expected += ", but";
  else
    expected += s_getStringExpected(es)+", but";
  if (!expected.empty())
    expected += " ";
  return msg+expected+"found "+std::to_string(suppliedSize)+" items";
}

string s_getExpected(const vector<const OClass*>& types) {
  if (types.empty()) return "";
  string expected = "'"+types[0]->name+"'";
  for (size_t i = 1; i < types.size() - 1; ++i)
    expected += ", '"+types[i]->name+"'";
  expected += " or '"+types.back()->name+"'";
  return expected;
}

string s_getStringExpected(const vector<int>& es) {
  string expected;
  for (size_t i = 1; i < es.size() - 1; ++i)
    expected += ", "+std::to_string(es[i]);
  expected += " or "+std::to_string(es.back());
  return expected;
}

//============================================================================//
//::::::::::::::::::::::::::::::   OM Methods   :::::::::::::::::::::::::::::://
//============================================================================//

ORet OInstance::ommDelete(OArg rec, int, OArg*, Session *m) {
  if (!m->isStdNullObject(this)) {
    _ASSERT(_REF(rec) == this, m);
    if (rec->isRef() && !rec->isConst()) {
      m->deleteObject(this);
      _REF(rec) = m->nullObject();
    }
    else {
      _DECL_METHOD_NAME("object::delete");
      const string varType{rec->isRef() ? "reference" : "variable"};
      const string msg = rec->isConst() ?
          "cannot delete constant " + varType :
          "receiver must be a reference (unsafe deletion)";
      _THROW_BAD_USAGE(msg)
    }
  }
  return undefinedResult();
}

ORet OInstance::ommDuplicate(OArg, int, OArg*, Session *m) {
  _ENTER_NOCHK("object::duplicate");
  ORef dup = m->duplicateObject(this);
  _CHECK_EXCEPTION();
  return dup->disposableResult();
}

ORet OInstance::ommMoveObject(OArg rec, int argc, OArg* argv, Session *m) {
  _ENTER("object::move:", 1);
  _ASSERT(_REF(rec) == this, m);
  _ASSERT(!argv[0]->movable() || !argv[0]->isConst(), m);
  if (rec->isConst())
    _THROW_BAD_USAGE("cannot move constant value")
  else if (argv[0]->movable())
    class_->moveObject(this, _ARG(0), m);
  else
    class_->copyObject(this, _ARG(0), m);
  _CHECK_EXCEPTION();
  _ASSERT(_REF(rec) == this, m);
  return makeResult();
}

ORet OInstance::inheritedCopy(OArg rec, int argc, OArg* argv, Session *m) {
  _ENTER("object::inheritedCopy", 1);
//  _CHECK_ARG_TYPEOF(0, class_); TODO
  _ASSERT(!class_->isCoreClass(), m);
  ORet r = class_->getBaseClass()->copyObject(this, _ARG(0), m);
  _CHECK_EXCEPTION();
  return r;
}

#if 0 // TODO is it necessary?
ORet OInstance::defaultCopy(int argc, OArg* argv, Session *m) {
  _ENTER("object::defaultCopy", 1);
  _CHECK_ARG(0);

  ORef r = class_->defaultCopy(this, _ARG(0), m);
  _RETHROW_IF(!r, _ox_method_name);
  return r->makeResult();
}
#endif // 0

ORet OInstance::ommGetProp(OArg rec, int argc, OArg* argv, Session *m) {
  _ENTER("object::property:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);

  const string& propName = _ARG(0)->castConstRef<oxString>();
  const Property& prop = class_->getProperty(propName);

  const OClass* caller = m->getCallingClass();
  const auto propData = class_->resolveProperty(propName);
  const auto offset = std::get<2>(propData);
  if (offset == -1) {
    string msg = class_->isMutable() ? "variable" : "type '"+class_->name+"'";
    if (class_->isMutable()) {
      void* const pv = m->getCurrentMachineValue<void*>(M_RECEIVER_VAR_NAME);
      if (pv) {
        const string varName = *static_cast<string*>(pv);
        delete static_cast<string*>(pv);
        msg += " '"+varName+"'";
      }
    }
    _THROW_RTE("'"+propName+"' is not a property of "+msg);
  }
  // check member's accessibility
  const auto access = std::get<3>(propData);
  const OClass* const owner = std::get<0>(propData);
  if (!owner->isMemberAccessible(access, caller))
    _THROW_RTE("'"+propName+"' is not accessible in this context");
  // get property
  OVar& r = this->property(offset);
  if (r.isRef()) {
    void* const pv = m->getCurrentMachineValue<void*>(M_REF_RESULT);
    if (pv)
      *static_cast<OVar**>(pv) = &r;
  }
  return r;
}

ORet OInstance::ommTypeName(OArg, int, OArg*, Session* m) {
  _ENTER_NOCHK("object::typename");
  ORef r = BTI::getTypeClass<string>()->createObject(m);
  _RETHROW_HERE();
  *_PStr(r) = class_->name;
  return r->disposableResult();
}

ORet OInstance::ommIsNull(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::isNull", 0);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(r)->setValue(false);
  return r->disposableResult();
}

ORet OInstance::ommType(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::type:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, typeName, 0);
  OClass* type = m->classMgr->getClass(typeName);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = type ? class_->typeOf(type) : false;
  return r->disposableResult();
}

ORet OInstance::ommTypeOf(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::typeof:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = argv[0]->undefined() ? false :
      class_->typeOf(_ARG(0)->getClass());
  return r->disposableResult();
}

ORet OInstance::ommSame(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::same:", 1);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(r)->setValue(this == _ARG(0));
  return r->disposableResult();
}

ORet OInstance::ommSameType(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::sameType:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, typeName, 0);
  OClass* type = m->classMgr->getClass(typeName);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  *_PBOOL(r) = type ? (class_ == type) : false;
  return r->disposableResult();
}

ORet OInstance::ommGetClassObject(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::class");
  _DECLARE_CLASS_INSTALLER(ClassObject);
  OClass* classObjFactory = m->classMgr->getClass("class object");
  if (!classObjFactory) {
    classObjFactory = g_installClassObjectClass(m);
    if (!classObjFactory) {
      m->throwBadClass(
      /* method      */ _ox_method_name,
      /* description */ "could not create class 'class object'",
      /* context     */ "OInstance::ommGetClassObject",
      /* reason      */ "");
      return undefinedResult();
    }
  }
  ORef classObj = classObjFactory->createObject(m);
  _RETHROW_HERE();
  _ASSERT(classObj != nullptr, m);
  static_cast<oxClassObject*>(classObj)->classPointer = class_;
  return classObj->disposableResult();
}

ORet OInstance::ommGetClassId(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::typeid");
  ORef r = BTI::getTypeClass<int>()->createObject(m);
  _RETHROW_HERE();
  _PINT(r)->setValue(getClassId());
  return r->disposableResult();
}

ORet OInstance::ommGetRootClassId(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::ctypeid");
  ORef r = BTI::getTypeClass<int>()->createObject(m);
  _RETHROW_HERE();
  _PINT(r)->setValue(getRootClassId());
  return r->disposableResult();
}

ORet OInstance::ommGetObjectRef(OArg, int, OArg*, Session* m) {
  _ENTER_NOCHK("object::oref");
  ORef r = BTI::getTypeClass<void*>()->createObject(m);
  _RETHROW_HERE();
  _PPTR(r)->setValue(static_cast<void*>(this));
  return r->disposableResult();
}

ORet OInstance::ommNoMove(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(_REF(rec) == this, m);
  return OVar{this, rec->isConst() ? USG_CONST : USG_NONE};
}

ORet OInstance::ommNoAuto(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(_REF(rec) == this, m);
  rec->undispose();
  return OVar{this, rec->isConst() ? USG_CONST : USG_NONE};
}

ORet OInstance::ommConst(OArg rec, int, OArg*, Session* m) {
  return OVar{this, USG_CONST};
}

ORet OInstance::ommMove(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::move");
  _ASSERT(_REF(rec) == this, m);
  if (rec->isConst() || rec->isLiteral())
    _THROW_BAD_USAGE(rec->isConst() ? "cannot move constant value"
                                    : "use 'pass' to move a literal value")
  else if (rec->movable())
    _THROW_BAD_USAGE("the variable is already movable")
  else if (rec->disposable())
    _THROW_BAD_USAGE("cannot move disposable variable")
  else if (m->isStdNullObject(_REF(rec)))
    return m->nullObject()->makeResult();
  return movableResult();
}

ORet OInstance::ommRemit(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::remit");
  _ASSERT(_REF(rec) == this, m);
  if (rec->isConst() || rec->isLiteral())
    _THROW_BAD_USAGE(!rec->isLiteral() ? "cannot remit constant value"
                                       : "use 'pass' to remit a literal value")
  else if (rec->isRef())
    _THROW_BAD_USAGE("cannot remit reference")
  else if (m->isStdNullObject(_REF(rec)))
    return m->nullObject()->makeResult();
  OVar result{_REF(rec), USG_DISPOSABLE};
  _REF(rec) = nullptr;
  rec->undispose();
  rec->unmove();
  return result;
}

ORet OInstance::ommPassLiteral(OArg rec, int, OArg*, Session* m) {
  _ENTER_NOCHK("object::pass");
  _ASSERT(_REF(rec) == this, m);
  if (!rec->isLiteral())
    _THROW_BAD_USAGE("use 'remit' for non-literal values")
  *rec = OVar{};
  return OVar{this, USG_DISPOSABLE};
}

ORet OInstance::ommInitTemplateProp(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("object::property:type:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  _CHECK_ARG_TYPEID(1, CLSID_STRING);
  _UNPACK_CONST_REF(String, propName, 0);
  _UNPACK_CONST_REF(String, typeName, 1);

  const OClass* caller = m->getCallingClass();
  const auto propData = class_->resolveProperty(propName);
  const auto offset = std::get<2>(propData);
  if (offset == -1)
    _THROW_RTE("'"+propName+"' is not a property of class '"+class_->name+"'");
  // check member's accessibility
  const auto access = std::get<3>(propData);
  const OClass* const owner = std::get<0>(propData);
  if (!owner->isMemberAccessible(access, caller))
    _THROW_RTE("'"+propName+"' is not accessible in this context");
  const OClass* const propType = std::get<1>(propData);
  if (g_ref(access) || propType != nullptr) // not a template property
    _THROW_RTE_FULL("'"+propName+"' is not a template property");
  OVar* const varPtr = this->propertyPtr(offset);
  _ASSERT(varPtr != nullptr, m);
  if (g_const(access))
    varPtr->setConst();
  if (varPtr->objectRef)
    return *varPtr;
  // initialize it
  OClass* typeClass = m->classMgr->getClass(typeName);
  if (!typeClass)
    _THROW_RTE("'"+typeName+"' is not a type name");
  ORef o = typeClass->createObject(m);
  _RETHROW_HERE();
  varPtr->objectRef = o;
  return varPtr->isConst() ? OVar(o, varPtr->usage | USG_IGNORE_CONST) : *varPtr;
}

ORet OInstance::ommApply(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 1 ? "object::apply:" : "object::apply:args:");
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_REF(String, method, 0);
  return m->execMethod(method, 0, rec, argc - 1, argv + 1).first;
}

ORet OInstance::ommExtend(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::member:value:");
  _MUTABLE_REC();
  ORef result = m->extendObject(this, argc, argv);
  _CHECK_EXCEPTION();
  return result->makeResult();
}

ORet OInstance::ommExtendBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::extendBy:");
  _MUTABLE_REC();
  ORef result = m->extendObject(this, argc, argv);
  _CHECK_EXCEPTION();
  return result->makeResult();
}

ORet OInstance::ommCreateExtension(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("object::extend:");
  ORef result = m->createObjectExtension(std::move(*rec), argc, argv);
  _CHECK_EXCEPTION();
  return result->disposableResult();
}

ORet OInstance::ommConditional(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("object::optFor:other:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_BLOCK);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  bool condition;
  if (getRootClass() == BTI::getTypeClass<bool>()) {
    condition = _P(bool, this)->getValue();
  }
  else if (getRootClass() == BTI::getTypeProxyClass<bool>()) {
    condition = _PTypeProxy(bool, this)->getValue();
  }
  else {
    // the caller is required to check accessibility
    OClass* const caller = m->getCallingClass();
    // get cast information
    const auto castInfo = class_->getMethodAccessAddress("bool", caller);
    const bool hasCast = castInfo.first;
    if (hasCast) {
      // cast boolean value
      const OClass* boolType = BTI::getTypeClass<bool>();
      ORet rv = m->execMethodAddress(castInfo.second, "bool", 0, rec, 0, 0);
      if (m->exceptionPending() || !m->checkCastResult(rv, boolType, class_)) {
        m->rethrow("object::optFor:other");
        return undefinedResult();
      }
      condition = static_cast<oxType<bool>*>(REF(rv))->getValue();
      m->deleteObject(REF(rv)); // it is disposable
    }
    else
      _THROW_RTE("could not convert type '"+class_->name+"' "
                 "to boolean in conditional expression");
  }
  ORet result = m->execBlock(g_blockId(_ARG(condition ? 0 : 1)));
  _CHECK_EXCEPTION();
  return result;
}
