#include "otypeProxy.hpp"
#include "objectAutoCleaner.hpp"
#include "odev.h"
#include "ortmgr.hpp"
// #include "obasicTypeInfo.hpp"
#include "otype.hpp"
// #include "oexcept.hpp"
#include "fswitch.h"
#include "odefs.h"
#include "selectors.h"

_TEMPLATE_DEFINITION(OTypeProxy, oxType<t>);

using std::string;

//:::::::::::::::::::::::::::   Global Functions   ::::::::::::::::::::::::::://

ORef g_renderProxyObject(ORef o, bool noCache, Session* m) {
  SWITCH_TABLE(0, CLSID_ULONG_PROXY)
    DEFINE_CASE(0, CLSID_ULONG_PROXY)
    DEFINE_CASE(0, CLSID_LONG_PROXY)
    DEFINE_CASE(0, CLSID_UINT_PROXY)
    DEFINE_CASE(0, CLSID_INT_PROXY)
    DEFINE_CASE(0, CLSID_SHORT_PROXY)
    DEFINE_CASE(0, CLSID_CHAR_PROXY)
    DEFINE_CASE(0, CLSID_DBL_PROXY)
    DEFINE_CASE(0, CLSID_FLT_PROXY)
    DEFINE_CASE(0, CLSID_BOOL_PROXY)
    DEFINE_CASE(0, CLSID_PTR_PROXY)
  END_SWITCH_TABLE(0)

  const auto classId = o->getRootClassId();
  if (classId > CLSID_PTR_PROXY || classId < CLSID_ULONG_PROXY)
    return o;

  SWITCH(0, classId)
    CASE(0,CLSID_ULONG_PROXY): return _PTypeProxy(ulong, o)->render(noCache, m);
    CASE(0,CLSID_LONG_PROXY):  return _PTypeProxy(long,  o)->render(noCache, m);
    CASE(0,CLSID_UINT_PROXY):  return _PTypeProxy(uint,  o)->render(noCache, m);
    CASE(0,CLSID_INT_PROXY):   return _PTypeProxy(int,   o)->render(noCache, m);
    CASE(0,CLSID_SHORT_PROXY): return _PTypeProxy(short, o)->render(noCache, m);
    CASE(0,CLSID_CHAR_PROXY):  return _PTypeProxy(char,  o)->render(noCache, m);
    CASE(0,CLSID_DBL_PROXY):   return _PTypeProxy(double,o)->render(noCache, m);
    CASE(0,CLSID_FLT_PROXY):   return _PTypeProxy(float, o)->render(noCache, m);
    CASE(0,CLSID_BOOL_PROXY):  return _PTypeProxy(bool,  o)->render(noCache, m);
    CASE(0,CLSID_PTR_PROXY):   return _PTypeProxy(void*, o)->render(noCache, m);
  END_SWITCH(0)

  return nullptr;
}

ORef g_duplicateObject(OArg var, Session* m) {
  ORef rendered = g_renderProxyObject(var->objectRef, m);
  if (m->exceptionPending()) return nullptr;
  if (rendered != var->objectRef) return rendered;
  // here var->objectRef is not a proxy object
  ORef replica = var->objectRef->getClass()->createObject(m);
  if (m->exceptionPending()) return nullptr;
  replica->copyObject(var->objectRef, m);
  return replica;
}

//::::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://

#define DEFINE_OPERATION(_OPNAME, _OP)                             \
struct Operation_ ## _OPNAME {                                     \
  typedef ops::_OP Op;                                             \
  template<typename t>                                             \
  static inline ORet exec(oxType<t>& obj, OArg* argv, Session* m)  \
  { OVar rec{&obj}; return obj._OPNAME(&rec, 1, argv, m); }        \
  static inline std::string name() noexcept {return #_OPNAME;}     \
  template<typename t>                                             \
  static inline ORet invoke(OArg* argv, OArg a[], Session* m)      \
  { return _P(t, _ARG(0))->oxType<t>::_OPNAME(argv[0], 1, a, m); } \
};

DEFINE_OPERATION(add, Add);
DEFINE_OPERATION(sub, Sub);
DEFINE_OPERATION(mul, Mul);
DEFINE_OPERATION(div, Div);
DEFINE_OPERATION(mod, Mod);
DEFINE_OPERATION(exp, Exp);
DEFINE_OPERATION(band, Band);
DEFINE_OPERATION(bor,  Bor);
DEFINE_OPERATION(bxor, Bxor);
DEFINE_OPERATION(shr,  Shr);
DEFINE_OPERATION(shl,  Shl);
DEFINE_OPERATION(incBy, Incby);
DEFINE_OPERATION(decBy, Decby);
DEFINE_OPERATION(mulBy, Mulby);
DEFINE_OPERATION(divBy, Divby);
DEFINE_OPERATION(modBy, Modby);
DEFINE_OPERATION(expBy, Expby);
DEFINE_OPERATION(bandw, Bandby);
DEFINE_OPERATION(borw,  Borby);
DEFINE_OPERATION(bxorw, Bxorby);
DEFINE_OPERATION(shrw,  Shrby);
DEFINE_OPERATION(shlw,  Shlby);
DEFINE_OPERATION(eq, Eq);
DEFINE_OPERATION(lt, Lt);
DEFINE_OPERATION(gt, Gt);
DEFINE_OPERATION(le, Le);
DEFINE_OPERATION(ge, Ge);
DEFINE_OPERATION(ne, Ne);
DEFINE_OPERATION(_and, And);
DEFINE_OPERATION(_or,  Or);
DEFINE_OPERATION(_xor, Xor);

template<typename _OP>
struct MethodRunner {
  static ORet applyMethod(OArg* argv, OArg a[], Session* m) {
    SWITCH_TABLE(0, 0)
      DEFINE_CASE(0, CLSID_ULONG)
      DEFINE_CASE(0, CLSID_LONG)
      DEFINE_CASE(0, CLSID_UINT)
      DEFINE_CASE(0, CLSID_INT)
      DEFINE_CASE(0, CLSID_SHORT)
      DEFINE_CASE(0, CLSID_CHAR)
      DEFINE_CASE(0, CLSID_DBL)
      DEFINE_CASE(0, CLSID_FLOAT)
      DEFINE_CASE(0, CLSID_BOOL)
      DEFINE_CASE(0, CLSID_PTR)
    END_SWITCH_TABLE(0)
    ORet r;
    const auto typeId = _ARG(0)->getClassId();
    SWITCH(0, typeId - CLSID_ULONG)
    CASE(0, CLSID_ULONG): r = _OP::template invoke<ulong >(argv, a, m); BREAK(0);
    CASE(0, CLSID_LONG):  r = _OP::template invoke<long  >(argv, a, m); BREAK(0);
    CASE(0, CLSID_UINT):  r = _OP::template invoke<uint  >(argv, a, m); BREAK(0);
    CASE(0, CLSID_INT):   r = _OP::template invoke<int   >(argv, a, m); BREAK(0);
    CASE(0, CLSID_SHORT): r = _OP::template invoke<short >(argv, a, m); BREAK(0);
    CASE(0, CLSID_CHAR):  r = _OP::template invoke<char  >(argv, a, m); BREAK(0);
    CASE(0, CLSID_DBL):   r = _OP::template invoke<double>(argv, a, m); BREAK(0);
    CASE(0, CLSID_FLOAT): r = _OP::template invoke<float >(argv, a, m); BREAK(0);
    CASE(0, CLSID_BOOL):  r = _OP::template invoke<bool  >(argv, a, m); BREAK(0);
    CASE(0, CLSID_PTR):   r = _OP::template invoke<void* >(argv, a, m);
    END_SWITCH(0)
    return r;
  }
};

static bool _basicTypeArg(const string& methodName, OArg* argv, Session* m) {
  const auto typeId = _ARG(0)->getClassId();
  if (typeId < CLSID_ULONG || typeId > CLSID_PTR) {
    const std::string msg = "unexpected argument type; expected "
        "a basic type, found '"+_ARG(0)->getClassName()+"'";
    m->throwBadArg(methodName, msg);
    return false;
  }
  return true;
}

//::::::::::::::::::::::::::::::   OTypeProxy   :::::::::::::::::::::::::::::://

template<typename t, typename _OP>
static string _getProxyOperatorName() {
  return "proxy<"+BTI::getTypeClass<t>()->getClassName()+">::operator"+_OP::Op::name();
}

template<typename t>
static string _getProxyMethodName(const string& name) {
  return "proxy<"+BTI::getTypeClass<t>()->getClassName()+">::"+name;
}

template<typename t>
  template<typename _OP>
  ORet OTypeProxy<t>::genericSetOperation(OArg* argv, Session* m) {
    oxType<t> typeObj{OBasicTypeInfo::getTypeClass<t>()};
    ORet r = genericOperation<_OP>(typeObj, argv, m);
    if (r.undefined()) return r;
    setUpdateValue(typeObj.getValue());
    return this->makeResult();
  }

template<typename t>
  template<typename _OP>
  ORet OTypeProxy<t>::genericOperation(oxType<t>& typeObj, OArg* argv, Session* m) {
    typeObj.setValue(this->getValue());
    ORet r = _OP::exec(typeObj, argv, m);
    return r.undefined() ? ORet{m->rethrow(_getProxyOperatorName<t, _OP>())} : r;
  }

template<typename _OP>
ORet OTypeProxy<bool>::genericSetOperation(OArg* argv, Session* m) {
  oxType<bool> typeObj{OBasicTypeInfo::getTypeClass<bool>()};
  ORet r = genericOperation<_OP>(typeObj, argv, m);
  if (r.undefined()) return r;
  setUpdateValue(typeObj.getValue());
  return this->makeResult();
}

template<typename _OP>
ORet OTypeProxy<bool>::genericOperation(oxType<bool>& typeObj, OArg* argv, Session* m) {
  typeObj.setValue(this->getValue());
  ORet r = _OP::exec(typeObj, argv, m);
  return r.undefined() ? ORet{m->rethrow(_getProxyOperatorName<bool, _OP>())} : r;
}

//::::::::::::::::::::::::::::   Std Operations   :::::::::::::::::::::::::::://

template<typename t>
ORet OTypeProxy<t>::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::ommValue(rec, argc, argv, m);
  if (r.undefined()) return ORet(m->rethrow(_getProxyMethodName<t>(S_value)));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::ommGetValue(OArg rec, int argc, OArg* argv, Session* m) {
  ORef r = BTI::getTypeClass<t>()->createObject(m);
  _RETHROW(_getProxyMethodName<t>("value"));
  _P(t, r)->setValue(this->getValue());
  return r->disposableResult();
}

template<typename t>
ORet OTypeProxy<t>::ommCopy(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::ommValue(rec, argc, argv, m);
  if (r.undefined()) return ORet(m->rethrow(_getProxyMethodName<t>(S_copy)));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::add(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_add>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::sub(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_sub>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::mul(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_mul>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::div(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_div>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::mod(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_mod>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::exp(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_exp>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::band(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_band>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::bor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_bor>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::bxor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_bxor>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::shr(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_shr>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::shl(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_shl>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::incBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_incBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::decBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_decBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::mulBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_mulBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::divBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_divBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::modBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_modBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::expBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_expBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::bandw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_bandw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::borw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_borw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::bxorw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_bxorw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::shrw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_shrw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::shlw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_shlw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::eq(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_eq>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::lt(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_lt>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::gt(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_gt>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::le(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_le>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::ge(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_ge>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::ne(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_ne>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::_and(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__and>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::_or(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__or>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::_xor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__xor>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::inc(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::inc(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<t>("operator++"));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::dec(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::dec(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<t>("operator--"));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::pinc(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::pinc(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<t>("operator++(post)"));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::pdec(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<t>::pdec(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<t>("operator--(post)"));
  updateValue();
  return r;
}

template<typename t>
ORet OTypeProxy<t>::addr(OArg rec, int, OArg*, Session* m) {
  // TODO ! check if nullptr works here
  return oxType<void*>(nullptr).createWithValue(getElemPtr(), m)
      ->disposableResult();
}

//:::::::::::::::::::::::::   Std Operations: bool   ::::::::::::::::::::::::://

ORet OTypeProxy<bool>::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<bool>::ommValue(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<bool>(S_value));
  updateValue();
  return r;
}

ORet OTypeProxy<bool>::ommGetValue(OArg rec, int argc, OArg* argv, Session* m) {
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW(_getProxyMethodName<bool>("value"));
  _P(bool, r)->setValue(this->getValue());
  return r->disposableResult();
}

ORet OTypeProxy<bool>::ommCopy(OArg rec, int argc, OArg* argv, Session* m) {
  ORet r = oxType<bool>::ommValue(rec, argc, argv, m);
  _RETHROW_IF(r.undefined(), _getProxyMethodName<bool>(S_copy));
  updateValue();
  return r;
}

ORet OTypeProxy<bool>::add(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_add>(argv, m);
}

ORet OTypeProxy<bool>::sub(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_sub>(argv, m);
}

ORet OTypeProxy<bool>::mul(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_mul>(argv, m);
}

ORet OTypeProxy<bool>::div(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_div>(argv, m);
}

ORet OTypeProxy<bool>::mod(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_mod>(argv, m);
}

ORet OTypeProxy<bool>::exp(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_exp>(argv, m);
}

ORet OTypeProxy<bool>::band(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_band>(argv, m);
}

ORet OTypeProxy<bool>::bor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_bor>(argv, m);
}

ORet OTypeProxy<bool>::bxor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_bxor>(argv, m);
}

ORet OTypeProxy<bool>::shr(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_shr>(argv, m);
}

ORet OTypeProxy<bool>::shl(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_shl>(argv, m);
}

ORet OTypeProxy<bool>::incBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_incBy>(argv, m);
}

ORet OTypeProxy<bool>::decBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_decBy>(argv, m);
}

ORet OTypeProxy<bool>::mulBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_mulBy>(argv, m);
}

ORet OTypeProxy<bool>::divBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_divBy>(argv, m);
}

ORet OTypeProxy<bool>::modBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_modBy>(argv, m);
}

ORet OTypeProxy<bool>::bandw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_bandw>(argv, m);
}

ORet OTypeProxy<bool>::borw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_borw>(argv, m);
}

ORet OTypeProxy<bool>::bxorw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_bxorw>(argv, m);
}

ORet OTypeProxy<bool>::shrw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_shrw>(argv, m);
}

ORet OTypeProxy<bool>::shlw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericSetOperation<Operation_shlw>(argv, m);
}

ORet OTypeProxy<bool>::eq(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_eq>(argv, m);
}

ORet OTypeProxy<bool>::lt(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_lt>(argv, m);
}

ORet OTypeProxy<bool>::gt(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_gt>(argv, m);
}

ORet OTypeProxy<bool>::le(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_le>(argv, m);
}

ORet OTypeProxy<bool>::ge(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_ge>(argv, m);
}

ORet OTypeProxy<bool>::ne(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation_ne>(argv, m);
}

ORet OTypeProxy<bool>::_and(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__and>(argv, m);
}

ORet OTypeProxy<bool>::_or(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__or>(argv, m);
}

ORet OTypeProxy<bool>::_xor(OArg rec, int argc, OArg* argv, Session* m) {
  return genericOperation<Operation__xor>(argv, m);
}

//::::::::::::::::::::::::::   Reverse Operations   :::::::::::::::::::::::::://

template<typename t>
  template<typename _OP>
  ORet OTypeProxy<t>::genericReverseOperation(OArg* argv, Session* m) {
    static const std::string methodName = _getProxyMethodName<t>(_OP::name());
    if (!_basicTypeArg(methodName, argv, m))
      return OVar::undefinedResult();
    oxType<t> val{OBasicTypeInfo::getTypeClass<t>()};
    val.setValue(this->getValue());
    OVar varThis{&val};
    OArg a[]{&varThis};
    ORet r = MethodRunner<_OP>::applyMethod(argv, a, m);
    _RETHROW(methodName);
    return r;
  }

template<typename t>
ORet OTypeProxy<t>::rsub(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_sub>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rdiv(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_div>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rmod(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_mod>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rexp(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_exp>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rshr(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shr>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rshl(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shl>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rincBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_incBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rdecBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_decBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rmulBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_mulBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rdivBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_divBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rmodBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_modBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rexpBy(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_expBy>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rbandw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_bandw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rborw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_borw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rbxorw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_bxorw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rshrw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shrw>(argv, m);
}

template<typename t>
ORet OTypeProxy<t>::rshlw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shlw>(argv, m);
}

//:::::::::::::::::::::::   Reverse Operations: bool   ::::::::::::::::::::::://

template<typename _OP>
ORet OTypeProxy<bool>::genericReverseOperation(OArg* argv, Session* m) {
  static const std::string methodName = _getProxyMethodName<bool>(_OP::name());
  if (!_basicTypeArg(methodName, argv, m))
    return undefinedResult();
  oxType<bool> val{OBasicTypeInfo::getTypeClass<bool>()};
  val.setValue(this->getValue());
  OVar varThis{&val};
  OArg a[]{&varThis};
  ORet r = MethodRunner<_OP>::applyMethod(argv, a, m);
  _RETHROW(methodName);
  return r;
}

ORet OTypeProxy<bool>::rmod(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_mod>(argv, m);
}

ORet OTypeProxy<bool>::rexp(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_exp>(argv, m);
}

ORet OTypeProxy<bool>::rshr(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shr>(argv, m);
}

ORet OTypeProxy<bool>::rshl(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shl>(argv, m);
}

ORet OTypeProxy<bool>::rbandw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_bandw>(argv, m);
}

ORet OTypeProxy<bool>::rborw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_borw>(argv, m);
}

ORet OTypeProxy<bool>::rbxorw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_bxorw>(argv, m);
}

ORet OTypeProxy<bool>::rshrw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shrw>(argv, m);
}

ORet OTypeProxy<bool>::rshlw(OArg rec, int argc, OArg* argv, Session* m) {
  return genericReverseOperation<Operation_shlw>(argv, m);
}

//:::::::::::::::::::::::::::::   Method Table   ::::::::::::::::::::::::::::://

#define METHOD_TABLE_FOR_OTypeProxy(t) \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(OTypeProxy<t>)     \
  PUBLIC (OTypeProxy<t>::ommValue,    S_value)      \
  PUBLIC (OTypeProxy<t>::ommValue,    S_init)       \
  PUBLIC (OTypeProxy<t>::ommGetValue, "value")      \
  PUBLIC (OTypeProxy<t>::ommCopy,     S_copy)       \
  PUBLIC (OTypeProxy<t>::add,         S_add)        \
  PUBLIC (OTypeProxy<t>::sub,         S_sub)        \
  PUBLIC (OTypeProxy<t>::mul,         S_mul)        \
  PUBLIC (OTypeProxy<t>::div,         S_div)        \
  PUBLIC (OTypeProxy<t>::mod,         S_mod)        \
  PUBLIC (OTypeProxy<t>::exp,         S_exp)        \
  PUBLIC (OTypeProxy<t>::band,        S_bitand)     \
  PUBLIC (OTypeProxy<t>::bor,         S_bitor)      \
  PUBLIC (OTypeProxy<t>::bxor,        S_bitxor)     \
  PUBLIC (OTypeProxy<t>::shr,         S_shiftr)     \
  PUBLIC (OTypeProxy<t>::shl,         S_shiftl)     \
  PUBLIC (OTypeProxy<t>::incBy,       S_incBy)      \
  PUBLIC (OTypeProxy<t>::decBy,       S_decBy)      \
  PUBLIC (OTypeProxy<t>::mulBy,       S_mulBy)      \
  PUBLIC (OTypeProxy<t>::divBy,       S_divBy)      \
  PUBLIC (OTypeProxy<t>::modBy,       S_modBy)      \
  PUBLIC (OTypeProxy<t>::expBy,       S_expBy)      \
  PUBLIC (OTypeProxy<t>::bandw,       S_bitandBy)   \
  PUBLIC (OTypeProxy<t>::borw,        S_bitorBy)    \
  PUBLIC (OTypeProxy<t>::bxorw,       S_bitxorBy)   \
  PUBLIC (OTypeProxy<t>::shrw,        S_shiftrBy)   \
  PUBLIC (OTypeProxy<t>::shlw,        S_shiftlBy)   \
  PUBLIC (OTypeProxy<t>::eq,          S_eq)         \
  PUBLIC (OTypeProxy<t>::lt,          S_lt)         \
  PUBLIC (OTypeProxy<t>::gt,          S_gt)         \
  PUBLIC (OTypeProxy<t>::le,          S_le)         \
  PUBLIC (OTypeProxy<t>::ge,          S_ge)         \
  PUBLIC (OTypeProxy<t>::ne,          S_ne)         \
  PUBLIC (OTypeProxy<t>::_and,        S__and)       \
  PUBLIC (OTypeProxy<t>::_or,         S__or)        \
  PUBLIC (OTypeProxy<t>::_xor,        S__xor)       \
  PUBLIC (OTypeProxy<t>::inc,         S_inc)        \
  PUBLIC (OTypeProxy<t>::dec,         S_dec)        \
  PUBLIC (OTypeProxy<t>::pinc,        S_postinc)    \
  PUBLIC (OTypeProxy<t>::pdec,        S_postdec)    \
  PUBLIC (OTypeProxy<t>::addr,        S_address)    \
  PUBLIC (OTypeProxy<t>::rsub,        S_rsub)       \
  PUBLIC (OTypeProxy<t>::rdiv,        S_rdiv)       \
  PUBLIC (OTypeProxy<t>::rmod,        S_rmod)       \
  PUBLIC (OTypeProxy<t>::rexp,        S_rexp)       \
  PUBLIC (OTypeProxy<t>::rshr,        S_rshiftr)    \
  PUBLIC (OTypeProxy<t>::rshl,        S_rshiftl)    \
  PUBLIC (OTypeProxy<t>::rincBy,      S_rincBy)     \
  PUBLIC (OTypeProxy<t>::rdecBy,      S_rdecBy)     \
  PUBLIC (OTypeProxy<t>::rmulBy,      S_rmulBy)     \
  PUBLIC (OTypeProxy<t>::rdivBy,      S_rdivBy)     \
  PUBLIC (OTypeProxy<t>::rmodBy,      S_rmodBy)     \
  PUBLIC (OTypeProxy<t>::rexpBy,      S_rexpBy)     \
  PUBLIC (OTypeProxy<t>::rbandw,      S_rbitandBy)  \
  PUBLIC (OTypeProxy<t>::rborw,       S_rbitorBy)   \
  PUBLIC (OTypeProxy<t>::rbxorw,      S_rbitxorBy)  \
  PUBLIC (OTypeProxy<t>::rshrw,       S_rshiftrBy)  \
  PUBLIC (OTypeProxy<t>::rshlw,       S_rshiftlBy)  \
_END_METHOD_TABLE()

//::::::::::::::::::::::::::::  OTypeProxy_Class  :::::::::::::::::::::::::::://

template<typename t>
OTypeProxy_Class<t>::OTypeProxy_Class(Session* m, const string& name) :
    oxType_Class<t>(m, name) {}

template<typename t>
OTypeProxy_Class<t>::OTypeProxy_Class(Session* m,
    const string& className, int baseClassId) : oxType_Class<t>(m, className) {}

template<typename t>
OClass* OTypeProxy_Class<t>::createClass(Session* m,
    const string& className) const {
  m->throwBadClass("OTypeProxy<>::createClass",
  /* description */"could not create class '"+className+"'",
  /* context     */"class extension in script",
  /* reason      */"the base class '"+this->name+"' is private",
  /* file, line  */__FILE__, __LINE__);
  return nullptr;
}

template<typename t>
OClass* OTypeProxy_Class<t>::createMutableClass(Session* m,
    const string& className) const {
  return BTI::getTypeClass<t>()->createMutableClass(m, className);
}

template<typename t>
ORef OTypeProxy_Class<t>::copyCore(ORef self, const_ORef r, Session* m) const {
  auto* lhs = static_cast<OTypeProxy<t>*>(self);
  const auto* rhs = static_cast<const OTypeProxy<t>*>(r);
  if (lhs->initialized()) {
    if (rhs->initialized()) {
      lhs->copy(rhs);
      lhs->updateValue();
    }
    else {
      // you cannot uninitialize a reference (a proxy is essentially a ref)
      const std::string methodName = "OTypeProxy_Class<"+
          BTI::getTypeClass<t>()->name+">::copyCore";
      m->throwRTE(methodName, "attempt to uninitialize a proxy type");
    }
  }
  else if (rhs->initialized()) {
    lhs->copy(rhs);
    lhs->setElemPtr(rhs->proxiedData_);
    // both lhs and rhs point at the same data; there is nothing to update
  }
  // if !lhs->initialized() and !rhs->initialized() there is nothing to do
  return self;
}

template<typename t>
ORef OTypeProxy_Class<t>::moveCore(ORef self, ORef r) const {
  auto* lhs = static_cast<OTypeProxy<t>*>(self);
  auto* rhs = static_cast<OTypeProxy<t>*>(r);
  if (lhs->initialized()) {
    if (rhs->initialized()) {
      lhs->swapValue(rhs);
      lhs->updateValue();
      // you cannot update the rhs - it's a useless ref now!
    }
    else {
      // you cannot uninitialize a reference (a proxy is essentially a ref)
      return nullptr;
    }
  }
  else if (rhs->initialized()) {
    lhs->swapValue(rhs);
    lhs->setElemPtr(rhs->proxiedData_);
    lhs->updateValue(); // not necessary
    // you cannot update the rhs - it's a useless ref now!
  }
  // if !lhs->initialized() and !rhs->initialized() there is nothing to do
  return self;
}

METHOD_TABLE_FOR_OTypeProxy(unsigned long);
METHOD_TABLE_FOR_OTypeProxy(long);
METHOD_TABLE_FOR_OTypeProxy(unsigned);
METHOD_TABLE_FOR_OTypeProxy(int);
METHOD_TABLE_FOR_OTypeProxy(short);
METHOD_TABLE_FOR_OTypeProxy(char);
METHOD_TABLE_FOR_OTypeProxy(double);
METHOD_TABLE_FOR_OTypeProxy(float);
METHOD_TABLE_FOR_OTypeProxy(void*);

_BEGIN_METHOD_TABLE(OTypeProxy<bool>, oxType<bool>)
  PUBLIC (OTypeProxy<bool>::ommValue,    S_value)
  PUBLIC (OTypeProxy<bool>::ommValue,    S_init)
  PUBLIC (OTypeProxy<bool>::ommGetValue, "value")
  PUBLIC (OTypeProxy<bool>::ommCopy,     S_copy)
  PUBLIC (OTypeProxy<bool>::add,         S_add)
  PUBLIC (OTypeProxy<bool>::sub,         S_sub)
  PUBLIC (OTypeProxy<bool>::mul,         S_mul)
  PUBLIC (OTypeProxy<bool>::div,         S_div)
  PUBLIC (OTypeProxy<bool>::mod,         S_mod)
  PUBLIC (OTypeProxy<bool>::exp,         S_exp)
  PUBLIC (OTypeProxy<bool>::band,        S_bitand)
  PUBLIC (OTypeProxy<bool>::bor,         S_bitor)
  PUBLIC (OTypeProxy<bool>::bxor,        S_bitxor)
  PUBLIC (OTypeProxy<bool>::shr,         S_shiftr)
  PUBLIC (OTypeProxy<bool>::shl,         S_shiftl)
  PUBLIC (OTypeProxy<bool>::rshr,        S_rshiftr)
  PUBLIC (OTypeProxy<bool>::rshl,        S_rshiftl)
  PUBLIC (OTypeProxy<bool>::incBy,       S_incBy)
  PUBLIC (OTypeProxy<bool>::decBy,       S_decBy)
  PUBLIC (OTypeProxy<bool>::mulBy,       S_mulBy)
  PUBLIC (OTypeProxy<bool>::divBy,       S_divBy)
  PUBLIC (OTypeProxy<bool>::modBy,       S_modBy)
  /**
   * There is no @c expBy operation for booleans.
   * See method table for oxType<bool>
   */
  PUBLIC (OTypeProxy<bool>::bandw,       S_bitandBy)
  PUBLIC (OTypeProxy<bool>::borw,        S_bitorBy)
  PUBLIC (OTypeProxy<bool>::bxorw,       S_bitxorBy)
  PUBLIC (OTypeProxy<bool>::shrw,        S_shiftrBy)
  PUBLIC (OTypeProxy<bool>::shlw,        S_shiftlBy)
  PUBLIC (OTypeProxy<bool>::eq,          S_eq)
  PUBLIC (OTypeProxy<bool>::lt,          S_lt)
  PUBLIC (OTypeProxy<bool>::gt,          S_gt)
  PUBLIC (OTypeProxy<bool>::le,          S_le)
  PUBLIC (OTypeProxy<bool>::ge,          S_ge)
  PUBLIC (OTypeProxy<bool>::ne,          S_ne)
  PUBLIC (OTypeProxy<bool>::_and,        S__and)
  PUBLIC (OTypeProxy<bool>::_or,         S__or)
  PUBLIC (OTypeProxy<bool>::_xor,        S__xor)
  PUBLIC (OTypeProxy<bool>::rmod,        S_rmod)
  PUBLIC (OTypeProxy<bool>::rexp,        S_rexp)
  PUBLIC (OTypeProxy<bool>::rbandw,      S_rbitandBy)
  PUBLIC (OTypeProxy<bool>::rborw,       S_rbitorBy)
  PUBLIC (OTypeProxy<bool>::rbxorw,      S_rbitxorBy)
  PUBLIC (OTypeProxy<bool>::rshrw,       S_rshiftrBy)
  PUBLIC (OTypeProxy<bool>::rshlw,       S_rshiftlBy)
_END_METHOD_TABLE()

//::::::::::::::::::::::::::::  Class Installer  ::::::::::::::::::::::::::::://

bool g_installTypeProxyClasses(Session* m) {
   OClass* cls;
   extern OClass* _OXType_class[];

   cls = new OTypeProxy_Class<unsigned long>(m, T_ulong_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_ULONG_PROXY);
   _OXType_class[CLSID_ULONG_PROXY] = cls;

   cls = new OTypeProxy_Class<long>(m, T_long_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_LONG_PROXY);
   _OXType_class[CLSID_LONG_PROXY] = cls;

   cls = new OTypeProxy_Class<unsigned>(m, T_uint_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_UINT_PROXY);
   _OXType_class[CLSID_UINT_PROXY] = cls;

   cls = new OTypeProxy_Class<int>(m, T_int_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_INT_PROXY);
   _OXType_class[CLSID_INT_PROXY] = cls;

   cls = new OTypeProxy_Class<short>(m, T_short_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_SHORT_PROXY);
   _OXType_class[CLSID_SHORT_PROXY] = cls;

   cls = new OTypeProxy_Class<char>(m, T_char_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_CHAR_PROXY);
   _OXType_class[CLSID_CHAR_PROXY] = cls;

   cls = new OTypeProxy_Class<double>(m, T_double_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_DBL_PROXY);
   _OXType_class[CLSID_DBL_PROXY] = cls;

   cls = new OTypeProxy_Class<float>(m, T_float_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_FLT_PROXY);
   _OXType_class[CLSID_FLT_PROXY] = cls;

   cls = new OTypeProxy_Class<bool>(m, T_bool_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_BOOL_PROXY);
   _OXType_class[CLSID_BOOL_PROXY] = cls;

   cls = new OTypeProxy_Class<void*>(m, T_pointer_type_proxy);
   if (!cls) return false;
   cls->finalize(m);
   ASSERT(cls->getClassId() == CLSID_PTR_PROXY);
   _OXType_class[CLSID_PTR_PROXY] = cls;

   return true;
}
