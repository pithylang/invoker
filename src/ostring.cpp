#include "object.hpp"
#include <stdexcept>
#include "odev.h"
#include "ostring.hpp"
#include "otypes.h"
#include "otype.hpp"
#include "ortmgr.hpp"
#include "ovector.hpp"
#include "otypeProxy.hpp"
#include "ostriter.hpp"
#include "oveciter.hpp"
#include "orange.hpp"
#include "oalgo.hpp"
#include "objectAutoCleaner.hpp"

#define _DEFINE_FIND_METHOD(_FIND, _METHOD)               \
  struct _FIND : om::GenericFind {                        \
    _FIND(const std::string& s) : GenericFind{ s } {}     \
    virtual size_type operator()() const override {       \
      if (pos == std::string::npos)                       \
        return p ? s._METHOD(*p) : s._METHOD(ch);         \
      return p ? s._METHOD(*p, pos) : s._METHOD(ch, pos); \
    }                                                     \
  }

_BEGIN_METHOD_TABLE(_ox(String), OInstance)
  PUBLIC (_ox(String)::ommAll           , "all")
  PUBLIC (_ox(String)::ommRAll          , "rall")
  PUBLIC (_ox(String)::ommCAll          , "call")
  PUBLIC (_ox(String)::ommCRAll         , "crall")
  PUBLIC (_ox(String)::method_print     , "print")
  PUBLIC (_ox(String)::method_println   , "println")
  PUBLIC (_ox(String)::ommValue         , S_value)
  PUBLIC (_ox(String)::ommValueFromSize , "value:from:size:")
  PUBLIC (_ox(String)::ommSizeChar      , "size:char:")
  PUBLIC (_ox(String)::ommSizeChar      , "size:")
  PUBLIC (_ox(String)::ommBegin         , "begin")
  PUBLIC (_ox(String)::ommRBegin        , "rbegin")
  PUBLIC (_ox(String)::ommCBegin        , "cbegin")
  PUBLIC (_ox(String)::ommCRBegin       , "crbegin")
  PUBLIC (_ox(String)::ommEnd           , "end")
  PUBLIC (_ox(String)::ommREnd          , "rend")
  PUBLIC (_ox(String)::ommCEnd          , "cend")
  PUBLIC (_ox(String)::ommCREnd         , "crend")
  PUBLIC (_ox(String)::ommSize          , "size")
  PUBLIC (_ox(String)::ommSize          , "length")
  PUBLIC (_ox(String)::ommMaxSize       , "maxSize")
  PUBLIC (_ox(String)::ommResize        , "resize:")
  PUBLIC (_ox(String)::ommResize        , "resize:char:")
  PUBLIC (_ox(String)::ommCapacity      , "capacity")
  PUBLIC (_ox(String)::ommReserve       , "reserve:")
  PUBLIC (_ox(String)::ommClear         , "clear")
  PUBLIC (_ox(String)::ommEmpty         , "empty")
  PUBLIC (_ox(String)::ommComponent     , "component:")
  PUBLIC (_ox(String)::ommAt            , "at:")
  PUBLIC (_ox(String)::ommPushBack      , "pushBack:")
  PUBLIC (_ox(String)::ommPopBack       , "popBack")
  PUBLIC (_ox(String)::ommAppend        , "append:")
  PUBLIC (_ox(String)::ommAppend        , "append:char:")
  PUBLIC (_ox(String)::ommAppend        , "append:size:")
  PUBLIC (_ox(String)::ommAppend        , "append:from:size:")
  PUBLIC (_ox(String)::ommAssign        , "assign:")
  PUBLIC (_ox(String)::ommAssign        , "assign:char:")
  PUBLIC (_ox(String)::ommAssign        , "assign:from:size:")
  PUBLIC (_ox(String)::ommAssign        , "assign:size:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:char:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:size:char:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:string:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:string:size:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:string:from:size:")
  PUBLIC (_ox(String)::ommInsert        , "insertAt:begin:end:")
  PUBLIC (_ox(String)::ommErase         , "erase")
  PUBLIC (_ox(String)::ommErase         , "eraseAt:")
  PUBLIC (_ox(String)::ommErase         , "eraseAt:end:")
  PUBLIC (_ox(String)::ommErase         , "eraseAt:size:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:end:string:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:end:from:to:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:end:size:char:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:size:string:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:n:size:char:")
  PUBLIC (_ox(String)::ommReplace       , "replaceAt:size:string:from:size:")
  PUBLIC (_ox(String)::ommSwap          , "swap:")
  PUBLIC (_ox(String)::ommCStr          , "c_str")
  PUBLIC (_ox(String)::ommData          , "data")
  PUBLIC (_ox(String)::ommNpos          , "npos")
  PUBLIC (_ox(String)::ommFind          , "find:")
  PUBLIC (_ox(String)::ommFind          , "find:from:")
  PUBLIC (_ox(String)::ommRFind         , "rfind:")
  PUBLIC (_ox(String)::ommRFind         , "rfind:from:")
  PUBLIC (_ox(String)::ommFindFirst     , "findFirstOf:")
  PUBLIC (_ox(String)::ommFindFirst     , "findFirstOf:from:")
  PUBLIC (_ox(String)::ommFindLast      , "findLastOf:")
  PUBLIC (_ox(String)::ommFindLast      , "findLastOf:end:")
  PUBLIC (_ox(String)::ommFindFirstNotOf, "findFirstNotOf:")
  PUBLIC (_ox(String)::ommFindFirstNotOf, "findFirstNotOf:from:")
  PUBLIC (_ox(String)::ommFindLastNotOf , "findLastNotOf:")
  PUBLIC (_ox(String)::ommFindLastNotOf , "findLastNotOf:end:")
  PUBLIC (_ox(String)::ommStartsWith    , "startsWith:")
  PUBLIC (_ox(String)::ommEndsWith      , "endsWith:")
  PUBLIC (_ox(String)::ommSubstring     , "substring")
  PUBLIC (_ox(String)::ommSubstring     , "substring:")
  PUBLIC (_ox(String)::ommSubstring     , "substring:size:")
  PUBLIC (_ox(String)::ommCompare       , "compare:")
  PUBLIC (_ox(String)::ommCompare       , "compare:size:string:")
  PUBLIC (_ox(String)::ommCompare       , "compare:size:string:from:size:")
  PUBLIC (_ox(String)::method_incBy     , S_incBy)
  PUBLIC (_ox(String)::method_add       , S_add)
  PUBLIC (_ox(String)::method_eq        , S_eq)
  PUBLIC (_ox(String)::method_ne        , S_ne)
  PUBLIC (_ox(String)::method_lt        , S_lt)
  PUBLIC (_ox(String)::method_gt        , S_gt)
  PUBLIC (_ox(String)::method_le        , S_le)
  PUBLIC (_ox(String)::method_ge        , S_ge)
_END_METHOD_TABLE()

_BEGIN_METHOD_TABLE(oxWString, OInstance)
_END_METHOD_TABLE()

using std::string, std::vector;

template<typename t>
  static OClass* s_class() noexcept { return BTI::getTypeClass<t>(); }

template<>
  OClass* s_class<StringIterator>() noexcept {
    extern OClass* _OXType_class[];
    return _OXType_class[CLSID_STRITER];
  }

template<>
  OClass* s_class<ORange>() noexcept {
    extern OClass* _OXType_class[];
    return _OXType_class[CLSID_RANGE];
  }

//============================================================================//
//:::::::::::::::::::::::::::::::   oxString   ::::::::::::::::::::::::::::::://
//============================================================================//

ORet _ox(String)::method_println(OArg, int argc, OArg*, Session* m) {
  m->stringOutput(*this+"\n"); // printf("%s\n", c_str());
  return makeResult();
}

ORet _ox(String)::method_print(OArg, int argc, OArg*, Session* m) {
  m->stringOutput(*this); //  printf("%s", c_str());
  return makeResult();
}

// basic_string()                                          [0]  [NS       ]
//
// basic_string(size_type count, CharT ch,
//              const Allocator& alloc = Allocator())      [2]  [size_type]
// basic_string(const basic_string& other, size_type pos,
//              const Allocator& alloc = Allocator())      [2]  [string   ]
// basic_string(const basic_string& other,
//              size_type pos, size_type count,
//              const Allocator& alloc = Allocator())      [3]  [string   ]
// template< class InputIt >
//   basic_string(InputIt first, InputIt last,
//                const Allocator& alloc = Allocator())    [2]  [iterator ]
// basic_string(const basic_string& other)                 [1]  [string   ]
// basic_string(basic_string&& other) noexcept             [1]  [string   ]
ORet _ox(String)::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::" S_value, 1);
  _MUTABLE_REC();
  string& lhs = castRef<_ox(String)>();
  if (OM_ARG_TYPEID(0, CLSID_STRING)) {
    if (argv[0]->disposable())
      lhs = std::move(_ARG(0)->castRef<_ox(String)>());
    else
      lhs = _ARG(0)->castRef<_ox(String)>();
  }
  else if (OM_ARG_TYPEID(0, CLSID_OBJVECTOR)) {
    const auto& v = _ARG(0)->castRef<ObjectVector>();
    _CHECK_ARG_SIZE(v.size(), (vector<int>{2, 3}), 0)
    OVariable vars[2]{OVariable{v[0]}, OVariable{v[1]}};
    OArg argv[2]{vars, vars + 1};
    if (v.size() == 2) {
      if (BTI::isBasicType(v[0])) {
        size_type count = g_castValue<size_type>(argv, m);
        _CHECK_EXCEPTION();
        if (!OM_ARG_TYPEID(1, CLSID_CHAR)) {
          OM_CHECK_ARG_ELEM_TYPEID(v[1], CLSID_CHAR, 0, 1);
        }
        lhs = string(count, g_typeValue<char>(v[1]));
      }
      else if (OM_ARG_TYPEID(0, CLSID_STRING)) {
        const string& other = v[0]->castConstRef<_ox(String)>();
        size_type pos = g_castValue<size_type>(v[1], m);
        _CHECK_EXCEPTION();
        lhs = string(other, pos);
      }
      else if (OM_ARG_TYPEID(0, CLSID_STRITER)) {
        if (!OM_ARG_TYPEID(1, CLSID_STRITER)) {
          OM_CHECK_ARG_ELEM_TYPEID(v[1], CLSID_STRITER, 0, 1);
        }
        _O(StringIterator)* it1 = static_cast<_O(StringIterator)*>(_ARG(0));
        _O(StringIterator)* it2 = static_cast<_O(StringIterator)*>(_ARG(1));
        if (it1->isReverse() != it2->isReverse())
          _THROW_RTE("iterator type mismatch")
        if (it1->isReverse()) {
          auto rbegin = std::make_reverse_iterator(*it1);
          auto rend   = std::make_reverse_iterator(*it2);
          lhs = string{rbegin, rend};
        }
        else
          lhs = string{*it1, *it2};
      }
      else {
        OM_CHECK_ARG_ELEM_TYPEID(v[0], (vector<int>{CLSID_ULONG,
            CLSID_STRING, CLSID_STRITER}), -1, 1);
      }
    }
    else /* v.size() == 3 */ {
      _CHECK_ARG_TYPEID(0, CLSID_STRING);
      const string& other = v[0]->castConstRef<_ox(String)>();
      size_type pos = g_castValue<size_type>(v[1], m);
      _CHECK_EXCEPTION();
      size_type count = g_castValue<size_type>(v[2], m);
      _CHECK_EXCEPTION();
      lhs = string(other, pos, count);
    }
  }
  else if (OM_ARG_TYPEID(0, CLSID_CHARVECTOR)) {
    const vector<char>& v = _ARG(0)->castRef<OVector<char>>();
    lhs = string(v.begin(), v.end());
  }
  else
    _CHECK_ARG_TYPEIDS(0, (vector<int>{CLSID_STRING, CLSID_OBJVECTOR}));
  return makeResult();
}

// basic_string(const basic_string& str, size_type pos, size_type n = npos);
ORet _ox(String)::ommValueFromSize(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::value:from:size:", 3);
  _MUTABLE_REC();
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  const size_type pos = g_castValue<size_type>(argv + 1, m);
  _CHECK_EXCEPTION();
  const size_type n = g_castValue<size_type>(argv + 2, m);
  _CHECK_EXCEPTION();
  *static_cast<string*>(this) = string{_ARG(0)->castConstRef<_ox(String)>(), pos, n};
  return makeResult();
}

#if 0 // NS
// basic_string(const basic_string& str, size_type pos, size_type n, const Alloc& a);
// basic_string(const CharT* s, size_type n, const Alloc& a = Alloc());
// basic_string(const CharT* s, const Alloc& a = Alloc());
#endif // NS

// basic_string(size_type n, CharT c, const Alloc& a = Alloc());
ORet _ox(String)::ommSizeChar(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string::size:char:");
  _MUTABLE_REC();
  _CHECK_ARGC_2(1, 2);
  const size_type n = g_castValue<size_type>(argv, m);
  _CHECK_EXCEPTION();
  const char c = argc == 1 ? '\0' : g_castValue<char>(argv + 1, m);
  _CHECK_EXCEPTION();
  *this = string(n, c);
  return makeResult();
}

#if 0 // TODO
// template<class InputIter>
//   basic_string(InputIter beg, InputIter end, const Alloc& a = Alloc())
#endif //0

//::::::::::::::::::::::::::::::   iterators   ::::::::::::::::::::::::::::::://

static const char* prefix[]{"", "c", "r", "cr"};

template<int _End, int _Const, int _Rev>
  ORet _genericIterator(String* _this, Session* m) {
    ORef r = s_class<StringIterator>()->createObject(m);
    _RETHROW("string::"+string{prefix[_Const+2*_Rev]}+(_End ? "end" : "begin"));
    auto& it = r->castRef<_O(StringIterator)>();
    if constexpr (_Rev) {
      if constexpr (_End)
        it = _this->begin();
      else
        it = _this->end();
    }
    else {
      if constexpr (_End)
        it = _this->end();
      else
        it = _this->begin();
    }
    if constexpr (_Const)
      static_cast<_O(StringIterator)*>(r)->setConst();
    static_cast<_O(StringIterator)*>(r)->setReverse(_Rev);
    return r->disposableResult();
  }

// iterator begin()
// const_iterator begin() const
// reverse_iterator rbegin()
// const_reverse_iterator rbegin() const

ORet _ox(String)::ommBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<0, 0, 0>(this, m); }
ORet _ox(String)::ommRBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<0, 0, 1>(this, m); }
ORet _ox(String)::ommCBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<0, 1, 0>(this, m); }
ORet _ox(String)::ommCRBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<0, 1, 1>(this, m); }

// iterator end()
// const_iterator end() const
// reverse_iterator rend()
// const_reverse_iterator rend() const

ORet _ox(String)::ommEnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<1, 0, 0>(this, m); }
ORet _ox(String)::ommREnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<1, 0, 1>(this, m); }
ORet _ox(String)::ommCEnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<1, 1, 0>(this, m); }
ORet _ox(String)::ommCREnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<1, 1, 1>(this, m); }

//:::::::::::::::::::::::::::::::   Capacity   ::::::::::::::::::::::::::::::://

//    size_type size() const
//    size_type length() const
ORet _ox(String)::ommSize(OArg, int argc, OArg* argv, Session* m) {
  ORef r = s_class<size_type>()->createObject(m);
  _RETHROW("string::size");
  _P(size_type, r)->setValue(size());
  return r->disposableResult();
}

//    size_type max_size() const
ORet _ox(String)::ommMaxSize(OArg, int argc, OArg* argv, Session* m) {
  ORef r = s_class<size_type>()->createObject(m);
  _RETHROW("string::maxSize");
  _P(size_type, r)->setValue(max_size());
  return r->disposableResult();
}

//    void resize(size_type n);
//    void resize(size_type n, CharT c);
ORet _ox(String)::ommResize(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 1 ? "string::resize:" : "string::resize:char:");
  _CHECK_ARGC_2(1, 2);
  _MUTABLE_REC();
  const size_type n = g_castValue<size_type>(argv, m);
  _CHECK_EXCEPTION();
  if (argc == 1)
    resize(n);
  else {
    const char c = g_castValue<char>(argv + 1, m);
    _CHECK_EXCEPTION();
    resize(n, c);
  }
  return makeResult();
}

//    size_type capacity() const;
ORet _ox(String)::ommCapacity(OArg, int argc, OArg* argv, Session* m) {
  ORef r = s_class<size_type>()->createObject(m);
  _RETHROW("string::capacity");
  _P(size_type, r)->setValue(capacity());
  return r->disposableResult();
}

//    void reserve(size_type res);
ORet _ox(String)::ommReserve(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::reserve:", 1);
  _MUTABLE_REC();
  size_type res = g_castValue<size_type>(argv, m);
  _CHECK_EXCEPTION();
  reserve(res);
  return makeResult();
}

//    void clear();
ORet _ox(String)::ommClear(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD("string::clear");
  clear();
  return makeResult();
}

//    bool empty() const
ORet _ox(String)::ommEmpty(OArg, int argc, OArg* argv, Session* m) {
  ORef r = s_class<bool>()->createObject(m);
  _RETHROW("string::empty");
  _PBOOL(r)->setValue(empty());
  return r->disposableResult();
}

//::::::::::::::::::::::::::::   Element Access   :::::::::::::::::::::::::::://

ORet _ox(String)::packComponentValue(char& c, const char* methodName, Session* m) {
  ORef r = BTI::getTypeProxyClass<char>()->createObject(m);
  _RETHROW(methodName);
  _PTypeProxy(char, r)->setValue(c);
  _PTypeProxy(char, r)->setElemPtr(&c);
  const bool constRec = m->getReceiver()->isConst();
  return OVar(r, USG_DISPOSABLE | (constRec ? USG_CONST : 0));
}

//    const_reference operator[] (size_type pos) const
//    reference operator[](size_type pos)
ORet _ox(String)::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string::component:");
  const size_type pos = g_castValue<size_type>(argv, m);
  _CHECK_EXCEPTION();
  if (!rec->isMutable())
    _RETURN_TYPE(oxType<char>, BTI::getTypeClass<char>(), operator[](pos))
  return packComponentValue(operator[](pos), _ox_method_name, m);
}

//    const_reference at(size_type n) const
//    reference at(size_type n)
ORet _ox(String)::ommAt(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string::at:");
  const size_type pos = g_castValue<size_type>(argv, m);
  _CHECK_EXCEPTION();
  if (!rec->isMutable())
    _RETURN_TYPE(oxType<char>, BTI::getTypeClass<char>(), at(pos))
  return packComponentValue(at(pos), _ox_method_name, m);
}

//::::::::::::::::::::::::::::::   Modifiers   ::::::::::::::::::::::::::::::://

// NS basic_string& operator+=(const CharT* s)

// basic_string& operator+=(const basic_string& str)
// basic_string& operator+=(CharT c)
ORet _ox(String)::method_incBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::incBy:", 1);
  _MUTABLE_REC();
  if (_ARG_TYPEID(0, CLSID_STRING))
    operator+=(_ARG(0)->castRef<_ox(String)>());
  else {
    char c = g_castValue<char>(argv, m);
    _CHECK_EXCEPTION();
    operator+=(c);
  }
  return makeResult();
}

//    void push_back(CharT c)
ORet _ox(String)::ommPushBack(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::pushBack:", 1);
  _MUTABLE_REC();
  char c = g_castValue<char>(argv, m);
  _CHECK_EXCEPTION();
  push_back(c);
  return makeResult();
}

//    void pop_back(CharT c)
ORet _ox(String)::ommPopBack(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string::popBack");
  _MUTABLE_REC();
  pop_back();
  return makeResult();
}

// NS basic_string& append(const CharT* s)
// NS basic_string& append(const CharT* s, size_type n)
// basic_string& append(const basic_string& str)
// basic_string& append(const basic_string& str, size_type pos, size_type n)
// basic_string& append(size_type n, CharT c)
ORet _ox(String)::ommAppend(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 1 ? "string::append:" :
                    argc == 2 ? "string::append:char:" :
                                "string::append:from:size:");
  _MUTABLE_REC();
  if (argc == 1) {
    _CHECK_ARG_TYPEID(0, CLSID_STRING);
    append(_ARG(0)->castConstRef<_ox(String)>());
  }
  else if (argc == 2) {
    if (_ARG_TYPEID(0, CLSID_STRING)) {
      const size_type n = g_castValue<size_type>(argv + 1, m);
      _RETHROW("string::append:size:");
      append(_ARG(0)->castConstRef<_ox(String)>().c_str(), n);
    }
    else {
      const size_type n = g_castValue<size_type>(argv, m);
      _CHECK_EXCEPTION();
      const char c = g_castValue<char>(argv + 1, m);
      _CHECK_EXCEPTION();
      append(n, c);
    }
  }
  else if (argc == 3) {
    _CHECK_ARG_TYPEID(0, CLSID_STRING);
    const size_type pos = g_castValue<size_type>(argv + 1, m);
    _CHECK_EXCEPTION();
    const size_type n = g_castValue<size_type>(argv + 2, m);
    _CHECK_EXCEPTION();
    append(_ARG(0)->castConstRef<_ox(String)>(), pos, n);
  }
  else
    _CHECK_ARGC_RANGE(1, 3);
  return makeResult();
}

#if 0 // TODO
    template<class _InputIterator>
    basic_string& append(_InputIterator first, _InputIterator last)
      { return this->replace(_M_iend(), _M_iend(), first, last); }
#endif

// basic_string& assign(const basic_string& str);
// basic_string& assign(const CharT* s)
// basic_string& assign(size_type n, CharT c)
// basic_string& assign(const basic_string& str, size_type pos, size_type n)
// basic_string& assign(const CharT* s, size_type n)
ORet _ox(String)::ommAssign(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 1 ? "string::assign:" :
                    argc == 2 ? "string::assign:char:" :
                                "string::assign:from:size:");
  _MUTABLE_REC();
  if (argc == 1) {
    _CHECK_ARG_TYPEID(0, CLSID_STRING);
    assign(_ARG(0)->castConstRef<_ox(String)>());
  }
  else if (argc == 2) {
    if (_ARG_TYPEID(0, CLSID_STRING)) {
      const size_type n = g_castValue<size_type>(argv + 1, m);
      _RETHROW("string::assign:size:");
      assign(_ARG(0)->castConstRef<_ox(String)>().c_str(), n);
    }
    else {
      const size_type n = g_castValue<size_type>(argv, m);
      _CHECK_EXCEPTION();
      const char c = g_castValue<char>(argv + 1, m);
      _CHECK_EXCEPTION();
      assign(n, c);
    }
  }
  else if (argc == 3) {
    _CHECK_ARG_TYPEID(0, CLSID_STRING);
    const size_type pos = g_castValue<size_type>(argv + 1, m);
    _CHECK_EXCEPTION();
    const size_type n = g_castValue<size_type>(argv + 2, m);
    _CHECK_EXCEPTION();
    assign(_ARG(0)->castConstRef<_ox(String)>(), pos, n);
  }
  else
    _CHECK_ARGC_RANGE(1, 3);
  return makeResult();
}

#if 0 // TODO
//    template<class _InputIterator>
//    basic_string& assign(_InputIterator first, _InputIterator last)

//    template<class _InputIterator>
//    void insert(iterator p, _InputIterator beg, _InputIterator end)
      { this->replace(p, p, beg, end); }
#endif

// basic_string& insert(size_type pos, const basic_string& str)
// basic_string& insert(size_type pos, size_type n, CharT ch)
// basic_string& insert(size_type pos, const basic_string& str,
//                      size_type from, size_type n = npos)
//
// basic_string& insert(size_type pos, const CharT* s, size_type n) // NS/IR
//
// iterator insert(const_iterator pos, CharT ch);
// iterator insert(const_iterator pos, size_type count, CharT ch)
// template<class InputIt>
//   iterator insert(const_iterator pos, InputIt first, InputIt last)

ORet _ox(String)::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 2 ? "string::insertAt:char:" :
                    argc == 3 ? "string::insertAt:size:char:" :
                    argc == 4 ? "string::insertAt:string:from:size:" :
                                "string::insertAt:");
  _MUTABLE_REC();
  _ASSERT(argc > 0, m);
  if (_ARG_TYPEID(0, CLSID_STRITER)) {
    StringIterator pos = _ARG(0)->castRef<_O(StringIterator)>();
    ORef r = s_class<StringIterator>()->createObject(m);
    _CHECK_EXCEPTION();
    if (argc == 2) {
      _CHECK_ARG_TYPEID(1, CLSID_CHAR);
      const char ch = g_castValue<char>(argv + 1, m);
      _CHECK_EXCEPTION();
      r->castRef<_O(StringIterator)>() = insert(pos, ch);
    }
    else if (argc == 3) {
      if (auto* req = _ARG(1)->getClass()->getRequirement();
          req && req->requiresInterface("InputIterator")) {
        _DECL_METHOD_NAME("string::insertAt:begin:end:");
        // iterator arguments must be of the same type
        _CHECK_ARG_TYPEID(2, _ARG(1)->getRootClassId());
        // initialize operation
        auto& op = _oxC(String)::insertOp;
        op.from = pos;
        // apply it
        bool handled;
        try {
          handled = op.apply(this, _ARG(1), _ARG(2));
        }
        catch (const std::logic_error& e) {
          _THROW_RTE(e.what());
        }
        if (handled)
          r->castRef<_O(StringIterator)>() = op.from;
        else {
          auto d = std::distance(this->begin(), pos);
          auto& container = this->castRef<_ox(String)>();
          auto curPos = pos;
          om::runLoop(m, argv + 1,
              [m, &container, &curPos](OVar result) { // loopAction
                // cast value to string element (char)
                ORef const o = REF(result);
                const auto rtId = o->getRootClassId();
                const char ch =
                  (rtId == CLSID_CHAR)       ? g_typeValue<char>(o) :
                  (rtId == CLSID_CHAR_PROXY) ? _PTypeProxy(char, o)->getValue() :
                                               g_castValue<char>(o, m);
                if (m->exceptionPending()) return;
                // insert value into this set
                const auto r = container.insert(curPos, ch);
                // advance to next position
                curPos = r + 1;
              });
          _CHECK_EXCEPTION();
          r->castRef<_O(StringIterator)>() = this->begin() + d;
        }
      }
      else {
        try {
          const size_type count = g_castValue<size_type>(argv + 1, m);
          if (m->exceptionPending()) throw 1;
          const char ch = g_castValue<char>(argv + 2, m);
          if (m->exceptionPending()) throw 2;
          r->castRef<_O(StringIterator)>() = insert(pos, count, ch);
        }
        catch (int n) {
          ObjectAutoCleaner cleaner{m, m->popException()};
          string* const method = static_cast<string*>(
              m->getCurrentMachineValue<void*>(M_METHOD_NAME));
          const string expectd = (!method || *method == _ox_method_name) ?
              (n == 1 ? "integer type" : "char") : "iterator";
          const string found = _ARG(n)->getClass()->isMutable() ?
              "mutable type" : _ARG(n)->getClassName();
          m->throwBadArg(method ? *method : _ox_method_name, n, expectd, found);
          return OVar::undefinedResult();
        }
      }
    }
    else
      _CHECK_ARGC_RANGE(1, 3);
    return r->disposableResult();
  }
  else {
    const size_type pos = g_castValue<size_type>(argv, m);
    _CHECK_EXCEPTION();
    if (argc == 2) {
      _DECL_METHOD_NAME("string::insertAt:string:");
      _CHECK_ARG_TYPEID(1, CLSID_STRING);
      insert(pos, _ARG(1)->castConstRef<_ox(String)>());
    }
    else if (argc == 3) {
      if (_ARG_TYPEID(1, CLSID_STRING)) {
        const size_type size = g_castValue<size_type>(argv + 2, m);
        _RETHROW("string::insertAt:string:size:");
        insert(pos, _ARG(1)->castConstRef<_ox(String)>().c_str(), size);
      }
      else {
        const size_type size = g_castValue<size_type>(argv + 1, m);
        _CHECK_EXCEPTION();
        const char ch = g_castValue<char>(argv + 2, m);
        _CHECK_EXCEPTION();
        insert(pos, size, ch);
      }
    }
    else if (argc == 4) {
      _CHECK_ARG_TYPEID(1, CLSID_STRING);
      const size_type from = g_castValue<size_type>(argv + 2, m);
      _CHECK_EXCEPTION();
      const size_type size = g_castValue<size_type>(argv + 3, m);
      _CHECK_EXCEPTION();
      insert(pos, _ARG(1)->castConstRef<_ox(String)>(), from, size);
    }
    else
      _CHECK_ARGC_RANGE(2, 4);
  }
  return makeResult();
}

// iterator erase(iterator position)
// iterator erase(iterator first, iterator last)
//
// basic_string& erase(size_type index = 0, size_type count = npos)
ORet _ox(String)::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc == 0 ? "string::erase" :
                    argc == 1 ? "string::eraseAt:" :
                    argc == 2 ? "string::eraseAt:end:" :
                                "string::eraseAt:");
  _MUTABLE_REC();
  if (argc == 0) {
    try { erase(); }
    catch (const std::out_of_range& ex) _THROW_BAD_USAGE(ex.what())
  }
  else if (_ARG_TYPEID(0, CLSID_STRITER)) {
    StringIterator pos = _ARG(0)->castRef<_O(StringIterator)>();
    ORef r = s_class<StringIterator>()->createObject(m);
    _CHECK_EXCEPTION();
    if (argc == 1)
      r->castRef<_O(StringIterator)>() = erase(pos);
    else if (argc == 2) {
      _CHECK_ARG_TYPEID(1, CLSID_STRITER);
      auto end  = _ARG(1)->castRef<_O(StringIterator)>();
      r->castRef<_O(StringIterator)>() = erase(pos, end);
    }
    else
      _CHECK_ARGC_2(1, 2)
    return r->makeResult();
  }
  else {
    size_type pos = g_castValue<size_type>(argv, m);
    _CHECK_EXCEPTION();
    try {
      if (argc == 1)
        erase(pos);
      else if (argc == 2) {
        size_type count = g_castValue<size_type>(argv + 1, m);
        _RETHROW("string::eraseAt:size:");
        erase(pos, count);
      }
      else
        _CHECK_ARGC_2(1, 2)
    }
    catch (const std::out_of_range& ex) {
      _THROW_BAD_USAGE(ex.what());
    }
  }
  return makeResult();
}

// Not Supported
//[3] basic_string& replace(size_type pos, size_type n1, const CharT* s)
//[4] basic_string& replace(size_type pos, size_type n1, const CharT* s, size_type n2)
//[4] basic_string& replace(iterator i1, iterator i2, const CharT* s, size_type n)
//[4] basic_string& replace(iterator i1, iterator i2, const CharT* k1, const CharT* k2)
//[4] basic_string& replace(iterator i1, iterator i2, CharT* k1, CharT* k2)
//[3] basic_string& replace(iterator i1, iterator i2, const CharT* s)

// string& replace(size_type pos, size_type n, const basic_string& str)
// string& replace(size_type pos, size_type n1, size_type n2, CharT c)
// string& replace(size_type pos1, size_type n1, const basic_string& str,
//                 size_type pos2, size_type n2);
// string& replace(iterator i1, iterator i2, size_type n, CharT c);
// string& replace(iterator i1, iterator i2, iterator k1, iterator k2)
// string& replace(iterator i1, iterator i2, const_iterator k1, const_iterator k2)
// string& replace(iterator i1, iterator i2, const basic_string& str)
ORet _ox(String)::ommReplace(OArg rec, int argc, OArg* argv, Session* m) {
  if (_ARG_TYPEID(0, CLSID_STRITER)) {
    _DECL_METHOD_NAME(argc == 3 ? "string::replaceAt:end:string:" :
                                  "string::replaceAt:end:from:to:");
    _MUTABLE_REC();
    _CHECK_ARG_TYPEID(1, CLSID_STRITER);
    StringIterator pos = _ARG(0)->castRef<_O(StringIterator)>();
    StringIterator end = _ARG(1)->castRef<_O(StringIterator)>();
    if (argc == 3) {
      _CHECK_ARG_TYPEID(2, CLSID_STRING);
      const auto& str = _ARG(2)->castConstRef<_ox(String)>();
      replace(pos, end, str);
    }
    else if (argc == 4) {
      if (auto* req = _ARG(2)->getClass()->getRequirement();
          req && req->requiresInterface("InputIterator")) {
        // iterator arguments must be same type
        _CHECK_ARG_TYPEID(3, _ARG(2)->getRootClassId());
        // initialize operation
        auto& op = _oxC(String)::replaceOp;
        op.from = pos;
        op.to = end;
        // apply it
        bool handled;
        try {
          handled = op.apply(this, _ARG(2), _ARG(3));
        }
        catch (const std::logic_error& e) {
          _THROW_RTE(e.what());
        }
        if (!handled) {
          const auto d = std::distance(this->begin(), pos);
          this->erase(pos, end); // now pos, end are not valid
          auto& container = this->castRef<_ox(String)>();
          StringIterator curPos = this->begin() + d;
          om::runLoop(m, argv + 2,
              [m, &container, &curPos](OVar result) { // loopAction
                ORef const o = REF(result);
                const auto rtId = o->getRootClassId();
                const char ch =
                  (rtId == CLSID_CHAR)       ? g_typeValue<char>(o) :
                  (rtId == CLSID_CHAR_PROXY) ? _PTypeProxy(char, o)->getValue() :
                                               g_castValue<char>(o, m);
                if (m->exceptionPending()) return;
                curPos = container.insert(curPos, ch);
                ++curPos;
              });
          _CHECK_EXCEPTION();
        }
      }
      else {
        _DECL_METHOD_NAME("string::replaceAt:end:size:char:");
        const size_type n = g_castValue<size_type>(argv + 2, m);
        _CHECK_EXCEPTION();
        const char ch = g_castValue<char>(argv + 3, m);
        _CHECK_EXCEPTION();
        replace(pos, end, n, ch);
      }
    }
    else
      _CHECK_ARGC_2(3, 4);
    return makeResult();
  }
  else {
    _DECL_METHOD_NAME(argc == 3 ? "string::replaceAt:size:string:" :
                      argc == 4 ? "string::replaceAt:n:size:char:" :
                                  "string::replaceAt:size:string:from:size:");
    _MUTABLE_REC();
    const size_type pos = g_castValue<size_type>(argv, m);
    _CHECK_EXCEPTION();
    const size_type size = g_castValue<size_type>(argv + 1, m);
    _CHECK_EXCEPTION();
    if (argc == 3) {
      _CHECK_ARG_TYPEID(2, CLSID_STRING);
      const auto& str = _ARG(2)->castConstRef<_ox(String)>();
      replace(pos, size, str);
    }
    else if (argc == 4) {
      const size_type n = g_castValue<size_type>(argv + 2, m);
      _CHECK_EXCEPTION();
      const char ch = g_castValue<char>(argv + 3, m);
      _CHECK_EXCEPTION();
      replace(pos, size, n, ch);
    }
    else if (argc == 5) {
      _CHECK_ARG_TYPEID(2, CLSID_STRING);
      const auto& str = _ARG(2)->castConstRef<_ox(String)>();
      const size_type from = g_castValue<size_type>(argv + 3, m);
      _CHECK_EXCEPTION();
      const size_type count = g_castValue<size_type>(argv + 4, m);
      _CHECK_EXCEPTION();
      replace(pos, size, str, from, count);
    }
    else
      _CHECK_ARGC_RANGE(3, 5);
    return makeResult();
  }
}

// NS size_type copy(CharT* s, size_type n,
//                   size_type pos = 0) const;

// void swap(basic_string& s);
ORet _ox(String)::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::swap:", 1);
  _MUTABLE_REC();
  _MUTABLE_ARG(0);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  swap(_ARG(0)->castRef<_ox(String)>());
  return makeResult();
}

//::::::::::::::::::::::::::   String operations   ::::::::::::::::::::::::::://

// const CharT* c_str() const
ORet _ox(String)::ommCStr(OArg rec, int argc, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<void*>()->createObject(m);
  _RETHROW("string::c_str");
  _PPTR(r)->setValue(const_cast<char*>(c_str()));
  return r->disposableResult();
}

// const CharT* data() const { return _M_data(); }
ORet _ox(String)::ommData(OArg rec, int argc, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<void*>()->createObject(m);
  _RETHROW("string::data");
  _PPTR(r)->setValue(data());
  return r->disposableResult();
}

ORet _ox(String)::ommNpos(OArg, int, OArg*, Session* m) {
  using npos_t = std::remove_const_t<decltype(string::npos)>;
  ORef r = BTI::getTypeClass<npos_t>()->createObject(m);
  _RETHROW("string::npos");
  _P(npos_t, r)->setValue(string::npos);
  return r->disposableResult();
}

// NS allocator_type get_allocator() const;

//:::::::::::::::::::::::::::::   find methods   ::::::::::::::::::::::::::::://

_DEFINE_FIND_METHOD(Find,           find             );
_DEFINE_FIND_METHOD(RFind,          rfind            );
_DEFINE_FIND_METHOD(FindFirstOf,    find_first_of    );
_DEFINE_FIND_METHOD(FindLastOf,     find_last_of     );
_DEFINE_FIND_METHOD(FindFirstNotOf, find_first_not_of);
_DEFINE_FIND_METHOD(FindLastNotOf,  find_last_not_of );

ORet _ox(String)::ommGenericFind(om::GenericFind&& find,
                                 int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("oxString::ommGenericFind");
  _CHECK_ARGC_2(1, 2);
  if (_ARG_TYPEID(0, CLSID_STRING))
    find.p = _ARG(0)->castPtr<_ox(String)>();
  else {
    find.ch = g_castValue<char>(argv, m);
    _CHECK_EXCEPTION();
  }
  if (argc == 2) {
    find.pos = g_castValue<size_type>(argv + 1, m);
    _CHECK_EXCEPTION();
  }
  ORef r = s_class<size_type>()->createObject(m);
  _CHECK_EXCEPTION();
  const auto found = find();
  _P(size_type, r)->setValue(found);
  return r->disposableResult();
}

// Not Supported
// size_type find (const CharT* s, size_type pos, size_type n) const
// size_type find (const CharT* s, size_type pos = 0) const
// size_type rfind(const CharT* s, size_type pos, size_type n) const
// size_type rfind(const CharT* s, size_type pos = npos) const
// size_type find_first_of(const CharT* s, size_type pos, size_type n) const
// size_type find_first_of(const CharT* s, size_type pos = 0) const
// size_type find_last_of(const CharT* s, size_type pos, size_type n) const
// size_type find_last_of(const CharT* s, size_type pos = npos) const
// size_type find_first_not_of(const CharT* s, size_type pos, size_type n) const
// size_type find_first_not_of(const CharT* s, size_type pos = 0) const
// size_type find_last_not_of(const CharT* s, size_type pos,  size_type n) const
// size_type find_last_not_of(const CharT* s, size_type pos = npos) const

// size_type find(const basic_string& str, size_type pos = 0) const
// size_type find(CharT c, size_type pos = 0) const;
ORet _ox(String)::ommFind(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(Find{*this}, argc, argv, m);
  _RETHROW("string::find:"+string{argc < 2 ? "" : "from:"});
  return result;
}

// size_type rfind(const basic_string& str, size_type pos = npos) const
// size_type rfind(CharT c, size_type pos = npos) const;
ORet _ox(String)::ommRFind(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(RFind{*this}, argc, argv, m);
  _RETHROW("string::rfind:"+string{argc < 2 ? "" : "from:"});
  return result;
}

// size_type find_first_of(const basic_string& str, size_type pos = 0) const
// size_type find_first_of(CharT c, size_type pos = 0) const
ORet _ox(String)::ommFindFirst(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(FindFirstOf{*this}, argc, argv, m);
  _RETHROW("string::findFirstOf:"+string{argc < 2 ? "" : "from:"});
  return result;
}

// size_type find_last_of(const basic_string& str, size_type pos = npos) const
// size_type find_last_of(CharT c, size_type pos = npos) const
ORet _ox(String)::ommFindLast(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(FindLastOf{*this}, argc, argv, m);
  _RETHROW("string::findLastOf:"+string{argc < 2 ? "" : "end:"});
  return result;
}

// size_type find_first_not_of(const basic_string& str, size_type pos = 0) const
// size_type find_first_not_of(CharT c, size_type pos = 0) const;
ORet _ox(String)::ommFindFirstNotOf(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(FindFirstNotOf{*this}, argc, argv, m);
  _RETHROW("string::findFirstNotOf:"+string{argc < 2 ? "" : "from:"});
  return result;
}

// size_type find_last_not_of(const basic_string& str, size_type pos = npos) const
// size_type find_last_not_of(CharT c, size_type pos = npos) const;
ORet _ox(String)::ommFindLastNotOf(OArg, int argc, OArg* argv, Session* m) {
  ORet result = ommGenericFind(FindLastNotOf{*this}, argc, argv, m);
  _RETHROW("string::findLastNotOf:"+string{argc < 2 ? "" : "end:"});
  return result;
}

//::::::::::::::::::::::::::::::   operations   :::::::::::::::::::::::::::::://

ORet _ox(String)::ommStartsWith(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::startsWith:", 1);
  _CHECK_EXCEPTION();
  string text;
  string* p = &text;
  if (_ARG_TYPEID(0, CLSID_STRING))
    p = _ARG(0)->castPtr<_ox(String)>();
  else if (_ARG_TYPEID(0, CLSID_CHAR))
    text = _P(char, _ARG(0))->getValue();
  else {
    _CHECK_ARG_TYPEIDS(0, (vector<int>{CLSID_STRING, CLSID_CHAR}))
  }
  _RETURN_BOOL(compare(0, p->size(), *p) == 0);
}

ORet _ox(String)::ommEndsWith(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string::endsWith:", 1);
  _CHECK_EXCEPTION();
  string text;
  string* p = &text;
  if (_ARG_TYPEID(0, CLSID_STRING))
    p = _ARG(0)->castPtr<_ox(String)>();
  else if (_ARG_TYPEID(0, CLSID_CHAR))
    text = _P(char, _ARG(0))->getValue();
  else {
    _CHECK_ARG_TYPEIDS(0, (vector<int>{CLSID_STRING, CLSID_CHAR}))
  }
  const auto n = p->size();
  _RETURN_BOOL(n > size() ? false : compare(size() - n, n, *p) == 0);
}

//::::::::::::::::::::::::::::::   substring   ::::::::::::::::::::::::::::::://

static const char* s_substrNames[]{
  "string::substring", "string::substring:", "string::substring:size:"
};

// basic_string substr(size_type pos = 0, size_type n = npos) const
ORet _ox(String)::ommSubstring(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(s_substrNames[argc > 2 ? 1 : argc]);
  size_type pos = 0, size = npos;
  try {
    switch (argc) {
      case 2: size = g_castValue<size_type>(argv + 1, m);
              if (m->exceptionPending()) throw 1;
      case 1: pos  = g_castValue<size_type>(argv + 0, m);
              if (m->exceptionPending()) throw 0;
      case 0: break;
      default: _CHECK_ARGC_RANGE(0, 2);
    }
  }
  catch (int n) {
    ObjectAutoCleaner autoCleaner{m, m->popException()};
    return m->throwBadArg(_ox_method_name, n,
        BTI::getTypeClass<size_type>()->name, _ARG(n)->getClass()->name);
  }
  ORef r = s_class<String>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<_ox(String)>() = substr(pos, size);
  return r->disposableResult();
}

//:::::::::::::::::::::::::::::::   compare   :::::::::::::::::::::::::::::::://

static const char* s_compareNames[]{
  "",
  "string::compare:",
  "string::compare:",
  "string::compare:size:string:",
  "string::compare:",
  "string::compare:size:string:from:size:"
};

// int compare(const basic_string& str) const
// int compare(size_type pos, size_type n, const basic_string& str) const;
// int compare(size_type pos1, size_type n1, const basic_string& str,
//    size_type pos2, size_type n2) const;
ORet _ox(String)::ommCompare(OArg, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(s_compareNames[argc > 5 ? 1 : argc]);
  int result;
  size_type pos1, n1, pos2 = 0, n2 = string::npos;
  try {
    switch (argc) {
      case 5:
        pos2 = g_castValue<size_type>(argv + 3, m);
        if (m->exceptionPending()) throw 3;
        n2   = g_castValue<size_type>(argv + 4, m);
        if (m->exceptionPending()) throw 4;
      case 3: {
        _CHECK_ARG_TYPEID(2, CLSID_STRING);
        pos1 = g_castValue<size_type>(argv + 0, m);
        if (m->exceptionPending()) throw 0;
        n1   = g_castValue<size_type>(argv + 1, m);
        if (m->exceptionPending()) throw 1;
        const string& text = _ARG(2)->castConstRef<_ox(String)>();
        result = this->__compare(pos1, n1, text, pos2, n2);
        break;
      }
      case 1:
        _CHECK_ARG_TYPEID(0, CLSID_STRING);
        result = this->__compare(0, string::npos,
            _ARG(0)->castConstRef<_ox(String)>(), 0);
        break;
      default:
        _CHECK_ARG_SIZE(argc, (std::vector<int>{1, 3, 5}), -1);
    }
  }
  catch (int n) {
    ObjectAutoCleaner autoCleaner{m, m->popException()};
    return m->throwBadArg(_ox_method_name, n,
        BTI::getTypeClass<size_type>()->name, _ARG(n)->getClass()->name);
  }
  catch (const std::out_of_range& e) {
    _THROW_RTE(e.what());
  }
  ORef r = s_class<int>()->createObject(m);
  _CHECK_EXCEPTION();
  _P(int, r)->setValue(result);
  return r->disposableResult();
}

//::::::::::::::::::   fix a bug in std::string::compare   ::::::::::::::::::://
static int _s_compare(string::size_type __n1, string::size_type __n2) noexcept {
  const string::difference_type __d = string::difference_type(__n1 - __n2);
#if defined(__GNUC__)
  if (__d > __gnu_cxx::__numeric_traits<int>::__max)
    return __gnu_cxx::__numeric_traits<int>::__max;
  else if (__d < __gnu_cxx::__numeric_traits<int>::__min)
    return __gnu_cxx::__numeric_traits<int>::__min;
#else
  if (__d > std::numeric_limits<int>::max())
    return std::numeric_limits<int>::max();
  else if (__d < std::numeric_limits<int>::min())
    return std::numeric_limits<int>::min();
#endif
  else
    return int(__d);
}

static string::size_type _s_check(const string& s,
    string::size_type __pos, const char* __s) {
  if (__pos > s.size())
    throw std::out_of_range(__s+string{": __pos (which is "}+
        std::to_string(__pos)+") > this->size() (which is "+
        std::to_string(s.size())+")");
  return __pos;
}

string::size_type _s_limit(const string& s, string::size_type __pos,
    string::size_type __off) noexcept {
  const bool __testoff =  __off < s.size() - __pos;
  return __testoff ? __off : s.size() - __pos;
}

int _ox(String)::__compare(size_type __pos1, size_type __n1,
    const basic_string& __str, size_type __pos2, size_type __n2) const {
  _s_check(*this, __pos1, "basic_string::compare");
  _s_check(__str, __pos2, "basic_string::compare");
  __n1 = _s_limit(*this, __pos1, __n1);
  __n2 = _s_limit(__str, __pos2, __n2);
  const size_type __len = std::min(__n1, __n2);
  int __r = ::strncmp(data() + __pos1, __str.data() + __pos2, __len);
  if (!__r)
    __r = _s_compare(__n1, __n2);
  return __r;
}
//::::::::::::::::::::::::::::   end bug fixing   :::::::::::::::::::::::::::://

// Not Supported
//    int compare(const CharT* s) const;
//
     // _GLIBCPP_RESOLVE_LIB_DEFECTS
     // 5. String::compare specification questionable
//    int compare(size_type pos, size_type n1,
//       const CharT* s) const;
//    int compare(size_type pos, size_type n1,
//       const CharT* s, size_type n2) const;

//::::::::::::::::::::::::::::::   operator+   ::::::::::::::::::::::::::::::://

#if 0 // Not supported
  template<typename CharT, typename Traits, typename Alloc>
  basic_string
  operator+(const CharT* lhs,
       const basic_string& rhs);
  template<typename CharT, typename Traits, typename Alloc>
  inline basic_string
  operator+(const basic_string& lhs,
      const CharT* rhs);
#endif

//template<typename CharT, typename Traits, typename Alloc>
//  basic_string
//  operator+(const basic_string& lhs,
//            const basic_string& rhs)

//template<typename CharT, typename Traits, typename Alloc>
//  inline basic_string
//  operator+(const basic_string& lhs,
//            CharT rhs)

ORet _ox(String)::method_add(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::add:", 1);

  ORef r = getClass()->createObject(m);
  _CHECK_EXCEPTION();

  string& result = r->castRef<_ox(String)>();
  if (_ARG_TYPEID(0, CLSID_STRING))
    result = *static_cast<String*>(this) + _ARG(0)->castConstRef<_ox(String)>();
  else {
    const char c = g_castValue<char>(argv, m);
    _CHECK_EXCEPTION();
    result = *static_cast<String*>(this) + c;
  }

  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   basic_string operator+(CharT lhs, const basic_string& rhs);
// implemented in oxType<char>

// Not Supported
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator==(const CharT* lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator!=(const basic_string& lhs, const CharT* rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator<(const basic_string& lhs, const CharT* rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator>(const basic_string& lhs, const CharT* rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator<=(const basic_string& lhs, const CharT* rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator>=(const basic_string& lhs, const CharT* rhs)
// Supported as method
// template<typename CharT, typename Traits, typename Alloc>
//   inline void swap(basic_string& lhs, basic_string& rhs)

//::::::::::::::::::::::   Operators == != < > <= >=   ::::::::::::::::::::::://

// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator==(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator==(const basic_string& lhs, const CharT* rhs)
ORet _ox(String)::method_eq(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::eq:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();
  _PBOOL(r)->setValue(*this == _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   bool operator!=(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   bool operator!=(const CharT* lhs, const basic_string& rhs)
ORet _ox(String)::method_ne(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::ne:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();
  _PBOOL(r)->setValue(*this != _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator<(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator<(const CharT* lhs, const basic_string& rhs)
ORet _ox(String)::method_lt(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::lt:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();
  _PBOOL(r)->setValue(*this < _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator>(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   inline bool operator>(const CharT* lhs, const basic_string& rhs)
ORet _ox(String)::method_gt(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::gt:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();
  _PBOOL(r)->setValue(*this > _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   bool operator<=(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   bool operator<=(const CharT* lhs, const basic_string& rhs)
ORet _ox(String)::method_le(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::le:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();
  _PBOOL(r)->setValue(*this <= _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// template<typename CharT, typename Traits, typename Alloc>
//   bool operator>=(const basic_string& lhs, const basic_string& rhs)
// template<typename CharT, typename Traits, typename Alloc>
//   bool operator>=(const CharT* lhs, const basic_string& rhs)
ORet _ox(String)::method_ge(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("string::ge:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);

  ORef r = s_class<bool>()->createObject(m);
  _CHECK_EXCEPTION();

  _PBOOL(r)->setValue(*this >= _ARG(0)->castConstRef<_ox(String)>());
  return r->disposableResult();
}

// stream package:
// template<typename CharT, typename Traits, typename Alloc>
//   basic_istream>& operator>>(basic_istream& is, basic_string& str);
// template<typename CharT, typename Traits, typename Alloc>
//   basic_ostream>& operator<<(basic_ostream& os, const basic_string& str);
// template<typename CharT, typename Traits, typename Alloc>
//   basic_istream& getline(basic_istream& is, basic_string& str, CharT delim);
// template<typename CharT, typename Traits, typename Alloc>
//   basic_istream& getline(basic_istream& is, basic_string& str);

//::::::::::::::::::::::::::::::::   Ranges   :::::::::::::::::::::::::::::::://

template<int _Const, int _Rev>
  ORet _genericAll(String* _this, Session* m) {
    ORef b, e, r;
    OClass* const stringIteratorType = s_class<StringIterator>();
    b = stringIteratorType->createObject(m);
    if (!m->exceptionPending()) {
      e = stringIteratorType->createObject(m);
      if (!m->exceptionPending())
        r = s_class<ORange>()->createObject(m);
    }
    _RETHROW("string::"+string{prefix[_Const+2*_Rev]}+"all");
    if constexpr (_Rev) {
      static_cast<_O(StringIterator)*>(b)->setReverse(true);
      static_cast<_O(StringIterator)*>(e)->setReverse(true);
      b->castRef<_O(StringIterator)>() = _this->rbegin().base();
      e->castRef<_O(StringIterator)>() = _this->rend().base();
    }
    else {
      b->castRef<_O(StringIterator)>() = _this->begin();
      e->castRef<_O(StringIterator)>() = _this->end();
    }
    if constexpr (_Const) {
      static_cast<_O(StringIterator)*>(b)->setConst();
      static_cast<_O(StringIterator)*>(e)->setConst();
    }
    static_cast<ORange*>(r)->addPipelineAll(m, b, e);
    return r->disposableResult();
  }

ORet _ox(String)::ommAll(OArg, int, OArg*, Session* m)
{ return _genericAll<0, 0>(this, m); }
ORet _ox(String)::ommRAll(OArg, int, OArg*, Session* m)
{ return _genericAll<0, 1>(this, m); }
ORet _ox(String)::ommCAll(OArg, int, OArg*, Session* m)
{ return _genericAll<1, 0>(this, m); }
ORet _ox(String)::ommCRAll(OArg, int, OArg*, Session* m)
{ return _genericAll<1, 1>(this, m); }

//============================================================================//
//:::::::::::::::::::::::::::::   _oxC(String)   ::::::::::::::::::::::::::::://
//============================================================================//

_DEFINE_MCLASS(String);
_DEFINE_MCLASS(WString);

using StringROP = _oxC(String)::StringROP;
StringROP _oxC(String)::insertOp{RangeOperationType::Insert, CLSID_STRING};
StringROP _oxC(String)::replaceOp{RangeOperationType::Replace, CLSID_STRING};

/* static */
void _oxC(String)::registerOperations(Session* m, bool makePublic) {
  // initialize operations
  _ASSERT(insertOp.empty(), m);
  _ASSERT(replaceOp.empty(), m);
  insertOp.addTies(
    _O(StringIterator)::Tie<StringROP>{},
    _O(VectorIterator)<char>::Tie<StringROP>{}
  );
  replaceOp.addTies(
    _O(StringIterator)::Tie<StringROP>{},
    _O(VectorIterator)<char>::Tie<StringROP>{}
  );
  if (makePublic)
    m->publicOperations(
      &_oxC(String)::insertOp,
      &_oxC(String)::replaceOp
    );
}
