#include <string>
#include <vector>
#include <algorithm>
#include "object.hpp"
#include "oclsmgr.hpp"
#include "odev.h"
#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
// #include "oexcept.hpp"
// #include "otypeless.hpp"
#include "oproto.hpp"
#include "obasicTypeInfo.hpp"
#include "selectors.h"
#include "otypes.h"
#include "odefs.h"

using std::string;
using std::vector;

/** Convert number to hexadecimal string */
string ul2hexstr(unsigned long x) {
  const string hexsym("0123456789abcdef");
  size_t mask = 0xf;
  string result;
  for (size_t i = 0; i < sizeof(x)*2; ++i, mask <<= 4)
    result = hexsym[(x & mask) >> i*4]+result;
  return result;
}

/** Convert pointer to hexadecimal string */
string ul2hexstr(void* p) {
  return ul2hexstr(reinterpret_cast<ulong>(p));
}

struct ScriptMethodResolveData {
   int ofs;
   OClass* cls;
};

#if 0
// TODO MT safe?
static TypelessPropertyList s_TypelessProps;
#endif // 0


const string& g_getClassName(OClass* c) {return c->name;}
static ORef s_ctorThrowBadClass(Session* m, const string& typeName,
                                            const string& reason);
static void s_classCtorBadClass(Session* m, const string& description,
                                            const string& reason);

//============================================================================//
//::::::::::::::::::::::::::::::::   OClass   :::::::::::::::::::::::::::::::://
//============================================================================//

OClass::OClass(OClassMgr* cm, const string& className) :
    Descriptor             {className},
    classId_               {cm->issueClassId()},
    flags_                 {CLSF_STATUS_OPEN},
    baseClass_             {nullptr},
    access_                {0, 0, 0},
    classBlockId_          {-1},
    dedicatedUses_         {0},
    req_                   {nullptr} {
  if (!cm->registerClass(this)) {
    // We cannot throw a VM-exception here because there is no RT object yet
    flags_ |= CLSF_STATUS_CTOR_FAILED;
    return;
  }
  // Cache root class. This class is a root class because this constructor
  // is never used by script derived classes.
  rootClass_ = this;
}

OClass::OClass(Session* m, const string& className) :
    Descriptor             {className},
    classId_               {m->classMgr->issueClassId()},
    flags_                 {CLSF_STATUS_OPEN},
    baseClass_             {nullptr},
    access_                {0, 0, 0},
    classBlockId_          {-1},
    dedicatedUses_         {0},
    req_                   {nullptr} {
  _ASSERT(classId_ < MAX_NUM_CLASSES, m);
  if (!m->classMgr->registerClass(this)) {
    s_classCtorBadClass(m,
        "the class '"+className+"' could not be registered",
        "a class with the same name is already registered");
    return;
  }
  // Cache root class. This class is a root class because this
  // constructor is never used by script derived classes.
  rootClass_ = this;
}

OClass::OClass(Session* m, const string& baseClass,
               const string& className) :
    Descriptor             {className},
    classId_               {m->classMgr->issueClassId()},
    flags_                 {CLSF_STATUS_OPEN},
    baseClass_             {m->classMgr->getClass(baseClass)},
    access_                {0, 0, 0},
    classBlockId_          {-1},
    dedicatedUses_         {0},
    req_                   {nullptr} {
  _ASSERT(classId_ < MAX_NUM_CLASSES, m);
  if (!baseClass_) {
    s_classCtorBadClass(m,
        "the type '"+className+"' could not be created",
        "the base type '"+baseClass+"' does not exist");
    return;
  }
  if (!m->classMgr->registerClass(this)) {
    s_classCtorBadClass(m,
        "the type '"+className+"' could not be registered",
        "a type with the same name is already registered");
    return;
  }
  // Cache root class
  rootClass_ = findRootClass(); // TODO change to baseClass->rootClass_?
  _ASSERT(rootClass_ != this, m);
  // inherit requirements
  req_ = baseClass_->getRequirement();
}

#define CLSF_DEDICATED_CLASS CLSF_STATUS_READY  | CLSF_STATUS_OPEN | \
                             CLSF_STATUS_HIDDEN | CLSF_STATUS_MUTABLE

OClass::OClass(Session* m, const OClass* baseClass, const string& className) :
    Descriptor             {className},
    classId_               {m->classMgr->issueMutableTypeId(this, m)},
    flags_                 {CLSF_DEDICATED_CLASS},
    baseClass_             {baseClass->getImmutableBase()},
    rootClass_             {baseClass->rootClass_},
    access_                {0, 0, 0},
    classBlockId_          {-1},
    dedicatedUses_         {0},
    req_                   {nullptr} {
  if (classId_ == 0) {
    m->rethrow("OClass::OClass", __FILE__, __LINE__);
    return;
  }
  _ASSERT(classId_ >= MAX_NUM_CLASSES, m);
  // inherit props and script-defined methods
  if (baseClass->isMutable()) {
    for (const OClass* mutableBase : baseClass->getMutableBases()) {
      props_ += mutableBase->props_;
      methods_ += mutableBase->methods_;
    }
  }
  // inherit requirements
  req_ = baseClass->getRequirement();
}

void s_classCtorBadClass(Session* m, const string& description,
                                     const string& reason) {
  m->throwBadClass(/* method      */ "OClass::OClass",
                   /* description */ description,
                   /* context     */ "class constructor",
                   /* reason      */ reason,
                   /* file, line  */ __FILE__, __LINE__);
}

OClass::~OClass() NOEXCEPT_IF_NDEBUG {
  ASSERT(dedicatedUses_ == 0)
//   if (eventTable_ != nullptr) // TODO enable when you enable event table
    // delete eventTable_;
}

OClass* OClass::createClass(Session* m, const string& c) const {
  _ENTER_NOCHK("OClass::createClass");
  _ASSERT(OClass::s_getRootClass(name, m) == BTI::getTypeClass<ORef>(), m);
  OClass* cls = new OClass(m, this->name, c);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

OClass* OClass::createMutableClass(Session* m, const string& className) const{
  _ENTER_NOCHK("OClass::createMutableClass");
  _ASSERT(this->getRootClass() == BTI::getTypeClass<ORef>(), m);
  OClass* cls = new OClass(m, this, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

//:::::::::::::::::::::::::::   Class Management   ::::::::::::::::::::::::::://

bool OClass::finalize(Session* m) {
  // if a property has undefined type, it is a template class
  if (props_.isTemplate())
    flags_ |= CLSF_HAS_TEMPLATE_MEMBERS;
  if (hasTemplateMembers() || (baseClass_ && baseClass_->isTemplateClass()))
    flags_ |= CLSF_STATUS_TEMPLATE;
  else
    flags_ |= CLSF_STATUS_READY;
  // check if it is recursively defined
  if (isRecursivelyDefined()) {
    if (m) // it can be that m is nullptr
      m->throwBadClass(
      /* method      */ "OClass::finalize",
      /* description */ "could not define class '"+name+"'",
      /* context     */ "class construction",
      /* reason      */ "the class has recursively defined properties",
      /* file, line  */ __FILE__, __LINE__);
    return false;
  }
  // pack member tables --> 🛠️ TODO
  ;
  // flag class closed
  flags_ &= ~CLSF_STATUS_OPEN;
  return true;
}

OClass* OClass::getImmutableBase() const noexcept {
  const OClass* c = this;
  while (c->isMutable())
    c = c->baseClass_;
  return const_cast<OClass*>(c);
}

vector<const OClass*> OClass::getMutableBases() const noexcept {
  vector<const OClass*> v;
  const OClass* c = this;
  while (c->isMutable()) {
    v.push_back(c);
    c = c->baseClass_;
  }
  const size_t n = v.size();
  for (size_t i = 0; i < n/2; ++i)
    std::swap(v[i], v[n - 1 - i]);
  return v;
}

//:::::::::::::::::::::::::::   Template Classes   ::::::::::::::::::::::::::://

static
string _errorMessage(int rc, const string& className, const string& propName);

vector<const OClass*> OClass::getTemplateParamTypes(int argc, OArg* argv,
    Session* m) const {
  _ASSERT(this->isTemplateClass(), m);
  vector<const OClass*> templateTypes;
  size_t k = 0;
  for (const OClass* p = this; p->isTemplateClass(); p = p->baseClass_) {
    if (!p->hasTemplateMembers()) {
      k += p->props_.size();
      continue;
    }
    for (size_t i = 0; i < p->props_.size(); ++i, ++k) {
      if (!p->isPropTemplate(i)) continue;
      if (k >= static_cast<size_t>(argc))
        throw static_cast<int>(templateTypes.size()); // error
      _ASSERT(!argv[k]->undefined(), m);
      templateTypes.push_back(_ARG(k)->getClass());
    }
  }
  // return k > argc ? vector<const OClass*>{} : templateTypes;
  return templateTypes;
}

vector<OArg> OClass::getArgsFromTemplateParams(size_t argc, OArg* argv) const {
  ASSERT(this->isTemplateClass());
  vector<OArg> args;
  size_t k = 0;
  for (const OClass* p = this; p->isTemplateClass(); p = p->baseClass_) {
    for (size_t i = 0; i < p->props_.size(); ++i) {
      if (p->isPropTemplate(i)) {
        if (k >= argc)
          return vector<OArg>{}; // error
        args.push_back(argv[k++]);
      }
      else
        args.push_back(nullptr);
    }
  }
  return k < argc ? /*error*/vector<OArg>{} : args;
}

OClass* OClass::findSpecialization(const vector<const OClass*>& paramTypes) const {
  const auto begin = specializations_.begin();
  const auto end = specializations_.end();
  auto it = std::find_if(begin, end, [this, &paramTypes](const OClass* c) {
    return this->matchesTemplateParams(c, paramTypes);
  });
  return it == end ? nullptr : const_cast<OClass*>(*it);
}

size_t OClass::countTemplateParams() const NOEXCEPT_IF_NDEBUG {
  ASSERT(this->isTemplateClass());
  size_t k = 0;
  for (const OClass* p = this; p->isTemplateClass(); p = p->baseClass_) {
    if (p->hasTemplateMembers()) {
      for (size_t i = 0; i < p->props_.size(); ++i)
        if (p->isPropTemplate(i)) ++k;
    }
  }
  return k;
}

bool OClass::matchesTemplateParams(const OClass* specialization,
    const vector<const OClass*>& templateTypes) const {
  ASSERT(this->isTemplateClass());
  ASSERT(!specialization->isTemplateClass());
  ASSERT(specialization->isSpecializationOf(this));
  size_t k = 0;
  const OClass* p = this;
  const OClass* q = specialization;
  for (; p->isTemplateClass(); p = p->baseClass_, q = q->baseClass_) {
    ASSERT(p->props_.size() == q->props_.size());
    if (!p->hasTemplateMembers()) continue;
    for (size_t i = 0; i < p->props_.size(); ++i) {
      ASSERT(p->isPropTemplate(i) || p->props_[i].type == q->props_[i].type);
      if (!p->isPropTemplate(i)) continue;
      if (k >= templateTypes.size() || q->props_[i].type != templateTypes[k])
        return false;
      ++k;
    }
  }
  return k == templateTypes.size();
}

OClass* OClass::specializeTemplateClass(Session* m,
    const vector<const OClass*>& templateTypes) {
  _ASSERT(isTemplateClass(), m);
  size_t base = 0;
  OClass* specialization = specialize(m, templateTypes, base);
  if (m->exceptionPending()) {
    m->rethrow("OClass::specializeTemplateClass", __FILE__, __LINE__);
    return nullptr;
  }
  base += props_.countTemplateProps();
  if (base < templateTypes.size()) {
    const string msg{"excessive template data"};
    m->throwRTE("OClass::specializeTemplateClass", msg, __FILE__, __LINE__);
    return nullptr;
  }
  _ASSERT(base == templateTypes.size(), m);
  return specialization; // it is the nullptr if exception is pending
}

OClass* OClass::specialize(Session* m,
                           const vector<const OClass*>& templateTypes,
                           size_t& base) {
  _ASSERT(isTemplateClass(), m);
  size_t originalBase = base;
  // specialize base class
  const OClass* baseClass = baseClass_;
  if (baseClass_->isTemplateClass()) {
    base += this->props_.countTemplateProps();
    baseClass = baseClass_->specialize(m, templateTypes, base);
    if (!baseClass || m->exceptionPending())
      return nullptr;
  }
  // create class extending baseClass
  const string className = buildTemplateClassName(templateTypes, originalBase);
  if (className.empty()) {
    m->throwRTE("OClass::specialize", "more template data were expected");
    return nullptr;
  }
  OClass* specialization = m->classMgr->getClass(className);
  if (specialization) {
    if (specialization->isSpecializationOf(this))
      return specialization;
    const string msg = "the type '"+className+"' is not a specialization "
        "of the template '"+name+"'";
    m->throwRTE("OClass::specialize", msg);
    return nullptr;
  }
  specialization = baseClass->createClass(m, className);
  if (m->exceptionPending())
    return nullptr;
  // add properties
  _ASSERT(!isClassReady(), m);
  auto& props = specialization->props_;
  for (size_t i = 0, k = 0; i < props_.size(); ++i) {
    // the next condition was already validated by buildTemplateClassName
    _ASSERT(!isPropTemplate(i) || originalBase + k < templateTypes.size(), m);
    // get next property's type
    const auto& prop = props_[i];
    const auto options = props_.protection(i);
    OClass* propType = nullptr;
    if (isPropTemplate(i))
      propType = const_cast<OClass*>(templateTypes[originalBase + k++]);
#if 0 // Q: is this necessary?
    else if (prop.type && prop.type->recursivelyExtends(specialization)) {
      const string reason =
          "the property '"+prop.name+"' is recursively defined";
      const string description =
          "could not construct the type '"+specialization->name+"'";
      m->throwBadClass( "OClass::specialize",
      /* description */ description,
      /* context     */ "template specialization",
      /* reason      */ reason,
      /* file, line  */ __FILE__, __LINE__);
      return nullptr;
    }
#endif // 0
    else
      propType = prop.type;
    // add property to specialization
    _ASSERT(!specialization->getProperty(prop.name).valid(), m);
    props.addPropertyNoCheck(string{prop.name}, propType, options);
  }
  // add methods
  auto& methods = specialization->methods_;
  for (size_t i = 0, k = 0; i < methods_.size(); ++i) {
    const auto& method = methods_[i];
    const auto options = methods_.protection(i);
    _DEBUG( const bool success = )
    methods.addMethod(string{method.name}, method.block, options);
    _ASSERT(success, m);
    _ASSERT(!methods.back().name.empty(), m);
    specialization->cacheMethodAddress(methods.back(), options);
  }
  // finalize
  specialization->finalize(m);
  // register it as specialization of this class
  specializations_.push_front(specialization);
  // finalize may throw exceptions
  _RETHROW_RETURN(nullptr, "OClass::specialize");
  return specialization;
}

bool OClass::isSpecializationOf(const OClass* templateClass) const
                                                NOEXCEPT_IF_NDEBUG {
  ASSERT(templateClass->isTemplateClass());
  const auto end = templateClass->specializations_.end();
  return std::find(templateClass->specializations_.begin(), end, this) != end;
}

string OClass::buildTemplateClassName(
    const vector<const OClass*>& templateTypes, size_t base) const {
  string templateClassName = name+"<";
  size_t k = 0;
  for (const OClass* p = this; p->isTemplateClass(); p = p->baseClass_) {
    for (size_t i = 0; i < p->props_.size(); ++i) {
      if (!p->isPropTemplate(i)) continue;
      if (base + k >= templateTypes.size()) return ""; // error
      templateClassName += (k ? "," : "") + templateTypes[base + k]->name;
      ++k;
    }
  }
  return templateClassName+">";
}

string _errorMessage(int rc, const string& className, const string& propName) {
  const string c{"class '"+className+"'"};
  const string p{"property '"+propName+"'"};
  return rc == 2 ? "duplicate "+p+" for "+c :
                   "could not add "+p+" because "+c+" is finalized";
}

//:::::::::::::::::::::::::   Property Management   :::::::::::::::::::::::::://

const vector<OVar>& OClass::propArray(ORef self) const noexcept
{ return self->props_; }

vector<OVar>& OClass::propArray(ORef self) noexcept
{ return self->props_; }

int OClass::addProperty(Session* m, string&& propName,
                        const string& className, ushort options) {
  // do not allow addition of properties to closed classes
  if (isClassReady()) {
    if (!m->warnOnAddMemberClassReady()) return 1;
    m->displayMessage("Note", "Property '"+propName+"' was not added to "
       "class '"+name+"' because it is not open. Definition was neglected.");
    return 0; // do not throw exception
  }
  // do not allow reintroducton of the same property
  if (getProperty(propName).valid()) {
    if (!m->warnOnDuplicateMember()) return 2;
    m->displayMessage("Note", "Property '"+propName+"' already belongs to "
        "class '"+name+".' Definition was neglected.");
    return 0; // do not throw exception
  }
  // get class
  OClass* c = nullptr;
  bool typeless = false;
  if (!className.empty()) {
    c = m->classMgr->getClass(className);
    if (c == nullptr) typeless = true;
  }
  // add property to table
  int ofs = props_.addPropertyNoCheck(std::move(propName), c, options);
  // if its type is not fixed determine it later
#if 0
  if (typeless) {
    s_TypelessProps.add(this, className, ofs);
  }
#endif // 0
  return 0;
}

int OClass::addMutableTypeProperty(Session* m, std::string&& name,
                                   OClass* type, ushort options) {
  // do not allow reintroducton of the same property
  if (getProperty(name).valid()) {
    if (!m->warnOnDuplicateMember()) return 2;
    m->displayMessage("Note", "Property '"+name+"' already belongs to "
        "class '"+name+".' Definition was neglected.");
    return 0; // do not throw exception
  }
  // add property to table
  props_.addPropertyNoCheck(std::move(name), type, options);
  return 0;
}

#if 0
void OClass::s_resolvePropTypes(Session* m) {
  for (auto& u : s_TypelessProps) {
    _ASSERT(u.ofs != -1, m);
    OClass* resolved = m->classMgr->getClass(u.typeName);
    if (resolved != nullptr) {
      // resolve property type
      u.owner->props_[u.ofs].type = resolved;
      // have TypelessMemberList::pack remove entry from TypelessMember table
      u.setNull();
    }
  }
  s_TypelessProps.pack();
}
#endif // 0

const Property& OClass::getProperty(const string &name) const noexcept {
  const OClass* p = this;
  while (p->baseClass_) {
    const Property& property = p->getThisProperty(name);
    if (property.valid()) return property;
    p = p->baseClass_;
  }
  return Property::null();
}

std::tuple<OClass*, OClass*, int, ushort>
OClass::resolveProperty(const string &name) const noexcept {
  const OClass* p = this;
  while (p->baseClass_) {
    const auto ofs = p->getThisPropOffset(name);
    if (ofs != -1)
      return {
          const_cast<OClass*>(p),
          p->props_[ofs].type,
          p->baseClass_->getInstanceSize() + ofs,
          p->props_.protection(ofs)
      };
    p = p->baseClass_;
  }
  return {nullptr, nullptr, -1, 0};
}

//::::::::::::::::::::::::::   Method Management   ::::::::::::::::::::::::::://

bool OClass::addScriptMethod(Session* m, const string& methodName, int blockId,
                                         ushort protection) {
  _ENTER_NOCHK("OClass::addScriptMethod");
  // Parse method name and arg names
  ProtoParser proto(methodName);
  if (!proto.parse())
    throw proto.errorMessage();
  // Add method
  if (!methods_.addMethod(proto.getMethodName(), blockId, protection)) {
    // Do not allow re-introducton of the same method
    if (!m->warnOnDuplicateMember()) return false;
    const string msg = "Class '"+name+"': duplicate "
        "definition for method '"+methodName+"' was neglected.";
    m->displayMessage("Note", msg);
    return true;
  }
  if (methods_.back().name.empty())
    throw string{"incorrect prototype key"};
  // Set argument names
  vector<string> argNames = std::move(proto.getArgNames());
  m->setMethodArgNames(blockId, argNames);
  _RETHROW_HERE_RETURN(false);
  // Special treatment for the copy, ctor and dtor methods
  cacheMethodAddress(methods_.back(), protection);
  return true;
}

bool OClass::setScriptMethodAddress(const string& methodName,
                                    ushort accessibility, int block) {
  Method &method = methods_.getItem(methodName);
  if (!method.valid()) return false;
  method.block = block;
  // Special treatment for copy, ctor and dtor
  cacheMethodAddress(method, accessibility);
  return true;
}

// Special treatment for the copy, ctor and dtor methods
void OClass::cacheMethodAddress(const Method& method, ushort accessibility) {
  if (method.name == S_constructor) {
    constructor_.scriptMethod = true;
    constructor_.scriptMethodAddress.block = method.block;
    access_[0] = accessibility;
    flags_ |= CLSF_HAS_CONSTRUCTOR;
  }
  else if (method.name == S_destructor) {
    destructor_.scriptMethod = true;
    destructor_.scriptMethodAddress.block = method.block;
    access_[1] = accessibility;
    flags_ |= CLSF_HAS_DESTRUCTOR;
  }
  else if (method.name == S_copy) {
    copyOperator_.scriptMethod = true;
    copyOperator_.scriptMethodAddress.block = method.block;
    access_[2] = accessibility;
    flags_ |= CLSF_HAS_COPY_METHOD;
  }
}

void OClass::cacheCoreMethodAddressCopy() noexcept {
  OAddr a;
  OCoreMethodInfo* methodInfo = getCoreMethodAddress(S_copy, &a);
  if (!methodInfo) return;
  copyOperator_.scriptMethod = false;
  copyOperator_.coreMethodAddress = a.coreMethodAddress;
  access_[2] = methodInfo->protect;
  flags_ |= CLSF_HAS_COPY_METHOD;
}

bool OClass::getMethodAddressByName(const string& methodName,
                                    OAddr* a, void* pv) const {
  union _tag_MethodResolveData {
    ScriptMethodResolveData data;
    OCoreMethodInfo* rec;
  } param, *pparam = &param;

  if (pv != nullptr) pparam = static_cast<_tag_MethodResolveData*>(pv);
  pparam->data.ofs = getScriptMethodAddress(methodName, a, &pparam->data.cls);

  return pparam->data.ofs != -1 ? true :
      (pparam->rec = getCoreMethodAddress(methodName, a)) != nullptr;
}

int OClass::getScriptMethodAddress(const string& s, OAddr* a, OClass** pp) const {
  const OClass* p = this;
  const OClass* const rootClass = getRootClass();
  int ofs = -1;
  // Stop at root class because root classes have no script methods
  while (p != rootClass && (ofs = p->getMethodOffset(s)) == -1)
    p = p->getBaseClass();
  if (ofs != -1) {
    const Method& method = p->methods_[ofs];
    a->scriptMethod = true;
    a->scriptMethodAddress.block = method.block;
    *pp = const_cast<OClass*>(p);
  }
  else {
    *pp = nullptr;
  }
  return ofs;
}

OCoreMethodInfo* OClass::getCoreMethodAddress(const string& name, OAddr* a) const {
  // Script-derived classes do not introduce core (C coded) methods,
  // so we start from the root class (C coded).
  oxCInheritanceData *p = getRootClass()->getCInheritanceData();

  while (p != nullptr) {
    for (OCoreMethodInfo *q = p->table; q->name != nullptr; ++q) {
      if (name == q->name) {
        if (a != nullptr) {
          a->scriptMethod = false;
          a->coreMethodAddress = q->f;
        }
        return q;
      }
    }
    p = p->base;
  }
  return nullptr;
}

SAddr OClass::getNewScriptMethodAddress(const string& methodName,
                                        ushort* pAccess) const {
  const auto methodIndex = methods_.getOffset(methodName);
  if (methodIndex == -1) return SAddr{-1};
  if (pAccess) *pAccess = methods_.protection(methodIndex);
  return SAddr{methods_[methodIndex].block};
}

const char* OClass::getNewCoreMethodName(const CoreMethodAddress& f) const {
  for (auto entry = getCInheritanceData()->table; entry->name; ++entry)
    if (entry->f == f) return entry->name;
  return nullptr;
}

//::::::::::::::::::::::::::::   Accessibility   ::::::::::::::::::::::::::::://

std::pair<const Property&, ushort>
OClass::getPropertyAccess(const std::string &name) const noexcept {
  OClass* p = const_cast<OClass*>(this);
  while (p->baseClass_) {
    const auto ofs = p->getThisPropOffset(name);
    if (ofs != -1) return {props_[ofs], props_.protection(ofs)};
    p = p->baseClass_;
  }
  return {Property::null(), 0};
}

std::pair<bool, OMethodAddress>
OClass::getMethodAccessAddress(const string& methodName,
                               const OClass* caller) const {
  union {
    ScriptMethodResolveData data;
    OCoreMethodInfo* rec;
  } method;
  OMethodAddress a;
  if (!getMethodAddressByName(methodName, &a, static_cast<void*>(&method)))
    return {false, a};
  const auto access = a.scriptMethod ?
      method.data.cls->methods_.protection(method.data.ofs) :
      g_accessibility(method.rec->protect);
  return {isMemberAccessible(access, caller), a};
}

bool OClass::isMethodAccessible(const string& methodName, OClass* caller) const
{ return getMethodAccessAddress(methodName, caller).first; }

bool OClass::isPropertyAccessible(const string& propName,
                                  OClass* caller) const {
  const auto propAccess = getPropertyAccess(propName);
  if (!propAccess.first.valid()) return true; // Accessor diagnoses the error
  return isMemberAccessible(propAccess.second, caller);
}

bool OClass::isMemberAccessible(ushort accessibility,
                                const OClass* caller) const noexcept {
  if (g_public(accessibility)) return true;
  if (!caller) return false; // only public members accessible here
  if (g_protected(accessibility)) return caller->extends(this);
  if (g_private(accessibility)) return caller == this;
  return false;
}

//:::::::::::::::::::::::::   Instance Management   :::::::::::::::::::::::::://

oxCInheritanceData *OClass::getCInheritanceData() const
{ return OInstance::getCInheritanceData(); }

size_t OClass::getAllocSize() const
{ return sizeof(OInstance); }

size_t OClass::getInstanceSize() const noexcept {
  const OClass* p = this;
  size_t size = 0;
  while (!p->isCoreClass()) {
    size += p->props_.size();
    p = p->baseClass_;
  }
  return size;
}

bool OClass::extends(const OClass* base) const {
  if (rootClass_ != base->rootClass_) return false;
  if (base == this) return true;
  for (const OClass* c = baseClass_; c != nullptr; c = c->baseClass_)
    if (base == c) return true;
  return false;
}

bool OClass::recursivelyExtends(const OClass* c) const noexcept {
  if (isCoreClass()) return false;
  if (!c) {
    c = this;
    if (baseClass_->extends(c)) return true;
  }
  else if (this == c || extends(c)) {
    // if a base class extends c then this extends c - no need to check them
    return true;
  }
  for (const OClass* p = this; !p->isCoreClass(); p = p->baseClass_) {
    for (size_t i = 0; i < p->props_.size(); ++i) {
      const auto& prop = p->props_[i];
      if (p->isPropReference(i) || /*template*/!prop.type) continue;
      if (prop.type->recursivelyExtends(c)) return true;
    }
  }
  return false;
}

bool OClass::introduces(const string& methodName) const {
  // check script methods
  if (methods_.getItem(methodName).valid()) return true;
  // script-coded classes introduce no c-methods and c-coded (base) classes
  // have all their methods newly introduced
  return isCoreClass() ? getCoreMethodAddress(methodName, nullptr) != nullptr
                       : false;
}

bool OClass::respondsTo(const string& methodName) {
  OMethodAddress a;
  return getMethodAddressByName(methodName, &a);
}

bool OClass::canCopy(ORef r) const
{ return r->getClass()->extends(this); }

const OClass* OClass::s_getRootClass(const std::string& className, Session* m) {
  OClass* c = m->classMgr->getClass(className);
  return c == nullptr ? nullptr : c->getRootClass();
}

const OClass* OClass::findRootClass() const {
  const OClass* c = this;
  while (!c->isCoreClass()) c = c->baseClass_;
  return c;
}

//:::::::::::::::::::::::::::::   Requirements   ::::::::::::::::::::::::::::://

bool OClass::importRequirement(Requirement* req) noexcept {
  if (req_ || !req)
    return false;
  req_ = req;
  return true;
}

//::::::::::::::::::::::::   Instance Construction   ::::::::::::::::::::::::://

/** Check if class is ready to create instances */
bool s_createObjectCheckClass(OClass* c, Session* m) {
  const bool cannotCreate = c->isMutable() ? false : c->isOpen();
  if (c->isTemplate() || cannotCreate || c->hasTypelessProps()) {
    const string reason =
        c->isMutable()  ? "mutable classes cannot create objects" : (
        c->isOpen()     ? "the class is still open" : (
        c->isTemplate() ? "template classes cannot directly create objects" :
                          "the type of a property is undefined"));
    m->throwBadClass(
    /* method      */ "OClass::createObject",
    /* description */ "could not create instance of class '"+c->name+"'",
    /* context     */ "constructor",
    /* reason      */ reason,
    /* file, line  */ __FILE__, __LINE__);
    return false;
  }
  return true;
}

// Allocate space, format object and construct properties
OInstance* OClass::createObject(bool forceNoCache, Session* m) {
  if (!s_createObjectCheckClass(this, m))
    return nullptr;
  // Construct core (C++) object
  m->forceNoCache = forceNoCache;
  OInstance* o = constructCore(m);
  m->forceNoCache = false;
  if (!o) {
    m->throwBadClass(
    /* method      */ "OClass::createObject",
    /* description */ "could not create instance of class '"+name+"'",
    /* context     */ "constructor",
    /* reason      */ "core constructor failure",
    /* file, line  */ __FILE__, __LINE__);
    return nullptr;
  }
  // increase number of objects this type if usage stats are available
  if (!isMutable())
    m->increaseUsage(this);
  // construct soft (OM) object
  OInstance* result = constructScriptedPart(o, m);
  if (!result) {
    // TODO destroy partially constructed object
    // For the time being we destroy its core, and pass the destruction
    // of its property objects to the run-time manager
    destructCore(o, m);
    if (m->exceptionPending())
      m->rethrow("OClass::createObject");
    else
      m->throwBadClass(
      /* method      */ "OClass::createObject",
      /* description */ "could not create instance of class '"+name+"'",
      /* context     */ "constructor",
      /* reason      */ "script constructor failure",
      /* file, line  */ __FILE__, __LINE__);
    return nullptr;
  }
  // register object id
  m->registerObject(o);
  // increase mutable instance counter
  if (o->getClass()->isMutable())
    ++o->getClass()->dedicatedUses_;
  return result;
}

// construct soft (script defined) object
ORef OClass::constructScriptedPart(ORef o, Session* m) {
  return hasConstructor() ? applyConstructor(o, m) : defaultConstructor(o, m);
}

ORef OClass::defaultConstructor(ORef o, Session* m) {
  // base (core) classes have no properties
  if (isCoreClass()) return o;

  // if we construct an extension of the runtime object m is null
  if (!m) {
    if (o->getRootTypeId() != CLSID_RTMGR) return nullptr;
    m = _PRTMgr(o);
  }

  // let base class construct its part
  if (baseClass_->constructScriptedPart(o, m) == nullptr) return nullptr;

  // construct new (introduced by this class) non-ref props
  for (size_t i = 0; i < props_.size(); ++i) {
    const auto& prop = props_[i];
    _ASSERT(!isPropTemplate(i), m);
    _ASSERT(isPropReference(i) || !prop.type->recursivelyExtends(this), m);
    ushort usage = isPropConst(i) ? USG_CONST : 0;
    ORef result = nullptr;
    if (isPropReference(i)) {
      usage |= USG_REF;
      result = m->nullObject();
    }
    else {
      result = prop.type->createObject(m);
      if (result == nullptr)
        return m->rethrow("OClass::defaultConstructor");
    }
    o->props_.emplace_back(result, usage);
  }
  return o;
}

ORef s_ctorThrowBadClass(Session* m, const string& typeName,
                                     const string& reason) {
  m->throwBadClass(
  /* method      */ "OClass::defaultConstructor",
  /* description */ "could not apply default constructor of class '"+typeName+"'",
  /* context     */ "constructor",
  /* reason      */ reason,
  /* file, line  */ __FILE__, __LINE__);
  return nullptr;
}

ORef OClass::applyConstructor(ORef self, Session* m) {
  // base class constructor
  if (isCoreClass()) return self;
  // if we construct an extension of the runtime object m is null
  if (!m) {
    if (self->getRootTypeId() != CLSID_RTMGR) return nullptr;
    m = _PRTMgr(self);
  }
  // let base class construct its part
  if (!baseClass_->constructScriptedPart(self, m)) return nullptr;
  // now apply user supplied constructor
  OVar rec{self};
  m->execMethodAddress(constructor_, "constructor", 0, &rec, 0, nullptr);
  return self;
}

OInstance* OClass::constructCore(Session* m) {
  OInstance* const o = getObjectFromCache<OInstance>(m);
  return o ? o : new OInstance{this};
}

static ORef s_createInitObjectBadClass(Session* m,
                                       const string& name,
                                       const string& reason,
                                       const string& file, int line) {
  m->throwBadClass(
  /* method      */ "OClass::createInitObject",
  /* description */ "could not create instance type '"+name+"'",
  /* context     */ "constructor",
  /* reason      */ reason,
  /* file, line  */ file, line);
  return nullptr;
}

OInstance* OClass::createInitObject(bool forceNoCache,
    Session* m, int argc, OArg* argv) {
  // check if class is ready to create instances
  const bool cannotCreate = isMutable() ? false : (flags_ & CLSF_STATUS_OPEN);
  if (cannotCreate || (flags_ & CLSF_STATUS_TYPELESS_PROPS)) {
    const string reason = (flags_ & CLSF_STATUS_OPEN) ?
        "the class is still open" :
        "a property's type is undefined";
    return s_createInitObjectBadClass(m, name, reason, __FILE__, __LINE__);
  }
  // check if we can move source object
  const bool sameType = this == _ARG(0)->getClass();
  if (argc == 1 && sameType) {
    if (argv[0]->disposable()) {
      ORef o = _ARG(0);
      *argv[0] = OVar{};
      return o;
    }
    if (argv[0]->movable()) {
      ORef o = _ARG(0)->getClass()->createObject(m);
      _RETHROW_RETURN(nullptr, "OClass::createInitObject");
      o->swapContent(_ARG(0));
      return o;
    }
  }
  // construct core (C++) object
  m->forceNoCache = forceNoCache;
  OInstance* o = constructCore(m);
  m->forceNoCache = false;
  if (!o) {
    const string reason{"core constructor failure"};
    return s_createInitObjectBadClass(m, name, reason, __FILE__, __LINE__);
  }
  // increase number of objects this type if usage stats are available
  if (!isMutable())
    m->increaseUsage(this);
  // construct uninitialized OM object
  o->props_.reserve(getInstanceSize());
  // initialize OM object
  ORef result = constructInitScriptedPart(o, argc, argv, 0, m);
  if (!result) {
    // TODO destroy partially constructed object
    // For the time being we destroy its core, and pass the destruction
    // of its property objects to the run-time manager
    destructCore(o, m);
    if (m->exceptionPending()) {
      m->rethrow("OClass::createInitObject");
      return nullptr;
    }
    else {
      const string reason{"script constructor failure"};
      return s_createInitObjectBadClass(m, name, reason, __FILE__, __LINE__);
    }
  }
  // register object id
  m->registerObject(o);
  return o;
}

ORef OClass::constructInitScriptedPart(ORef o, int argc, OArg* argv,
                                       size_t base, Session* m) {
  _ENTER_NOCHK("OClass::constructInitScriptedPart");

  // base (core) classes have no properties
  if (isCoreClass()) {
    if (base >= argc)
      return o;
    if (base < argc - 1) {
      const string methodName = getClassName()+"::core object initializer";
      m->throwBadArgc(__PRETTY_FUNCTION__, methodName, "too many initializers");
      return nullptr;
    }
    m->setValueWithCast(this, o, argv[base], true, true);
    _RETHROW_HERE_RETURN(nullptr);
    return o;
  }

  // let base class construct its part
  const size_t newBase = base + props_.size();
  if (!baseClass_->constructInitScriptedPart(o, argc, argv, newBase, m))
    return nullptr;

  // construct new (introduced by this class), non-ref, non RCE-properties
  for (size_t i = 0; i < props_.size(); ++i) {
    const auto& prop = props_[i];
    _ASSERT(!isPropTemplate(i), m);
    _ASSERT(isPropReference(i) || !prop.type->recursivelyExtends(this), m);
    ushort usage = isPropConst(i) ? USG_CONST : 0;
    o->props_.emplace_back(ORef{nullptr}, usage);
    const bool argPresent = (base + i < argc) && argv[base + i];
    OVar& lhs = o->props_.back();
    OVar& rhs = argPresent ? *argv[base + i] : lhs;
    if (isPropReference(i)) {
      lhs.usage |= USG_REF;
      if (argPresent && !rhs.undefined()) {
        REF(lhs) = REF(rhs);
        lhs.usage |= USG_CONST;
      }
      else
        REF(lhs) = m->nullObject();
    }
    else if (bool sameType = true;
             !argPresent || !(sameType = REF(rhs)->class_ == prop.type)) {
      _ASSERT(!!argPresent || sameType, m);
      REF(lhs) = prop.type->createObject(m);
      if (!REF(lhs) || m->exceptionPending())
        return m->rethrow(_ox_method_name, __FILE__, __LINE__);
      if (!sameType && REF(rhs)->getClassId() != CLSID_NULLOBJ) {
        m->copyFrom(&lhs, &rhs);
        if (m->exceptionPending()) {
          ObjectAutoCleaner clean{m, REF(lhs)};
          return m->rethrow(_ox_method_name, __FILE__, __LINE__);
        }
      }
    }
    else
      m->emplace(lhs, rhs);
  }
  return o;
}

//:::::::::::::::::::::::::   Instance Destruction   ::::::::::::::::::::::::://

void OClass::deleteObject(OInstance* o, Session* m) {
  OClass* const type = o->getClass();
  // destroy properties
  deleteScriptedPart(o, m);
  // remove from list
  m->deregisterObject(o);
  // destroy C++ object
  destructCore(o, m);
  // decrease number of objects this type if usage stats are available
  _ASSERT(m->checkUsageData(this), m);
  if (!type->isMutable())
    m->reduceUsage(this);
  // check for errors
  if (m->exceptionPending())
    m->rethrow("OClass::deleteObject");
  _ASSERT(!type->isMutable() || type->dedicatedUses_ > 0, m);
  if (type->isMutable() && --type->dedicatedUses_ == 0)
    m->deleteMutableClass(type);
}

ORef OClass::deleteScriptedPart(OInstance* o, Session* m) {
  if (hasDestructor()) // invoke user defined destructor
    return applyDestructor(o, m);
  // OMS-system supplied destructor - destroys non-recursively defined props
  return defaultDestructor(o, m);
}

ORef OClass::defaultDestructor(ORef o, Session* m) {
  // destroy new (introduced by this class) non-template properties
  deleteProperties(o, m);
  // let base class destroy its part
  if (!isCoreClass())
    return baseClass_->deleteScriptedPart(o, m);
  // return object ptr
  return o;
}

ORef OClass::deleteProperties(OInstance* self, Session* m) {
  _DEBUG( const size_t parentSize = self->props_.size() - props_.size(); )

  ObjectAutoCleaner cleaner{m};
  for (auto it = props_.rbegin(); it != props_.rend(); ++it) {
    const auto i = std::distance(it, props_.rend()) - 1;

    _ASSERT(isPropReference(i) == self->props_[parentSize + i].isRef(), m);

    if (isPropReference(i)) {
      OVar& propVar = self->props_.back();
      _ASSERT(!propVar.undefined(), m);
      if (propVar.disposable() && BTI::isBasicTypeProxy(REF(propVar)))
        REF(propVar)->deleteObject(m);
      self->props_.pop_back();
      continue;
    }

    ORef &o = REF(self->props_.back());
    _ASSERT(o == REF(self->props_.at(parentSize + i)), m);

    if (o == nullptr) {
      self->props_.pop_back();
      continue;
    }

    _ASSERT(o->getClass() == it->type, m);
    _ASSERT(!o->getClass()->recursivelyExtends(this), m);

    cleaner.add(o);
    self->props_.pop_back();
  }

  cleaner.cleanup();
  if (m->exceptionPending())
    m->rethrow("OClass::deleteProperties");

  return self;
}

ORef OClass::applyDestructor(ORef self, Session* m) {
  // Destruct this object
  OVar rec{self};
  ORet r = m->execMethodAddress(destructor_, "destructor", 0, &rec, 0, nullptr);
  // Invoke base class destructor
  if (!isCoreClass())
    return baseClass_->deleteScriptedPart(self, m);
  return r.objectRef;
}

void OClass::destructCore(OInstance* self, Session* m) {
  if (!freeCached<OInstance>(self))
    delete self; // Destroy object by calling object's destructor
}

//:::::::::::::::::::::::::::::   Copy Method   :::::::::::::::::::::::::::::://

// If copyObject was called from OInstance::copyObject, then it is
// guaranteed that the following asserions hold true:
// this == self->class_ and this == r->class
// Otherwise, you cannot assume that
// this == self->class_
// For example, see defaultCopy.
//
// When this method is called from Session::extendedCopy, which is
// the case with scripts, rhsExtendsLhs is always true, and the
// following else-if block is never executed.
ORet OClass::copyObject(ORef self, const_ORef r, Session* m) {
  _ENTER_NOCHK("OClass::copyObject");
  _ASSERT(r && r->getClass() && self->getClass(), m);

  const bool rhsExtendsLhs = r->class_->extends(self->class_);
  if (rhsExtendsLhs) {
    if (isCoreClass()) {
      ORef result = copyCore(self, r, m);
      _RETHROW_HERE();
      if (result) _RETURN(result);
      m->throwBadCoreCopy(_ox_method_name,
          "unable to copy core of object type '"+r->getClassName()+"' "
          "to core of object type '"+self->getClassName()+"'",
          __FILE__, __LINE__);
      return OVariable::undefinedResult();
    }
    if (!hasCopyMethod()) {
      ORet rv = defaultCopy(self, r, m);
      _RETHROW_HERE();
      return rv;
    }
  }
  else if (!hasCopyMethod()) {
    m->throwBadArg(_ox_method_name,
        "unexpected argument type '"+r->getClassName()+"'");
    return OVariable::undefinedResult();
  }

  OVar arg{const_cast<ORef>(r)};
  OArg argv[]{&arg};
  const ulong flags = 0;
  OVar rec{self};
  ORet rv = m->execMethodAddress(copyOperator_, S_copy, flags, &rec, 1, argv);
  _RETHROW_HERE();
  return rv;
}

ORet OClass::defaultCopy(ORef self, const_ORef r, Session* m) {
  _ENTER_NOCHK("OClass::defaultCopy");
  _ASSERT(!isCoreClass(), m);

  const ORet result = baseClass_->copyObject(self, r, m);
  _RETHROW_HERE_IF(result.undefined());
  copyProperties(self->props_, r->props_, m);
  _RETHROW_HERE();

  return self->makeResult();
}

void OClass::copyProperties(vector<OVar>& lhs,
                            const vector<OVar>& rhs,
                            Session* m) {
  const size_t b = baseClass_->getInstanceSize();
  for (size_t at = 0; at < props_.size(); ++at) {
    copyProperty(props_[at], props_.protection(at),
                 REF(lhs[b + at]), REF(rhs[b + at]), m);
    _RETHROW_NORET("OClass::copyProperties");
  }
}

void OClass::copyProperty(const Property& prop,
                          ushort accessibility,
                          ORef& lhs,
                          const ORef& rhs,
                          Session* m) {
  _ENTER_NOCHK("OClass::copyProperty");
  _ASSERT(!prop.isTemplate(accessibility), m);
  _ASSERT(g_ref(accessibility) || !prop.type->recursivelyExtends(this), m);
  _ASSERT(g_ref(accessibility) || rhs->getClass() == prop.type, m);
  _ASSERT(g_ref(accessibility) || (lhs && lhs->getClass() == prop.type), m);

  if (g_ref(accessibility))
    lhs = rhs;
  else
    prop.type->copyObject(lhs, rhs, m);
  _RETHROW_HERE_NORET();
}

//:::::::::::::::::::::::::::::   Move Method   :::::::::::::::::::::::::::::://

// See comments preceding copyObject
ORet OClass::moveObject(ORef lhs, ORef rhs, Session* m) {
  _ENTER_NOCHK("OClass::moveObject");

  if (rhs->getClass() != lhs->getClass() || this != lhs->getClass()) {
    const string lhsName = lhs->getClassName();
    const string msg = (this != lhs->getClass() ?
        "the class '"+getClassName()+"' cannot be used to move objects type '" :
        "cannot move object type '"+rhs->getClassName()+"' to '")+lhsName+"'";
    _THROW_BAD_USAGE(msg);
  }

  if (!moveCore(lhs, rhs))
    _THROW_BAD_USAGE("cannot move core of object type '"+
        rhs->getClassName()+"' to '"+lhs->getClassName()+"'")

  std::swap(lhs->props_, rhs->props_);

  _RETURN(lhs);
}

//::::::::::::::::::::::::   Object Cache Creation   ::::::::::::::::::::::::://

void OClass::createObjectCache(const ObjectUsage& configData) {
  // check if core class has overriden this method
  if (getRootType() != BTI::getTypeClass<ORef>()) {
    const string msg = "class '"+getRootType()->name+"' did not override "
                       "OClass::createObjectCache()";
    throw std::logic_error{msg};
  }
  // check if already constructed
  if (objectCache_)
    throw std::logic_error{"cache already constructed for objects type "+name};
  if (const size_t n = configData.getCacheSize(); n != 0)
    objectCache_ = std::make_unique<ObjectCache<OInstance>>(n, this);
}

//:::::::::::::::::::::::::::::   Apply Method   ::::::::::::::::::::::::::::://

ORet OClass::applyMethod(const std::string& methodName,
                         OArg self, std::vector<OArg>& args, Session* m) {
  return m->execMethod(methodName, 0, self, args.size(), args.data()).first;
}

ORet OClass::applyMethod(const std::string& methodName,
                         OArg self, int argc, OArg argv[], Session* m) {
  return m->execMethod(methodName, 0, self, argc, argv).first;
}

//:::::::::::::::::::::::::::::  Cast Operators  ::::::::::::::::::::::::::::://

void OClass::s_constCast(OArg* argv)
{ argv[0]->usage &= ~USG_CONST; }

OClass* OClass::reinterpretCast(ORef o) noexcept {
  if (o == nullptr) return nullptr;
  OClass* originalClass = o->class_;
  o->class_ = this;
  return originalClass;
}

//:::::::::::::::::::::::::::::  Class Printer  :::::::::::::::::::::::::::::://

static void s_printCAddr(CAddr *a, StringPrinter sout) {
  constexpr size_t SIZE = sizeof(CAddr)/sizeof(ulong);
  ulong *p = reinterpret_cast<ulong*>(a);
  string result;
  for (size_t i = 0; i < SIZE; ++i) {
    const string converted = ul2hexstr(p[i]);
    result = converted + result;
  }
  auto pos = result.find_first_not_of("0");
  result = (pos == string::npos) ? "0" : result.substr(pos);
  sout("0x"+result);
}

void OClass::print(StringPrinter strOut, bool alsoCMethodTable) {
  // print class info
  strOut("Class ");
  Descriptor::print(strOut);
  strOut("\n");
  strOut("Class id: "+std::to_string(classId_)+"\n");
  strOut("Base class: ");
  if (isCoreClass()) strOut("none\n");
  else strOut(baseClass_->name+", class id: "+
              std::to_string(baseClass_->getClassId())+"\n");
  strOut("Root class: ");
  if (getRootClass() == nullptr) strOut("none\n");
  else strOut(findRootClass()->name+", class id: "+
              std::to_string(getRootClass()->getClassId())+"\n");
  strOut("Instance allocation size: "+std::to_string(getAllocSize())+"\n");
  bool b = false;
  if (hasConstructor()) {
    strOut("Has constructor");
    b = true;
  }
  if (hasDestructor()) {
    strOut(string(b ? "," : "Has")+" destructor");
    b = true;
  }
  if (hasCopyMethod()) {
    strOut(string(b ? " and" : "Has")+" copy method");
    b = true;
  }
  strOut(b ? "\n" : "");

  // members
  strOut("Properties:");
  if (props_.empty()) strOut(" none\n");
  else {strOut("\n"); props_.print(strOut);}

  // C-methods
  if (alsoCMethodTable) {
    strOut("C methods:\n");
    oxCInheritanceData *p = findRootClass()->getCInheritanceData();
    while (p != nullptr) {
      OCoreMethodInfo *q = p->table;
      while (q->name != nullptr) {
        strOut("C, {"+string(q->name)+", ");
        s_printCAddr(&q->f, strOut);
        strOut("}\n");
        ++q;
      }
      p = p->base;
    }
  }

  // script-methods
  strOut("Script methods:");
  if (methods_.empty()) strOut(" none\n");
  else {strOut("\n"); methods_.print(strOut);}
}

#if 0
//:::::::::::::::::::::::::  Event handling support  ::::::::::::::::::::::::://

// addEvent:handler:id:to:param:
bool OClass::addEvent(const char *handler, ORef _type,
                      ORef _id, ORef _to, ORef _param)
{
  if (getEventTable() == nullptr) // Create event table
    eventTable_ = new oxEventTable;

  int i;

  if (_type->getClass()->getClassId() == CLSID_STRING)
  {
    const char *type = _PStr(_type)->c_str();
    i = eventTable_->addEvent(type, handler);
  }
  else // integer
  {
    int type = _PINT(_type)->getValue();
    i = eventTable_->addEvent(type, handler);
  }

  if (i == -1) // handler is nullptr
    return false;

  if (_id == NULLID)
    eventTable_->setId(i, -1);
  else if (_id->getClass()->getClassId() == CLSID_STRING)
    eventTable_->setId(i, _PStr(_id)->c_str());
  else // integer
    eventTable_->setId(i, _PINT(_id)->getValue());

  if (_to == NULLID)
    eventTable_->setLastId(i, -1);
  else if (_to->getClass()->getClassId() == CLSID_STRING)
    eventTable_->setLastId(i, _PStr(_to)->c_str());
  else // integer
    eventTable_->setLastId(i, _PINT(_to)->getValue());

  if (_param == NULLID)
    eventTable_->setParam(i, nullptr, 0);
  else if (_param->getClass()->getClassId() == CLSID_STRING)
    eventTable_->setParam(i, _PStr(_param)->c_str());
  else // integer
  {
//      void *pv = (void *)_PINT(_param)->getValue();
    eventTable_->setParam(i, _param, 0);
  }

  return true;
}

const char *OClass::getResolveName(int entryIndex,
                                   int type)
{
  oxList<ResolveEntry>::Element *prev;
  oxList<ResolveEntry>::Element *pResolveEntry =
    eventTable_ ?
    eventTable_->getResolveEntry(entryIndex, type, &prev)
    : nullptr;
  if (pResolveEntry == nullptr)
    return nullptr;
  const char *resolve = pResolveEntry->data().m_stringId;
  eventTable_->removeResolveInfo(pResolveEntry, prev);
  return resolve;
}

void OClass::resolveEventData(int i, int value, int which)
{
  switch (which)
  {
    case _RESOLVE_EVENT_TYPE:
       eventTable_->setEventType(i, value); break;
    case _RESOLVE_ID:
       eventTable_->setId(i, value); break;
    case _RESOLVE_LAST_ID:
       eventTable_->setLastId(i, value); break;
    default:
       _Message(nullptr, "resolve event data: invalid data "
          "type");
  }
}

void OClass::resolveEventParam(int i, ORef value)
{
  eventTable_->setParam(i, value, 0);
}

#endif
