#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "ostring.hpp"
#include "ovector.hpp"
#include "orange.hpp"
#include "oiter.hpp"
#include "oveciter.hpp"
#include "osetiter.hpp"
#include "orangeBreakLoop.hpp"

using std::string;
using std::vector;
using std::set;

#define _REF(o) (o)->objectRef

#if 0
template<typename t>
requires (std::is_same_v<t, ORef>)
inline void g_deleteObjects(Session* m, t p) {
  p->deleteObject(m);
}
template<typename t, typename... s>
requires (std::is_same_v<t, ORef>)
inline void g_deleteObjects(Session* m, t p, s... q) {
  g_deleteObjects(m, p);
  if (!m->exceptionPending())
    g_deleteObjects(m, q...);
}
#endif // 0

static bool s_cmpValues(OArg lhsVar, OArg rhsVar, Session* m) noexcept {
  OArg argv[]{rhsVar};
  auto result = m->execMethod(S_ne, 0, lhsVar, 1, argv);
  // no need to check result.second
  ObjectAutoCleaner _{m, result.first};
  _CHECK_EXCEPTION_RETURN(false);
  return _P(bool, REF(result.first))->getValue();
}

//============================================================================//
//::::::::::::::::::::::::::   Pipeline Elememts   ::::::::::::::::::::::::::://
//============================================================================//

using pipeline_t = PipelineElementType;

PipelineElement::PipelineElement(Session* m, pipeline_t type) :
    type{type}, m{m}, begin{nullptr}, end{nullptr}, size{0} {}

PipelineElement::PipelineElement(Session* m, pipeline_t type, const OAddr& a) :
    type{type}, m{m}, action{a}, begin{nullptr}, end{nullptr}, size{0} {}

PipelineElement::PipelineElement(Session* m, pipeline_t type, int blockId) :
    type{type}, m{m}, begin{nullptr}, end{nullptr}, size{0}
{ setBlockId(blockId); }

PipelineElement::PipelineElement(Session* m, pipeline_t type, ORef begin) :
    type{type}, m{m}, begin{begin}, end{nullptr}, size{0} {}

PipelineElement::PipelineElement(Session* m, pipeline_t type, ORef b, ORef e) :
    type{type}, m{m}, begin{b}, end{e}, size{0} {}

void PipelineElement::clear() {
  switch (type) {
    case ITERATOR:
    case ITERATOR_PAIR:
    case ITERATOR_COUNT:
    case ITERATOR_SENTRY: clearIterator(); break;
    case IOTA:            clearIota();     break;
    case TAKE:
    case TAKE_WHILE:      clearTake();     break;
    case DROP:
    case DROP_WHILE:      clearDrop();     break;
    case SPLIT:           clearSplit();    break;
    default: {
      ObjectAutoCleaner cleaner{m, {begin, end}};
    }
  }
}

void PipelineElement::clearIterator() {
  _ASSERT(type == ITERATOR || type == ITERATOR_PAIR ||
          type == ITERATOR_COUNT || type == ITERATOR_SENTRY, m);
  // do nothing
}

void PipelineElement::clearIota() {
  _ASSERT(type == IOTA, m);
  ObjectAutoCleaner cleaner{m, {begin, end}};
}

void PipelineElement::clearTake() {
  _ASSERT(type == TAKE || type == TAKE_WHILE, m);
  ObjectAutoCleaner cleaner{m, {nullptr, end}};
}

void PipelineElement::clearDrop() {
  _ASSERT(type == DROP || type == DROP_WHILE, m);
  ObjectAutoCleaner cleaner{m, {begin, nullptr}};
}

void PipelineElement::clearSplit() {
  _ASSERT(type == SPLIT, m);
  ObjectAutoCleaner cleaner{m, {begin, end}};
}

void PipelineElement::prepareLambda() {
  _ASSERT(action.valid() && action.scriptMethod, m);
  vector<string> argNames{"value"};
  m->setMethodArgNames(action.scriptMethodAddress.block, argNames);
}

bool PipelineElement::applySentry(ORange& range,
                                  OVar* value) const noexcept {
  _ENTER_NOCHK("PipelineElement::applySentry");
  _RETHROW_HERE_RETURN(false);
  const ulong flags = 0;
  const auto& a = action.scriptMethodAddress;
  const string method{"pipelineAction:"};
  OArg argv[]{value};
  OVar rec{&range};
  ORet result = m->execScriptMethod(a, method, flags, &rec, 1, argv);
  _RETHROW_HERE_RETURN(false);
  if (REF(result)->getRootClassId() != BTI::getTypeClassId<bool>()) {
    m->throwRTE(_ox_method_name, "sentry should return boolean type");
    return false;
  }
  bool shouldContinue = g_typeValue<bool>(REF(result));
  return shouldContinue;
}

ORet PipelineElement::applyTransform(ORange& range,
                                     OVar* value) const noexcept {
  const ulong flags = 0;
  const auto& a = action.scriptMethodAddress;
  OArg argv[]{value};
  OVar rec{&range};
  return m->execScriptMethod(a, "pipelineAction:", flags, &rec, 1, argv);
}

//============================================================================//
//::::::::::::::::::::::::::::   LoopTerminator   :::::::::::::::::::::::::::://
//============================================================================//

bool LoopTerminator::operator()(ORef it) const noexcept {
  _ENTER_NOCHK("LoopTerminator::operator()");
  Session* const m = pipelineElement.m;
  OVar rhsVar{pipelineElement.end};
  OArg argv[]{&rhsVar};
  auto result = m->execMethod(S_ne, 0, it, 1, argv);
  // no need to check result.second
  _RETHROW_HERE_RETURN(false);
  const bool notEqual = _P(bool, REF(result.first))->getValue();
  if (result.first.disposable()) {
    m->deleteObject(REF(result.first));
    _RETHROW_HERE_RETURN(false);
  }
  return notEqual;
}

bool LoopTerminator::applySentry(ORef value) const noexcept {
  OVar var{value};
  return pipelineElement.applySentry(range, &var);
}

std::function<bool(ORef)> LoopTerminator::getContinue(ORef it) noexcept {
  switch (pipelineElement.type) {
    case ALL: return [this, it](ORef) { return (*this)(it); };
    case ITERATOR: return [](ORef) { return true; };
    case ITERATOR_PAIR: return [this, it](ORef) { return (*this)(it); };
    case ITERATOR_COUNT: return [this](ORef)
             { return range.iterationCounter < pipelineElement.size; };
    case ITERATOR_SENTRY: return [this](ORef value)
             { return applySentry(value); };
  }
  return [](ORef) { return true; };
}

//============================================================================//
//::::::::::::::::::::::::::::::::   ORange   :::::::::::::::::::::::::::::::://
//============================================================================//

ORange::~ORange() {
  for (auto& pipelineElement : pipeline_)
    pipelineElement.clear();
}

void ORange::addPipelineAll(Session* m, ORef begin, ORef end) {
  pipeline_.emplace_back(m, ALL, begin, end);
}

void ORange::run() {
  if (pipeline_.empty()) return;
  PipelineElement& pipelineElement = pipeline_.front();
  _ASSERT(pipelineElement.type < NUM_INIT_PIPELINE_ELEM_TYPES &&
          pipelineElement.type > UNKNOWN, pipelineElement.m);
  pipelineRunner_[pipelineElement.type](this, 0, nullptr);
}

bool ORange::runStage(size_t stage, ORef value) {
  if (stage == pipeline_.size()) return true; // TODO
  PipelineElement& pipelineElement = pipeline_[stage];
  _ASSERT(pipelineElement.type < NUM_PIPELINE_ELEMENT_TYPES &&
          pipelineElement.type >= TAKE, pipelineElement.m);
  return pipelineRunner_[pipelineElement.type](this, stage, value);
}

bool ORange::signalEnd(size_t stage) {
  return runStage(stage, nullptr);
}

//::::::::::::::::::::::::::   Pipeline Callbacks   :::::::::::::::::::::::::://

/*static*/
bool ORange::runUnknown(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("range::runUnknown");
  Session* const m = thisRange->getPipelineElement(stage).m;
  m->throwRTE(_ox_method_name, "unknown pipeline element");
  return false;
}

/*static*/
bool ORange::runInitialLoop(ORange* thisRange, size_t, ORef) {
  const bool handled = thisRange->runNativeLoop();
  if (handled) return true;
  // handle custom type iterators and/or containers
  return thisRange->runGeneralLoop();
}

/*static*/
bool ORange::runIota(ORange* thisRange, size_t, ORef) {
  _ENTER_NOCHK("ORange::runIota");
  auto& pipelineElement = thisRange->pipeline_.front();
  Session* const m = pipelineElement.m;
  OCountIterator* const end = pipelineElement.end ?
      static_cast<OCountIterator*>(pipelineElement.end) : nullptr;
  OCountIterator it{pipelineElement.m->getClass(CLSID_COUNTITER)};
  for (it = *static_cast<OCountIterator*>(pipelineElement.begin);
       end ? it.getCount() < end->getCount() : true;
       it.advance()) {
    OVar rec{&it};
    ORet varValue = it.ommGetValue(&rec, 0, nullptr, m);
    _RETHROW_HERE_RETURN(true);
    ObjectAutoCleaner valueCleaner{m, varValue};
    if (!thisRange->runStage(1, REF(varValue))) break;
    _RETHROW_HERE_RETURN(true);
  }
  _RETHROW_HERE_RETURN(true);
  thisRange->signalEnd(1);
  _RETHROW_HERE_RETURN(true);
  return true;
}

/*static*/
bool ORange::runTake(ORange* thisRange, size_t stage, ORef value) {
  auto& pipelineElement = thisRange->getPipelineElement(stage);
  OCountIterator* countIter = static_cast<OCountIterator*>(pipelineElement.end);
  if (endSignal(value)) {
    countIter->setBegin(0);
    return thisRange->signalEnd(stage + 1);
  }
  if (countIter->getCount() < pipelineElement.size) {
    const bool canContinue = thisRange->runStage(stage + 1, value);
    countIter->advance();
    return canContinue;
  }
  return false;
}

/*static*/
bool ORange::runTakeWhile(ORange* thisRange, size_t stage, ORef value) {
  if (endSignal(value)) return thisRange->signalEnd(stage + 1);
  LoopTerminator loopTerminator{*thisRange, stage};
  if (loopTerminator.applySentry(value))
    return thisRange->runStage(stage + 1, value);
  return false;
}

/*static*/
bool ORange::runDrop(ORange* thisRange, size_t stage, ORef value) {
  auto& pipelineElement = thisRange->getPipelineElement(stage);
  auto* countIter = static_cast<OCountIterator*>(pipelineElement.begin);
  if (endSignal(value)) {
    countIter->setBegin(0);
    return thisRange->signalEnd(stage + 1);
  }
  if (countIter->getCount() < pipelineElement.size) { // discard this item
    countIter->advance();
    return true;
  }
  return thisRange->runStage(stage + 1, value);
}

/*static*/
bool ORange::runDropWhile(ORange* thisRange, size_t stage, ORef value) {
  if (endSignal(value)) return thisRange->signalEnd(stage + 1);
  LoopTerminator loopTerminator{*thisRange, stage};
  if (loopTerminator.applySentry(value))
    return true;
  return thisRange->runStage(stage + 1, value);
}

/*static*/
bool ORange::runFilter(ORange* thisRange, size_t stage, ORef value) {
  if (endSignal(value)) return thisRange->signalEnd(stage + 1);
  LoopTerminator loopTerminator{*thisRange, stage};
  if (loopTerminator.applySentry(value))
    return thisRange->runStage(stage + 1, value);
  return true;
}

/*static*/
bool ORange::runTransform(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("ORange::runTransform");
  const auto& pipelineElement = thisRange->getPipelineElement(stage);
  if (pipelineElement.requiresNotification())
    return thisRange->runProcess(thisRange, stage, value);
  if (endSignal(value)) return thisRange->signalEnd(stage + 1);
  Session* const m = pipelineElement.m;
  OVar var{value};
  ORet result = pipelineElement.applyTransform(*thisRange, &var);
  _RETHROW_HERE_RETURN(false);
  value = REF(result);
  ObjectAutoCleaner valueCleaner{m, result};
  return thisRange->runStage(stage + 1, value);
}

/*static*/
bool ORange::runProcess(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("ORange::runProcess");
  const auto& pipelineElement = thisRange->getPipelineElement(stage);
  Session* const m = pipelineElement.m;
  if (endSignal(value)) {
    ORef nullValue = m->getClass(CLSID_NULLOBJ)->createObject(m);
    ObjectAutoCleaner cleaner{m, nullValue};
    _RETHROW_HERE_RETURN(false);
    OVar nullVar{nullValue};
    ORet result = pipelineElement.applyTransform(*thisRange, &nullVar);
    cleaner.addDisposable(result);
    _RETHROW_HERE_RETURN(false);
    if (REF(result)->getClassId() != CLSID_NULLOBJ) {
      // more data exist
      thisRange->runStage(stage + 1, REF(result));
      _RETHROW_HERE_RETURN(false);
    }
    return thisRange->signalEnd(stage + 1);
  }
  OVar var{value};
  ORet result = pipelineElement.applyTransform(*thisRange, &var);
  _RETHROW_HERE_RETURN(false);
  value = REF(result);
  ObjectAutoCleaner valueCleaner{m, result};
  if (REF(result)->getClassId() == CLSID_NULLOBJ)
    return true; // request more data
  // proceed to next stage
  return thisRange->runStage(stage + 1, value);
}

/*static*/
bool ORange::runElements(ORange* thisRange, size_t stage, ORef value) {
  return thisRange->runKeysOrValues(2, stage, value);
}

/*static*/
bool ORange::runKeys(ORange* thisRange, size_t stage, ORef value) {
  return thisRange->runKeysOrValues(0, stage, value);
}

/*static*/
bool ORange::runValues(ORange* thisRange, size_t stage, ORef value) {
  return thisRange->runKeysOrValues(1, stage, value);
}

bool ORange::runKeysOrValues(size_t which, size_t stage, ORef value) {
  if (endSignal(value)) return signalEnd(stage + 1);

  static const string method[3] {"key", "value", ""};
  static const char* methodName[3] {"ORange::runKeys",
                                    "ORange::runValues",
                                    "ORange::runElements"};
  const char* _ox_method_name = methodName[which];
  const auto& pipelineElement = pipeline_[stage];
  Session* const m = pipelineElement.m;
  ORet result;
  OMethodAddress a;
  OVar rec{value};
  if (method[which].empty() ||
      !value->getClass()->getMethodAddressByName(method[which], &a)) {
    if (!value->getClass()->getMethodAddressByName(S_component, &a)) {
      const string msg = method[which].empty() ?
          "method 'component:' (operator[]) is required" :
          "method '"+method[which]+"' or 'component:' (operator[]) is required";
      m->throwRTE(_ox_method_name, msg);
      return false;
    }
    oxType<size_t> index{BTI::getTypeClass<size_t>()};
    index.setValue(which == 2 ? pipelineElement.size : which);
    OVar var{&index};
    OArg argv[]{&var};
    result = m->execMethodAddress(a, S_component, 0, &rec, 1, argv);
  }
  else
    result = m->execMethodAddress(a, methodName[which], 0, &rec, 0, nullptr);
  _RETHROW_HERE_RETURN(false);
  value = REF(result);
  ObjectAutoCleaner valueCleaner{m, result};
  return runStage(stage + 1, value);
}

/*static*/
bool ORange::runMerge(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("ORange::runMerge");
  Session* const m = thisRange->getPipelineElement(stage).m;
  const bool canContinue = thisRange->runMerger(stage, value);
  _RETHROW_HERE_RETURN(false);
  return canContinue;
}

/*static*/
bool ORange::runSplit(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("ORange::runSplit");
  Session* const m = thisRange->getPipelineElement(stage).m;
  const bool canContinue = thisRange->runSplitter(stage, value);
  _RETHROW_HERE_RETURN(false);
//   if (endSignal(value)) return /*immaterial*/true; // already handled
//   return canContinue ? thisRange->runStage(stage + 1, value) : false;
  return canContinue;
}

/*static*/
bool ORange::runReverse(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("range::runReverse");
  Session* const m = thisRange->getPipelineElement(stage).m;
  m->throwRTE(_ox_method_name, "the reverse feature is not available yet");
  return false;
}

/*static*/
bool ORange::runJoin(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("range::runJoin");
  Session* const m = thisRange->getPipelineElement(stage).m;
  m->throwRTE(_ox_method_name, "the join feature is not available yet");
  return false;
}

/*static*/
bool ORange::runExecute(ORange* thisRange, size_t stage, ORef value) {
  _ENTER_NOCHK("ORange::runExecute");
  auto& pipelineElement = thisRange->getPipelineElement(stage);
  Session* const m = pipelineElement.m;
  OVar var{value};
  ObjectAutoCleaner varCleaner{m};
  if (endSignal(value)) {
    if (!pipelineElement.size) /* ignore it */ return true;
    REF(var) = m->getClass(CLSID_NULLOBJ)->createObject(m);
    varCleaner.add(REF(var));
    _RETHROW_HERE_RETURN(false);
  }
  ORet result = pipelineElement.applyTransform(*thisRange, &var);
  ObjectAutoCleaner valueCleaner{m, result};
  _RETHROW_HERE_RETURN(false);
  return true;
}

//::::::::::::::::::   Initial Pipeline Element Utilities   :::::::::::::::::://

bool ORange::runNativeLoop() noexcept {
  const auto iterTypeId = pipeline_.front().begin->getClassId();
  switch (iterTypeId) {
    case CLSID_ULONGVECITER: return nativeLoop<ulong,  OVectorIterator<ulong>>();
    case CLSID_LONGVECITER:  return nativeLoop<long,   OVectorIterator<long>>();
    case CLSID_UINTVECITER:  return nativeLoop<uint,   OVectorIterator<uint>>();
    case CLSID_INTVECITER:   return nativeLoop<int,    OVectorIterator<int>>();
    case CLSID_SHORTVECITER: return nativeLoop<short,  OVectorIterator<short>>();
    case CLSID_CHARVECITER:  return nativeLoop<char,   OVectorIterator<char>>();
    case CLSID_DBLVECITER:   return nativeLoop<double, OVectorIterator<double>>();
    case CLSID_FLTVECITER:   return nativeLoop<float,  OVectorIterator<float>>();
    case CLSID_BOOLVECITER:  return nativeLoop<bool,   OVectorIterator<bool>>();
    case CLSID_OBJVECITER:   return nativeLoop<ORef,   OVectorIterator<ORef>>();
    case CLSID_ULONGSETITER: return nativeLoop<ulong,  OSetIterator<ulong>>();
    case CLSID_LONGSETITER:  return nativeLoop<long,   OSetIterator<long>>();
    case CLSID_DBLSETITER:   return nativeLoop<double, OSetIterator<double>>();
    case CLSID_STRSETITER:   return nativeLoop<string, OSetIterator<string>>();
    case CLSID_OBJSETITER:   return nativeLoop<ORef,   OSetIterator<ORef>>();
  }
  return false;
}

template<typename t, typename _It>
requires (!std::is_same_v<t, ORef>)
static ORet _getValue(_It& oiter, Session* m) {
  OVar rec{&oiter};
  ORet varValue = oiter.ommGetValue(&rec, 0, nullptr, m);
  if (m->exceptionPending()) return OVar::undefinedResult();
  if (REF(varValue)->isTypeProxy()) {
    ORef value = g_convertProxy<t>(REF(varValue), m);
    if (!m->exceptionPending() && varValue.disposable())
      m->deleteObject(REF(varValue));
    return value->disposableResult();
  }
  return varValue;
}

template<typename t, typename _It>
requires (std::is_same_v<t, ORef>)
static inline ORet _getValue(_It& oiter, Session* m) {
  OVar rec{&oiter};
  return oiter.ommGetValue(&rec, 0, nullptr, m);
}

template<typename t, typename _It>
requires (!std::is_same_v<t, ORef> && std::is_same_v<_It, OVectorIterator<t>>)
static inline OClass* _getContainerClass() { return BTI::getVectorClass<t>(); }

template<typename t, typename _It>
requires (!std::is_same_v<t, ORef> && std::is_same_v<_It, OSetIterator<t>>)
static inline OClass* _getContainerClass() { return BTI::getSetClass<t>(); }

template<typename t, typename _It>
requires (std::is_same_v<t, ORef> && std::is_same_v<_It, OVectorIterator<ORef>>)
static inline OClass* _getContainerClass() { return BTI::getObjectVectorClass(); }

template<typename t, typename _It>
requires (std::is_same_v<t, ORef> && std::is_same_v<_It, OSetIterator<ORef>>)
static inline OClass* _getContainerClass() { return BTI::getObjectSetClass(); }

template<typename t, typename _It>
requires (std::is_same_v<_It, OVectorIterator<t>>)
static inline OClass* _getIteratorClass() { return BTI::getVectorIteratorClass<t>(); }

template<typename t, typename _It>
requires (std::is_same_v<_It, OSetIterator<t>>)
static inline OClass* _getIteratorClass() { return BTI::getSetIteratorClass<t>(); }

template<typename t, typename _It>
static inline string _getNativeLoopMethodName() {
  return "ORange::nativeLoop<"+_getContainerClass<t, _It>()->name+">";
}

template<typename t, typename _It>
bool ORange::nativeLoop() noexcept {
  _ENTER_METHOD_NOCHK((_getNativeLoopMethodName<t, _It>()));
  auto& pipelineElement = pipeline_.front();
  Session* const m = pipelineElement.m;
  _It it{_getIteratorClass<t, _It>()};
  it = *static_cast<_It*>(pipelineElement.begin);
  NativeLoopTerminator<t> loopTerminator{*this};
  auto shouldContinue = loopTerminator.getContinue(it);
  for (iterationCounter = 0; true; ++it, ++iterationCounter) {
    ORef value = nullptr;
    OVar varValue{value};
    if (pipelineElement.type == ITERATOR_SENTRY) {
      varValue = _getValue<t>(it, m);
      _RETHROW_HERE_RETURN(true);
      value = REF(varValue);
    }
    ObjectAutoCleaner valueCleaner{m, varValue};
    if (!shouldContinue(value)) break;
    if (!value) {
      OVar rec{&it};
      varValue = it.ommGetValue(&rec, 0, nullptr, m);
      _RETHROW_HERE_RETURN(true);
      value = REF(varValue);
      valueCleaner.addDisposable(varValue);
    }
    if (!runStage(1, value)) break;
  }
  _RETHROW_HERE_RETURN(true);
  signalEnd(1);
  _RETHROW_HERE_RETURN(true);
  return true;
}

//::::::::::::::::::::   General Initial Pipeline Loop   ::::::::::::::::::::://

bool ORange::runGeneralLoop() noexcept {
  _ENTER_NOCHK("ORange::runGeneralLoop");
  auto& pipelineElement = pipeline_.front();
  Session* const m = pipelineElement.m;
  ORef it = m->duplicateObject(pipelineElement.begin);
  _RETHROW_HERE_RETURN(true);
  ObjectAutoCleaner itAutoCleaner{m, it};
  LoopTerminator loopTerminator{*this};
  auto shouldContinue = loopTerminator.getContinue(it);
  for (iterationCounter = 0; true; ++iterationCounter) {
    ORef value = nullptr;
    OVar varValue{value};
    if (pipelineElement.type == ITERATOR_SENTRY) {
      auto result = m->execMethod("value", 0, it, 0, nullptr);
      _RETHROW_HERE_RETURN(true);
      varValue = result.first;
      value = REF(varValue);
    }
    ObjectAutoCleaner valueCleaner{m, varValue};
    if (!shouldContinue(value)) break;
    if (!value) {
      auto result = m->execMethod("value", 0, it, 0, nullptr);
      _RETHROW_HERE_RETURN(true);
      varValue = result.first;
      value = REF(varValue);
      valueCleaner.addDisposable(varValue);
    }
    if (!runStage(1, value)) break;
    // advance "it" (working copy of pipelineElement.begin)
    m->execMethod("inc", 0, it, 0, nullptr);
    _RETHROW_HERE_RETURN(true);
  }
  _RETHROW_HERE_RETURN(true);
  signalEnd(1);
  _RETHROW_HERE_RETURN(true);
  return true;
}

//:::::::::::::::::::   Split Pipeline Element Utilities   ::::::::::::::::::://

bool ORange::runSplitter(size_t stage, ORef value) noexcept {
  ORef delimiter = pipeline_[stage].end;
  switch (delimiter->getClassId()) {
    case CLSID_ULONGVECTOR:
      return split<ulong >(stage, *_PVector(ulong,  delimiter), value);
    case CLSID_LONGVECTOR:
      return split<long  >(stage, *_PVector(long,   delimiter), value);
    case CLSID_UINTVECTOR:
      return split<uint  >(stage, *_PVector(uint,   delimiter), value);
    case CLSID_INTVECTOR:
      return split<int   >(stage, *_PVector(int,    delimiter), value);
    case CLSID_SHORTVECTOR:
      return split<short >(stage, *_PVector(short,  delimiter), value);
    case CLSID_CHARVECTOR:
      return split<char  >(stage, *_PVector(char,   delimiter), value);
    case CLSID_DBLVECTOR:
      return split<double>(stage, *_PVector(double, delimiter), value);
    case CLSID_FLTVECTOR:
      return split<float >(stage, *_PVector(float,  delimiter), value);
    case CLSID_BOOLVECTOR:
      return split<bool  >(stage, *_PVector(bool,   delimiter), value);
    case CLSID_REFVECTOR:
      return split<ORef  >(stage, *_PVector(ORef,   delimiter), value);
    case CLSID_OBJVECTOR:
      return split<ORef  >(stage, *_PObjVector(     delimiter), value);
  }
  return false;
}

template<typename t, typename _C>
requires SplitBufferReq<t, _C>
struct SplitBuffer {
  ORange& range;
  const _C& delimiter;
  ObjectVector& buffer;
  size_t& base;

  inline bool canConclude() const noexcept
  { return buffer.size() - base + 1 == delimiter.size(); }

  inline bool checkValue(ORef value, Session* m) const
  { return cmp(/* anything */0, value, m); }

  inline bool cmp(size_t which, Session* m) const
  { return cmp(which, nullptr, m); }

  bool cmp(size_t which, ORef value, Session* m) const
      requires (!std::is_same_v<t, ORef>) {
    const size_t bufferSize = buffer.size();
    if (value) which = bufferSize - base;
    _ASSERT(bufferSize - base < delimiter.size(), m);
    _ASSERT(which <= bufferSize - base, m);
    oxType<t> lhs{BTI::getTypeClass<t>()};
    lhs.setValue(delimiter[which]);
    OVar rhs{base + which == bufferSize ? value : buffer[base + which]};
    OArg argv[1]{&rhs};
    OVar rec{&lhs};
    ORet result = lhs.eq(&rec, 1, argv, m);
    _CHECK_EXCEPTION_RETURN(false);
    const bool isEqual = _P(bool, REF(result))->getValue();
    if (result.disposable())
      m->deleteObject(REF(result));
    return isEqual;
  }

  bool cmp(size_t which, ORef value, Session* m) const
      requires (std::is_same_v<t, ORef>) {
    const size_t bufferSize = buffer.size();
    if (value) which = bufferSize - base;
    _ASSERT(bufferSize - base < delimiter.size(), m);
    _ASSERT(which <= bufferSize - base, m);
    auto lhs = delimiter[which];
    OVar rhs{base + which == bufferSize ? value : buffer[base + which]};
    OArg argv[1]{&rhs};
    auto result = m->execMethod("eq:", 0, lhs, 1, argv);
    _CHECK_EXCEPTION_RETURN(false);
    ObjectAutoCleaner cleanResult{m, REF(result.first)};
    if (!REF(result.first)->getClass()->typeOf(BTI::getTypeClass<bool>())) {
      const string msg = lhs->getClass()->name+"::eq: expected return value "
          "type: 'bool'; found: '"+REF(result.first)->getClass()->name+"'";
      cleanResult.now();
      m->throwRTE("SplitBuffer::cmp", msg);
      return false;
    }
    return _P(bool, REF(result.first))->getValue();
  }

  void discardDelimiter(Session* m) {
    for (auto it = buffer.begin() + base; it != buffer.end(); ++it) {
      m->deleteObject(*it);
      if (m->exceptionPending()) break;
    }
    buffer.erase(buffer.begin() + base, buffer.end());
  }

#if 0
  bool forwardBuffer(size_t stage) {
    ObjectVector chunk{BTI::getObjectVectorClass()};
    chunk.push_back(&buffer);
    return range.runStage(stage + 1, &chunk);
  }
#endif // 0

  void reset(Session* m) {
    for (auto& item : buffer) {
      m->deleteObject(item);
      if (m->exceptionPending()) break;
    }
    buffer.erase(buffer.begin(), buffer.end());
    base = 0;
  }

  bool match(Session* m) noexcept {
    for (size_t i = 0; i < buffer.size() - base; ++i)
      if (!cmp(i, m) || m->exceptionPending()) return false;
    return true;
  }

  void rebase(Session* m) noexcept {
    ++base; // skip an item because the sequence doesn't match the delimiter
    for (; base < buffer.size(); ++base)
      if (match(m) || m->exceptionPending()) break;
  }

  inline void push(ORef value, Session* m) {
    ORef replica = m->duplicateObject(value);
    if (!m->exceptionPending())
      buffer.emplace_back(replica);
  }
};

template<typename t, typename _C>
bool ORange::split(size_t stage, const _C& delimiter, ORef value) noexcept {
  PipelineElement& pipelineElement = getPipelineElement(stage);
  Session* const m = pipelineElement.m;
  _ASSERT(pipelineElement.begin->getClassId() == CLSID_OBJVECTOR, m);
  ObjectVector& buffer = *static_cast<ObjectVector*>(pipelineElement.begin);
  auto& base = pipelineElement.size;
  SplitBuffer<t, _C> splitBuffer{ *this, delimiter, buffer, base };

  if (endSignal(value)) {
    if (buffer.empty()) return signalEnd(stage + 1);
    // splitBuffer.forwardBuffer(stage);
    runStage(stage + 1, &buffer);
    if (!m->exceptionPending()) {
      splitBuffer.reset(m);
      if (!m->exceptionPending()) return signalEnd(stage + 1);
    }
    return !m->exceptionPending();
  }

  const bool valueMatches = splitBuffer.checkValue(value, m);
  _CHECK_EXCEPTION_RETURN(false);
  bool canContinue = true;
  if (valueMatches && splitBuffer.canConclude()) {
    splitBuffer.discardDelimiter(m);
    if (!m->exceptionPending()) {
    //   canContinue = splitBuffer.forwardBuffer(stage);
      canContinue = runStage(stage + 1, &buffer);
      if (!m->exceptionPending())
        splitBuffer.reset(m);
    }
  }
  else
    splitBuffer.push(value, m);
  if (!m->exceptionPending() && !valueMatches)
    splitBuffer.rebase(m);
  _CHECK_EXCEPTION_RETURN(false);
  return canContinue;
}

//:::::::::::::::::::   Merge Pipeline Element Utilities   ::::::::::::::::::://

using bool2 = std::pair<bool, bool>;

bool ORange::runMerger(size_t stage, ORef value) noexcept {
  if (endSignal(value)) return signalEnd(stage + 1);
  Session* const m = getPipelineElement(stage).m;
  const bool2 result = runNativeMerger(stage, value);
  _CHECK_EXCEPTION_RETURN(false);
  const bool handled = result.second;
  if (handled) return result.first;
  return /* canContinue */ generalMergeLoop(stage, value);
}

#define _TVEC_MERGE_LOOP(t, val) \
    nativeMergeLoop<t>(stage, *static_cast<OVector<t>*>(val))
#define _TSET_MERGE_LOOP(t, val) \
    nativeMergeLoop<t>(stage, *static_cast<OSet<t>*>(val))
#define _OVEC_MERGE_LOOP(t, val) \
    nativeMergeLoop<t>(stage, *static_cast<ObjectVector*>(val))
#define _OSET_MERGE_LOOP(t, val) \
    nativeMergeLoop<t>(stage, *static_cast<ObjectSet*>(val))

bool2 ORange::runNativeMerger(size_t stage, ORef value) noexcept {
  const auto iterTypeId = value->getClassId();
  switch (iterTypeId) {
    case CLSID_ULONGVECTOR: return _TVEC_MERGE_LOOP(ulong,  value);
    case CLSID_LONGVECTOR:  return _TVEC_MERGE_LOOP(long,   value);
    case CLSID_UINTVECTOR:  return _TVEC_MERGE_LOOP(uint,   value);
    case CLSID_INTVECTOR:   return _TVEC_MERGE_LOOP(int,    value);
    case CLSID_SHORTVECTOR: return _TVEC_MERGE_LOOP(short,  value);
    case CLSID_CHARVECTOR:  return _TVEC_MERGE_LOOP(char,   value);
    case CLSID_DBLVECTOR:   return _TVEC_MERGE_LOOP(double, value);
    case CLSID_FLTVECTOR:   return _TVEC_MERGE_LOOP(float,  value);
    case CLSID_BOOLVECTOR:  return _TVEC_MERGE_LOOP(bool,   value);
    case CLSID_REFVECTOR:   return _TVEC_MERGE_LOOP(ORef,   value);
    case CLSID_OBJVECTOR:   return _OVEC_MERGE_LOOP(ORef,   value);
    case CLSID_ULONGSET:    return _TSET_MERGE_LOOP(ulong,  value);
    case CLSID_LONGSET:     return _TSET_MERGE_LOOP(long,   value);
    case CLSID_DBLSET:      return _TSET_MERGE_LOOP(double, value);
    case CLSID_STRSET:      return _TSET_MERGE_LOOP(string, value);
    case CLSID_REFSET:      return _OSET_MERGE_LOOP(ORef,   value);
    case CLSID_OBJSET:      return _OSET_MERGE_LOOP(ORef,   value);
  }
  return {true, false};
}

#undef _TVEC_MERGE_LOOP
#undef _TSET_MERGE_LOOP
#undef _OVEC_MERGE_LOOP
#undef _OSET_MERGE_LOOP

template<typename t, typename _C> requires (std::is_same_v<_C, OVector<t>>)
inline string _getContainerTypeName(const _C&)
{ return BTI::getVectorClass<t>()->name; }

template<typename t, typename _C> requires (std::is_same_v<_C, ObjectVector>)
inline string _getContainerTypeName(const _C&)
{ return BTI::getObjectVectorClass()->name; }

template<typename t, typename _C> requires (std::is_same_v<_C, OSet<t>>)
inline string _getContainerTypeName(const _C&)
{ return BTI::getSetClass<t>()->name; }

template<typename t, typename _C> requires (std::is_same_v<_C, ObjectSet>)
inline string _getContainerTypeName(const _C& c) {
  const bool b = c.isOwner();
  return (b ? BTI::getObjectSetClass() : BTI::getSetClass<ORef>())->name;
}

template<typename t, typename _C>
static string _getNativeMergeLoopMethodName(const _C& c) {
  const string typeName{
    std::is_same_v<t, ORef> ? "object" : BTI::getTypeClass<t>()->name};
  const string containerName{_getContainerTypeName<t>(c)};
  return "ORange::nativeMergeLoop<"+typeName+", "+containerName+">";
}

static const bool2 FAIL_RESULT{false, true};

template<typename t, typename _V>
requires (!std::is_same_v<t, bool>)
inline void _setRef(OTypeProxy<t>& proxy, _V ref) { proxy.setElemPtr(&ref); }

template<typename t, typename _V>
requires (std::is_same_v<t, bool>)
inline void _setRef(OTypeProxy<t>& proxy, _V ref) { proxy.setElemRef(ref); }

template<typename t, typename _C>
requires (!std::is_same_v<t, ORef> //&& !std::is_same_v<t, bool>
                                   && !std::is_same_v<_C, OSet<t>>)
bool2 ORange::nativeMergeLoop(size_t stage, _C& container) noexcept {
  Session* const m = getPipelineElement(stage).m;
  bool canContinue = true;
  for (auto it = container.begin(); canContinue && it != container.end(); ++it) {
    OTypeProxy<t> value{BTI::getTypeProxyClass<t>()};
    value.setValue(*it);
    _setRef<t>(value, *it);
    canContinue = runStage(stage + 1, &value);
    _RETHROW(_getNativeMergeLoopMethodName<t>(container)), FAIL_RESULT;
  }
  return {canContinue, true};
}

template<typename t, typename _C>
requires (!std::is_same_v<t, ORef> && !std::is_same_v<t, string> &&
                                      std::is_same_v<_C, OSet<t>>)
bool2 ORange::nativeMergeLoop(size_t stage, _C& container) noexcept {
  Session* const m = getPipelineElement(stage).m;
  bool canContinue = true;
  for (auto it = container.begin(); canContinue && it != container.end(); ++it) {
    oxType<t> value{BTI::getTypeClass<t>()};
    value.setValue(*it);
    canContinue = runStage(stage + 1, &value);
    _RETHROW(_getNativeMergeLoopMethodName<t>(container)), FAIL_RESULT;
  }
  return {canContinue, true};
}

template<typename t, typename _C>
requires (std::is_same_v<t, ORef>)
bool2 ORange::nativeMergeLoop(size_t stage, _C& container) noexcept {
  Session* const m = getPipelineElement(stage).m;
  bool canContinue = true;
  for (auto it = container.begin(); canContinue && it != container.end(); ++it) {
    ORef value = *it;
    canContinue = runStage(stage + 1, value);
    _RETHROW(_getNativeMergeLoopMethodName<t>(container)), FAIL_RESULT;
  }
  return {canContinue, true};
}

template<typename t, typename _C>
requires (std::is_same_v<t, string>)
bool2 ORange::nativeMergeLoop(size_t stage, _C& container) noexcept {
  Session* const m = getPipelineElement(stage).m;
  bool canContinue = true;
  for (auto it = container.begin(); canContinue && it != container.end(); ++it) {
    oxString value{BTI::getTypeClass<string>()};
    value = *it;
    canContinue = runStage(stage + 1, &value);
    _RETHROW(_getNativeMergeLoopMethodName<t>(container)), FAIL_RESULT;
  }
  return {canContinue, true};
}

bool ORange::generalMergeLoop(size_t stage, ORef container) noexcept {
  _ENTER_NOCHK("ORange::generalMergeLoop");
  Session* const m = getPipelineElement(stage).m;
  auto rb = m->execMethod("begin", 0, container, 0, nullptr);
  _RETHROW_HERE_RETURN(false);
  ORef it = REF(rb.first);
  ObjectAutoCleaner iteratorCleaner{m, rb.first};
  auto re = m->execMethod("end", 0, container, 0, nullptr);
  _RETHROW_HERE_RETURN(false);
  ORef end = REF(re.first);
  iteratorCleaner.addDisposable(re.first);
  bool canContinue = true;
  while (canContinue && !s_cmpValues(&rb.first, &re.first, m)) {
    auto result = m->execMethod("value", 0, it, 0, nullptr);
    _RETHROW_HERE_RETURN(false);
    ORef value = REF(result.first);
    ObjectAutoCleaner valueCleaner{m, result.first};
    canContinue = runStage(stage + 1, value);
    // advance "it"
    if (canContinue && !m->exceptionPending())
      m->execMethod("inc", 0, it, 0, nullptr);
    _RETHROW_HERE_RETURN(false);
  }
  return canContinue;
}

//::::::::::::::::::::::::::::   ORange Methods   :::::::::::::::::::::::::::://

ORet ORange::ommIterator(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::from:", 1);
  // check if the supplied items are iterators
  Requirement* req = _ARG(0)->getClass()->getRequirement();
  if (!req || !req->requiresInterface("BasicIterator"))
    _THROW_RTE("iterator arguments are required");
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // add initial pipeline
  pipeline_.emplace_back(m, ITERATOR, _REF(argv[0]));
  return makeResult();
}

// range from: begin to: end;
// range from: begin to: {...};
ORet ORange::ommIteratorPair(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::from:to:", 2);
  if (_ARG(1)->getClassId() == CLSID_BLOCK)
    return ommIteratorSentry(rec, argc, argv, m);
  // check if the supplied items are iterators
  Requirement* req = _ARG(0)->getClass()->getRequirement();
  if (!req || !req->requiresInterface("BasicIterator"))
    _THROW_RTE("iterator arguments are required");
  // check if the supplied iterators are the same type
  if (argv[1]->undefined() || _ARG(0)->getClassId() != _ARG(1)->getClassId()) {
    const string found = argv[1]->undefined() ? "null" : _ARG(1)->getClassName();
    _THROW_RTE("iterator type mismatch: "
        "expected '"+_ARG(0)->getClassName()+"' found '"+found+"'");
  }
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // add initial pipeline
  pipeline_.emplace_back(m, ITERATOR_PAIR, _REF(argv[0]), _REF(argv[1]));
  return makeResult();
}

ORet ORange::ommIteratorCount(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::from:count:", 2);
  // check if the supplied item is an iterator
  Requirement* req = _ARG(0)->getClass()->getRequirement();
  if (!req || !req->requiresInterface("BasicIterator"))
    _THROW_RTE("iterator argument is required");
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // unpack integer argument
  size_t count = g_castValue<size_t>(argv + 1, m);
  _CHECK_EXCEPTION();
  // add initial pipeline
  pipeline_.emplace_back(m, ITERATOR_COUNT, _REF(argv[0]));
  pipeline_.back().size = count;
  return makeResult();
}

ORet ORange::ommIteratorSentry(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::from:to:", 2);
  // check if the supplied item is an iterator
  Requirement* req = _ARG(0)->getClass()->getRequirement();
  if (!req || !req->requiresInterface("BasicIterator"))
    _THROW_RTE("iterator argument is required");
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // unpack block argument
  _ASSERT(_ARG(1)->getClassId() == CLSID_BLOCK, m); // caller ckecked it
  int blockId = g_blockId(_ARG(1));
  // add initial pipeline
  pipeline_.emplace_back(m, ITERATOR_SENTRY, blockId);
  pipeline_.back().begin = _ARG(0);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommIota(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::iota:", 1);
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // unpack integer argument
  long begin = g_castValue<long>(argv, m);
  _CHECK_EXCEPTION();
  // create count iterator
  ORef countIter = m->getClass(CLSID_COUNTITER)->createObject(m);
  static_cast<OCountIterator*>(countIter)->setBegin(begin);
  _CHECK_EXCEPTION();
  // add initial pipeline
  pipeline_.emplace_back(m, IOTA, countIter, nullptr);
  return makeResult();
}

ORet ORange::ommIotaEnd(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::iota:end:", 2);
  // check if this is an initial pipeline element
  if (!pipeline_.empty())
    _THROW_RTE("the pipeline is already initialized")
  // unpack integer arguments
  long begin = g_castValue<long>(argv, m), end;
  if (!m->exceptionPending())
    end = g_castValue<long>(argv + 1, m);
  _CHECK_EXCEPTION();
  // create count iterator
  ORef countIter = m->getClass(CLSID_COUNTITER)->createObject(m);
  static_cast<OCountIterator*>(countIter)->setBegin(begin);
  _CHECK_EXCEPTION();
  // create end iterator
  ORef endIter = m->getClass(CLSID_COUNTITER)->createObject(m);
  static_cast<OCountIterator*>(endIter)->setBegin(end);
  _CHECK_EXCEPTION();
  // add initial pipeline
  pipeline_.emplace_back(m, IOTA, countIter, endIter);
  return makeResult();
}

ORet ORange::ommTake(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::take:", 1);
  // unpack integer argument
  size_t size = g_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // create counter
  ORef counter = m->getClass(CLSID_COUNTITER)->createObject(m);
  _CHECK_EXCEPTION();
  // add pipeline
  pipeline_.emplace_back(m, TAKE, nullptr, counter);
  pipeline_.back().size = size;
  return makeResult();
}

ORet ORange::ommTakeWhile(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::takeWhile:", 1);
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(_ox_method_name, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, TAKE_WHILE, blockId);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommDrop(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::drop:", 1);
  // unpack integer argument
  size_t size = g_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // create counter
  ORef counter = m->getClass(CLSID_COUNTITER)->createObject(m);
  _CHECK_EXCEPTION();
  // add pipeline
  pipeline_.emplace_back(m, DROP, counter);
  pipeline_.back().size = size;
  return makeResult();
}

ORet ORange::ommDropWhile(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::dropWhile:", 1);
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(_ox_method_name, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, DROP_WHILE, blockId);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommFilter(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::filter:", 1);
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(_ox_method_name, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, FILTER, blockId);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommTransform(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD("range::transform:", 1);
  addTransform(_ox_method_name, argv, m);
  if (m->exceptionPending()) return undefinedResult();
  pipeline_.back().size = 0;
  return makeResult();
}

ORet ORange::ommProcess(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_METHOD("range::process:", 1);
  addTransform(_ox_method_name, argv, m);
  if (m->exceptionPending()) return undefinedResult();
  pipeline_.back().size = 1;
  return m->exceptionPending() ? undefinedResult() : makeResult();
}

ORet ORange::addTransform(const string& methodName, OArg* argv, Session* m) {
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(methodName, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE_METHOD(methodName, "the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE_METHOD(methodName, "the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, TRANSFORM, blockId);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommElements(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::elements:", 1);
  // unpack integer argument
  size_t size = g_castValue<size_t>(argv, m);
  _CHECK_EXCEPTION();
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, ELEMENTS);
  pipeline_.back().size = size;
  return makeResult();
}

ORet ORange::ommKeys(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::keys");
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  pipeline_.emplace_back(m, KEYS);
  return makeResult();
}

ORet ORange::ommValues(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::values");
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  pipeline_.emplace_back(m, VALUES);
  return makeResult();
}

ORet ORange::ommReverse(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::reverse");
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  pipeline_.emplace_back(m, REVERSE);
  return makeResult();
}

ORet ORange::ommMerge(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::merge");
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  // add pipeline
  pipeline_.emplace_back(m, MERGE);
  return makeResult();
}

template<typename t>
static ORef _setupSplit(OArg* argv, Session* m) {
  ORef delimiter = BTI::getVectorClass<t>()->createObject(m);
  _CHECK_EXCEPTION_RETURN(nullptr);
  auto arg = _PVector(t, _ARG(0));
  if (argv[0]->disposable() && _ARG(0)->getClassId() != CLSID_OBJVECTOR)
    _PVector(t, delimiter)->swap(*arg);
  else
    _PVector(t, delimiter)->assign(arg->begin(), arg->end());
  return delimiter;
}

template<>
ORef _setupSplit<string>(OArg* argv, Session* m) {
  ORef delimiter = BTI::getVectorClass<char>()->createObject(m);
  _CHECK_EXCEPTION_RETURN(nullptr);
  auto arg = static_cast<string*>(_PStr(_ARG(0)));
  _PVector(char, delimiter)->assign(arg->begin(), arg->end());
  return delimiter;
}

static ORef _getDelimiter(OArg* argv, Session* m) {
  ORef delimiter = BTI::getObjectVectorClass()->createObject(m);
  _CHECK_EXCEPTION_RETURN(nullptr);
  OVar delimVar{delimiter};
  OMethodAddress a, b;
  bool responds = _ARG(0)->getClass()->getMethodAddressByName("begin", &a);
  if (responds)
    responds = _ARG(0)->getClass()->getMethodAddressByName("end", &b);
  if (!responds) return nullptr;
  ORet it1 = m->execMethodAddress(a, "begin", 0, &delimVar, 0, nullptr), it2;
  if (!m->exceptionPending())
    it2 = m->execMethodAddress(b, "end", 0, &delimVar, 0, nullptr);
  _CHECK_EXCEPTION_RETURN(nullptr);
  // TODO check if begin returns a value that conforms to iterator requirements
  OVectorIterator<ORef> pos{BTI::getVectorIteratorClass<ORef>()};
  pos = _PObjVector(delimiter)->end();
  OVar varPos{&pos};
  OArg args[3] {&varPos, &it1, &it2};
  ORet result = _PObjVector(delimiter)->ommInsertRange(&delimVar, 3, argv, m);
  _CHECK_EXCEPTION_RETURN(nullptr);
  if (result.disposable())
    m->deleteObject(REF(result));
  return delimiter;
}

ORet ORange::ommSplit(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::split:", 1);
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  ORef delimiter;
  switch (_ARG(0)->getRootClassId()) {
    case CLSID_STRING:      delimiter = _setupSplit<string>(argv, m); break;
    case CLSID_ULONGVECTOR: delimiter = _setupSplit<ulong >(argv, m); break;
    case CLSID_LONGVECTOR:  delimiter = _setupSplit<long  >(argv, m); break;
    case CLSID_UINTVECTOR:  delimiter = _setupSplit<uint  >(argv, m); break;
    case CLSID_INTVECTOR:   delimiter = _setupSplit<int   >(argv, m); break;
    case CLSID_SHORTVECTOR: delimiter = _setupSplit<short >(argv, m); break;
    case CLSID_CHARVECTOR:  delimiter = _setupSplit<char  >(argv, m); break;
    case CLSID_DBLVECTOR:   delimiter = _setupSplit<double>(argv, m); break;
    case CLSID_FLTVECTOR:   delimiter = _setupSplit<float >(argv, m); break;
    case CLSID_BOOLVECTOR:  delimiter = _setupSplit<bool  >(argv, m); break;
    case CLSID_REFVECTOR:   delimiter = _setupSplit<ORef  >(argv, m); break;
    case CLSID_OBJVECTOR:   delimiter = _setupSplit<ORef  >(argv, m); break;
    default: delimiter = nullptr;
  }
  _CHECK_EXCEPTION();
  if (!delimiter) {
    delimiter = _getDelimiter(argv, m);
    _CHECK_EXCEPTION();
    if (!delimiter) {
      delimiter = BTI::getVectorClass<ORef>()->createObject(m);
      _CHECK_EXCEPTION();
      _PVector(ORef, delimiter)->push_back(_ARG(0));
    }
  }
  ORef buffer = BTI::getObjectVectorClass()->createObject(m);
  _CHECK_EXCEPTION();
  // add pipeline
  pipeline_.emplace_back(m, SPLIT, buffer, delimiter);
  pipeline_.back().size = 0;
  return makeResult();
}

ORet ORange::ommJoin(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::join");
  if (pipeline_.empty())
    _THROW_RTE("the pipeline is not initialized")
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE("the pipeline is finalized")
  m->throwRTE(_ox_method_name, "the join feature is not available yet");
  return makeResult();
}

ORet ORange::ommDo(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::do:", 1);
  addDo(_ox_method_name, argv, m);
  if (m->exceptionPending()) return undefinedResult();
  pipeline_.back().requireNotification(false);
  // run the pipeline
  run();
  _RETHROW_HERE();
  return makeResult();
}

ORet ORange::ommRun(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::run:", 1);
  addDo(_ox_method_name, argv, m);
  if (m->exceptionPending()) return undefinedResult();
  pipeline_.back().requireNotification(true);
  // run the pipeline
  run();
  _RETHROW_HERE();
  return makeResult();
}

ORet ORange::addDo(const string& method, OArg* argv, Session* m) {
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(method, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty())
    _THROW_RTE_METHOD(method, "the pipeline is not initialized");
  // check if the pipeline is closed
  if (pipeline_.back().type == EXECUTE)
    _THROW_RTE_METHOD(method, "the pipeline is finalized");
  // add pipeline
  pipeline_.emplace_back(m, EXECUTE, blockId);
  pipeline_.back().prepareLambda();
  return makeResult();
}

ORet ORange::ommRedo(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::redo:", 1);
  // check argument
  if (_ARG(0)->getClassId() != CLSID_BLOCK)
    return m->badArgTypeId(_ox_method_name, 0, CLSID_BLOCK, argv);
  const auto blockId = g_blockId(_ARG(0));
  // check if this is an initial pipeline element
  if (pipeline_.empty() || pipeline_.back().type != EXECUTE)
    _THROW_RTE("incomplete pipeline")
  // modify pipeline
  pipeline_.back().setBlockId(blockId);
  pipeline_.back().prepareLambda();
  // run the pipeline
  run();
  _RETHROW_HERE();
  return makeResult();
}

ORet ORange::ommRedoAsIs(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("range::redo");
  // check if this is an initial pipeline element
  if (pipeline_.empty() || pipeline_.back().type != EXECUTE)
    _THROW_RTE("incomplete pipeline")
  // run the pipeline
  run();
  _RETHROW_HERE();
  return makeResult();
}

ORet ORange::bor(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("range::" S_bitor, 1);
  switch (_ARG(0)->getClassId()) {
    case  CLSID_BLOCK: return ommDo(rec, argc, argv, m);
    case  CLSID_STRING: {
      const auto& name = *static_cast<string*>(_PStr(_ARG(0)));
      if (name == "iota"    ) return defaultIota(m)                 ; else
      if (name == "sequence") return defaultIota(m)                 ; else
      if (name == "keys"    ) return ommKeys    (rec, 0, nullptr, m); else
      if (name == "values"  ) return ommValues  (rec, 0, nullptr, m); else
      if (name == "reverse" ) return ommReverse (rec, 0, nullptr, m); else
      if (name == "merge"   ) return ommMerge   (rec, 0, nullptr, m); else
      if (name == "join"    ) return ommJoin    (rec, 0, nullptr, m); else
      if (name == "redo"    ) return ommRedo    (rec, 0, nullptr, m); else
        _THROW_RTE("unknown pipeline element")
    }
    case  CLSID_ULONG:
    case  CLSID_LONG:
    case  CLSID_UINT:
    case  CLSID_INT:
    case  CLSID_SHORT: {
      const long n = g_castValue<long>(argv, m);
      _CHECK_EXCEPTION();
      if (n >= 0)
        return ommTake(rec, 1, argv, m);
      oxType<long> size{BTI::getTypeClass<long>()};
      size.setValue(-n);
      OVar var{&size};
      OArg args[1]{&var};
      return ommDrop(rec, 1, args, m);
    }
    default:
      return m->badArgTypeId(_ox_method_name, 0, CLSID_BLOCK, argv);
  }
  return makeResult();
}

ORet ORange::defaultIota(Session* m) {
  oxType<long> start{BTI::getTypeClass<long>()};
  start.setValue(0);
  OVar var{&start};
  OArg argv[1]{&var};
  OVar rec{this};
  return ommIota(&rec, 1, argv, m);
}

//::::::::::::::::::::::::::::::  Method Table  :::::::::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(ORange, OInstance)
  PUBLIC (ORange::ommIterator,       "from:"       )
  PUBLIC (ORange::ommIteratorPair,   "from:to:"    )
  PUBLIC (ORange::ommIteratorCount,  "from:count:" )
  PUBLIC (ORange::ommIteratorSentry, "from:to:"    )
  PUBLIC (ORange::ommIota,           "iota:"       )
  PUBLIC (ORange::ommIotaEnd,        "iota:end:"   )
  PUBLIC (ORange::ommTake,           "take:"       )
  PUBLIC (ORange::ommTakeWhile,      "takeWhile:"  )
  PUBLIC (ORange::ommDrop,           "drop:"       )
  PUBLIC (ORange::ommDropWhile,      "dropWhile:"  )
  PUBLIC (ORange::ommFilter,         "filter:"     )
  PUBLIC (ORange::ommTransform,      "transform:"  )
  PUBLIC (ORange::ommProcess,        "process:"    )
  PUBLIC (ORange::ommElements,       "elements:"   )
  PUBLIC (ORange::ommKeys,           "keys"        )
  PUBLIC (ORange::ommValues,         "values"      )
  PUBLIC (ORange::ommReverse,        "reverse"     )
  PUBLIC (ORange::ommMerge,          "merge"       )
  PUBLIC (ORange::ommSplit,          "split:"      )
  PUBLIC (ORange::ommJoin,           "join"        )
  PUBLIC (ORange::ommDo,             "do:"         )
  PUBLIC (ORange::ommRun,            "run:"        )
  PUBLIC (ORange::ommRedo,           "redo:"       )
  PUBLIC (ORange::ommRedoAsIs,       "redo"        )
  PUBLIC (ORange::bor,               S_bitor       )
_END_METHOD_TABLE()

//:::::::::::::::::::::::::::  Metaclass Methods  :::::::::::::::::::::::::::://

ORef ORange_Class::copyCore(ORef self, const_ORef r, Session* m) const {
  _ENTER_NOCHK("ORange_Class::copyCore");
  auto& lhs = *static_cast<ORange*>(self);
  const auto& rhs = *static_cast<const ORange*>(r);
  lhs = rhs;
  return self;
}

ORef ORange_Class::moveCore(ORef self, ORef r) const {
  if (r->getRootClass() != getRootClass()) return nullptr;
  ORange* const _this = static_cast<ORange*>(self);
  *_this = std::move(*static_cast<ORange*>(r));
  return self;
}

OClass* ORange_Class::createClass(Session* m, const string& className) const {
  _ENTER_NOCHK("ORange_Class::createClass");
  OClass* cls = new ORange_Class(m, this->name, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

OClass* ORange_Class::createMutableClass(Session* m,
    const string& className) const {
  _ENTER_NOCHK("ORange_Class::createMutableClass");
  OClass* cls = new ORange_Class(m, this, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

//============================================================================//
//:::::::::::::::::::::::::::   Class Installer   :::::::::::::::::::::::::::://
//============================================================================//

bool g_installRangeClass(Session* m) {
  extern OClass* _OXType_class[];

  OClass* cls = new ORange_Class(m, "range");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_RANGE);
  _OXType_class[CLSID_RANGE] = cls;

  return true;
}
