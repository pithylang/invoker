#include "object.hpp"
#include "omapi.h"

// =========================================================
// SDOM API
// =========================================================

extern "C" {

class Session;

#if 0
Session *oxGetSession (ORef rtm)
{  return (Session *) _PRTMgr(rtm);  }

OClass *oxObjectClass (ORef self)
{  return self->getClass();  }

OClass *oxBasicTypeClass (int iClassId)
{  return _OXType_class[iClassId];  }

void oxResolveMemberTypes (Session *session)
{  OClass::s_resolvePropTypes(session);  }

//void oxCheckIndefiniteMembers(OClass *c)
//{  c->checkIndefiniteMembers();  }

bool oxGetMethodAddressByName (OClass *pClass, const char *method,
                              OMethodAddress *a, void **ppv)
{  return pClass->getMethodAddressByName(method, a, ppv);  }

ORef oxCreateObject (const char *type, Session *m)
{  return oxGetClass(type)->createObject(m);  }

ORef oxCreateObjectClass (OClass *pClass, Session *m)
{  return pClass->createObject(m);  }

void oxDeleteObject (ORef self, Session *m)
{  self->deleteObject(m);  }

void oxSetIntObjectValue (ORef self, int value)
{  _PINT(self)->setValue(value);  }

void oxSetBoolObjectValue (ORef self, bool value)
{  _PBOOL(self)->setValue(value);  }

void oxSetPointerObjectValue (ORef self, void *value)
{  _PPTR(self)->setValue(value);  }

void oxSetStringObjectValue (ORef self, const char *str)
{  *_PStr(self) = str;  }

void oxSetTypeObjectValue (ORef self, void *pValue)
{
  switch (self->getClassId())
  {
  case CLSID_UINT:   _PUINT(self)->setValue(*(uint*)pValue); break;
  case CLSID_INT:    _PINT(self)->setValue(*(int*)pValue); break;
  case CLSID_SHORT:  _PSHORT(self)->setValue(*(short*)pValue); break;
  case CLSID_CHAR:   _PCHAR(self)->setValue(*(char*)pValue); break;
  case CLSID_DOUBLE: _PDBL(self)->setValue(*(double*)pValue); break;
  case CLSID_FLOAT:  _PFLT(self)->setValue(*(float*)pValue); break;
  case CLSID_BOOL:   _PBOOL(self)->setValue(*(bool*)pValue); break;
  case CLSID_PTR:    _PPTR(self)->setValue(*(void**)pValue); break;
  default:
    if (self->getClassId() == CLSID_STRING)
       *_PStr(self) = (const char *)pValue;
  }
}

void oxGetTypeObjectValue (ORef self, void *p)
{
  switch (self->getClassId())
  {
  case CLSID_UINT:   *(uint*)p = _PUINT(self)->getValue(); break;
  case CLSID_INT:    *(int*)p = _PINT(self)->getValue(); break;
  case CLSID_SHORT:  *(short*)p = _PSHORT(self)->getValue(); break;
  case CLSID_CHAR:   *(char*)p = _PCHAR(self)->getValue(); break;
  case CLSID_DOUBLE: *(double*)p = _PDBL(self)->getValue(); break;
  case CLSID_FLOAT:  *(float*)p = _PFLT(self)->getValue(); break;
  case CLSID_BOOL:   *(bool*)p = _PBOOL(self)->getValue(); break;
  case CLSID_PTR:    *(void**)p = _PPTR(self)->getValue(); break;
  case CLSID_STRING: *(char**)p = (char*) _PStr(self)->c_str();
                                                           break;
  }
}

ORef oxApplyMethod  (ORef self, const char *method,
                     int argc, OArg *argv, Session *m)
{  return self->applyMethod(method, argc, argv, m);  }

ORef oxApplySelector(ORef self, const OMethodAddress *a,
                     int argc, OArg *argv, Session *m)
{  return applyMethod_(self, a, argc, argv, m);  }

ORef oxApplyMethodAddress (ORef self, const OMethodAddress *a,
                           int argc, OArg *argv, ORef sessionId)
{  return applyMethod_(self, a, argc, argv, _PRTMgr(sessionId));  }

bool oxGetMMethodAddressByName(const char *s, void *a)
{  return OClass::getMMethodAddressByName(s, (MMethodAddress*)a);  }

void     oxSetMachine         (ORef rtm, VirtualMachine *vm)
{  _PRTMgr(rtm)->setMachine(vm);  }

// Pointer as obtained by oxInitialize
void     oxTerminate          (vmType pClassMgr)
{  ((oxClassMgr *) pClassMgr)->term();  }

ORef     oxRethrowException   (Session *m, const char *name)
{  m->rethrow(name);  }

void     oxClassDeleteObject  (OClass *cls, ORef o, Session *m)
{  cls->deleteObject(o, m);  }

ORef     oxCreateRTMgrObject  ()
{  return oxGetClass(OX_RTMgrName)->createObject(nullptr);  }

ORef     oxDeleteClass        (const char *name)
{  delete oxGetClass(name);  }

bool oxGetDMethodAddressByName(OClass *cls, const char *s,
                               vmClassSegs **ppClassSegs,
                               int *cs, int *ds)
{
  OMethodAddress a;
  if (!cls->getMethodAddressByName(s, &a) ||
      !DMethodAddressOk(a))
    return false;
  *cs = a.dynamic_method_address.cs;
  *ds = a.dynamic_method_address.ds;
  *ppClassSegs = a.dynamic_method_address.pClassSegs;
  return true;
}

const char *oxGetClassName(OClass *pClass)
{  return pClass->getName();  }

int oxBaseObjectSize()
{  return sizeof(OInstance);  }

int oxGetMemberOffset(ORef r, const char *name)
{  return r->getClass()->getMemberOfs(name);  }

bool oxIsPropTemplate(ORef r, const char *name) {
  // Search member list
  int ofs = r->getClass()->getMemberOfs(name);
  ASSERT(ofs != -1); // checked previously
  return r->getClass()->isPropTemplate(ofs);
}

bool oxIsMemberConst(ORef r, int ofs)
{ return r->getClass()->isPropConst(ofs);  }

bool oxClassExtends(OClass *c, OClass *b)
{ return c->extends(b);  }

void oxDeleteTmpObject(ORef self, ORef rtm)
{ self->getClass()->deleteObject(self, _PRTMgr(rtm));  }

int oxGetRootClassId(ORef r)
{ return r->getRootClassId();  }

ORef *oxGetMembers(ORef r)
{ return r->getClass()->memberArray(r);  }

OClass **oxGetBaseClasses()
{ return _OXType_class;  }

OClass *oxGetBaseClass(int which)
{ return _OXType_class[which];  }

bool oxExceptionPending(ORef so)
{ return ((ORTMgr *) so)->exceptionPending();  }

vmClassSegs *oxGetClassSegs(OClass *cls)
{ return cls->getClassSegs();  }

bool oxCheckSelectorAccessibility(OClass *c, ORef selector,
                       OClass *caller, const char **pName) {
  oxSelectorCache *p = (oxSelectorCache *)selector;
  if (pName != 0)
    *pName = p->getName();
  return c->checkMethodAccessibility(p->getName(), caller);
}

bool oxGetVariableData    (const char *varname,
                           unsigned fOpt,
                           ORef *result,
                           unsigned *pfRes) {
  if (g_GetDataProc == nullptr) {
    *pfRes = _GET_DATA_INV_VM;
    return false;
  }
  return g_GetDataProc(varname, fOpt, result, pfRes);
}

} // extern "C"

#endif // 0
