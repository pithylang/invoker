#include "mcaches.hpp"
#include "object.hpp"
#include "ortmgr.hpp"
// #include <tuple>
#include <algorithm>

//============================================================================//
//::::::::::::::::::::::::::::   PropertyCache   ::::::::::::::::::::::::::::://
//============================================================================//

std::tuple<int, bool>
PropertyCache::getPropertyForClass(OClass* c, OClass* caller) {
  using std::ignore;
  ASSERT(caller->typeOf(c));
  if (table.size() == 1 && table[0].owner == c)
    return {table[0].offset, true};
  int foundOffset = lookup_(c);
  if (foundOffset != -1) return {foundOffset, false};
  ushort access;
  OClass* introducer;
  std::tie(introducer, ignore, foundOffset, access) = c->resolveProperty(name);
  if (foundOffset == -1) /*not a member*/ return {-1, false};
  return {foundOffset, !g_private(access) || c == caller};
}

int PropertyCache::lookup_(const OClass* c) const noexcept
{
  auto it = std::find(table.begin(), table.end(), PropertyCacheRec(c, -1));
  return it == table.end() ? -1 : it->offset;
}

//============================================================================//
//::::::::::::::::::::::::::::   SelectorCache   ::::::::::::::::::::::::::::://
//============================================================================//

std::tuple<const OAddr&, bool>
SelectorCache::getSelectorForClass(OClass* c, OClass* caller, Session* m) {
  if (table.size() == 1 && table[0].caller == c)
    return {table[0].methodAddress, false};
  static const OAddr nullAddress;
  const OAddr* pa = lookup_(c);
  if (pa) return {*pa, false}; // ok
#if 0
  OClass* const baseType = prefix.empty() ? c :
      m->classMgr->getClass(m->isBuiltinType(prefix) ?
                            prefix : c->getPrefix() + prefix);
#else
  OClass* baseType{nullptr};
  if (prefix.empty())
    baseType = c;
  else if (OClass* const prefixType = m->classMgr->getClass(prefix);
      prefixType && (m->isBuiltinType(prefixType) || c->getPrefix().empty()))
    baseType = prefixType;
  if (!baseType)
    baseType = m->classMgr->getClass(c->getPrefix() + prefix);
#endif
  if (!baseType)
    return {nullAddress, true}; // not responding
  const auto [accessible, f] = baseType->getMethodAccessAddress(name, caller);
  if (!f.valid())
    return {nullAddress, true}; // not responding
  return {accessible ? addSelector(c, f) : nullAddress, false};
      //               ^ok                 ^responding but not accessible
}

const OAddr* SelectorCache::lookup_(const OClass* c) const {
  auto it = std::find(table.begin(), table.end(), SelectorCacheRec(c));
  return it == table.end() ? nullptr : &it->methodAddress;
}

void SelectorCache::adjustName_(Session* m) noexcept {
  const auto pos = name.rfind("::");
  if (pos == std::string::npos) return;
  prefix = m->getPrefixedTypeName(name.substr(0, pos));
  name = name.substr(pos + 2);
}
