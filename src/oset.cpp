#include <memory>
#include <algorithm>
#include "objectAutoCleaner.hpp"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "ovector.hpp"
#include "oset.hpp"
#include "obasicTypeInfo.hpp"
#include "oveciter.hpp"
#include "osetiter.hpp"
#include "orange.hpp"
#include "oalgo.hpp"
#include "util.hpp"
#include "fswitch.h"

_TEMPLATE_DEFINITION(OSet, OInstance);

using std::string;
using oset_t = std::set<ORef, ORefSetCompare>;

#define _CHECK_BAD_ARGTYPE(i,TYPE,METHNAM)           \
  _ASSERT(TYPE->isCoreClass(), m);                   \
  if (!OM_ARG_TYPE(i, TYPE))                         \
    return m->badArgType(METHNAM, i, TYPE, argv)

#define _CHECK_BAD_ARG(i,TYPEID,METHNAM)             \
  _ASSERT(m->getClass(TYPEID)->isCoreClass(), m);    \
  if (!OM_ARG_TYPEID(i, TYPEID))                     \
    return m->badArgTypeId(METHNAM, i, TYPEID, argv)

static
bool operator==(const OMethodAddress& a, const OMethodAddress& b) noexcept {
  return a.scriptMethod == b.scriptMethod && (a.scriptMethod ?
      a.scriptMethodAddress.eq(b.scriptMethodAddress) :
      a.coreMethodAddress == b.coreMethodAddress);
}

template<typename t>
  string s_getFullMethodName(const string& methodName) noexcept
  { return OBasicTypeInfo::getSetClass<t>()->name+"::"+methodName; }

template<typename t>
  inline OClass* s_getSetIteratorClass() noexcept
  { return OBasicTypeInfo::getSetIteratorClass<t>(); }

//============================================================================//
//::::::::::::::::::::::::::::   ORefSetCompare   :::::::::::::::::::::::::::://
//::::::::::::::::::::::::   Object Set Comparison   ::::::::::::::::::::::::://
//============================================================================//

ORefSetCompare::ORefSetCompare() :
    currentSession{nullptr}, initialized{false} {}

bool ORefSetCompare::operator()(const ORef& lhs, const ORef& rhs) const {
  if (!initialized) {
    // we use this trick to get access to the comparison structure
    auto* const _this = const_cast<ORefSetCompare*>(this);
    OInstance o{reinterpret_cast<OClass*>(_this)};
    *const_cast<ORef&>(lhs) = o;
    _this->initialized = true;
  }
  if (compare.notValid())
    return std::less<ORef>::operator()(lhs, rhs);
  Session* const m = currentSession;
  OVar rhsVar{rhs};
  OArg argv[1]{&rhsVar};
  OVar lhsVar{lhs};
  ORet r = m->execMethodAddress(compare, "compare:", EMF_CONST_REC,
      &lhsVar, 1, argv);
  if (m->exceptionPending()) {
    m->rethrow("ORefSetCompare::operator()", __FILE__, __LINE__);
    throw string{"OSet comparison error"};
  }
  if (REF(r)->getRootClassId() != BTI::getTypeClassId<bool>()) {
    const string msg = "set comparison method did not return boolean type";
    m->throwRTE("ORefSetCompare::operator()", msg);
    throw string{"OSet comparison error"};
  }
  const bool result = _P(bool, REF(r))->getValue();
  if (r.disposable()) {
    m->deleteObject(REF(r));
    if (m->exceptionPending()) {
      m->rethrow("ORefSetCompare::operator()", __FILE__, __LINE__);
      throw string{"OSet comparison error"};
    }
  }
  return result;
}

//============================================================================//
//::::::::::::::::::::::::::::   ORefSetPrepare   :::::::::::::::::::::::::::://
//::::::::::::::::::::::::   Prepare Set Operation   ::::::::::::::::::::::::://
//============================================================================//

bool ORefSetPrepare::insert(size_t i, const ORef& value) {
  OClass* const c = value->getClass();
  _ASSERT(c, m);
  _ASSERT(c->getClassId() != 0, m);
  _ASSERT(comparison != nullptr, m);
  _ASSERT(!(i == 0 && typeId != 0) || comparison->compare.valid(), m);
  if (typeId == 0) {
    if (i == 0) {
      _ASSERT(comparison->initialized, m);
      // cache data for comparison method
      if (!c->getMethodAddressByName("compare:", &comparison->compare))
        return (errorNumber = NO_COMPARE_METHOD) == NO_ERROR;
      typeId = c->getClassId();
    }
    else
      return (errorNumber = TYPELESS_OBJECT_SET) == NO_ERROR;
  }
  else if (c->getClassId() != typeId &&
           !c->extends(m->classMgr->getClass(typeId)))
    return (errorNumber = TYPE_MISMATCH) == NO_ERROR;
  comparison->currentSession = m;
  return (errorNumber = NO_ERROR) == NO_ERROR;
}

void ORefSetPrepare::insertError(const string& methodName, const ORef& value) {
  OClass* const c = value->getClass();
  string msg;
  switch (errorNumber) {
    case NO_COMPARE_METHOD:
      msg = "object type '"+c->name+"' cannot be accepted as a set "
            "element because it does not support the 'compare:' method";
      break;
    case TYPELESS_OBJECT_SET:
      msg = "cannot insert item type '"+c->name+"' into typeless set";
      break;
    case TYPE_MISMATCH:
      msg = "cannot insert item type '"+c->name+"' into set of "
            "objects type '"+m->classMgr->getClass(typeId)->name+"'";
      break;
    default:
      return;
  }
  m->throwRTE(methodName, msg);
}

bool ORefSetPrepare::copy(OClass* setType, const OMethodAddress& rhsCompare) {
  _ASSERT(setType, m);
  _ASSERT(setType->getClassId() != 0, m);
  _ASSERT(comparison != nullptr, m);
  _ASSERT(comparison->initialized, m);
  _ASSERT(rhsCompare.valid(), m);

  if (typeId == 0) {
    _DEBUG( OMethodAddress a; );
    _ASSERT(setType->getMethodAddressByName("compare:", &a), m);
    _ASSERT(a == rhsCompare, m);
    // cache data for comparison method
    comparison->compare = rhsCompare;
    typeId = setType->getClassId();
  }
  else if (typeId != setType->getClassId())
    return false;

  _ASSERT(comparison->compare == rhsCompare, m);
  comparison->currentSession = m;
  return true;
}

ORet ORefSetPrepare::badArgType(const string& methodName,
                                int argn, OArg* argv) const {
  const OClass* const setType = m->classMgr->getClass(typeId);
  const string msg = argn > -1 ?
      "unexpected type for argument "+std::to_string(argn+1)+"\n"
      "expected '"+setType->name+"', "
      "found '"+_ARG(argn)->getClassName()+"' "
      :
      "cannot insert item type '"+_ARG(0)->getClassName()+"' "
      "into set of objects type '"+setType->name+"'";
  m->throwBadArg(methodName, msg);
  return OVariable::undefinedResult();
}

//============================================================================//
//:::::::::::::::::::::::::::::::   type set   ::::::::::::::::::::::::::::::://
//============================================================================//

//::::::::::::::::::   set -- init: initialize components   :::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommInit(OArg rec, int argc, OArg* argv, Session* m) {
    _ASSERT(this->empty(), m);
    for (int i = 0; i < argc; ++i) {
      const t value = g_castValue<t>(argv + i, m);
      _RETHROW(s_getFullMethodName<t>("init:"));
      this->insert(value);
    }
    return makeResult();
  }

template<>
  ORet OSet<string>::ommInit(OArg rec, int argc, OArg* argv, Session* m) {
    _ASSERT(this->empty(), m);
    for (int i = 0; i < argc; ++i) {
      _CHECK_BAD_ARG(i, CLSID_STRING, s_getFullMethodName<string>("init:"));
      _UNPACK_CONST_REF(String, value, i);
      this->insert(value);
    }
    return makeResult();
  }

//:::::::::::::::::::::::::::::   set -- count   ::::::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommCount(OArg rec, int, OArg* argv, Session* m) {
    const t value = g_castValue<t>(argv, m);
    _RETHROW(s_getFullMethodName<t>("count:"));
    ORef r = BTI::getTypeClass<size_t>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>("count:"));
    _P(size_t, r)->setValue(this->count(value));
    return r->disposableResult();
  }

template<>
  ORet OSet<string>::ommCount(OArg rec, int, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<string>("count:"));
    _CHECK_BAD_ARG(0, CLSID_STRING, _ox_method_name);
    _UNPACK_CONST_REF(String, value, 0);
    ORef r = BTI::getTypeClass<size_t>()->createObject(m);
    _CHECK_EXCEPTION();
    _P(size_t, r)->setValue(this->count(value));
    return r->disposableResult();
  }

//:::::::::::::::::::::::::::::   set -- size   :::::::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommSize(OArg rec, int, OArg*, Session* m) {
    ORef r = BTI::getTypeClass<size_t>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>("size"));
    _P(size_t, r)->setValue(this->size());
    return r->disposableResult();
  }

//:::::::::::::::::::::::::::::   set -- find   :::::::::::::::::::::::::::::://

// iterator find( const Key& key );
template<typename t>
  ORet OSet<t>::ommFind(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD(s_getFullMethodName<t>("find:"), 1);
    const t value = g_castValue<t>(argv, m);
    _CHECK_EXCEPTION();
    ORef r = BTI::getSetIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    // *_PSetIter(t, r) = this->find(value);
    r->castRef<_O(SetIterator)<t>>() = this->find(value);
    return r->disposableResult();
  }

template<>
  ORet OSet<string>::ommFind(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD(s_getFullMethodName<string>("find:"), 1);
    _CHECK_BAD_ARG(0, CLSID_STRING, _ox_method_name);
    _UNPACK_CONST_REF(String, value, 0);
    ORef r = s_getSetIteratorClass<string>()->createObject(m);
    _CHECK_EXCEPTION();
    // *_PSetIter(string, r) = this->find(value);
    r->castRef<_O(SetIterator)<string>>() = this->find(value);
    return r->disposableResult();
  }

//:::::::::::::::::::::::::::::   set -- empty   ::::::::::::::::::::::::::::://

// rec empty;
template<typename t>
  ORet OSet<t>::ommEmpty(OArg rec, int, OArg*, Session* m) {
    ORef r = BTI::getTypeClass<bool>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>("empty"));
    _P(bool, r)->setValue(this->empty());
    return r->disposableResult();
  }

//:::::::::::::::::::::::::::::   set -- erase   ::::::::::::::::::::::::::::://

// size_type erase(const Key& key);
template<typename t>
  ORet OSet<t>::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("erase:"));
    _MUTABLE_REC();
    const t key = g_castValue<t>(argv, m);
    _RETHROW_HERE();
    // do the call
    size_t retValue = this->erase(key);
    // pack return value
    ORef r = BTI::getTypeClass<size_t>()->createObject(m);
    _CHECK_EXCEPTION();
    _P(size_t, r)->setValue(retValue);
    return r->disposableResult();
  }

template<>
  ORet OSet<string>::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<string>("erase:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(0, CLSID_STRING, _ox_method_name);
    _UNPACK_CONST_REF(String, key, 0);
    // do the call
    size_type retValue = this->erase(key);
    // pack return value
    ORef r = BTI::getTypeClass<size_type>()->createObject(m);
    _CHECK_EXCEPTION();
    _P(size_type, r)->setValue(retValue);
    return r->disposableResult();
  }

//iterator erase(const_iterator position);
template<typename t>
  ORet OSet<t>::ommEraseAt(OArg rec, int argc, OArg* argv, Session* m) {
    // using setiter_t = typename std::set<t>::iterator;
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("eraseAt:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(0, BTI::getSetIteratorClassId<t>(), _ox_method_name);
    const auto& it = _ARG(0)->castRef<_O(SetIterator)<t>>();
    // validate iterator
    _DEBUG(                                                  \
      if (!validateIterator(*static_cast<set_t*>(this), it)) \
        _THROW_RTE("iterator does not belong to set");       \
    )
    // pack return value
    ORef r = s_getSetIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<_O(SetIterator)<t>>() = this->erase(it);
    return r->disposableResult();
  }

//iterator erase(const_iterator first, const_iterator last);
template<typename t>
  ORet OSet<t>::ommEraseRange(OArg rec, int argc, OArg* argv, Session* m) {
    // using setiter_t = typename std::set<t>::iterator;
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("eraseFrom:to:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(0, BTI::getSetIteratorClassId<t>(), _ox_method_name);
    _CHECK_BAD_ARG(1, BTI::getSetIteratorClassId<t>(), _ox_method_name);
    const auto& from = _ARG(0)->castRef<_O(SetIterator)<t>>();
    const auto& to   = _ARG(1)->castRef<_O(SetIterator)<t>>();
    // validate iterators
    _DEBUG(                                                           \
      if (!validateIterators(*static_cast<set_t*>(this), {from, to})) \
        _THROW_RTE("iterator does not belong to set");                \
    )
    // pack return value
    ORef r = s_getSetIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<_O(SetIterator)<t>>() = this->erase(from, to);
    return r->disposableResult();
  }

//:::::::::::::::::::::::::::::   set -- clear   ::::::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommClear(OArg rec, int, OArg*, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<t>("clear"));
    this->clear();
    return makeResult();
  }

//::::::::::::::::::::::::::::   set -- insert   ::::::::::::::::::::::::::::://

template<typename t, typename _It>
  static ORet s_packInsertResult(typename std::pair<_It, bool>& rv, Session* m) {
    ORef oIterator = s_getSetIteratorClass<t>()->createObject(m);
    if (m->exceptionPending()) return OVariable::undefinedResult();
    oIterator->castRef<_O(SetIterator)<t>>() = rv.first;
    ORef oSuccess = BTI::getTypeClass<bool>()->createObject(m);
    if (m->exceptionPending()) return OVariable::undefinedResult();
    _P(bool, oSuccess)->setValue(rv.second);
    ORef r = BTI::getObjectVectorClass()->createObject(m);
    if (m->exceptionPending()) return OVariable::undefinedResult();
    ObjectVector& v = *_PObjVector(r);
    v.resize(2);
    v[0] = oIterator;
    v[1] = oSuccess;
    return r->disposableResult();
  }

template<typename t>
  ORet OSet<t>::packInsertResult(const std::string& methodName,
      std::pair<typename std::set<t>::iterator, bool>& rv, Session* m) {
    ORet r = s_packInsertResult<t, typename std::set<t>::iterator>(rv, m);
    _RETHROW(methodName);
    return r;
  }

template<typename t, typename _Cont>
  struct OSetInsertLoop : OBasicLoop {
    using setiter_t = typename _Cont::iterator;
    _Cont& container;
    setiter_t hint;

    OSetInsertLoop(_Cont& c) : container{c}, hint{c.begin()} {}

    virtual void action(OVar result, Session* m) noexcept override {
      OArg argv[]{&result};
      const t value = g_castValue<t>(argv, m);
      if (m->exceptionPending()) return;
      hint = container.insert(hint, value);
    }
  };

template<typename _Cont>
  struct OSetInsertLoop<string, _Cont> : OBasicLoop {
    using setiter_t = typename _Cont::iterator;
    _Cont& container;
    setiter_t hint;
    getmsg_t getNames;

    OSetInsertLoop(_Cont& c, getmsg_t f) :
        container{c}, hint{c.begin()}, getNames{f} {}

    virtual void action(OVar result, Session* m) noexcept override {
      if (REF(result)->getRootClassId() != CLSID_STRING) {
        const string type = REF(result)->getClassName();
        auto names = getNames();
        m->throwRTE(names.second, "cannot insert objects "
            "type '"+type+"' into a '"+names.first+"'");
        return;
      }
      const string& value = *static_cast<String*>(_PStr(REF(result)));
      hint = container.insert(hint, value);
    }
  };

#define VEC_ITER(t, n) _ARG(n)->castRef<_O(VectorIterator)<t>>()
#define SET_ITER(t, n) _ARG(n)->castRef<_O(SetIterator)<t>>()

template<typename t>
  bool OSet<t>::insertTypeRange(OArg* argv) {
    const auto argTypeId = _ARG(0)->getClassId();
    switch (argTypeId) {
      case CLSID_ULONGVECITER:
        this->insert(VEC_ITER(ulong, 0),  VEC_ITER(ulong, 1)); break;
      case CLSID_LONGVECITER:
        this->insert(VEC_ITER(long, 0),   VEC_ITER(long, 1)); break;
      case CLSID_UINTVECITER:
        this->insert(VEC_ITER(uint, 0),   VEC_ITER(uint, 1)); break;
      case CLSID_INTVECITER:
        this->insert(VEC_ITER(int, 0),    VEC_ITER(int, 1)); break;
      case CLSID_SHORTVECITER:
        this->insert(VEC_ITER(short, 0),  VEC_ITER(short, 1)); break;
      case CLSID_CHARVECITER:
        this->insert(VEC_ITER(char, 0),   VEC_ITER(char, 1)); break;
      case CLSID_DBLVECITER:
        this->insert(VEC_ITER(double, 0), VEC_ITER(double, 1)); break;
      case CLSID_FLTVECITER:
        this->insert(VEC_ITER(float, 0),  VEC_ITER(float, 1)); break;
      case CLSID_BOOLVECITER:
        this->insert(VEC_ITER(bool, 0),   VEC_ITER(bool, 1)); break;
      case CLSID_ULONGSETITER:
        this->insert(SET_ITER(ulong, 0),  SET_ITER(ulong, 1)); break;
      case CLSID_LONGSETITER:
        this->insert(SET_ITER(long, 0),   SET_ITER(long, 1)); break;
      case CLSID_DBLSETITER:
        this->insert(SET_ITER(double, 0), SET_ITER(double, 1)); break;
      default:
        return false;
    }
    return true;
  }

template<>
  bool OSet<string>::insertTypeRange(OArg* argv) {
    if (_ARG(0)->getClassId() != CLSID_STRSETITER)
      return false;
    this->insert(SET_ITER(string, 0), SET_ITER(string, 1));
    return true;
  }

#undef SET_ITER
#undef VEC_ITER

// std::pair<iterator, bool> insert(const value_type& val);
// std::pair<iterator, bool> insert(value_type&& val);
// set insert: val;
template<typename t>
  ORet OSet<t>::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("insert:"));
    _MUTABLE_REC();
    const t value = g_castValue<t>(argv, m);
    _RETHROW_HERE();
    auto rv = this->insert(value);
    return packInsertResult(_ox_method_name, rv, m);
  }

template<>
  ORet OSet<string>::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<string>("insert:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(0, CLSID_STRING, _ox_method_name);
    _UNPACK_CONST_REF(String, value, 0);
    auto rv = this->insert(value);
    return packInsertResult(_ox_method_name, rv, m);
  }

// iterator insert (const_iterator position, const value_type& val);
// set insert: val hint: position;
template<typename t>
  ORet OSet<t>::ommInsertHint(OArg rec, int argc, OArg* argv, Session* m) {
    // using setiter_t = typename std::set<t>::iterator;
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("insert:hint:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(1, BTI::getSetIteratorClassId<t>(), _ox_method_name);
    // unpack arguments
    // const setiter_t& pos = *_PSetIter(t, _ARG(1));
    const auto& pos = _ARG(1)->castRef<_O(SetIterator)<t>>();
    _DEBUG(                                                   \
      if (!validateIterator(*static_cast<set_t*>(this), pos)) \
        _THROW_RTE("iterator does not belong to set");        \
    )
    const t value = g_castValue<t>(argv, m);
    _CHECK_EXCEPTION();
    // pack result
    ORef r = s_getSetIteratorClass<t>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<_O(SetIterator)<t>>() = this->insert(pos, value);
    return r->disposableResult();
  }

template<>
  ORet OSet<string>::ommInsertHint(OArg rec, int argc, OArg* argv, Session* m) {
    // using setiter_t = typename std::set<string>::iterator;
    _ENTER_METHOD_NOCHK(s_getFullMethodName<string>("insert:hint:"));
    _MUTABLE_REC();
    _CHECK_BAD_ARG(1, BTI::getSetIteratorClassId<string>(), _ox_method_name);
    // unpack arguments
    // const setiter_t& pos = *_PSetIter(string, _ARG(1));
    const auto& pos = _ARG(1)->castRef<_O(SetIterator)<string>>();
    _DEBUG(                                                   \
      if (!validateIterator(*static_cast<set_t*>(this), pos)) \
        _THROW_RTE("iterator does not belong to set");        \
    )
    _CHECK_BAD_ARG(0, CLSID_STRING, _ox_method_name);
    // _UNPACK_CONST_REF(String, value, 0);
    const auto& value = _ARG(0)->castRef<oxString>();
    // pack result
    ORef r = s_getSetIteratorClass<string>()->createObject(m);
    _CHECK_EXCEPTION();
    r->castRef<_O(SetIterator)<string>>() = this->insert(pos, value);
    return r->disposableResult();
  }

// void insert(InputIterator first, InputIterator last)
template<typename t> // t = ulong, long, double
  ORet OSet<t>::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("insertFrom:to:"));
    _MUTABLE_REC();
    if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
      _THROW_RTE("iterator type mismatch; expected '"+_ARG(0)->getClassName()+
                 "' found '"+_ARG(1)->getClassName()+"'");
    // handle compatible types
    if (insertTypeRange(argv))
      return makeResult();
    // run iteration loop
    OSetInsertLoop<t, std::set<t>> loop{*this};
    loop.run(argv, m);
    _RETHROW(s_getFullMethodName<t>("insertFrom:to:"));
    return makeResult();
  }

template<>
  ORet OSet<string>::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
    // using setiter_t = typename std::set<string>::iterator;
    _ENTER_METHOD_NOCHK(s_getFullMethodName<string>("insertFrom:to:"));
    _MUTABLE_REC();
    if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
      _THROW_RTE("iterator type mismatch; expected '"+_ARG(0)->getClassName()+
                 "' found '"+_ARG(1)->getClassName()+"'");
    // handle compatible types
    if (insertTypeRange(argv))
      return makeResult();
    // run iteration loop
    OSetInsertLoop<string, std::set<string>> loop{*this,
        [m]() -> std::pair<string, string> {
          string methodName = s_getFullMethodName<string>("insertFrom:to:");
          return {BTI::getSetClass<string>()->name, methodName};
        }
    };
    loop.run(argv, m);
    _RETHROW(s_getFullMethodName<string>("insertFrom:to:"));
    return makeResult();
  }

//:::::::::::::::::::::::::::::   set -- swap:   ::::::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD(s_getFullMethodName<t>("swap:"), 1);
    _MUTABLE_REC();
    _MUTABLE_ARG(0);
    _ASSERT(getRootClass() == BTI::getSetClass<t>(), m);
    _CHECK_ARG_CLASS(0, getRootClass());
    std::set<t>& r = *_PSet(t, _ARG(0));
    this->swap(r);
    return makeResult();
  }

//::::::::::::::::::::::::::   set -- begin, end   ::::::::::::::::::::::::::://

static const char* prefix[]{"", "r"};

template<typename _Set>
  string _methodName(const string& name, bool) noexcept
  { return BTI::getSetClass<typename _Set::value_type>()->name+"::"+name; }
template<>
  string _methodName<ObjectSet>(const string& name, bool owner) noexcept
  { return (owner ? BTI::getObjectSetClass()
                  : BTI::getSetClass<ORef>())->name+"::"+name; }

template<typename _Set, int _End, int _Rev>
  ORet _genericIterator(_Set* _this, Session* m) {
    using t = typename _Set::value_type;
    ORef r = s_getSetIteratorClass<t>()->createObject(m);
    _RETHROW(_methodName<_Set>(prefix[_Rev]+string{_End ? "end" : "begin"},
        _this->isOwner()));
    auto& it = r->castRef<_O(SetIterator)<t>>();
    if constexpr (_Rev) {
      if constexpr (_End)
        it = _this->begin();
      else
        it = _this->end();
    }
    else {
      if constexpr (_End)
        it = _this->end();
      else
        it = _this->begin();
    }
    static_cast<_O(SetIterator)<t>*>(r)->setReverse(_Rev);
    return r->disposableResult();
  }

template<typename t>
  ORet OSet<t>::ommBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<OSet<t>, 0, 0>(this, m); }
template<typename t>
  ORet OSet<t>::ommEnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<OSet<t>, 1, 0>(this, m); }
template<typename t>
  ORet OSet<t>::ommRBegin(OArg, int, OArg*, Session* m)
  { return _genericIterator<OSet<t>, 0, 1>(this, m); }
template<typename t>
  ORet OSet<t>::ommREnd(OArg, int, OArg*, Session* m)
  { return _genericIterator<OSet<t>, 1, 1>(this, m); }

//:::::::::::::::::::::::::::   set -- set type   :::::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommSetType(OArg rec, int argc, OArg*, Session* m) {
    _ENTER_METHOD(s_getFullMethodName<t>("type:"), 1);
    _THROW_RTE("the type of the set is already defined")
    return makeResult();
  }

//:::::::::::::::::::::::::   set -- compare info   :::::::::::::::::::::::::://

template<typename t>
  ORet OSet<t>::ommCompareInfo(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("compareInfo"));
    OClass* const stringClass = m->classMgr->getClass(CLSID_STRING);
    ORef result = BTI::getObjectVectorClass()->createObject(m);
    _CHECK_EXCEPTION();
    // std::vector<ORef>& v = *static_cast<std::vector<ORef>*>(_PObjVector(result));
    std::vector<ORef>& v = result->castRef<ObjectVector>();
    // 0: initialized
    // 1: element type
    // 2: method
    v.resize(3);
    for (auto i : {0, 1, 2}) {
      v[i] = stringClass->createObject(m);
      if (m->exceptionPending()) break;
    }
    _CHECK_EXCEPTION();
    *_PStr(v[0]) = "default";
    *_PStr(v[1]) = std::is_same_v<t, string> ? stringClass->name :
                                               BTI::getTypeClass<t>()->name;
    *_PStr(v[2]) = "default";
    return result->disposableResult();
  }

//::::::::::::::::::::::::::::::::   Ranges   :::::::::::::::::::::::::::::::://

template<typename _Set, int _Rev>
  ORet _genericAll(_Set* _this, Session* m) {
    using t = typename _Set::value_type;
    ORef b, e, r;
    b = BTI::getSetIteratorClass<t>()->createObject(m);
    if (!m->exceptionPending()) {
      e = BTI::getSetIteratorClass<t>()->createObject(m);
      if (!m->exceptionPending())
        r = ORange::getRangeClass()->createObject(m);
    }
    _RETHROW(_methodName<_Set>(prefix[_Rev]+string{"all"}, _this->isOwner()));
    if constexpr (_Rev) {
      static_cast<_O(SetIterator)<t>*>(b)->setReverse(true);
      static_cast<_O(SetIterator)<t>*>(e)->setReverse(true);
      b->castRef<_O(SetIterator)<t>>() = _this->rbegin().base();
      e->castRef<_O(SetIterator)<t>>() = _this->rend().base();
    }
    else {
      b->castRef<_O(SetIterator)<t>>() = _this->begin();
      e->castRef<_O(SetIterator)<t>>() = _this->end();
    }
    static_cast<ORange*>(r)->addPipelineAll(m, b, e);
    return r->disposableResult();
  }

template<typename t>
  ORet OSet<t>::ommAll(OArg, int, OArg*, Session* m)
  { return _genericAll<OSet<t>, 0>(this, m); }

template<typename t>
ORet OSet<t>::ommRAll(OArg, int, OArg*, Session* m)
  { return _genericAll<OSet<t>, 1>(this, m); }

//:::::::::::::::::::::::::::::   Method Table   ::::::::::::::::::::::::::::://

#define METHOD_TABLE_FOR_set(t)                             \
OM_BEGIN_TEMPLATE_METHOD_TABLE_FOR(OSet<t>)                 \
  PUBLIC (OSet<t>::ommAll,            "all"               ) \
  PUBLIC (OSet<t>::ommRAll,           "rall"              ) \
  PUBLIC (OSet<t>::ommBegin,          "begin"             ) \
  PUBLIC (OSet<t>::ommBegin,          "cbegin"            ) \
  PUBLIC (OSet<t>::ommRBegin,         "rbegin"            ) \
  PUBLIC (OSet<t>::ommRBegin,         "crbegin"           ) \
  PUBLIC (OSet<t>::ommClear,          "clear"             ) \
  PUBLIC (OSet<t>::ommCompareInfo,    "compareInfo"       ) \
  PUBLIC (OSet<t>::ommCount,          "count:"            ) \
  PUBLIC (OSet<t>::ommEmpty,          "empty"             ) \
  PUBLIC (OSet<t>::ommEnd,            "end"               ) \
  PUBLIC (OSet<t>::ommEnd,            "cend"              ) \
  PUBLIC (OSet<t>::ommREnd,           "rend"              ) \
  PUBLIC (OSet<t>::ommREnd,           "crend"             ) \
  PUBLIC (OSet<t>::ommFind,           "find:"             ) \
  PUBLIC (OSet<t>::ommErase,          "erase:"            ) \
  PUBLIC (OSet<t>::ommEraseAt,        "eraseAt:"          ) \
  PUBLIC (OSet<t>::ommEraseRange,     "eraseFrom:to:"     ) \
  PUBLIC (OSet<t>::ommInsert,         "insert:"           ) \
  PUBLIC (OSet<t>::ommInsertHint,     "insert:hint:"      ) \
  PUBLIC (OSet<t>::ommInsertRange,    "insertFrom:to:"    ) \
  PUBLIC (OSet<t>::ommSetType,        "type:"             ) \
  PUBLIC (OSet<t>::ommSize,           "size"              ) \
  PUBLIC (OSet<t>::ommSwap,           "swap:"             ) \
_END_METHOD_TABLE()

METHOD_TABLE_FOR_set(ulong);
METHOD_TABLE_FOR_set(long);
METHOD_TABLE_FOR_set(double);
METHOD_TABLE_FOR_set(string);

//::::::::::::::::::::::::::   Metaclass Methods   ::::::::::::::::::::::::::://

template<typename t>
  ORef OSet_Class<t>::copyCore(ORef self, const_ORef r, Session* m) const {
    if (r->getRootClass() != getRootClass()) return nullptr;
    *_PSet(t, self) = *_PCSet(t, r);
    return self;
  }

template<typename t>
  ORef OSet_Class<t>::moveCore(ORef self, ORef r) const {
    if (r->getRootClass() != getRootClass()) return nullptr;
    *_PSet(t, self) = std::move(*_PSet(t, r));
    return self;
  }

template<typename t>
  OClass* OSet_Class<t>::createClass(Session* m,
      const std::string& className) const {
    OClass* cls = new OSet_Class(m, this->name, className);
    if (m->exceptionPending()) {
      OClass* const type = std::is_same_v<t, string> ?
          m->classMgr->getClass(CLSID_STRING) : BTI::getTypeClass<t>();
      m->rethrow("OSet_Class<"+type->name+">::createClass");
    }
    return cls;
  }

template<typename t>
  OClass* OSet_Class<t>::createMutableClass(Session* m,
      const std::string& className) const {
    OClass* cls = new OSet_Class(m, this, className);
    if (m->exceptionPending()) {
      OClass* const type = std::is_same_v<t, string> ?
          m->classMgr->getClass(CLSID_STRING) : BTI::getTypeClass<t>();
      m->rethrow("OSet_Class<"+type->name+">::createMutableClass");
    }
    return cls;
  }

//::::::::::::::::::::::::   Template Instantiation   :::::::::::::::::::::::://

template class OSet<ulong>;
template class OSet<long>;
template class OSet<double>;
template class OSet<string>;

template class OSet_Class<ulong>;
template class OSet_Class<long>;
template class OSet_Class<double>;
template class OSet_Class<string>;

//============================================================================//
//::::::::::::::::::::::::::::::   ObjectSet   ::::::::::::::::::::::::::::::://
//============================================================================//
// TODO validation (use Session::diagnostics)

//::::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://

string ObjectSet::getFullMethodName(const string& methodName) const {
  return (isOwner() ? BTI::getObjectSetClass()->name :
      BTI::getSetClass<ORef>()->name)+"::"+methodName;
}

void ObjectSet::clearContainer(Session* m) {
  std::vector<ORef> items{begin(), end()};
  clear();
  ObjectAutoCleaner cleaner{m, items};
}

ORet ObjectSet::copyItems(const ObjectSet& rhs, Session* m) {
  _ENTER_NOCHK("ObjectSet::copyItems");
  _ASSERT(this->size() == 0, m);
  auto& lhs = *this;
  const bool owner = isOwner();
  OClass* const rhsSetType = m->classMgr->getClass(rhs.typeId_);
  ORefSetPrepare prepare{m, lhs.compare_, lhs.typeId_};
  size_t i = 0;
  auto hint = lhs.begin();
  for (const auto rhsItem : rhs) {
    if (i++ == 0 && (!rhsSetType                    ||
                     !rhs.compare_                  ||
                     !rhs.compare_->compare.valid() ||
                     !prepare.copy(rhsSetType, rhs.compare_->compare))) {
      m->throwBadCoreCopy(_ox_method_name, "cannot copy set");
      return undefinedResult();
    }
    ORef value;
    if (owner) {
      OClass* itemClass = rhsItem->getClass();
      value = itemClass->createObject(m);
      _CHECK_EXCEPTION();
      itemClass->copyObject(value, rhsItem, m);
      _CHECK_EXCEPTION();
    }
    else
      value = rhsItem;
    hint = lhs.insert(hint, value);
  }
  return makeResult();
}

#define VEC_ITER(t, n) _ARG(n)->castRef<_O(VectorIterator)<t>>()
#define SET_ITER(t, n) _ARG(n)->castRef<_O(SetIterator)<t>>()

bool ObjectSet::insertRange(OArg* argv, Session* m) {
  const bool owner = isOwner();
  size_t i = size();
  ORefSetPrepare prepare{m, compare_, typeId_};
  auto transform = [this, owner, m, &i, &prepare](ORef e) {
    ORef value = e;
    if (owner) {
      value = m->duplicateObject(e);
      if (m->exceptionPending()) throw string{"error"};
    }
    if (!prepare.insert(i, value)) {
      prepare.insertError(getFullMethodName("insertFrom:to:"), value);
      throw string{"error"};
    }
    ++i;
    return value;
  };
  auto hint = begin();
  switch (_ARG(0)->getClassId()) {
    case CLSID_OBJVECITER:
      for (auto it = VEC_ITER(ORef, 0); it != VEC_ITER(ORef, 1); ++it)
        hint = this->insert(hint, transform(*it));
      break;
    case CLSID_OBJSETITER:
      for (auto it = SET_ITER(ORef, 0); it != SET_ITER(ORef, 1); ++it)
        hint = this->insert(hint, transform(*it));
      break;
    default:
      return false;
  }
  return true;
}

#undef SET_ITER
#undef VEC_ITER

//::::::::::::::::::::::::::   set -- constructor   :::::::::::::::::::::::::://

ObjectSet::ObjectSet(OClass* c) : OInstance(c), compare_{nullptr}, typeId_{0} {
  OInstance probe{nullptr};
  // first insertion does not call ORefSetCompare::operator()
  insert(&probe);
  // second try calls ORefSetCompare::operator() twice
  insert(&probe);
  // the first comparison sets the class field to the comparison structure
  compare_ = reinterpret_cast<ORefSetCompare*>(probe.getClass());
  // clear the container
  clear();
}

//::::::::::::::   object set -- init: initialize components   ::::::::::::::://

ORet ObjectSet::ommInit(OArg rec, int argc, OArg* argv, Session* m) {
  _ASSERT(this->empty(), m);
  for (int i = 0; i < argc; ++i) {
    ORef value;
    if (isOwner()) {
      value = m->dupRenderObject(argv[i]);
      _RETHROW(getFullMethodName("init:"));
    }
    else
      value = _ARG(i);
    ORefSetPrepare prepare{m, compare_, typeId_};
    if (!prepare.insert(i, value)) {
      prepare.insertError(getFullMethodName("init:"), value);
      return undefinedResult();
    }
    try {
      this->insert(value);
    }
    catch (const string&) {
      _ASSERT(m->exceptionPending(), m);
      _RETHROW(getFullMethodName("init:"));
    }
  }
  return makeResult();
}

//:::::::::::::::::::::::::   object set -- count   :::::::::::::::::::::::::://

ORet ObjectSet::ommCount(OArg rec, int, OArg* argv, Session* m) {
  ORef value = _ARG(0);
  _RETHROW(getFullMethodName("count:"));
  ORef r = OBasicTypeInfo::getTypeClass<size_t>()->createObject(m);
  _RETHROW(getFullMethodName("count:"));
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    _P(size_t, r)->setValue(this->count(value));
  }
  catch (const string&) {
    m->deleteObject(r);
    _RETHROW(getFullMethodName("count:"));
  }
  return r->disposableResult();
}

//::::::::::::::::::::::::::   object set -- size   :::::::::::::::::::::::::://

// rec size;
ORet ObjectSet::ommSize(OArg rec, int, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<size_t>()->createObject(m);
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    _P(size_t, r)->setValue(this->size());
  }
  catch (const string&) {
    m->deleteObject(r);
    _RETHROW(getFullMethodName("size"));
  }
  return r->disposableResult();
}

//::::::::::::::::::::::::::   object set -- find   :::::::::::::::::::::::::://

// iterator find( const Key& key );
ORet ObjectSet::ommFind(OArg rec, int argc, OArg* argv, Session* m) {
  ORef value = _ARG(0);
  ORef r = s_getSetIteratorClass<ORef>()->createObject(m);
  _RETHROW(getFullMethodName("find:"));
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    // *_PSetIter(ORef, r) = this->find(value);
    r->castRef<_O(SetIterator)<ORef>>() = this->find(value);
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("find:"));
  }
  return r->disposableResult();
}

//:::::::::::::::::::::::::   object set -- empty   :::::::::::::::::::::::::://

// rec empty;
ORet ObjectSet::ommEmpty(OArg rec, int, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW(getFullMethodName("empty"));
  _P(bool, r)->setValue(this->empty());
  return r->disposableResult();
}

//:::::::::::::::::::::::::   object set -- erase   :::::::::::::::::::::::::://

// size_type erase(const Key& key);
ORet ObjectSet::ommErase(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("erase:"));
  // though the first condition inside the if-expression is theoretically
  // useless, as it is implicit in the second, it is there for performance
  // reasons
  if (_ARG(0)->getClassId() != typeId_ &&
      !_ARG(0)->getClass()->extends(m->getClass(typeId_)))
    return m->badArgTypeId(getFullMethodName("erase:"), 0, typeId_, argv);
  ORef key = _ARG(0);
  // do the call
  size_type retValue = 0;
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    auto it = this->find(key);
    if (it != this->end()) {
      ORef value = *it;
      this->erase(it);
      if (isOwner()) {
        m->deleteObject(value);
        _RETHROW(getFullMethodName("erase:"));
      }
      retValue = 1;
    }
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("erase:"));
  }
  // pack return value
  ORef r = BTI::getTypeClass<size_type>()->createObject(m);
  _RETHROW(getFullMethodName("erase:"));
  _P(size_type, r)->setValue(retValue);
  return r->disposableResult();
}

//iterator erase(const_iterator position);
ORet ObjectSet::ommEraseAt(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("eraseAt:"));
  // using setiter_t = oset_t::iterator;
  _CHECK_BAD_ARGTYPE(0, s_getSetIteratorClass<ORef>(),
                        getFullMethodName("eraseAt:"));
  const auto& it = _ARG(0)->castRef<_O(SetIterator)<ORef>>();
  // validate iterator
  _DEBUG(                                                   \
    if (!validateIterator(*static_cast<oset_t*>(this), it)) \
      _THROW_RTE_METHOD("iterator does not belong to set",  \
                        getFullMethodName("eraseAt:"));     \
  )
  // do the call
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    ORef key = it == this->end() ? nullptr : *it;
    auto retValue = this->erase(it);
    if (isOwner() && retValue != this->end() && key != nullptr) {
      m->deleteObject(key);
      _RETHROW(getFullMethodName("eraseAt:"));
    }
    // pack return value
    ORef r = s_getSetIteratorClass<ORef>()->createObject(m);
    _RETHROW(getFullMethodName("eraseAt:"));
    // *static_cast<oset_t::iterator*>(_PSetIter(ORef, r)) = std::move(retValue);
    r->castRef<_O(SetIterator)<ORef>>() = std::move(retValue);
    return r->disposableResult();
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("eraseAt:"));
  }
  return undefinedResult();
}

//iterator erase(const_iterator first, const_iterator last);
ORet ObjectSet::ommEraseRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("eraseFrom:to:"));
  // using setiter_t = oset_t::iterator;
  _CHECK_BAD_ARGTYPE(0, s_getSetIteratorClass<ORef>(),
                        getFullMethodName("eraseFrom:to:"));
  _CHECK_BAD_ARGTYPE(1, s_getSetIteratorClass<ORef>(),
                        getFullMethodName("eraseFrom:to:"));
  const auto& from = _ARG(0)->castRef<_O(SetIterator)<ORef>>();
  const auto& to   = _ARG(1)->castRef<_O(SetIterator)<ORef>>();
  // validate iterators
  _DEBUG(                                                            \
    if (!validateIterators(*static_cast<oset_t*>(this), {from, to})) \
      _THROW_RTE_METHOD("iterator does not belong to set",           \
                        getFullMethodName("eraseFrom:to:"));         \
  )
  // store elements to delete
  std::vector<ORef> toDelete;
  if (isOwner())
    toDelete.assign(from, to);
  // do the call
  ORefSetPrepare{m, compare_, typeId_}.find();
  try {
    auto retValue = this->erase(from, to);
    for (auto it = toDelete.begin(); it != toDelete.end(); ++it) {
      m->deleteObject(*it);
      _RETHROW(getFullMethodName("eraseFrom:to:"));
    }
    // pack return value
    ORef r = s_getSetIteratorClass<ORef>()->createObject(m);
    _RETHROW(getFullMethodName("eraseFrom:to:"));
    r->castRef<_O(SetIterator)<ORef>>() = std::move(retValue);
    return r->disposableResult();
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("eraseFrom:to:"));
  }
  return undefinedResult();
}

//:::::::::::::::::::::::::   object set -- clear   :::::::::::::::::::::::::://

ORet ObjectSet::ommClear(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("clear"));
  if (isOwner()) {
    clearContainer(m);
    _RETHROW(getFullMethodName("clear"));
  }
  else
    this->clear();
  return makeResult();
}

//:::::::::::::::::::::::::   object set -- insert   ::::::::::::::::::::::::://

// std::pair<iterator, bool> insert(const value_type& val);
// std::pair<iterator, bool> insert(value_type&& val);
// set insert: val;
ORet ObjectSet::ommInsert(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("insert:"));
  // using setiter_t = oset_t::iterator;
  ORefSetPrepare prepare{m, compare_, typeId_};
  if (!prepare.insert(this->size(), _ARG(0))) {
    prepare.insertError(getFullMethodName("insert:"), _ARG(0));
    return undefinedResult();
  }
  ORef value = isOwner() ? m->dupRenderObject(argv[0]) : _ARG(0);
  _RETHROW(getFullMethodName("insert:"));
  try {
    auto rv = this->insert(value);
    // pack result
    ORet result = s_packInsertResult<ORef, oset_t::iterator>(rv, m);
    _RETHROW(getFullMethodName("insert:"));
    return result;
  }
  catch (const string&) {
    _ASSERT(m->exceptionPending(), m);
    _RETHROW(getFullMethodName("insert:"));
  }
  return undefinedResult();
}

// iterator insert (const_iterator position, const value_type& val);
// set insert: val hint: position;
ORet ObjectSet::ommInsertHint(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("insert:hint:"));
  // using setiter_t = oset_t::iterator;
  // check arguments
  _CHECK_BAD_ARGTYPE(1, s_getSetIteratorClass<ORef>(),
                        getFullMethodName("insert:hint:"));
  if (!isOwner() && argv[0]->disposable()) {
    const string msg = "you cannot add temporary objects to a "
                       "container type '"+getRootClass()->name+"'";
    _THROW_RTE_METHOD(getFullMethodName("insert:hint:"), msg);
  }
  // prepare insert
  ORefSetPrepare prepare{m, compare_, typeId_};
  if (!prepare.insert(this->size(), _ARG(0))) {
    prepare.insertError(getFullMethodName("insert:hint:"), _ARG(0));
    return undefinedResult();
  }
  // unpack arguments
  ORef value = isOwner() ? m->dupRenderObject(argv[0]) : _ARG(0);
  _RETHROW(getFullMethodName("insert:hint:"));
  // const setiter_t& pos = *_PSetIter(ORef, _ARG(1));
  const auto& pos = _ARG(1)->castRef<_O(SetIterator)<ORef>>();
  _DEBUG(                                                    \
    if (!validateIterator(*static_cast<oset_t*>(this), pos)) \
      _THROW_RTE_METHOD("iterator does not belong to set",   \
                        getFullMethodName("insert:hint:"));  \
  )
  // do the call
  try {
    auto it = this->insert(pos, value);
    // pack result
    ORef r = s_getSetIteratorClass<ORef>()->createObject(m);
    _RETHROW(getFullMethodName("insert:hint:"));
    // *_PSetIter(ORef, r) = std::move(it);
    r->castRef<_O(SetIterator)<ORef>>() = std::move(it);
    return r->disposableResult();
  }
  catch (const string&) {
    _ASSERT(m->exceptionPending(), m);
    m->rethrow(getFullMethodName("insert:hint:"));
  }
  return undefinedResult();
}

template<typename _Cont>
  struct ObjectSetLoop : OBasicLoop, ValueExtractor<ORef> {
    using setiter_t = typename _Cont::iterator;
    _Cont& container;
    ORefSetPrepare& prepare;
    setiter_t hint;
    size_t i;
    getmsg_t getNames;

    ObjectSetLoop(_Cont& c, ORefSetPrepare& prepare, bool owner, getmsg_t f) :
        container{c}, prepare{prepare}, hint{c.begin()},
        i{c.size()}, getNames{f}, ValueExtractor<ORef>{owner, f} {}

    virtual void action(OVar result, Session* m) noexcept override {
      ORef value = this->extractValue(result, m);
      if (m->exceptionPending()) return;
      if (!prepare.insert(i, value)) {
        auto names = getNames();
        prepare.insertError(names.second, value);
        return;
      }
      hint = container.insert(hint, value);
      ++i;
    }
  };

// void insert(InputIterator first, InputIterator last)
ORet ObjectSet::ommInsertRange(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("insertFrom:to:"));
  if (_ARG(0)->getClassId() != _ARG(1)->getClassId())
    _THROW_RTE_METHOD(getFullMethodName("insertFrom:to:"),
        "iterator type mismatch; expected '"+_ARG(0)->getClassName()+"' "
        "found '"+_ARG(1)->getClassName()+"'");
  // try to handle compatible types
  try {
    const bool handled = insertRange(argv, m);
    if (handled) return this->makeResult();
  }
  catch (const string&) {
    _RETHROW(getFullMethodName("insertFrom:to:"));
  }
  // run iteration loop
  ORefSetPrepare prepare{m, compare_, typeId_};
  ObjectSetLoop<oset_t> loop{*this, prepare, isOwner(),
      [this]() -> std::pair<string, string> {
        string methodName = getFullMethodName("insertFrom:to:");
        return {getRootClass()->name, methodName};
      }
  };
  loop.run(argv, m);
  _RETHROW(getFullMethodName("insertFrom:to:"));
  return this->makeResult();
}

//::::::::::::::::::::::::::   object set -- swap   :::::::::::::::::::::::::://

void ObjectSet::swap(ObjectSet& rhs) {
  static_cast<oset_t*>(this)->swap(rhs);
  std::swap(compare_, rhs.compare_);
  std::swap(typeId_, rhs.typeId_);
}

ORet ObjectSet::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("swap:"));
  _MUTABLE_ARG_METHOD(0, getFullMethodName("swap:"));
  _CHECK_ARGC_METHOD(getFullMethodName("swap:"), 1);
  _ASSERT(getRootClass() == BTI::getObjectSetClass(), m);
  _CHECK_ARG_CLASS_METHOD(0, getRootClass(), getFullMethodName("swap:"));
  ObjectSet& rhs = *static_cast<ObjectSet*>(_ARG(0));
  this->swap(rhs);
  return makeResult();
}

//:::::::::::::::::::::::   object set -- begin, end   ::::::::::::::::::::::://

// ORet ObjectSet::ommBegin(int, OArg*, Session* m) {
  // ORef r = BTI::getSetIteratorClass<ORef>()->createObject(m);
  // _RETHROW(getFullMethodName("begin"));
  // r->castRef<_O(SetIterator)<ORef>>() = this->begin();
  // return r->disposableResult();
// }

// ORet ObjectSet::ommEnd(int, OArg*, Session* m) {
  // ORef r = BTI::getSetIteratorClass<ORef>()->createObject(m);
  // _RETHROW(getFullMethodName("end"));
  // r->castRef<_O(SetIterator)<ORef>>() = this->end();
  // return r->disposableResult();
// }

ORet ObjectSet::ommBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ObjectSet, 0, 0>(this, m); }
ORet ObjectSet::ommEnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ObjectSet, 1, 0>(this, m); }
ORet ObjectSet::ommRBegin(OArg, int, OArg*, Session* m)
{ return _genericIterator<ObjectSet, 0, 1>(this, m); }
ORet ObjectSet::ommREnd(OArg, int, OArg*, Session* m)
{ return _genericIterator<ObjectSet, 1, 1>(this, m); }

//:::::::::::::::::::::::::::   set -- set type   :::::::::::::::::::::::::::://

ORet ObjectSet::ommSetType(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(getFullMethodName("type:"));
  _CHECK_ARGC_METHOD(getFullMethodName("type:"), 1);
  _CHECK_BAD_ARG(0, CLSID_STRING, getFullMethodName("type:"));
  _UNPACK_CONST_REF(String, type, 0);
  if (typeId_ != 0)
    _THROW_RTE_METHOD(getFullMethodName("type:"),
                      "the type of the set is already defined")
  if (!compare_ || !compare_->initialized)
    _THROW_RTE_METHOD(getFullMethodName("type:"), "improperly constructed set")
  if (compare_->compare.valid())
    _THROW_RTE_METHOD(getFullMethodName("type:"), "improperly initialized set")
  const OClass* const c = m->classMgr->getClass(type);
  if (!c)
    _THROW_RTE_METHOD(getFullMethodName("type:"), "there is no type '"+type+"'")
  else if (!c->getMethodAddressByName("compare:", &compare_->compare))
    _THROW_RTE_METHOD(getFullMethodName("type:"), "the type '"+type+"' "
                      "does not support the method 'compare:'")
  typeId_ = c->getClassId();
  return makeResult();
}

//:::::::::::::::::::::::::   set -- compare info   :::::::::::::::::::::::::://

ORet ObjectSet::ommCompareInfo(OArg rec, int argc, OArg* argv, Session* m) {
  OClass* const stringClass = m->classMgr->getClass(CLSID_STRING);
  ORef result = BTI::getObjectVectorClass()->createObject(m);
  _RETHROW(getFullMethodName("compareInfo"));
  // std::vector<ORef>& v = *static_cast<std::vector<ORef>*>(_PObjVector(result));
  std::vector<ORef>& v = result->castRef<ObjectVector>();
  // 0: initialized
  // 1: element type
  // 2: method
  v.resize(3);
  for (auto i : {0, 1, 2}) {
    v[i] = stringClass->createObject(m);
    if (m->exceptionPending()) break;
  }
  _RETHROW(getFullMethodName("compareInfo"));
  if (!this->compare_ || !typeId_) {
    string initialized = this->compare_ ?
        string{compare_->initialized ? "" : "not "}+"initialized" :
        "improperly constructed set";
    OClass* typeClass = typeId_ ? m->classMgr->getClass(typeId_) : nullptr;
    OMethodAddress nullMethod;
    const auto& cmp = compare_ ? compare_->compare : nullMethod;
    *_PStr(v[0]) = initialized;
    *_PStr(v[1]) = typeClass ? typeClass->name : "unknown type";
    *_PStr(v[2]) = cmp.notValid() ? string{"none"} : (
          cmp.scriptMethod ? std::to_string(cmp.scriptMethodAddress.block) :
          "core method");
  }
  else {
    OClass* typeClass = m->classMgr->getClass(typeId_);
    *_PStr(v[0]) = string{compare_->initialized ? "" : "not "}+"initialized";
    *_PStr(v[1]) = typeClass ? typeClass->name : "unknown type";
    const auto& cmp = compare_->compare;
    *_PStr(v[2]) = cmp.notValid() ? string{"none"} : (
          cmp.scriptMethod ? std::to_string(cmp.scriptMethodAddress.block) :
          "core method");
  }
  return result->disposableResult();
}

//::::::::::::::::::::::::::::::::   Ranges   :::::::::::::::::::::::::::::::://

ORet ObjectSet::ommAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ObjectSet, 0>(this, m); }

ORet ObjectSet::ommRAll(OArg, int, OArg*, Session* m)
{ return _genericAll<ObjectSet, 1>(this, m); }

//:::::::::::::::::::::::::::::   Method Table   ::::::::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(ObjectSet, OInstance)
  PUBLIC (ObjectSet::ommAll,         "all"           )
  PUBLIC (ObjectSet::ommRAll,        "rall"          )
  PUBLIC (ObjectSet::ommBegin,       "begin"         )
  PUBLIC (ObjectSet::ommBegin,       "cbegin"        )
  PUBLIC (ObjectSet::ommRBegin,      "rbegin"        )
  PUBLIC (ObjectSet::ommRBegin,      "crbegin"       )
  PUBLIC (ObjectSet::ommClear,       "clear"         )
  PUBLIC (ObjectSet::ommCompareInfo, "compareInfo"   )
  PUBLIC (ObjectSet::ommCount,       "count:"        )
  PUBLIC (ObjectSet::ommEmpty,       "empty"         )
  PUBLIC (ObjectSet::ommEnd,         "end"           )
  PUBLIC (ObjectSet::ommEnd,         "cend"          )
  PUBLIC (ObjectSet::ommREnd,        "rend"          )
  PUBLIC (ObjectSet::ommREnd,        "crend"         )
  PUBLIC (ObjectSet::ommFind,        "find:"         )
  PUBLIC (ObjectSet::ommErase,       "erase:"        )
  PUBLIC (ObjectSet::ommEraseAt,     "eraseAt:"      )
  PUBLIC (ObjectSet::ommEraseRange,  "eraseFrom:to:" )
  PUBLIC (ObjectSet::ommInsert,      "insert:"       )
  PUBLIC (ObjectSet::ommInsertHint,  "insert:hint:"  )
  PUBLIC (ObjectSet::ommInsertRange, "insertFrom:to:")
  PUBLIC (ObjectSet::ommSetType,     "type:"         )
  PUBLIC (ObjectSet::ommSize,        "size"          )
  PUBLIC (ObjectSet::ommSwap,        "swap:"         )
_END_METHOD_TABLE()

//::::::::::::::::::::::::::   Metaclass Methods   ::::::::::::::::::::::::::://

void ObjectSet_Class::destructCore(ORef self, Session* m) {
  ObjectSet* _this = static_cast<ObjectSet*>(self);
  if (_this->isOwner())
    _this->clearContainer(m);
  if (!freeCached<ObjectSet>(self)) delete _this;
}

ORef ObjectSet_Class::copyCore(ORef self, const_ORef r, Session* m) const {
  _ENTER_NOCHK("ObjectSet_Class::copyCore");
  auto& lhs = *static_cast<ObjectSet*>(self);
  const auto& rhs = *static_cast<const ObjectSet*>(r);
  if (lhs.isOwner() != rhs.isOwner()) {
    m->throwBadCoreCopy(_ox_method_name, "incompatible set types");
    return nullptr;
  }
  if (lhs.isOwner()) {
    lhs.clearContainer(m);
    _RETHROW_HERE_RETURN(nullptr);
    lhs.copyItems(rhs, m);
    _RETHROW_HERE_RETURN(nullptr);
  }
  else {
    static_cast<oset_t&>(lhs) = static_cast<const oset_t&>(rhs);
  }
  return self;
}

ORef ObjectSet_Class::moveCore(ORef self, ORef r) const {
  if (r->getRootClass() != getRootClass()) return nullptr;
  ObjectSet* const _this = static_cast<ObjectSet*>(self);
  _this->swap(*static_cast<ObjectSet*>(r));
  return self;
}

OClass* ObjectSet_Class::createClass(Session* m,
    const string& className) const {
  _ENTER_NOCHK("ObjectSet_Class::createClass");
  OClass* cls = new ObjectSet_Class(m, this->name, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

OClass* ObjectSet_Class::createMutableClass(Session* m,
    const string& className) const {
  _ENTER_NOCHK("ObjectSet_Class::createMutableClass");
  OClass* cls = new ObjectSet_Class(m, this, className);
  _RETHROW_HERE_RETURN(nullptr);
  return cls;
}

//============================================================================//
//::::::::::::::::::::::::::::  Class Installer  ::::::::::::::::::::::::::::://
//============================================================================//

bool g_installSetClasses(Session* m) {
  extern OClass* _OXType_class[];
  OClass* cls;

  cls = new OSet_Class<ulong>(m, "ulong set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_ULONGSET);
  _OXType_class[CLSID_ULONGSET] = cls;

  cls = new OSet_Class<long>(m, "long set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_LONGSET);
  _OXType_class[CLSID_LONGSET] = cls;

  cls = new OSet_Class<double>(m, "double set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_DBLSET);
  _OXType_class[CLSID_DBLSET] = cls;

  cls = new OSet_Class<string>(m, "string set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_STRSET);
  _OXType_class[CLSID_STRSET] = cls;

  cls = new ObjectSet_Class(m, "reference set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_REFSET);
  _OXType_class[CLSID_REFSET] = cls;

  cls = new ObjectSet_Class(m, "set");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_OBJSET);
  _OXType_class[CLSID_OBJSET] = cls;

  return true;
}
