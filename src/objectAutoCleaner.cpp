#include "objectAutoCleaner.hpp"
#include "odev.h"

using std::string;
using std::vector;

std::string ul2hexstr(void*); // implemented in oclass.cpp

ObjectAutoCleaner::ObjectAutoCleaner(ObjectAutoCleaner&& cleaner) :
    m{cleaner.m}, objects{std::move(cleaner.objects)} {}

ObjectAutoCleaner::ObjectAutoCleaner(Session* m, const vector<ORef>& v) :
    m{m}, objects{v} {}

ObjectAutoCleaner::ObjectAutoCleaner(Session* m, vector<ORef>&& v) :
    m{m}, objects{std::move(v)} {}

ObjectAutoCleaner& ObjectAutoCleaner::operator=(ObjectAutoCleaner&& cleaner) {
  m = cleaner.m;
  objects = std::move(cleaner.objects);
  return *this;
}

ObjectAutoCleaner::ObjectAutoCleaner(Session* m, const OVariable& var) : m{m}
{ addDisposable(var); }

ObjectAutoCleaner::ObjectAutoCleaner(Session* m, OVariable&& var) : m{m}
{ addDisposable(std::move(var)); }

bool ObjectAutoCleaner::empty() const noexcept {
  for (const auto o : objects)
    if (o) return false;
  return true;
}

void ObjectAutoCleaner::add(ORef o) {
  if (o) objects.push_back(o);
}

void ObjectAutoCleaner::addDisposable(const OVariable& var) {
  if (var.disposable() && var.objectRef)
    objects.push_back(var.objectRef);
}

void ObjectAutoCleaner::addDisposable(OVariable&& var) {
  const bool disposable = var.disposable();
  ORef o = std::exchange(var.objectRef, nullptr);
  OM_ASSERT(var.objectRef == nullptr, m);
  if (disposable && o)
    objects.push_back(o);
}

void ObjectAutoCleaner::cancel() noexcept {
  objects.clear();
}

void ObjectAutoCleaner::cancel(const_ORef o) noexcept {
  auto it = std::find(objects.begin(), objects.end(), o);
  if (it != objects.end())
    *it = nullptr;
}

void ObjectAutoCleaner::now() {
  if (objects.empty()) return;
  for (auto& o : objects) {
    if (!o) continue;
    if (!m->isObject(o)) {
      m->throwRTE("ObjectAutoCleaner::now",
                  "encountered a non-object at 0x"+ul2hexstr(o));
      o = nullptr;
      break;
    }
    m->deleteObject(o);
    o = nullptr;
    if (m->exceptionPending()) break;
  }
  if (m->exceptionPending()) {
    pack();
    m->discardObjects(objects);
    objects.clear();
  }
}

void ObjectAutoCleaner::cleanup() {
  // remove and save current exception (if any) for further use
  ORef pendingException = m->popException();
  // clean up
  now();
  // clear exception thrown while cleaning up
  if (m->exceptionPending() && pendingException)
    m->discardObjects({m->popException()});
  else if (m->exceptionPending())
    pendingException = m->popException();
  // restore exception (if any)
  m->setException(pendingException);
}

template<typename t>
void packVector(vector<t>& v) {
  size_t d = 0;
  for (auto it = v.end(); it != v.begin();) {
    auto p = it - 1;
    size_t n = 0;
    while (!*p && p != v.begin() && !*(p - 1)) {
      --p;
      ++n;
    }
    if (!*p) {
      std::copy(it, v.end() - d, p);
      d += n + 1;
      it = p;
    }
    else
      --it;
  }
  v.erase(v.end() - d, v.end());
}

void ObjectAutoCleaner::pack() {
  packVector(objects);
}
