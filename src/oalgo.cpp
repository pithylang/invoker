#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "otype.hpp"
#include "oalgo.hpp"
#include "odev.h"

ORet OBasicLoop::run(OArg* argv, Session* m) noexcept {
  // get a working copy of first
  ORef it = _ARG(0)->getClass()->createObject(m);
  _CHECK_EXCEPTION_NO_RETHROW();
  OVar varIt{it};
  m->copyFrom(&varIt, argv[0]);
  it = REF(varIt);
  ObjectAutoCleaner itAutoCleaner{m, it};
  _CHECK_EXCEPTION_NO_RETHROW();
  // iterate over the range [first, last) performing action
  while (true) {
    // check if it == last
    auto result = m->execMethod("eq:", 0, it, 1, argv + 1);
    // no need to check result.second
    _CHECK_EXCEPTION_NO_RETHROW();
    const bool isEqual = _P(bool, REF(result.first))->getValue();
    if (result.first.disposable()) {
      m->deleteObject(REF(result.first));
      _CHECK_EXCEPTION_NO_RETHROW();
    }
    if (isEqual) break;
    // dereference it (get value pointed to by it)
    result = m->execMethod("value", 0, it, 0, nullptr);
    _CHECK_EXCEPTION_NO_RETHROW();
    ObjectAutoCleaner resultCleaner{m, result.first};
    // extract value & insert it into the container
    this->action(result.first, m);
    _CHECK_EXCEPTION_NO_RETHROW();
    // advance it (working copy of first)
    result = m->execMethod("inc", 0, it, 0, nullptr);
    _CHECK_EXCEPTION_NO_RETHROW();
  }
  // delete working copy
  itAutoCleaner.cancel();
  m->deleteObject(it);
  _CHECK_EXCEPTION_NO_RETHROW();
  // return result
  return this->getResult(m);
}

namespace om {

ORet runLoop(Session* m, OArg* argv,
             const std::function<void(OVar)>& loopAction,
             const std::function<ORet()>& resultGetter) noexcept {
  // get a working copy of first
  ORef it = _ARG(0)->getClass()->createObject(m);
  _CHECK_EXCEPTION_NO_RETHROW();
  OVar varIt{it};
  m->copyFrom(&varIt, argv[0]);
  it = REF(varIt);
  ObjectAutoCleaner itAutoCleaner{m, it};
  _CHECK_EXCEPTION_NO_RETHROW();
  // iterate over the range [first, last) performing action
  while (true) {
    // check if it == last
    auto result = m->execMethod("eq:", 0, it, 1, argv + 1);
    // no need to check result.second
    _CHECK_EXCEPTION_NO_RETHROW();
    const bool isEqual = _P(bool, REF(result.first))->getValue();
    if (result.first.disposable()) {
      m->deleteObject(REF(result.first));
      _CHECK_EXCEPTION_NO_RETHROW();
    }
    if (isEqual) break;
    // dereference it (get value pointed to by it)
    result = m->execMethod("value", 0, it, 0, nullptr);
    _CHECK_EXCEPTION_NO_RETHROW();
    ObjectAutoCleaner resultCleaner{m, result.first};
    // do action, e.g., extract value & insert it into the container
    loopAction(result.first);
    _CHECK_EXCEPTION_NO_RETHROW();
    // advance it (working copy of first)
    result = m->execMethod("inc", 0, it, 0, nullptr);
    _CHECK_EXCEPTION_NO_RETHROW();
  }
  // delete working copy
  itAutoCleaner.cancel();
  m->deleteObject(it);
  _CHECK_EXCEPTION_NO_RETHROW();
  // return result
  return resultGetter();
}

}

ORef ValueExtractor<ORef>::extractValue(OVar result, Session* m) noexcept {
  if (containerOwned)
    return m->dupRenderObject(&result);
  if (result.disposable()) {
    const auto [container, method] = getNames();
    m->throwRTE(method, "cannot add temporary objects to '"+container+"'");
    return nullptr;
  }
  return result.objectRef;
}
