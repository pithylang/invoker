#include <vector>
#include <algorithm>
#include "oattrs.hpp"
#include "omembers.hpp"
#include "ortmgr.hpp"

using std::string;
using std::vector;

vector<string> split(const string& str, const string& delim);
void g_Trim(string& s);

static const std::vector<Attr> s_attrFlags {
  {"public",    M_PUBLIC   },
  {"protected", M_PROTECTED},
  {"private",   M_PRIVATE  },
  {"const",     M_CONST    },
  {"constThis", M_CONST    },
  {"static",    M_STATIC   },
};

bool AttrParser::parse() {
  flags = 0;
  error = "";
  auto keys = split(attrs, ",");
  for (auto& key : keys) trim(key);
  bool publicFlag = false;
  for (const auto& key : keys) {
    if (key.empty()) continue;
    const Attr toFind{key, 0};
    const auto it = std::find(s_attrFlags.begin(), s_attrFlags.end(), toFind);
    if (it == s_attrFlags.end()) { // unknown key
      error = key;
      return false;
    }
    // consistency checks
    if (key == "public") {
      if (publicFlag /*duplicate public key*/ ||
          (flags & M_PROTECTED) /*incompatible keys*/ ||
          (flags & M_PRIVATE) /*incompatible keys*/)
        return false;
      publicFlag = true;
    }
    else if (publicFlag && (it->flag & M_ACCESSIBILITY)) {
      /*incompatible keys*/ return false;
    }
    if ((flags & it->flag) != 0) /*duplicate key*/ return false;
    if ((flags & M_ACCESSIBILITY) && (it->flag & M_ACCESSIBILITY))
      /*incompatible keys*/ return false;
    if ((flags & (M_CONST | M_STATIC)) && (it->flag & (M_CONST | M_STATIC)))
      /*incompatible keys*/ return false;
    flags |= it->flag;
  }
  return true;
}

void AttrParser::parse(Session* m, const char* methodName) {
  if (parse()) return;
  const string msg = incorrectAttribute() ?
    "unknown property attribute '"+error+"'" :
    "duplicate or icompatible property attributes";
  m->throwBadUsage(__PRETTY_FUNCTION__, msg, methodName);
}

void AttrParser::trim(string& s)
{ g_Trim(s); }
