#include "object.hpp"
#include <iterator>
#include <stdexcept>
#include "odev.h"
#include "ostring.hpp"
#include "ostriter.hpp"
#include "otypes.h"
#include "otype.hpp"
#include "ortmgr.hpp"
#include "ovector.hpp"
#include "otypeProxy.hpp"
#include "orange.hpp"

using std::vector;
using std::string;

bool g_installStringIteratorClasses(Session*);

template<typename t>
  static OClass* s_class() noexcept { return BTI::getTypeClass<t>(); }

template<>
  OClass* s_class<StringIterator>() noexcept {
    extern OClass* _OXType_class[];
    return _OXType_class[CLSID_STRITER];
  }

template<>
  OClass* s_class<StringReverseIterator>() noexcept {
    extern OClass* _OXType_class[];
    return _OXType_class[CLSID_STRRITER];
  }

template<>
  OClass* s_class<ORange>() noexcept {
    extern OClass* _OXType_class[];
    return _OXType_class[CLSID_RANGE];
  }

using StringRIterator = StringReverseIterator;

//============================================================================//
//::::::::::::::::::::::::::::   StringIterator   :::::::::::::::::::::::::::://
//============================================================================//

#if 0
namespace __gnu_cxx {
  using std::iterator_traits;
  using std::iterator;
  template<typename _Iterator, typename _Container>
  class __normal_iterator
    : public iterator<typename iterator_traits<_Iterator>::iterator_category,
                      typename iterator_traits<_Iterator>::value_type,
                      typename iterator_traits<_Iterator>::difference_type,
                      typename iterator_traits<_Iterator>::pointer,
                      typename iterator_traits<_Iterator>::reference> {
  protected:
    _Iterator _M_current;

  public:
    typedef typename iterator_traits<_Iterator>::difference_type
                                difference_type;
    typedef typename iterator_traits<_Iterator>::reference   reference;
    typedef typename iterator_traits<_Iterator>::pointer     pointer;

    __normal_iterator() : _M_current(_Iterator()) { }

    explicit
    __normal_iterator(const _Iterator& i) : _M_current(i) { }

    // Allow iterator to const_iterator conversion
    template<typename _Iter>
    inline __normal_iterator(
       const __normal_iterator<_Iter, _Container>& i) :
          _M_current(i.base()) { }
#endif // 0

//::::::::::::::::::::::::::::   Helper methods   :::::::::::::::::::::::::::://

ORet _O(StringIterator)::component(difference_type i, Session* m) {
  /*if (const_) {
    char ch = reverse_ ?
      std::make_reverse_iterator(static_cast<base_class>(*this)[i] :
      base()[i];
    _RETURN_TYPE(oxType<char>, BTI::getTypeClass<char>(), ch);
  }*/
  ORef r = BTI::getTypeProxyClass<char>()->createObject(m);
  _RETHROW("OStringIterator::component");
  if (reverse_) {
    auto p = std::make_reverse_iterator(static_cast<base_class>(*this));
    _PTypeProxy(char, r)->setValue(p[i]);
    _PTypeProxy(char, r)->setElemPtr(&p[i]);
  }
  else {
    auto p = base();
    _PTypeProxy(char, r)->setValue(p[i]);
    _PTypeProxy(char, r)->setElemPtr(p + i);
  }
  return OVar(r, USG_DISPOSABLE | (const_ ? USG_CONST : 0));
}

//::::::::::::::::::::   Conversion and initialization   ::::::::::::::::::::://

//::::::::::::::::::::   Forward iterator requirements   ::::::::::::::::::::://

//    reference operator*() const
ORet _O(StringIterator)::ommGetValue(OArg, int argc, OArg* argv, Session* m) {
  return component(0, m);
}

//    __normal_iterator& operator++();
ORet _O(StringIterator)::inc(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_inc);
  _MUTABLE_REC();
  reverse_ ? operator--() : operator++();
  return makeResult();
}

//    __normal_iterator operator++(int);
ORet _O(StringIterator)::postinc(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_postinc);
  _MUTABLE_REC();
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<_O(StringIterator)>() = reverse_ ? operator--() : operator++(0);
  static_cast<_O(StringIterator)*>(r)->setReverse(reverse_);
  return r->disposableResult();
}

//:::::::::::::::::   Bidirectional iterator requirements   :::::::::::::::::://

// __normal_iterator& operator--();
ORet _O(StringIterator)::dec(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_dec);
  _MUTABLE_REC();
  reverse_ ? operator++() : operator--();
  return makeResult();
}

// __normal_iterator operator--(int);
ORet _O(StringIterator)::postdec(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_postdec);
  _MUTABLE_REC();
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<_O(StringIterator)>() = reverse_ ? operator++(0) : operator--(0);
  static_cast<_O(StringIterator)*>(r)->setReverse(reverse_);
  return r->disposableResult();
}

//:::::::::::::::::   Random access iterator requirements   :::::::::::::::::://

// reference operator[](const difference_type& n) const;
ORet _O(StringIterator)::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string iterator::" S_component, 1);
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  return component(i, m);
}

// __normal_iterator& operator+=(const difference_type& n);
ORet _O(StringIterator)::incBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string iterator::" S_incBy, 1);
  _MUTABLE_REC();
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  reverse_ ? operator-=(i) : operator+=(i);
  return makeResult();
}

// __normal_iterator operator+(const difference_type& n) const;
ORet _O(StringIterator)::add(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string iterator::" S_add, 1);
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  _O(StringIterator)& oit = *static_cast<_O(StringIterator)*>(r);
  oit.setReverse(reverse_);
  oit = reverse_ ? operator-(i) : operator+(i);
  return r->disposableResult();
}

// __normal_iterator& operator-=(const difference_type& n);
ORet _O(StringIterator)::decBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string iterator::" S_decBy, 1);
  _MUTABLE_REC();
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  reverse_ ? operator+=(i) : operator-=(i);
  return makeResult();
}

// __normal_iterator operator-(const difference_type& n) const;
ORet _O(StringIterator)::sub(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string iterator::" S_sub, 1);
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  _O(StringIterator)& oit = *static_cast<_O(StringIterator)*>(r);
  oit.setReverse(reverse_);
  oit = reverse_ ? operator+(i) : operator-(i);
  return r->disposableResult();
}

  // Note: In what follows, the left- and right-hand-side iterators are
  // allowed to vary in types (conceptually in cv-qualification) so that
  // comparaison between cv-qualified and non-cv-qualified iterators be
  // valid.  However, the greedy and unfriendly operators in std::rel_ops
  // will make overload resolution ambiguous (when in scope) if we don't
  // provide overloads whose operands are of the same type.  Can someone
  // remind me what generic programming is about? -- Gaby

//::::::::::::::::::::   Forward iterator requirements   ::::::::::::::::::::://

#define _GENERIC_CMP(p, q, __OP__)           \
  _CHECK_ARG_TYPEID(0, CLSID_STRITER);       \
  if (q.reverse_ != reverse_)                \
    _THROW_RTE("string iterator mismatch")   \
  ORef r = s_class<bool>()->createObject(m); \
  _CHECK_EXCEPTION();                        \
  _PBOOL(r)->setValue(*p __OP__ q);          \
  return r->disposableResult();

//  template<typename _IteratorL, typename _IteratorR,
//                                typename _Container>
//  inline bool operator==(
//    const __normal_iterator<_IteratorL, _Container>& lhs,
//    const __normal_iterator<_IteratorR, _Container>& rhs)
ORet _O(StringIterator)::eq(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_eq);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), ==);
}

ORet _O(StringIterator)::ne(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_ne);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), !=);
}

ORet _O(StringIterator)::gt(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_gt);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), >);
}

ORet _O(StringIterator)::ge(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_ge);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), >=);
}

ORet _O(StringIterator)::lt(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_lt);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), <);
}

ORet _O(StringIterator)::le(OArg, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string iterator::" S_le);
  _GENERIC_CMP(this, static_cast<_O(StringIterator)&>(*_ARG(0)), <=);
}

#undef _GENERIC_CMP

#if 0  // TODO
  // _GLIBCPP_RESOLVE_LIB_DEFECTS
  // According to the resolution of DR179 not only the various comparison
  // operators but also operator- must accept mixed iterator/const_iterator
  // parameters.
template<typename _IteratorL, typename _IteratorR,
                              typename _Container>
  inline typename
  __normal_iterator<_IteratorL, _Container>::difference_type
  operator-(
    const __normal_iterator<_IteratorL, _Container>& lhs,
    const __normal_iterator<_IteratorR, _Container>& rhs)
template<typename _Iterator, typename _Container>
  inline __normal_iterator<_Iterator, _Container>
  operator+(typename __normal_iterator<_Iterator, _Container>
    ::difference_type n,
    const __normal_iterator<_Iterator, _Container>& i)
#endif //0

_BEGIN_METHOD_TABLE(_O(StringIterator), ORandomAccessIterator)
  PUBLIC(_O(StringIterator)::ommGetValue,  "value")
  PUBLIC(_O(StringIterator)::inc,          S_inc)
  PUBLIC(_O(StringIterator)::postinc,      S_postinc)
  PUBLIC(_O(StringIterator)::dec,          S_dec)
  PUBLIC(_O(StringIterator)::postdec,      S_postdec)
  PUBLIC(_O(StringIterator)::ommComponent, S_component)
  PUBLIC(_O(StringIterator)::incBy,        S_incBy)
  PUBLIC(_O(StringIterator)::add,          S_add)
  PUBLIC(_O(StringIterator)::decBy,        S_decBy)
  PUBLIC(_O(StringIterator)::sub,          S_sub)
  PUBLIC(_O(StringIterator)::eq,           S_eq)
  PUBLIC(_O(StringIterator)::ne,           S_ne)
  PUBLIC(_O(StringIterator)::gt,           S_gt)
  PUBLIC(_O(StringIterator)::ge,           S_ge)
  PUBLIC(_O(StringIterator)::lt,           S_lt)
  PUBLIC(_O(StringIterator)::le,           S_le)
_END_METHOD_TABLE()

//:::::::::::::::::::::::   StringIterator Metatype   :::::::::::::::::::::::://

_DEFINE_META_TYPE(StringIterator);

META(StringIterator)::META(StringIterator)(Session* m, const std::string& typeName)
  : OClass(m, typeName) {
  m->importRequirement(this, "RandomAccessIterator");
  m->rethrow("'string iterator class'::'iterator class constructor'");
}

//============================================================================//
//::::::::::::::::::::::::   StringReverseIterator   ::::::::::::::::::::::::://
//============================================================================//

#if 0 // Definition of reverse_iterator
template<typename _Iterator>
  class reverse_iterator
  : public iterator<typename iterator_traits<_Iterator>::iterator_category,
                    typename iterator_traits<_Iterator>::value_type,
                    typename iterator_traits<_Iterator>::difference_type,
                    typename iterator_traits<_Iterator>::pointer,
                    typename iterator_traits<_Iterator>::reference> {
  protected:
    _Iterator current;

  public:
    typedef _Iterator iterator_type;
    typedef typename iterator_traits<_Iterator>::difference_type difference_type;
    typedef typename iterator_traits<_Iterator>::reference   reference;
    typedef typename iterator_traits<_Iterator>::pointer     pointer;

  public:
    /** The default constructor gives an undefined state to this %iterator. */
    reverse_iterator() { }
    /** This %iterator will move in the opposite direction that @p x does. */
    explicit
    reverse_iterator(iterator_type x) : current(x) { }
    /** The copy constructor is normal. */
    reverse_iterator(const reverse_iterator& x)
    : current(x.current) { }
    /** Copy reverse_iterator across other types in the normal fashion. */
    template<typename _Iter>
      reverse_iterator(const reverse_iterator<_Iter>& x)
    : current(x.base()) { }
#endif // 0

// Initialize iterator
ORet OStringReverseIterator::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_value, 1);

  if (_ARG_TYPEID(0, CLSID_STRITER))
    current = _ARG(0)->castRef<_O(StringIterator)>();
  else if (_ARG_TYPEID(0, CLSID_STRRITER))
    current = _ARG(0)->castRef<OStringReverseIterator>().base();
  else
    _CHECK_ARG_TYPEIDS(0, (vector<int>{CLSID_STRITER, CLSID_STRRITER}));

  return makeResult();
}

//    iterator_type base() const
ORet OStringReverseIterator::ommBase(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::base");
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<_O(StringIterator)>() = base();
  return r->disposableResult();
}

ORet OStringReverseIterator::component(difference_type i, Session* m) {
  pointer ptr = &(operator[](i));
  ORef r = BTI::getTypeProxyClass<char>()->createObject(m);
  _RETHROW("OStringReverseIterator::component");
  _PTypeProxy(char, r)->setValue(*ptr);
  _PTypeProxy(char, r)->setElemPtr(ptr);
  return OVar(r, USG_DISPOSABLE | (const_ ? USG_CONST : 0));
}

//    reference operator*() const
ORet OStringReverseIterator::ommGetValue(OArg rec, int argc, OArg* argv, Session* m) {
  return component(0, m);
}

//    reverse_iterator& operator++()
ORet OStringReverseIterator::inc(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_inc);
  _MUTABLE_REC();
  operator++();
  return makeResult();
}

//    reverse_iterator operator++(int)
ORet OStringReverseIterator::postinc(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_postinc);
  _MUTABLE_REC();
  ORef r = s_class<StringRIterator>()->createObject(m);
  r->castRef<OStringReverseIterator>() = operator++(0);
  return r->disposableResult();
}

//      reverse_iterator& operator--()
ORet OStringReverseIterator::dec(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_dec);
  _MUTABLE_REC();
  operator--();
  return makeResult();
}

//    reverse_iterator operator--(int)
ORet OStringReverseIterator::postdec(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_postdec);
  _MUTABLE_REC();
  ORef r = s_class<StringRIterator>()->createObject(m);
  r->castRef<OStringReverseIterator>() = operator--(0);
  return r->disposableResult();
}

//    reverse_iterator operator+(difference_type n) const
ORet OStringReverseIterator::add(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_add, 1);
  difference_type i = g_castValue<difference_type>(argv, m);
  ORef r = s_class<StringRIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OStringReverseIterator>() = operator+(i);
  return r->disposableResult();
}

//    reverse_iterator& operator+=(difference_type n)
ORet OStringReverseIterator::incBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_incBy, 1);
  _MUTABLE_REC();
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  operator+=(i);
  return makeResult();
}

//    reverse_iterator operator-(difference_type n) const
ORet OStringReverseIterator::sub(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_sub, 1);
  difference_type i = g_castValue<difference_type>(argv, m);
  ORef r = s_class<StringIterator>()->createObject(m);
  _CHECK_EXCEPTION();
  r->castRef<OStringReverseIterator>() = operator-(i);
  return r->disposableResult();
}

//    reverse_iterator& operator-=(difference_type n)
ORet OStringReverseIterator::decBy(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_decBy, 1);
  _MUTABLE_REC();
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  operator-=(i);
  return makeResult();
}

//    reference operator[](difference_type n) const
ORet OStringReverseIterator::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("string reverse iterator::" S_component, 1);
  const difference_type i = g_castValue<difference_type>(argv, m);
  _CHECK_EXCEPTION();
  return component(i, m);
}

#define _GENERIC_CMP(p, q, __OP__)           \
  _CHECK_ARG_TYPEID(0, CLSID_STRRITER);      \
  ORef r = s_class<bool>()->createObject(m); \
  _CHECK_EXCEPTION();                        \
  _PBOOL(r)->setValue(*p __OP__ q);          \
  return r->disposableResult();

// template<typename _It>
//   bool operator==(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   bool operator<(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   bool operator!=(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   bool operator>(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   bool operator<=(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   bool operator>=(const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   typename reverse_iterator<_It>::difference_type operator-(
//     const reverse_iterator<_It>& x, const reverse_iterator<_It>& y)
// template<typename _It>
//   reverse_iterator<_It> operator+(
//     typename reverse_iterator<_It>::difference_type n,
//     const reverse_iterator<_It>& x)

ORet OStringReverseIterator::eq(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_eq);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), ==);
}

ORet OStringReverseIterator::ne(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_ne);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), !=);
}

ORet OStringReverseIterator::gt(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_gt);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), >);
}

ORet OStringReverseIterator::ge(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_ge);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), >=);
}

ORet OStringReverseIterator::lt(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_lt);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), <);
}

ORet OStringReverseIterator::le(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("string reverse iterator::" S_le);
  _GENERIC_CMP(this, _ARG(0)->castConstRef<OStringReverseIterator>(), <=);
}

#undef _GENERIC_CMP

_BEGIN_METHOD_TABLE(OStringReverseIterator, _O(StringIterator))
  PUBLIC(OStringReverseIterator::ommValue,     S_value)
  PUBLIC(OStringReverseIterator::ommBase,      "base")
  PUBLIC(OStringReverseIterator::ommGetValue,  "value")
  PUBLIC(OStringReverseIterator::ommComponent, S_component)
  PUBLIC(OStringReverseIterator::inc,          S_inc)
  PUBLIC(OStringReverseIterator::postinc,      S_postinc)
  PUBLIC(OStringReverseIterator::dec,          S_dec)
  PUBLIC(OStringReverseIterator::postdec,      S_postdec)
  PUBLIC(OStringReverseIterator::incBy,        S_incBy)
  PUBLIC(OStringReverseIterator::add,          S_add)
  PUBLIC(OStringReverseIterator::decBy,        S_decBy)
  PUBLIC(OStringReverseIterator::sub,          S_sub)
  PUBLIC(OStringReverseIterator::eq,           S_eq)
  PUBLIC(OStringReverseIterator::ne,           S_ne)
  PUBLIC(OStringReverseIterator::gt,           S_gt)
  PUBLIC(OStringReverseIterator::ge,           S_ge)
  PUBLIC(OStringReverseIterator::lt,           S_lt)
  PUBLIC(OStringReverseIterator::le,           S_le)
_END_METHOD_TABLE()

//::::::::::::::::::::   StringReverseIterator Metatype   :::::::::::::::::::://

_DEFINE_META_TYPE(StringReverseIterator);

META(StringReverseIterator)::META(StringReverseIterator)(Session* m, const std::string& typeName)
  : OClass(m, typeName) {
  m->importRequirement(this, "RandomAccessIterator");
  m->rethrow("'string reverse iterator class'::'iterator class constructor'");
}

//============================================================================//
//:::::::::::::::::::::::::::   Class Installer   :::::::::::::::::::::::::::://
//============================================================================//

bool g_installStringIteratorClasses(Session* m) {
  extern OClass* _OXType_class[];
  OClass *cls;

  cls = new OStringIterator_Class(m, "string iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_STRITER);
  _OXType_class[CLSID_STRITER] = cls;

  cls = new OStringReverseIterator_Class(m, "string reverse iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_STRRITER);
  _OXType_class[CLSID_STRRITER] = cls;

  return true;
}
