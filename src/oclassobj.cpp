#include <algorithm>
#include "oclassobj.hpp"
#include "odev.h"
#include "ortmgr.hpp"
#include "ostring.hpp"
#include "otype.hpp"
#include "objectAutoCleaner.hpp"
#include "oattrs.hpp"

using std::string;

//============================================================================//
//:::::::::::::::::::::::::::::   ClassObject   :::::::::::::::::::::::::::::://
//============================================================================//

ORef ClassObject::isNull(Session* m) const {
  _ENTER_NOCHK("ClassObject::isNull");
  ORef o = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE_RETURN(o);
  _PBOOL(o)->setValue(classPointer == nullptr);
  return o;
}

ORef ClassObject::valid(Session* m) const {
  _ENTER_NOCHK("ClassObject::valid");
  ORef o = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE_RETURN(o);
  _PBOOL(o)->setValue(classPointer != nullptr);
  return o;
}

int ClassObject::addProperty(Session* m, const std::string& name,
    const std::string& typeName, unsigned short pl) {
  const string prefixedTypeName = m->getPrefixedTypeName(typeName);
  return classPointer->addProperty(m, string{name}, prefixedTypeName, pl);
}

//============================================================================//
//::::::::::::::::::::::::::::   oxClassObject   ::::::::::::::::::::::::::::://
//============================================================================//

_BEGIN_METHOD_TABLE(oxClassObject, OInstance)
  PUBLIC (oxClassObject::ommAddPublicProperty,    "prop:type:")
  PUBLIC (oxClassObject::ommAddPublicProperty,    "property:type:")
  PUBLIC (oxClassObject::ommAddPublicProperty,    "public:type:")
  PUBLIC (oxClassObject::ommAddProtectedProperty, "protected:type:")
  PUBLIC (oxClassObject::ommAddPrivateProperty,   "private:type:")
  PUBLIC (oxClassObject::ommAddPublicProperty,    "prop:type:attr:")
  PUBLIC (oxClassObject::ommAddPublicProperty,    "property:type:attribute:")

  PUBLIC (oxClassObject::ommAddPublicTemplate,    "property:")
  PUBLIC (oxClassObject::ommAddPublicTemplate,    "prop:")
  PUBLIC (oxClassObject::ommAddPublicTemplate,    "public:")
  PUBLIC (oxClassObject::ommAddProtectedTemplate, "protected:")
  PUBLIC (oxClassObject::ommAddPrivateTemplate,   "private:")

  PUBLIC (oxClassObject::ommAddPublicRef,       "reference:")
  PUBLIC (oxClassObject::ommAddPublicRef,       "ref:")
  PUBLIC (oxClassObject::ommAddRefAttr,         "reference:attribute:")
  PUBLIC (oxClassObject::ommAddRefAttr,         "ref:attr:")
  PUBLIC (oxClassObject::ommAddProtectedRef,    "protectedReference:")
  PUBLIC (oxClassObject::ommAddPrivateRef,      "privateReference:")

  PUBLIC (oxClassObject::ommAddMethod,          "method:attribute:definition:")
  PUBLIC (oxClassObject::ommAddPublicMethod,    "method:definition:")
  PUBLIC (oxClassObject::ommAddPublicMethod,    "public:definition:")
  PUBLIC (oxClassObject::ommAddProtectedMethod, "protected:definition:")
  PUBLIC (oxClassObject::ommAddPrivateMethod,   "private:definition:")
  PUBLIC (oxClassObject::ommAddMethod,          "method:attr:def:")
  PUBLIC (oxClassObject::ommAddPublicMethod,    "method:def:")
  PUBLIC (oxClassObject::ommAddPublicMethod,    "public:def:")
  PUBLIC (oxClassObject::ommAddProtectedMethod, "protected:def:")
  PUBLIC (oxClassObject::ommAddPrivateMethod,   "private:def:")

  PUBLIC (oxClassObject::ommAddObjectCache,     "cache:")

  PUBLIC (oxClassObject::ommCastBool, "bool")
  PUBLIC (oxClassObject::ommGetPrintout, "getPrintout")
  PUBLIC (oxClassObject::ommIsNull, "isNull")
  PUBLIC (oxClassObject::ommIsTemplate, "template")
  PUBLIC (oxClassObject::ommIsMutable, "mutable")
  PUBLIC (oxClassObject::ommPrint, "print")
  PUBLIC (oxClassObject::ommValid, "valid")

  PUBLIC (oxClassObject::ommAlias, "as:")
  PUBLIC (oxClassObject::ommAlias, "alias:")
  PUBLIC (oxClassObject::ommAlias, "usingAs:")
  PUBLIC (oxClassObject::ommIsAlias, "alias")
  PUBLIC (oxClassObject::ommIsAliasOf, "aliasOf:")
_END_METHOD_TABLE()

_DEFINE_MCLASS(ClassObject)

_IMPLEMENT_CLASS_INSTALLER(ClassObject)

static
string _errorMessage(int rc, const string& className, const string& propName) {
  return rc == 2 ?
  "duplicate property "+propName+" for class "+className :
  "could not add property "+propName+" because class "+className+" is finalized";
}

//::::::::::::::::::::::::::::::   Properties   :::::::::::::::::::::::::::::://

ORet oxClassObject::ommAddPublicProperty(OArg, int argc, OArg* argv, Session* m) {
  static const char* methodNames[2]{
      "class object::public:type:", "class object::property:type:attribute:"};
  _DECL_METHOD_NAME(methodNames[argc == 2 ? 0 : 1]);
  _CHECK_ARGC_RANGE(2, 3);
  _UNPACK_CONST_REF(String, name, 0);
  _UNPACK_CONST_REF(String, type, 1);
  ushort flags = M_PUBLIC;
  if (argc == 3) {
    _UNPACK_CONST_REF(String, attrString, 2);
    AttrParser attrs(attrString);
    attrs.parse(m, _ox_method_name);
    _CHECK_EXCEPTION();
    flags = attrs.flags;
  }
  const int rc = addProperty(m, name, type, flags);
  if (rc == 0) return makeResult();
  m->throwRTE(_ox_method_name, _errorMessage(rc, classPointer->name, name));
  return OVariable::undefinedResult();
}

ORet oxClassObject::ommAddProtectedProperty(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::protected:type:", 2);
  _UNPACK_CONST_REF(String, name, 0);
  _UNPACK_CONST_REF(String, type, 1);
  const int rc = addProperty(m, name, type, M_PROTECTED);
  if (rc == 0) return makeResult();
  m->throwRTE(_ox_method_name, _errorMessage(rc, classPointer->name, name));
  return OVariable::undefinedResult();
}

ORet oxClassObject::ommAddPrivateProperty(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::private:type:", 2);
  _UNPACK_CONST_REF(String, name, 0);
  _UNPACK_CONST_REF(String, type, 1);
  const int rc = addProperty(m, name, type, M_PRIVATE);
  if (rc == 0) return makeResult();
  m->throwRTE(_ox_method_name, _errorMessage(rc, classPointer->name, name));
  return OVariable::undefinedResult();
}

//:::::::::::::::::::::::::   Template Properties   :::::::::::::::::::::::::://

ORet oxClassObject::_genericAddTemplate(const std::string& methodName,
                                        const std::string& name,
                                        ushort flags, Session* m) {
  const int rc = addProperty(m, name, "", flags);
  if (rc == 0) return makeResult();
  m->throwRTE(methodName, _errorMessage(rc, classPointer->name, name));
  return OVariable::undefinedResult();
}

ORet oxClassObject::ommAddPublicTemplate(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::public:", 1);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddTemplate(_ox_method_name, propName, M_PUBLIC, m);
}

ORet oxClassObject::ommAddProtectedTemplate(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::protected:", 1);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddTemplate(_ox_method_name, propName, M_PROTECTED, m);
}

ORet oxClassObject::ommAddPrivateTemplate(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::private:", 1);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddTemplate(_ox_method_name, propName, M_PRIVATE, m);
}

//::::::::::::::::::::::::::::::   References   :::::::::::::::::::::::::::::://

ORet oxClassObject::_genericAddRef(const string& methodName,
                                   const string& propName,
                                   ushort flags, Session* m) {
  const int rc = classPointer->addProperty(m, string(propName), "",
                                           M_REF | flags);
  if (rc == 0) return makeResult();
  m->throwRTE(methodName, _errorMessage(rc, classPointer->name, propName));
  return OVariable::undefinedResult();
}

ORet oxClassObject::ommAddRefAttr(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::reference:attribute:", 2);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  _CHECK_ARG_TYPEID(1, CLSID_STRING);
  _UNPACK_CONST_REF(String, propName, 0);
  _UNPACK_CONST_REF(String, attrString, 1);
  // parse attribute string
  AttrParser attrs(attrString);
  /*if (!attrs.parse()) {
    const string msg = attrs.incorrectAttribute() ?
      "unknown property attribute '"+attrs.error+"'" :
      "duplicate or icompatible property attributes";
    m->throwRTE(_ox_method_name, msg);
    return OVariable::undefinedResult();
  }*/
  attrs.parse(m, _ox_method_name);
  _CHECK_EXCEPTION();
  return _genericAddRef(_ox_method_name, propName, attrs.flags, m);
}

ORet oxClassObject::ommAddPublicRef(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::reference:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddRef(_ox_method_name, propName, M_PUBLIC, m);
}

ORet oxClassObject::ommAddProtectedRef(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::reference:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddRef(_ox_method_name, propName, M_PROTECTED, m);
}

ORet oxClassObject::ommAddPrivateRef(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::reference:", 1);
  _CHECK_ARG_TYPEID(0, CLSID_STRING);
  _UNPACK_CONST_REF(String, propName, 0);
  return _genericAddRef(_ox_method_name, propName, M_PRIVATE, m);
}

//:::::::::::::::::::::::::::::::   Methods   :::::::::::::::::::::::::::::::://

ORet oxClassObject::_genericAddMethod(const string& methodName,
                                      const string& proto,
                                      ushort flags,
                                      int blockId,
                                      Session* m) {
  try {
    if (classPointer->addScriptMethod(m, proto, blockId, flags))
      return makeResult();
    const auto& name = classPointer->getMethodTable().back().name;
    m->throwRTE(methodName, "duplicate method '"+name+"' for class "+
      classPointer->name);
  }
  catch (const string& msg) {
    const auto& name = classPointer->getMethodTable().back().name;
    m->throwRTE(methodName, msg+" in method '"+name+"' class "+
      classPointer->name);
  }
  return OVariable::undefinedResult();
}

ORet oxClassObject::ommAddMethod(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("class object::method:attribute:definition:", 3);
  _CHECK_ARG_TYPEID(1, CLSID_STRING);
  _CHECK_ARG_TYPEID(2, CLSID_BLOCK);
  _UNPACK_CONST_REF(String, proto, 0);
  _UNPACK_CONST_REF(String, attrString, 1);
  const int blockId = g_blockId(_ARG(2));
  // parse attribute string
  AttrParser attrs(attrString);
  /*if (!attrs.parse()) {
    const string msg = attrs.incorrectAttribute() ?
      "unknown method attribute '"+attrs.error+"'" :
      "duplicate or icompatible attributes in method prototype";
    m->throwRTE(_ox_method_name, msg);
    return OVariable::undefinedResult();
  }*/
  attrs.parse(m, _ox_method_name);
  _CHECK_EXCEPTION();
  // add method
  return _genericAddMethod(_ox_method_name, proto, attrs.flags, blockId, m);
}

ORet oxClassObject::ommAddPublicMethod(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("class object::method:definition:", 2);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  _UNPACK_CONST_REF(String, proto, 0);
  const int blockId = g_blockId(_ARG(1));
  return _genericAddMethod(_ox_method_name, proto, M_PUBLIC, blockId, m);
}

ORet oxClassObject::ommAddProtectedMethod(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("class object::protected:definition:", 2);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  _UNPACK_CONST_REF(String, proto, 0);
  const int blockId = g_blockId(_ARG(1));
  return _genericAddMethod(_ox_method_name, proto, M_PROTECTED, blockId, m);
}

ORet oxClassObject::ommAddPrivateMethod(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("class object::private:definition:", 2);
  _CHECK_ARG_TYPEID(1, CLSID_BLOCK);
  _UNPACK_CONST_REF(String, proto, 0);
  const int blockId = g_blockId(_ARG(1));
  return _genericAddMethod(_ox_method_name, proto, M_PRIVATE, blockId, m);
}

//::::::::::::::::::::::::::::   Object Caches   ::::::::::::::::::::::::::::://

ORet oxClassObject::ommAddObjectCache(OArg, int argc, OArg* argv, Session* m) {
  _ENTER("class object::cache:", 1);
  size_t n = g_castValue<size_t>(argv, m);
  if (m->exceptionPending()) {
    ObjectAutoCleaner cleaner{m, m->popException()};
    m->throwBadArg(_ox_method_name, 1, "integer type", _ARG(0)->getClassName());
  }
  else if (!classPointer) {
    m->throwBadUsage(__PRETTY_FUNCTION__, "null type", _ox_method_name);
  }
  else if (classPointer->isTemplateClass()) {
    /* ignore */; // TODO issue log message
  }
  else if (!classPointer->isOpen() || classPointer->isMutable()) {
    const string msg{"method available only in class definition block"};
    m->throwBadUsage(__PRETTY_FUNCTION__, msg, _ox_method_name);
  }
  else if (n != 0) {
    ObjectUsage objectUsage;
    objectUsage.limitup = n;
    try {
      classPointer->createObjectCache(objectUsage);
    }
    catch (const std::logic_error& except) {
      m->throwBadClass(
      /* method      */ "Session::createClassObject",
      /* description */ "unable to configure class '"+classPointer->name+"'",
      /* context     */ "object cache creation",
      /* reason      */ except.what(),
      /* file, line  */ __FILE__, __LINE__);
    }
  }
  return OVariable::undefinedResult();
}

//::::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://

ORet oxClassObject::ommIsNull(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::isNull");
  ORef o = ClassObject::isNull(m);
  _RETHROW_HERE();
  return o->disposableResult();
}

ORet oxClassObject::ommValid(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::valid");
  ORef o = valid(m);
  _RETHROW_HERE();
  return o->disposableResult();
}

ORet oxClassObject::ommCastBool(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::bool");
  ORef o = valid(m);
  _RETHROW_HERE();
  return o->disposableResult();
}

ORet oxClassObject::ommIsTemplate(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::template");
  ORef o = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(o)->setValue(classPointer->isTemplateClass());
  return o->disposableResult();
}

ORet oxClassObject::ommIsMutable(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::mutable");
  ORef o = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _PBOOL(o)->setValue(classPointer->isMutable());
  return o->disposableResult();
}

ORet oxClassObject::ommGetPrintout(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("class object::getPrintout");
  ORef o = BTI::getTypeClass<string>()->createObject(m);
  _RETHROW_HERE();
  string& out = *_PStr(o);
  if (!classPointer) out = "null class object";
  else classPointer->print([&out](const string& s) {out += s;}, true);
  return o->disposableResult();
}

ORet oxClassObject::ommPrint(OArg rec, int argc, OArg* argv, Session* m) {
  struct _OString : oxString { // just make a method public
    ORet print(OArg rec, int, OArg* argv, Session* m)
    { return method_print(rec, 0, argv, m); }
  };
  oxString o{BTI::getTypeClass<string>()};
  string& out = static_cast<string&>(o);
  if (!classPointer) out = "null class object";
  else classPointer->print([&out](const string& s) {out += s;}, true);
  OVar self{&o};
  static_cast<_OString*>(&o)->print(&self, 0, nullptr, m);
  return makeResult();
}

//:::::::::::::::::::::::::::::::   Aliases   :::::::::::::::::::::::::::::::://

ORet oxClassObject::ommAlias(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("alias:", 1);
  if (!classPointer)
    _THROW_RTE("invalid class object")
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, newTypeName, 0);
  const string absTypeName = m->getTypePrefix() + newTypeName;
  m->classMgr->aliasClass(classPointer, absTypeName);
  if (m->importing())
    m->getPackageInfo().registeredTypes.emplace_back(absTypeName, classPointer);
  return makeResult();
}

ORet oxClassObject::ommIsAlias(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER_NOCHK("alias");
  if (!classPointer)
    _THROW_RTE("invalid class object")
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _RETHROW_HERE_IF(!r);
  _P(bool, r)->setValue(m->classMgr->isAlias(classPointer));
  return r->disposableResult();
}

ORet oxClassObject::ommIsAliasOf(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("aliasOf:", 1);
  _CHECK_ARG_TYPE(0, BTI::getTypeClass<string>());
  _UNPACK_CONST_REF(String, typeName, 0);
  OClass* type = m->classMgr->getClass(typeName);
  ORef r = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_HERE();
  _RETHROW_HERE_IF(!r);
  _P(bool, r)->setValue(type == classPointer &&
                        m->classMgr->isAlias(classPointer));
  return r->disposableResult();
}
