#include "oreqs.hpp"
#include "ortmgr.hpp"
#include <algorithm>

using std::string;

//============================================================================//
//:::::::::::::::::::::::::::::   Requirement   :::::::::::::::::::::::::::::://
//============================================================================//

//:::::::::::::::::::::::::::::   Constructors   ::::::::::::::::::::::::::::://

Requirement::Requirement(Session* m, const string& name, Requirement* parent,
                         std::vector<string>&& imports,
                         std::vector<string>&& methods,
                         std::vector<string>&& props) :
    parent_{parent}, name{name} {
  if (!setImports(m, std::move(imports))) return;
  if (!setMethods(m, std::move(methods))) return;
  setProps(m, std::move(props));
}

template<typename T>
static bool _hasDuplicates(const std::vector<T>& v) {
  if (v.empty()) return false;
  for (auto it = v.begin(); it != v.end() - 1; ++it)
    if (std::find(it + 1, v.end(), *it) != v.end())
      return true;
  return false;
}

bool Requirement::setImports(Session* m, std::vector<string>&& imports) {
  const auto impEnd = imports.end();
  if (parent_ && std::find(imports.begin(), impEnd, parent_->name) != impEnd) {
    m->throwRTE("Requirement::setImports",
                "parent requirement matches import '"+parent_->name+"'");
    return false;
  }
  if (_hasDuplicates(imports)) {
    m->throwRTE("Requirement::setImports", "duplicate requirement sets");
    return false;
  }
  imports_.resize(imports.size());
  size_t i = 0;
  for (const string& reqsId : imports) {
    Requirement* const reqs = m->getRequirement(reqsId);
    if (!reqs) {
      m->throwRTE("Requirement::setImports", "unknown requirement '"+reqsId+"'");
      return false;
    }
    imports_[i++] = reqs;
  }
  return true;
}

bool Requirement::setMethods(Session* m, std::vector<string>&& methods) {
  if (_hasDuplicates(methods)) {
    m->throwRTE("Requirement::setMethods", "duplicate method requirement");
    return false;
  }
  methods_.swap(methods);
  return true;
}

bool Requirement::setProps(Session* m, std::vector<string>&& props) {
  if (_hasDuplicates(props)) {
    m->throwRTE("Requirement::setProps", "duplicate property requirement");
    return false;
  }
  props_.swap(props);
  return true;
}

//:::::::::::::::::::::::::   Requirement Methods   :::::::::::::::::::::::::://

bool Requirement::requiresItem(const string& itemName,
                               RequiredItemType itemType) const {
  if (itemType == METHOD) return requiresMethod(itemName);
  if (itemType == PROP  ) return requiresProp(itemName);
  if (itemType == IMPORT) return requiresInterface(itemName);
  return false;
}

bool Requirement::requiresInterface(const string& interfaceId) const {
  if (name == interfaceId)
    return true;
  // look into imports
  for (Requirement* const req : imports_)
    if (req->requiresInterface(interfaceId))
      return true;
  // look into parent requirements
  if (parent_ && parent_->requiresInterface(interfaceId))
    return true;
  return false;
}

bool Requirement::requiresMethod(const string& methodName) const {
  // look into this set of requirements
  if (std::find(methods_.begin(), methods_.end(), methodName) != methods_.end())
    return true;
  // look into imports
  for (Requirement* const reqs : imports_)
    if (reqs->requiresMethod(methodName))
      return true;
  // look into parent requirements
  if (parent_ && parent_->requiresMethod(methodName))
    return true;
  return false;
}

bool Requirement::requiresProp(const std::string& propName) const {
  // look into this set of requirements
  if (std::find(props_.begin(), props_.end(), propName) != props_.end())
    return true;
  // look into imports
  for (Requirement* const reqs : imports_)
    if (reqs->requiresProp(propName))
      return true;
  // look into parent requirements
  if (parent_ && parent_->requiresProp(propName))
    return true;
  return false;
}

//============================================================================//
//:::::::::::::::::::::::::   Requirement Manager   :::::::::::::::::::::::::://
//============================================================================//

static bool _reqsCmp(const std::unique_ptr<Requirement>& a, const string& b) {
  return a->name < b;
}

/*bool ReqMgr::registerRequirement(Requirement* req) {
  const auto end = reqs_.end();
  const auto p = std::lower_bound(reqs_.begin(), end, req->name, _reqsCmp);
  if (p != end && (*p)->name == req->name)
    return false;
  reqs_.insert(p, std::unique_ptr<Requirement>(req));
  return true;
}*/

Requirement* ReqMgr::registerRequirement(Requirement&& req) {
  const auto end = reqs_.end();
  const auto p = std::lower_bound(reqs_.begin(), end, req.name, _reqsCmp);
  if (p != end && (*p)->name == req.name)
    return nullptr;
  auto it = reqs_.insert(p, std::make_unique<Requirement>(std::move(req)));
  return it->get();
}

Requirement* ReqMgr::getRequirement(const std::string& reqId) const {
  const auto end = reqs_.end();
  const auto pos = std::lower_bound(reqs_.begin(), end, reqId, _reqsCmp);
  return pos != end && (*pos)->name == reqId ? pos->get() : nullptr;
}

bool ReqMgr::addRequirement(OClass* type, Requirement&& req) {
  Requirement* p = registerRequirement(std::move(req));
  if (!p) return false;
  return type->importRequirement(p);
}

bool ReqMgr::importRequirement(OClass* type, const std::string& reqId) {
  Requirement* req = getRequirement(reqId);
  if (!req) return false;
  return type->importRequirement(req);
}

