#include <string>
#include "omembers.hpp"

using std::string;

const string& g_getClassName(OClass*);
/** Convert number to hexadecimal string */
string ul2hexstr(ulong); // implemented in OClass
/** Convert number to hexadecimal string */
string ul2hexstr(void*); // implemented in OClass

/// Print member protection
static void s_printProtection(StringPrinter stringPrinter, ushort protection) {
  const string accessibility = g_public(protection) ? "public" :
      (g_protected(protection) ? "protected" :
          (g_private(protection) ? "private" : "noaccess"));
  stringPrinter(accessibility);
}

//============================================================================//
//::::::::::::::::::::::::::::  Property, Method  :::::::::::::::::::::::::::://
//============================================================================//

void Property::print(StringPrinter stringPrinter, ushort pl) const {
  Descriptor::print(stringPrinter);
  stringPrinter(", ");
  s_printProtection(stringPrinter, pl);
  if (type == nullptr) {
    string type(g_ref(pl) ? ", REF" : ", T");
    stringPrinter(", class 'null' (0x0), "+type);
  }
  else {
    const string p = ul2hexstr(type);
    auto pos = p.find_first_not_of("0");
    stringPrinter(", class '"+g_getClassName(type)+"' (0x"+p.substr(pos)+")");
  }
}

void Method::print(StringPrinter stringPrinter, ushort pl) const {
  Descriptor::print(stringPrinter);
  stringPrinter(", ");
  s_printProtection(stringPrinter, pl);
  if (valid())
    stringPrinter(", block: "+std::to_string(block));
  else
    stringPrinter(", not valid");
}

//============================================================================//
//::::::::::::::::::::::::  Property, Method Tables  ::::::::::::::::::::::::://
//============================================================================//

int PropertyTable::addPropertyNoCheck(std::string&& name, OClass *c, ushort pl) {
  emplace_back(std::move(name), c);
  protection_.push_back(pl);
  return size() - 1;
}

bool MethodTable::addMethod(std::string&& name, int blockId, ushort pl) {
  if (getOffset(name) != -1)
    return false;
  this->emplace_back(std::move(name), blockId);
  protection_.push_back(pl);
  return true;
}

void PropertyTable::print(StringPrinter sout) const {
  for (int i = 0; i < (int) size(); ++i) {
    const auto& m = (*this)[i];
    m.print(sout, protection(i));
    sout("\n");
  }
}

void MethodTable::print(StringPrinter sout) const {
  for (int i = 0; i < (int) size(); ++i) {
    sout("+ ");
    const auto& m = (*this)[i];
    m.print(sout, protection(i));
    sout("\n");
  }
}
