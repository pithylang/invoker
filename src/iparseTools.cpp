#include "iparseTools.hpp"

#if 0
// Parser options
_OX_LINK bool  g_emitLineInfo = true; // determined from other options
_OX_LINK bool  g_emitFileInfo = true; // determined from other options
_OX_LINK bool  g_autoBreakSwitchCases = false;
_OX_LINK bool  g_undeclSymbolsMembers = false;
_OX_LINK bool  g_undeclSymbolsWithPrefixMembers = false;
_OX_LINK bool  g_undeclSymbolsGlobals = false;
_OX_LINK bool  g_warnOnUndeclSymbol = true;
_OX_LINK bool  g_verbose = false;
#endif // 0

using std::string;

yy::parser::symbol_type yylex(IParser& thisParser) {
  return thisParser.lex();
}

//============================================================================//
//:::::::::::::::::::::::::::::::   IParser   :::::::::::::::::::::::::::::::://
//============================================================================//

IParser::IParser(const char* theScript, ulong flags) :
    lineNumber_              {0},
    script                   (theScript),
    cursor_                  (const_cast<char*>(theScript)),
    parseOptions             {flags},
    sourceFile               {"unknown"},
    systemName               {"sys"},
    systemSymbol             {'$'},
    classCreateMethods       {"class:def:",
                              "class:extends:def:",
                              "class:definition:",
                              "class:extends:definition:"},
    varCreateMethods         {{"auto:value:", 0, false},
                              {"const:value:", 0, false},
                              {"auto:type:", 0, false},
                              {"const:type:", 0, false},
                              {"ref:", 0, false},
                              {"reference:", 0, false},
                              {"static:type:", 0, false},
                              {"staticConst:type:", 0, false},
                              {"isNull:", 0, true},
                              {"isObject:", 0, true},
                              {"isUndefined:", 0, true},
                              {"delete:", 0, true}}
{}

IParser::~IParser() {
#if 0
  ISymbol **ppisym = m_TmpList.exportArray();
  for (int i = 0; i < m_TmpList.count(); ++i)
     delete ppisym[i];
  ITmpList::freeExportedArray(ppisym);
  // Don't Destroy class definition list
  // Destroy lists of user specified keywords
  deleteUserKeywords();
  // Destroy user definitions
  for (int i = 0; i < m_GlobalDefs.size(); ++i)
     delete m_GlobalDefs[i];
#endif // 0
}

int IParser::parseProgram() {
  yy::parser thisParser(*this);
  return thisParser.parse();
}

const VarCreateMethod*
IParser::getVarCreateMethod(const std::string& methodName) const {
  const auto end = varCreateMethods.end();
  auto it = std::find_if(varCreateMethods.begin(), end,
      [&methodName](const VCM& vcm) {return methodName == vcm.name;});
  return it == end ? nullptr : &*it;
}
