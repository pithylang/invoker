// A Bison parser, made by GNU Bison 3.8.2.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018-2021 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

// DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
// especially those whose name start with YY_ or yy_.  They are
// private implementation details that can be changed or removed.





#include "iparser.hpp"


// Unqualified %code blocks.
#line 40 "src/bison/i.y"

#include "itypes.hpp"
#include "selectors.h"
#include "iparseTools.hpp"

#define S_ext "extend:"
#define S_extBy "extendBy:"

#line 55 "src/iparser.cpp"


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif


// Whether we are compiled with exception support.
#ifndef YY_EXCEPTIONS
# if defined __GNUC__ && !defined __EXCEPTIONS
#  define YY_EXCEPTIONS 0
# else
#  define YY_EXCEPTIONS 1
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (false)
# endif


// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << '\n';                       \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yy_stack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YY_USE (Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void> (0)
# define YY_STACK_PRINT()                static_cast<void> (0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

namespace yy {
#line 147 "src/iparser.cpp"

  /// Build a parser object.
  parser::parser (IParser& parseServices_yyarg)
#if YYDEBUG
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
#else
    :
#endif
      parseServices (parseServices_yyarg)
  {}

  parser::~parser ()
  {}

  parser::syntax_error::~syntax_error () YY_NOEXCEPT YY_NOTHROW
  {}

  /*---------.
  | symbol.  |
  `---------*/



  // by_state.
  parser::by_state::by_state () YY_NOEXCEPT
    : state (empty_state)
  {}

  parser::by_state::by_state (const by_state& that) YY_NOEXCEPT
    : state (that.state)
  {}

  void
  parser::by_state::clear () YY_NOEXCEPT
  {
    state = empty_state;
  }

  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  parser::by_state::by_state (state_type s) YY_NOEXCEPT
    : state (s)
  {}

  parser::symbol_kind_type
  parser::by_state::kind () const YY_NOEXCEPT
  {
    if (state == empty_state)
      return symbol_kind::S_YYEMPTY;
    else
      return YY_CAST (symbol_kind_type, yystos_[+state]);
  }

  parser::stack_symbol_type::stack_symbol_type ()
  {}

  parser::stack_symbol_type::stack_symbol_type (YY_RVREF (stack_symbol_type) that)
    : super_type (YY_MOVE (that.state), YY_MOVE (that.location))
  {
    switch (that.kind ())
    {
      case symbol_kind::S_block: // block
        value.YY_MOVE_OR_COPY< IBlock > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.YY_MOVE_OR_COPY< IItem > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.YY_MOVE_OR_COPY< IReceivable > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.YY_MOVE_OR_COPY< IStringWithAttrs > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.YY_MOVE_OR_COPY< ISymbol > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.YY_MOVE_OR_COPY< IUnit > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.YY_MOVE_OR_COPY< std::vector<IUnit> > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

#if 201103L <= YY_CPLUSPLUS
    // that is emptied.
    that.state = empty_state;
#endif
  }

  parser::stack_symbol_type::stack_symbol_type (state_type s, YY_MOVE_REF (symbol_type) that)
    : super_type (s, YY_MOVE (that.location))
  {
    switch (that.kind ())
    {
      case symbol_kind::S_block: // block
        value.move< IBlock > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.move< IItem > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.move< IReceivable > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.move< IStringWithAttrs > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.move< ISymbol > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.move< IUnit > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.move< std::vector<IUnit> > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

    // that is emptied.
    that.kind_ = symbol_kind::S_YYEMPTY;
  }

#if YY_CPLUSPLUS < 201103L
  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
    switch (that.kind ())
    {
      case symbol_kind::S_block: // block
        value.copy< IBlock > (that.value);
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.copy< IItem > (that.value);
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.copy< IReceivable > (that.value);
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.copy< IStringWithAttrs > (that.value);
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.copy< ISymbol > (that.value);
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.copy< IUnit > (that.value);
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.copy< std::vector<IUnit> > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }

  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (stack_symbol_type& that)
  {
    state = that.state;
    switch (that.kind ())
    {
      case symbol_kind::S_block: // block
        value.move< IBlock > (that.value);
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        value.move< IItem > (that.value);
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        value.move< IReceivable > (that.value);
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        value.move< IStringWithAttrs > (that.value);
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        value.move< ISymbol > (that.value);
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        value.move< IUnit > (that.value);
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        value.move< std::vector<IUnit> > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    // that is emptied.
    that.state = empty_state;
    return *this;
  }
#endif

  template <typename Base>
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YY_USE (yyoutput);
    if (yysym.empty ())
      yyo << "empty symbol";
    else
      {
        symbol_kind_type yykind = yysym.kind ();
        yyo << (yykind < YYNTOKENS ? "token" : "nterm")
            << ' ' << yysym.name () << " ("
            << yysym.location << ": ";
        switch (yykind)
    {
      case symbol_kind::S_ID: // "identifier"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 498 "src/iparser.cpp"
        break;

      case symbol_kind::S_SELID: // "selector"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 504 "src/iparser.cpp"
        break;

      case symbol_kind::S_ULONG: // "ulong"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 510 "src/iparser.cpp"
        break;

      case symbol_kind::S_LONG: // "long"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 516 "src/iparser.cpp"
        break;

      case symbol_kind::S_UINT: // "uint"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 522 "src/iparser.cpp"
        break;

      case symbol_kind::S_INT: // "int"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 528 "src/iparser.cpp"
        break;

      case symbol_kind::S_SHORT: // "short"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 534 "src/iparser.cpp"
        break;

      case symbol_kind::S_CHAR: // "char"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 540 "src/iparser.cpp"
        break;

      case symbol_kind::S_BOOL: // "bool"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 546 "src/iparser.cpp"
        break;

      case symbol_kind::S_FLT: // "float"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 552 "src/iparser.cpp"
        break;

      case symbol_kind::S_DBL: // "double"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 558 "src/iparser.cpp"
        break;

      case symbol_kind::S_STRING: // "string"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 564 "src/iparser.cpp"
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IStringWithAttrs > (); }
#line 570 "src/iparser.cpp"
        break;

      case symbol_kind::S_pgm: // pgm
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < std::vector<IUnit> > (); }
#line 576 "src/iparser.cpp"
        break;

      case symbol_kind::S_block: // block
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IBlock > (); }
#line 582 "src/iparser.cpp"
        break;

      case symbol_kind::S_stlist: // stlist
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < std::vector<IUnit> > (); }
#line 588 "src/iparser.cpp"
        break;

      case symbol_kind::S_stm: // stm
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IUnit > (); }
#line 594 "src/iparser.cpp"
        break;

      case symbol_kind::S_unit: // unit
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IUnit > (); }
#line 600 "src/iparser.cpp"
        break;

      case symbol_kind::S_item: // item
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IItem > (); }
#line 606 "src/iparser.cpp"
        break;

      case symbol_kind::S_itemi: // itemi
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IItem > (); }
#line 612 "src/iparser.cpp"
        break;

      case symbol_kind::S_itemac: // itemac
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IItem > (); }
#line 618 "src/iparser.cpp"
        break;

      case symbol_kind::S_lit: // lit
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 624 "src/iparser.cpp"
        break;

      case symbol_kind::S_cexpr: // cexpr
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < std::vector<IUnit> > (); }
#line 630 "src/iparser.cpp"
        break;

      case symbol_kind::S_expr: // expr
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IUnit > (); }
#line 636 "src/iparser.cpp"
        break;

      case symbol_kind::S_recbl: // recbl
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IReceivable > (); }
#line 642 "src/iparser.cpp"
        break;

      case symbol_kind::S_jsond: // jsond
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < IReceivable > (); }
#line 648 "src/iparser.cpp"
        break;

      case symbol_kind::S_ssel: // ssel
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 654 "src/iparser.cpp"
        break;

      case symbol_kind::S_sel: // sel
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < ISymbol > (); }
#line 660 "src/iparser.cpp"
        break;

      case symbol_kind::S_dmexpr: // dmexpr
#line 148 "src/bison/i.y"
                 { yyo << yysym.value.template as < std::vector<IUnit> > (); }
#line 666 "src/iparser.cpp"
        break;

      default:
        break;
    }
        yyo << ')';
      }
  }
#endif

  void
  parser::yypush_ (const char* m, YY_MOVE_REF (stack_symbol_type) sym)
  {
    if (m)
      YY_SYMBOL_PRINT (m, sym);
    yystack_.push (YY_MOVE (sym));
  }

  void
  parser::yypush_ (const char* m, state_type s, YY_MOVE_REF (symbol_type) sym)
  {
#if 201103L <= YY_CPLUSPLUS
    yypush_ (m, stack_symbol_type (s, std::move (sym)));
#else
    stack_symbol_type ss (s, sym);
    yypush_ (m, ss);
#endif
  }

  void
  parser::yypop_ (int n) YY_NOEXCEPT
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - YYNTOKENS] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - YYNTOKENS];
  }

  bool
  parser::yy_pact_value_is_default_ (int yyvalue) YY_NOEXCEPT
  {
    return yyvalue == yypact_ninf_;
  }

  bool
  parser::yy_table_value_is_error_ (int yyvalue) YY_NOEXCEPT
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::operator() ()
  {
    return parse ();
  }

  int
  parser::parse ()
  {
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

#if YY_EXCEPTIONS
    try
#endif // YY_EXCEPTIONS
      {
    YYCDEBUG << "Starting parse\n";


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, YY_MOVE (yyla));

  /*-----------------------------------------------.
  | yynewstate -- push a new symbol on the stack.  |
  `-----------------------------------------------*/
  yynewstate:
    YYCDEBUG << "Entering state " << int (yystack_[0].state) << '\n';
    YY_STACK_PRINT ();

    // Accept?
    if (yystack_[0].state == yyfinal_)
      YYACCEPT;

    goto yybackup;


  /*-----------.
  | yybackup.  |
  `-----------*/
  yybackup:
    // Try to take a decision without lookahead.
    yyn = yypact_[+yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token\n";
#if YY_EXCEPTIONS
        try
#endif // YY_EXCEPTIONS
          {
            symbol_type yylookahead (yylex (parseServices));
            yyla.move (yylookahead);
          }
#if YY_EXCEPTIONS
        catch (const syntax_error& yyexc)
          {
            YYCDEBUG << "Caught exception: " << yyexc.what() << '\n';
            error (yyexc);
            goto yyerrlab1;
          }
#endif // YY_EXCEPTIONS
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    if (yyla.kind () == symbol_kind::S_YYerror)
    {
      // The scanner already issued an error message, process directly
      // to error recovery.  But do not keep the error token as
      // lookahead, it is too special and may lead us to an endless
      // loop in error recovery. */
      yyla.kind_ = symbol_kind::S_YYUNDEF;
      goto yyerrlab1;
    }

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.kind ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.kind ())
      {
        goto yydefault;
      }

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", state_type (yyn), YY_MOVE (yyla));
    goto yynewstate;


  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[+yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;


  /*-----------------------------.
  | yyreduce -- do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_ (yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
      switch (yyr1_[yyn])
    {
      case symbol_kind::S_block: // block
        yylhs.value.emplace< IBlock > ();
        break;

      case symbol_kind::S_item: // item
      case symbol_kind::S_itemi: // itemi
      case symbol_kind::S_itemac: // itemac
        yylhs.value.emplace< IItem > ();
        break;

      case symbol_kind::S_recbl: // recbl
      case symbol_kind::S_jsond: // jsond
        yylhs.value.emplace< IReceivable > ();
        break;

      case symbol_kind::S_STRWATTR: // "string with attributes"
        yylhs.value.emplace< IStringWithAttrs > ();
        break;

      case symbol_kind::S_ID: // "identifier"
      case symbol_kind::S_SELID: // "selector"
      case symbol_kind::S_ULONG: // "ulong"
      case symbol_kind::S_LONG: // "long"
      case symbol_kind::S_UINT: // "uint"
      case symbol_kind::S_INT: // "int"
      case symbol_kind::S_SHORT: // "short"
      case symbol_kind::S_CHAR: // "char"
      case symbol_kind::S_BOOL: // "bool"
      case symbol_kind::S_FLT: // "float"
      case symbol_kind::S_DBL: // "double"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_lit: // lit
      case symbol_kind::S_ssel: // ssel
      case symbol_kind::S_sel: // sel
        yylhs.value.emplace< ISymbol > ();
        break;

      case symbol_kind::S_stm: // stm
      case symbol_kind::S_unit: // unit
      case symbol_kind::S_expr: // expr
        yylhs.value.emplace< IUnit > ();
        break;

      case symbol_kind::S_pgm: // pgm
      case symbol_kind::S_stlist: // stlist
      case symbol_kind::S_cexpr: // cexpr
      case symbol_kind::S_dmexpr: // dmexpr
        yylhs.value.emplace< std::vector<IUnit> > ();
        break;

      default:
        break;
    }


      // Default location.
      {
        stack_type::slice range (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, range, yylen);
        yyerror_range[1].location = yylhs.location;
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
#if YY_EXCEPTIONS
      try
#endif // YY_EXCEPTIONS
        {
          switch (yyn)
            {
  case 2: // pgm: %empty
#line 153 "src/bison/i.y"
                      { /* pgm <-- %empty */; }
#line 969 "src/iparser.cpp"
    break;

  case 3: // pgm: stlist
#line 154 "src/bison/i.y"
                      { parseServices.parsedProgram = std::move(yystack_[0].value.as < std::vector<IUnit> > ()); }
#line 975 "src/iparser.cpp"
    break;

  case 4: // block: "{" stlist "}"
#line 156 "src/bison/i.y"
                       { yylhs.value.as < IBlock > () = std::move(yystack_[1].value.as < std::vector<IUnit> > ()); }
#line 981 "src/iparser.cpp"
    break;

  case 5: // stlist: stm
#line 158 "src/bison/i.y"
                      { if (!yystack_[0].value.as < IUnit > ().isNull()) yylhs.value.as < std::vector<IUnit> > ().push_back(std::move(yystack_[0].value.as < IUnit > ())); }
#line 987 "src/iparser.cpp"
    break;

  case 6: // stlist: stlist stm
#line 159 "src/bison/i.y"
                      { yylhs.value.as < std::vector<IUnit> > () = std::move(yystack_[1].value.as < std::vector<IUnit> > ());
                        if (!yystack_[0].value.as < IUnit > ().isNull()) yylhs.value.as < std::vector<IUnit> > ().push_back(std::move(yystack_[0].value.as < IUnit > ())); }
#line 994 "src/iparser.cpp"
    break;

  case 7: // stm: ";"
#line 162 "src/bison/i.y"
                      { /* stm: ";" */ }
#line 1000 "src/iparser.cpp"
    break;

  case 8: // stm: unit ";"
#line 163 "src/bison/i.y"
                      { if (yystack_[1].value.as < IUnit > ().isEncapsulatedBlock()) {
                          yystack_[1].value.as < IUnit > ().receiver.get<IBlock>().evaluate = false;
                          yystack_[1].value.as < IUnit > ().receiver.get<IBlock>().execute = true;
                        }
                        yystack_[1].value.as < IUnit > ().loc = yystack_[1].location;
                        yylhs.value.as < IUnit > () = std::move(yystack_[1].value.as < IUnit > ()); }
#line 1011 "src/iparser.cpp"
    break;

  case 9: // unit: expr
#line 170 "src/bison/i.y"
                      { yylhs.value.as < IUnit > () = std::move(yystack_[0].value.as < IUnit > ()); }
#line 1017 "src/iparser.cpp"
    break;

  case 10: // unit: item recbl
#line 171 "src/bison/i.y"
                      { yystack_[1].value.as < IItem > ().throwIfBlock(yystack_[1].location);
                        yylhs.value.as < IUnit > () = IUnit{std::move(yystack_[1].value.as < IItem > ()), std::move(yystack_[0].value.as < IReceivable > ())}; }
#line 1024 "src/iparser.cpp"
    break;

  case 11: // unit: "@" item recbl
#line 173 "src/bison/i.y"
                      { IUnit u{}; u.makeBlockInvocation(std::move(yystack_[1].value.as < IItem > ()), yystack_[1].location);
                        yylhs.value.as < IUnit > () = IUnit{IItem{std::move(u)}, std::move(yystack_[0].value.as < IReceivable > ())}; }
#line 1031 "src/iparser.cpp"
    break;

  case 12: // unit: unit "=>" recbl
#line 176 "src/bison/i.y"
                      { yylhs.value.as < IUnit > () = IUnit(IItem(std::move(yystack_[2].value.as < IUnit > ())), std::move(yystack_[0].value.as < IReceivable > ())); }
#line 1037 "src/iparser.cpp"
    break;

  case 13: // unit: item "..." "+=" "{" jsond "}"
#line 178 "src/bison/i.y"
                      { yystack_[5].value.as < IItem > ().throwIfBlock(yystack_[5].location);
                        yystack_[1].value.as < IReceivable > ().renderArgList(ISymbol{S_extBy, SYMTYPE_LITERAL_SEL});
                        yylhs.value.as < IUnit > () = IUnit{std::move(yystack_[5].value.as < IItem > ()), std::move(yystack_[1].value.as < IReceivable > ())}; }
#line 1045 "src/iparser.cpp"
    break;

  case 14: // item: "."
#line 182 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = ISymbol("this", SYMTYPE_LITERAL_ID); }
#line 1051 "src/iparser.cpp"
    break;

  case 15: // item: "identifier"
#line 183 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1057 "src/iparser.cpp"
    break;

  case 16: // item: lit
#line 184 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1063 "src/iparser.cpp"
    break;

  case 17: // item: itemac
#line 185 "src/bison/i.y"
                      {}
#line 1069 "src/iparser.cpp"
    break;

  case 18: // item: itemi
#line 186 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = std::move(yystack_[0].value.as < IItem > ()); }
#line 1075 "src/iparser.cpp"
    break;

  case 19: // item: item "." "identifier"
#line 187 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setMemberAccessExpression(std::move(yystack_[2].value.as < IItem > ()), std::move(yystack_[0].value.as < ISymbol > ()), yylhs.location); }
#line 1081 "src/iparser.cpp"
    break;

  case 20: // item: item "[" unit "]"
#line 189 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setSubscriptedExpression(std::move(yystack_[3].value.as < IItem > ()), std::move(yystack_[1].value.as < IUnit > ()), yylhs.location); }
#line 1087 "src/iparser.cpp"
    break;

  case 21: // item: "[" cexpr "]"
#line 190 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = IArrayExpression(std::move(yystack_[1].value.as < std::vector<IUnit> > ())); }
#line 1093 "src/iparser.cpp"
    break;

  case 22: // item: "[" cexpr "," "]"
#line 192 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = IArrayExpression(std::move(yystack_[2].value.as < std::vector<IUnit> > ())); }
#line 1099 "src/iparser.cpp"
    break;

  case 23: // item: "[" item "..." cexpr "]"
#line 194 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setArrayWithItemExpansion(std::move(yystack_[1].value.as < std::vector<IUnit> > ()), std::move(yystack_[3].value.as < IItem > ()), yylhs.location, false); }
#line 1105 "src/iparser.cpp"
    break;

  case 24: // item: "[" cexpr "," item "..." "]"
#line 196 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setArrayWithItemExpansion(std::move(yystack_[4].value.as < std::vector<IUnit> > ()), std::move(yystack_[2].value.as < IItem > ()), yylhs.location, true); }
#line 1111 "src/iparser.cpp"
    break;

  case 25: // item: "[" "]"
#line 197 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = IArrayExpression(); }
#line 1117 "src/iparser.cpp"
    break;

  case 26: // item: "[" "," "]"
#line 198 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = IArrayExpression(); }
#line 1123 "src/iparser.cpp"
    break;

  case 27: // item: "{" "}"
#line 199 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = IReceivable(); }
#line 1129 "src/iparser.cpp"
    break;

  case 28: // item: "{" jsond "}"
#line 200 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = std::move(yystack_[1].value.as < IReceivable > ()); }
#line 1135 "src/iparser.cpp"
    break;

  case 29: // item: "{" cexpr "}"
#line 201 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = ISetExpression(std::move(yystack_[1].value.as < std::vector<IUnit> > ())); }
#line 1141 "src/iparser.cpp"
    break;

  case 30: // item: "{" cexpr "," "}"
#line 203 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = ISetExpression(std::move(yystack_[2].value.as < std::vector<IUnit> > ())); }
#line 1147 "src/iparser.cpp"
    break;

  case 31: // item: "{" item "..." cexpr "}"
#line 205 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setSetWithItemExpansion(std::move(yystack_[1].value.as < std::vector<IUnit> > ()), std::move(yystack_[3].value.as < IItem > ()), yylhs.location, false); }
#line 1153 "src/iparser.cpp"
    break;

  case 32: // item: "{" cexpr "," item "..." "}"
#line 207 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().setSetWithItemExpansion(std::move(yystack_[4].value.as < std::vector<IUnit> > ()), std::move(yystack_[2].value.as < IItem > ()), yylhs.location, true); }
#line 1159 "src/iparser.cpp"
    break;

  case 33: // item: "{" "," "}"
#line 208 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = ISetExpression(); }
#line 1165 "src/iparser.cpp"
    break;

  case 34: // item: "(" unit ")"
#line 209 "src/bison/i.y"
                      { if (yystack_[1].value.as < IUnit > ().isEncapsulatedBlock())
                          yylhs.value.as < IItem > () = std::move(yystack_[1].value.as < IUnit > ().receiver);
                        else
                          yylhs.value.as < IItem > () = std::move(yystack_[1].value.as < IUnit > ()); }
#line 1174 "src/iparser.cpp"
    break;

  case 35: // item: block
#line 213 "src/bison/i.y"
                      { yylhs.value.as < IItem > () = std::move(yystack_[0].value.as < IBlock > ()); }
#line 1180 "src/iparser.cpp"
    break;

  case 36: // itemi: "string with attributes"
#line 215 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().fromStringWithAttrs(parseServices,
                            std::move(yystack_[0].value.as < IStringWithAttrs > ().name), yystack_[0].value.as < IStringWithAttrs > ().attrs, yystack_[0].location); }
#line 1187 "src/iparser.cpp"
    break;

  case 37: // itemi: "string with attributes" "{" cexpr "}"
#line 218 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().fromInitListExpr(parseServices,
                            std::move(yystack_[3].value.as < IStringWithAttrs > ().name), yystack_[3].value.as < IStringWithAttrs > ().attrs, std::move(yystack_[1].value.as < std::vector<IUnit> > ()), yystack_[3].location); }
#line 1194 "src/iparser.cpp"
    break;

  case 38: // itemi: "string with attributes" "{" cexpr "," "}"
#line 221 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().fromInitListExpr(parseServices,
                            std::move(yystack_[4].value.as < IStringWithAttrs > ().name), yystack_[4].value.as < IStringWithAttrs > ().attrs, std::move(yystack_[2].value.as < std::vector<IUnit> > ()), yystack_[4].location); }
#line 1201 "src/iparser.cpp"
    break;

  case 39: // itemi: "string with attributes" "{" "<" cexpr ">}"
#line 224 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().fromTemplateInitExpr(parseServices,
                            std::move(yystack_[4].value.as < IStringWithAttrs > ().name), yystack_[4].value.as < IStringWithAttrs > ().attrs, std::move(yystack_[1].value.as < std::vector<IUnit> > ()), yystack_[4].location);}
#line 1208 "src/iparser.cpp"
    break;

  case 40: // itemi: "string with attributes" "{" "<" cexpr "," ">}"
#line 227 "src/bison/i.y"
                      { yylhs.value.as < IItem > ().fromTemplateInitExpr(parseServices,
                            std::move(yystack_[5].value.as < IStringWithAttrs > ().name), yystack_[5].value.as < IStringWithAttrs > ().attrs, std::move(yystack_[2].value.as < std::vector<IUnit> > ()), yystack_[5].location);}
#line 1215 "src/iparser.cpp"
    break;

  case 41: // itemac: "identifier" "::" "identifier"
#line 230 "src/bison/i.y"
                      { throw syntax_error(yystack_[2].location, "the symbol '"+yystack_[0].value.as < ISymbol > ().name+"' "
                            "cannot be accessed as '"+yystack_[2].value.as < ISymbol > ().name+"::"+yystack_[0].value.as < ISymbol > ().name+"' "
                            "in this context");}
#line 1223 "src/iparser.cpp"
    break;

  case 42: // itemac: "identifier" "::" itemac
#line 234 "src/bison/i.y"
                      {}
#line 1229 "src/iparser.cpp"
    break;

  case 43: // lit: "ulong"
#line 236 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1235 "src/iparser.cpp"
    break;

  case 44: // lit: "long"
#line 237 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1241 "src/iparser.cpp"
    break;

  case 45: // lit: "uint"
#line 238 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1247 "src/iparser.cpp"
    break;

  case 46: // lit: "int"
#line 239 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1253 "src/iparser.cpp"
    break;

  case 47: // lit: "short"
#line 240 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1259 "src/iparser.cpp"
    break;

  case 48: // lit: "char"
#line 241 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1265 "src/iparser.cpp"
    break;

  case 49: // lit: "bool"
#line 242 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1271 "src/iparser.cpp"
    break;

  case 50: // lit: "float"
#line 243 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1277 "src/iparser.cpp"
    break;

  case 51: // lit: "double"
#line 244 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1283 "src/iparser.cpp"
    break;

  case 52: // lit: "string"
#line 245 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1289 "src/iparser.cpp"
    break;

  case 53: // cexpr: unit
#line 247 "src/bison/i.y"
                      { yylhs.value.as < std::vector<IUnit> > ().push_back(std::move(yystack_[0].value.as < IUnit > ())); }
#line 1295 "src/iparser.cpp"
    break;

  case 54: // cexpr: cexpr "," cexpr
#line 249 "src/bison/i.y"
                      { yylhs.value.as < std::vector<IUnit> > ().reserve(yystack_[2].value.as < std::vector<IUnit> > ().size() + yystack_[0].value.as < std::vector<IUnit> > ().size());
                        yylhs.value.as < std::vector<IUnit> > () = std::move(yystack_[2].value.as < std::vector<IUnit> > ());
                        for (auto& i : yystack_[0].value.as < std::vector<IUnit> > ())
                          yylhs.value.as < std::vector<IUnit> > ().push_back(std::move(i)); }
#line 1304 "src/iparser.cpp"
    break;

  case 55: // expr: item
#line 297 "src/bison/i.y"
                      { yylhs.value.as < IUnit > () = std::move(yystack_[0].value.as < IItem > ()); }
#line 1310 "src/iparser.cpp"
    break;

  case 56: // expr: expr "||=" expr
#line 299 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_orBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1317 "src/iparser.cpp"
    break;

  case 57: // expr: expr "^^=" expr
#line 302 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_xorBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1324 "src/iparser.cpp"
    break;

  case 58: // expr: expr "&&=" expr
#line 305 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_andBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1331 "src/iparser.cpp"
    break;

  case 59: // expr: expr "|=" expr
#line 308 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitorBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1338 "src/iparser.cpp"
    break;

  case 60: // expr: expr "^=" expr
#line 311 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitxorBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1345 "src/iparser.cpp"
    break;

  case 61: // expr: expr "&=" expr
#line 314 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitandBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1352 "src/iparser.cpp"
    break;

  case 62: // expr: expr ">>=" expr
#line 317 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_shiftrBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1359 "src/iparser.cpp"
    break;

  case 63: // expr: expr "<<=" expr
#line 320 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_shiftlBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1366 "src/iparser.cpp"
    break;

  case 64: // expr: expr "-=" expr
#line 323 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_decBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1373 "src/iparser.cpp"
    break;

  case 65: // expr: expr "+=" expr
#line 326 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_incBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1380 "src/iparser.cpp"
    break;

  case 66: // expr: expr "%=" expr
#line 329 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_modBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1387 "src/iparser.cpp"
    break;

  case 67: // expr: expr "/=" expr
#line 332 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_divBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1394 "src/iparser.cpp"
    break;

  case 68: // expr: expr "*=" expr
#line 335 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_mulBy, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1401 "src/iparser.cpp"
    break;

  case 69: // expr: expr ":=" expr
#line 338 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setReferenceOperation(std::move(yystack_[2].value.as < IUnit > ()), std::move(yystack_[0].value.as < IUnit > ()), yystack_[1].location, false); }
#line 1408 "src/iparser.cpp"
    break;

  case 70: // expr: expr "::=" expr
#line 341 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setReferenceOperation(std::move(yystack_[2].value.as < IUnit > ()), std::move(yystack_[0].value.as < IUnit > ()), yystack_[1].location, true); }
#line 1415 "src/iparser.cpp"
    break;

  case 71: // expr: expr "=" expr
#line 344 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setAssignmentOperation(std::move(yystack_[2].value.as < IUnit > ()), std::move(yystack_[0].value.as < IUnit > ()), yystack_[1].location, false); }
#line 1422 "src/iparser.cpp"
    break;

  case 72: // expr: expr "@=" expr
#line 347 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setAssignmentOperation(std::move(yystack_[2].value.as < IUnit > ()), std::move(yystack_[0].value.as < IUnit > ()), yystack_[1].location, true); }
#line 1429 "src/iparser.cpp"
    break;

  case 73: // expr: dmexpr ":" expr
#line 350 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setConditionalOperation(
                            parseServices.inlineConditionalOperator(),
                            std::move(yystack_[2].value.as < std::vector<IUnit> > ()[0]), std::move(yystack_[2].value.as < std::vector<IUnit> > ()[1]), std::move(yystack_[0].value.as < IUnit > ()), yystack_[2].location); }
#line 1438 "src/iparser.cpp"
    break;

  case 74: // expr: "{" item "..." jsond "}"
#line 355 "src/bison/i.y"
                      { yystack_[3].value.as < IItem > ().throwIfBlock(yystack_[3].location);
                        yystack_[1].value.as < IReceivable > ().renderArgList(ISymbol{S_ext, SYMTYPE_LITERAL_SEL});
                        yylhs.value.as < IUnit > () = IUnit{std::move(yystack_[3].value.as < IItem > ()), std::move(yystack_[1].value.as < IReceivable > ())}; }
#line 1446 "src/iparser.cpp"
    break;

  case 75: // expr: expr "||" expr
#line 359 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S__or, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1453 "src/iparser.cpp"
    break;

  case 76: // expr: expr "^^" expr
#line 362 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S__xor, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1460 "src/iparser.cpp"
    break;

  case 77: // expr: expr "&&" expr
#line 365 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S__and, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1467 "src/iparser.cpp"
    break;

  case 78: // expr: expr "|" expr
#line 368 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitor, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1474 "src/iparser.cpp"
    break;

  case 79: // expr: expr "^" expr
#line 371 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitxor, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1481 "src/iparser.cpp"
    break;

  case 80: // expr: expr "&" expr
#line 374 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_bitand, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1488 "src/iparser.cpp"
    break;

  case 81: // expr: expr "!=" expr
#line 377 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_ne, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1495 "src/iparser.cpp"
    break;

  case 82: // expr: expr "==" expr
#line 380 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_eq, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1502 "src/iparser.cpp"
    break;

  case 83: // expr: expr ">=" expr
#line 383 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_ge, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1509 "src/iparser.cpp"
    break;

  case 84: // expr: expr ">" expr
#line 386 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_gt, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1516 "src/iparser.cpp"
    break;

  case 85: // expr: expr "<=" expr
#line 389 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_le, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1523 "src/iparser.cpp"
    break;

  case 86: // expr: expr "<" expr
#line 392 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_lt, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1530 "src/iparser.cpp"
    break;

  case 87: // expr: expr ">>" expr
#line 395 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_shiftr, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1537 "src/iparser.cpp"
    break;

  case 88: // expr: expr "<<" expr
#line 398 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_shiftl, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1544 "src/iparser.cpp"
    break;

  case 89: // expr: expr "-" expr
#line 401 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_sub, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1551 "src/iparser.cpp"
    break;

  case 90: // expr: expr "+" expr
#line 404 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_add, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1558 "src/iparser.cpp"
    break;

  case 91: // expr: expr "%" expr
#line 407 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_mod, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1565 "src/iparser.cpp"
    break;

  case 92: // expr: expr "/" expr
#line 410 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_div, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1572 "src/iparser.cpp"
    break;

  case 93: // expr: expr "*" expr
#line 413 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_mul, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1579 "src/iparser.cpp"
    break;

  case 94: // expr: expr "**" expr
#line 416 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().loc = yystack_[2].location; yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setBinaryOperation(std::move(yystack_[2].value.as < IUnit > ()), S_exp, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1586 "src/iparser.cpp"
    break;

  case 95: // expr: "+" expr
#line 419 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation("+", std::move(yystack_[0].value.as < IUnit > ())); }
#line 1593 "src/iparser.cpp"
    break;

  case 96: // expr: "-" expr
#line 422 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_neg, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1600 "src/iparser.cpp"
    break;

  case 97: // expr: "!" expr
#line 425 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S__not, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1607 "src/iparser.cpp"
    break;

  case 98: // expr: "~" expr
#line 428 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_bitinv, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1614 "src/iparser.cpp"
    break;

  case 99: // expr: expr "++"
#line 431 "src/bison/i.y"
                      { yystack_[1].value.as < IUnit > ().loc = yystack_[1].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_postinc, std::move(yystack_[1].value.as < IUnit > ())); }
#line 1621 "src/iparser.cpp"
    break;

  case 100: // expr: expr "--"
#line 434 "src/bison/i.y"
                      { yystack_[1].value.as < IUnit > ().loc = yystack_[1].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_postdec, std::move(yystack_[1].value.as < IUnit > ())); }
#line 1628 "src/iparser.cpp"
    break;

  case 101: // expr: "++" expr
#line 437 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_inc, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1635 "src/iparser.cpp"
    break;

  case 102: // expr: "--" expr
#line 440 "src/bison/i.y"
                      { yystack_[0].value.as < IUnit > ().loc = yystack_[0].location;
                        yylhs.value.as < IUnit > ().setUnaryOperation(S_dec, std::move(yystack_[0].value.as < IUnit > ())); }
#line 1642 "src/iparser.cpp"
    break;

  case 103: // expr: "@" item
#line 442 "src/bison/i.y"
                              { yylhs.value.as < IUnit > ().makeBlockInvocation(std::move(yystack_[0].value.as < IItem > ()), yystack_[0].location); }
#line 1648 "src/iparser.cpp"
    break;

  case 104: // recbl: ssel
#line 444 "src/bison/i.y"
                      { yylhs.value.as < IReceivable > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1654 "src/iparser.cpp"
    break;

  case 105: // recbl: jsond
#line 445 "src/bison/i.y"
                      { yylhs.value.as < IReceivable > () = std::move(yystack_[0].value.as < IReceivable > ()); }
#line 1660 "src/iparser.cpp"
    break;

  case 106: // jsond: sel expr
#line 447 "src/bison/i.y"
                      { yylhs.value.as < IReceivable > () = IReceivable(std::move(yystack_[1].value.as < ISymbol > ()), /*TODO*/ IUnit(std::move(yystack_[0].value.as < IUnit > ()))); }
#line 1666 "src/iparser.cpp"
    break;

  case 107: // jsond: jsond "selector" expr
#line 449 "src/bison/i.y"
                      { yylhs.value.as < IReceivable > () = std::move(yystack_[2].value.as < IReceivable > ()) +
                             IReceivable(std::move(yystack_[1].value.as < ISymbol > ()), IUnit(std::move(yystack_[0].value.as < IUnit > ()))); }
#line 1673 "src/iparser.cpp"
    break;

  case 108: // jsond: jsond ":" expr
#line 452 "src/bison/i.y"
                      { ISymbol sym{":", SYMTYPE_LITERAL_SEL};
                        yylhs.value.as < IReceivable > () = std::move(yystack_[2].value.as < IReceivable > ()) +
                             IReceivable(std::move(sym), IUnit(std::move(yystack_[0].value.as < IUnit > ()))); }
#line 1681 "src/iparser.cpp"
    break;

  case 109: // ssel: "identifier"
#line 456 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1687 "src/iparser.cpp"
    break;

  case 110: // ssel: "identifier" "::" ssel
#line 457 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); yylhs.value.as < ISymbol > ().name = yystack_[2].value.as < ISymbol > ().name+"::"+yylhs.value.as < ISymbol > ().name; }
#line 1693 "src/iparser.cpp"
    break;

  case 111: // ssel: "string" "::" ssel
#line 459 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); yylhs.value.as < ISymbol > ().name = yystack_[2].value.as < ISymbol > ().name+"::"+yylhs.value.as < ISymbol > ().name; }
#line 1699 "src/iparser.cpp"
    break;

  case 112: // sel: "selector"
#line 461 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); }
#line 1705 "src/iparser.cpp"
    break;

  case 113: // sel: "identifier" "::" sel
#line 462 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); yylhs.value.as < ISymbol > ().name = yystack_[2].value.as < ISymbol > ().name+"::"+yylhs.value.as < ISymbol > ().name; }
#line 1711 "src/iparser.cpp"
    break;

  case 114: // sel: "string" "::" sel
#line 464 "src/bison/i.y"
                      { yylhs.value.as < ISymbol > () = std::move(yystack_[0].value.as < ISymbol > ()); yylhs.value.as < ISymbol > ().name = yystack_[2].value.as < ISymbol > ().name+"::"+yylhs.value.as < ISymbol > ().name; }
#line 1717 "src/iparser.cpp"
    break;

  case 115: // dmexpr: expr "?" expr
#line 466 "src/bison/i.y"
                      { yystack_[2].value.as < IUnit > ().throwIfReceiverIsBlock(yystack_[2].location);
                        yylhs.value.as < std::vector<IUnit> > ().reserve(2);
                        yylhs.value.as < std::vector<IUnit> > ().emplace_back(std::move(yystack_[2].value.as < IUnit > ()));
                        yylhs.value.as < std::vector<IUnit> > ().emplace_back(std::move(yystack_[0].value.as < IUnit > ()));
                        yylhs.value.as < std::vector<IUnit> > ()[0].loc = yystack_[2].location;
                        yylhs.value.as < std::vector<IUnit> > ()[1].loc = yystack_[0].location; }
#line 1728 "src/iparser.cpp"
    break;


#line 1732 "src/iparser.cpp"

            default:
              break;
            }
        }
#if YY_EXCEPTIONS
      catch (const syntax_error& yyexc)
        {
          YYCDEBUG << "Caught exception: " << yyexc.what() << '\n';
          error (yyexc);
          YYERROR;
        }
#endif // YY_EXCEPTIONS
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, YY_MOVE (yylhs));
    }
    goto yynewstate;


  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        context yyctx (*this, yyla);
        std::string msg = yysyntax_error_ (yyctx);
        error (yyla.location, YY_MOVE (msg));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.kind () == symbol_kind::S_YYEOF)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:
    /* Pacify compilers when the user code never invokes YYERROR and
       the label yyerrorlab therefore never appears in user code.  */
    if (false)
      YYERROR;

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();
    goto yyerrlab1;


  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    // Pop stack until we find a state that shifts the error token.
    for (;;)
      {
        yyn = yypact_[+yystack_[0].state];
        if (!yy_pact_value_is_default_ (yyn))
          {
            yyn += symbol_kind::S_YYerror;
            if (0 <= yyn && yyn <= yylast_
                && yycheck_[yyn] == symbol_kind::S_YYerror)
              {
                yyn = yytable_[yyn];
                if (0 < yyn)
                  break;
              }
          }

        // Pop the current state because it cannot handle the error token.
        if (yystack_.size () == 1)
          YYABORT;

        yyerror_range[1].location = yystack_[0].location;
        yy_destroy_ ("Error: popping", yystack_[0]);
        yypop_ ();
        YY_STACK_PRINT ();
      }
    {
      stack_symbol_type error_token;

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = state_type (yyn);
      yypush_ ("Shifting", YY_MOVE (error_token));
    }
    goto yynewstate;


  /*-------------------------------------.
  | yyacceptlab -- YYACCEPT comes here.  |
  `-------------------------------------*/
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;


  /*-----------------------------------.
  | yyabortlab -- YYABORT comes here.  |
  `-----------------------------------*/
  yyabortlab:
    yyresult = 1;
    goto yyreturn;


  /*-----------------------------------------------------.
  | yyreturn -- parsing is finished, return the result.  |
  `-----------------------------------------------------*/
  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    YY_STACK_PRINT ();
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
#if YY_EXCEPTIONS
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack\n";
        // Do not try to display the values of the reclaimed symbols,
        // as their printers might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
#endif // YY_EXCEPTIONS
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what ());
  }

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr;
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              else
                goto append;

            append:
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

  std::string
  parser::symbol_name (symbol_kind_type yysymbol)
  {
    return yytnamerr_ (yytname_[yysymbol]);
  }



  // parser::context.
  parser::context::context (const parser& yyparser, const symbol_type& yyla)
    : yyparser_ (yyparser)
    , yyla_ (yyla)
  {}

  int
  parser::context::expected_tokens (symbol_kind_type yyarg[], int yyargn) const
  {
    // Actual number of expected tokens
    int yycount = 0;

    const int yyn = yypact_[+yyparser_.yystack_[0].state];
    if (!yy_pact_value_is_default_ (yyn))
      {
        /* Start YYX at -YYN if negative to avoid negative indexes in
           YYCHECK.  In other words, skip the first -YYN actions for
           this state because they are default actions.  */
        const int yyxbegin = yyn < 0 ? -yyn : 0;
        // Stay within bounds of both yycheck and yytname.
        const int yychecklim = yylast_ - yyn + 1;
        const int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
        for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
          if (yycheck_[yyx + yyn] == yyx && yyx != symbol_kind::S_YYerror
              && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
            {
              if (!yyarg)
                ++yycount;
              else if (yycount == yyargn)
                return 0;
              else
                yyarg[yycount++] = YY_CAST (symbol_kind_type, yyx);
            }
      }

    if (yyarg && yycount == 0 && 0 < yyargn)
      yyarg[0] = symbol_kind::S_YYEMPTY;
    return yycount;
  }






  int
  parser::yy_syntax_error_arguments_ (const context& yyctx,
                                                 symbol_kind_type yyarg[], int yyargn) const
  {
    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state merging
         (from LALR or IELR) and default reductions corrupt the expected
         token list.  However, the list is correct for canonical LR with
         one exception: it will still contain any token that will not be
         accepted due to an error action in a later state.
    */

    if (!yyctx.lookahead ().empty ())
      {
        if (yyarg)
          yyarg[0] = yyctx.token ();
        int yyn = yyctx.expected_tokens (yyarg ? yyarg + 1 : yyarg, yyargn - 1);
        return yyn + 1;
      }
    return 0;
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (const context& yyctx) const
  {
    // Its maximum.
    enum { YYARGS_MAX = 5 };
    // Arguments of yyformat.
    symbol_kind_type yyarg[YYARGS_MAX];
    int yycount = yy_syntax_error_arguments_ (yyctx, yyarg, YYARGS_MAX);

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
      default: // Avoid compiler warnings.
        YYCASE_ (0, YY_("syntax error"));
        YYCASE_ (1, YY_("syntax error, unexpected %s"));
        YYCASE_ (2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_ (3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_ (4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_ (5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    std::ptrdiff_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += symbol_name (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char parser::yypact_ninf_ = -121;

  const signed char parser::yytable_ninf_ = -1;

  const short
  parser::yypact_[] =
  {
     411,  1120,   701,   701,   701,   701,  -121,   701,   701,   742,
     288,   244,  -121,   -42,  -121,  -121,  -121,  -121,  -121,  -121,
    -121,  -121,  -121,  -121,   -35,    52,  -121,   411,  -121,    -1,
       8,  -121,  -121,  -121,   814,   -22,   244,    62,  1120,   -18,
      65,    65,    65,    65,    65,    65,    30,   -21,  -121,    49,
      11,    25,    43,  -121,     6,  -121,    33,   329,    -1,    13,
      35,    89,   701,    46,   455,  -121,  -121,   -43,  -121,   114,
      67,   742,    70,    73,  -121,   -15,  -121,   701,   701,   701,
     701,   701,   701,   701,   701,   701,   701,   701,   701,   701,
     701,   701,   701,   701,   701,   701,   701,   701,   701,   701,
     701,   701,   701,   701,   701,   701,   701,   701,   701,   701,
     701,   701,  -121,  -121,   701,   701,   701,   701,    15,  -121,
     -18,  -121,  -121,   496,   537,  -121,  -121,   -23,    36,  -121,
     370,   578,  -121,  -121,   701,   701,   814,   -42,  -121,   742,
      37,  -121,    78,  -121,     9,   -43,   -43,  1087,  1123,   863,
     863,   863,   902,   902,   976,   976,   939,   939,   939,  1013,
    1050,   814,  1174,  1201,  1227,  1252,  1276,   229,  1299,   127,
    1320,  1340,   154,  1358,   134,   134,   290,   290,    63,    63,
      63,    63,   814,   814,   814,   814,   496,    32,  -121,    28,
    -121,     6,    33,  -121,    77,  -121,    40,   146,  -121,    41,
     814,   814,    21,   619,  -121,    36,  -121,  -121,  -121,   742,
    -121,    -2,    36,  -121,  -121,     1,   660,  -121,  -121,   226,
    -121,  -121,  -121,  -121
  };

  const signed char
  parser::yydefact_[] =
  {
       2,     0,     0,     0,     0,     0,    14,     0,     0,     0,
       0,     0,     7,    15,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    36,     0,    35,     3,     5,     0,
      55,    18,    17,    16,     9,     0,     0,   103,     0,    55,
      95,    96,    98,    97,   101,   102,     0,     0,    25,    53,
      55,     0,     0,    27,    15,   112,    52,     0,    53,    55,
       0,     0,     0,     0,     0,     1,     6,     0,     8,     0,
       0,     0,   109,     0,    10,   105,   104,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    99,   100,     0,     0,     0,     0,    55,    11,
     103,    34,    26,     0,     0,    21,    33,     0,     0,     4,
       0,     0,    29,    28,     0,     0,   106,    41,    42,     0,
       0,    12,     0,    19,     0,     0,     0,    69,    70,    60,
      59,    61,    63,    62,    65,    64,    66,    67,    68,    71,
      72,   115,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    90,    89,    93,    92,
      91,    94,    57,    58,    56,    73,     0,     0,    22,    55,
      54,    41,     0,   113,     0,   114,     0,     0,    30,    55,
     108,   107,     0,     0,    37,     0,    20,   110,   111,     0,
      23,     0,     0,    31,    74,     0,     0,    39,    38,     0,
      24,    32,    40,    13
  };

  const short
  parser::yypgoto_[] =
  {
    -121,  -121,  -121,   141,    -9,    99,     0,  -121,   -61,  -121,
      -5,   140,   -17,    -8,   -25,  -120,  -121
  };

  const unsigned char
  parser::yydefgoto_[] =
  {
       0,    25,    26,    57,    28,    49,    39,    31,    32,    33,
     190,    34,    74,    75,    76,    62,    35
  };

  const short
  parser::yytable_[] =
  {
      30,    37,   138,    61,    67,    51,    60,   193,   195,    30,
      50,    59,    69,   142,    67,   123,   142,   130,    66,   186,
     119,    63,    72,    55,    64,   193,   195,    30,    61,    70,
     216,    60,   211,    73,   124,    67,   118,   122,   120,    71,
     117,   209,   191,    55,   131,   215,   203,   134,    66,   209,
     141,   135,    65,   192,    67,    70,   220,    30,    70,   140,
      70,   221,    70,    68,    30,    71,   138,   206,    71,   127,
      71,    30,    71,    72,    55,    70,    72,    55,    72,    55,
      72,    55,   217,   125,    73,    71,   121,    73,    70,    73,
     210,    73,   193,    72,    55,   132,   128,   204,    71,    29,
     213,   194,    55,   126,    73,   111,    72,    55,    46,    70,
      58,   137,   192,   112,   113,   112,   113,    73,   187,    71,
     207,   208,   197,    30,   189,   196,    29,    72,    55,   142,
      30,   199,   143,   145,   202,    58,   146,   205,    73,    30,
     212,    27,    40,    41,    42,    43,     0,    44,    45,   133,
       0,   134,     0,     0,     0,   135,    29,    -1,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     144,   106,   107,   108,   109,   110,   111,   112,   113,     0,
       0,   196,     0,     0,   112,   113,    30,    -1,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   219,     0,     0,
       0,     0,   136,    30,   112,   113,   214,     0,   134,    30,
       0,     0,   135,     0,     0,     0,    30,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,     1,    52,   182,   183,   184,   185,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,     0,     0,   200,   201,     0,     0,     0,   112,
     113,     2,     3,     0,     0,     0,   223,     0,   134,     4,
       5,     6,   135,     0,     7,     8,     1,    47,     0,     9,
       0,    10,     0,    11,    53,     0,     0,     0,    12,    54,
      55,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      56,    24,     0,     0,     0,     2,     3,     0,     0,   108,
     109,   110,   111,     4,     5,     6,     0,     1,     7,     8,
     112,   113,     0,     9,     0,    10,    48,    11,     0,     0,
       0,     0,     0,    13,     0,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,     2,     3,     0,     0,
       0,     0,     0,     0,     4,     5,     6,     0,     1,     7,
       8,     0,     0,     0,     9,   142,    10,     0,    11,   129,
       0,     0,     0,    12,    13,     0,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,     2,     3,     0,
       0,     0,     0,     0,     0,     4,     5,     6,     0,     1,
       7,     8,     0,     0,     0,     9,     0,    10,     0,    11,
       0,     0,     0,     0,     0,    54,    55,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    56,    24,     2,     3,
       0,     0,     0,     0,     0,     0,     4,     5,     6,     0,
       0,     7,     8,     1,     0,     0,     9,     0,    10,     0,
      11,     0,     0,     0,     0,    12,    13,     0,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,   139,
       0,     0,     2,     3,     0,     0,     0,     0,     0,     0,
       4,     5,     6,     0,     1,     7,     8,     0,     0,     0,
       9,   142,    10,     0,    11,     0,     0,     0,     0,     0,
      13,     0,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,     2,     3,     0,     0,     0,     0,     0,
       0,     4,     5,     6,     0,     1,     7,     8,     0,     0,
       0,     9,     0,    10,     0,    11,     0,     0,     0,     0,
       0,    13,     0,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,     2,     3,     0,     0,     0,     0,
       0,     0,     4,     5,     6,     0,     1,     7,     8,     0,
       0,     0,     9,     0,    10,   188,    11,     0,     0,     0,
       0,     0,    13,     0,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,     2,     3,     0,     0,     0,
       0,     0,     0,     4,     5,     6,     0,     1,     7,     8,
       0,     0,     0,     9,     0,    10,     0,    11,   198,     0,
       0,     0,     0,    13,     0,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,     2,     3,     0,     0,
       0,     0,     0,     0,     4,     5,     6,     0,     1,     7,
       8,     0,     0,     0,     9,     0,    10,     0,    11,   218,
       0,     0,     0,     0,    13,     0,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,     2,     3,     0,
       0,     0,     0,     0,     0,     4,     5,     6,     0,    38,
       7,     8,     0,     0,     0,     9,     0,    10,     0,    11,
       0,   222,     0,     0,     0,    13,     0,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,     2,     3,
       0,     0,     0,     0,     0,     0,     4,     5,     6,     0,
       1,     7,     8,     0,     0,     0,     9,     0,    10,     0,
      11,     0,     0,     0,     0,     0,    13,     0,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,     2,
       3,     0,     0,     0,     0,     0,     0,     4,     5,     6,
       0,     0,     7,     8,     0,     0,     0,     9,     0,    10,
       0,    11,     0,     0,     0,     0,     0,    13,     0,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      77,    78,     0,     0,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,     0,     0,     0,
       0,     0,     0,     0,   112,   113,   114,   115,   116,    77,
      78,     0,     0,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,     0,     0,    77,    78,
       0,     0,     0,   112,   113,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,    77,    78,     0,     0,     0,
       0,     0,   112,   113,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,    77,    78,     0,     0,     0,     0,     0,   112,
     113,    84,    85,     0,     0,     0,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,    77,
      78,     0,     0,     0,     0,     0,   112,   113,     0,     0,
       0,     0,     0,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,    77,    78,     0,     0,
       0,     0,     0,   112,   113,     0,     0,     0,     0,     0,
       0,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,    -1,    78,     0,     0,     0,     0,     0,
     112,   113,     0,     0,     0,     0,     0,     0,     0,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
      -1,     0,     0,     0,     0,     0,     0,   112,   113,     0,
       0,     0,     0,     0,     0,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,     0,     6,     0,     0,
       0,     0,     0,   112,   113,     9,     0,    10,     0,    36,
       0,     0,     0,     0,     0,    13,     0,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,     0,     0,     0,
       0,     0,     0,     0,   112,   113,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,     0,     0,     0,     0,     0,     0,
       0,   112,   113,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
       0,     0,     0,     0,     0,     0,     0,   112,   113,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,     0,     0,     0,     0,     0,
       0,     0,   112,   113,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,     0,
       0,     0,     0,     0,     0,     0,   112,   113,    -1,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,     0,     0,     0,     0,     0,     0,     0,   112,
     113,    -1,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,     0,     0,     0,     0,     0,     0,     0,
     112,   113,    -1,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,     0,     0,     0,     0,     0,     0,     0,
     112,   113,    -1,   104,   105,   106,   107,   108,   109,   110,
     111,     0,     0,     0,     0,     0,     0,     0,   112,   113
  };

  const short
  parser::yycheck_[] =
  {
       0,     1,    63,    11,     5,    10,    11,   127,   128,     9,
      10,    11,     4,    15,     5,     4,    15,     4,    27,     4,
      37,    63,    65,    66,    59,   145,   146,    27,    36,    47,
       9,    36,     4,    76,     9,     5,    36,    58,    38,    57,
      62,     9,    65,    66,     9,     4,     9,    62,    57,     9,
      67,    66,     0,    76,     5,    47,    58,    57,    47,    64,
      47,    60,    47,    64,    64,    57,   127,    58,    57,    63,
      57,    71,    57,    65,    66,    47,    65,    66,    65,    66,
      65,    66,    61,    58,    76,    57,    56,    76,    47,    76,
      58,    76,   212,    65,    66,    60,    63,    60,    57,     0,
      60,    65,    66,    60,    76,    42,    65,    66,     9,    47,
      11,    65,    76,    50,    51,    50,    51,    76,   123,    57,
     145,   146,   130,   123,   124,   130,    27,    65,    66,    15,
     130,   131,    65,    63,   139,    36,    63,    59,    76,   139,
      63,     0,     2,     3,     4,     5,    -1,     7,     8,    60,
      -1,    62,    -1,    -1,    -1,    66,    57,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      71,    37,    38,    39,    40,    41,    42,    50,    51,    -1,
      -1,   186,    -1,    -1,    50,    51,   186,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,   205,    -1,    -1,
      -1,    -1,    62,   203,    50,    51,    60,    -1,    62,   209,
      -1,    -1,    66,    -1,    -1,    -1,   216,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,     8,     9,   114,   115,   116,   117,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    -1,    -1,   134,   135,    -1,    -1,    -1,    50,
      51,    37,    38,    -1,    -1,    -1,    60,    -1,    62,    45,
      46,    47,    66,    -1,    50,    51,     8,     9,    -1,    55,
      -1,    57,    -1,    59,    60,    -1,    -1,    -1,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    -1,    -1,    -1,    37,    38,    -1,    -1,    39,
      40,    41,    42,    45,    46,    47,    -1,     8,    50,    51,
      50,    51,    -1,    55,    -1,    57,    58,    59,    -1,    -1,
      -1,    -1,    -1,    65,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,     8,    50,
      51,    -1,    -1,    -1,    55,    15,    57,    -1,    59,    60,
      -1,    -1,    -1,    64,    65,    -1,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,     8,
      50,    51,    -1,    -1,    -1,    55,    -1,    57,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      -1,    50,    51,     8,    -1,    -1,    55,    -1,    57,    -1,
      59,    -1,    -1,    -1,    -1,    64,    65,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    34,
      -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,     8,    50,    51,    -1,    -1,    -1,
      55,    15,    57,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      65,    -1,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,     8,    50,    51,    -1,    -1,
      -1,    55,    -1,    57,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    65,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,     8,    50,    51,    -1,
      -1,    -1,    55,    -1,    57,    58,    59,    -1,    -1,    -1,
      -1,    -1,    65,    -1,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    37,    38,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,     8,    50,    51,
      -1,    -1,    -1,    55,    -1,    57,    -1,    59,    60,    -1,
      -1,    -1,    -1,    65,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,     8,    50,
      51,    -1,    -1,    -1,    55,    -1,    57,    -1,    59,    60,
      -1,    -1,    -1,    -1,    65,    -1,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,     8,
      50,    51,    -1,    -1,    -1,    55,    -1,    57,    -1,    59,
      -1,    61,    -1,    -1,    -1,    65,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
       8,    50,    51,    -1,    -1,    -1,    55,    -1,    57,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    65,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    37,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    -1,    50,    51,    -1,    -1,    -1,    55,    -1,    57,
      -1,    59,    -1,    -1,    -1,    -1,    -1,    65,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
       6,     7,    -1,    -1,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    51,    52,    53,    54,     6,
       7,    -1,    -1,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    -1,    -1,     6,     7,
      -1,    -1,    -1,    50,    51,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,     6,     7,    -1,    -1,    -1,
      -1,    -1,    50,    51,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,     6,     7,    -1,    -1,    -1,    -1,    -1,    50,
      51,    15,    16,    -1,    -1,    -1,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,     6,
       7,    -1,    -1,    -1,    -1,    -1,    50,    51,    -1,    -1,
      -1,    -1,    -1,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,     6,     7,    -1,    -1,
      -1,    -1,    -1,    50,    51,    -1,    -1,    -1,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,     6,     7,    -1,    -1,    -1,    -1,    -1,
      50,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
       7,    -1,    -1,    -1,    -1,    -1,    -1,    50,    51,    -1,
      -1,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    -1,    47,    -1,    -1,
      -1,    -1,    -1,    50,    51,    55,    -1,    57,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    65,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    51,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    50,    51,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    51,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    51,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    50,    51,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,
      51,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    51,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    51,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    51
  };

  const signed char
  parser::yystos_[] =
  {
       0,     8,    37,    38,    45,    46,    47,    50,    51,    55,
      57,    59,    64,    65,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    89,    94,    59,    84,     8,    84,
      89,    89,    89,    89,    89,    89,    83,     9,    58,    83,
      84,    88,     9,    60,    65,    66,    76,    81,    83,    84,
      88,    91,    93,    63,    59,     0,    82,     5,    64,     4,
      47,    57,    65,    76,    90,    91,    92,     6,     7,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    50,    51,    52,    53,    54,    62,    84,    90,
      84,    56,    58,     4,     9,    58,    60,    63,    63,    60,
       4,     9,    60,    60,    62,    66,    89,    65,    86,    34,
      88,    90,    15,    65,    83,    63,    63,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,     4,    88,    58,    84,
      88,    65,    76,    93,    65,    93,    88,    91,    60,    84,
      89,    89,    88,     9,    60,    59,    58,    92,    92,     9,
      58,     4,    63,    60,    60,     4,     9,    61,    60,    91,
      58,    60,    61,    60
  };

  const signed char
  parser::yyr1_[] =
  {
       0,    78,    79,    79,    80,    81,    81,    82,    82,    83,
      83,    83,    83,    83,    84,    84,    84,    84,    84,    84,
      84,    84,    84,    84,    84,    84,    84,    84,    84,    84,
      84,    84,    84,    84,    84,    84,    85,    85,    85,    85,
      85,    86,    86,    87,    87,    87,    87,    87,    87,    87,
      87,    87,    87,    88,    88,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    90,    90,    91,    91,    91,    92,
      92,    92,    93,    93,    93,    94
  };

  const signed char
  parser::yyr2_[] =
  {
       0,     2,     0,     1,     3,     1,     2,     1,     2,     1,
       2,     3,     3,     6,     1,     1,     1,     1,     1,     3,
       4,     3,     4,     5,     6,     2,     3,     2,     3,     3,
       4,     5,     6,     3,     3,     1,     1,     4,     5,     5,
       6,     3,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     1,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     5,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     1,     1,     2,     3,     3,     1,
       3,     3,     1,     3,     3,     3
  };


#if YYDEBUG || 1
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a YYNTOKENS, nonterminals.
  const char*
  const parser::yytname_[] =
  {
  "\"end of file\"", "error", "\"invalid token\"", "\"..\"", "\"...\"",
  "\"=>\"", "\":=\"", "\"::=\"", "\"@\"", "\",\"", "\"^=\"", "\"|=\"",
  "\"&=\"", "\"<<=\"", "\">>=\"", "\"+=\"", "\"-=\"", "\"%=\"", "\"/=\"",
  "\"*=\"", "\"=\"", "\"@=\"", "\"?\"", "\"||\"", "\"^^\"", "\"&&\"",
  "\"|\"", "\"^\"", "\"&\"", "\"!=\"", "\"==\"", "\">=\"", "\">\"",
  "\"<=\"", "\"<\"", "\">>\"", "\"<<\"", "\"+\"", "\"-\"", "\"*\"",
  "\"/\"", "\"%\"", "\"**\"", "UPLUS", "UMINUS", "\"~\"", "\"!\"", "\".\"",
  "RPLUSPLUS", "RMINUSMINUS", "\"++\"", "\"--\"", "\"^^=\"", "\"&&=\"",
  "\"||=\"", "\"(\"", "\")\"", "\"[\"", "\"]\"", "\"{\"", "\"}\"",
  "\">}\"", "\":\"", "\"::\"", "\";\"", "\"identifier\"", "\"selector\"",
  "\"ulong\"", "\"long\"", "\"uint\"", "\"int\"", "\"short\"", "\"char\"",
  "\"bool\"", "\"float\"", "\"double\"", "\"string\"",
  "\"string with attributes\"", "$accept", "pgm", "block", "stlist", "stm",
  "unit", "item", "itemi", "itemac", "lit", "cexpr", "expr", "recbl",
  "jsond", "ssel", "sel", "dmexpr", YY_NULLPTR
  };
#endif


#if YYDEBUG
  const short
  parser::yyrline_[] =
  {
       0,   153,   153,   154,   156,   158,   159,   162,   163,   170,
     171,   173,   175,   177,   182,   183,   184,   185,   186,   187,
     188,   190,   191,   193,   195,   197,   198,   199,   200,   201,
     202,   204,   206,   208,   209,   213,   215,   217,   220,   223,
     226,   230,   233,   236,   237,   238,   239,   240,   241,   242,
     243,   244,   245,   247,   248,   297,   298,   301,   304,   307,
     310,   313,   316,   319,   322,   325,   328,   331,   334,   337,
     340,   343,   346,   349,   354,   358,   361,   364,   367,   370,
     373,   376,   379,   382,   385,   388,   391,   394,   397,   400,
     403,   406,   409,   412,   415,   418,   421,   424,   427,   430,
     433,   436,   439,   442,   444,   445,   447,   448,   451,   456,
     457,   458,   461,   462,   463,   466
  };

  void
  parser::yy_stack_print_ () const
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << int (i->state);
    *yycdebug_ << '\n';
  }

  void
  parser::yy_reduce_print_ (int yyrule) const
  {
    int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):\n";
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG


} // yy
#line 2585 "src/iparser.cpp"

#line 474 "src/bison/i.y"


void yy::parser::error(const location_type& l, const std::string& msg) {
  if (parseServices.testingMode()) return;
  std::cerr << l << ": " << msg << '\n';
}
