#include "object.hpp"
#include "ortmgr.hpp"
#include "mframe.hpp"
#include "mcaches.hpp"

using std::string;

#define _REF(var) (var)->objectRef

//============================================================================//
//::::::::::::::::::::::::::::::   MCallFrame   :::::::::::::::::::::::::::::://
//============================================================================//

void MCallFrame::destroy(Session* m) {
  for (size_t n : argsToRestore)
    args[n]->dispose();
  if (restoreRec)
    receiver->dispose();
  for (auto& item : extraLocals)
    if (item.get() != nullptr && item.get()->disposable()) {
      m->deleteObject(_REF(item.get()));
      if (m->exceptionPending()) break;
    }
  delete this;
}

bool MCallFrame::isArg(OVar* p) const noexcept {
  return std::find(args.begin(), args.end(), p) != args.end();
}

bool MCallFrame::isExtraLocal(OVar* p) const noexcept {
  for (const auto& item : extraLocals)
    if (item.get() == p) return true;
  return false;
}

string MCallFrame::getMethodName() const noexcept {
  const MCallFrame* p = this;
  while (p && p->isInlineBlock())
    p = p->prev;
  return p ? SELECTOR_CACHE(p->selector)->name : "main";
}

string MCallFrame::getFullMethodName() const noexcept {
  const MCallFrame* p = this;
  while (p && p->isInlineBlock())
    p = p->prev;
  if (!p) return "main";
  const auto rec = _REF(p->receiver);
  const string prefix = rec->getClassId() == CLSID_RTMGR ?
      "system::" : rec->getClassName() + "::";
  return prefix + SELECTOR_CACHE(p->selector)->name;
}

const string& MCallFrame::methodName() const noexcept {
  static const string DEFAULT{"main"};
  const MCallFrame* p = this;
  while (p && p->isInlineBlock())
    p = p->prev;
  return p ? SELECTOR_CACHE(p->selector)->name : DEFAULT;
}

//:::::::::::::::::::::::::::::   Push Methods   ::::::::::::::::::::::::::::://

void MCallFrame::pushUndisposed(OVar* var, bool asArg) noexcept {
  if (asArg) {
    auto n = args.size();
    args.push_back(var);
    if (var->disposable())
      argsToRestore.push_back(n);
  }
  else {
    receiver = var;
    if (var->disposable())
      restoreRec = true;
  }
  if (var->disposable())
    var->undispose();
  else if (var->movable())
    var->unmove();
}

//::::::::::::::::::::::::::::   Search Methods   :::::::::::::::::::::::::::://

MCallFrame* MCallFrame::searchParentFramesForLocal(OVar* var) const noexcept {
  for (MCallFrame* p = prev; p; p = p->prev)
    if (!p->trivial() && p->isLocal(var))
      return p;
  return nullptr;
}

MCallFrame* MCallFrame::searchParentFramesForArg(OVar* var) const noexcept {
  for (MCallFrame* p = prev; p; p = p->prev)
    if (!p->trivial() && p->isArg(var))
      return p;
  return nullptr;
}
