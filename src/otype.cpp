// #include <iostream>
#include <cmath>
#include "odev.h"
#include "otypes.h"
#include "otype.hpp"
// #include "oclsmgr.hpp"
#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "ostring.hpp"
#include "odefs.h"
#include "otypeProxy.hpp"
#include "selectors.h"
#include "fswitch.h"

std::string ul2hexstr(ulong); // implemented in oclass.cpp
bool g_installTypeClasses(Session*);

#define _REF(arg) (arg)->objectRef

using std::string;

#if !defined(__clang__)
_TEMPLATE_DEFINITION(oxType, OInstance);
#endif // !defined(__clang__)

//:::::::::::::::::::::::::::  Reverse Operations  ::::::::::::::::::::::::::://

#define DEFINE_ROP(OP_NAME)                       \
struct ROP_ ## OP_NAME {                          \
  static inline const std::string name() noexcept \
  { return S_r ## OP_NAME; }                      \
};

DEFINE_ROP(incBy)
DEFINE_ROP(decBy)
DEFINE_ROP(mulBy)
DEFINE_ROP(divBy)
DEFINE_ROP(modBy)
DEFINE_ROP(expBy)
DEFINE_ROP(bitandBy)
DEFINE_ROP(bitorBy)
DEFINE_ROP(bitxorBy)
DEFINE_ROP(shiftrBy)
DEFINE_ROP(shiftlBy)
DEFINE_ROP(add)
DEFINE_ROP(sub)
DEFINE_ROP(mul)
DEFINE_ROP(div)
DEFINE_ROP(mod)
DEFINE_ROP(exp)
DEFINE_ROP(bitand)
DEFINE_ROP(bitor)
DEFINE_ROP(bitxor)
DEFINE_ROP(shiftr)
DEFINE_ROP(shiftl)
DEFINE_ROP(eq)
DEFINE_ROP(gt)
DEFINE_ROP(lt)
DEFINE_ROP(ge)
DEFINE_ROP(le)
DEFINE_ROP(ne)
DEFINE_ROP(_and)
DEFINE_ROP(_or)
DEFINE_ROP(_xor)

//:::::::::::::::::::::::::::   Cast Operations   :::::::::::::::::::::::::::://

#define DEFINE_CASTOP(OP_NAME)                    \
struct CASTOP_ ## OP_NAME {                       \
  typedef OP_NAME type;                           \
  static inline const std::string name() noexcept \
  { return S_ ## OP_NAME; }                       \
};

DEFINE_CASTOP(ulong)
DEFINE_CASTOP(long)
DEFINE_CASTOP(uint)
DEFINE_CASTOP(int)
DEFINE_CASTOP(short)
DEFINE_CASTOP(char)
DEFINE_CASTOP(double)
DEFINE_CASTOP(float)
DEFINE_CASTOP(bool)

//:::::::::::::::::::::::::::::::  Utilities  :::::::::::::::::::::::::::::::://

template<typename t>
requires (std::is_arithmetic_v<t> ||
          std::is_same_v<t, bool> || std::is_pointer_v<t>)
inline static ORef _createWithValue(const t value, Session* m) {
  ORef r = OBasicTypeInfo::getTypeClass<t>()->createObject(m);
  if (m->exceptionPending()) return nullptr;
  _P(t, r)->setValue(value);
  return r;
}

template<typename t>
inline static ORef _createWithValue(const t value, Session* m) {
  m->throwRTE(__PRETTY_FUNCTION__, "unavailable operation", __FILE__, __LINE__);
  return nullptr;
}

template<typename OP, typename t, typename T>
requires (!ops::IsCopy<OP>)
inline static ORet _applyOperation(oxType<t>& o, T rhs, Session* m) {
  t lhs = o.getValue();
  auto r = ops::Operation<OP>::apply(lhs, rhs);
  if constexpr_ (ops::uop<decltype(r)>())
    return OVar::undefinedResult();
  ORef result = _createWithValue(r, m);
  if (m->exceptionPending() || !result)
    return OVar::undefinedResult();
  return result->disposableResult();
}

template<typename OP, typename t, typename T>
requires ops::IsCopy<OP>
inline static ORet _applyOperation(oxType<t>& o, T rhs, Session* m) {
  t& lhs = o.refValue();
  const auto result = ops::Operation<OP>::copy(lhs, rhs);
  if constexpr_ (ops::uop<decltype(result)>())
    return OVar::undefinedResult();
  return o.makeResult();
}

template<int _TID>
  inline static ORef _createObject(Session* m)
  { return BTI::getBuiltinType<_TID>()->createObject(m); }

//============================================================================//
//:::::::::::::::::::::::::  C-Type Encapsulations  :::::::::::::::::::::::::://
//============================================================================//

template<typename t>
string CType<t>::toString() const
{ return std::to_string(value); }
template<> string CType<double>::toString() const {
  char buf[512];
  sprintf(buf, "%g", value);
  return string{buf};
}
template<> string CType<float>::toString() const {
  char buf[512];
  sprintf(buf, "%g", value);
  return string{buf};
}
template<> string CType<char>::toString() const { return string(1, value); }
template<> string CType<bool>::toString() const
{ return value ? "true" : "false"; }
template<> string CType<void*>::toString() const
{ return ul2hexstr((ulong)value); }

template<typename t>
  template<typename OP, typename T>
  requires IsThrowing<OP>
  ORet oxType<t>::getResult(T rhs, Session* m) {
    try {
      return _applyOperation<OP,t,T>(*this, rhs, m);
    }
    catch (const string& msg) {
      m->throwRTE(getMethodName<OP>(), "division by zero");
    }
    return undefinedResult();
  }

template<typename t>
  template<typename OP, typename T>
  requires (!IsThrowing<OP>)
  ORet oxType<t>::getResult(T rhs, Session* m) {
    return _applyOperation<OP,t,T>(*this, rhs, m);
  }

template<typename t>
  template<typename OP>
  ORet oxType<t>::execOperation(ORef o, Session* m) {
    SWITCH_TABLE_CHK(0, CLSID_ULONG, CLSID_POINTER)
      DEFINE_CASE(0, CLSID_ULONG)
      DEFINE_CASE(0, CLSID_LONG)
      DEFINE_CASE(0, CLSID_UINT)
      DEFINE_CASE(0, CLSID_INT)
      DEFINE_CASE(0, CLSID_SHORT)
      DEFINE_CASE(0, CLSID_CHAR)
      DEFINE_CASE(0, CLSID_DOUBLE)
      DEFINE_CASE(0, CLSID_FLOAT)
      DEFINE_CASE(0, CLSID_BOOL)
      DEFINE_CASE(0, CLSID_POINTER)
    END_SWITCH_TABLE(0)

    SWITCH_CHK(0, o->getClassId())
      CASE(0, CLSID_ULONG):   return getResult<OP>(_P(ulong,  o)->getValue(), m);
      CASE(0, CLSID_LONG):    return getResult<OP>(_P(long,   o)->getValue(), m);
      CASE(0, CLSID_UINT):    return getResult<OP>(_P(uint,   o)->getValue(), m);
      CASE(0, CLSID_INT):     return getResult<OP>(_P(int,    o)->getValue(), m);
      CASE(0, CLSID_SHORT):   return getResult<OP>(_P(short,  o)->getValue(), m);
      CASE(0, CLSID_CHAR):    return getResult<OP>(_P(char,   o)->getValue(), m);
      CASE(0, CLSID_DOUBLE):  return getResult<OP>(_P(double, o)->getValue(), m);
      CASE(0, CLSID_FLOAT):   return getResult<OP>(_P(float,  o)->getValue(), m);
      CASE(0, CLSID_BOOL):    return getResult<OP>(_P(bool,   o)->getValue(), m);
      CASE(0, CLSID_POINTER): return getResult<OP>(_P(void*,  o)->getValue(), m);
      DEFAULT(0):
    END_SWITCH(0)

    return OVar::undefinedResult();
  }

template<typename t>
  ORet oxType<t>::assignValue(OArg* argv, Session* m) {
    ORet result = execOperation<ops::Ass>(_ARG(0), m);
    _RETHROW(getMethodName<ops::Ass>());
    if (!result.undefined()) return result;
    // special handling for type proxy objects
    if (ORef o = g_renderProxyObject(_ARG(0), m); o != _ARG(0)) {
      ObjectAutoCleaner cleanRendered{m, o};
      result = execOperation<ops::Ass>(o, m);
      _RETHROW(getMethodName<ops::Ass>());
      if (!result.undefined()) return result;
    }
    // try to convert argument to this type
    int rc;
    auto converted = m->valueCast(BTI::getTypeClass<t>(), _ARG(0), &rc);
    ObjectAutoCleaner clean{m, converted};
    _RETHROW(getMethodName<ops::Ass>());
    if (rc != EMR_OK) {
      const string msg = getMethodName<ops::Ass>()+" does not "
          "accept operands type '"+_ARG(0)->getClassName()+"'";
      m->throwRTE(getMethodName<ops::Ass>(), msg);
      return OVar::undefinedResult();
    }
    // try to apply operation with converted operand
    result = execOperation<ops::Ass>(REF(converted), m);
    _RETHROW(getMethodName<ops::Ass>());
    if (!result.undefined()) return result;
    // internal error - it should have worked
    m->throwRTE(getMethodName<ops::Ass>(), "internal error", __FILE__, __LINE__);
    return OVar::undefinedResult();
  }

//============================================================================//
//::::::::::::::::::::::::::   Generic Operations   :::::::::::::::::::::::::://
//============================================================================//

template<typename t>
  static constexpr size_t _maxSize() { return sizeof(t); }
template<typename t, typename s, typename... v>
  static constexpr size_t _maxSize()
  { return std::max(sizeof(t), _maxSize<s, v...>()); }
static constexpr size_t _bufferSize() {
  return _maxSize<oxType<ulong>,    oxType<long>,
                  oxType<unsigned>, oxType<int>,
                  oxType<short>,    oxType<char>,
                  oxType<bool>,     oxType<void*>,
                  oxType<double>,   oxType<float>>();
}

struct BasicTypeVariant {
  char buf_[_bufferSize()];

  template<typename t>
    t* pointer() {return reinterpret_cast<t*>(buf_);}
  template<typename t>
    t& reference() {return *pointer<t>();}
  template<typename t>
    const t& reference() const {return *pointer<t>();}

  template<typename t>
    void initBasicType() {new(buf_) oxType<t>{BTI::getTypeClass<t>()};}
  template<typename t>
    void setValue(const t& value) {reference<t>() = value;}
  template<typename t>
    t& getValue() {return reference<t>();}
  template<typename t>
    const t& getValue() const {return reference<t>();}

  template<typename t>
    ORef castType(ORef thisObject) {
      initBasicType<t>();
      setValue(*_P(t, thisObject));
      BTI::getTypeClass<t>()->reinterpretCast(pointer<oxType<t>>());
      return pointer<oxType<t>>();
    }
};

static ORef _castBasicTypeObject(OArg* argv, BasicTypeVariant& v) {
  SWITCH_TABLE_CHK(0, CLSID_ULONG, CLSID_POINTER)
    DEFINE_CASE(0, CLSID_ULONG)
    DEFINE_CASE(0, CLSID_LONG)
    DEFINE_CASE(0, CLSID_UINT)
    DEFINE_CASE(0, CLSID_INT)
    DEFINE_CASE(0, CLSID_SHORT)
    DEFINE_CASE(0, CLSID_CHAR)
    DEFINE_CASE(0, CLSID_DOUBLE)
    DEFINE_CASE(0, CLSID_FLOAT)
    DEFINE_CASE(0, CLSID_BOOL)
    DEFINE_CASE(0, CLSID_POINTER)
  END_SWITCH_TABLE(0)

  SWITCH_CHK(0, _ARG(0)->getRootClassId())
    CASE(0, CLSID_ULONG  ): return v.castType<ulong >(_ARG(0));
    CASE(0, CLSID_LONG   ): return v.castType<long  >(_ARG(0));
    CASE(0, CLSID_UINT   ): return v.castType<uint  >(_ARG(0));
    CASE(0, CLSID_INT    ): return v.castType<int   >(_ARG(0));
    CASE(0, CLSID_SHORT  ): return v.castType<short >(_ARG(0));
    CASE(0, CLSID_CHAR   ): return v.castType<char  >(_ARG(0));
    CASE(0, CLSID_DOUBLE ): return v.castType<double>(_ARG(0));
    CASE(0, CLSID_FLOAT  ): return v.castType<float >(_ARG(0));
    CASE(0, CLSID_BOOL   ): return v.castType<bool  >(_ARG(0));
    CASE(0, CLSID_POINTER): return v.castType<void* >(_ARG(0));
    DEFAULT(0):
  END_SWITCH(0)

  return nullptr;
}

static bool _hasScriptMethod(ORef obj, const string& methodName) {
  OClass* p;
  OAddr a;
  return obj->getClass()->getScriptMethodAddress(methodName, &a, &p) != -1;
}

template<class t>
  template<typename _OP, typename _ROP>
  ORet oxType<t>::applyOperation(OArg* argv, Session* m) {
    if (OBasicTypeInfo::extendsBasicType(_ARG(0))) {
      if (_hasScriptMethod(_ARG(0), _ROP::name()))
        return OVar::undefinedResult();
      BasicTypeVariant b;
      ORef o = _castBasicTypeObject(argv, b);
      return execOperation<_OP>(o, m);
    }
    return execOperation<_OP>(_ARG(0), m);
  }

template<typename t>
  template<typename _OP, typename _ROP>
  ORet oxType<t>::genericBinaryOperation(OArg* argv, Session* m) {
    ORet result = applyOperation<_OP, _ROP>(argv, m);
    _RETHROW(getMethodName<_OP>());
    if (!result.undefined()) return result;
    // that should have worked if the operation is permissible for basic types
    // special treatment for non-basic types
    ObjectAutoCleaner exceptionCleaner{ m };
    if (!OBasicTypeInfo::isBasicType(_ARG(0))) {
      // try to apply reverse operation
      OVar varThis{this};
      OArg args[]{&varThis};
      auto r = m->execMethodNoThrowUntilExec(_ROP::name(), argv[0], 1, args);
      if (m->isExceptionBadArg()) {
        exceptionCleaner.add(m->popException());
      }
      else {
        _RETHROW(getMethodName<_OP>());
        if (r.second == EMR_OK) return r.first;
        // _ARG(0) does not respond or method is not accessible
        // try to convert argument to this type
        int rc = -1;
        ORet converted;
        const auto rtid = _REF(argv[0])->getRootClassId();
        if (!std::is_same_v<t, bool> || (rtid != CLSID_PTR &&
                                         rtid != CLSID_PTRVECELEM)) {
          converted = m->valueCast(BTI::getTypeClass<t>(), _ARG(0), &rc);
          _RETHROW(getMethodName<_OP>());
        }
        if (rc == EMR_OK) {
          // try to apply operation with converted operand
          result = execOperation<_OP>(REF(converted), m);
          if (converted.disposable())
            REF(converted)->deleteObject(m);
          _RETHROW(getMethodName<_OP>());
          if (!result.undefined()) return result;
          // internal error - it should have worked
          m->throwRTE(getMethodName<_OP>(), "internal error", __FILE__,
              __LINE__);
          return OVar::undefinedResult();
        }
      }
    }
    const string msg = getMethodName<_OP>()+" does not accept "
                       "operands type '"+_ARG(0)->getClassName()+"'";
    m->throwBadArg(getMethodName<_OP>(), msg);
    return OVar::undefinedResult();
  }

template<typename t>
  template<typename _OP, typename _ROP>
  ORet oxType<t>::genericBinaryOperationThrow(OArg* argv, Session* m) {
  string errorMessage;
  try { return genericBinaryOperation<_OP, _ROP>(argv, m); }
  catch (const string& msg) { errorMessage = msg; }
  catch (...) { errorMessage = "unknown error during operation"; }
  m->throwRTE(getMethodName<_OP>(), errorMessage);
  return OVar::undefinedResult();
}

template<typename t>
  template<typename _OP, typename _ROP>
  ORet oxType<t>::genericBinaryOperationNoCast(OArg* argv, Session* m) {
    ORet result = applyOperation<_OP, _ROP>(argv, m);
    _RETHROW(getMethodName<_OP>());
    if (!result.undefined()) return result;
    // try to apply operation after exchanging operands
    if (!OBasicTypeInfo::isBasicType(_ARG(0))) {
      OVar varThis{this};
      OArg args[]{&varThis};
      auto r = m->execMethodNoThrowUntilExec(_ROP::name(), argv[0], 1, args);
      _RETHROW(getMethodName<_OP>());
      if (r.second == EMR_OK) return r.first;
      // _ARG(0) does not respond or method is not accessible
    }
    const string msg = getMethodName<_OP>()+" does not accept "
        "operands type '"+_ARG(0)->getClassName()+"'";
    m->throwRTE(getMethodName<_OP>(), msg);
    return OVar::undefinedResult();
  }

static ORet _operationError(const string& className, const string& cf,
                            const string& f, int line, Session* m) {
  const string msg = "not applicable to type '"+className+"'";
  m->throwBadUsage(cf, msg, f, __FILE__, line);
  return OVar::undefinedResult();
}

//============================================================================//
//::::::::::::::::::::::::::   Binary Operations   ::::::::::::::::::::::::::://
//============================================================================//

//:::::::::::::::  O.7 operator+, "add:" -- ALL except: bool  :::::::::::::::://
template<class t>
ORet oxType<t>::add(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Add, ROP_add>(argv, m);
}

template<>
ORet oxType<char>::add(OArg, int, OArg* argv, Session* m) {
  if (_ARG_TYPEID(0, CLSID_STRING)) {
    ORef r = _createObject<CLSID_STRING>(m);
    _RETHROW(getMethodName<ops::Add>());
    _REF_DECL(String, rval, r);
    rval = getValue() + _VALUE(String, _ARG(0));
    return r->disposableResult();
  }
  return genericBinaryOperation<ops::Add, ROP_add>(argv, m);
}

//::::::::::::::::  O.8 operator- "sub:" -- ALL except: bool  :::::::::::::::://
template<class t>
ORet oxType<t>::sub(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Sub, ROP_sub>(argv, m);
}

//::::::::::::  O.9 operator* "mul:" -- ALL except: bool, void*  ::::::::::::://
template<class t>
ORet oxType<t>::mul(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Mul, ROP_mul>(argv, m);
}

//::::::::::::  O.10 operator/ "div:" -- ALL except: bool, void*  :::::::::::://
template<class t>
ORet oxType<t>::div(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationThrow<ops::Div, ROP_div>(argv, m);
}

//::::::::::::  O.11 operator% "mod:" -- ALL except: bool, void*  :::::::::::://
template<class t>
ORet oxType<t>::mod(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationThrow<ops::Mod, ROP_mod>(argv, m);
}

//:::::::::::::::  O.12 exp "exp:" -- ALL except: bool, void*  ::::::::::::::://
template<class t>
ORet oxType<t>::exp(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Exp, ROP_exp>(argv, m);
}

//============================================================================//
//::::::::::::::::::::::::::::   Bit Operators   ::::::::::::::::::::::::::::://
//============================================================================//

//::::::::::::::::::::::::   O.18 operator& bitand:   :::::::::::::::::::::::://
template<class t>
ORet oxType<t>::band(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationNoCast<ops::Band, ROP_bitand>(argv, m);
}

//::::::::::::::::::::::::   O.19 operator| bitor:   ::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::bor(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationNoCast<ops::Bor, ROP_bitor>(argv, m);
}

//::::::::::::::::::::::::   O.20 operator^ bitxor:   :::::::::::::::::::::::://
template<class t>
ORet oxType<t>::bxor(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationNoCast<ops::Bxor, ROP_bitxor>(argv, m);
}

//:::::::::::::::::::::::   O.21 operator>> shiftr:   :::::::::::::::::::::::://
template<class t>
ORet oxType<t>::shr(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationNoCast<ops::Shr, ROP_shiftr>(argv, m);
}

//:::::::::::::::::::::::   O.22 operator<< shiftl:   :::::::::::::::::::::::://
template<class t>
ORet oxType<t>::shl(OArg, int, OArg* argv, Session* m) {
  return genericBinaryOperationNoCast<ops::Shl, ROP_shiftl>(argv, m);
}

//============================================================================//
//:::::::::::::::::::::::::::   Copy Operations   :::::::::::::::::::::::::::://
//============================================================================//

//::::::::::::::  C.1 operator+=, "incBy:" -- ALL except: bool  :::::::::::::://
template<typename t>
ORet oxType<t>::incBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_incBy)
  return genericBinaryOperation<ops::Incby, ROP_incBy>(argv, m);
}

//::::::::::::::  C.2 operator-=, "decBy:" -- ALL except: bool  :::::::::::::://
template<typename t>
ORet oxType<t>::decBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_decBy)
  return genericBinaryOperation<ops::Decby, ROP_decBy>(argv, m);
}

//::::::::::  C.3 operator*=, "mulBy:" -- ALL except: bool, void*  ::::::::::://
template<typename t>
ORet oxType<t>::mulBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_mulBy)
  return genericBinaryOperation<ops::Mulby, ROP_mulBy>(argv, m);
}

//::::::::::  C.4 operator/=, "divBy:" -- ALL except: bool, void*  ::::::::::://
template<class t>
ORet oxType<t>::divBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_divBy)
  return genericBinaryOperation<ops::Divby, ROP_divBy>(argv, m);
}

//::::::::::  C.5 operator%=, "modBy:" -- ALL except: bool, void*  ::::::::::://
template<class t>
ORet oxType<t>::modBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_modBy)
  return genericBinaryOperation<ops::Modby, ROP_modBy>(argv, m);
}

//:::::::::::::::  C.6 "expBy:" -- ALL except: bool, void*  :::::::::::::::::://
template<class t>
ORet oxType<t>::expBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_expBy)
  return genericBinaryOperation<ops::Expby, ROP_expBy>(argv, m);
}

template<>
ORet oxType<bool>::expBy(OArg rec, int, OArg*, Session*) {
  return OVar::undefinedResult();
}

//:::::::::  C.13 operator&=, "bitandBy:" -- NONE except: unsigned  :::::::::://
template<class t>
ORet oxType<t>::bandw(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_bitandBy)
  return genericBinaryOperationNoCast<ops::Bandby, ROP_bitandBy>(argv, m);
}

//::::::::::  C.14 operator|=, "bitorBy:" -- NONE except: unsigned  :::::::::://
template<class t>
ORet oxType<t>::borw(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_bitorBy)
  return genericBinaryOperationNoCast<ops::Borby, ROP_bitorBy>(argv, m);
}

//:::::::::  C.15 operator^=, "bitxorBy:" -- NONE except: unsigned  :::::::::://
template<class t>
ORet oxType<t>::bxorw(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_bitxorBy)
  return genericBinaryOperationNoCast<ops::Bxorby, ROP_bitxorBy>(argv, m);
}

//:::::::::  C.16 operator>>=, "shiftrBy:" -- NONE except: unsigned  ::::::::://
template<class t>
ORet oxType<t>::shrw(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_shiftrBy)
  return genericBinaryOperationNoCast<ops::Shrby, ROP_shiftrBy>(argv, m);
}

//:::::::::  C.17 operator<<=, "shiftlBy:" -- NONE except: unsigned  ::::::::://
template<class t>
ORet oxType<t>::shlw(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_shiftlBy)
  return genericBinaryOperationNoCast<ops::Shlby, ROP_shiftlBy>(argv, m);
}

//============================================================================//
//:::::::::::::::::::::::::   Comparison Operators   ::::::::::::::::::::::::://
//============================================================================//

template<class t>
ORet oxType<t>::eq(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Eq, ROP_eq>(argv, m);
}

template<class t>
ORet oxType<t>::gt(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Gt, ROP_lt>(argv, m);
}

template<class t>
ORet oxType<t>::lt(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Lt, ROP_gt>(argv, m);
}

template<class t>
ORet oxType<t>::ge(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Ge, ROP_le>(argv, m);
}

template<class t>
ORet oxType<t>::le(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Le, ROP_ge>(argv, m);
}

template<class t>
ORet oxType<t>::ne(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Ne, ROP_ne>(argv, m);
}

template<class t>
  template<typename _OP, typename _ROP>
  ORet oxType<t>::genericPointerCmpOperation(OArg* argv, Session* m) {
    return undefinedResult();
  }

template<>
  template<typename _OP, typename _ROP>
  ORet oxType<void*>::genericPointerCmpOperation(OArg* argv, Session* m) {
    ORet result = applyOperation<_OP, _ROP>(argv, m);
    _RETHROW(getMethodName<_OP>());
    if (!result.undefined()) return result;
    // try to apply operation after exchanging operands
    if (!OBasicTypeInfo::isBasicType(_ARG(0))) {
      OVar varThis{this};
      OArg args[]{&varThis};
      auto r = m->execMethodNoThrowUntilExec(_ROP::name(), argv[0], 1, args);
      _RETHROW(getMethodName<_OP>());
      if (r.second == EMR_OK) return r.first;
    }
    // error
    const string msg = "incorrect operation on operands "
        "type pointer, '"+_ARG(0)->getClassName()+"'";
    m->throwRTE(getMethodName<_OP>(), msg);
    return undefinedResult();
  }

template<> ORet oxType<void*>::eq(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Eq, ROP_eq>(argv, m);
}

template<> ORet oxType<void*>::gt(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Gt, ROP_lt>(argv, m);
}

template<> ORet oxType<void*>::lt(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Lt, ROP_gt>(argv, m);
}

template<> ORet oxType<void*>::ge(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Ge, ROP_le>(argv, m);
}

template<> ORet oxType<void*>::le(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Le, ROP_ge>(argv, m);
}

template<> ORet oxType<void*>::ne(OArg, int argc, OArg* argv, Session* m) {
  return genericPointerCmpOperation<ops::Ne, ROP_ne>(argv, m);
}

//============================================================================//
//:::::::::::::::::  Logical Operators and or not (negate)  :::::::::::::::::://
//============================================================================//

template<class t>
ORet oxType<t>::_and(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::And, ROP__and>(argv, m);
}

template<class t>
ORet oxType<t>::_or(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Or, ROP__or>(argv, m);
}

template<class t>
ORet oxType<t>::_xor(OArg, int argc, OArg* argv, Session* m) {
  return genericBinaryOperation<ops::Xor, ROP__xor>(argv, m);
}

//============================================================================//
//:::::::::::::::::::::::::::   Unary Operators   :::::::::::::::::::::::::::://
//============================================================================//

//::::::::::::::::::::::::::   U.1. Reverse sign   ::::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::neg(OArg, int, OArg*, Session* m) {
  auto r = createWithValue(-this->getValue(), m);
  _RETHROW(getClassName()+"::operator-(pre)");
  return r->disposableResult();
}

template<> ORet oxType<bool>::neg(OArg, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "neg", __LINE__, m);
}
template<> ORet oxType<void*>::neg(OArg, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "neg", __LINE__, m);
}

//::::::::::::::::::::::::::::   U.2. Increment   :::::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::inc(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_inc);
  this->incValue();
  return this->makeResult();
}

template<> ORet oxType<bool>::inc(OArg rec, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "inc", __LINE__, m);
}

//::::::::::::::::::::::::::::   U.3. Decrement   :::::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::dec(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_dec);
  this->decValue();
  return this->makeResult();
}

template<> ORet oxType<bool>::dec(OArg rec, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "dec", __LINE__, m);
}

//:::::::::::::::::::::::::   U.4. Post-increment   :::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::pinc(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_postinc);
  ORef r = createWithValue(this->getValue(), m);
  _RETHROW(getClassName()+"::operator++(post)");
  this->incValue();
  return r->disposableResult();
}

template<> ORet oxType<bool>::pinc(OArg rec, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "postinc", __LINE__, m);
}

//:::::::::::::::::::::::::   U.5. Post-decrement   :::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::pdec(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_postdec);
  ORef r = createWithValue(this->getValue(), m);
  _RETHROW(getClassName()+"::operator--(post)");
  this->decValue();
  return r->disposableResult();
}

template<> ORet oxType<bool>::pdec(OArg rec, int, OArg*, Session* m) {
  return _operationError(getClassName(), __PRETTY_FUNCTION__, "postdec", __LINE__, m);
}

//::::::::::::::::::::::::::   U.6. Bit inversion   :::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::bitinv(OArg rec, int, OArg*, Session* m)
{ return createWithValue(~this->getValue(), m)->disposableResult(); }

// They are not used. They are here just for the compiler.
template<> ORet oxType<bool>::bitinv(OArg, int, OArg*, Session* m) {return undefinedResult();}
template<> ORet oxType<void*>::bitinv(OArg, int, OArg*, Session* m) {return undefinedResult();}
template<> ORet oxType<double>::bitinv(OArg, int, OArg*, Session* m) {return undefinedResult();}
template<> ORet oxType<float>::bitinv(OArg, int, OArg*, Session* m) {return undefinedResult();}

//:::::::::::::::::::::::::::   U.7. Logical not   ::::::::::::::::::::::::::://
template<class t>
ORet oxType<t>::_not(OArg rec, int, OArg*, Session* m) {
  auto r = _createWithValue(!this->getValue(), m);
  return r ? r->disposableResult() : undefinedResult();
}

//============================================================================//
//::::::::::::::::::::::::::::::::   value:   :::::::::::::::::::::::::::::::://
//============================================================================//

template<class t>
ORet oxType<t>::ommValue(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(BTI::getTypeClass<t>()->name+"::" S_value)
  ORet result = this->assignValue(argv, m);
  _RETHROW(getMethodName<ops::Ass>());
  return result;
}

//============================================================================//
//:::::::::::::::::::::  Cast operators -- imitate C !  :::::::::::::::::::::://
//============================================================================//

template<typename t>
  template<typename _OP>
  ORet oxType<t>::genericCastOperation(Session* m) {
    auto r = _createWithValue(static_cast<typename _OP::type>(this->getValue()), m);
    _RETHROW(getClassName()+"::" + _OP::name());
    return r->disposableResult();
  }

template<>
  template<typename _OP>
  ORet oxType<void*>::genericCastOperation(Session* m) {
    const string msg = "cast operator is not applicable to pointer type";
    m->throwBadUsage(__PRETTY_FUNCTION__, msg, "pointer::"+_OP::name());
    return undefinedResult();
  }

template<typename t>
ORet oxType<t>::cast_ulong(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_ulong>(m);
}

template<typename t>
ORet oxType<t>::cast_long(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_long>(m);
}

template<typename t>
ORet oxType<t>::cast_uint(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_uint>(m);
}

template<typename t>
ORet oxType<t>::cast_int(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_int>(m);
}

template<typename t>
ORet oxType<t>::cast_short(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_short>(m);
}

template<typename t>
ORet oxType<t>::cast_char(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_char>(m);
}

template<typename t>
ORet oxType<t>::cast_double(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_double>(m);
}

template<typename t>
ORet oxType<t>::cast_float(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_float>(m);
}

template<typename t>
ORet oxType<t>::cast_bool(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_bool>(m);
}

template<class t>
ORet oxType<t>::cast_pointer(OArg, int, OArg*, Session*) {return undefinedResult();}

template<> ORet oxType<void*>::cast_ulong(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_ulong>(m);
}

template<> ORet oxType<void*>::cast_long(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_long>(m);
}

template<> ORet oxType<void*>::cast_uint(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_uint>(m);
}

template<> ORet oxType<void*>::cast_int(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_int>(m);
}

template<> ORet oxType<void*>::cast_short(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_short>(m);
}

template<> ORet oxType<void*>::cast_char(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_char>(m);
}

template<> ORet oxType<void*>::cast_double(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_double>(m);
}

template<> ORet oxType<void*>::cast_float(OArg, int, OArg*, Session* m) {
  return genericCastOperation<CASTOP_float>(m);
}

template<> ORet oxType<void*>::cast_bool(OArg, int, OArg*, Session* m) {
  ORef r = _createObject<CLSID_BOOL>(m);
  _RETHROW("pointer::" S_bool);
  *_PBOOL(r) = (this->getValue() != nullptr);
  return r->disposableResult();
}

template<> ORet oxType<void*>::cast_pointer(OArg, int, OArg*, Session* m) {
  ORef r = _createObject<CLSID_POINTER>(m);
  _RETHROW("pointer::" S_pointer);
  _PPTR(r)->setValue(this->getValue());
  return r->disposableResult();
}

template<class t>
ORet oxType<t>::ommToString(OArg, int argc, OArg* argv, Session* m) {
  ORef r = _createObject<CLSID_STRING>(m);
  _RETHROW(getClassName()+"::" S_string);
  *_PStr(r) = this->toString();
  return r->disposableResult();
}

//============================================================================//
//::::::::::::::::::::::::::::  Address operator  :::::::::::::::::::::::::::://
//============================================================================//

template<class t>
ORet oxType<t>::addr(OArg, int, OArg*, Session* m) {
  auto r = _createWithValue(static_cast<void*>(&this->refValue()), m);
  _RETHROW(getClassName()+"::" S_address);
  return r->disposableResult();
}

//============================================================================//
//:::::::::::::::::::::::::  Various class methods  :::::::::::::::::::::::::://
//============================================================================//

template<class t>
ORet oxType<t>::print(OArg, int, OArg*, Session* m) {
  m->stringOutput(this->toString());
  return this->makeResult();
}

template<class t>
ORet oxType<t>::println(OArg, int, OArg*, Session* m) {
  m->stringOutput(this->toString()+"\n");
  return this->makeResult();
}

#define S_pinc     S_postinc
#define S_pdec     S_postdec
#define S_bandw    S_bitandBy
#define S_borw     S_bitorBy
#define S_bxorw    S_bitxorBy
#define S_shrw     S_shiftrBy
#define S_shlw     S_shiftlBy
#define S_bandBy   S_bitandBy
#define S_borBy    S_bitorBy
#define S_bxorBy   S_bitxorBy
#define S_band     S_bitand
#define S_bor      S_bitor
#define S_bxor     S_bitxor
#define S_shr      S_shiftr
#define S_shl      S_shiftl
#define S_addr     S_address
#define S_cast_pointer S_pointer
#define tostr      ommToString

#define PUBLISH_FOR(t,m) \
  PUBLIC(oxType<t>::m, S_##m)

#define PUBLISH_CMP_METHODS_FOR(t) \
  PUBLISH_FOR(t, eq)          \
  PUBLISH_FOR(t, gt)          \
  PUBLISH_FOR(t, lt)          \
  PUBLISH_FOR(t, ge)          \
  PUBLISH_FOR(t, le)          \
  PUBLISH_FOR(t, ne)

#define PUBLISH_CAST_METHODS_FOR(t) \
  PUBLISH_FOR(t, cast_ulong)  \
  PUBLISH_FOR(t, cast_long)   \
  PUBLISH_FOR(t, cast_uint)   \
  PUBLISH_FOR(t, cast_int)    \
  PUBLISH_FOR(t, cast_short)  \
  PUBLISH_FOR(t, cast_char)   \
  PUBLISH_FOR(t, cast_double) \
  PUBLISH_FOR(t, cast_float)  \
  PUBLISH_FOR(t, cast_bool)

#define PUBLISH_INCDEC_METHODS_FOR(t) \
  PUBLISH_FOR(t, inc)         \
  PUBLISH_FOR(t, dec)         \
  PUBLISH_FOR(t, pinc)        \
  PUBLISH_FOR(t, pdec)        \
  PUBLISH_FOR(t, incBy)       \
  PUBLISH_FOR(t, decBy)

#define PUBLISH_MATH_METHODS_FOR(t) \
  PUBLISH_INCDEC_METHODS_FOR(t)     \
  PUBLISH_FOR(t, mulBy)       \
  PUBLISH_FOR(t, divBy)       \
  PUBLISH_FOR(t, modBy)       \
  PUBLISH_FOR(t, expBy)       \
  PUBLISH_FOR(t, add)         \
  PUBLISH_FOR(t, sub)         \
  PUBLISH_FOR(t, mul)         \
  PUBLISH_FOR(t, div)         \
  PUBLISH_FOR(t, mod)         \
  PUBLISH_FOR(t, exp)

#define PUBLISH_BIT_METHODS_FOR(t) \
  PUBLISH_FOR(t, bandw)       \
  PUBLISH_FOR(t, borw)        \
  PUBLISH_FOR(t, bxorw)       \
  PUBLISH_FOR(t, shrw)        \
  PUBLISH_FOR(t, shlw)        \
  PUBLISH_FOR(t, band)        \
  PUBLISH_FOR(t, bor)         \
  PUBLISH_FOR(t, bxor)        \
  PUBLISH_FOR(t, bitinv)      \
  PUBLISH_FOR(t, shr)         \
  PUBLISH_FOR(t, shl)

#define PUBLISH_BOOL_METHODS_FOR(t) \
  PUBLISH_FOR(t, _and)        \
  PUBLISH_FOR(t, _or)         \
  PUBLISH_FOR(t, _xor)        \
  PUBLISH_FOR(t, _not)

#define METHOD_TABLE_FOR_INT_TYPE(t) \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(oxType<t>) \
  PUBLIC(oxType<t>::ommValue, S_value)      \
  PUBLIC(oxType<t>::ommValue, S_init)       \
  PUBLIC(oxType<t>::ommValue, S_copy)       \
  PUBLISH_MATH_METHODS_FOR(t) \
  PUBLISH_BOOL_METHODS_FOR(t) \
  PUBLISH_BIT_METHODS_FOR(t)  \
  PUBLISH_CMP_METHODS_FOR(t)  \
  PUBLISH_CAST_METHODS_FOR(t) \
  PUBLISH_FOR(t, addr)        \
  PUBLISH_FOR(t, print)       \
  PUBLISH_FOR(t, println)     \
  PUBLISH_FOR(t, neg)         \
  PUBLISH_FOR(t, tostr)       \
_END_METHOD_TABLE()

#define METHOD_TABLE_FOR_FP_TYPE(t) \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(oxType<t>) \
  PUBLIC(oxType<t>::ommValue, S_value)      \
  PUBLIC(oxType<t>::ommValue, S_init)       \
  PUBLIC(oxType<t>::ommValue, S_copy)       \
  PUBLISH_MATH_METHODS_FOR(t) \
  PUBLISH_CMP_METHODS_FOR(t)  \
  PUBLISH_CAST_METHODS_FOR(t) \
  PUBLISH_BOOL_METHODS_FOR(t) \
  PUBLISH_FOR(t, addr)        \
  PUBLISH_FOR(t, print)       \
  PUBLISH_FOR(t, println)     \
  PUBLISH_FOR(t, neg)         \
  PUBLISH_FOR(t, tostr)       \
_END_METHOD_TABLE()

//============================================================================//
//:::::::::::::::::::  class oxType_Class implementation  :::::::::::::::::::://
//============================================================================//

template<typename t>
OClass* oxType_Class<t>::createClass(Session* m, const std::string& className)
    const {
  OClass* cls = new oxType_Class(m, this->name, className);
  m->rethrow("oxType_Class<"+BTI::getTypeClass<t>()->name+">::createClass");
  return cls;
}

template<typename t>
OClass* oxType_Class<t>::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new oxType_Class(m, this, className);
  m->rethrow("oxType_Class<"+BTI::getTypeClass<t>()->name+
             ">::createMutableClass");
  return cls;
}

METHOD_TABLE_FOR_INT_TYPE(unsigned long)
METHOD_TABLE_FOR_INT_TYPE(long)
METHOD_TABLE_FOR_INT_TYPE(unsigned)
METHOD_TABLE_FOR_INT_TYPE(int)
METHOD_TABLE_FOR_INT_TYPE(short)
METHOD_TABLE_FOR_INT_TYPE(char)

METHOD_TABLE_FOR_FP_TYPE(double)
METHOD_TABLE_FOR_FP_TYPE(float)

_BEGIN_TEMPLATE_METHOD_TABLE_FOR(oxType<bool>)
  PUBLIC(oxType<bool>::ommValue, S_value)
  PUBLIC(oxType<bool>::ommValue, S_init)
  PUBLIC(oxType<bool>::ommValue, S_copy)
  // PUBLISH_MATH_METHODS_FOR(bool)
  PUBLISH_INCDEC_METHODS_FOR(bool)
  PUBLISH_FOR(bool, add)
  PUBLISH_FOR(bool, sub)
  PUBLISH_FOR(bool, mul)
  PUBLISH_FOR(bool, div)
  PUBLISH_FOR(bool, mod)
  PUBLISH_FOR(bool, exp)
  PUBLISH_FOR(bool, mulBy)
  PUBLISH_FOR(bool, divBy)
  PUBLISH_FOR(bool, modBy)
  PUBLISH_BIT_METHODS_FOR(bool)
  PUBLISH_CMP_METHODS_FOR(bool)
  PUBLISH_BOOL_METHODS_FOR(bool)
  PUBLISH_FOR(bool, eq)
  PUBLISH_FOR(bool, ne)
  PUBLISH_CAST_METHODS_FOR(bool)
  PUBLISH_FOR(bool, addr)
  PUBLISH_FOR(bool, print)
  PUBLISH_FOR(bool, println)
  PUBLISH_FOR(bool, tostr)
_END_METHOD_TABLE()

_BEGIN_TEMPLATE_METHOD_TABLE_FOR(oxType<void*>)
  PUBLIC(oxType<void*>::ommValue, S_value)
  PUBLIC(oxType<void*>::ommValue, S_init)
  PUBLIC(oxType<void*>::ommValue, S_copy)
  PUBLISH_BOOL_METHODS_FOR(void*)
  PUBLISH_INCDEC_METHODS_FOR(void*)
  PUBLISH_CMP_METHODS_FOR(void*)
  // PUBLISH_CAST_METHODS_FOR(void*)
  // PUBLISH_FOR(void*, add)
  // PUBLISH_FOR(void*, sub)
  PUBLISH_FOR(void*, cast_bool)
  PUBLISH_FOR(void*, cast_pointer)
  PUBLISH_FOR(void*, addr)
  PUBLISH_FOR(void*, print)
  PUBLISH_FOR(void*, println)
  PUBLISH_FOR(void*, tostr)
_END_METHOD_TABLE()

bool g_installTypeClasses(Session* m) {
  OClass* cls;
  extern OClass* _OXType_class[];

  cls = new oxType_Class<ulong>(m, "ulong");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_ULONG, m);
  _OXType_class[CLSID_ULONG] = cls;

  cls = new oxType_Class<long>(m, "long");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_LONG, m);
  _OXType_class[CLSID_LONG] = cls;

  cls = new oxType_Class<uint>(m, T_uint);
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_UINT, m);
  _OXType_class[CLSID_UINT] = cls;

  cls = new oxType_Class<int>(m, "int");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_INT, m);
  _OXType_class[CLSID_INT] = cls;

  cls = new oxType_Class<short>(m, "short");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_SHORT, m);
  _OXType_class[CLSID_SHORT] = cls;

  cls = new oxType_Class<char>(m, "char");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_CHAR, m);
  _OXType_class[CLSID_CHAR] = cls;

  cls = new oxType_Class<double>(m, "double");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_DOUBLE, m);
  _OXType_class[CLSID_DOUBLE] = cls;

  cls = new oxType_Class<float>(m, "float");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_FLOAT, m);
  _OXType_class[CLSID_FLOAT] = cls;

  cls = new oxType_Class<bool>(m, "bool");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_BOOL, m);
  _OXType_class[CLSID_BOOL] = cls;

  cls = new oxType_Class<void*>(m, "pointer");
  if (!cls) return false;
  cls->finalize(m);
  _ASSERT(cls->getClassId() == CLSID_PTR, m);
  _OXType_class[CLSID_PTR] = cls;

  return true;
}

//============================================================================//
//::::::::::::::::::   class oxBlock_Class implementation   :::::::::::::::::://
//============================================================================//
_DEFINE_MCLASS(Block)
_IMPLEMENT_CLASS_INSTALLER(Block)

//============================================================================//
//::::::::::::::::::::::::   Template Instantiation   :::::::::::::::::::::::://
//============================================================================//

template class oxType<unsigned long>;
template class oxType<long>;
template class oxType<unsigned>;
template class oxType<int>;
template class oxType<short>;
template class oxType<char>;
template class oxType<double>;
template class oxType<float>;
template class oxType<bool>;
template class oxType<void*>;

template class oxType_Class<unsigned long>;
template class oxType_Class<long>;
template class oxType_Class<unsigned>;
template class oxType_Class<int>;
template class oxType_Class<short>;
template class oxType_Class<char>;
template class oxType_Class<double>;
template class oxType_Class<float>;
template class oxType_Class<bool>;
template class oxType_Class<void*>;
