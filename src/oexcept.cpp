#include "oexcept.hpp"
#include "ortmgr.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "ostring.hpp"

using std::string;
using std::vector;

bool g_installExceptionClasses(Session*);
bool g_uninstallExceptionClasses(Session*);

//============================================================================//
//:::::::::::::::::::::::::::::  ExceptionFrame  ::::::::::::::::::::::::::::://
//============================================================================//

ExceptionFrame &ExceptionFrame::operator=(const ExceptionFrame& r) {
  method = r.method;
  file = r.file;
  line = r.line;
  return *this;
}

string ExceptionFrame::getString() const {
  string msg = method;
  if (!file.empty()) msg += " in "+file;
  if (line != -1) msg += " line "+std::to_string(line);
  return msg;
}

//============================================================================//
//:::::::::::::::::::::::::::::  ExceptionBase  :::::::::::::::::::::::::::::://
//============================================================================//

string ExceptionBase::callStack() const {
  string msg;
  if (!empty()) {
    const auto size = this->size();
    auto it = begin();
    for (size_t i = 0; i < size; ++i, ++it)
      msg += "#"+std::to_string(i)+": "+it->getString()
                +(i == size - 1 ? "" : "\n");
  }
  return msg;
}

const ExceptionFrame& ExceptionBase::getMainFrame() const {
  static const ExceptionFrame NULL_FRAME;
  if (empty()) return NULL_FRAME; // It is an error (bug) if there are no frames
  auto frame = begin();
  while (true) {
    auto it = frame;
    if (++it == end()) break;
    ++frame;
  }
  return *frame;
}

std::forward_list<ExceptionFrame>::size_type ExceptionBase::size() const noexcept {
  std::forward_list<ExceptionFrame>::size_type n = 0;
  for (const auto& item : *this) ++n;
  return n;
}

//:::::::::::::::::::::::::::  ExceptionBadClass  :::::::::::::::::::::::::::://

string ExceptionBadClass::message() const {
  string msg = what;
  msg += description.empty() ? "" : ": "+description;
  msg += context.empty() ? "" : "\ncontext: "+context;
  msg += reason.empty() ? "" : "\nreason: "+reason;
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();

  return msg;
}

//::::::::::::::::::::::::::::  ExceptionBadArg  ::::::::::::::::::::::::::::://

string ExceptionBadArg::message() const {
  string msg = what + (info.empty() ? "" : ": "+info);
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();
  return msg;
}

//::::::::::::::::::::::::::::  ExceptionBadArgc  :::::::::::::::::::::::::::://

string ExceptionBadArgc::message() const {
  string msg = what+(scriptMethodName.empty() ? ""
                     : " in method '"+scriptMethodName+"'");
  if (expectedArgc != -1) {
    msg += ": expected "+std::to_string(expectedArgc)+" argument";
    if (expectedArgc != 1) msg += "s";
  }
  else if (!info.empty()) {
    msg += ": "+info;
  }
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();
  return msg;
}

//::::::::::::::::::::::::::  ExceptionBadCoreCopy  :::::::::::::::::::::::::://

string ExceptionBadCoreCopy::message() const {
  string msg = what + (info.empty() ? "" : ": "+info);
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();
  return msg;
}

//:::::::::::::::::::::::::::  ExceptionBadUsage  :::::::::::::::::::::::::::://

string ExceptionBadUsage::message() const {
  string msg = what+(scriptMethodName.empty() ? ""
                     : " of method '"+scriptMethodName+"'");
  if (!info.empty())
    msg += ": "+info;
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();
  return msg;
}

//::::::::::::::::::::::::::::::  ExceptionRTE  :::::::::::::::::::::::::::::://

string ExceptionRTE::message() const {
  string msg = what + (info.empty() ? "" : ": "+info);
  if (empty())
    msg += "\nnote: missing exception frame (please report a bug)";
  else if (&front() != &getMainFrame())
    msg += "\ncall stack:\n"+callStack();
  return msg;
}

//============================================================================//
//::::::::::::::::::::::::::::  OM Encapsulation  :::::::::::::::::::::::::::://
//============================================================================//

ORet oxExceptionBase::ommGetMessage(OArg rec, int, OArg*, Session* m) {
  _ENTER_NOCHK("exception::message");
  ORef msg = BTI::getTypeClass<string>()->createObject(m);
  _CHECK_EXCEPTION();
  ExceptionBase* except = getClass()->castException(this);
  *_PString(msg) = except->message();
  return msg->disposableResult();
}

ORet oxExceptionBase::ommGetLine(OArg rec, int, OArg*, Session* m) {
  _ENTER_NOCHK("exception::line");
  ORef r = BTI::getTypeClass<string>()->createObject(m);
  _CHECK_EXCEPTION();
  ExceptionBase* except = getClass()->castException(this);
  const auto line = except->getMainFrame().line;
  *_PString(r) = line == -1 ? "" : std::to_string(line);
  return r->disposableResult();
}

_DEFINE_MCLASS(ExceptionBase);
_DEFINE_MCLASS(ExceptionBadClass);
_DEFINE_MCLASS(ExceptionBadArg);
_DEFINE_MCLASS(ExceptionBadArgc);
_DEFINE_MCLASS(ExceptionBadCoreCopy);
_DEFINE_MCLASS(ExceptionBadUsage);
_DEFINE_MCLASS(ExceptionRTE);

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBase, OInstance)
  PUBLIC (oxExceptionBase::ommGetMessage, "message")
  PUBLIC (oxExceptionBase::ommGetLine, "line")
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBadClass, oxExceptionBase)
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBadArg, oxExceptionBase)
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBadArgc, oxExceptionBase)
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBadCoreCopy, oxExceptionBase)
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionBadUsage, oxExceptionBase)
_END_METHOD_TABLE()

_BEGIN_EXPORTED_METHOD_TABLE(oxExceptionRTE, oxExceptionBase)
_END_METHOD_TABLE()

//============================================================================//
//:::::::::::::::::::  Installation of Exception classes  :::::::::::::::::::://
//============================================================================//

bool g_installExceptionClasses(Session* m) {
  OClass* cls;
  cls = new oxExceptionBase_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionBadClass_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionBadArg_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionBadArgc_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionBadUsage_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionBadCoreCopy_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  cls = new oxExceptionRTE_Class(m);
  if (!cls) return false;
  cls->finalize(m);

  return true;
}

bool g_uninstallExceptionClasses(Session* m) {
  static const char* exceptionClasses[] {
    "exception",
    "bad class",
    "bad arg",
    "bad argc",
    "bad core copy",
    "bad usage",
    "runtime error"
  };

  for (auto className : exceptionClasses) {
    OClass* cls = m->classMgr->getClass(className);
    if (cls == nullptr) return false;
    delete cls;
  }

  return true;
}
