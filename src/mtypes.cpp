#include <algorithm>
#include "mtypes.hpp"
#include "lvector.hpp"

using std::string;
using std::vector;

//============================================================================//
//:::::::::::::::::::::::::::::::   MSymbol   :::::::::::::::::::::::::::::::://
//============================================================================//

MSymbol& MSymbol::operator=(MSymbol&& r) {
  *static_cast<ISymbol*>(this) = std::move(*static_cast<ISymbol*>(&r));
  variableOffset = r.variableOffset;
  definitionIP = r.definitionIP;
  return *this;
}

int MSymbol::getTmpId() const noexcept {
  if (name.length() < 2 || !isVariable() || name[0] != '#') return -1;
  char *endptr;
  const auto i = strtol(name.c_str() + 1, &endptr, 10);
  return *endptr == 0 ? i : -1;
}

void MSymbol::s_convert(ISymbol& symbol) {
  if (symbol.isId())
    symbol.type = SYMTYPE_VARIABLE | (symbol.type & SYMTYPE_USAGE_MASK);
  else if (symbol.getLiteralType() == SYMTYPE_LITERAL_SEL)
    symbol.type = SYMTYPE_SELECTOR | SYMTYPE_STATIC;
  else if (symbol.isLiteralValue())
    symbol.type |= SYMTYPE_STATIC;
}

bool MSymbol::s_isNumber(const char* text) noexcept {
  const char* p = text;
  while (*p)
    if (*p > '0' - 1 && *p < '9' + 1) ++p;
    else return false;
  return true;
}

//============================================================================//
//:::::::::::::::::::::::::::::   MDataSegment   ::::::::::::::::::::::::::::://
//============================================================================//

bool MDataSegment::isSymbolName(const string& symbolName) const noexcept {
  for (const auto& symbol : localSymbolTable)
    if (symbol.name == symbolName) return true;
  return false;
}

MOperand MDataSegment::findSymbol(const string& symbolName) const noexcept {
  for (size_t i = 0; i < localSymbolTable.size(); ++i)
    if (symbolName == localSymbolTable[i].name) return static_cast<MOperand>(i);
  return SYM_INDEX_NOT_FOUND;
}

MOperand MDataSegment::findSymbol(const ISymbol& r) const noexcept {
  MSymbol symbol{r};
  MSymbol::s_convert(symbol);
  for (size_t i = 0; i < localSymbolTable.size(); ++i)
    if (localSymbolTable[i] == symbol) return static_cast<MOperand>(i);
  return SYM_INDEX_NOT_FOUND;
}

MOperand MDataSegment::addSymbol(const ISymbol& symbol) {
  const MOperand operand = findSymbol(symbol);
  if (operand != SYM_INDEX_NOT_FOUND) return operand;
  add(symbol);
  const auto index = localSymbolTable.size() - 1;
  if (localSymbolTable[index].isLiteralValue())
    localSymbolTable[index].setStatic();
  return index;
}

MOperand MDataSegment::addSymbol(ISymbol&& symbol) {
  const MOperand operand = findSymbol(symbol);
  if (operand != SYM_INDEX_NOT_FOUND) return operand;
  add(std::move(symbol));
  const auto index = localSymbolTable.size() - 1;
  if (localSymbolTable[index].isLiteralValue())
    localSymbolTable[index].setStatic();
  return index;
}

MOperand MDataSegment::addThisSymbol(bool argReference) {
  ISymbol symbol("this", SYMTYPE_LITERAL_ID);
  auto index = findSymbol(symbol);
  if (index == SYM_INDEX_NOT_FOUND) {
    MSymbol::s_convert(symbol);
    index = addSymbol(std::move(symbol));
  }
  localSymbolTable[index].variableOffset = -1;
  if (argReference)
    localSymbolTable[index].type |= SYMTYPE_ARG_REF;
  return index;
}

bool MDataSegment::setMethodArgNames_PREV(vector<string>& argNames) {
  // check if 'this' was supplied as an argument
  const auto END = argNames.end();
  if (std::find(argNames.begin(), END, string{"this"}) != END)
    throw string{"'this' cannot be used as an argument name"};
  // check for duplicate arguments
  for (auto it = argNames.begin(); it != END; ++it)
    if (std::find(it + 1, END, *it) != END)
      throw "duplicate argument '"+*it+"' in argument list";
  // add 'this' to local symbol table as argument reference
  addThisSymbol(true);
  // now add argument names to local symbol table as argument references
  int argIndex = 0;
  for (auto& argName : argNames) {
    ISymbol symbol(std::move(argName), SYMTYPE_LITERAL_ID);
    auto index = findSymbol(symbol);
    if (index == SYM_INDEX_NOT_FOUND) {
      MSymbol::s_convert(symbol);
      index = addSymbol(std::move(symbol));
    }
    else if (index == SYM_INDEX_SYSOBJECT)
      return false;
    localSymbolTable[index].type |= SYMTYPE_ARG_REF;
    localSymbolTable[index].variableOffset = argIndex++;
  }
  return true;
}

void MDataSegment::setMethodArgNames(vector<string>& argNames) {
  // build list for new symbols
  vector<MSymbol> argSymbols;
  argSymbols.reserve(argNames.size() + 1);
  // add 'this' to symbol list
  argSymbols.emplace_back("this", SYMTYPE_LITERAL_ID);
  MSymbol::s_convert(argSymbols.back());
  argSymbols.back().variableOffset = -1;
  // add argument symbols
  for (size_t i = 0; i < argNames.size(); ++i) {
    argSymbols.emplace_back(std::move(argNames[i]), SYMTYPE_LITERAL_ID);
    MSymbol::s_convert(argSymbols.back());
    argSymbols.back().variableOffset = i;
  }
  // add new arg symbols or modify those already present in the symbol table
  auto end = argSymbols.end();
  for (size_t i = 0; i < localSymbolTable.size() && !argSymbols.empty(); ++i) {
    auto& symbol = localSymbolTable[i];
    auto it = std::find(argSymbols.begin(), end, symbol);
    if (it != end) {
      symbol.type |= SYMTYPE_ARG_REF;
      symbol.variableOffset = it->variableOffset;
      if (argSymbols.size() > 1)
        *it = std::move(argSymbols.back());
      argSymbols.pop_back();
      end = argSymbols.end();
    }
  }
  // add symbols not present in the symbol table
  for (auto& argSymbol : argSymbols) {
    localSymbolTable.emplace_back(std::move(argSymbol));
    localSymbolTable.back().type |= SYMTYPE_ARG_REF;
  }
}

void MDataSegment::checkMethodArgNames(const vector<string>& argNames) const {
  const auto end = argNames.end();
  const auto begin = argNames.begin();
  if (end == begin) return;
  // check if 'this' was supplied as an argument
  if (std::find(begin, end, string{"this"}) != end)
    throw string{"'this' cannot be used as an argument name"};
  // check for duplicate arguments
  for (auto it = begin; it != end - 1; ++it)
    if (std::find(it + 1, end, *it) != end)
      throw "duplicate argument '"+*it+"' in argument list";
}

void MDataSegment::refreshLambdaArglist(vector<string>& argNames,
                                        bool diagnoseUndefinedArgs) {
  ASSERT(!argNames.empty());
  // build new symbol list
  vector<MSymbol> argSymbols;
  argSymbols.reserve(argNames.size());
  for (size_t i = 0; i < argNames.size(); ++i) {
    argSymbols.emplace_back(std::move(argNames[i]), SYMTYPE_LITERAL_ID);
    MSymbol::s_convert(argSymbols.back());
    argSymbols.back().variableOffset = i;
  }
  // modify arg symbols already present in the symbol table
  auto end = argSymbols.end();
  for (size_t i = 0; i < localSymbolTable.size(); ++i) {
    auto& symbol = localSymbolTable[i];
    if (diagnoseUndefinedArgs && argSymbols.empty()) {
      if (symbol.isArgRef())
        symbol.variableOffset = -2;
      continue;
    }
    auto it = std::find(argSymbols.begin(), end, symbol);
    if (it != end) {
      symbol.type = (symbol.type & ~SYMTYPE_USAGE_MASK) | SYMTYPE_ARG_REF;
      symbol.variableOffset = it->variableOffset;
      if (argSymbols.size() > 1)
        *it = std::move(argSymbols.back());
      argSymbols.pop_back();
      end = argSymbols.end();
    }
    if (!diagnoseUndefinedArgs && argSymbols.empty())
      return;
  }
  ASSERT(localSymbolTable[0].variableOffset == -1);
  // add symbols not present in the symbol table
  for (auto& argSymbol : argSymbols) {
    localSymbolTable.emplace_back(std::move(argSymbol));
    localSymbolTable.back().type |= SYMTYPE_ARG_REF;
  }
}

void MDataSegment::prepDiagsForLambdaNoArgs() {
  for (size_t i = 1; i < localSymbolTable.size(); ++i) {
    auto& symbol = localSymbolTable[i];
    if (symbol.isArgRef())
      symbol.variableOffset = -2;
  }
}

bool MDataSegment::checkLambdaProto(vector<string>& argNames) {
  if (argNames.empty()) {
    // skip first entry, which is the `this` variable
    for (size_t i = 1; i < localSymbolTable.size(); ++i)
      if (localSymbolTable[i].isArgRef()) return false;
  }
  else {
    // build new symbol list
    vector<MSymbol> argSymbols;
    argSymbols.reserve(argNames.size());
    for (size_t i = 0; i < argNames.size(); ++i) {
      argSymbols.emplace_back(std::move(argNames[i]), SYMTYPE_LITERAL_ID);
      MSymbol::s_convert(argSymbols.back());
      argSymbols.back().variableOffset = i;
    }
    // check if arg symbols match those in the symbol table
    auto end = argSymbols.end();
    for (size_t i = 0; i < localSymbolTable.size(); ++i) {
      auto& symbol = localSymbolTable[i];
      if (!symbol.isArgRef() || symbol.name == "this")
        continue;
      if (argSymbols.empty())
        return false;
      auto it = std::find(argSymbols.begin(), end, symbol);
      if (it != end) {
        if (symbol.variableOffset != it->variableOffset)
          return false;
        if (argSymbols.size() > 1)
          *it = std::move(argSymbols.back());
        argSymbols.pop_back();
        end = argSymbols.end();
      }
    }
    return argSymbols.empty();
  }
  return true;
}

//============================================================================//
//::::::::::::::::::::::::::::::::   MBlock   :::::::::::::::::::::::::::::::://
//============================================================================//

MBlock::MBlock(MBlock&& b) :
    dataSegment          {std::move(b.dataSegment)         },
    codeSegment          {std::move(b.codeSegment)         },
    classInheritanceTree {std::move(b.classInheritanceTree)},
    definedClass         {std::move(b.definedClass)        },
    parentIndex          {std::move(b.parentIndex)         },
    evaluate             {std::move(b.evaluate)            },
    execute              {std::move(b.execute)             },
    type                 {std::move(b.type)                } {
}

void MBlock::finalize() {
  unsigned currentLocalVarOffset = 0, currentStaticVarOffset = 0;
  for (auto& symbol : dataSegment.localSymbolTable) {
    if (symbol.isStatic()) {
      symbol.variableOffset = currentStaticVarOffset++;
    }
    else if (symbol.isSelector()) /* sels are static */ {
      symbol.setStatic();
      symbol.variableOffset = currentStaticVarOffset++;
    }
    else if (symbol.isVariable()) {
      symbol.variableOffset = currentLocalVarOffset++;
    }
  }
  // finish off the static data segment
  dataSegment.staticData.provide(currentStaticVarOffset);
  // fix enter instruction
  if (codeSegment[0].opcode == enter)
    codeSegment[0].operand = currentLocalVarOffset;
  // order classes for registration
  orderClasses();
}

void MBlock::orderClasses() {
  using node_t = i1::TreeNode<InheritanceData>*;
  if (classInheritanceTree.empty()) return;
  vector<std::pair<node_t, node_t>> relocationPairs;
  for (auto* inheritanceData : classInheritanceTree.getRoot()->branches()) {
    const string& baseClass = inheritanceData->data().base;
    if (baseClass == "object")
      continue;
    auto* baseClassNode = classInheritanceTree.getNode(
        InheritanceData{baseClass, ""});
    if (baseClassNode)
      relocationPairs.push_back({baseClassNode, inheritanceData});
  }
  for (auto& relocationPair : relocationPairs)
    relocationPair.first->relink(relocationPair.second);
}

const MBlock* MBlock::getParentMethodBlock(const i1::vector<MBlock>& bt) const {
  const MBlock* p = this;
  while (p && p->type != METHOD)
    p = p->parentIndex == -1 ? nullptr : &bt[p->parentIndex];
  return p;
}

const OClass* MBlock::getOwnerClass(const i1::vector<MBlock>& bt) const {
  const MBlock* const block = getParentMethodBlock(bt);
//   ASSERT(!block || bt[block->parentIndex].definedClass != nullptr);
  return block ? bt[block->parentIndex].definedClass : nullptr;
}

void MBlock::setMethodArgNames(vector<string>& argNames) {
  type = METHOD;
  dataSegment.setMethodArgNames(argNames);
}

#if 0
void MBlock::predefineConstants(const std::vector<OInstance*>& values) {
  static const string vars[]{"true", "false", "null"};
  auto& symTab = dataSegment.localSymbolTable;
  for (auto i : {0, 1, 2}) {
    if (!values[i]) continue;
    auto varIndex = dataSegment.findSymbol(vars[i]);
    if (varIndex == SYM_INDEX_NOT_FOUND || !symTab[varIndex].isVariable())
      varIndex = addSymbol(ISymbol(string(vars[i]), SYMTYPE_STATIC_VAR));
    else
      symTab[varIndex].type |= SYMTYPE_STATIC;
    auto ret = dataSegment.staticData.add_primary(OVariable(values[i]));
    symTab[varIndex].variableOffset = std::get<1>(ret);
    auto& var = std::get<0>(ret);
    var.setConst();
    var.setRef();
    ASSERT(&var == &dataSegment.staticData[symTab[varIndex].variableOffset]);
    ASSERT(var.objectRef == values[i]);
  }
}
#endif // 0

//============================================================================//
//:::::::::::::::::::::::::::::   Disassembler   ::::::::::::::::::::::::::::://
//============================================================================//

constexpr int TAB_WIDTH = 2;

string MInstruction::disassemble(int spacing) const {
  static const string opcodeName[]{
    "nop",    "setl",    "setf",    "push",    "pushso",  "pushrz",
    "exec",   "execb",   "execlb",  "execgp",  "execcb",  "stor",
    "enter",  "leave",   "jmp",     "dto",     "dtor",    "rop",
    "chkrv",  "lres",    "pushra",  "mref",    "pushrec", "pushsel",
    "pushb",  "rbc",     "setv",    "pushsym", "!!!"
  };

  static const string ropcodeName[]{
    "nopex", "scop", "lcru", "zres", "stchk", "cres",
  };

  static int maxLength = 0;

  if (maxLength == 0) {
    for (const auto& name : opcodeName)
      if (maxLength < name.length()) maxLength = name.length();
    for (const auto& name : ropcodeName)
      if (maxLength < name.length()) maxLength = name.length();
  }

  switch (opcode) {
    case nop:
    case pushso: return opcodeName[opcode];
    case rop: if (operand >= 0 && operand <= cres) return ropcodeName[operand];
  }

  const int numTrailingSpaces = maxLength - opcodeName[opcode].length();
  return (opcode >= 0 && opcode < oc_end)
    ? opcodeName[opcode]+string(numTrailingSpaces+spacing, ' ')+std::to_string(operand)
    : string("???");
}

string MSymbol::disassemble() const {
  string code;
  if ((type & SYMTYPE_LITERAL_MASK) != 0) {
    const string prefix = (type & SYMTYPE_LITERAL_HEXA) ? "0x" : (
        (type & SYMTYPE_LITERAL_BINARY) ? "0b" : "");
    switch (type & SYMTYPE_LITERAL_MASK) {
      case SYMTYPE_LITERAL_ULONG:
        code = ".Const ulong value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_LONG:
        code = ".Const long value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_UINT:
        code = ".Const uint value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_INT:
        code = ".Const int value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_SHORT:
        code = ".Const short value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_CHAR:
        code = ".Const char value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_BOOL:
        code = ".Const bool value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_DOUBLE:
        code = ".Const double value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_FLOAT:
        code = ".Const float value = "+prefix+name+" offset ";
        break;
      case SYMTYPE_LITERAL_STRING:
        code = ".Const string value = \""+prefix+name+"\""+" offset ";
        break;
      case SYMTYPE_LITERAL_ID:
        code = ".Identifier (parser) "+name+(isStatic() ? " static " : " ")
                             +string("offset ");
        break;
      case SYMTYPE_LITERAL_SEL:
        code = ".Selector (parser) "+name+(isStatic() ? " static " : " ")
                           +string("offset ");
        break;
    }
    code += std::to_string(variableOffset);
  }
  else if (auto varType = getVariableType(); varType != 0) {
    switch (varType) {
      case SYMTYPE_VARIABLE:
        code = ".Variable "+name+(isStatic() ? " static " : " ")
                           +string("offset ");
        break;
      case SYMTYPE_SELECTOR:
        code = ".Selector "+name+(isStatic() ? " static " : " ")
                           +string("offset ");
        break;
      case SYMTYPE_CLASS:
        code = ".Class "+name+(isStatic() ? " static " : " ")+string("offset ");
        break;
      case SYMTYPE_BLOCK:
        code = ".Block "+name+(isStatic() ? " static " : " ")+string("offset ");
        break;
    }
    code += std::to_string(variableOffset);
  }
  else {
    code = ".UnknownType "+name+(isStatic() ? " static " : " ")
                          +string("offset ");
    code += std::to_string(variableOffset);
  }
  return code;
}

void MBlock::disassemble(string& code, int depth) const {
  const string prependSpaces(TAB_WIDTH*depth, ' ');
  code += prependSpaces+".Data\n";
  dataSegment.disassemble(code, depth);
  code += prependSpaces+".Code\n";
  disassembleCodeSegment(code, depth+1);
  code += "\n";
}

void MBlock::disassembleCodeSegment(string& code, int depth) const {
  const int spacing = 2;
  const string prependSpaces(TAB_WIDTH*depth, ' ');
  for (const auto& instruction : codeSegment)
    code += prependSpaces+instruction.disassemble(spacing)+"\n";
}

void MDataSegment::disassemble(string& code, int depth) const {
  const string prependSpaces(TAB_WIDTH*depth, ' ');
  for (const auto& symbol : localSymbolTable)
    code += prependSpaces+symbol.disassemble()+"\n";
}
