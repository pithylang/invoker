#include "osetiter.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "ortmgr.hpp"

using std::string;
using std::set;

_TEMPLATE_DEFINITION(OSetIterator, OBidirectionalIterator);

template<typename t>
  static string s_getFullMethodName(const string& methodName) noexcept {
    return BTI::getSetIteratorClass<t>()->name+"::"+methodName;
  }

template<typename t>
  void s_setValue(ORef r, const t& val) noexcept {_P(t, r)->setValue(val);}
template<>
  void s_setValue<string>(ORef r, const string& val) noexcept {*_PStr(r) = val;}

//============================================================================//
//:::::::::::::::::::::::::::::   OSetIterator   ::::::::::::::::::::::::::::://
//============================================================================//

template<typename t>
  ORet _O(SetIterator)<t>::ommGetValue(OArg rec, int, OArg*, Session* m) {
    ORef r = BTI::getTypeClass<t>()->createObject(m);
    _RETHROW(s_getFullMethodName<t>("value"));
    auto& value = reverse_ ? *std::make_reverse_iterator(*this) :
                             this->operator*();
    s_setValue(r, value);
    return OVar(r, USG_DISPOSABLE|USG_CONST);
  }

// it component: arg0; it[arg0];
template<typename t>
  ORet _O(SetIterator)<t>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
    m->throwRTE(s_getFullMethodName<t>(S_component),
                "'"+getRootClass()->name+"' does not support operator[]");
    return undefinedResult();
  }

template<typename t>
  ORet _O(SetIterator)<t>::inc(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<t>(S_inc));
    reverse_ ? this->operator--() : this->operator++();
    return makeResult();
  }

template<typename t>
  ORet _O(SetIterator)<t>::dec(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<t>(S_dec));
    reverse_ ? this->operator++() : this->operator--();
    return makeResult();
  }

template<typename t>
  ORet _O(SetIterator)<t>::postinc(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>(S_postinc));
    _MUTABLE_REC();
    OClass* const type = BTI::getSetIteratorClass<t>();
    _RETURN_TYPE(_O(SetIterator)<t>, type, reverse_ ? this->operator--(0) :
                                                      this->operator++(0));
  }

template<typename t>
  ORet _O(SetIterator)<t>::postdec(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>(S_postdec));
    _MUTABLE_REC();
    OClass* const type = BTI::getSetIteratorClass<t>();
    _RETURN_TYPE(_O(SetIterator)<t>, type, reverse_ ? this->operator++(0) :
                                                      this->operator--(0));
  }

/// it1 == it2;
template<typename t>
  ORet _O(SetIterator)<t>::eq(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>(S_eq));
    _CHECK_ARG_TYPE(0, BTI::getSetIteratorClass<t>());
    if (_PCSetIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("set iterator mismatch")
    const auto& it = _ARG(0)->castRef<_O(SetIterator)>();
    _RETURN_TYPE(oxType<bool>, BTI::getTypeClass<bool>(), *this == it);
  }

/// it1 != it2;
template<typename t>
  ORet _O(SetIterator)<t>::ne(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>(S_ne));
    _CHECK_ARG_TYPE(0, BTI::getSetIteratorClass<t>());
    if (_PCSetIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("set iterator mismatch")
    const auto& it = _ARG(0)->castRef<_O(SetIterator)>();
    _RETURN_TYPE(oxType<bool>, BTI::getTypeClass<bool>(), *this != it);
  }

template<typename t>
  ORet _O(SetIterator)<t>::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<t>("swap:"));
    _MUTABLE_REC();
    _MUTABLE_ARG(0);
    const auto typeId = BTI::getSetIteratorClassId<t>();
    if (_ARG(0)->getRootClassId() != typeId)
      return m->badArgTypeId(_ox_method_name, 0, typeId, argv);
    std::swap(castRef<_O(SetIterator)>(), _ARG(0)->castRef<_O(SetIterator)>());
    return makeResult();
  }

//============================================================================//
//::::::::::::::::::::::::::   OSetIterator_Class   :::::::::::::::::::::::::://
//============================================================================//

template<typename t>
  META(SetIterator)<t>::META(SetIterator)(Session* m,
      const std::string& typeName) : OClass(m, typeName) {
    m->importRequirement(this, "RandomAccessIterator");
    m->rethrow("'"+typeName+" class'::'iterator class constructor'");
  }

template<typename t>
  ORef META(SetIterator)<t>::copyCore(ORef self, const_ORef r, Session* m) const {
    // error condition handled by OClass::copyObject
    if (r->getRootClass() != getRootClass()) return nullptr;
    *_PSetIter(t, self) = *_PCSetIter(t, r);
    return self;
  }

template<typename t>
  ORef META(SetIterator)<t>::moveCore(ORef self, ORef r) const {
    // error condition handled by OClass::moveObject
    if (r->getRootClass() != getRootClass()) return nullptr;
    _O(SetIterator)<t>* const _this = static_cast<_O(SetIterator)<t>*>(self);
    *_this = std::move(*_PSetIter(t, r));
    return self;
  }

template<typename t>
  OClass* META(SetIterator)<t>::createClass(Session* m,
      const std::string& typeName) const {
    _ENTER_METHOD_NOCHK("OSetIterator_Class<"+BTI::getTypeClass<t>()->name+
                        ">::createClass");
    OClass* cls = new OSetIterator_Class(m, this->name, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

template<typename t>
  OClass* META(SetIterator)<t>::createMutableClass(Session* m,
      const std::string& typeName) const {
    _ENTER_METHOD_NOCHK("OSetIterator_Class<"+BTI::getTypeClass<t>()->name+
                        ">::createMutableClass");
    OClass* cls = new OSetIterator_Class(m, this, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

//============================================================================//
//::::::::::::::::::::::::::   OSetIterator<ORef>   :::::::::::::::::::::::::://
//============================================================================//

ORet _O(SetIterator)<ORef>::ommGetValue(OArg rec, int argc, OArg* argv, Session* m) {
    ORef const value = reverse_ ? *std::make_reverse_iterator(*this) :
                                  this->operator*();
    return OVar{value, USG_CONST};
  }

ORet _O(SetIterator)<ORef>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
    const string msg = "'"+getRootClass()->name+"' does not support operator[]";
    m->throwRTE(s_getFullMethodName<ORef>(S_component), msg);
    return undefinedResult();
  }

ORet _O(SetIterator)<ORef>::inc(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<ORef>(S_inc));
    reverse_ ? this->operator--() : this->operator++();
    return makeResult();
  }

ORet _O(SetIterator)<ORef>::dec(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(s_getFullMethodName<ORef>(S_dec));
    reverse_ ? this->operator++() : this->operator--();
    return makeResult();
  }

ORet _O(SetIterator)<ORef>::postinc(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<ORef>(S_postinc));
    _MUTABLE_REC();
    OClass* const type = BTI::getSetIteratorClass<ORef>();
    _RETURN_TYPE(_O(SetIterator)<ORef>, type,  reverse_ ? this->operator--(0) :
                                                          this->operator++(0));
  }

ORet _O(SetIterator)<ORef>::postdec(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<ORef>(S_postdec));
    _MUTABLE_REC();
    OClass* const type = BTI::getSetIteratorClass<ORef>();
    _RETURN_TYPE(_O(SetIterator)<ORef>, type, reverse_ ? this->operator++(0) :
                                                         this->operator--(0));
  }

/// it1 == it2;
ORet _O(SetIterator)<ORef>::eq(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<ORef>(S_eq));
    _CHECK_ARG_TYPE(0, BTI::getSetIteratorClass<ORef>());
    if (_PCSetIter(ORef, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("set iterator mismatch")
    const auto& it = _ARG(0)->castRef<_O(SetIterator)<ORef>>();
    _RETURN_TYPE(oxType<bool>, BTI::getTypeClass<bool>(), *this == it);
  }

/// it1 != it2;
ORet _O(SetIterator)<ORef>::ne(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(s_getFullMethodName<ORef>(S_ne));
    _CHECK_ARG_TYPE(0, BTI::getSetIteratorClass<ORef>());
    if (_PCSetIter(ORef, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("set iterator mismatch")
    const auto& it = _ARG(0)->castRef<_O(SetIterator)<ORef>>();
    _RETURN_TYPE(oxType<bool>, BTI::getTypeClass<bool>(), *this != it);
  }

ORet _O(SetIterator)<ORef>::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD(s_getFullMethodName<ORef>("swap:"), 1);
    _MUTABLE_REC();
    _MUTABLE_ARG(0);
    const auto typeId = BTI::getSetIteratorClassId<ORef>();
    if (_ARG(0)->getRootClassId() != typeId)
      return m->badArgTypeId(_ox_method_name, 0, typeId, argv);
    std::swap(castRef<_O(SetIterator)>(), _ARG(0)->castRef<_O(SetIterator)>());
    return makeResult();
  }

//============================================================================//
//:::::::::::::::::::::   OSetIterator -- Method Table   ::::::::::::::::::::://
//============================================================================//

static const char* s_methodNames[] {
  "value",
  S_component,
  S_inc,
  S_dec,
  S_postinc,
  S_postdec,
  S_eq,
  S_ne,
  "swap:"
};

#define PUBLIC_(m, i) PUBLIC(m, s_methodNames[i])

#define METHOD_TABLE_FOR_OSetIterator(t)             \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(_O(SetIterator)<t>) \
  PUBLIC_(_O(SetIterator)<t>::ommGetValue,   0)      \
  PUBLIC_(_O(SetIterator)<t>::ommComponent,  1)      \
  PUBLIC_(_O(SetIterator)<t>::inc,           2)      \
  PUBLIC_(_O(SetIterator)<t>::dec,           3)      \
  PUBLIC_(_O(SetIterator)<t>::postinc,       4)      \
  PUBLIC_(_O(SetIterator)<t>::postdec,       5)      \
  PUBLIC_(_O(SetIterator)<t>::eq,            6)      \
  PUBLIC_(_O(SetIterator)<t>::ne,            7)      \
  PUBLIC_(_O(SetIterator)<t>::ommSwap,       8)      \
_END_METHOD_TABLE()

METHOD_TABLE_FOR_OSetIterator(ulong);
METHOD_TABLE_FOR_OSetIterator(long);
METHOD_TABLE_FOR_OSetIterator(double);
METHOD_TABLE_FOR_OSetIterator(string);

_BEGIN_METHOD_TABLE(_O(SetIterator)<ORef>, OBidirectionalIterator)
  PUBLIC_(_O(SetIterator)<ORef>::ommGetValue,   0)
  PUBLIC_(_O(SetIterator)<ORef>::ommComponent,  1)
  PUBLIC_(_O(SetIterator)<ORef>::inc,           2)
  PUBLIC_(_O(SetIterator)<ORef>::dec,           3)
  PUBLIC_(_O(SetIterator)<ORef>::postinc,       4)
  PUBLIC_(_O(SetIterator)<ORef>::postdec,       5)
  PUBLIC_(_O(SetIterator)<ORef>::eq,            6)
  PUBLIC_(_O(SetIterator)<ORef>::ne,            7)
  PUBLIC_(_O(SetIterator)<ORef>::ommSwap,       8)
_END_METHOD_TABLE()

bool g_installSetIteratorClasses(Session* m) {
  extern OClass* _OXType_class[];
  OClass* cls;

  cls = new META(SetIterator)<ulong>(m, "ulong set iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_ULONGSETITER);
  _OXType_class[CLSID_ULONGSETITER] = cls;

  cls = new META(SetIterator)<long>(m, "long set iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_LONGSETITER);
  _OXType_class[CLSID_LONGSETITER] = cls;

  cls = new META(SetIterator)<double>(m, "double set iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_DBLSETITER);
  _OXType_class[CLSID_DBLSETITER] = cls;

  cls = new META(SetIterator)<string>(m, "string set iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_STRSETITER);
  _OXType_class[CLSID_STRSETITER] = cls;

  cls = new META(SetIterator)<ORef>(m, "set iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_OBJSETITER);
  _OXType_class[CLSID_OBJSETITER] = cls;

  return true;
}
