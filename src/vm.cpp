#include <algorithm>
#include "vm.hpp"
#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "oclsmgr.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "oexcept.hpp"
#include "oswitchtab.hpp"
#include "oclassobj.hpp"
#include "multiType.hpp"
#include "selectors.h"
// #include "mdebugger.hpp"
#include "fswitch.h"

using std::string;
using std::vector;
using std::array;

#define _Session _PRTMgr(SO)
#define _REF(var) (var)->objectRef
#define _ASSERT_TODO(condition, session) _ASSERT(condition, session)

#define LIT_TYPE_SWITCH_TABLE(TABLE_ID) \
  SWITCH_TABLE(TABLE_ID, 0)             \
    DEFINE_CASE(TABLE_ID, NO_LITERAL)   \
    DEFINE_CASE(TABLE_ID, ULONG)        \
    DEFINE_CASE(TABLE_ID, LONG)         \
    DEFINE_CASE(TABLE_ID, UINT)         \
    DEFINE_CASE(TABLE_ID, INT)          \
    DEFINE_CASE(TABLE_ID, SHORT)        \
    DEFINE_CASE(TABLE_ID, CHAR)         \
    DEFINE_CASE(TABLE_ID, BOOL)         \
    DEFINE_CASE(TABLE_ID, DOUBLE)       \
    DEFINE_CASE(TABLE_ID, FLOAT)        \
    DEFINE_CASE(TABLE_ID, STRING)       \
    DEFINE_CASE(TABLE_ID, SEL)          \
    DEFINE_CASE(TABLE_ID, ID)           \
    DEFINE_CASE(TABLE_ID, PTR)          \
    DEFINE_CASE(TABLE_ID, ERROR1)       \
    DEFINE_CASE(TABLE_ID, ERROR2)       \
  END_SWITCH_TABLE(TABLE_ID)

//============================================================================//
//:::::::::::::::   VirtualMachine - Global and static data   :::::::::::::::://
//============================================================================//

unsigned VirtualMachine::s_debuggerMutex = 0;
vector<ORef> VirtualMachine::s_globalConstants{nullptr, nullptr, nullptr};

bool g_deferTmpDeletion(const VirtualMachine* vm, OVar* rhs);

//============================================================================//
//:::::::::::::::::::::   VirtualMachine Constructors   :::::::::::::::::::::://
//============================================================================//

VirtualMachine::VirtualMachine(Session* m,
                               MBlockTable* blockTable,
                               MVIASArgTable& tableVIASA) :
    parentMachine  (        nullptr         ),
    debugger       (        nullptr         ),
    callStack      {        nullptr         },
    switchTablePtr (    new MSwitchTable    ),
    switchTableRef (    *switchTablePtr     ),
    BT             (       blockTable       ),
    BP             (        nullptr         ),
    IP             (           0            ),
    SO             (static_cast<ORTMgr*>(m) ),
    SOVAR          (         SO, 0          ),
    LR             (           1            ),
    RC             (        M_FRAME         ),
    XF             (        M_NORMAL        ),
    BR             (           0            ),
    returnPoint    {                        },
    tableVIASArgs  (       tableVIASA       ) {
  // configure session
  m->setMainMachine(this);
  // initialize static data
  if (!s_globalConstants[0]) initGlobalConstants();
  // begin new frame
  callStack.push();
  // save previous frame registers
  callStack.top->BP = BP;
  callStack.top->IP = IP;
  saveRC(callStack.top);
  // switch to new frame
  BP = callStack.top->block = &(*BT)[0];
  IP = 0;
  // initialize global symbols
  initGlobalVariables();
}

VirtualMachine::VirtualMachine(VirtualMachine& vm) :
    parentMachine  (          &vm           ),
    debugger       (      vm.debugger       ),
    callStack      {    vm.callStack.top    },
    switchTablePtr (        nullptr         ),
    switchTableRef (   vm.switchTableRef    ),
    BT             (         vm.BT          ),
    BP             (        nullptr         ),
    IP             (           0            ),
    SO             (         vm.SO          ),
    SOVAR          (         SO, 0          ),
    LR             (           1            ),
    RC             (        M_FRAME         ),
    XF             (        M_NORMAL        ),
    BR             (           0            ),
    returnPoint    {                        },
    tableVIASArgs  (    vm.tableVIASArgs    ) {}

MCallFrame* VirtualMachine::currentFrame() const noexcept {
  MCallFrame* p = callStack.top;
  while (p && !p->block) p = p->prev;
  return p;
}

MCallFrame* VirtualMachine::parent() const noexcept {
  MCallFrame* p = callStack.top->prev;
  while (p && !p->block) p = p->prev;
  return p;
}

std::tuple<MCallFrame*, MCallFrame*>
VirtualMachine::getParentChildPair() const noexcept {
  MCallFrame* p = callStack.top->prev;
  MCallFrame* child = callStack.top;
  while (p && !p->block) {child = p; p = p->prev;}
  return {p, child};
}

MCallFrame* VirtualMachine::getParentMethodFrame() const noexcept {
  MCallFrame* p = parent();
  while (p && p->isInlineBlock()) {
    p = p->prev;
    while (p && !p->block) p = p->prev;
  }
  _ASSERT(!p || (p->receiver && _REF(p->receiver)), _Session);
  return p;
}

MCallFrame* VirtualMachine::getCurrentMethodFrame() const noexcept {
  MCallFrame* p = callStack.top;
  while (p && p->isInlineBlock()) {
    p = p->prev;
    while (p && !p->block) p = p->prev;
  }
  _ASSERT(!p || (p->receiver && _REF(p->receiver)), _Session);
  return p;
}

//============================================================================//
//::::::::::::::::::::  Initialization And Termination   ::::::::::::::::::::://
//============================================================================//

void VirtualMachine::init(int blockId) {
  const auto parentMachineST = parentMachine->callStack.top;
  callStack.top = parentMachineST;
  // set default return condition
  setReturnCondition(parentMachineST);
  // begin new frame
  callStack.push();
  // save parent machine registers
  callStack.top->BP = /*parentMachineST->*/BP;
  callStack.top->IP = /*parentMachineST->*/IP;
  saveRC(callStack.top);
  // switch to new frame
  BP = callStack.top->block = &(*BT)[blockId];
  IP = 0;
}

void VirtualMachine::initGlobalConstants() {
  static const string s_methodName{"VirtualMachine::initGlobalConstants"};
  Session* const m = _Session;
  s_globalConstants[0] = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_NORET(s_methodName);
  s_globalConstants[1] = BTI::getTypeClass<bool>()->createObject(m);
  _RETHROW_NORET(s_methodName);
  s_globalConstants[2] = m->nullObject();
  _PBOOL(s_globalConstants[0])->setValue(true);
  _PBOOL(s_globalConstants[1])->setValue(false);
}

void VirtualMachine::initGlobalVariables() {
  if (BP->dataSegment.localSymbolTable.size() < 4) return;
  _DEBUG( Session* const m = _Session; )
  _ASSERT(s_globalConstants[0] != nullptr, m);
  _ASSERT(BP->dataSegment.localSymbolTable[1].name == "true", m);
  _ASSERT(BP->dataSegment.localSymbolTable[2].name == "false", m);
  _ASSERT(BP->dataSegment.localSymbolTable[3].name == "null", m);
  // set true
  OVar* varPtr = &getStaticVar(BP, 0);
  if (!varPtr->undefined()) return; // already initialized
  _REF(varPtr) = s_globalConstants[0];
  varPtr->setRef();
  varPtr->setConst();
  // set false
  varPtr = &getStaticVar(BP, 1);
  _REF(varPtr) = s_globalConstants[1];
  varPtr->setRef();
  varPtr->setConst();
  // set null
  varPtr = &getStaticVar(BP, 2);
  _REF(varPtr) = s_globalConstants[2];
  varPtr->setRef();
  varPtr->setConst();
}

//============================================================================//
//:::::::::::::::::::::::::::   Method Execution   ::::::::::::::::::::::::::://
//============================================================================//

void VirtualMachine::execBlock(int blockId,
                               MBlock::Type setType,
                               MOpcode opcode) {
  execBlockKeepResult(blockId, setType, opcode);
  if (!returning()) deleteResult();
}

void VirtualMachine::execBlockKeepResult(int blockId,
                                         MBlock::Type setType,
                                         MOpcode opcode) {
  init(blockId);
  if (setType != MBlock::PLAIN) BP->type = setType;
  if (opcode != nop) setReturnCondition(opcode);
  runStdMode();
}

bool VirtualMachine::execBlockGetResult(int blockId, MBlock::Type setType) {
  execBlockKeepResult(blockId, setType);
  const bool retVal = _Session->exceptionPending() ? false :
      g_typeValue<bool>(REF(RES));
  deleteResult();
  return retVal;
}

std::pair<ORet, int>
VirtualMachine::execMethod(OClass* baseClass,
                           const string& method,
                           ulong flags,
                           OArg rec, int argc, OArg* argv) {
  _ENTER_NOCHK("VirtualMachine::execMethod");
  Session* const m = _Session;
  _ASSERT_TODO(!parent() || parent()->block == BP, m);
  _ASSERT(_REF(rec)->getClass()->extends(baseClass), m);
  const OClass* caller = parent() ? parent()->block->getOwnerClass(*BT)
                                  : nullptr;
  // get address of method
  OAddr methodAddress;
  auto result = baseClass->getMethodAccessAddress(method, caller);
  if (result.second.notValid()) {
    if (flags & EMF_NO_THROW_UNTIL_EXEC)
      return {OVar::undefinedResult(), EMR_METHOD_NOT_RESPONDING};
    else
      m->throwRTE(method, "class '"+baseClass->name+
                          "' does not respond to method '"+method+"'");
  }
  else if (!result.first) {
    if (flags & EMF_NO_THROW_UNTIL_EXEC)
      return {OVar::undefinedResult(), EMR_METHOD_NOT_ACCESSIBLE};
    else
      m->throwRTE(method, "method "+method+" is not accessible");
  }
  else
    methodAddress = result.second;

  if (m->exceptionPending())
    return {OVar::undefinedResult(), 0};

  // execute method
  OVar retObj = execMethodAddress(methodAddress, method, flags, rec, argc, argv);
  if (m->exceptionPending()) {
    m->rethrow(method);
    return {OVar::undefinedResult(), EMR_OK};
  }
  return {retObj, EMR_OK};
}

ORet VirtualMachine::execMethodAddress(const OMethodAddress& methodAddress,
                                       const string& methodName,
                                       ulong flags,
                                       OArg rec, int argc, OArg* argv) {
  _ENTER_NOCHK("VirtualMachine::execMethodAddress");
  Session* const m = _Session;
  OVar retObj;
  if (methodAddress.scriptMethod) { // script method
    const auto blockIdMethod = methodAddress.scriptMethodAddress.block;
    // we start a new VM because we must return here if an error occurs
    VirtualMachine vm(*this);
    vm.init(blockIdMethod);
    retObj = vm.execScriptMethod(methodName, blockIdMethod, flags,
        rec, argc, argv);
  }
  else { // core method
    const auto& f = methodAddress.coreMethodAddress;
    retObj = (_REF(rec)->*f)(rec, argc, argv, m);
    if (retObj.undefined() && !m->exceptionPending()) {
      m->throwRTE(methodName, "error executing method");
      return OVar::undefinedResult();
    }
  }
  _RETHROW(methodName);
  return retObj;
}

ORet VirtualMachine::execScriptMethod(const string& method,
                                      int blockIdMethod,
                                      ulong flags,
                                      OArg rec, int argc, OArg* argv) {
  _ENTER_NOCHK("VirtualMachine::execScriptMethod");
  Session* const m = _Session;
  // set up frame
  MCallFrame* curFrame = callStack.top;
  for (int i = 0; i < argc; ++i)
    curFrame->args.push_back(argv[i]);
  curFrame->receiver = rec;
  OVar selVar;
#if USE_SELECTOR_CACHES
  SelectorCache selectorCache{m, method};
  selVar.selectorCache = &selectorCache;
  selVar.type = VARTYPE_SEL_CACHE;
#else // !USE_SELECTOR_CACHES
  SelectorData selectorData;
  selVar.selectorData = &selectorData;
  selVar.selectorData->name = method;
  selVar.selectorData->owner = nullptr;
  selVar.type = VARTYPE_SEL_DATA;
#endif // USE_SELECTOR_CACHES
  selVar.usage = 0;
  curFrame->selector = selVar;
  // run method -- TODO run in the mode of `this` machine
  runStdMode();
  // check exception
  _CHECK_EXCEPTION();
  // return value
  return RES;
}

//::::::::::::::::::::::::::::::::   loops   ::::::::::::::::::::::::::::::::://

struct AtExit {
  VirtualMachine& vm;
  bool active;
  AtExit(VirtualMachine& toFinalize) : vm{toFinalize}, active{true} {}
  AtExit(VirtualMachine& toFinalize, bool b) : vm{toFinalize}, active{b} {}
  ~AtExit() {
    if (!active || !_PRTMgr(vm.SO)->exceptionPending()) return;
    vm.setReturnCondition(vm.parentMachine->callStack.top);
    vm.runStdMode();
  }
  void activate() noexcept {active = true;}
  void deactivate() noexcept {active = false;}
};

void VirtualMachine::execLoop(const vector<int>& args,
                              bool whileLoop, bool conditionFirst) {
  _ENTER_NOCHK("VirtualMachine::execLoop");

  Session* const m = _Session;
  _ASSERT(args.size() == 2 || args.size() == 4, m);
  _ASSERT(!inBreakCondition(), m);

  beginLoop(args, conditionFirst);

  if (args.size() == 2) { // while, until loops
    VirtualMachine doVM(*this);
    VirtualMachine condVM(conditionFirst ? *this : doVM);
    const bool doIsFirst = !conditionFirst;
    AtExit atExit(doVM, doIsFirst); // active when do block comes first
    while (true) {
      if (doIsFirst) { // execute block
        doVM.execBlock(args[1], MBlock::LOOP, leave);
        if (m->exceptionPending()) {
          atExit.deactivate();
          _RETHROW_HERE_NORET();
        }
        if (inBreakCondition()) {
          exitBreakCondition();
          break; // finish do block
        }
        if (doVM.returning()) {
          // transfer RES and XF flag to parent machine
          enterReturnCondition();
          RES = doVM.RES;
          // do block already finished
          atExit.deactivate();
          return;
        }
      }
      // check condition
      const bool retVal = condVM.execBlockGetResult(args[0], MBlock::LOOP_COND);
      _RETHROW_HERE_NORET();
      const bool stopRunning = whileLoop ? !retVal : retVal;
      if (stopRunning) break;
      if (doIsFirst) { // finish do block
        doVM.setReturnCondition(doVM.parentMachine->callStack.top);
        doVM.runStdMode();
        if (m->exceptionPending())
          atExit.deactivate(); // because block was just finished
        _RETHROW_HERE_NORET();
      }
      else { // execute do block
        doVM.execBlock(args[1], MBlock::LOOP);
        _RETHROW_HERE_NORET();
        if (inBreakCondition()) {
          _ASSERT(!doVM.returning(), m);
          exitBreakCondition();
          break; // finish
        }
        if (doVM.returning()) { // transfer RES and XF flag to parent machine
          enterReturnCondition();
          RES = doVM.RES;
          _ASSERT(!atExit.active, m);
          return;
        }
      }
    }
    if (doIsFirst) { // finish do block
      _ASSERT(doVM.BP == doVM.callStack.top->block, m);
      _ASSERT(doVM.BP == &(*BT)[args[1]], m);
      _ASSERT(doVM.BP->codeSegment[doVM.IP].opcode == leave, m);
      atExit.deactivate();
      doVM.setReturnCondition(doVM.parentMachine->callStack.top);
      doVM.runStdMode();
      _RETHROW_HERE_NORET();
    }
  }
  else { // for loop
    // initialize loop
    VirtualMachine startVM(*this);
    AtExit atExit(startVM);
    startVM.execBlock(args[0], MBlock::LOOP_INIT, leave);
    if (m->exceptionPending()) {
      atExit.deactivate();
      _RETHROW_HERE_NORET();
    }
    // a start block may issue a break
    const bool runLoop = !inBreakCondition();
    if (runLoop) {
      VirtualMachine condVM(startVM), stepVM(startVM), doVM(startVM);
      while (true) {
        // check condition
        const bool keepRunning = condVM.execBlockGetResult(args[1],
            MBlock::LOOP_COND);
        _RETHROW_HERE_NORET();
        if (!keepRunning) break;
        // execute block
        doVM.execBlock(args[3], MBlock::LOOP);
        _RETHROW_HERE_NORET();
        if (inBreakCondition()) {
          _ASSERT(!doVM.returning(), m);
          exitBreakCondition();
          break; // finish start block
        }
        if (doVM.returning()) {
          // transfer RES and XF flag to parent machine
          enterReturnCondition();
          RES = doVM.RES;
          return;
        }
        // prepare for next step
        stepVM.execBlock(args[2], MBlock::LOOP_STEP);
        _RETHROW_HERE_NORET();
      }
    }
    else
      exitBreakCondition();
    // finish start block
    atExit.deactivate();
    startVM.setReturnCondition(startVM.parentMachine->callStack.top);
    startVM.runStdMode();
    _RETHROW_HERE_NORET();
  }
}

void VirtualMachine::beginLoop(const vector<int>& args, bool conditionFirst) {
  if (args.size() == 2) {
    if (!conditionFirst)
      (*BT)[args[0]].parentIndex = args[1];
  }
  else { // args.size() == 4
    (*BT)[args[1]].parentIndex =
    (*BT)[args[2]].parentIndex =
    (*BT)[args[3]].parentIndex = args[0];
  }
}

//:::::::::::::::::::::::::::::   if-then-else   ::::::::::::::::::::::::::::://

bool VirtualMachine::execIf(OArg ifCondBlock, OArg thenBlock,
                            const vector<OArg>& elseIfBlocks,
                            OArg elseBlock) {
  _ENTER_NOCHK("VirtualMachine::execIf");
  _ASSERT_TODO(!parent() || parent()->block == BP, _Session);
  const OClass* caller = parent() ? parent()->block->getOwnerClass(*BT)
                                  : nullptr;
  Session* const m = _Session;
  bool executed = execIfThen(caller, ifCondBlock, thenBlock);
  _RETHROW_HERE_RETURN(executed);
  if (executed) return true;
  if (returning()) return true; // TODO maybe false
  for (size_t i = 0; !executed && i < elseIfBlocks.size(); i += 2) {
    executed = execIfThen(caller, elseIfBlocks[i], elseIfBlocks[i + 1]);
    _RETHROW_HERE_RETURN(executed);
    if (returning()) return executed; // TODO maybe false
  }
  if (!executed && elseBlock) {
    execIfThen(caller, nullptr, elseBlock);
    _RETHROW_HERE_RETURN(executed);
  }
  return executed;
}

bool VirtualMachine::execIfThen(const OClass* caller, OArg condBlock,
                                                      OArg thenBlock) {
  _ENTER_NOCHK("VirtualMachine::execIfThen");
  Session* const m = _Session;
  bool mustRun;

  // check condition
  const int condClassId = condBlock ? _REF(condBlock)->getClassId() : -1;
  if (condClassId == -1) // else
    mustRun = true;
  else if (condClassId == CLSID_BLOCK) {
    const int condBlockId = g_blockId(_REF(condBlock));
    VirtualMachine condBlockVM(*this);
    mustRun = condBlockVM.execBlockGetResult(condBlockId);
    _RETHROW_HERE_RETURN(false);
  }
  else if (condClassId == CLSID_BOOL)
    mustRun = g_typeValue<bool>(_REF(condBlock));
  else { // the supplied value must be converted to bool
    mustRun = g_castValue<bool>(&condBlock, m);
    _RETHROW_HERE_RETURN(false);
  }

  // execute block
  if (mustRun && thenBlock) {
    _ASSERT(_REF(thenBlock)->getClassId() == CLSID_BLOCK, m);
    const int blockId = g_blockId(_REF(thenBlock));
    VirtualMachine blockVM(*this);
    blockVM.execBlock(blockId);
    _RETHROW_HERE_RETURN(false);
    if (blockVM.returning()) { // transfer RES and XF flag to parent machine
      enterReturnCondition();
      RES = blockVM.RES;
    }
  }

  return mustRun;
}

//::::::::::::::::::::::::::::::::   switch   :::::::::::::::::::::::::::::::://

template<typename T>
T VirtualMachine::castValue(ORef rec) {
  _ENTER_METHOD_NOCHK("VirtualMachine::castValue<"+
                      BTI::getTypeClass<T>()->name+">");
  Session* const m = _Session;
  ORet result = m->valueCast(BTI::getTypeClass<T>(), rec);
  _RETHROW_HERE_RETURN(T{});
  ObjectAutoCleaner cleaner{m, result};
  T convertedValue = g_typeValue<T>(REF(result));
  _RETHROW_HERE_RETURN(T{});
  return convertedValue;
}

template<>
string VirtualMachine::castValue(ORef rec) {
  _ENTER_NOCHK("VirtualMachine::castValue<string>");
  Session* const m = _Session;
  OVar recVar{rec};
  auto result = execMethod(S_string, 0, &recVar, 0, nullptr);
  _RETHROW_HERE_RETURN(string{});
  const auto& strValue = *static_cast<String*>(_PString(REF(result.first)));
  ObjectAutoCleaner c{m, result.first};
  return strValue;
}

void VirtualMachine::execSwitch(OArg switchVar,
                                const vector<OArg>& vals,
                                const vector<OArg>& blocks,
                                OArg defBlock) {
  _ENTER_NOCHK("VirtualMachine::execSwitch");
  Session* const m = _Session;
  _ASSERT(blocks.size() == vals.size(), m);

  MCallFrame* p;
  MCallFrame* child;
  std::tie(p, child) = getParentChildPair();
  auto& switchBase = getSwitch(p ? p->block : nullptr, child->IP);
  if (!switchBase) // initialize switch
    initSwitch(switchBase, _REF(switchVar), vals);
  // get index of block to execute
  size_t i;
  switch (switchBase->dataType) {
    case Switch::SWT_UINT:
      i = static_cast<SwitchData<ulong>*>(switchBase)->
          index(castValue<ulong>(_REF(switchVar)));
      break;
    case Switch::SWT_INT:
      i = static_cast<SwitchData<long>*>(switchBase)->
          index(castValue<long>(_REF(switchVar)));
      break;
    case Switch::SWT_FP:
      i = static_cast<SwitchData<double>*>(switchBase)->
          index(castValue<double>(_REF(switchVar)));
      break;
    case Switch::SWT_BOOL:
      i = static_cast<SwitchData<bool>*>(switchBase)->
          index(castValue<bool>(_REF(switchVar)));
      break;
    case Switch::SWT_STRING:
      i = static_cast<SwitchData<string>*>(switchBase)->
          index(*_PString(_REF(switchVar)));
  }
  // execute block
  OArg const block = i == vals.size() ? defBlock : blocks[i];
  if (block) {
    _ASSERT(_REF(block)->getClassId() == CLSID_BLOCK, m);
    const int blockId = g_blockId(_REF(block));
    VirtualMachine blockVM(*this);
    blockVM.execBlock(blockId);
    _RETHROW_HERE_NORET();
    _ASSERT(blockVM.returning() != inBreakCondition() ||
            !inBreakCondition(), m);
    if (blockVM.returning()) { // transfer RES and XF flag to parent machine
      enterReturnCondition();
      RES = blockVM.RES;
    }
    else if (inBreakCondition()) {
      exitBreakCondition();
    }
  }
}

Switch*& VirtualMachine::getSwitch(const MBlock* b, int ip) const {
  Session* const m = _Session;
  long blockId = 0;
  if (b) {
    const int startFrom = b->parentIndex == -1 ? 0 : b->parentIndex;
    blockId = BT->index(b, startFrom);
    _ASSERT(blockId != -1, m);
  }
  MSwitchTable& switchTable = switchTableRef;
  return switchTable[blockId][ip];
}

int s_getCaseType(int classId) {
  int caseType = -1;
  switch (classId) {
    case CLSID_ULONG:
    case CLSID_UINT:   caseType = CLSID_ULONG;  break;
    case CLSID_LONG:
    case CLSID_INT:
    case CLSID_SHORT:
    case CLSID_CHAR:   caseType = CLSID_LONG;   break;
    case CLSID_DOUBLE:
    case CLSID_FLOAT:  caseType = CLSID_DOUBLE; break;
    case CLSID_BOOL:   caseType = CLSID_BOOL;   break;
    case CLSID_STRING: caseType = CLSID_STRING; break;
  }
  return caseType;
}

int s_decideSwitchType(const vector<OArg>& vals) {
  ASSERT(!vals.empty());
  int switchType = -1;
  for (OArg arg : vals) {
    int argType = s_getCaseType(_REF(arg)->getClassId());
    if (argType == -1) return -1; // unexpected switch case type
    if (switchType == -1) switchType = argType;
    if (switchType == argType) continue;
    // switchType != argType
    if (switchType == CLSID_DOUBLE || argType == CLSID_DOUBLE ||
        switchType == CLSID_STRING || argType == CLSID_STRING ||
        switchType == CLSID_BOOL   || argType == CLSID_BOOL)
      return 0; //mismatch
    // both switchType, argType are either CLSID_ULONG or CLSID_LONG
    switchType = CLSID_ULONG;
  }
  return switchType;
}

void VirtualMachine::initSwitch(Switch*& switchBase,
                                ORef switchObj,
                                const vector<OArg>& vals) {
  _ENTER_NOCHK("VirtualMachine::initSwitch");
  Session* const m = _Session;
  int switchType = m->policySwitchTypeFromVar() ?
      s_getCaseType(switchObj->getClassId()) : // decide from var
      s_decideSwitchType(vals); // decide from cases
  if (switchType == -1 || switchType == 0) {
    static const char* msg[2] {
      "unexpected switch case type",
      "switch case type mismatch"
    };
    m->throwRTE(_ox_method_name, msg[switchType + 1]);
    return;
  }
  // do switch initialization
  switch (switchType) {
    case CLSID_ULONG:
      initSwitchData<ulong>(switchBase, vals);
      switchBase->dataType = Switch::SWT_UINT;
      break;
    case CLSID_LONG:
      initSwitchData<long>(switchBase, vals);
      switchBase->dataType = Switch::SWT_INT;
      break;
    case CLSID_DOUBLE:
      initSwitchData<double>(switchBase, vals);
      switchBase->dataType = Switch::SWT_FP;
      break;
    case CLSID_BOOL:
      initSwitchData<bool>(switchBase, vals);
      switchBase->dataType = Switch::SWT_BOOL;
      break;
    case CLSID_STRING:
      initSwitchData<string>(switchBase, vals);
      switchBase->dataType = Switch::SWT_STRING;
      break;
    default:
      _ASSERT(false, m);
  }
  _RETHROW_HERE_NORET();
}

template<typename T>
void VirtualMachine::initSwitchData(Switch*& switchBase,
                                    const vector<OArg>& vals) {
  _ENTER_NOCHK("VirtualMachine::initSwitchData<>");
  Session* const m = _Session;
  vector<T> switchValues;
  for (OArg arg : vals) {
    T value = castValue<T>(_REF(arg));
    _RETHROW_HERE_NORET();
    switchValues.push_back(value);
  }
  SwitchData<T>* switchData = new SwitchData<T>(switchValues);
  switchBase = switchData;
}

//:::::::::::::::::::::::::::::   Flow Control   ::::::::::::::::::::::::::::://

void VirtualMachine::execFlowControl(int n) {
  if (n >= 0) setBreakCondition(n);
}

ORet VirtualMachine::execReturn(OArg arg, bool ref) {
  Session* const m = _Session;

  if (!(BP->type == MBlock::PLAIN ||
        BP->type == MBlock::METHOD ||
        BP->type == MBlock::LOOP)) { // handle error
    const string methodName{"VirtualMachine::execReturn"};
    string msg;
    switch (BP->type) {
      case MBlock::CLASS:
        msg = "return in class definition block"; break;
      case MBlock::LOOP_INIT:
        msg = "return in loop initialization block"; break;
      case MBlock::LOOP_COND:
        msg = "return in loop condition"; break;
      case MBlock::LOOP_STEP:
        msg = "return in loop step block"; break;
      default:
        msg = "unknown block type in return method";
    }
    _Session->throwRTE(methodName, msg);
    return OVar::undefinedResult();
  }

  _ASSERT(XF == M_NORMAL, m);
  _ASSERT(!ref || arg, m);
  if (ref) {
    OVar result = *arg;
    _ASSERT(!arg->disposable() || result.disposable(), _Session);
    if (arg->disposable())
      *arg = OVar{};
    enterReturnCondition();
    return result;
  }
  // we have arg == nullptr when returning without arguments
  if (!arg || arg->undefined()) {
    enterReturnCondition();
    return OVar{};
  }
  OVar rv;
  m->emplace(rv, *arg);
  rv.dispose();
  _RETHROW("VirtualMachine::execReturn");
  enterReturnCondition();
  return rv;
}

ORet g_execReturn(VirtualMachine* vm, OArg arg, bool ref) {
  return vm->execReturn(arg, ref);
}

//::::::::::::::::::::::::::::::   Try-Catch   ::::::::::::::::::::::::::::::://

struct AtExitCatch {
  VirtualMachine& vm;
  bool active;
  AtExitCatch(VirtualMachine& toFinalize) : vm{toFinalize}, active{true} {}
  ~AtExitCatch() {now();}
  void now() {
    if (!active) return;
    vm.setReturnCondition(vm.parentMachine->callStack.top); // finish
    vm.runStdMode();
    active = false;
  }
  void activate(bool on = true) noexcept {active = on;}
};

ORet VirtualMachine::execTryCatch(OArg tryBlock,
                                  int doBlockId,
                                  int catchBlockId) {
  _ENTER_NOCHK("VirtualMachine::execTryCatch");
  Session* const m = _Session;
  _ASSERT(!m->exceptionPending(), m);
  _ASSERT(_REF(tryBlock)->getClassId() == CLSID_BLOCK, m);
  _ASSERT(!m->savedException(), m);

  // execute try block
  const int tryBlockId = g_blockId(_REF(tryBlock));
  // recall that parent/child machines share the same session
  VirtualMachine tryVM(*this);
  tryVM.execBlock(tryBlockId);
  if (m->exceptionPending()) {
    // retrieve exception (this clears the exception from the Session)
    const auto f = SO->getClass()->getMethodAccessAddress(
        "getException", nullptr).second.coreMethodAddress;
    const auto e = (SO->*f)(&SOVAR, 0, nullptr, m);
    if (catchBlockId != -1) {
      // make it parent of the do-block
      (*BT)[doBlockId].parentIndex = catchBlockId;
      // run catch block until the 'leave' instruction
      VirtualMachine catchVM{*this};
      AtExitCatch atExit{catchVM};
      catchVM.execBlockKeepResult(catchBlockId, MBlock::PLAIN, leave);
      m->clearException();
      // check result of catch clause
      auto& r = REF(catchVM.RES);
      const auto eClassId = r ? r->getRootClassId() : -1;
      if (eClassId < CLSID_EXCEPT || eClassId > CLSID_RTE) {
        atExit.now();
        m->clearException();
        m->deleteObject(REF(e));
        m->clearException();
        m->throwRTE("system::try:catch:do:", eClassId == -1 ?
            "unknown type in catch clause" : "incorrect catch clause");
        return OVar::undefinedResult(); // pass it through
      }
      // set exception variable
      if (!REF(e)->typeOf(r->getClass())) {
        // exception escapes handling
        atExit.now();
        m->clearException();
        m->setException(REF(e));
        return OVar::undefinedResult(); // pass it through
      }
      r->getClass()->copyObject(r, REF(e), m);
      if (!m->exceptionPending())
        m->deleteObject(REF(e));
      if (m->exceptionPending()) { // TODO maybe clear the exception and
        atExit.activate(false);    //      finish execution of catchVM
        return OVar::undefinedResult(); // pass it through
      }
      // execute do block
      VirtualMachine doVM(catchVM);
      doVM.execBlock(doBlockId);
      _ASSERT(!m->savedException(), m);
      // if the do block threw an exception
      if (m->exceptionPending()) { // TODO see previous todo
        atExit.activate(false);    //      maybe use a Session policy flag
        return OVar::undefinedResult(); // pass it through
      }
      if (doVM.returning()) {
        // transfer RES and XF flag to parent machine
        enterReturnCondition();
        RES = doVM.RES;
      }
    }
    else {
      // save exception for the script's convenience
      m->saveException(REF(e));
      _ASSERT(!m->exceptionPending(), m);
      // execute do (catch) block
      VirtualMachine doVM(*this);
      doVM.execBlock(doBlockId);
      if (m->exceptionPending()) {
        // an exception was thrown in the do (catch) block - let it pass through
        if (m->savedException()) { // script didn't retrieve the saved exception
          // retrieve the saved exception (that thrown in the try block)
          const auto eTryBlock = (SO->*f)(&SOVAR, 0, nullptr, m);
          _ASSERT(!m->savedException(), m);
          _ASSERT(m->exceptionPending(), m);
          REF(eTryBlock)->getClass()->deleteObject(REF(eTryBlock), m);
        }
        return OVar::undefinedResult(); // pass it through
      }
      else { // everything was fine in the catch block
        // remove saved exception
        m->clearException();
        _ASSERT(!m->savedException(), m);
        _ASSERT(!m->exceptionPending(), m);
        if (doVM.returning()) {
          // transfer RES and XF flag to parent machine
          enterReturnCondition();
          RES = doVM.RES;
        }
      }
    }
  }
  else if (tryVM.returning()) {
    // transfer RES and XF flag to parent machine
    enterReturnCondition();
    RES = tryVM.RES;
  }
  return RES;
}

ORet VirtualMachine::execTryCatch(OArg tryBlock,
                                  const vector<int>& doBlocks,
                                  const vector<int>& catchBlocks) {
  _ENTER_NOCHK("VirtualMachine::execTryCatch");
  Session* const m = _Session;
  _ASSERT(!m->exceptionPending(), m);
  _ASSERT(_REF(tryBlock)->getClassId() == CLSID_BLOCK, m);
  _ASSERT(!m->savedException(), m);

  // execute try block
  const int tryBlockId = g_blockId(_REF(tryBlock));
  // recall that parent/child machines share the same session
  VirtualMachine tryVM{*this};
  tryVM.execBlock(tryBlockId);
  if (m->exceptionPending()) {
    for (size_t i = 0; i < doBlocks.size(); ++i) {
      const int doBlockId = doBlocks[i];
      const int catchBlockId = catchBlocks[i];
      // retrieve exception (this clears the exception from the Session)
      const auto f = SO->getClass()->getMethodAccessAddress(
          "getException", nullptr).second.coreMethodAddress;
      const auto e = (SO->*f)(&SOVAR, 0, nullptr, m);
      // make it parent of the do-block
      (*BT)[doBlockId].parentIndex = catchBlockId;
      // run catch block until the 'leave' instruction
      VirtualMachine catchVM(*this);
      AtExitCatch atExit{catchVM};
      catchVM.execBlockKeepResult(catchBlockId, MBlock::PLAIN, leave);
      m->clearException();
      // check result of catch clause
      auto& r = REF(catchVM.RES);
      const auto eClassId = r ? r->getRootClassId() : -1;
      if (eClassId < CLSID_EXCEPT || eClassId > CLSID_RTE) {
        atExit.now();
        m->clearException();
        m->deleteObject(REF(e));
        m->clearException();
        m->throwRTE("system::try:catch:do:", eClassId == -1 ?
            "unknown type in catch clause" : "incorrect catch clause");
        return OVar::undefinedResult(); // pass it through
      }
      // set exception variable
      if (!REF(e)->typeOf(r->getClass())) {
        atExit.now();
        m->clearException();
        m->setException(REF(e));
        continue;
      }
      r->getClass()->copyObject(r, REF(e), m);
      if (!m->exceptionPending())
        m->deleteObject(REF(e));
      if (m->exceptionPending()) { // TODO maybe clear the exception and
        atExit.activate(false);    //      finish execution of catchVM
        return OVar::undefinedResult(); // pass it through
      }
      // execute do block
      VirtualMachine doVM(catchVM);
      doVM.execBlock(doBlockId);
      _ASSERT(!m->savedException(), m);
      // if the do block threw an exception
      if (m->exceptionPending()) { // TODO see previous todo
        atExit.activate(false);    //      maybe use a Session policy flag
        return OVar::undefinedResult(); // pass it through
      }
      if (doVM.returning()) {
        // transfer RES and XF flag to parent machine
        enterReturnCondition();
        RES = doVM.RES;
      }
      return RES;
    }
  }
  else if (tryVM.returning()) {
    // transfer RES and XF flag to parent machine
    enterReturnCondition();
    RES = tryVM.RES;
  }
  return RES;
}

ORet g_execTryCatch(VirtualMachine* vm, OArg tryBlock,
    const vector<int>& catchBlocks, const vector<int>& doBlocks) {
  return catchBlocks.empty() ?
    vm->execTryCatch(tryBlock, doBlocks.front(), -1) :
    vm->execTryCatch(tryBlock, doBlocks, catchBlocks);
}

inline bool s_disposableBasicTypeProxy(const OVar& var) noexcept {
  return var.disposable() && BTI::isBasicTypeProxy(REF(var));
}

void VirtualMachine::exitCallFrame(ObjectAutoCleaner& stackCleaner) {
#if 0 // enable to display diagnostic information
  /*int bid = 0;
  for (; &(*BT)[bid] != BP && bid < (int)BT->size(); ++bid);
  if (bid == (int)BT->size()) bid = -1;*/
  string ul2hexstr(void*);
  const int bid = getIndexBP();
  _Session->displayMessage("DIAGNOSTIC INFORMATION",
      "from machine "+ul2hexstr(reinterpret_cast<void*>(this))+" "
      "exiting block "+std::to_string(bid));
#endif // 0
  Session* const m = _Session;
  MCallFrame* const curFrame = callStack.top;
  _ASSERT(curFrame, m);
  MCallFrame* const fp = currentFrame();
  _ASSERT_TODO(curFrame == fp, m);

#if 0 // enable to display diagnostic information
  m->displayMessage("info",
      "callStack.top = "+ul2hexstr(curFrame)+" "
      "fp = "+ul2hexstr(fp)+" "
      "callStack.top->prev = "+ul2hexstr(curFrame->prev));
#endif // 0
#if 0
  // delete left-behind temporary variables
  for (const auto& symbol : BP->dataSegment.localSymbolTable) {
    if (!symbol.isTmp()) continue;
    auto& var = getLocalVar(fp, symbol.variableOffset);
    if (var.undefined() || var.isRef()) continue;
    stackCleaner.addDisposable(std::move(var));
  }
  // destroy local vars
  _DEBUG(                                                         \
    const int numLocals = curFrame->localSize();                  \
    _ASSERT(BP->codeSegment[0].opcode != enter ||                 \
            numLocals == BP->codeSegment[0].operand, m);          \
    int numLocals2 = 0;                                           \
    for (const auto& sym : BP->dataSegment.localSymbolTable) {    \
      if (sym.isVariable() && !sym.isStatic() && !sym.isArgRef()) \
        ++numLocals2;                                             \
    }                                                             \
    _ASSERT(numLocals >= numLocals2, m);                          \
  )
  for (OVar& var : curFrame->locals) {
    if (var.undefined() ||
        (var.isRef() && !s_disposableBasicTypeProxy(var)) ||
        var.type == VARTYPE_INT_DATA)
      continue;
    // stackCleaner.add(std::move(var));
    stackCleaner.add(std::exchange(REF(var), nullptr));
  }
#endif // 0
  // clean top stack frame
  cleanStackFrame(stackCleaner, fp);
  // restore machine state
  _ASSERT(BP == curFrame->block, m);
  BP = curFrame->BP;
  IP = curFrame->IP;
  restoreRC(curFrame);
  // clean machine stack
  callStack.pop(m);
  // NOTE: Now the condition BP == curFrame->block may be not valid
  _ASSERT(!returning(), m);
  if (inBreakCondition() || returning()) {
    // pop all unfinished frames
    const MCallFrame* const mostBaseStack =
        (parentMachine ? parentMachine->callStack.top : nullptr);
    while (callStack.top != mostBaseStack && callStack.top->trivial()) {
      // TODO URGENT! curFrame != callStack.top
      // Did you mean callStack.top instead of curFrame here?
      _ASSERT(false, m);
      BP = curFrame->BP;
      IP = curFrame->IP;
      restoreRC(curFrame);
      callStack.pop(m);
    }
  }
}

void VirtualMachine::cleanStackFrame(ObjectAutoCleaner& stackCleaner,
                                     MCallFrame* fp) {
  Session* const m = _Session;
  // delete left-behind temporary variables
  for (const auto& symbol : BP->dataSegment.localSymbolTable) {
    if (!symbol.isTmp()) continue;
    auto& var = getLocalVar(fp, symbol.variableOffset);
    if (var.undefined() || var.isRef()) continue;
    stackCleaner.addDisposable(std::move(var));
  }
  // destroy local vars
  _DEBUG(                                                         \
    const int numLocals = fp->localSize();                        \
    _ASSERT(BP->codeSegment[0].opcode != enter ||                 \
            numLocals == BP->codeSegment[0].operand, m);          \
    int numLocals2 = 0;                                           \
    for (const auto& sym : BP->dataSegment.localSymbolTable) {    \
      if (sym.isVariable() && !sym.isStatic() && !sym.isArgRef()) \
        ++numLocals2;                                             \
    }                                                             \
    _ASSERT(numLocals >= numLocals2, m);                          \
  )
  for (OVar& var : fp->locals) {
    if (var.undefined() ||
        (var.isRef() && !s_disposableBasicTypeProxy(var)) ||
        var.type == VARTYPE_INT_DATA)
      continue;
    stackCleaner.add(std::exchange(REF(var), nullptr));
  }
}

void VirtualMachine::defExceptionHandler(int exitCode) {
  Session* const m = _Session;
  _ASSERT(m->exceptionPending(), m);
  OAddr methodAddress = SO->getClass()->getMethodAccessAddress(
      "getException", nullptr).second;
  const auto& f = methodAddress.coreMethodAddress;
  auto e = (SO->*f)(&SOVAR, 0, nullptr, _Session);
  ExceptionBase* except = REF(e)->getClass()->castException(REF(e));
  const auto line = m->getMainMachine()->LR;
  const string msg{std::move(except->message())};
  const string method = except->getMainFrame().method;
  const string type = except->what; // same as REF(e)->getClassName()
  // delete exception
  m->deleteObject(REF(e));
  if (m->exceptionPending())
    exit(DEFAULT_ERROR_EXIT_CODE);
  // handle the script thrown exception
  if (m->testingMode()) {
    if (!m->isExpectedException(line, type, method, msg))
      exit(EXCEPTION_TEST_FAILED);
  }
  else {
    const string strLine = ", line "+std::to_string(line);
    if (msg.empty())
      m->displayMessage("", "uncaught exception"+strLine);
    else
      m->displayMessage("uncaught exception"+strLine, msg);
    if (exitCode != 0)
      exit(exitCode);
  }
}

int VirtualMachine::defExitHandler() {
  Session* const m = _Session;
  _ASSERT(m->registerObjects(), m);
  _ASSERT(!m->exceptionPending(), m);
  if (RES.undefined() || !m->isObject(REF(RES)) || !m->isValidObject(REF(RES)))
    return 0;
  // try to convert return value into an int value
  auto result = REF(RES)->getClass()->getMethodAccessAddress("int", nullptr);
  if (!result.first || result.second.scriptMethod) return 0;
  const auto& f = result.second.coreMethodAddress;
  auto converted = (REF(RES)->*f)(&RES, 0, nullptr, _Session);
  ObjectAutoCleaner clean{m, REF(converted)};
  // handle error
  if (m->exceptionPending()) {
    OAddr methodAddress = SO->getClass()->getMethodAccessAddress(
        "getException", nullptr).second;
    const auto& f = methodAddress.coreMethodAddress;
    auto e = (SO->*f)(&SOVAR, 0, nullptr, _Session);
    if (!e.undefined() && e.disposable())
      m->deleteObject(REF(e));
    return DEFAULT_ERROR_EXIT_CODE;
  }
  int exitCode = _P(int, REF(converted))->getValue();
  if (!RES.undefined() && RES.disposable()) {
    m->deleteObject(REF(RES));
    if (m->exceptionPending())
      exit(DEFAULT_ERROR_EXIT_CODE);
  }
  if (m->testingMode())
    return m->expectedRetValue(exitCode) ? 0 : RETURN_VALUE_TEST_FAILED;
  return exitCode;
}

//::::::::::::::::::::   Const Variable Initialization   ::::::::::::::::::::://

static ORef s_createConstObject(OVar& var, int classId, Session* m) noexcept {
  ORef o = m->getClass(classId)->createObject(m);
  if (m->exceptionPending()) {
    m->rethrow("s_createConstObject");
    o = nullptr;
  }
  else if (!o) {
    m->throwRTE("s_createConstObject",
        "could not create object type "+m->getClass(classId)->name);
  }
  else {
    REF(var) = o;
    var.type = VARTYPE_OBJECT;
    var.usage = USG_CONST;
  }
  return o;
}

static int s_getIntegerBase(const MSymbol& symbol) noexcept {
  switch (symbol.type & SYMTYPE_FORMAT_MASK) {
    case 0: return 10;
    case SYMTYPE_LITERAL_HEXA: return 16;
    case SYMTYPE_LITERAL_BINARY: return 2;
  }
  return 0;
}

bool VirtualMachine::s_violates(MBlock::Type blockType,
                                SearchRestriction restriction) noexcept {
  switch (restriction) {
    case METHOD_BOUNDARY: return blockType == MBlock::METHOD;
    case CLASS_BOUNDARY:  return blockType == MBlock::CLASS;
    case BLOCK_BOUNDARY:  return true;
  }
  return false;
}

#if 0
int VirtualMachine::initFromHigherBlock() const {
  _DEBUG( Session* m = _Session; const auto opc = BP->codeSegment[IP].opcode; )
  _ASSERT(opc == push || opc == pushrec || opc == pushrz, m);
  _ASSERT(SR != nullptr, m);
  _ASSERT(!SR->isTmp(), m);
  _ASSERT_TODO(!parent() || parent()->block == BP, m);
  // walk up the block inheritance tree to see if it is defined
  // in a parent block
  bool varWasUninitialized = false;
//   constexpr SearchRestriction restriction = METHOD_BOUNDARY;
  constexpr SearchRestriction restriction = NO_RESTRICTIONS;
  bool borderCrossed = s_violates(BP->type, restriction);
  int blockIndex = borderCrossed ? -1 : parent()->block->parentIndex;
  while (blockIndex != -1 && !borderCrossed) {
    MBlock* b = &(*BT)[blockIndex];
    borderCrossed = s_violates(b->type, restriction);
    // exclude class definition blocks? (no! due to static props)
    for (const auto& symbol : b->dataSegment.localSymbolTable) {
      // skip literals, props, tmps, refs, selectors, class vars,
      // blocks and inaccessible local vars
      if (!symbol.isVariable() || symbol.isPropRef() || symbol.isSymRef())
        continue;
      // we postpone our checking of tmps to improve performance
      _ASSERT(!symbol.isLiteral(), m);
      _ASSERT(!symbol.isSelector(), m);
      _ASSERT(symbol.getVariableType() != SYMTYPE_CLASS, m);
      _ASSERT(symbol.getVariableType() != SYMTYPE_BLOCK, m);
      if (symbol.name != SR->name) continue;
      // we have found a parent block containing the symbol...
      if (symbol.access != SYMACC_ALL) // ...but it is inaccessible
        return M_INIT_VAR_NO_ACCESS;
      // if we are here we have a valid symbol in a parent block
      const MSymbolReference::Type symRefType = symbol.isStatic() ?
          MSymbolReference::RT_STATIC : (
            symbol.isArgRef() ? (symbol.variableOffset == -1 ?
              MSymbolReference::RT_REC_REF : MSymbolReference::RT_ARG_REF
            ) : MSymbolReference::RT_LOCAL
          );
      MCallFrame* const base = symRefType == MSymbolReference::RT_STATIC ?
          nullptr : parent()->prev;
      MSymbolReference symbolReference{b, symbol.variableOffset, symRefType};
      OVar* referencedVar = symbolReference.dereference(base);
      if (!referencedVar)
        return M_INIT_VAR_BLOCK_INACTIVE;
      if (referencedVar->undefined()) {
        varWasUninitialized = true;
        break;
      }
#if USE_SYMBOL_REFERENCE_CACHES
      // make current symbol a static reference to this variable
      SR->type |= SYMTYPE_SYM_REF;
      SR->setStatic();
      // add entry to static data segment
      _ASSERT_TODO(!parent() || parent()->block == BP, m);
      auto sr = new MSymbolReference(std::move(symbolReference));
      auto& staticData = parent()->block->dataSegment.staticData;
      staticData.emplace_back(sr, ushort{0});
      _ASSERT(staticData.back().type == VARTYPE_SYM_REF, m);
      SR->variableOffset = staticData.size() - 1;
      _ASSERT(staticData[SR->variableOffset].symRef->dereference(base)
              == referencedVar, m);
#endif // USE_SYMBOL_REFERENCE_CACHES
      // finally push the variable onto the execution stack
      if (BP->codeSegment[IP].opcode == push)
        callStack.top->args.push_back(referencedVar);
      else
        callStack.top->receiver = referencedVar;
#if !USE_SYMBOL_REFERENCE_CACHES
      // update local variable
      OVar& localVar = getLocalVar();
      localVar = *referencedVar;
      // make it a ref to escape destruction
      localVar.usage |= USG_REF;
#endif // USE_SYMBOL_REFERENCE_CACHES
      // finished!
      return M_INIT_VAR_OK;
    }
    blockIndex = b->parentIndex;
  }
  return varWasUninitialized ? M_INIT_VAR_UNINIT : M_INIT_VAR_NOT_FOUND;
  ///// the above code is equivalent to
  const auto [rc, var] = initFromHigherBlock(*SR, NO_RESTRICTIONS);
  if (rc == M_INIT_VAR_OK)
    callStack.top->push(var, BP->codeSegment[IP].opcode == push);
  return rc;
  ///// end
}
#endif // 0

std::tuple<int,OVar*> VirtualMachine::initFromHigherBlock(
    MSymbol &symbol, SearchRestriction restriction) const {
  _DEBUG( Session* m = _Session; )
  _ASSERT(!symbol.isTmp(), m);
  _ASSERT_TODO(!parent() || parent()->block == BP, m);
  // walk up the block inheritance tree to see if symbol
  // is defined in a parent block
  bool varWasUninitialized = false;
  bool borderCrossed = s_violates(BP->type, restriction);
  int blockIndex = borderCrossed ? -1 : parent()->block->parentIndex;
  while (blockIndex != -1 && !borderCrossed) {
    MBlock* b = &(*BT)[blockIndex];
    borderCrossed = s_violates(b->type, restriction);
    // exclude class definition blocks? (no! due to static props)
    for (const auto& upSym : b->dataSegment.localSymbolTable) {
      // skip literals, props, tmps, refs, selectors, class vars,
      // blocks and inaccessible local vars
      if (!upSym.isVariable() || upSym.isPropRef() || upSym.isSymRef())
        continue;
      // we postpone our checking of tmps to improve performance
      _ASSERT(!upSym.isLiteral(), m);
      _ASSERT(!upSym.isSelector(), m);
      _ASSERT(upSym.getVariableType() != SYMTYPE_CLASS, m);
      _ASSERT(upSym.getVariableType() != SYMTYPE_BLOCK, m);
      if (upSym.name != symbol.name) continue;
      // we have found a parent block containing the symbol...
      if (upSym.access != SYMACC_ALL) // ...but it is inaccessible
        return { M_INIT_VAR_NO_ACCESS, nullptr };
      // now we have a valid symbol in a parent block
      const MSymbolReference::Type symRefType = upSym.isStatic() ?
          MSymbolReference::RT_STATIC : (
            upSym.isArgRef() ? (upSym.variableOffset == -1 ?
              MSymbolReference::RT_REC_REF : MSymbolReference::RT_ARG_REF
            ) : MSymbolReference::RT_LOCAL
          );
      MCallFrame* const base = symRefType == MSymbolReference::RT_STATIC ?
          nullptr : parent()->prev;
      MSymbolReference symbolReference{b, upSym.variableOffset, symRefType};
      OVar* referencedVar = symbolReference.dereference(base);
      if (!referencedVar)
        return { M_INIT_VAR_BLOCK_INACTIVE, nullptr };
      if (referencedVar->undefined()) {
        varWasUninitialized = true;
        break;
      }
#if USE_SYMBOL_REFERENCE_CACHES
      // make current symbol a static reference to this variable
      symbol.type |= SYMTYPE_SYM_REF;
      symbol.setStatic();
      // add entry to static data segment
      _ASSERT_TODO(!parent() || parent()->block == BP, m);
      auto sr = new MSymbolReference(std::move(symbolReference));
      auto& staticData = parent()->block->dataSegment.staticData;
      staticData.emplace_back(sr, ushort{0});
      _ASSERT(staticData.back().type == VARTYPE_SYM_REF, m);
      symbol.variableOffset = staticData.size() - 1;
      _ASSERT(staticData[symbol.variableOffset].symRef->dereference(base)
              == referencedVar, m);
#endif // USE_SYMBOL_REFERENCE_CACHES
#if !USE_SYMBOL_REFERENCE_CACHES
      // update local variable
      OVar& localVar = getLocalVar();
      localVar = *referencedVar;
      // make it a ref to escape destruction
      localVar.usage |= USG_REF;
#endif // USE_SYMBOL_REFERENCE_CACHES
      // finished!
      return { M_INIT_VAR_OK, referencedVar };
    }
    blockIndex = b->parentIndex;
  }
  const int rc = varWasUninitialized ? M_INIT_VAR_UNINIT : M_INIT_VAR_NOT_FOUND;
  return { rc, nullptr };
}

static void s_initLiteral(const MSymbol& symbol, OVar& var, Session* m) {
  LIT_TYPE_SWITCH_TABLE(1);
  const char* value = symbol.name.c_str();
  ORef& o = REF(var);
  bool created = true;
  SWITCH (1, symbol.getLiteralType() >> 4)
    CASE (1, NO_LITERAL): // this should never happen!!!
      _ASSERT(false, m);
      created = false;
      BREAK(1);

    CASE (1, ULONG):
      if (s_createConstObject(var, CLSID_ULONG, m))
        *_PULONG(o) = strtoul(value, NULL, s_getIntegerBase(symbol));
      BREAK(1);

    CASE (1, LONG):
      if (s_createConstObject(var, CLSID_LONG, m))
        *_PLONG(o) = strtol(value, NULL, s_getIntegerBase(symbol));
      BREAK(1);

    CASE (1, UINT):
      if (s_createConstObject(var, CLSID_UINT, m))
        *_PUINT(o) = (uint)strtoul(value, NULL, s_getIntegerBase(symbol));
      BREAK(1);

    CASE (1, INT):
      if (s_createConstObject(var, CLSID_INT, m))
        *_PINT(o) = (int)strtol(value, NULL, s_getIntegerBase(symbol));
      BREAK(1);

    CASE (1, SHORT):
      if (s_createConstObject(var, CLSID_SHORT, m))
        *_PSHORT(o) = (short)strtol(value, NULL, s_getIntegerBase(symbol));
      BREAK(1);

    CASE (1, CHAR):
      if (s_createConstObject(var, CLSID_CHAR, m)) {
        const int base = s_getIntegerBase(symbol);
        if (base == 16 || base == 2)
          *_PCHAR(o) = (char)strtol(value, NULL, base);
        else
          *_PCHAR(o) = value[0];
      }
      BREAK(1);

    CASE (1, DOUBLE):
      if (s_createConstObject(var, CLSID_DBL, m))
        *_PDBL(o) = strtod(value, NULL);
      BREAK(1);

    CASE (1, FLOAT):
      if (s_createConstObject(var, CLSID_FLT, m))
        *_PFLT(o) = strtof(value, NULL);
      BREAK(1);

    CASE (1, STRING):
      if (s_createConstObject(var, CLSID_STRING, m))
        *_PStr(o) = symbol.name;
      BREAK(1);

    CASE (1, BOOL):   // no bool literal
    CASE (1, PTR):    // no pointer literal
    CASE (1, SEL):    // no selector literal
    CASE (1, ID):     // no object id literal
    CASE (1, ERROR1): // undefinded type value
    CASE (1, ERROR2): // undefinded type value
      created = false;
      BREAK(1);
  END_SWITCH(1);
  if (created)
    var.setLiteral();
}

void VirtualMachine::initLiteral(OVar& var) const {
  s_initLiteral(*SR, var, _Session);
}

//============================================================================//
//::::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://
//============================================================================//

bool VirtualMachine::assignReference(OVar& lhs, const OArg rhs, bool makeConst) {
  const bool rhsIsBasicTypeProxy = BTI::isBasicTypeProxy(_REF(rhs));
  if (rhs->disposable() && !rhsIsBasicTypeProxy)
    return false;
  // rhs is not disposable or it is a basic type proxy ref
  lhs.objectRef = rhs->objectRef;
  lhs.setRef();
  if (makeConst || rhs->isConst())
    lhs.setConst();
  else if (lhs.isConst())
    lhs.usage &= ~USG_CONST;
  _ASSERT(!rhs->disposable() || !rhs->isRef(), _Session)
  if (rhs->disposable() && rhsIsBasicTypeProxy) {
    MCallFrame* f = callStack.top->searchParentFramesForLocal(rhs);
    if (!f) return false;
    f->autoCleanTmp(rhs);
  }
  return true;
}

bool g_deferTmpDeletion(const VirtualMachine* vm, OVar* rhs) {
  MCallFrame* f = vm->callStack.top->searchParentFramesForLocal(rhs);
  if (!f) return false;
  f->autoCleanTmp(rhs);
  return true;
}

void VirtualMachine::deleteResult() {
  if (!RES.disposable()) return;
//   RES.undispose();
  if (!RES.undefined())
    static_cast<Session*>(_Session)->deleteObject(REF(RES));
  RES = OVar{};
}

bool VirtualMachine::acceptVIASA(const std::string& methodName, int argIndex) {
  for (const auto& i : tableVIASArgs)
    if (i.methodName == methodName &&
        std::find(i.args.begin(), i.args.end(), argIndex) != i.args.end())
      return true;
  return false;
}

const OClass* VirtualMachine::getCaller() const {
  _DEBUG( Session* const m = _Session; )
  _ASSERT_TODO(!parent() || parent()->block == BP, m);
  const auto frame = parent();
  return frame ? frame->block->getOwnerClass(*BT) : nullptr;
}

int VirtualMachine::getIndexBP() const noexcept {
  for (size_t bid = 0; bid < BT->size(); ++bid)
    if (&(*BT)[bid] == BP) return (int)bid;
  return -1;
}

string ul2hexstr(void* p);

string VirtualMachine::callStackRundown() const {
  string out = "\n";
  int i = 0;
  auto p = callStack.top;
  do {
    out += "#"+std::to_string(i)+" frame "+ul2hexstr(p);
    if (!p) break;
    out += ", block ";
    if (p->block) {
      bool found = false;
      for (size_t blockId = 0; blockId < BT->size(); ++blockId) {
        if (&(*BT)[blockId] == p->block) {
          out += std::to_string(blockId);
          out += ", type ";
          switch (p->block->type) {
            case MBlock::PLAIN : out += "PLAIN" ; break;
            case MBlock::LOOP  : out += "LOOP"  ; break;
            case MBlock::METHOD: out += "METHOD"; break;
            case MBlock::CLASS : out += "CLASS" ; break;
            default            : out += "?"     ; break;
          }
          found = true;
          break;
        }
      }
      if (!found) out += "not found";
    }
    else {
      out += "null";
    }
    out += "\n";
    p = p->prev;
  }
  while (true);
  return out;
}

std::tuple<OVar*, int, const string&>
VirtualMachine::resolveSymbolType(int symbolIndex, int what) const {
  return resolveSymbolType(BP->dataSegment.localSymbolTable[symbolIndex], what);
}

std::tuple<OVar*, int, const string&>
VirtualMachine::resolveSymbolType(MSymbol& symbol, int what) const {
  _DEBUG( Session* const m = _Session; )

  if ((symbol.isLiteral() || !symbol.isVariable()) && what < 10)
    what = -1;

  switch (what) {
    case RSYM_LOCAL_DEF: /* local */ {
      if ((symbol.type & SYMTYPE_USAGE_MASK)) break;
      const auto frame = parent();
      _ASSERT(frame, m);
      return {&frame->locals[symbol.variableOffset], -1, symbol.name};
    }

    case RSYM_STATIC_DEF: /* static */ {
      if (symbol.type & (SYMTYPE_USAGE_MASK & ~SYMTYPE_STATIC))
        break;
      auto& staticData = BP->dataSegment.staticData;
      if (symbol.definitionIP == -2) {
        return {&staticData[symbol.variableOffset], -2, symbol.name};
      }
      else if (symbol.definitionIP == IP) {
        _ASSERT(IP != -1, m);
        symbol.definitionIP = -2;
        return {&staticData[symbol.variableOffset], IP, symbol.name};
      }
      else if (symbol.definitionIP == -1) {
        // check if symbol is already defined
        bool varIsDefined = symbol.isStatic();
        if (!varIsDefined) {
          OVar* var = resolveSymbol(symbol, BLOCK_BOUNDARY);
          if (var && !var->undefined() && var->type != VARTYPE_INT_DATA)
            varIsDefined = true;
        }
        if (varIsDefined)
          return {nullptr, -1, symbol.name};
        // add new static variable
        staticData.push_back(new OVariable{});
        // modify symbol data
        symbol.setStatic();
        symbol.variableOffset = staticData.size() - 1;
        symbol.definitionIP = IP;
        return {&staticData.back(), -1, symbol.name};
      }
      else { // the test `symbol.definitionIP == IP` failed
        return {nullptr, -1, symbol.name};
      }
    }

    case RSYM_ANY:
      return {resolveSymbol(symbol), -1, symbol.name};
  }

  return {nullptr, -1, symbol.name};
}

std::tuple<OVar*, int, const string&>
g_resolveSymbolType(VirtualMachine* vm, int symbolIndex, int what)
{ return vm->resolveSymbolType(symbolIndex, what); }

MReqValue g_getMachineValue(VirtualMachine* vm, MParam which) {
  SWITCH_TABLE(0, 0)
    DEFINE_CASE(0, M_REGISTER_RES)
    DEFINE_CASE(0, M_IS_RETURNING)
    DEFINE_CASE(0, M_CALLER_CLASS)
    DEFINE_CASE(0, M_THIS)
    DEFINE_CASE(0, M_RECEIVER)
    DEFINE_CASE(0, M_RECEIVER_VAR_NAME)
    DEFINE_CASE(0, M_METHOD_NAME)
    DEFINE_CASE(0, M_REF_RESULT)
    DEFINE_CASE(0, M_ARG_0)
    DEFINE_CASE(0, M_REGISTER_IP)
    DEFINE_CASE(0, M_REGISTER_LR)
  END_SWITCH_TABLE(0)

  MReqValue reqValue;
  Session* const m = _PRTMgr(vm->SO);

  SWITCH (0, which)
    CASE(0, M_REGISTER_RES):
      reqValue.set<OVar>(vm->RES);
      _ASSERT(reqValue.is<OVar>(), m);
      BREAK(0);

    CASE(0, M_IS_RETURNING):
      reqValue.set<bool>(vm->returning());
      _ASSERT(reqValue.is<bool>(), m);
      BREAK(0);

    CASE(0, M_CALLER_CLASS):
      reqValue.set<void*>(const_cast<OClass*>(vm->getCaller()));
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);

    CASE(0, M_THIS):
      reqValue.set<void*>(vm->varThis());
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);

    CASE(0, M_RECEIVER): {
      MCallFrame* const fp = vm->callStack.top;
      if (MCallFrame* const pfp = fp->prev; pfp && pfp->referenceResult &&
          _REF(pfp->referenceResult) == _REF(fp->receiver))
        reqValue.set<void*>(pfp->referenceResult);
      else
        reqValue.set<void*>(fp->receiver);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);
    }

    CASE(0, M_ARG_0): {
      MCallFrame* const fp = vm->callStack.top;
      if (MCallFrame* const pfp = fp->prev; pfp && pfp->referenceResult &&
          fp->args.size() && _REF(pfp->referenceResult) == _REF(fp->args[0]))
        reqValue.set<void*>(pfp->referenceResult);
      else
        reqValue.set<void*>(fp->args[0]);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);
    }

#if 0
    CASE(0, M_TMP_RECEIVER): {
      OVar* filtered{nullptr};
      OVar* receiver = vm->callStack.top->receiver;
      const MCallFrame* const p = vm->parent();
      for (const auto& symbol : p->block->dataSegment.localSymbolTable)
        if (symbol.isTmp() && !symbol.isLiteral() &&
            REF(p->locals[symbol.variableOffset]) == _REF(receiver)) {
          filtered = receiver;
          break;
        }
      reqValue.set<void*>(filtered);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);
    }
#endif // 0

    CASE(0, M_RECEIVER_VAR_NAME): {
      string* result{nullptr};
      OVar* receiver = vm->callStack.top->receiver;
      const MCallFrame* const p = vm->parent();
      if (p && p->block) {
        const string varName = vm->getVariableName(*p->block, _REF(receiver));
        if (!varName.empty())
          result = new string{varName};
      }
      reqValue.set<void*>(result);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);
    }

    CASE(0, M_METHOD_NAME): {
      MCallFrame* const fp = vm->callStack.top;
      reqValue.set<void*>(fp ? (void*)(&fp->methodName()) : nullptr);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);
    }

    CASE(0, M_REF_RESULT):
      reqValue.set<void*>(&vm->callStack.top->prev->referenceResult);
      _ASSERT(reqValue.is<void*>(), m);
      BREAK(0);

    CASE(0, M_REGISTER_IP):
      reqValue.set<decltype(VirtualMachine::IP)>(vm->IP);
      _ASSERT(reqValue.is<decltype(VirtualMachine::IP)>(), m);
      BREAK(0);

    CASE(0, M_REGISTER_LR):
      reqValue.set<decltype(VirtualMachine::LR)>(m->getMainMachine()->LR); // TODO ???
      _ASSERT(reqValue.is<decltype(VirtualMachine::LR)>(), m);
      BREAK(0);
  END_SWITCH(0)

  return reqValue;
}

bool VirtualMachine::isLocalVar(OVar* v, const MCallFrame* f) const noexcept {
  const auto& locals = (f ?: parent())->locals;
  return !(locals.empty() || v < &*locals.begin() || v >= &*locals.end());
}

bool g_isLocalVar(const VirtualMachine* vm, OVar* var) noexcept {
  return vm->isLocalVar(var, nullptr);
}

bool VirtualMachine::isArgument(OVar* v, const MCallFrame* f) const noexcept {
  const auto& args = (f ?: parent())->args;
  return std::find(args.begin(), args.end(), v) != args.end();
}

bool g_isArgument(const VirtualMachine* vm, OVar* var) noexcept {
  return vm->isArgument(var, nullptr);
}

OVar* VirtualMachine::resolveSymbol(MSymbol& symbol,
                                    SearchRestriction restriction) const {
  _DEBUG( Session* const m = _Session; )
  SWITCH_TABLE_VAR_TYPES(3);

  SWITCH (3, VAR_USAGE(symbol.type))
    CASE (3, VU_VAR): {
      auto& var = parent()->locals[symbol.variableOffset];
      if (!var.undefined() || var.isRef())
        return &var;
      // check if variable is a property of this object
      if (const auto [methodFrame, owner, offset, access] =
          getDataIfProp(symbol.name); methodFrame) {
        // it is a property of this object
        const ORef rec = _REF(methodFrame->receiver);
        // if it is accessible...
        if (!g_private(access) || rec->getClass() == owner) {
          symbol.type |= SYMTYPE_PROP_REF;
          symbol.variableOffset = offset;
          return &rec->property(offset);
          // Note: rec == methodFrame->receiver->objectRef == refThis()
        }
        else
          return &var;
      }
      // not a property; try to resolve symbol from a parent block
      else {
        const auto [ec, foundVar] = initFromHigherBlock(symbol, restriction);
        return ec == M_INIT_VAR_OK ? foundVar : &var;
      }
    }

    CASE (3, VU_STATIC): {
      _ASSERT(!symbol.isSymRef(), m);
      OVar* var = &BP->dataSegment.staticData[symbol.variableOffset];
      if (var->undefined()) {
        if (symbol.isLiteral())
          s_initLiteral(symbol, *var, _Session);
        else if (symbol.isBlockId()) {
          Session* const m = static_cast<Session*>(_Session);
          _REF(var) = m->getClass(CLSID_BLOCK)->createObject(m);
          if (m->exceptionPending())
            return nullptr;
          g_refBlockId(_REF(var)) = symbol.getBlockId();
        }
      }
      return var;
    }

    CASE (3, VU_ARG_REF):
    CASE (3, VU_STATIC_ARG_REF): {
      MCallFrame* frame = getParentMethodFrame();
      _ASSERT(frame && frame->block == BP->getParentMethodBlock(*BT), m);
      const auto ofs = symbol.variableOffset;
      return ofs == -1 ? frame->receiver : frame->args[ofs];
    }

    CASE (3, VU_PROP_REF):
      _ASSERT(getParentMethodFrame() &&
          getParentMethodFrame()->block == BP->getParentMethodBlock(*BT), m);
      return &refThis()->property(symbol.variableOffset);

    CASE (3, VU_STATIC_PROP_REF):
      _ASSERT(false, m);
      BREAK(3);

    CASE (3, VU_SYM_REF):
    CASE (3, VU_STATIC_SYM_REF): {
      _ASSERT(symbol.isStatic(), m);
      const auto& staticData = BP->dataSegment.staticData;
      const auto& symRef = *staticData[symbol.variableOffset].symRef;
      return symRef.dereference(parent()->prev);
    }

    CASE (3, _6):
    CASE (3, _7):
    CASE (3, _10):
    CASE (3, _11):
    CASE (3, _12):
    CASE (3, _13):
    CASE (3, _14):
    CASE (3, _15):
  END_SWITCH (3)

  return nullptr;
}

bool VirtualMachine::isUndefindedReceiver(const OVar& var, const string& what) {
  const auto _Operation = BP->codeSegment[IP].opcode;
  if (_Operation == pushrec && var.undefined()) {
    const string msg = "undefined "+what+" used as receiver: '"+SR->name+"'";
    Session* const m = _PRTMgr(SO);
    m->throwRTE(parent() ? parent()->getMethodName() : "main", msg);
    return true;
  }
  return false;
}

void VirtualMachine::error(const string& msg) {
  const MCallFrame* p = parent();
  const string methodName{p ? p->getMethodName() : "main"};
  _Session->throwRTE(methodName, msg, "", LR);
}

void VirtualMachine::errorNoProp(const string& propName, const string& type) {
  const auto& symtab = BP->dataSegment.localSymbolTable;
  OClass* const thisType = symtab[0].name != "this" ||
                           symtab[0].variableOffset != -1 ||
                           !parent() ?
      nullptr : _REF(parent()->receiver)->getClass();
  const string msg = thisType == SO->getClass() ?
    "'this' argument is required to access property '"+propName+"' from lambda" :
    "'"+propName+"' is not a property of '"+type+"'";
  error(msg);
}

string VirtualMachine::getVariableName(const MBlock& block, ORef o) const {
  for (const auto& symbol : block.dataSegment.localSymbolTable) {
    if (symbol.isLiteral() || symbol.isSelector() ||
        symbol.getVariableType() == SYMTYPE_BLOCK ||
        symbol.isPropRef() || symbol.isArgRef() ||
        symbol.isSymRef()  || symbol.isTmp())
      continue;
    OVar* var = resolveSymbol(const_cast<MSymbol&>(symbol), BLOCK_BOUNDARY);
    if (var && _REF(var) == o)
      return symbol.name;
  }
  return string{};
}

//============================================================================//
//:::::::::::::::::   Class Registration And Construction   :::::::::::::::::://
//============================================================================//

void VirtualMachine::registerClassesInBlock() const {
  struct RegisterClassAction {
    typedef i1::TreeNode<MBlock::InheritanceData> node_t;
    Session* m;
    RegisterClassAction(Session* m) : m{m} {}
    bool operator()(node_t* node) {
      const string& typeName = node->data().name;
      if (typeName != "*") {
#if 0 // TODO make it a log message
        std::cout << "create type " << typeName << " extending "
                  << node->data().base;
#endif
        const auto c = m->createClassObject(typeName, node->data().base);
        if (m->exceptionPending() || !c)
          return false;
        if (m->importing())
          m->getPackageInfo().registeredTypes.emplace_back(typeName, c);
#if 0 // TODO make it a log message
        if (m->importing()) {
          string packageName = m->getPackageInfo().typePrefix;
          packageName.pop_back();
          std::cout << " for package " << packageName << std::endl;
        }
#endif
      }
      return true;
    }
  };

  Session* m = _Session;
  auto& classTree = BP->classInheritanceTree;
  RegisterClassAction registerClass(m);
  auto rv = classTree.walkDownWhile(registerClass);
  if (m->exceptionPending())
    m->rethrow("VirtualMachine::registerClassesInBlock");
  else if (rv != nullptr)
    m->throwRTE("VirtualMachine::registerClassesInBlock",
        "could not create class object '"+rv->data().name+"'");
}

ORet VirtualMachine::execDefineClass(OClass* c, int blockId) {
  VirtualMachine vm(*this);
  vm.init(blockId);
  return vm.defineClass(c);
}

ORet VirtualMachine::defineClass(OClass* c) {
  _ENTER_NOCHK("VirtualMachine::defineClass");
  Session* const m = _Session;
  MCallFrame* const returnCondition = returnPoint.endFrame;
  BP->type = MBlock::CLASS;
  BP->definedClass = c;
  int ip = 0;
  if (BP->codeSegment[0].opcode != enter) {
    // find enter instruction
    for (const auto& instruction : BP->codeSegment)
      if (instruction.opcode == enter) break;
      else ++ip;
    if (ip == BP->codeSegment.size())
      _THROW_RTE("incorrect class definition block (missing enter instruction)")
  }
  setReturnCondition(ip + 1);
  _ASSERT(RC == M_IP, m);
  // run machine until after it has executed the enter instruction
  runStdMode();
  // set 'this'
  OClass* classObjFactory = m->classMgr->getClass("class object");
  if (!classObjFactory) {
    classObjFactory = g_installClassObjectClass(m);
    if (!classObjFactory)
      _THROW_RTE("could not create class 'class object'")
  }
  oxClassObject classObj{classObjFactory};
  classObj.classPointer = c;
  OVar var(&classObj, USG_REF); // must be ref to escape destruction
  _ASSERT(BP->dataSegment.localSymbolTable[0].name == "this", m);
  const auto ofs = BP->dataSegment.localSymbolTable[0].variableOffset;
  callStack.top->locals[ofs] = var;
  // finish machine
  setReturnCondition(returnCondition);
  runStdMode();
  deleteResult();
  return OVar::undefinedResult();
}

void VirtualMachine::setMethodArgNames(int blockId, vector<string>& argNames) {
  MBlock& block = (*BT)[blockId];
  try {
    block.dataSegment.checkMethodArgNames(argNames);
  }
  catch (const string& msg) {
    _Session->throwRTE("VirtualMachine::setMethodArgNames", msg);
    return;
  }
  block.setMethodArgNames(argNames);
}

//============================================================================//
//:::::::::::::::::::::::::::   Package Handling   ::::::::::::::::::::::::::://
//============================================================================//

int VirtualMachine::loadPackage(const string& filePath) {
  Session* const m = _Session;
  i1::vector<MBlock> packageBlockTable;
  CompilePackageData compilationData{packageBlockTable, m->parseOptions,
      m->generateAssembly(), m->testingMode(), m->verboseOutput(),
      m->getParseExceptionToIgnore()
  };
  if (g_compilePackage(filePath, compilationData) != COMPILE_PACKAGE_OK) {
    const string msg{"package "+filePath+": "+compilationData.message};
    m->throwRTE("VirtualMachine::loadPackage", msg);
    return -1;
  }
  // link package
  const string totalTypePrefix{std::move(m->getTypePrefix())};
  const int parentBlockIndex = BT->index(BP);
  const int base = static_cast<int>(BT->size() - 1);
  for (size_t i = 1; i < packageBlockTable.size(); ++i) {
    auto& block = packageBlockTable[i];
    if (block.parentIndex == 0)
      block.parentIndex = parentBlockIndex;
    else
      block.parentIndex += base;
    // link data segment
    for (auto& symbol : block.dataSegment.localSymbolTable) {
      if (symbol.getVariableType() != SYMTYPE_BLOCK)
        continue;
      const int blockIndex = symbol.getBlockId();
      _ASSERT(blockIndex != -1, m);
      symbol.name = '@' + std::to_string(blockIndex + base);
    }
    // link code
    for (auto& instruction : block.codeSegment) {
      if (instruction.opcode == execb/* || instruction.opcode == rbc*/)
        instruction.operand += base;
    }
    // make class names local to package
    struct LocalizeTypeNamesAction {
      typedef i1::TreeNode<MBlock::InheritanceData> node_t;
      Session* m;
      const string& prefix;
      LocalizeTypeNamesAction(Session* m, const string& prefix) :
          m{m}, prefix{prefix} {}
      void operator()(node_t* node) {
        auto& inheritanceData = node->data();
        if (inheritanceData.name == "*") return;
        inheritanceData.name = prefix + inheritanceData.name;
        if (!inheritanceData.base.empty() &&
            !m->isBuiltinType(inheritanceData.base))
          inheritanceData.base = prefix + inheritanceData.base;
      }
    };
    LocalizeTypeNamesAction localizeTypeNames{m, totalTypePrefix};
    block.classInheritanceTree.walkDown(localizeTypeNames);
    if (m->exceptionPending()) {
      m->rethrow("VirtualMachine::loadPackage");
      return -1;
    }
    // move block to main program block table
    BT->push_back(std::move(block));
    _ASSERT(block.classInheritanceTree.getRoot() == nullptr, m);
  }
  return base + 1;
}

int g_loadPackage(Session* m, const string& filePath) {
  return m->getCurrentMachine()->loadPackage(filePath);
}

std::pair<int, string> VirtualMachine::exportVars(int blockId,
    vector<std::pair<string, ORef>>& varImports) {
  _ENTER_NOCHK("VirtualMachine::exportVars");
  Session* const m = _Session;
  auto& block = (*BT)[blockId];
  for (const auto& symbol : block.dataSegment.localSymbolTable) {
    if (symbol.isLiteral() || symbol.isSelector() ||
        symbol.getVariableType() == SYMTYPE_BLOCK ||
        symbol.isPropRef() || symbol.isArgRef() ||
        symbol.isSymRef()  || symbol.isTmp())
      continue;
    const auto VE = varImports.end();
    auto it = varImports.begin();
    for (; it != VE; ++it) {
      if (it->second || it->first != symbol.name)
        continue;
      const bool staticVar = symbol.isStatic();
      const auto ofs = symbol.variableOffset;
      OVar& var = staticVar ? getStaticVar(&block, ofs) : getLocalVar(ofs);
      if (var.undefined() || var.isRef()) {
        const string affix = staticVar ? "static " : "";
        const string msg = var.isRef() ? "cannot export reference '" :
            "cannot export uninitialized "+affix+"variable '";
        m->throwRTE(_ox_method_name, msg+symbol.name+"'");
        return {
            var.isRef() ? EXPORT_PACKAGE_REF : (
            staticVar   ? EXPORT_PACKAGE_UNINIT_STATIC :
                          EXPORT_PACKAGE_UNINIT_LOCAL), symbol.name};
      }
      else {
        auto& varName = it->first;
        if (var.isConst())
          varName = '!' + varName;
        std::swap(it->second, REF(var));
        _ASSERT(var.undefined(), m);
      }
      break;
    }
  }
  for (const auto& [varName, object] : varImports)
    if (!object) return {EXPORT_PACKAGE_VAR_NOT_FOUND, varName};
  return {EXPORT_PACKAGE_OK, ""};
}

std::pair<int, string> VirtualMachine::exportAllVars(int blockId,
    vector<std::pair<string, ORef>>& varImports) {
  _ENTER_NOCHK("VirtualMachine::exportAllVars");
  Session* const m = _Session;
  _ASSERT(varImports.empty(), m);
  auto& block = (*BT)[blockId];
  for (const auto& symbol : block.dataSegment.localSymbolTable) {
    if (symbol.isLiteral() || symbol.isSelector() ||
        symbol.getVariableType() == SYMTYPE_BLOCK ||
        symbol.isPropRef() || symbol.isArgRef() ||
        symbol.isSymRef()  || symbol.isTmp())
      continue;
    const auto ofs = symbol.variableOffset;
    _ASSERT(ofs >= 0, m);
    OVar& var = symbol.isStatic()? getStaticVar(&block, ofs) : getLocalVar(ofs);
    if (var.undefined() || var.isRef())
      continue;
    string varName = var.isConst() ? '!' + symbol.name : symbol.name;
    varImports.emplace_back(std::move(varName), nullptr);
    std::swap(varImports.back().second, REF(var));
    _ASSERT(var.undefined(), m);
  }
  return {EXPORT_PACKAGE_OK, ""};
}

std::pair<int, string> g_exportVars(Session* m, int blockId,
    vector<std::pair<string, ORef>>& varImports) {
  VirtualMachine* const vm = m->getCurrentMachine();
  return varImports.empty() ? vm->exportAllVars(blockId, varImports) :
                              vm->exportVars(blockId, varImports);
}
