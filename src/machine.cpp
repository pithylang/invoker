#include "util.hpp"
#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "otype.hpp"
#include "otypeProxy.hpp"
#include "ostring.hpp"
#include "oexcept.hpp"
#include "vm.hpp"
// #include "vmdebugger.hpp"
#include "fswitch.h"

#define _Instruction (  BP->codeSegment[IP]   )
#define _Operation   (  _Instruction.opcode   )
#define _Operand     (  _Instruction.operand  )
#define _Session     (  static_cast<Session*>(_PRTMgr(SO)) )
#define _SymbolTable (  BP->dataSegment.localSymbolTable   )
#define SET_SR()     SR = &_SymbolTable[_Operand]
#define _REF(varPtr) (varPtr)->objectRef

#define _ASSERT_TODO(condition, session) _ASSERT(condition, session)

#ifdef MAKING_DIAGNOSTIC_MACHINE
#  define _DIAGNOSTIC_MACHINE true
#else
#  define _DIAGNOSTIC_MACHINE false
#endif // MAKING_DIAGNOSTIC_MACHINE

# define VAR_CATEGORY_SWITCH_TABLE(TABLE_ID)  \
    SWITCH_TABLE(TABLE_ID, 0)                 \
      DEFINE_CASE(TABLE_ID, SYMTYPE_UNKNOWN)  \
      DEFINE_CASE(TABLE_ID, SYMTYPE_VARIABLE) \
      DEFINE_CASE(TABLE_ID, SYMTYPE_SELECTOR) \
      DEFINE_CASE(TABLE_ID, SYMTYPE_BLOCK)    \
      DEFINE_CASE(TABLE_ID, SYMTYPE_CLASS)    \
    END_SWITCH_TABLE(TABLE_ID)

using std::string;

string ul2hexstr(ulong);

struct SetCurrentMachine {
  Session* m_;
  VirtualMachine* prev_;
  SetCurrentMachine(Session* m, VirtualMachine* cur) :
      m_(m), prev_(m->getCurrentMachine()) {m_->setCurrentMachine(cur);}
  ~SetCurrentMachine() {m_->setCurrentMachine(prev_);}
};

void VirtualMachine::runStdMode() {
  _ASSERT(!(RC == M_OPCODE && _Operation == returnPoint.endOpcode) &&
          !(RC == M_IP && IP >= returnPoint.endIP), _PRTMgr(SO));

  SWITCH_TABLE(0, nop)
    DEFINE_CASE(0, nop)
    DEFINE_CASE(0, setl)
    DEFINE_CASE(0, setf)
    DEFINE_CASE(0, push)
    DEFINE_CASE(0, pushso)
    DEFINE_CASE(0, pushrz)
    DEFINE_CASE(0, exec)
    DEFINE_CASE(0, execb)
    DEFINE_CASE(0, execlb)
    DEFINE_CASE(0, execgp)
    DEFINE_CASE(0, execcb)
    DEFINE_CASE(0, stor)
    DEFINE_CASE(0, enter)
    DEFINE_CASE(0, leave)
    DEFINE_CASE(0, jmp)
    DEFINE_CASE(0, dto)
    DEFINE_CASE(0, dtor)
    DEFINE_CASE(0, rop)
    DEFINE_CASE(0, chkrv)
    DEFINE_CASE(0, lres)
    DEFINE_CASE(0, pushra)
    DEFINE_CASE(0, mref)
    DEFINE_CASE(0, pushrec)
    DEFINE_CASE(0, pushsel)
    DEFINE_CASE(0, pushb)
    DEFINE_CASE(0, rbc)
    DEFINE_CASE(0, setv)
    DEFINE_CASE(0, pushsym)
    DEFINE_CASE(0, oc_end)
  END_SWITCH_TABLE(0)

  SetCurrentMachine currentMachineData(_Session, this);
  bool keepRunning = true;

  do {

    if (inBreakCondition()) {
      Session* const m = _Session;
      _ASSERT(!returning(), m);
      while (_Operation != leave) ++IP; // TODO
      switch (BP->type) {
        case MBlock::PLAIN:
          if (BP->parentIndex == -1) m->throwRTE("main",
              "incorrect or misplaced break method: crossed main block border");
          break;
        case MBlock::CLASS:
          error("incorrect or misplaced break method: crossed class border");
          break;
        case MBlock::METHOD:
          error("incorrect or misplaced break method: crossed method border");
          break;
        case MBlock::LOOP_STEP:
          error("incorrect or misplaced break method: loop step block");
          break;
        case MBlock::LOOP_COND:
          error("incorrect or misplaced break method: loop condition block");
          break;
      }
      if (m->exceptionPending())
        goto handleException;
      if (RC == M_OPCODE && _Operation == leave) break; // TODO check on
                                                        // _Operation not necessary
    }
    else if (returning()) {
      IP = BP->codeSegment.size() - 1;
      if (BP->type == MBlock::METHOD ||
          (BP->type == MBlock::PLAIN && BP->evaluate)) {
        _ASSERT(!(BP->type == MBlock::PLAIN && BP->evaluate) ||
                !BP->execute, _Session);
        exitReturnCondition();
      }
    }

    SWITCH (0, _Operation)

      // Void operation
      CASE (0, nop):
      CASE (0, oc_end):
        BREAK(0);

      // Set current source line
      CASE (0, setl):
        _ASSERT(_Operation == setl, _Session);
        _Session->getMainMachine()->LR = LR = _Operand;
        BREAK(0);

      // Set current source file
      CASE (0, setf):
#ifdef MAKING_DIAGNOSTIC_MACHINE
        machine->execute_file();
#endif
        BREAK(0);

      // Push receiver onto execution stack
      CASE (0, pushrec):
        _ASSERT(_Operation == pushrec, _Session);
        // begin new frame
        callStack.push();
        // the rest is like a push

      // Push (argument) identifier onto execution stack
      CASE (0, push): {
        CONNECTION_POINT(0, push)

        _ASSERT(_Operation == push    ||
                _Operation == pushrec ||
                _Operation == pushrz, _Session);
        _ASSERT_TODO(!parent() || parent()->block == BP, _Session);

        Session* const m = _Session;
        setSR();

        if (SR->isLiteral()) {
          OVar& var = getStaticVar(); // literals are static vars
          if (var.undefined()) {
            initLiteral(var);
            if (m->exceptionPending() || var.undefined()) {
              if (m->exceptionPending())
                m->rethrow(parent() ? parent()->getMethodName() : "main");
              else
                error("icorrect or corrupted literal '"+SR->name+"'");
              BREAK(0);
            }
          }
          callStack.top->push(var, _Operation == push);
        }
        else { // not a literal
          _ASSERT(SR->getVariableType() == SYMTYPE_BLOCK ||
                  SR->isVariable(), m);
          _ASSERT(!SR->isSelector(), m);
          _ASSERT(SR->getVariableType() != SYMTYPE_CLASS, m);

          if (SR->isPropRef()) {
            _ASSERT(parent() != nullptr, m);
            _ASSERT(SR->variableOffset != -1, m);
            OVar* rec = varThis();
            if (_REF(rec)->validMemberOffset(SR->variableOffset)) {
              auto& varMember = _REF(rec)->property(SR->variableOffset);
#if __DEBUG__
              if (isUndefindedReceiver(varMember, "reference property")); else
#endif // __DEBUG__
              callStack.top->push(varMember, _Operation == push);
            }
            else
              errorNoProp(SR->name, _REF(rec)->getClassName());
          }
          else if (SR->isArgRef()) {
            _ASSERT(parent(), m);
            const auto argNum = SR->variableOffset;
            if (argNum != -2) {
              MCallFrame* parentMethodFrame = getParentMethodFrame();
              _ASSERT(parentMethodFrame && parentMethodFrame->block
                  == BP->getParentMethodBlock(*BT), m);
              if (argNum < (int)parentMethodFrame->args.size()) {
                auto arg = argNum == -1 ? parentMethodFrame->receiver :
                                          parentMethodFrame->args[argNum];
#if __DEBUG__
                if (isUndefindedReceiver(*arg,
                    argNum == -1 ? "receiver" : "argument")); else
#endif // __DEBUG__
                callStack.top->pushUndisposed(arg, _Operation == push);
              }
              else
                error("the argument symbol '"+SR->name+"' is out of range");
            }
            else
              error("the argument symbol '"+SR->name+"' is undefined");
          }
#if USE_SYMBOL_REFERENCE_CACHES
          else if (SR->isSymRef()) { // this must be before isStatic check
            _ASSERT(SR->isStatic(), m);
            const auto& staticData = parent()->block->dataSegment.staticData;
            const auto& symRef = *staticData[SR->variableOffset].symRef;
            OVar* varPtr = symRef.dereference(parent()->prev);
#if __DEBUG__
            if (isUndefindedReceiver(*varPtr, "symbol reference")); else
#endif // __DEBUG__
            callStack.top->push(varPtr, _Operation == push);
          }
#endif // USE_SYMBOL_REFERENCE_CACHES
          else if (SR->isStatic()) {
            _ASSERT(!SR->isSymRef(), m);
            OVar& var = getStaticVar();
            if (var.undefined()) {
              if (SR->getVariableType() == SYMTYPE_BLOCK &&
                  _Operation == pushrec) {
                REF(var) = m->getClass(CLSID_BLOCK)->createObject(m);
                g_refBlockId(REF(var)) = SR->getBlockId();
                callStack.top->push(var, false);
              }
              else // static variables are already initialized
                m->throwRTE("main", "null "+string{_Operation == push ?
                    "argument" : "receiver"});
            }
            else
              callStack.top->push(var, _Operation == push);
          }
          else { // symbol is not static
            OVar& var = getLocalVar();
            if (var.undefined()) { // object reference is not initialized
              _ASSERT(!SR->isStatic(), m);
              // check if variable is a null reference
              if (var.isRef()) {
#if __DEBUG__
                if (isUndefindedReceiver(var, "reference")); else
#endif // __DEBUG__
                callStack.top->push(var, _Operation == push);
              }
              // check if variable is a property of this object
              else if (const auto [methodFrame, owner, offset, access] =
                    getDataIfProp(SR->name); methodFrame) {
                // it is a property of this object
                const ORef rec = _REF(methodFrame->receiver);
                // if it is accessible...
                if (!g_private(access) || rec->getClass() == owner) {
                  SR->type |= SYMTYPE_PROP_REF;
                  SR->variableOffset = offset;
                  // update local value
                  auto& prop = rec->property(offset);
                  // push the variable onto the stack
#if __DEBUG__
                  if (isUndefindedReceiver(prop, "reference property"))
                    BREAK(0);
#endif // __DEBUG__
                  const bool asArg = _Operation == push;
                  if (!methodFrame->receiver->isMutable()) {
                    const ushort usg = prop.usage|USG_CONST|USG_REF;
                    callStack.top->pushExtraLocal(OVar{REF(prop), usg}, asArg);
                  }
                  else
                    callStack.top->push(prop, asArg);
                }
                else
                  error("member '"+SR->name+"' is not accessible");
              }
              // check if the method being called supports VIASA args
              else if (_Operation == push && // we are pushing an argument
                       !callStack.top->isInlineBlock() && // no inline block
                       acceptVIASA(curMethodName(), curArgIndex())) {
                // create string and push it to local stack
                ORef o = BTI::getTypeClass<string>()->createObject(m);
                if (m->exceptionPending())
                  m->rethrow(curMethodName(), __PRETTY_FUNCTION__, __LINE__);
                else {
                  *_PStr(o) = SR->name;
                  var.objectRef = o;
                  var.usage |= USG_CONST;
                  callStack.top->args.push_back(&var);
                }
              }
              // check if the symbol is a temporary variable
              else if (SR->isTmp()) {
                // temporary symbols cannot be initialized from parent frames
                const string msg = "undefined temporary (did you serialize "
                                   "an invocation returning nothing?)";
                m->throwRTE(parent()->getMethodName(), msg);
              }
              // finally, try to resolve symbol from a parent block
              else {
                const auto [rc, p] = initFromHigherBlock(*SR, NO_RESTRICTIONS);
                if (rc == M_INIT_VAR_OK)
                  callStack.top->pushUndisposed(p, _Operation == push);
                else {
                  string msg;
                  if (_Operation == pushrz) {
                    if (rc == M_INIT_VAR_UNINIT)
                      msg = "'"+SR->name+"' refers to uninitialized "
                            "variable from parent block";
                    else
                      callStack.top->receiver = &var;
                  }
                  else {
                    const string varName = "'"+SR->name+"'";
                    switch (rc) {
                      case M_INIT_VAR_NOT_FOUND:
                        msg = "undefined variable "+varName;
                        break;
                      case M_INIT_VAR_NO_ACCESS:
                        msg = varName+" is private to code block";
                        break;
                      case M_INIT_VAR_BLOCK_INACTIVE:
                        msg = varName+" is inaccessible (inactive code"
                                      " block or lambda without this)";
                        break;
                      case M_INIT_VAR_UNINIT:
                        msg = varName+" is uninitialized";
                        break;
                      default:
                        msg = "could not assess the status of "+varName;
                    }
                  }
                  if (!msg.empty()) {
                    MCallFrame* const p = parent();
                    m->throwRTE(p ? p->getFullMethodName() : "main", msg, "", LR);
                  }
                }
              }
            }
            else { // variable (object reference + its usage) is initialized
              _ASSERT(!var.disposable() || SR->isTmp(), m);
              if (_Operation == push) {
                if (var.notInitializable())
                  m->throwRTE(SELECTOR_CACHE(callStack.top->selector)->name,
                      "improper use of static variable definition");
                else
                  callStack.top->args.push_back(&var);
              }
              else { // a just-created unnamed constant cannot get
                     // further initialized once passed as an argument
                _ASSERT(!(var.usage & USG_IGNORE_CONST) || SR->isTmp(), m);
                // if ((var.usage & USG_IGNORE_CONST) != 0)
                //   var.usage &= ~USG_IGNORE_CONST;
                callStack.top->receiver = &var;
              }
            }
          }
        }
        _DEBUG( SR = nullptr; )
        BREAK(0);
      }

      // push selector
      CASE (0, pushsel): {
        _ASSERT(_Operation == pushsel, _Session);
        Session* const m = _Session;
        setSR();

        // It can be that a selector is used by different types of
        // receivers at various places in the script. We could handle
        // this by simply storing its newly resolved version in a
        // global or a local static variable. Instead, in order to
        // make a provision for the possibility that a different
        // receiver from the same position in the script (for example,
        // a template receiver) also makes use of the same selector,
        // we create a cache and store it in the local static segment.
        //
        // For performance comparison we also store the resolved
        // method data as a local static variable. Undefine
        // USE_METHOD_CACHES or set it to 0 to select this method.

        // The interpreter creates selectors as local static symbols.
        _ASSERT(SR->isStatic(), _Session);
        OVar& var = getStaticVar();

        // initialize selector if not initialized
#if USE_SELECTOR_CACHES
        if (var.undefinedSelector()) {
          var.selectorCache = new SelectorCache(m, SR->name);
          var.type = VARTYPE_SEL_CACHE;
          var.usage = 0;
        }
#else // !USE_SELECTOR_CACHES
        if (var.undefinedSelector()) {
          var.selectorData = new SelectorData;
          var.selectorData->name = SR->name;
          var.selectorData->owner = nullptr;
          var.type = VARTYPE_SEL_DATA;
          var.usage = 0;
        }
        else {
          _ASSERT(var.selectorData->name == SR->name, m);
        }
#endif // USE_SELECTOR_CACHES
        // push selector cache into current frame
        callStack.top->selector = var;

        _DEBUG( SR = nullptr; )
        BREAK(0);
      }

      CASE (0, pushr):       // push register RES
        BREAK(0);

      // push register SO
      CASE (0, pushso):
        _ASSERT(_Operation == pushso, _Session);
        if (_Operand == 1) {
          // begin new frame
          callStack.push();
          // push receiver
          callStack.top->receiver = &SOVAR;
        }
        else
          callStack.top->args.push_back(&SOVAR);
        BREAK(0);

      // push symbol on stack as a receiver
      CASE (0, pushrz):
        _ASSERT(_Operation == pushrz, _Session);
        // begin a new frame
        callStack.push();
        // branch off to push
        BRANCH(0, push);

      // push symbol on stack
      CASE (0, pushsym): {
        _ASSERT(_Operation == pushsym, _Session);
        setSR();
        // callStack.top->extraLocals.emplace_back(OVar{size_t(_Operand)});
        // callStack.top->args.push_back(&callStack.top->extraLocals.back());
        callStack.top->pushExtraLocal(OVar{size_t(_Operand)});
        BREAK(0);
      }

      // execute unit
      CASE (0, exec): {
        _ASSERT(_Operation == exec, _Session);
        _ASSERT(callStack.top->args.size() == _Operand, _Session);
        Session* const m = _Session;
        if (callStack.top->receiver->notInitializable()) {
          // receiver is the result of a static variable construction
          if (callStack.top->receiver->disposable()) {
            m->throwRTE(SELECTOR_CACHE(callStack.top->selector)->name,
                "a static variable definition's result cannot be disposable");
            BREAK(0);
          }
          RES = *callStack.top->receiver;
          callStack.pop(m);
          BREAK(0);
        }
        // Get receiver's and caller's classes
        OClass* c = _REF(callStack.top->receiver)->getClass();
        OClass* caller = refThis() ? refThis()->getClass() : nullptr;
        const OVar& var = callStack.top->selector;
        _ASSERT(!var.undefinedSelector(), m);
#if USE_SELECTOR_CACHES
        _ASSERT(var.type == VARTYPE_SEL_CACHE, m);
        const auto result = var.selectorCache->getSelectorForClass(c, caller, m);
        const auto& methodAddress = std::get<0>(result);
        const bool notResponding = std::get<1>(result);
        const string& prefix = var.selectorCache->prefix;
        const string& name = var.selectorCache->name;
        if (!notResponding && !methodAddress.valid()) {
          const string callingMethod = parent() ? (
              (caller ? caller->name+"::" : "")+parent()->getMethodName()) :
              "main";
          const string methodName = prefix.empty() ? name :
              prefix + "::" + name;
          m->throwRTE(callingMethod, "'"+methodName+"' is not accessible");
          BREAK(0);
        }
#else // !USE_SELECTOR_CACHES
        _ASSERT(var.type == VARTYPE_SEL_DATA, _Session);
        var.selectorData->owner = c;
        const string& prefix = var.selectorData->prefix;
        const string& name = var.selectorData->name;
        OAddr& methodAddress = var.selectorData->methodAddress;
        bool notResponding = false;
        OClass* const b = prefix.empty() ? c : m->classMgr->getClass(prefix);
        if (b) {
          auto result = b->getMethodAccessAddress(name, caller);
          if (result.second.notValid()) { // error
            _ASSERT(methodAddress.notValid(), m); // null (invalid) address
            notResponding = true;
          }
          else if (!result.first) {
            _ASSERT(methodAddress.notValid(), m); // null (invalid) address
            const string methodName = prefix.empty() ? name :
                prefix + "::" + name;
            m->throwRTE(methodName, "'"+methodName+"' is not accessible");
            BREAK(0);
          }
          else
            methodAddress = result.second;
        }
        else
          notResponding = true;
#endif // USE_SELECTOR_CACHES
        if (notResponding) {
          MCallFrame* f = getParentMethodFrame();
          const string callingMethod =
              f ? (caller ? caller->name+"::" : "")+f->getMethodName() : "main";
          const string methodName = prefix.empty() ? name :
              prefix + "::" + name;
          m->throwRTE(callingMethod, "class '"+c->name+"' "
              "does not respond to method '"+methodName+"'", "", LR);
          BREAK(0);
        }

        auto* const stackTop = callStack.top;
        // save previous frame registers
        stackTop->BP = BP;
        stackTop->IP = IP; // to be fixed by 'leave' for script methods
        saveRC(stackTop);
        if (methodAddress.scriptMethod) {
          // switch to new frame
          const auto blockId = methodAddress.scriptMethodAddress.block;
          BP = stackTop->block = &(*BT)[blockId];
          IP = -1;
          setDefaultRC();
          // basic assumption: ONLY exec can produce a return condition
          _ASSERT(!returning(), m);
        }
        else { // core method
          stackTop->block = nullptr;
          // apply method
          const auto& f = methodAddress.coreMethodAddress;
          const int argc = stackTop->args.size();
          OArg* const argv = stackTop->args.data();
          OArg rec = stackTop->receiver;
          if (MCallFrame* const prev = stackTop->prev;
              prev && prev->referenceResult &&
              _REF(prev->referenceResult) == _REF(stackTop->receiver))
            rec = prev->referenceResult;
          RES = (_REF(stackTop->receiver)->*f)(rec, argc, argv, m);
          // restore machine state
          BP = stackTop->BP;
          IP = stackTop->IP;
          restoreRC(stackTop);
          callStack.pop(m);
          // basic assumption: ONLY exec can produce a return condition
          if (returning()) {
            IP = BP->codeSegment.size() - 1;
            if (BP->type == MBlock::METHOD ||
                (BP->type == MBlock::PLAIN && BP->evaluate)) {
              _ASSERT(!(BP->type == MBlock::PLAIN && BP->evaluate) ||
                      !BP->execute, m);
              exitReturnCondition();
            }
            BRANCH(0, leave);
          }
        }
        BREAK(0);
      }

      // execute block
      CASE (0, execb):
        _ASSERT(_Operation == execb, _Session);
        // begin new frame
        callStack.push();
        // save previous frame registers
        callStack.top->BP = BP;
        callStack.top->IP = IP; // to be fixed by 'leave'
        saveRC(callStack.top);
        // switch to new frame
        BP = callStack.top->block = &(*BT)[_Operand];
        IP = -1;
        setDefaultRC();
        BREAK(0);

      // execute lambda block
      CASE (0, execlb): {
        _ASSERT(_Operation == execlb, _Session);
        Session* const m = _Session;
        const OVar& var = *callStack.top->receiver;
        _ASSERT(!var.undefined(), m);
        if (REF(var)->getClassId() != CLSID_BLOCK) {
          error("expected block argument");
          BREAK(0);
        }
        const int blockId = g_blockId(REF(var));
        if (_Operand != 0) { // set argument names
          const auto& args = callStack.top->args;
          _ASSERT(_Operand == callStack.top->args.size(), m);
          auto& block = (*BT)[blockId];
          if (block.type != MBlock::METHOD || m->checkLambdaProto()
                                           || m->mutableLambdaProto()) {
            _ASSERT(_ref(args.front())->getClassId() == CLSID_STRING, m);
            const bool hasThis = *_PString(_ref(args.front())) == "this";
            // build list of argument names
            std::vector<string> argNames;
            argNames.reserve(args.size() - (hasThis ? 1 : 0));
            size_t indexArg = 0;
            for (const auto& arg : args) {
              _ASSERT(_ref(arg)->getClassId() == CLSID_STRING, m);
              if (hasThis && indexArg++ == 0) continue;
              argNames.push_back(*_PString(_ref(arg)));
            }
            // apply action
            if (block.type != MBlock::METHOD) {
              // no need to check arguments - already checked
              block.setMethodArgNames(argNames);
            }
            else if (m->checkLambdaProto()) {
              if (!block.dataSegment.checkLambdaProto(argNames))
                m->throwRTE("MDataSegment::checkLambdaProto",
                    "lambda's prototype has changed");
            }
            else /* mutable lambda proto */ {
              const bool diags = m->undefinedArgsDiagnostics();
              block.dataSegment.refreshLambdaArglist(argNames, diags);
            }
          }
          callStack.pop(m);
        }
        else if (!callStack.top->args.empty()) {
          const auto& methodName = SELECTOR_CACHE(callStack.top->selector)->name;
        //   const bool hasThis = strncmp("this:", methodName.c_str(), 5) == 0;
          const bool hasThis = StringView{methodName}.startsWith("this:", 5);
          // ===================================================
          // *** B E N C H M A R K *** Performance Check Route 1
          // ===================================================
#if 1
          // prepare frame
          if (hasThis) {
            callStack.top->receiver = callStack.top->args.front();
            if (!callStack.top->receiver || callStack.top->receiver->undefined()) {
              m->throwRTE(methodName, "null receiver");
              callStack.pop(m);
              BREAK(0);
            }
            callStack.top->args.erase(callStack.top->args.begin());
          }
          else {
            callStack.top->receiver = varThis();
            if (!callStack.top->receiver || callStack.top->receiver->undefined())
              callStack.top->receiver = &SOVAR;
          }
          // save previous frame registers
          callStack.top->BP = BP;
          callStack.top->IP = IP; // to be fixed by 'leave'
          saveRC(callStack.top);
          // switch to new frame
          BP = callStack.top->block = &(*BT)[blockId];
          IP = -1;
          setDefaultRC();
          // ===================================================
          // *** B E N C H M A R K *** Performance Check Route 2
          // ===================================================
#else
          OArg rec;
          if (hasThis) {
            rec = callStack.top->args.front();
            if (rec)
              callStack.top->args.erase(callStack.top->args.begin());
            else
              m->throwRTE(methodName, "null receiver");
          }
          else {
            rec = varThis();
            if (!rec || rec->undefined()) rec = &SOVAR;
          }
          if (!m->exceptionPending()) {
            VirtualMachine vm(*this);
            vm.init(blockId);
            RES = vm.execScriptMethod(methodName, blockId, 0, rec,
              callStack.top->args.size(), callStack.top->args.data());
          }
          callStack.pop(m);
#endif
        }
        else {
          auto& block = (*BT)[blockId];
          if (block.type != MBlock::METHOD) {
            block.type = MBlock::METHOD;
            _ASSERT(block.dataSegment.localSymbolTable[0].name == "this", m);
            block.dataSegment.localSymbolTable[0].type |= SYMTYPE_ARG_REF;
            block.dataSegment.localSymbolTable[0].variableOffset = -1;
          }
          else if (m->checkLambdaProto()) {
            std::vector<string> EMPTY;
            if (!block.dataSegment.checkLambdaProto(EMPTY))
              m->throwRTE("MDataSegment::checkLambdaProto",
                  "lambda's prototype has changed");
              callStack.pop(m);
              BREAK(0);
          }
          else if (m->mutableLambdaProto() && m->undefinedArgsDiagnostics()) {
            block.dataSegment.prepDiagsForLambdaNoArgs();
          }
          // adjust method name (for diagnostic purposes)
          string& methodName = SELECTOR_CACHE(callStack.top->selector)->name;
          if (methodName.length() == 7)
            methodName += std::to_string(blockId);
          // save previous frame registers
          callStack.top->BP = BP;
          callStack.top->IP = IP; // to be fixed by 'leave'
          saveRC(callStack.top);
          // switch to new frame
          BP = callStack.top->block = &(*BT)[blockId];
          IP = -1;
          setDefaultRC();
        }
        BREAK(0);
      }

      // execute get property
      CASE (0, execgp): {
        _ASSERT(_Operation == execgp, _Session);
        _ASSERT(callStack.top->args.size() == 1, _Session);
        _ASSERT(callStack.top->isInlineBlock(), _Session);
        Session* const m = _Session;
        // get receiver's and caller's classes
        ORef const o = refThis();
        OClass* const c = _REF(callStack.top->receiver)->getClass();
        OClass* const caller = o ? o->getClass() : nullptr;
        // resolve member
        ORef const oMemberName = _REF(callStack.top->args[0]);
        const auto& propName = oMemberName->castRef<oxString>();
        const auto propInfo = c->resolveProperty(propName);
        const auto ofs = std::get<2>(propInfo);
        if (ofs == -1) {
#ifdef __DEBUG__
          const string strBI = std::to_string(getIndexBP());
          const string trailer = " (BI:IP = "+strBI+":"+std::to_string(IP)+")";
#else // !__DEBUG__
          const string trailer{};
#endif // !__DEBUG__
          string msg = c->isMutable() ? "variable" : "type '"+c->name+"'";
          if (c->isMutable()) {
            const MCallFrame* const p = parent();
            if (p && p->block) {
              const string varName = getVariableName(*p->block,
                  _REF(callStack.top->receiver));
              if (!varName.empty()) msg += " '"+varName+"'";
            }
          }
          error(msg+" has no property '"+propName+"'"+trailer);
          callStack.pop(m);
          BREAK(0);
        }
        // check member's accessibility
        const auto access = std::get<3>(propInfo);
        const OClass* const owner = std::get<0>(propInfo);
        if (!owner->isMemberAccessible(access, caller)) {
          error("'"+propName+"' is not accessible in this context");
          callStack.pop(m);
          BREAK(0);
        }
        // get property
        auto& r = _REF(callStack.top->receiver)->property(ofs);
        if (callStack.top->receiver->isConst())
          r.setConst();
        RES = r;
        // this is only used by mref
        callStack.top->prev->referenceResult = r.isRef() ? &r : nullptr;
        callStack.pop(m);
        BREAK(0);
      }

      // execute conditional block
      CASE (0, execcb): {
        _ASSERT(_Operation == execcb, _Session);
        _ASSERT(callStack.top->args.size() == 2, _Session);
        _ASSERT(callStack.top->isInlineBlock(), _Session);
        Session* const m = _Session;
        int i = -1;
        OArg* const argv = callStack.top->args.data();
        if (!_ARG_TYPEID(i = 0, CLSID_BLOCK) ||
            !_ARG_TYPEID(i = 1, CLSID_BLOCK)) {
          const string msg = "unexpected type for option "+std::to_string(i+1)
              +" of conditional expression: '" +_ARG(i)->getClassName()+"'";
          error(msg);
          callStack.pop(m);
          BREAK(0);
        }
        // unpack receiver to get the condition's value
        ORef rec = _REF(callStack.top->receiver);
        const OClass* const recType = rec->getRootClass();
        const OClass* const boolType = BTI::getTypeClass<bool>();
        bool condition;
        if (recType == boolType)
          condition = _P(bool, rec)->getValue();
        else if (recType == BTI::getTypeProxyClass<bool>())
          condition = _PTypeProxy(bool, rec)->getValue();
        else {
          ORet result = m->valueCast(BTI::getTypeClass<bool>(), rec);
          if (m->exceptionPending() ||
              !m->checkCastResult(result, boolType, rec->getClass())) {
            const MCallFrame* p = parent();
            m->rethrow(p ? p->getMethodName() : "main", "", LR);
          }
          else {
            condition = _P(bool, REF(result))->getValue();
            m->deleteObject(REF(result)); // it is disposable
            if (m->exceptionPending()) {
              const MCallFrame* p = parent();
              m->rethrow(p ? p->getMethodName() : "main", "", LR);
            }
          }
        }
        if (m->exceptionPending()) {
          callStack.pop(m);
        }
        else {
          const auto& args = callStack.top->args;
          const auto blockId = g_blockId(_REF(args[condition ? 0 : 1]));
          // simulate an `execb`
          callStack.pop(m);
          // begin new frame
          callStack.push();
          // save previous frame registers
          callStack.top->BP = BP;
          callStack.top->IP = IP; // to be fixed by 'leave'
          saveRC(callStack.top);
          // switch to new frame
          BP = callStack.top->block = &(*BT)[blockId];
          IP = -1;
          setDefaultRC();
        }
        BREAK(0);
      }

      // store register RES to indicated id (must be tmp)
      CASE (0, stor): {
        _ASSERT(_Operation == stor, _Session);
        setSR();
        _ASSERT(SR->isTmp(), _Session);
        OVar& tmpVar = getLocalVar(currentFrame(), SR->variableOffset);
        tmpVar = RES;
        RES = OVar{};
        BREAK(0);
      }

      // enter frame
      CASE (0, enter):
        _ASSERT(_Operation == enter, _Session);
        // create locals
        callStack.top->locals = std::vector<OVar>(_Operand, OVar());
        BREAK(0);

      // exit frame
      CASE (0, leave): {
        CONNECTION_POINT(0, leave)
        _ASSERT(_Operation == leave, _Session);

        // IMPORTANT NOTE:
        // This condition should be at the top of the do loop,
        // just above the breaking condition. Currently, this
        // occurs only for the leave VM-instruction; therefore,
        // for performance reasons we placed it here.
        if (_Session->exceptionPending()) // TODO check!
          BREAK(0);

        MCallFrame* const curFrame = callStack.top;
        _ASSERT(curFrame, _Session);
#if 0
        // delete left-behind temporary variables
        for (const auto& symbol : BP->dataSegment.localSymbolTable) {
          if (!symbol.isTmp()) continue;
          auto& var = getLocalVar(curFrame, symbol.variableOffset);
          if (var.undefined() || var.isRef()) continue;
          if (var.disposable())
            _Session->deleteObject(REF(var));
          REF(var) = nullptr;
        }
        // destroy local vars
        _DEBUG(                                                         \
          const int numLocals = curFrame->localSize();                  \
          _ASSERT(BP->codeSegment[0].opcode != enter ||                 \
                  numLocals == BP->codeSegment[0].operand, _Session);   \
          int numLocals2 = 0;                                           \
          for (const auto& sym : BP->dataSegment.localSymbolTable) {    \
            if (sym.isVariable() && !sym.isStatic() && !sym.isArgRef()) \
              ++numLocals2;                                             \
          }                                                             \
          _ASSERT(numLocals >= numLocals2, _Session);                   \
        )
        for (OVar& var : callStack.top->locals) {
          if (var.undefined() || var.isRef() || var.type == VARTYPE_INT_DATA)
            continue;
          _Session->deleteObject(REF(var));
          REF(var) = nullptr;
        }
#endif // 0
        ObjectAutoCleaner stackCleaner{_Session};
        cleanStackFrame(stackCleaner, curFrame);
        // restore machine state
        _ASSERT(BP == curFrame->block, _Session);
        BP = curFrame->BP;
        IP = curFrame->IP;
        restoreRC(curFrame);
        // clean machine stack
        callStack.pop(_Session);
        // NOTE: Now the condition BP == callStack.top->block may be not valid
        if (inBreakCondition() || returning()) {
          // pop all unfinished frames
          const MCallFrame* const mostBaseStack =
              (parentMachine ? parentMachine->callStack.top : nullptr);
          while (callStack.top != mostBaseStack && callStack.top->trivial()) {
            // TODO URGENT! see comment in exitCallFrame()
            BP = curFrame->BP;
            IP = curFrame->IP;
            restoreRC(curFrame);
            callStack.pop(_Session);
          }
        }
        BREAK(0);
      }

      CASE (0, jmp):         // unconditionally jump to
        IP = _Operand; // specified offset
        continue;
        BREAK(0);

      // destroy disposed tmp object
      CASE (0, dto): {
        Session* const m = _Session;
        _ASSERT(_Operation == dto, m);
        setSR();
        _ASSERT(SR->isVariable(), m);
        _ASSERT(SR->isTmp(), m);

        OVar& var = getLocalVar(currentFrame(), SR->variableOffset);
        if (var.disposable() && !var.undefined()) {
#if 0
          ORef const o = REF(var);
          const string& typeName = o->getClass()->name;
          m->deleteObject(o);
          if (m->exceptionPending() || REF(RES) == o) {
            MCallFrame* p = parent();
            const string methodName{ p ? p->getMethodName() : "main" };
            if (m->exceptionPending())
              m->rethrow(methodName);
            else
              m->throwRTE(methodName, "cannot return disposable object type '"+
                  typeName+"'");
          }
#else
          ORef const o = REF(var);
          if (REF(RES) == o)
            REF(RES) = m->nullObject();
          m->deleteObject(o);
        }
#endif // 0
        // TODO clean up!
        // _ref(var) = nullptr;
        // var.usage = 0;
        var = OVar{};

        _DEBUG( SR = nullptr; )
        BREAK(0);
      }

      CASE (0, dtor):        // similar to dto, object is referenced by RES
        _ASSERT(_Operation == dtor, _Session);
        _ASSERT(!RES.undefined(), _Session);
        if (RES.disposable())
          _Session->deleteObject(_ref(RES));
        RES = OVar{};
        BREAK(0);

      CASE (0, rop):         // Register operations
        switch (_Operand) {
          case scop:             // set const operation flag
            callStack.top->constOperation = true;
            break;
          case lcru:             // removed
            break;
          case zres:             // zero RES
            RES = OVar{};
            break;
          case stchk:            // check stack
            break;
          case cres:             // make RES constant
            break;
        }
        BREAK(0);

      CASE (0, chkrv):
        _ASSERT(_Operation == chkrv, _Session);
        setSR();
        if (SR->isTmp())
          error("temporary values (rvalues) are not allowed here");
        BREAK(0);

      CASE (0, lres): {
        _ASSERT(_Operation == lres, _Session);
        setSR();
        OVar* var = resolveSymbol(*SR);
        if (var) {
          if (var->undefined()) {
            auto fp = currentFrame();
            _Session->throwRTE(fp ? fp->getMethodName() : "main",
                               "uninitialized variable '"+SR->name+"'");
          }
          else {
            RES = *var;
            var->undispose();
          }
        }
        else {
          auto fp = currentFrame();
          _Session->throwRTE(fp ? fp->getMethodName() : "main",
                             "unknown type symbol '"+SR->name+"'");
        }
        callStack.pop(_Session);
        BREAK(0);
      }

      CASE (0, pushra):
        BREAK(0);

      // make indicated argument a reference
      CASE (0, mref): {
        _ASSERT(_Operation == mref, _Session);
        SWITCH_TABLE_VAR_TYPES(3);

        const char* const REF_TMP_VAR = "cannot reference temporary variable";
        Session* const m = _Session;
        setSR();
        MCallFrame* const fp = callStack.top;
        auto rhs = fp->receiver;
        const bool makeConst = fp->constOperation;

        SWITCH (3, VAR_USAGE(SR->type))
          CASE (3, VU_VAR):
            if (SR->isTmp()) { // lhs is a temporary variable
              if (fp->referenceResult) {
                if (!assignReference(*fp->referenceResult, rhs, makeConst))
                  error(REF_TMP_VAR);
              }
              else
                error("temporary variables cannot be references");
            }
            else {
              OVar& lhs = getLocalVar();
              if (lhs.undefined()) {
                if (const auto [methodFrame, owner, offset, access] =
                    getDataIfProp(SR->name); methodFrame) {
                  // it is a property of this object
                  const ORef rec = _REF(methodFrame->receiver);
                  // it must be a reference
                  if (!g_ref(access))
                    error("the property '"+SR->name+"' is not a reference");
                  else if (!g_private(access) || rec->getClass() == owner) {
                    // it is accessible
                    SR->type |= SYMTYPE_PROP_REF;
                    SR->variableOffset = offset;
                    // update local value
                    auto& varMember = rec->property(offset);
                    if (!assignReference(varMember, rhs, makeConst))
                      error(REF_TMP_VAR);
                  }
                  else
                    error("the property '"+SR->name+"' is not accessible");
                  BREAK(3);
                }
              }
              // handle local variable
              if (lhs.isRef()) {
                if (!lhs.undefined() && BTI::isBasicTypeProxy(REF(lhs)))
                  REF(lhs)->deleteObject(m);
                if (!assignReference(lhs, rhs, makeConst))
                  error(REF_TMP_VAR);
              }
              else if (lhs.undefined()) {
                const auto [ec, referencedVar] = initFromHigherBlock(*SR,
                    /*METHOD_BOUNDARY*/ NO_RESTRICTIONS);
                if (ec == M_INIT_VAR_OK) {
                  if (referencedVar->isRef()) {
                    if (!assignReference(*referencedVar, rhs, makeConst))
                      error(REF_TMP_VAR);
                  }
                  else // that was an attempt to convert a non-ref to a ref
                    error("the symbol '"+SR->name+"' is not a reference");
                }
                else if (!assignReference(lhs, rhs, makeConst))
                  error(REF_TMP_VAR);
              }
              else { // initialized but not as a reference
                const string s = BTI::isBasicTypeProxy(REF(lhs)) ?
                    "a proxy; it cannot be treated as " : "not ";
                error("the symbol '"+SR->name+"' is "+s+"a reference");
              }
            }
            BREAK(3);

          CASE (3, VU_STATIC): {
            _ASSERT(SR->isVariable(), m);
            _ASSERT(!SR->isSymRef(), m);
            auto& lhs = getStaticVar();
            if (lhs.isRef()) {
              if (!lhs.undefined() && BTI::isBasicTypeProxy(REF(lhs)))
                REF(lhs)->deleteObject(m);
              if (!assignReference(lhs, rhs, makeConst))
                error(REF_TMP_VAR);
            }
            else
              error("static symbol "+SR->name+" is not a reference variable");
            BREAK(3);
          }

          CASE (3, VU_ARG_REF):
          CASE (3, VU_STATIC_ARG_REF):
            _ASSERT(getParentMethodFrame() && getParentMethodFrame()->block
                == BP->getParentMethodBlock(*BT), m);
            error("cannot convert argument '"+SR->name+"' to reference");
            BREAK(3);

          CASE (3, VU_PROP_REF): {
            _ASSERT(parent() != nullptr, m);
            _ASSERT(SR->variableOffset != -1, m);
            ORef thisRef = refThis();
            if (!thisRef->validMemberOffset(SR->variableOffset)) {
              errorNoProp(SR->name, thisRef->getClassName());
              BREAK(3);
            }
            auto& varMember = thisRef->property(SR->variableOffset);
            if (varMember.isRef()) {
              if (!varMember.undefined() &&
                  BTI::isBasicTypeProxy(REF(varMember)))
                REF(varMember)->deleteObject(m);
              if (!assignReference(varMember, rhs, makeConst))
                error(REF_TMP_VAR);
            }
            else
              error("property '"+SR->name+"' is not a reference");
            BREAK(3);
          }

          CASE (3, VU_STATIC_PROP_REF):
            _ASSERT(false, m);
            BREAK(3);

          CASE (3, VU_SYM_REF):
          CASE (3, VU_STATIC_SYM_REF): {
            _ASSERT(SR->isStatic(), m);
            const auto& staticData = BP->dataSegment.staticData;
            const auto& symRef = *staticData[SR->variableOffset].symRef;
            OVar* varPtr = symRef.dereference(parent()->prev);
            if (varPtr->isRef()) {
              if (!varPtr->undefined() && BTI::isBasicTypeProxy(_REF(varPtr)))
                _REF(varPtr)->deleteObject(m);
              if (!assignReference(*varPtr, rhs, makeConst))
                error(REF_TMP_VAR);
            }
            else
              error("cannot convert variable '"+SR->name+"' to reference");
            BREAK(3);
          }

          CASE (3, _6):
          CASE (3, _7):
          CASE (3, _10):
          CASE (3, _11):
          CASE (3, _12):
          CASE (3, _13):
          CASE (3, _14):
          CASE (3, _15):
            m->throwRTE("reference variable",
                        "unknown type symbol '"+SR->name+"'");
        END_SWITCH (3)

        callStack.pop(m);
        BREAK(0);
      }

      CASE (0, pushb): {     // push code block
        _ASSERT(_Operation == pushb, _Session);
        setSR();
        _ASSERT(SR->isBlockId(), _Session);
        _ASSERT(SR->isStatic(), _Session);
        OVar& var = getStaticVar();
        if (var.undefined()) {
          Session* m = static_cast<Session*>(_Session);
          _ref(var) = m->getClass(CLSID_BLOCK)->createObject(m);
          g_refBlockId(_ref(var)) = SR->getBlockId();
        }
        callStack.top->args.push_back(&var);

        _DEBUG( SR = nullptr; )
        BREAK(0);
      }

      // load classes defined locally in the block
      CASE (0, rbc):
        _ASSERT(_Operation == rbc, _Session);
        //setSR(); // apostol
        registerClassesInBlock();
        BREAK(0);

      // set value to uninitialized variable or copy
      CASE (0, setv): {
        // Important notes:
        // - a method argument can be disposable without being tmp
        // - a method argument can be a literal value (e.g. `rec f: 1`)
        _ASSERT(_Operation == setv, _Session);
        _ASSERT_TODO(!parent() || parent()->block == BP, _Session);
        Session* const m = _Session;
        setSR();
        OVar* const rec = callStack.top->receiver;
        OVar* const arg = callStack.top->args[0];
        const bool undefinedReceiver = rec->undefined();
        const bool isProxy = BTI::isBasicTypeProxy(_REF(arg));
        _ASSERT(!arg->isLiteral()  || !arg->disposable(), m)
        _ASSERT(!arg->disposable() || !arg->isLiteral(),  m)
        if (undefinedReceiver) {
          _ASSERT(!rec->isRef(), m);
          m->emplace(*rec, *arg);
          if (callStack.top->constOperation)
            rec->setConst();
        //   RES = OVar{_REF(rec)}; // TODO other?
          RES = *rec;
        }
        else { // !undefinedReceiver
          _ASSERT(rec->type == VARTYPE_OBJECT || rec->isRef(), m);
#if 1
          // * IMPORTANT * This piece of code is (almost) identical
          // with Session::smartCopy(...); consider modifying it
          // when making changes here.
          if (!rec->isMutable())
            m->throwRTE(parent()->getMethodName(),
                        "cannot modify constant object");
          else if (callStack.top->constOperation) {
            m->throwRTE(parent()->getMethodName(),
                        "improperly used const assignment");
          }
          else {
            const bool sameType = _REF(arg)->getClass() == _REF(rec)->getClass();
            if (sameType && arg->disposable()) {
              _REF(rec)->swapContent(_REF(arg));
              RES = OVar{_REF(rec)};
            }
            else if (sameType && arg->movable()/* && !isProxy*/) {
              _REF(rec)->swapContent(_REF(arg));
              RES = OVar{_REF(rec)};
            }
            else { // do safe copy
              if (SO->getClassId() == CLSID_RTMGR) {
                RES = m->copyFrom(rec, arg);
              }
              else {
                OArg argv[2]{rec, arg};
                auto [result, _] = execMethod("copy:from:", 0, &SOVAR, 2, argv);
                RES = result;
              }
            }
          }
#else
          m->smartCopy(*rec, *arg);
#endif // 0
        }
        callStack.pop(m);
        BREAK(0);
      }

      DEFAULT(0):
        _ASSERT(false, _Session);

    END_SWITCH(0)

    // proceed to next instruction
    ++IP;

  handleException:

    // decide what to do in the next step
    if (_Session->exceptionPending()) {
#ifdef __DEBUG__
#if 0
      _Session->displayMessage("Call Stack Rundown", callStackRundown());
#endif // 0
#endif // __DEBUG__
      // recursively exit frames cleaning up local variables
      ObjectAutoCleaner stackCleaner{_Session};
      while (keepRunning) {
        // exit from an unfinished frame
        while (callStack.top && !callStack.top->block)
          callStack.pop(_Session);
        if (!callStack.top) {
          keepRunning = false;
          break;
        }
        // exit frame after having cleaned up local variables
        exitCallFrame(stackCleaner);
        keepRunning = callStack.top !=
            (parentMachine ? parentMachine->callStack.top : nullptr);
      }
    }
    else { // decide if return condition is met
      switch (RC) {
        case M_FRAME:
          keepRunning = (callStack.top != returnPoint.endFrame);
          break;
        case M_IP:
          keepRunning = returning() ? false : (IP < returnPoint.endIP);
          break;
        case M_OPCODE:
          keepRunning = returning() ? false :
              (_Operation != returnPoint.endOpcode);
          break;
        default:
          keepRunning = true;
          _Session->throwRTE("main",
              "unknown return condition (possibly corrupted system)");
      }
    }
  }
  while (keepRunning);
}
