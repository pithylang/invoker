#include <algorithm>
#include <limits>
#include "otest.hpp"

using std::string;

const size_t Checker::MAX_SIZE_LEFTOVERS{std::numeric_limits<size_t>::max()};
const int Checker::MIN_RET_VALUE{std::numeric_limits<int>::min()};

bool ExpectedException::valid() const noexcept {
  return line != -1 || type != "" || location != "" || message != "";
}

inline bool match(const string& a, const string& b) noexcept
{ return a.empty() || b.empty() || a == b; }
inline bool match(int a, int b) noexcept
{ return a == -1 || b == -1 || a == b; }

bool ExpectedException::matches(const ExpectedException& e) const noexcept {
  return match(line, e.line) && match(type, e.type) &&
      match(location, e.location) && match(message, e.message);
}

void Checker::init() noexcept {
  expectedLeftovers = MAX_SIZE_LEFTOVERS; // disabled
  expectedRetValue = MIN_RET_VALUE; // disabled
}

bool Checker::expandCheckpoint(const string &text) {
  if (checkpointComplete())
  	return false;
  checkpoint += text;
  return true;
}

void Checker::mergeCheck() {
  if (checkpointComplete())
    checkpoint.pop_back();
}

void Checker::setCheckpoint() {
  if (!checkpointComplete())
    checkpoint.push_back('\n');
}

CheckResult Checker::performCheck(const string &expectedText) {
  if (!checkpointComplete())
  	return CHECKPOINT_DEFINED_BUT_IGNORED;
  if (checkpoint.substr(0, checkpoint.size() - 1) != expectedText)
    return CHECK_FAILED;
  checkpoint.clear();
  return CHECK_PASSED;
}

bool Checker::expectException(ExpectedException&& expectedException) {
  if (!expectedException.valid()) return false;
  expectedExceptions.emplace_back(std::move(expectedException));
  return true;
}

bool Checker::expectException(int line, const std::string& type,
      const std::string& location, const std::string& message) {
  return expectException(ExpectedException{line, type, location, message});
}

bool Checker::isExpectedException(const ExpectedException& e) const noexcept {
  auto it = std::find_if(expectedExceptions.begin(), expectedExceptions.end(),
  	[&e](const ExpectedException& item) { return e.matches(item); });
  return it != expectedExceptions.end();
}

bool Checker::isExpectedException(int line, const string& type,
      const string& loc, const string& msg) const noexcept {
  return isExpectedException(ExpectedException{line, type, loc, msg});
}
