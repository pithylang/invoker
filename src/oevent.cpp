// =========================================================
// oxEventTable
// =========================================================
int oxEventTable::addEvent(int type, const char *handler)
{
  if (handler == nullptr)
    return -1;
  oxEventTableEntry event(handler);
  m_entries.add(event);
  int i = eventTableSize() - 1;
  setEventType(i, type);
  return i;
}

int oxEventTable::addEvent(const char *strEvType,
                           const char *handler)
{
  if (handler == nullptr)
    return -1;
  oxEventTableEntry event(handler);
  m_entries.add(event);
  int i = eventTableSize() - 1;
  setEventType(i, strEvType);
  return i;
}

void oxEventTable::setEventType(int i, int type)
{
  m_entries[i].m_eventType = type;
  setEventTypeResolved(i);
}

void oxEventTable::setEventType(int i, const char *strEvType)
{
  ResolveEntry r = {_UNRESOLVED_EVENT_TYPE, i, strEvType};
  m_resolveInfo.add(r);
  m_entries[i].set(_UNRESOLVED_EVENT_TYPE);
}

void oxEventTable::setId(int i, int eventId)
{
  m_entries[i].m_id = eventId;
  setIdResolved(i);
}

void oxEventTable::setId(int i, const char *strEventId)
{
  ResolveEntry r = {_UNRESOLVED_ID, i, strEventId};
  m_resolveInfo.add(r);
  m_entries[i].set(_UNRESOLVED_ID);
}

void oxEventTable::setLastId(int i, int eventId)
{
  m_entries[i].m_lastId = eventId;
  setLastIdResolved(i);
}

void oxEventTable::setLastId(int i, const char *strEventId)
{
  ResolveEntry r = {_UNRESOLVED_LAST_ID, i, strEventId};
  m_resolveInfo.add(r);
  m_entries[i].set(_UNRESOLVED_LAST_ID);
}

void oxEventTable::setParam(int i, ORef param, int)
{
  m_entries[i].m_param = param;
  setParamResolved(i);
}

void oxEventTable::setParam(int i, const char *strParam)
{
  ResolveEntry r = {_UNRESOLVED_PARAM, i, strParam};
  m_resolveInfo.add(r);
  m_entries[i].set(_UNRESOLVED_PARAM);
}

void oxEventTable::setMethodAddress(int i, OMethodAddress *pa)
{
  m_entries[i].m_address = *pa;
  setMethodResolved(i);
}

oxEventTable::ResolveInfo::Element *
oxEventTable::getResolveEntry(int entryIndex,
                             int type,
                             ResolveInfo::Element **ppPrev)
{
  static bool bInitConvTable = false;
  static int s_convTable[8];

  if (!bInitConvTable)
  {
    s_convTable[_RESOLVE_EVENT_TYPE] = _UNRESOLVED_EVENT_TYPE;
    s_convTable[_RESOLVE_ID        ] = _UNRESOLVED_ID;
    s_convTable[_RESOLVE_LAST_ID   ] = _UNRESOLVED_LAST_ID;
    s_convTable[_RESOLVE_PARAM     ] = _UNRESOLVED_PARAM;
    bInitConvTable = true;
  }

  ASSERT(type >= _RESOLVE_EVENT_TYPE &&
         type <= _RESOLVE_PARAM);

  type = s_convTable[type];

  ResolveInfo::Element *p = m_resolveInfo.first();
  if (ppPrev)
    *ppPrev = nullptr;
  for (; p; p = m_resolveInfo.next(p))
  {
    if (p->data().m_type == type &&
        p->data().m_entryIndex == entryIndex)
       return p;
    if (ppPrev)
       *ppPrev = p;
  }
  return nullptr;
}

void oxEventTable::removeResolveInfo(
                    ResolveInfo::Element *pResolveEntry,
                    ResolveInfo::Element *prev)
{
  pResolveEntry = m_resolveInfo.remove(pResolveEntry, prev);
  if (pResolveEntry)
    delete pResolveEntry;
}


