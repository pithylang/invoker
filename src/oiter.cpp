// #include "ostring.hpp"
#include "odev.h"
#include "ortmgr.hpp"
#include "otype.hpp"
#include "obasicTypeInfo.hpp"
#include "oreqs.hpp"
#include "objectAutoCleaner.hpp"
#include "oiter.hpp"
#include "selectors.h"

using std::string;

//::::::::::::::::::::::::::::::   Utilities   ::::::::::::::::::::::::::::::://

static string s_getMethodName(ORef self, const string& methodName) {
  return self->getRootClass()->name+"::"+methodName;
}

enum HandleResultMode {
  NO_HANDLING = 0,
  BOOL_FLIP,
  INT_TO_EQ,
  INT_TO_NE,
  INT_TO_GE,
  INT_TO_LE,
  INT_TO_GT,
  INT_TO_LT,
};

static ORet s_genericOperation(const string& op, const string& opName,
                               HandleResultMode handleResultMode,
                               ORef self, OArg* argv, Session* m) {
  using diff_t = std::ptrdiff_t;
  const OClass* const thisRootClass = self->getRootClass();
  // check argument type
  if (!_ARG_TYPE(0, thisRootClass)) {
    const string methodName = s_getMethodName(self, opName);
    const string msg =
        "unexpected argument type in method "+methodName+
        "\nexpected "+thisRootClass->name+", found "+_ARG(0)->getClassName();
    m->throwBadArg(methodName, msg, __FILE__, __LINE__);
    return OInstance::undefinedResult();
  }
  // apply operation
  auto result = m->execMethod(op, 0, self, 1, argv);
  // no need to check result.second
  _RETHROW(s_getMethodName(self, opName));
  ORef o = REF(result.first);
  // package return value
  ObjectAutoCleaner cleaner{ m, result.first };
  if (handleResultMode == BOOL_FLIP) {
    if (o->getClassId() != BTI::getTypeClassId<bool>())
      OM_THROW_RTE_METHOD(s_getMethodName(self, opName),
          "unexpected result type of method '"+op+"'")
    const bool retValue = _P(bool, o)->getValue();
    // create return value
    ORef r = OBasicTypeInfo::getTypeClass<bool>()->createObject(m);
    _RETHROW(s_getMethodName(self, opName));
    _P(bool, r)->setValue(!retValue);
    return r->disposableResult();
  }
  else if (handleResultMode != NO_HANDLING) {
    if (o->getClassId() != BTI::getTypeClassId<diff_t>())
      OM_THROW_RTE_METHOD(s_getMethodName(self, opName),
          "unexpected result type of method '"+op+"'")
    const diff_t retValue = _P(diff_t, o)->getValue();
    // create return value
    ORef r = OBasicTypeInfo::getTypeClass<bool>()->createObject(m);
    _RETHROW(s_getMethodName(self, opName));
    switch (handleResultMode) {
      case INT_TO_EQ: _P(bool, r)->setValue(retValue == 0); break;
      case INT_TO_NE: _P(bool, r)->setValue(retValue != 0); break;
      case INT_TO_GE: _P(bool, r)->setValue(retValue >= 0); break;
      case INT_TO_LE: _P(bool, r)->setValue(retValue <= 0); break;
      case INT_TO_GT: _P(bool, r)->setValue(retValue >  0); break;
      case INT_TO_LT: _P(bool, r)->setValue(retValue <  0); break;
      default: break;
    }
    return r->disposableResult();
  }
  cleaner.cancelOne();
  return result.first;
}

//============================================================================//
//::::::::::::::::::::::::::::::   OIterator   ::::::::::::::::::::::::::::::://
//============================================================================//

static const char* const implemFor = "implementation for the method '";
static const char* const required = "' must be provided";
static const char* const iteratorTypeName = "iterator::";

ORet OIterator::ommGetValue(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ iteratorTypeName }+"value",
                          implemFor+string{ "value" }+required)
}

ORet OIterator::ommSwap(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ iteratorTypeName }+"swap:",
                          implemFor+string{ "swap:" }+required)
}

ORet OIterator::inc(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ iteratorTypeName }+"operator++",
                          implemFor+string{ "inc" }+required)
}

//:::::::::::::::::::::::   OIterator - Method Table   ::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(OIterator, OInstance)
  PUBLIC(OIterator::ommGetValue,   "value")
  PUBLIC(OIterator::ommSwap,       "swap:")
  PUBLIC(OIterator::inc,           S_inc)
_END_METHOD_TABLE()

//:::::::::::::::::::::::::::   OIterator_Class   :::::::::::::::::::::::::::://

OIterator_Class::OIterator_Class(Session* m, const std::string& className) :
    OClass(m, className) {
  m->addRequirement(this, Requirement{m, "BasicIterator", nullptr, {},
      {"inc", "swap:", "value"}, {}});
  m->rethrow("iterator class::iterator class constructor");
}

OClass* OIterator_Class::createClass(Session* m,
      const std::string& className) const {
  OClass* cls = new OIterator_Class(m, this->name, className);
  m->rethrow("OIterator_Class::createClass");
  return cls;
}

OClass* OIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OIterator_Class(m, this, className);
  m->rethrow("OIterator_Class::createMutableClass");
  return cls;
}

//============================================================================//
//::::::::::::::::::::::::::::   OInputIterator   :::::::::::::::::::::::::::://
//============================================================================//

static const char* const inputIteratorTypeName = "'input iterator'::";

ORet OInputIterator::ommGetValue(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{inputIteratorTypeName}+"value",
                          implemFor+string{ "value" }+required)
}

ORet OInputIterator::ommSwap(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{inputIteratorTypeName}+"swap:",
                          implemFor+string{ "swap:" }+required)
}

ORet OInputIterator::ommComponent(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{inputIteratorTypeName}+"component:",
                          implemFor+string{ "component:" }+required)
}

ORet OInputIterator::inc(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{inputIteratorTypeName}+"operator++",
                          implemFor+string{ "inc" }+required)
}

ORet OInputIterator::eq(OArg, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{inputIteratorTypeName}+"operator==",
                          implemFor+string{ "eq:" }+required)
}

ORet OInputIterator::ne(OArg, int, OArg* argv, Session* m) {
  return s_genericOperation(S_eq, "operation!=", BOOL_FLIP, this, argv, m);
}

//:::::::::::::::::::::::   OInputIterator - Method Table   ::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(OInputIterator, OInstance)
  PUBLIC(OInputIterator::ommComponent,  S_component)
  PUBLIC(OInputIterator::ommGetValue,   "value")
  PUBLIC(OInputIterator::ommSwap,       "swap:")
  PUBLIC(OInputIterator::inc,           S_inc  )
  PUBLIC(OInputIterator::eq,            S_eq   )
  PUBLIC(OInputIterator::ne,            S_ne   )
_END_METHOD_TABLE()

//:::::::::::::::::::::::::::   OInputIterator_Class   :::::::::::::::::::::::::::://

OInputIterator_Class::OInputIterator_Class(Session* m,
    const std::string& className) : OClass(m, className) {
  m->addRequirement(this, Requirement{m, "InputIterator", nullptr,
      {"BasicIterator"}, {"component:", "eq", "ne"}, {}});
  m->rethrow("input iterator class::iterator class constructor");
}

OClass* OInputIterator_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OInputIterator_Class(m, this->name, className);
  m->rethrow("OInputIterator_Class::createClass");
  return cls;
}

OClass* OInputIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OInputIterator_Class(m, this, className);
  m->rethrow("OInputIterator_Class::createMutableClass");
  return cls;
}

//============================================================================//
//::::::::::::::::::::::::::::   OCountIterator   :::::::::::::::::::::::::::://
//============================================================================//

ORet OCountIterator::ommGetValue(OArg rec, int, OArg*, Session* m) {
  ORef r = BTI::getTypeClass<long>()->createObject(m);
  _RETHROW("count iterator::value");
  _P(long, r)->setValue(count_);
  return r->disposableResult();
}

ORet OCountIterator::ommSwap(OArg rec, int, OArg* argv, Session* m) {
  _ENTER_NOCHK("count iterator::swap:");
  _CHECK_ARG_TYPE(0, getRootClass());
  auto& r = *static_cast<OCountIterator*>(_ARG(0));
  std::swap(count_, r.count_);
  std::swap(step_, r.step_);
  return makeResult();
}

ORet OCountIterator::inc(OArg rec, int, OArg*, Session* m) {
  _MUTABLE_REC_METHOD("count iterator::inc")
  advance();
  return makeResult();
}

ORet OCountIterator::ommBeginStep(OArg rec, int argc, OArg* argv, Session* m) {
  _DECL_METHOD_NAME(argc > 1 ? "count iterator::begin:step:" :
                               "count iterator::begin:");
  _MUTABLE_REC();
  const difference_t begin = g_castValue<difference_t>(argv, m);
  _RETHROW_HERE();
  count_ = begin;
  if (argc > 2)
    _CHECK_ARGC_2(1, 2)
  if (argc > 1) {
    const difference_t step = g_castValue<difference_t>(argv + 1, m);
    _RETHROW_HERE();
    step_ = step;
  }
  return makeResult();
}

ORet OCountIterator::ommStep(OArg rec, int argc, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD("count iterator::step:");
  const difference_t step = g_castValue<difference_t>(argv, m);
  _RETHROW("count iterator::step:");
  step_ = step;
  return makeResult();
}

ORet OCountIterator::eq(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("count iterator::eq:", 1);
  _CHECK_ARG_TYPE(0, getRootClass());
  _RETURN_BOOL(count_ == static_cast<OCountIterator*>(_ARG(0))->count_);
}

ORet OCountIterator::ne(OArg rec, int argc, OArg* argv, Session* m) {
  _ENTER("count iterator::ne:", 1);
  _CHECK_ARG_TYPE(0, getRootClass());
  _RETURN_BOOL(count_ != static_cast<OCountIterator*>(_ARG(0))->count_);
}

//:::::::::::::::::::::::   OCountIterator - Method Table   ::::::::::::::::::::::://

_BEGIN_METHOD_TABLE(OCountIterator, OInputIterator)
  PUBLIC(OCountIterator::ommGetValue,   "value")
  PUBLIC(OCountIterator::ommSwap,       "swap:")
  PUBLIC(OCountIterator::ommStep,       "step:")
  PUBLIC(OCountIterator::ommBeginStep,  "begin:")
  PUBLIC(OCountIterator::ommBeginStep,  "value:")
  PUBLIC(OCountIterator::ommBeginStep,  "begin:step:")
  PUBLIC(OCountIterator::inc,           S_inc)
  PUBLIC(OCountIterator::eq,            S_eq)
  PUBLIC(OCountIterator::ne,            S_ne)
_END_METHOD_TABLE()

//:::::::::::::::::::::::::   OCountIterator_Class   ::::::::::::::::::::::::://

OCountIterator_Class::OCountIterator_Class(Session* m,
    const std::string& className) : OClass(m, className) {
  m->importRequirement(this, "InputIterator");
  m->rethrow("count iterator class::iterator class constructor");
}

OClass* OCountIterator_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OCountIterator_Class(m, this->name, className);
  m->rethrow("OCountIterator_Class::createClass");
  return cls;
}

OClass* OCountIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OCountIterator_Class(m, this, className);
  m->rethrow("OCountIterator_Class::createMutableClass");
  return cls;
}

ORef OCountIterator_Class::copyCore(ORef self, const_ORef r, Session*) const {
  OCountIterator& lhs = *static_cast<OCountIterator*>(self);
  const OCountIterator& rhs = *static_cast<const OCountIterator*>(r);
  lhs = rhs;
  return self;
}

ORef OCountIterator_Class::moveCore(ORef self, ORef r) const {
  OCountIterator& lhs = *static_cast<OCountIterator*>(self);
  OCountIterator& rhs = *static_cast<OCountIterator*>(r);
  lhs = std::move(rhs);
  return self;
}

//============================================================================//
//:::::::::::::::::::::::::::   OForwardIterator   ::::::::::::::::::::::::::://
//============================================================================//

static const char* const fwdIteratorTypeName = "'forward iterator'::";

ORet OForwardIterator::ommGetValue(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"value",
                          implemFor+string{ "value" }+required)
}

ORet OForwardIterator::ommSwap(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"swap:",
                          implemFor+string{ "swap:" }+required)
}

ORet OForwardIterator::ommComponent(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"component:",
                          implemFor+string{ "component:" }+required)
}

ORet OForwardIterator::inc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"operator++",
                          implemFor+string{ "inc" }+required)
}

ORet OForwardIterator::postinc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"operator++(post)",
                          implemFor+string{ "postinc" }+required)
}

ORet OForwardIterator::eq(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ fwdIteratorTypeName }+"operator==",
                          implemFor+string{ "eq:" }+required)
}

ORet OForwardIterator::ne(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_eq, "operation!=", BOOL_FLIP, this, argv, m);
}

//:::::::::::::::::::   OForwardIterator - Method Table   :::::::::::::::::::://

_BEGIN_METHOD_TABLE(OForwardIterator, OInstance)
  PUBLIC(OForwardIterator::ommComponent,  S_component)
  PUBLIC(OForwardIterator::ommGetValue,   "value"  )
  PUBLIC(OForwardIterator::ommSwap,       "swap:"  )
  PUBLIC(OForwardIterator::inc,           S_inc    )
  PUBLIC(OForwardIterator::postinc,       S_postinc)
  PUBLIC(OForwardIterator::eq,            S_eq     )
  PUBLIC(OForwardIterator::ne,            S_ne     )
_END_METHOD_TABLE()

//::::::::::::::::::::::::   OForwardIterator_Class   :::::::::::::::::::::::://

OForwardIterator_Class::OForwardIterator_Class(Session* m,
    const std::string& className) : OClass(m, className) {
  m->addRequirement(this, Requirement{m, "ForwardIterator", nullptr,
      {"InputIterator"}, {"postinc"}, {}});
  m->rethrow("forward iterator class::iterator class constructor");
}

OClass* OForwardIterator_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OForwardIterator_Class(m, this->name, className);
  m->rethrow("OForwardIterator_Class::createClass");
  return cls;
}

OClass* OForwardIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OForwardIterator_Class(m, this, className);
  m->rethrow("OForwardIterator_Class::createMutableClass");
  return cls;
}

//============================================================================//
//::::::::::::::::::::::::   OBidirectionalIterator   :::::::::::::::::::::::://
//============================================================================//

static const char* const bidirIteratorTypeName = "'bidirectional iterator'";

ORet OBidirectionalIterator::ommGetValue(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"value",
                          implemFor+string{ "value" }+required)
}

ORet OBidirectionalIterator::ommSwap(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"swap:",
                          implemFor+string{ "swap:" }+required)
}

ORet OBidirectionalIterator::ommComponent(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"component:",
                          implemFor+string{ "component:" }+required)
}

ORet OBidirectionalIterator::inc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"operator++",
                          implemFor+string{ "inc" }+required)
}

ORet OBidirectionalIterator::dec(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"operator--",
                          implemFor+string{ "dec" }+required)
}

ORet OBidirectionalIterator::postinc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"operator++(post)",
                          implemFor+string{ "postinc" }+required)
}

ORet OBidirectionalIterator::postdec(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"operator--(post)",
                          implemFor+string{ "postdec" }+required)
}

ORet OBidirectionalIterator::eq(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ bidirIteratorTypeName }+"operator==",
                          implemFor+string{ "eq:" }+required)
}

ORet OBidirectionalIterator::ne(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_eq, "operation!=", BOOL_FLIP, this, argv, m);
}

//::::::::::::::::   OBidirectionalIterator - Method Table   :::::::::::::::://

_BEGIN_METHOD_TABLE(OBidirectionalIterator, OInstance)
  PUBLIC(OBidirectionalIterator::ommComponent,  S_component)
  PUBLIC(OBidirectionalIterator::ommGetValue,   "value"  )
  PUBLIC(OBidirectionalIterator::ommSwap,       "swap:"  )
  PUBLIC(OBidirectionalIterator::inc,           S_inc    )
  PUBLIC(OBidirectionalIterator::dec,           S_dec    )
  PUBLIC(OBidirectionalIterator::postinc,       S_postinc)
  PUBLIC(OBidirectionalIterator::postdec,       S_postdec)
  PUBLIC(OBidirectionalIterator::eq,            S_eq     )
  PUBLIC(OBidirectionalIterator::ne,            S_ne     )
_END_METHOD_TABLE()

//:::::::::::::::::::::   OBidirectionalIterator_Class   ::::::::::::::::::::://

OBidirectionalIterator_Class::OBidirectionalIterator_Class(Session* m,
    const std::string& className) : OClass(m, className) {
  m->addRequirement(this, Requirement{m, "BidirectionalIterator", nullptr,
      {"ForwardIterator"}, {"dec", "postdec"}, {}});
  m->rethrow("bidirectional iterator class::iterator class constructor");
}

OClass* OBidirectionalIterator_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OBidirectionalIterator_Class(m, this->name, className);
  m->rethrow("OBidirectionalIterator_Class::createClass");
  return cls;
}

OClass* OBidirectionalIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new OBidirectionalIterator_Class(m, this, className);
  m->rethrow("OBidirectionalIterator_Class::createMutableClass");
  return cls;
}

//============================================================================//
//::::::::::::::::::::::::   ORandomAccessIterator   ::::::::::::::::::::::::://
//============================================================================//

static const char* const raIteratorTypeName = "'random access iterator'";

ORet ORandomAccessIterator::ommGetValue(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"value",
                          implemFor+string{ "value" }+required)
}

ORet ORandomAccessIterator::ommSwap(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"swap:",
                          implemFor+string{ "swap:" }+required)
}

ORet ORandomAccessIterator::ommComponent(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"component:",
                          implemFor+string{ "component:" }+required)
}

ORet ORandomAccessIterator::add(OArg rec, int, OArg* argv, Session* m) {
  oxType<difference_t> rhs{BTI::getTypeClass<difference_t>()};
  const auto value = g_castValue<difference_t>(argv, m);
  _RETHROW(s_getMethodName(this, "operator+"));
  rhs.setValue(-value);
  OVar var{&rhs};
  OArg a[1]{&var};
  return s_genericOperation(S_sub, "operator-", NO_HANDLING, this, a, m);
}

ORet ORandomAccessIterator::sub(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator-",
                          implemFor+string{ "sub:" }+required)
}

ORet ORandomAccessIterator::incBy(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator+=",
                          implemFor+string{ "" S_incBy "" }+required)
}

ORet ORandomAccessIterator::decBy(OArg rec, int, OArg* argv, Session* m) {
  _MUTABLE_REC_METHOD(s_getMethodName(this, "operator-="));
  oxType<difference_t> rhs{BTI::getTypeClass<difference_t>()};
  const auto value = g_castValue<difference_t>(argv, m);
  _RETHROW(s_getMethodName(this, "operator-="));
  rhs.setValue(-value);
  OVar var{&rhs};
  OArg a[1]{&var};
  return s_genericOperation(S_incBy, "operator-=", NO_HANDLING, this, a, m);
}

ORet ORandomAccessIterator::inc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator++",
                          implemFor+string{ "inc" }+required)
}

ORet ORandomAccessIterator::dec(OArg rec, int, OArg* argv, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator--",
                          implemFor+string{ "dec" }+required)
}

ORet ORandomAccessIterator::postinc(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator++(post)",
                          implemFor+string{ "postinc" }+required)
}

ORet ORandomAccessIterator::postdec(OArg rec, int, OArg*, Session* m) {
  _THROW_BAD_USAGE_METHOD(string{ raIteratorTypeName }+"operator--(post)",
                          implemFor+string{ "postdec" }+required)
}

ORet ORandomAccessIterator::eq(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator==", INT_TO_EQ, this, argv, m);
}

ORet ORandomAccessIterator::ne(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator!=", INT_TO_NE, this, argv, m);
}

ORet ORandomAccessIterator::ge(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator>=", INT_TO_GE, this, argv, m);
}

ORet ORandomAccessIterator::le(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator<=", INT_TO_LE, this, argv, m);
}

ORet ORandomAccessIterator::gt(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator>", INT_TO_GT, this, argv, m);
}

ORet ORandomAccessIterator::lt(OArg rec, int, OArg* argv, Session* m) {
  return s_genericOperation(S_sub, "operator<", INT_TO_LT, this, argv, m);
}

//:::::::::::::::::   ORandomAccessIterator - Method Table   ::::::::::::::::://

_BEGIN_METHOD_TABLE(ORandomAccessIterator, OInstance)
  PUBLIC(ORandomAccessIterator::ommComponent,  S_component)
  PUBLIC(ORandomAccessIterator::ommGetValue,   "value"  )
  PUBLIC(ORandomAccessIterator::ommSwap,       "swap:"  )
  PUBLIC(ORandomAccessIterator::add,           S_add    )
  PUBLIC(ORandomAccessIterator::sub,           S_sub    )
  PUBLIC(ORandomAccessIterator::incBy,         S_incBy  )
  PUBLIC(ORandomAccessIterator::decBy,         S_decBy  )
  PUBLIC(ORandomAccessIterator::inc,           S_inc    )
  PUBLIC(ORandomAccessIterator::dec,           S_dec    )
  PUBLIC(ORandomAccessIterator::postinc,       S_postinc)
  PUBLIC(ORandomAccessIterator::postdec,       S_postdec)
  PUBLIC(ORandomAccessIterator::eq,            S_eq     )
  PUBLIC(ORandomAccessIterator::ne,            S_ne     )
  PUBLIC(ORandomAccessIterator::gt,            S_gt     )
  PUBLIC(ORandomAccessIterator::lt,            S_lt     )
  PUBLIC(ORandomAccessIterator::ge,            S_ge     )
  PUBLIC(ORandomAccessIterator::le,            S_le     )
_END_METHOD_TABLE()

//:::::::::::::::::::::   ORandomAccessIterator_Class   :::::::::::::::::::::://

ORandomAccessIterator_Class::ORandomAccessIterator_Class(Session* m,
    const std::string& className) : OClass(m, className) {
  m->addRequirement(this, Requirement{m, "RandomAccessIterator", nullptr,
      {"BidirectionalIterator"},
      {S_add, S_sub, S_incBy, S_decBy, S_gt, S_lt, S_ge, S_le}, {}});
  m->rethrow("'random access iterator class'::'iterator class constructor'");
}

OClass* ORandomAccessIterator_Class::createClass(Session* m,
    const std::string& className) const {
  OClass* cls = new ORandomAccessIterator_Class(m, this->name, className);
  m->rethrow("ORandomAccessIterator_Class::createClass");
  return cls;
}

OClass* ORandomAccessIterator_Class::createMutableClass(Session* m,
    const std::string& className) const {
  OClass* cls = new ORandomAccessIterator_Class(m, this, className);
  m->rethrow("ORandomAccessIterator_Class::createMutableClass");
  return cls;
}

//============================================================================//
//:::::::::::::::::::::::::::   Class Installer   :::::::::::::::::::::::::::://
//============================================================================//

bool g_installIteratorClasses(Session* m) {
  extern OClass* _OXType_class[];
  OClass* cls;

  cls = new OIterator_Class(m, "iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_ITER);
  _OXType_class[CLSID_ITER] = cls;

  cls = new OInputIterator_Class(m, "input iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_INPITER);
  _OXType_class[CLSID_INPITER] = cls;

  cls = new OForwardIterator_Class(m, "forward iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_FWDITER);
  _OXType_class[CLSID_FWDITER] = cls;

  cls = new OBidirectionalIterator_Class(m, "bidirectional iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_BIDITER);
  _OXType_class[CLSID_BIDITER] = cls;

  cls = new ORandomAccessIterator_Class(m, "random access iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_RAITER);
  _OXType_class[CLSID_RAITER] = cls;

  cls = new OCountIterator_Class(m, "count iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_COUNTITER);
  _OXType_class[CLSID_COUNTITER] = cls;

  return true;
}
