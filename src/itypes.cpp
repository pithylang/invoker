#include <algorithm>
#include <charconv>
#include "itypes.hpp"
#include "iparser.hpp"
#include "iparseTools.hpp"
#include "selectors.h"

using std::string;
using std::vector;

//============================================================================//
//::::::::::::::::::::::::::::::::  ISymbol  ::::::::::::::::::::::::::::::::://
//============================================================================//
ISymbol::ISymbol(const char *symbolName, ushort symbolType) :
    name{symbolName}, type{symbolType}, access{0} {}

ISymbol::ISymbol(const char *begin, int length, ushort symbolType) :
    name{begin, begin + length}, type{symbolType}, access{0} {}

ISymbol::ISymbol(string&& text, ushort symbolType) :
    name{std::move(text)}, type{symbolType}, access{0} {}

ISymbol::ISymbol(ISymbol&& r) : name{std::move(r.name)} {
  std::swap(type, r.type); std::swap(access, r.access);
}

ISymbol::ISymbol(const ISymbol& r) :
    name{r.name}, type(r.type), access(r.access) {}

ISymbol& ISymbol::operator=(ISymbol&& r) {
  type = r.type;
  access = r.access;
  name = std::move(r.name);
  return *this;
}

int ISymbol::getBlockId() const noexcept {
  if (name.length() < 2 || VAR_TYPE(type) != SYMTYPE_BLOCK || name[0] != '@')
    return -1;
  char *endptr;
  const auto i = strtol(name.c_str() + 1, &endptr, 10);
  return *endptr == 0 ? i : -1;
}

int ISymbol::getIntegerBase() const noexcept {
  switch (type & SYMTYPE_FORMAT_MASK) {
    case 0: return 10;
    case SYMTYPE_LITERAL_HEXA: return 16;
    case SYMTYPE_LITERAL_BINARY: return 2;
  }
  return 0;
}

//:::::::::::::::::::::::::::   Type Conversions   ::::::::::::::::::::::::::://

template<typename t> concept _Integral = std::is_integral_v<t>;
template<typename t> concept _FP = std::is_floating_point_v<t>;

static string s_toCharsErrorHandler(const std::to_chars_result& r,
    const char* end) {
  if (r.ec != std::errc{})
    return std::make_error_code(r.ec).message();
  else if (r.ptr == end)
    return "value too large";
  return "";
}

template<_Integral t>
  std::pair<string, bool> toString(t value, int base) {
    constexpr size_t SIZE = 80;
    char buf[SIZE];
    const auto result = std::to_chars(buf, buf + SIZE, value, base);
    const string msg = s_toCharsErrorHandler(result, buf + SIZE);
    if (!msg.empty()) return {msg, false};
    *result.ptr = '\0';
    return {buf, true};
  }

template<_FP t>
  std::pair<string, bool> toString(t value, std::chars_format fmt) {
    constexpr size_t SIZE = 80;
    char buf[SIZE];
    const auto result = std::to_chars(buf, buf + SIZE, value, fmt);
    const string msg = s_toCharsErrorHandler(result, buf + SIZE);
    if (!msg.empty()) return {msg, false};
    *result.ptr = '\0';
    return {buf, true};
  }
template<_FP t>
  inline std::pair<string, bool> toString(t value)
  { return toString(value, std::chars_format::general); }

static string s_fromCharsErrorHandler(const std::from_chars_result& r,
    const char* end) {
  if (r.ec == std::errc{} && r.ptr == end)
    return "";
  if (r.ec == std::errc::invalid_argument)
    return "not a number";
  if (r.ec == std::errc::result_out_of_range)
    return "number is out of range";
  return "non-digits at and of number";
}

template<_Integral t>
  typename std::pair<t, string> fromString(const string& id, int base) {
    int start = 0;
    bool applyMinusAtEnd = id[0] == '-';
    if constexpr (std::is_unsigned_v<t>) {
      if (applyMinusAtEnd)
        return {t{}, "unsigned integers cannot have a sign"};
    }
    else {
      if (applyMinusAtEnd && (base == 2 || base == 16))
        start = 1;
      else
        applyMinusAtEnd = false;
    }
    if (id[0] == '+')
      start = 1;
    if ((base == 16 && id.size() > start + 2 &&
         id[start] == '0' && toupper(id[start + 1]) == 'X') ||
        (base == 2 && id.size() > start + 2 &&
         id[start] == '0' && toupper(id[start + 1]) == 'B'))
      start += 2;
    const char* begin = id.c_str() + start;
    const char* end = id.c_str() + id.size();
    t value;
    const auto result = std::from_chars(begin, end, value, base);
    const string msg = s_fromCharsErrorHandler(result, end);
    if (msg.empty() && applyMinusAtEnd)
      value = -value;
    return {value, msg};
  }

template<_FP t>
  typename std::pair<t, string> fromString(const string& id, std::chars_format fmt) {
    const int start = id[0] == '+' ? 1 : 0;
    const char* begin = id.c_str() + start;
    const char* end = id.c_str() + id.size();
    t value;
    const auto result = std::from_chars(begin, end, value, fmt);
    const string msg = s_fromCharsErrorHandler(result, end);
    return {value, msg};
  }
template<_FP t>
  typename std::pair<t, string> fromString(const string& id, int = -1)
  { return fromString<t>(id, std::chars_format::general); }

//:::::::::::::::::::::::   Inline Unary Operations   :::::::::::::::::::::::://

enum OperationType {
  O__not,
  O_bitinv,
  O_add,
  O_sub,
  O_mul,
  O_div,
  O_mod,
  O_bitor,
  O_bitxor,
  O_bitand,
  O_shiftr,
  O_shiftl,
};

// static const std::pair<string, bool> NOT_HANDLED{"", false};
static const ISymbol::BOPResult NOT_HANDLED{"", 0, false};

static bool s_notHandled(const std::pair<string, bool>& r) noexcept
{ return r.first.empty() && !r.second; }

template<typename t>
  std::pair<string, bool> ISymbol::unaryOperation(int op, int base) const {
    auto [value, msg] = fromString<t>(name, base);
    if (!msg.empty()) return {msg, false};
    else if (op == O__not  ) value = !value;
    else if (op == O_bitinv) value = ~value;
    else return {"unexpected operation", false};
    return toString(value, base);
  }

string ISymbol::applyUnaryOperation(int op, const yy::location& loc) const {
  bool success;
  string value;
  const int base = getIntegerBase();

  switch (getLiteralType()) {
    case SYMTYPE_LITERAL_ULONG:
      std::tie(value, success) = unaryOperation<ulong>(op, base);
      break;

    case SYMTYPE_LITERAL_LONG:
      std::tie(value, success) = unaryOperation<long>(op, base);
      break;

    case SYMTYPE_LITERAL_UINT:
      std::tie(value, success) = unaryOperation<uint>(op, base);
      break;

    case SYMTYPE_LITERAL_INT:
      std::tie(value, success) = unaryOperation<int>(op, base);
      break;

    case SYMTYPE_LITERAL_SHORT:
      std::tie(value, success) = unaryOperation<short>(op, base);
      break;

    case SYMTYPE_LITERAL_CHAR:
      std::tie(value, success) = unaryOperation<char>(op, base);
      break;

    default:
      return "";
  }

  if (!success) // value holds an error message
    throw yy::parser::syntax_error{loc, value};
  return value;
}

//:::::::::::::::::::::::   Inline Binary Operations   ::::::::::::::::::::::://

#define _DECL_INLINE_OPERATION(opname, opsym)                          \
template<typename t, typename s>                                       \
  struct opname {                                                      \
    using result_type = decltype(t{} opsym s{});                       \
    static inline result_type op(t lhs, s rhs) {return lhs opsym rhs;} \
  };

template<template<typename t, typename s> typename _Op, typename t, typename s>
  typename _Op<t,s>::result_type s_applyOp(t lhs, s rhs) {
    return _Op<t,s>::op(lhs, rhs);
  }

_DECL_INLINE_OPERATION(_Add,     +)
_DECL_INLINE_OPERATION(_Sub,     -)
_DECL_INLINE_OPERATION(_Mul,     *)
_DECL_INLINE_OPERATION(_Div,     /)
_DECL_INLINE_OPERATION(_Mod,     %)
_DECL_INLINE_OPERATION(_Bitor,   |)
_DECL_INLINE_OPERATION(_Bitxor,  ^)
_DECL_INLINE_OPERATION(_Bitand,  &)
_DECL_INLINE_OPERATION(_Shiftr, >>)
_DECL_INLINE_OPERATION(_Shiftl, <<)

template<typename t>
ushort getSymbolType() noexcept {
  using T = typename std::remove_cv<t>::type;
  if constexpr (std::is_same_v<T, ulong> ) return SYMTYPE_LITERAL_ULONG;
  if constexpr (std::is_same_v<T, long>  ) return SYMTYPE_LITERAL_LONG;
  if constexpr (std::is_same_v<T, uint>  ) return SYMTYPE_LITERAL_UINT;
  if constexpr (std::is_same_v<T, int>   ) return SYMTYPE_LITERAL_INT;
  if constexpr (std::is_same_v<T, short> ) return SYMTYPE_LITERAL_SHORT;
  if constexpr (std::is_same_v<T, char>  ) return SYMTYPE_LITERAL_CHAR;
  if constexpr (std::is_same_v<T, bool>  ) return SYMTYPE_LITERAL_BOOL;
  if constexpr (std::is_same_v<T, double>) return SYMTYPE_LITERAL_DOUBLE;
  if constexpr (std::is_same_v<T, float> ) return SYMTYPE_LITERAL_FLOAT;
  else return 0;
}

using BOPResult = ISymbol::BOPResult;

template<typename t>
  BOPResult ISymbol::applyBOPWithLHSType(int op, const ISymbol& rhs) const {
    int base = -1;
    if constexpr (std::is_integral_v<t>) {
      base = getIntegerBase();
    }
    auto [value, msg] = fromString<t>(name, base);
    if (!msg.empty()) return BOPResult{msg, 0, false};

    BOPResult result;
    switch (rhs.getLiteralType()) {
      case SYMTYPE_LITERAL_ULONG:
        result = rhs.applyReverseBOP<t, ulong>(op, value, base);
        break;
      case SYMTYPE_LITERAL_LONG:
        result = rhs.applyReverseBOP<t, long>(op, value, base);
        break;
      case SYMTYPE_LITERAL_UINT:
        result = rhs.applyReverseBOP<t, uint>(op, value, base);
        break;
      case SYMTYPE_LITERAL_INT:
        result = rhs.applyReverseBOP<t, int>(op, value, base);
        break;
      case SYMTYPE_LITERAL_SHORT:
        result = rhs.applyReverseBOP<t, short>(op, value, base);
        break;
      case SYMTYPE_LITERAL_CHAR:
        result = rhs.applyReverseBOP<t, char>(op, value, base);
        break;
      case SYMTYPE_LITERAL_DOUBLE:
        result = rhs.applyReverseBOP<t, double>(op, value, base);
        break;
      case SYMTYPE_LITERAL_FLOAT:
        result = rhs.applyReverseBOP<t, float>(op, value, base);
        break;
      default:
        return NOT_HANDLED;
    }

    return result;
  }

template<_Integral t>
  BOPResult convertResult(t result, int base) {
    BOPResult r{.symbolType = 0};
    std::tie(r.value, r.success) = toString(result, base);
    if (r.success) r.symbolType = getSymbolType<t>();
    return r;
  }
template<_FP t>
  BOPResult convertResult(t result, int) {
    BOPResult r{.symbolType = 0};
    std::tie(r.value, r.success) = toString(result);
    if (r.success) r.symbolType = getSymbolType<t>();
    return r;
  }

template<template<typename t, typename s> typename _Op, typename t, typename s>
requires (std::is_integral_v<t> && std::is_integral_v<s>)
  BOPResult applyIntConvertResult(t value1, s value2, int base) {
    BOPResult r{.symbolType = 0};
    const auto result = s_applyOp<_Op>(value1, value2);
    std::tie(r.value, r.success) = toString(result, base);
    if (r.success) r.symbolType = getSymbolType<decltype(result)>();
    return r;
  }
template<template<typename t, typename s> typename _Op, typename t, typename s>
requires (!(std::is_integral_v<t> && std::is_integral_v<s>))
  BOPResult applyIntConvertResult(t, s, int) { return NOT_HANDLED; }

template<typename t, typename s>
  BOPResult ISymbol::applyReverseBOP(int op, t value1, int base1) const {
    int base2 = -1;
    std::pair<s, string> conversionResult;
    if constexpr (std::is_integral_v<s>) {
      base2 = getIntegerBase();
      conversionResult = fromString<s>(name, base2);
    }
    else if constexpr (std::is_floating_point_v<s>) {
      conversionResult = fromString<s>(name, std::chars_format::general);
    }
    else {
      return NOT_HANDLED;
    }
    string msg = std::move(conversionResult.second);
    if (!msg.empty()) return BOPResult{msg, 0, false};
    s value2 = conversionResult.first;
    switch (op) {
      case O_add:
        return convertResult(s_applyOp<_Add>(value1, value2), base1);
      case O_sub:
        return convertResult(s_applyOp<_Sub>(value1, value2), base1);
      case O_mul:
        return convertResult(s_applyOp<_Mul>(value1, value2), base1);
      case O_div:
        return convertResult(s_applyOp<_Div>(value1, value2), base1);
      case O_mod:
        return applyIntConvertResult<_Mod>(value1, value2, base1);
      case O_bitor:
        return applyIntConvertResult<_Bitor>(value1, value2, base1);
      case O_bitxor:
        return applyIntConvertResult<_Bitxor>(value1, value2, base1);
      case O_bitand:
        return applyIntConvertResult<_Bitand>(value1, value2, base1);
      case O_shiftr:
        return applyIntConvertResult<_Shiftr>(value1, value2, base1);
      case O_shiftl:
        return applyIntConvertResult<_Shiftl>(value1, value2, base1);
    }
    return NOT_HANDLED;
  }

ISymbol ISymbol::applyBinaryOperation(int op, const ISymbol& rhs,
                                      const yy::location& loc) const {
  BOPResult result;

  switch (getLiteralType()) {
    case SYMTYPE_LITERAL_ULONG:  result = applyBOPWithLHSType<ulong>(op, rhs);
      break;
    case SYMTYPE_LITERAL_LONG:   result = applyBOPWithLHSType<long>(op, rhs);
      break;
    case SYMTYPE_LITERAL_UINT:   result = applyBOPWithLHSType<uint>(op, rhs);
      break;
    case SYMTYPE_LITERAL_INT:    result = applyBOPWithLHSType<int>(op, rhs);
      break;
    case SYMTYPE_LITERAL_SHORT:  result = applyBOPWithLHSType<short>(op, rhs);
      break;
    case SYMTYPE_LITERAL_CHAR:   result = applyBOPWithLHSType<char>(op, rhs);
      break;
    case SYMTYPE_LITERAL_DOUBLE: result = applyBOPWithLHSType<double>(op, rhs);
      break;
    case SYMTYPE_LITERAL_FLOAT:  result = applyBOPWithLHSType<float>(op, rhs);
      break;
    default:
      return ISymbol{""}; // not handled
  }

  if (!result.success) {
    if (result.value.empty()) // not handled
      return ISymbol{""};
    else // value holds an error message
      throw yy::parser::syntax_error{loc, result.value};
  }
  return ISymbol{std::move(result.value), result.symbolType};
}

//============================================================================//
//:::::::::::::::::::::::::::::::::  IItem  :::::::::::::::::::::::::::::::::://
//============================================================================//
IItemType::IItemType() { new(buffer) IVariant(); }

IItemType::~IItemType() {
  reinterpret_cast<IVariant*>(buffer)->~IVariant();
}

IItem::IItem(IItem&& item) { *this = std::move(item); }

IItem& IItem::operator=(IItem&& r) {
  *static_cast<IItemType *>(this) = std::move(r);
  return *this;
}

IItem& IItem::operator=(ISymbol&& simpleItem) {
  static_cast<IVariant&>(*this) = std::move(simpleItem);
  return *this;
}

IItem& IItem::operator=(IUnit&& unit) {
  IUnit *newUnit = new IUnit(std::move(unit));
  static_cast<IVariant&>(*this) = std::unique_ptr<IUnit>(newUnit);
  return *this;
}

IItem& IItem::operator=(IReceivable&& r) {
  static_cast<IVariant&>(*this) = std::move(r);
  return *this;
}

IItem& IItem::operator=(IBlock&& block) {
  static_cast<IVariant&>(*this) = std::move(block);
  return *this;
}

IItem& IItem::operator=(IArrayExpression&& r) {
  static_cast<IVariant&>(*this) = std::move(r);
  return *this;
}

IItem& IItem::operator=(ISetExpression&& r) {
  static_cast<IVariant&>(*this) = std::move(r);
  return *this;
}

void IItem::setMemberAccessExpression(IItem&& item, ISymbol&& id,
                                      const yy::location& location) {
  if (item.is<IBlock>() && !item.get<IBlock>().evaluate)
    throw yy::parser::syntax_error(location, "blocks have no members");
  vector<IUnit> argList(1);
  id.type = SYMTYPE_LITERAL_STRING;
  argList[0] = std::move(id);
  IReceivable r{string{"#gp"}, std::move(argList)};
  IUnit unit{std::move(item), std::move(r)};
  unit.loc = location;
  *this = std::move(unit);
}

void IItem::setSubscriptedExpression(IItem&& item, IUnit&& expr,
                                     const yy::location& location) {
  if (item.is<IBlock>() && !item.get<IBlock>().evaluate)
    throw yy::parser::syntax_error(location, "blocks cannot have subscripts");
  vector<IUnit> argList(1);
  argList[0] = std::move(expr);
  IReceivable r{string{"component:"}, std::move(argList)};
  IUnit unit{std::move(item), std::move(r)};
  unit.loc = location;
  *this = std::move(unit);
}

#if 0
void IItem::makeBlockInvocation(ISymbol&& sym, const yy::location& location) {
  ISymbol sel("#bl", SYMTYPE_LITERAL_SEL);
  *this = IUnit(IItem(std::move(sym)), IReceivable(std::move(sel)));
}

void IItem::makeBlockInvocation(IItem&& item, const yy::location& location) {
  ISymbol sel("#bl", SYMTYPE_LITERAL_SEL);
  *this = IUnit(std::move(item), IReceivable(std::move(sel)));
}
#endif // 0

void IItem::fromStringWithAttrs(const IParser& parseServices,
                                string&& id, size_t attrs,
                                const yy::location& location) {
  switch (attrs) {
    case SAT_NONE: {
      ISymbol name{std::move(id), SYMTYPE_LITERAL_STRING};
      *this = std::move(name);
      break;
    }
    case SAT_AUTOVAR:
    case SAT_NOAUTOVAR:
    case SAT_CONSTVAR: {
      ISymbol rec{parseServices.systemName.c_str(), SYMTYPE_LITERAL_ID};
      ISymbol arg{std::move(id), SYMTYPE_LITERAL_STRING};
      vector<IUnit> argList;
      argList.emplace_back(IItem{std::move(arg)});
      string sel{attrs == SAT_AUTOVAR   ? "auto:" :
                 attrs == SAT_NOAUTOVAR ? "noauto:" :
                                          "const:"};
      IUnit u{IItem{std::move(rec)},
              IReceivable{std::move(sel), std::move(argList)}};
      u.loc = location;
      *this = std::move(u);
      break;
    }
    case SAT_CLASSOBJ: {
      ISymbol rec{parseServices.systemName.c_str(), SYMTYPE_LITERAL_ID};
      ISymbol arg{std::move(id), SYMTYPE_LITERAL_STRING};
      vector<IUnit> argList;
      argList.emplace_back(IItem{std::move(arg)});
      IUnit u{IItem{std::move(rec)},
              IReceivable{string{"getClass:"}, std::move(argList)}};
      u.loc = location;
      *this = std::move(u);
      break;
    }
    case SAT_PACKAGE: {
      // build receiver unit
      IItem recItem;
      string typeName{"package"};
      vector<IUnit> initList;
      initList.emplace_back(ISymbol{std::move(id), SYMTYPE_LITERAL_STRING});
      recItem.fromInitListExpr(parseServices, std::move(typeName),
          SAT_AUTOVAR, std::move(initList), location);
      // build main unit
      ISymbol rec{parseServices.systemName.c_str(), SYMTYPE_LITERAL_ID};
      vector<IUnit> argList;
      argList.emplace_back(std::move(recItem));
      string sel{"base:member:value:"};
      IUnit u{IItem{std::move(rec)},
              IReceivable{std::move(sel), std::move(argList)}};
      u.loc = location;
      *this = std::move(u);
      break;
    }
  }
}

void IItem::fromTemplateInitExpr(const IParser& parseServices,
                                 string&& id,
                                 size_t attrs,
                                 vector<IUnit>&& cexpr,
                                 const yy::location& location) {
  switch (attrs) {
    case SAT_AUTOVAR:
    case SAT_CONSTVAR: {
      ISymbol rec{parseServices.systemName.c_str(), SYMTYPE_LITERAL_ID};
      ISymbol arg{std::move(id), SYMTYPE_LITERAL_STRING};
      vector<IUnit> argList{std::move(cexpr)};
      argList.emplace_back(IItem{std::move(arg)});
      string sel{attrs == SAT_AUTOVAR ? "crofta:" : "crcofta:"};
      *this = IUnit{IItem{std::move(rec)},
                    IReceivable{std::move(sel), std::move(argList)}};
      break;
    }
    case SAT_PACKAGE:
      throw yy::parser::syntax_error(location,
          "incorrect package specification");
    default:
      throw yy::parser::syntax_error(location, "incorrect type specification "
          "in template parameter initialization expression");
  }
}

void IItem::fromInitListExpr(const IParser& parseServices,
                             string&& id,
                             size_t attrs,
                             vector<IUnit>&& cexpr,
                             const yy::location& location) {
  switch (attrs) {
    case SAT_AUTOVAR:
    case SAT_CONSTVAR:
    case SAT_NOAUTOVAR: {
      ISymbol rec{parseServices.systemName.c_str(), SYMTYPE_LITERAL_ID};
      ISymbol arg{std::move(id), SYMTYPE_LITERAL_STRING};
      vector<IUnit> argList{std::move(cexpr)};
      argList.emplace_back(IItem{std::move(arg)});
      string sel{attrs == SAT_AUTOVAR   ? "crofil:" :
                 attrs == SAT_NOAUTOVAR ? "crnofil:" : "crcofil:"};
      *this = IUnit{IItem{std::move(rec)},
                    IReceivable{std::move(sel), std::move(argList)}};
      break;
    }
    default:
      throw yy::parser::syntax_error(location, "incorrect type specification "
          "in initializer list expression");
  }
}

void IItem::setArrayWithItemExpansion(std::vector<IUnit>&& cexpr, IItem&& a,
                                      const yy::location& loc, bool atEnd) {
  ; // TODO
}

void IItem::setSetWithItemExpansion (std::vector<IUnit>&& cexpr, IItem&& s,
                                      const yy::location& loc, bool atEnd) {
  ; // TODO
}

void IItem::throwIfBlock(const yy::location& location) const {
  if (is<IBlock>())
    throw yy::parser::syntax_error(location, "block receiver (to invoke a "
        "lambda add a '@' in front of the block)");
}

//============================================================================//
//::::::::::::::::::::::::::::::  IReceivable  ::::::::::::::::::::::::::::::://
//============================================================================//
IReceivable::IReceivable() : method("", SYMTYPE_LITERAL_SEL) {}

IReceivable::IReceivable(ISymbol&& selector) { *this = std::move(selector); }

IReceivable::IReceivable(ISymbol&& selector, IUnit&& expr) :
    method(std::move(selector)), args(1) { args[0] = std::move(expr); }

IReceivable::IReceivable(std::string&& selectorName, vector<IUnit>&& argList) :
    method(ISymbol(std::move(selectorName), SYMTYPE_LITERAL_SEL)),
    args(std::move(argList)) {}

IReceivable::IReceivable(IReceivable&& r) :
    method(std::move(r.method)),
    args(std::move(r.args)) {}

IReceivable& IReceivable::operator=(IReceivable&& r) {
  method = std::move(r.method);
  args = std::move(r.args);
  return *this;
}

IReceivable& IReceivable::operator=(ISymbol&& symbol) {
  method = std::move(symbol);
  method.type = SYMTYPE_LITERAL_SEL;
  args = std::vector<IUnit>(0);
  return *this;
}

IReceivable& IReceivable::renderArgList(ISymbol&& newSelector) {
  void g_replaceAll(string& s, const string& from, const string& to);
  // split selector name into keys
  vector<string> keys = splitSelector(":");
  ASSERT(!keys.empty());
  if (!keys.back().empty())
    throw string("improper ISON definition");
  ASSERT(keys.size() == this->args.size() + 1);
  // create new arg list
  vector<IUnit> newArgs;
  vector<string>::size_type i = 0;
  for (auto& arg : args) {
    // make key argument
    string key = std::move(keys[i++]);
    if (key.empty()) {
      if (!arg.isItem() || !arg.receiver.is<ISymbol>())
        throw string{"improper ISON definition"};
      else
        key = arg.receiver.get<ISymbol>().name;
    }
    g_replaceAll(key, "\x1f", ":");
    newArgs.emplace_back(ISymbol{std::move(key), SYMTYPE_LITERAL_STRING});
    newArgs.push_back(std::move(arg));
  }
  // Execute statement
  method = ISymbol{std::move(newSelector.name), SYMTYPE_LITERAL_SEL};
  args.swap(newArgs);
  return *this;
}

IReceivable operator+(IReceivable&& a, IReceivable&& b) {
  if (b.method.name.size() != 1 && a.method.name.size() > 1 &&
      a.method.name[a.method.name.size() - 2] == ':') {
    ASSERT(!b.args.empty());
    const auto& loc = b.args.front().loc;
    throw yy::parser::syntax_error(loc, "incorrect selector specification");
  }
  IReceivable r;
  r.args.reserve(a.args.size() + b.args.size());
  r = std::move(a);
  r.method.name += b.method.name;
  for (auto& arg : b.args)
    r.args.push_back(std::move(arg));
  return r;
}

//============================================================================//
//:::::::::::::::::::::::::::::::::  IUnit  :::::::::::::::::::::::::::::::::://
//============================================================================//
IUnit::IUnit(IUnit&& r) :
    receiver   {std::move(r.receiver)  },
    receivable {std::move(r.receivable)},
    loc        {std::move(r.loc)       } {}

IUnit::IUnit(IItem&& item) : receiver{std::move(item)} {}

IUnit::IUnit(IItem&& item, IReceivable&& r) :
    receiver{std::move(item)}, receivable{std::move(r)} {}

IUnit& IUnit::operator=(IItem&& item) {
  receiver = std::move(item);
  return *this;
}

IUnit& IUnit::operator=(IUnit&& r) {
  receiver = std::move(r.receiver);
  receivable = std::move(r.receivable);
  loc = std::move(r.loc);
  return *this;
}

static void s_makeEncapsulatedBlock(IUnit&& expr);

bool IUnit::isClassDeclaration(const IParser& parser) const noexcept {
  return receiver.is<ISymbol>() && !receivable.isNull() &&
         parser.isClassDeclaration(receivable.method.name);
}

bool IUnit::isVCMNotUsableAsArg(const IParser& parser) const noexcept {
  string g_getStdMethodName(const string& originalMethodName);
  if (!receiver.is<ISymbol>() || receivable.isNull() || receivable.args.empty())
    return false;
  const ISymbol& symbol = receiver.get<ISymbol>();
  if (!(symbol.name == parser.systemName && symbol.type == SYMTYPE_LITERAL_ID))
    return false;
  const auto methName = g_getStdMethodName(receivable.method.name);
  const auto vcm = parser.getVarCreateMethod(methName);
  return vcm != nullptr && !vcm->usableAsArg;
}

#if 0
bool IUnit::isLiteralNumber() const noexcept {
  return isItem() && receiver.is<ISymbol>() &&
      receiver.get<ISymbol>().isLiteralNumber();
}

bool IUnit::isLiteralInteger() const noexcept {
  return isItem() && receiver.is<ISymbol>() &&
      receiver.get<ISymbol>().isLiteralInteger();
}
#else
bool IUnit::isLiteralNumber() const noexcept {
  if (!isItem()) return false;
  const auto& rec = receiver.isEncapsulatedUnit() ?
      receiver.getEncapsulatedUnit().receiver : receiver;
  return rec.is<ISymbol>() && rec.get<ISymbol>().isLiteralNumber();
}

bool IUnit::isLiteralInteger() const noexcept {
  if (!isItem()) return false;
  const auto& rec = receiver.isEncapsulatedUnit() ?
      receiver.getEncapsulatedUnit().receiver : receiver;
  return rec.is<ISymbol>() && rec.get<ISymbol>().isLiteralInteger();
}
#endif // 0

void IUnit::throwIfReceiverIsBlock(const yy::location& location) const {
  if (!receiver.is<IBlock>() || isBlockEvaluation()) return;
  throw yy::parser::syntax_error(location, "block receiver (to invoke a "
      "lambda add a '@' in front of the block)");
}

void IUnit::setBinaryOperation(IUnit&& expr1, const char* op, IUnit&& expr2) {
  auto unitLocation = expr1.loc; unitLocation += expr2.loc;
  if (expr1.isLiteralNumber() && expr2.isLiteralNumber() &&
      handleLiteralBOP(op, expr1, expr2, unitLocation))
    return;
  IReceivable recbl(ISymbol(op, SYMTYPE_LITERAL_SEL), std::move(expr2));
  if (expr1.receivable.isNull()) {
    receiver = std::move(expr1.receiver);
    receivable = std::move(recbl);
  }
  else
    *this = IUnit(IItem(std::move(expr1)), std::move(recbl));
  loc = unitLocation;
}

bool IUnit::handleLiteralBOP(const char* op, const IUnit& expr1, const IUnit& expr2,
                                  const yy::location& location) {
  const string opName = op;
  const ISymbol& lhs = expr1.receiver.isEncapsulatedUnit() ?
      expr1.receiver.getEncapsulatedUnit().receiver.get<ISymbol>() :
      expr1.receiver.get<ISymbol>();
  const ISymbol& rhs = expr2.receiver.isEncapsulatedUnit() ?
      expr2.receiver.getEncapsulatedUnit().receiver.get<ISymbol>() :
      expr2.receiver.get<ISymbol>();
  int opId;
  /**/ if (opName == S_add   ) opId = O_add;
  else if (opName == S_sub   ) opId = O_sub;
  else if (opName == S_mul   ) opId = O_mul;
  else if (opName == S_div   ) opId = O_div;
  else if (opName == S_mod   ) opId = O_mod;
  else if (opName == S_bitor ) opId = O_bitor;
  else if (opName == S_bitxor) opId = O_bitxor;
  else if (opName == S_bitand) opId = O_bitand;
  else if (opName == S_shiftr) opId = O_shiftr;
  else if (opName == S_shiftl) opId = O_shiftl;
  else return false;
  ISymbol result = lhs.applyBinaryOperation(opId, rhs, location);
  if (result.name.empty())
    return false;
  receiver = std::move(result);
  loc = location;
  return true;
}

void IUnit::setUnaryOperation(const char* op, IUnit&& expr) {
  if (expr.isLiteralNumber() &&
      handleLiteralUOP(op, std::move(expr), expr.loc))
    return;
  if (string{op} == "+") { // ignore it
    *this = std::move(expr);
    return;
  }
  IReceivable recbl(ISymbol(op, SYMTYPE_LITERAL_SEL));
  if (expr.receivable.isNull()) {
    receiver = std::move(expr.receiver);
    receivable = std::move(recbl);
  }
  else
    *this = IUnit(IItem(std::move(expr)), std::move(recbl));
  loc = expr.loc;
}

bool IUnit::handleLiteralUOP(const char* op, IUnit&& expr,
                             const yy::location& location) {
  const string opName = op;
  if ((opName == S__not || opName == S_bitinv) && !expr.isLiteralInteger())
    return false;
  ISymbol& rhs = expr.receiver.isEncapsulatedUnit() ?
      expr.receiver.getEncapsulatedUnit().receiver.get<ISymbol>() :
      expr.receiver.get<ISymbol>();
  if ((opName == "+" || opName == S_neg) &&
      (rhs.name[0] == '+' || rhs.name[0] == '-'))
    throw yy::parser::syntax_error{location, "the number  is already signed"};
  bool handled = true;
  if (opName == "+")
    rhs.name = '+' + rhs.name;
  else if (opName == S_neg)
    rhs.name = '-' + rhs.name;
  else if (opName == S__not)
    rhs.name = rhs.applyUnaryOperation(O__not, location);
  else if (opName == S_bitinv)
    rhs.name = rhs.applyUnaryOperation(O_bitinv, location);
  else if (opName == S_postinc || opName == S_postdec ||
           opName == S_inc || opName == S_dec)
    handled = false;
  else
    handled = false;
  if (!handled)
    return false;
  receiver = std::move(rhs);
  loc = location;
  return true;
}

void IUnit::setAssignmentOperation(IUnit&& expr1, IUnit&& expr2,
                                   const yy::location& location,
                                   bool isConst) {
  static const string text[2]{"assignment to ", " (did you mean '=='?)"};
  const char* op = "#cp";
  ASSERT(expr1.isItem()); // TODO
  if (expr1.isItem()) {
    const auto& item = expr1.receiver;
    if (item.is<ISymbol>()) {
      if (item.get<ISymbol>().isLiteralValue())
        throw yy::parser::syntax_error{location, assignError("literal")};
      op = isConst ? "#ca" : "#ai";
    }
    else if (item.is<IBlock>() && !item.get<IBlock>().evaluate)
      throw yy::parser::syntax_error{location, assignError("code block")};
    else if (item.is<IArrayExpression>())
      op = isConst ? "#Da" : "#da";
    else if (isConst && item.isEncapsulatedUnit())
      throw yy::parser::syntax_error{location, assignError()};
  }
  setBinaryOperation(std::move(expr1), op, std::move(expr2));
}

/* static */
string IUnit::assignError(const char* what) {
  static const string text[3]{"assignment to ", " (did you mean '=='?)",
      "incorrect constant definition"};
  return what ? text[0]+"code block"+text[1] : text[2];
}

void IUnit::setReferenceOperation(IUnit&& expr1, IUnit&& expr2,
                                  const yy::location& location,
                                  bool isConst) {
  const char* op = isConst ? "#cr" : "#rf";
  if (expr1.isItem()) {
    const auto& item = expr1.receiver;
    static const string msg{" cannot be references (did you mean '==' ?)"};
    if (item.is<ISymbol>() && item.get<ISymbol>().isLiteralValue())
      throw yy::parser::syntax_error{location, "literals"+msg};
    else if (item.is<IBlock>() && !item.get<IBlock>().evaluate)
      throw yy::parser::syntax_error{location, "code blocks"+msg};
    else if (item.is<IArrayExpression>())
      op = isConst ? "#cd" : "#dr";
  }
  setBinaryOperation(std::move(expr1), op, std::move(expr2));
}

void IUnit::makeBlockInvocation(IItem&& item, const yy::location& location) {
  if (item.is<IBlock>()) {
    item.get<IBlock>().execute = false;
    item.get<IBlock>().evaluate = true;
    *this = std::move(item);
  }
  else {
    ISymbol sel{"#bl", SYMTYPE_LITERAL_SEL};
    *this = IUnit{std::move(item), IReceivable{std::move(sel)}};
  }
}

void IUnit::setConditionalOperation(bool inlineConditionalOperator,
    IUnit&& expr1, IUnit&& expr2, IUnit&& expr3, const yy::location& location) {
  s_makeEncapsulatedBlock(std::move(expr2));
  s_makeEncapsulatedBlock(std::move(expr3));
  string sel{inlineConditionalOperator ? "#cb" : "optFor:other:"};
  vector<IUnit> argList;
  argList.reserve(2);
  argList.push_back(std::move(expr2));
  argList.push_back(std::move(expr3));
  IReceivable recbl{std::move(sel), std::move(argList)};
  if (expr1.receivable.isNull()) {
    receiver = std::move(expr1.receiver);
    receivable = std::move(recbl);
  }
  else
    *this = IUnit{IItem{std::move(expr1)}, std::move(recbl)};
  loc = location;
}

void s_makeEncapsulatedBlock(IUnit&& expr) {
  if (expr.isEncapsulatedBlock()) return;
  vector<IUnit> stmList;
  stmList.push_back(std::move(expr));
  IBlock b;
  b = std::move(stmList);
  IItem item;
  item = std::move(b);
  expr = std::move(item);
  ASSERT(expr.isEncapsulatedBlock());
}

//============================================================================//
//:::::::::::::::::::::::::::::::::  IBlock  ::::::::::::::::::::::::::::::::://
//============================================================================//
IBlock::IBlock(IBlock&& r) {*this = std::move(r);}

IBlock& IBlock::operator=(vector<IUnit>&& r) {
  *(std::vector<IUnit>*)this = std::move(r);
  return *this;
}

IBlock& IBlock::operator=(IBlock&& r) {
  *(std::vector<IUnit>*)this = std::move(r);
  evaluate = r.evaluate;
  execute = r.execute;
  return *this;
}

//============================================================================//
//::::::::::::::::::::::::::::  Output Operators  :::::::::::::::::::::::::::://
//============================================================================//
std::ostream& operator<<(std::ostream& os, const ISymbol& r) {
  os << "ISymbol " << r.name << " (type " << r.type << ")";
  return os;
}

std::ostream& operator<<(std::ostream& os, const IItem& r) {
  os << "IItem ";
  switch (static_cast<const IVariant&>(r).which()) {
    case 0: os << "(empty)";
      break;
    case 1: os << "literal or ID [" << r.get<ISymbol>() << "]";
      break;
    case 2: os << "Unit";
      break;
    case 3: os << "ARRAY";
      break;
    case 4: os << "SET";
      break;
    case 5: os << "OBJECT";
      break;
    case 6: os << "Block";
      break;
  }
  return os;
}

std::ostream& operator<<(std::ostream& os, const IReceivable& r) {
  return os;
}

std::ostream& operator<<(std::ostream& os, const IUnit& r) {
  return os;
}

std::ostream& operator<<(std::ostream& os, const vector<IUnit>& r) {
  return os;
}

std::ostream& operator<<(std::ostream& os, const IStringWithAttrs& r) {
  os << r.name;
  switch (r.attrs) {
    case SAT_AUTOVAR:  os << "(auto construction)"; break;
    case SAT_CONSTVAR: os << "(const construction)"; break;
    case SAT_CLASSOBJ: os << "(class object)"; break;
  }
  return os;
}
