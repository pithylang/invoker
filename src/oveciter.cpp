#include "oveciter.hpp"
#include "otypeProxy.hpp"
#include "otype.hpp"
#include "ortmgr.hpp"

using std::string;
using std::vector;

_TEMPLATE_DEFINITION(OVectorIterator, ORandomAccessIterator);

template<typename t>
  string _O(VectorIterator)<t>::getFullMethodName(const string& methodName) noexcept
  { return BTI::getVectorIteratorClass<t>()->name+"::"+methodName; }

template<typename t>
  ORet _O(VectorIterator)<t>::packElementValue(t& x,
      const char* methodName, Session* m) const
      requires (!std::is_same_v<t, bool> && !std::is_same_v<t, ORef>) {
    ORef r = BTI::getTypeProxyClass<t>()->createObject(m);
    _RETHROW(getFullMethodName(methodName));
    _PTypeProxy(t, r)->setValue(x);
    _PTypeProxy(t, r)->setElemPtr(&x);
    return OVar(r, USG_DISPOSABLE | (const_ ? USG_CONST : 0));
  }

template<typename t>
  ORet _O(VectorIterator)<t>::packElementValue(const vector<bool>::reference& x,
      const char* methodName, Session* m) const
      requires std::is_same_v<t, bool> {
    ORef r = BTI::getTypeProxyClass<bool>()->createObject(m);
    _RETHROW(getFullMethodName(methodName));
    _PTypeProxy(bool, r)->setValue(x);
    _PTypeProxy(bool, r)->setElemRef(x);
    return OVar(r, USG_DISPOSABLE | (const_ ? USG_CONST : 0));
  }

template<typename t>
  ORet _O(VectorIterator)<t>::getResult(_O(VectorIterator)::base_class&& value,
                                        Session* m) const noexcept {
    ORef r = BTI::getVectorIteratorClass<t>()->createObject(m);
    if (m->exceptionPending())
      return undefinedResult();
    r->castRef<_O(VectorIterator)<t>>() = std::move(value);
    if (const_)
      static_cast<_O(VectorIterator)<t>*>(r)->setConst();
    if (reverse_)
      static_cast<_O(VectorIterator)<t>*>(r)->setReverse(true);
    return r->disposableResult();
  }

//============================================================================//
//:::::::::::::::::::::::::::   OVectorIterator   :::::::::::::::::::::::::::://
//============================================================================//

template<typename t>
  ORet _O(VectorIterator)<t>::ommGetValue(OArg rec, int, OArg*, Session* m) {
    auto& value = reverse_ ? *std::make_reverse_iterator(*this) :
                             this->operator*();
    return packElementValue(value, "value", m);
  }

template<>
  ORet _O(VectorIterator)<bool>::ommGetValue(OArg rec, int, OArg*, Session* m) {
    const auto& value = reverse_ ? *std::make_reverse_iterator(*this) :
                                   this->operator*();
    return packElementValue(value, "value", m);
  }

template<>
  ORet _O(VectorIterator)<ORef>::ommGetValue(OArg rec, int argc, OArg* argv, Session* m) {
    ORef value = reverse_ ? *std::make_reverse_iterator(*this) :
                            this->operator*();
    return OVar(value, USG_REF | (const_ ? USG_CONST : 0));
  }

// it component: arg0; it[arg0];
template<typename t>
  ORet _O(VectorIterator)<t>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
    const difference_t i = g_castValue<difference_t>(argv, m);
    _RETHROW(getFullMethodName(S_component));
    auto it = reverse_ ? *this - (i + 1) : *this + i;
    return packElementValue(*it, S_component, m);
  }

template<>
  ORet _O(VectorIterator)<ORef>::ommComponent(OArg rec, int argc, OArg* argv, Session* m) {
    difference_t i = g_castValue<difference_t>(argv, m);
    _RETHROW(getFullMethodName(S_component));
    auto it = reverse_ ? *this - (i + 1) : *this + i;
    return OVar(*it, USG_REF | (const_ ? USG_CONST : 0));
  }

template<typename t>
  ORet _O(VectorIterator)<t>::inc(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(getFullMethodName(S_inc));
    reverse_ ? this->operator--() : this->operator++();
    return makeResult();
  }

template<typename t>
  ORet _O(VectorIterator)<t>::dec(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(getFullMethodName(S_dec));
    reverse_ ? this->operator++() : this->operator--();
    return makeResult();
  }

template<typename t>
  ORet _O(VectorIterator)<t>::postinc(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_postinc));
    _MUTABLE_REC();
    ORet r = reverse_ ? getResult(this->operator--(0), m) :
                        getResult(this->operator++(0), m);
    _CHECK_EXCEPTION();
    return r;
  }

template<typename t>
  ORet _O(VectorIterator)<t>::postdec(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_postdec));
    _MUTABLE_REC();
    ORet r = reverse_ ? getResult(this->operator++(0), m) :
                        getResult(this->operator--(0), m);
    _CHECK_EXCEPTION();
    return r;
  }

/// it += n;
template<typename t>
  ORet _O(VectorIterator)<t>::incBy(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(getFullMethodName(S_incBy));
    difference_t i = g_castValue<difference_t>(argv, m);
    _RETHROW(getFullMethodName(S_incBy));
    reverse_ ? this->operator-=(i) : this->operator+=(i);
    return makeResult();
  }

/// it -= n;
template<typename t>
  ORet _O(VectorIterator)<t>::decBy(OArg rec, int argc, OArg* argv, Session* m) {
    _MUTABLE_REC_METHOD(getFullMethodName(S_decBy));
    difference_t i = g_castValue<difference_t>(argv, m);
    _RETHROW(getFullMethodName(S_decBy));
    reverse_ ? this->operator+=(i) : this->operator-=(i);
    return makeResult();
  }

/// it1 < it2;
template<typename t>
  ORet _O(VectorIterator)<t>::lt(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_lt));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this < _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 > it2;
template<typename t>
  ORet _O(VectorIterator)<t>::gt(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_gt));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this > _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 <= it2;
template<typename t>
  ORet _O(VectorIterator)<t>::le(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_le));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this <= _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 >= it2;
template<typename t>
  ORet _O(VectorIterator)<t>::ge(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_ge));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this >= _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 == it2;
template<typename t>
  ORet _O(VectorIterator)<t>::eq(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_eq));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this == _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 != it2;
template<typename t>
  ORet _O(VectorIterator)<t>::ne(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_ne));
    _CHECK_ARG_TYPE(0, BTI::getVectorIteratorClass<t>());
    if (_PCVecIter(t, _ARG(0))->reverse_ != reverse_)
      _THROW_RTE("vector iterator mismatch")
    _RETURN_BOOL(*this != _ARG(0)->castConstRef<_O(VectorIterator)>());
  }

/// it1 + n
template<typename t>
  ORet _O(VectorIterator)<t>::add(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_add));
    const difference_t i = g_castValue<difference_t>(argv, m);
    _CHECK_EXCEPTION();
    ORet r = getResult(*this + (reverse_ ? -i : i), m);
    _CHECK_EXCEPTION();
    return r;
  }

/// it1 - n
/// it1 - it2
template<typename t>
  ORet _O(VectorIterator)<t>::sub(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD_NOCHK(getFullMethodName(S_sub));
    if (_ARG_TYPE(0, BTI::getVectorIteratorClass<t>())) {
      auto& it = _ARG(0)->castRef<OVectorIterator>();
      OClass* const type = BTI::getTypeClass<difference_t>();
      _RETURN_TYPE(oxType<difference_t>, type, *this - it);
    }
    const difference_t i = g_castValue<difference_t>(argv, m);
    _CHECK_EXCEPTION();
    ORet r = getResult(*this - (reverse_ ? -i : i), m);
    _CHECK_EXCEPTION();
    return r;
  }

template<typename t>
  ORet _O(VectorIterator)<t>::ommSwap(OArg rec, int argc, OArg* argv, Session* m) {
    _ENTER_METHOD(getFullMethodName("swap:"), 1);
    _MUTABLE_REC();
    _MUTABLE_ARG(0);
    const auto typeId = BTI::getVectorIteratorClassId<t>();
    if (_ARG(0)->getRootClassId() != typeId)
      return m->badArgTypeId(_ox_method_name, 0, typeId, argv);
    std::swap(castRef<OVectorIterator>(), _ARG(0)->castRef<OVectorIterator>());
    return makeResult();
  }

//============================================================================//
//::::::::::::::::::::::::   OVectorIterator_Class   ::::::::::::::::::::::::://
//============================================================================//

template<typename t>
  META(VectorIterator)<t>::META(VectorIterator)(Session* m,
      const std::string& typeName) : OClass(m, typeName) {
    m->importRequirement(this, "RandomAccessIterator");
    m->rethrow("'"+typeName+" class'::'iterator class constructor'");
  }

template<typename t>
  ORef META(VectorIterator)<t>::copyCore(ORef self, const_ORef r,
                                         Session* m) const {
    // error condition handled by OClass::copyObject
    if (r->getRootClass() != getRootClass()) return nullptr;
    *_PVecIter(t, self) = *_PCVecIter(t, r);
    return self;
  }

template<typename t>
  ORef META(VectorIterator)<t>::moveCore(ORef self, ORef r) const {
    // error condition handled by OClass::moveObject
    if (r->getRootClass() != getRootClass()) return nullptr;
    auto* const _this = static_cast<_O(VectorIterator)<t>*>(self);
    *_this = std::move(*static_cast<_O(VectorIterator)<t>*>(r));
    return self;
  }

template<typename t>
  OClass* META(VectorIterator)<t>::createClass(Session* m,
      const string& typeName) const {
    _ENTER_METHOD_NOCHK(META_NAME(VectorIterator) "<"+
        BTI::getTypeClass<t>()->name+">::createClass");
    OClass* cls = new META(VectorIterator)(m, this->name, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

template<>
  OClass* META(VectorIterator)<ORef>::createClass(Session* m,
      const string& typeName) const {
    _ENTER_NOCHK(META_NAME(VectorIterator) "::createClass");
    OClass* cls = new META(VectorIterator)(m, this->name, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

template<typename t>
  OClass* META(VectorIterator)<t>::createMutableClass(Session* m,
      const string& typeName) const {
    _ENTER_METHOD_NOCHK(META_NAME(VectorIterator) "<"+
        BTI::getTypeClass<t>()->name+">::createMutableClass");
    OClass* cls = new META(VectorIterator)(m, this, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

template<>
  OClass* META(VectorIterator)<ORef>::createMutableClass(Session* m,
      const string& typeName) const {
    _ENTER_NOCHK(META_NAME(VectorIterator) "::createMutableClass");
    OClass* cls = new META(VectorIterator)(m, this, typeName);
    _RETHROW_HERE_RETURN(nullptr);
    return cls;
  }

//============================================================================//
//:::::::::::::::::::   OVectorIterator -- Method Table   :::::::::::::::::::://
//============================================================================//

static const char* s_methodNames[] {
  "value",
  S_component,
  S_add,
  S_sub,
  S_inc,
  S_dec,
  S_postinc,
  S_postdec,
  S_incBy,
  S_decBy,
  S_lt,
  S_gt,
  S_le,
  S_ge,
  S_eq,
  S_ne,
  "swap:",
};

#define PUBLIC_(m, i) PUBLIC(m, s_methodNames[i])

#define METHOD_TABLE_FOR_OVectorIterator(t)          \
_BEGIN_TEMPLATE_METHOD_TABLE_FOR(OVectorIterator<t>) \
  PUBLIC_(OVectorIterator<t>::ommGetValue,   0) \
  PUBLIC_(OVectorIterator<t>::ommComponent,  1) \
  PUBLIC_(OVectorIterator<t>::add,           2) \
  PUBLIC_(OVectorIterator<t>::sub,           3) \
  PUBLIC_(OVectorIterator<t>::inc,           4) \
  PUBLIC_(OVectorIterator<t>::dec,           5) \
  PUBLIC_(OVectorIterator<t>::postinc,       6) \
  PUBLIC_(OVectorIterator<t>::postdec,       7) \
  PUBLIC_(OVectorIterator<t>::incBy,         8) \
  PUBLIC_(OVectorIterator<t>::decBy,         9) \
  PUBLIC_(OVectorIterator<t>::lt,           10) \
  PUBLIC_(OVectorIterator<t>::gt,           11) \
  PUBLIC_(OVectorIterator<t>::le,           12) \
  PUBLIC_(OVectorIterator<t>::ge,           13) \
  PUBLIC_(OVectorIterator<t>::eq,           14) \
  PUBLIC_(OVectorIterator<t>::ne,           15) \
  PUBLIC_(OVectorIterator<t>::ommSwap,      16) \
_END_METHOD_TABLE()

METHOD_TABLE_FOR_OVectorIterator(ulong);
METHOD_TABLE_FOR_OVectorIterator(long);
METHOD_TABLE_FOR_OVectorIterator(unsigned);
METHOD_TABLE_FOR_OVectorIterator(int);
METHOD_TABLE_FOR_OVectorIterator(short);
METHOD_TABLE_FOR_OVectorIterator(char);
METHOD_TABLE_FOR_OVectorIterator(double);
METHOD_TABLE_FOR_OVectorIterator(float);
METHOD_TABLE_FOR_OVectorIterator(bool);
METHOD_TABLE_FOR_OVectorIterator(void*);
METHOD_TABLE_FOR_OVectorIterator(ORef);

bool g_installVectorIteratorClasses(Session* m) {
  extern OClass* _OXType_class[];
  OClass* cls;

  cls = new META(VectorIterator)<ulong>(m, "ulong vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_ULONGVECITER);
  _OXType_class[CLSID_ULONGVECITER] = cls;

  cls = new META(VectorIterator)<long>(m, "long vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_LONGVECITER);
  _OXType_class[CLSID_LONGVECITER] = cls;

  cls = new META(VectorIterator)<unsigned>(m, "unsigned vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_UINTVECITER);
  _OXType_class[CLSID_UINTVECITER] = cls;

  cls = new META(VectorIterator)<int>(m, "int vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_INTVECITER);
  _OXType_class[CLSID_INTVECITER] = cls;

  cls = new META(VectorIterator)<short>(m, "short vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_SHORTVECITER);
  _OXType_class[CLSID_SHORTVECITER] = cls;

  cls = new META(VectorIterator)<char>(m, "char vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_CHARVECITER);
  _OXType_class[CLSID_CHARVECITER] = cls;

  cls = new META(VectorIterator)<double>(m, "double vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_DBLVECITER);
  _OXType_class[CLSID_DBLVECITER] = cls;

  cls = new META(VectorIterator)<float>(m, "float vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_FLTVECITER);
  _OXType_class[CLSID_FLTVECITER] = cls;

  cls = new META(VectorIterator)<bool>(m, "bool vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_BOOLVECITER);
  _OXType_class[CLSID_BOOLVECITER] = cls;

  cls = new META(VectorIterator)<void*>(m, "pointer vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_PTRVECITER);
  _OXType_class[CLSID_PTRVECITER] = cls;

  cls = new META(VectorIterator)<ORef>(m, "vector iterator");
  if (!cls) return false;
  cls->finalize(m);
  ASSERT(cls->getClassId() == CLSID_OBJVECITER);
  _OXType_class[CLSID_OBJVECITER] = cls;

  return true;
}
