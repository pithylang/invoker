#include <cstddef>
#include <dlfcn.h>
#include "odefs.h"
#include "opackage.hpp"
#include "obasicTypeInfo.hpp"
#include "opackdev.h"
#include "ortmgr.hpp"
#include "objectAutoCleaner.hpp"
#include "ostring.hpp"
#include "otypes.h"
#include "ovector.hpp"
#include "oset.hpp"
#include <filesystem>
#include <system_error>
#include <vector>
#include "odev.h"

using std::string;
using std::vector;
namespace fs = std::filesystem;

vector<string> split(const string& str, const string& delim);

template<typename t>
std::pair<bool, t> findIn(const vector<std::pair<string, t>>& l,
                          const string& name) {
  const auto it = std::find_if(l.begin(), l.end(),
      [&name](const auto& p) {return p.first == name;});
  if (it == l.end())
    return {false, t{}};
  return {true, it->second};
}

bool isIn(const vector<std::pair<string, OClass*>>& l, OClass* type) {
  // return findIn(l, name).first;
  return std::find_if(l.begin(), l.end(),
      [type](const auto& p) {return p.second == type;}) != l.end();
}

template<typename t>
bool isEmptyOrContains(const t begin, const t end, const string& typeName) {
  return begin == end || std::find(begin, end, typeName) != end;
}

static string s_getFilePath(const string& path,
                            const vector<string>& paths = {"lib"}) {
  if (fs::exists(path))
    return path;
  string filePath = path;
  if (fs::path{filePath}.is_absolute())
    return "";
  for (const auto& subPath : paths) {
    fs::path thisPath = subPath/fs::path{filePath};
    if (fs::exists(thisPath))
      return thisPath;
  }
  return "";
}

//============================================================================//
//:::::::::::::::::::::::::::::::   Package   :::::::::::::::::::::::::::::::://
//============================================================================//

static ORef s_varConstructor(Session*, OClass*, PObjectInitializer);

vector<Package*> Package::s_topLevelPackages_;

//:::::::::::::::::::::::   General Package Methods   :::::::::::::::::::::::://

/* static */
Package* Package::s_findMainPackage(std::function<bool(Package*)> pred) {
  const auto end = s_topLevelPackages_.end();
  const auto it = std::find_if(s_topLevelPackages_.begin(), end, pred);
  return it == end ? nullptr : *it;
}

/* static */
bool Package::s_importsPackageWithId(const Package* importer, const string& id) {
  if (id.empty()) return false;
  Package* const found = importer ?
      importer->findImportee([&id](const Package* p) {return id == p->id;}) :
      s_findMainPackage([&id](const Package* p) {return id == p->id;});
  return found != nullptr;
}

/* static */
Package* Package::s_scanPackagesUntil(std::function<bool(Package*)> cond) {
  for (Package* topLevel : s_topLevelPackages_) {
    Package* result = topLevel->scanImporteesUntil(cond);
    if (result) return result;
  }
  return nullptr;
}

Package* Package::scanImporteesUntil(std::function<bool(Package*)> cond) {
  if (cond(this)) return this;
  for (Package* p : importees_) {
    Package* result = p->scanImporteesUntil(cond);
    if (result) return result;
  }
  return nullptr;
}

Package* Package::findImportee(std::function<bool(const Package*)> pred) const {
  for (const Package* const p : importees_)
    if (pred(p)) return const_cast<Package*>(p);
  return nullptr;
}

Package* Package::findNonameImporterWithPath(const string& path) const {
  for (const Package* p = this; p; p = p->importer_) {
    if (!p->id.empty()) return nullptr;
    Package* importee = p->findImportee([&path](const Package* p) {
      std::error_code ec;
      return p->id.empty() && fs::equivalent(p->fileName, path, ec);
    });
    if (std::error_code ec; importee || fs::equivalent(p->fileName, path, ec))
      return const_cast<Package*>(p);
  }
  return nullptr;
}

void Package::setFilePath(Package* importer, const string& path) {
  const string stdPath = s_getFilePath(path);
  fileName = (!stdPath.empty() || !importer) ? stdPath :
      importer->getPathInSubdir(path);
}

string Package::getPathInSubdir(const string& path) const {
  fs::path relativeToThisPath = fs::canonical(
      fs::path{fileName}.remove_filename()/path);
  if (fs::exists(relativeToThisPath)) return relativeToThisPath;
  return "";
}

bool Package::isCoreLibrary() const {
  constexpr auto EOS = string::npos;
  const auto pos = fileName.rfind('/');
  string fileNameNoPath = pos == EOS ? fileName : fileName.substr(pos + 1);
  auto tokens = std::move(split(fileNameNoPath, "."));
  while (!tokens.empty() && tokens.back().find_first_not_of("0123456789") == EOS)
    tokens.pop_back();
  return tokens.empty() || tokens.size() == 1 ? false : tokens.back() == "so";
}

bool Package::isScriptPackage() const {
  return getScriptPackageType() == PKG_PACKAGE;
}

bool Package::isScriptInclude() const {
  return getScriptPackageType() == PKG_INCLUDE;
}

int Package::getScriptPackageType() const {
  constexpr auto EOS = string::npos;
  const auto pos = fileName.rfind('.');
  const string extension = pos == EOS ? "" : fileName.substr(pos + 1);
  return extension == "is"  ? PKG_INCLUDE  : (
         extension == "isp" ? PKG_PACKAGE : PKG_UNKNOWN);
}

OClass* Package::getImportType(const string& typeName) const {
  for (const auto& [name, type] : typeImports_)
    if (name == typeName) return type;
  return nullptr;
}

void Package::importPackage(Session* m) {
  OM_ENTER_NOCHK("Package::importPackage");
  if (fileName.empty())
    OM_THROW_RTE_NORET("missing package file specification")
  // determine package type
  setPackageType();
  if (type == PKG_UNKNOWN)
    OM_THROW_RTE_NORET("unknown package type")
  // register package
  if (!registerPackage(m)) // the package is already in use
    return;
  // load package file
  load(m);
  OM_RETHROW_HERE_NORET();
  // import requested items from package
  if (type == PKG_CORE) {
    importFromCorePackage(m);
    requestedImports_.clear();
  }
  else if (type == PKG_PACKAGE) {
    if (id.empty() && !m->importing())
      OM_THROW_RTE_NORET("unnamed packages can only be "
                         "imported from the main program")
    importFromScriptPackage(m);
    requestedImports_.clear();
    OM_ASSERT(m->importing(), m);
  }
  else if (type == PKG_INCLUDE) {
    if (!requestedImports_.empty())
      OM_THROW_RTE_NORET("script "+fileName+": you cannot specify imports "
          "when importing plain scripts");
    importScript(m);
  }
  m->endPackage();
  OM_RETHROW_HERE_NORET();
}

void Package::setPackageType() {
  if (isCoreLibrary())
    type = PKG_CORE;
  else if (isScriptPackage())
    type = PKG_PACKAGE;
  else if (isScriptInclude())
    type = PKG_INCLUDE;
  else
    type = PKG_UNKNOWN;
}

bool Package::registerPackage(Session* m) {
  if (m->importing()) {
    Package* const importer = m->getPackageInfo().package;
    if (importer->findNonameImporterWithPath(getFilePath()) != nullptr)
      return false;
    importer_ = importer;
    importer_->importees_.push_back(this);
  }
  else {
    s_topLevelPackages_.push_back(this);
  }
  return true;
}

void Package::load(Session* m) {
  OM_ENTER_NOCHK("Package::load");
  OM_ASSERT(type != PKG_UNKNOWN, m);
  if (type == PKG_CORE) {
#ifdef __DEBUG__
    const int options = RTLD_NOW;
#else // !__DEBUG__
    const int options = RTLD_LAZY;
#endif // !__DEBUG__
    string msg;
    hlib = dlopen(fileName.c_str(), options);
    if (!hlib) {
      msg = dlerror();
      fs::path filePath{ fileName };
      if (fs::exists(filePath) || filePath.is_absolute())
        OM_THROW_RTE_NORET(msg);
      if (!(hlib = dlopen(string{"lib"/filePath}.c_str(), options))) {
        if (char* error = dlerror(); error)
          msg = string{ error }+" ("+msg+")";
        OM_THROW_RTE_NORET(msg);
      }
    }
    m->beginPackage(this, string{ id.empty() ? "" : id+"." });
  }
  else if (type == PKG_PACKAGE || type == PKG_INCLUDE) {
    OM_ASSERT(!getFilePath().empty(), m); // TODO apostol
    loadScriptPackage(m, getFilePath());
    OM_RETHROW_HERE_NORET();
  }
  else
    OM_THROW_RTE_NORET("unknown package type")
}

void Package::extend(Session* m) {
  ORef const o = static_cast<_O(Package)*>(this);
  m->extendObject(o, varImports_, USG_DISPOSABLE);
  OM_RETHROW_NORET("Package::extend");
}

//::::::::::::::::::::::::::::   Core Packages   ::::::::::::::::::::::::::::://

bool Package::getImportingFunction() {
  if (importPackageInfo) return true;
  dlerror(); // clear possible error code
  void* const pfunc = dlsym(hlib, "getPackageExportInfo");
  if (!pfunc || dlerror()) return false;
  importPackageInfo = reinterpret_cast<PackageInfoImporter>(pfunc);
  return true;
}

void Package::importFromCorePackage(Session* m) {
  OM_ENTER_NOCHK("Package::importFromCorePackage");
  if (requestedImports_.size() == 1 && requestedImports_.front() == "*")
    requestedImports_.pop_back();
  // get importing function
  if (!getImportingFunction()) {
    m->throwRTE(_ox_method_name, "no importing function (possibly "
      "not a core package)");
    return;
  }
  // apply it
  const auto result = importCorePackage(m);
  // handle errors
  switch (result.rc) {
    case PKG_ERROR_NONE: break;
    case PKG_ERROR_IMPORT_NOT_FOUND:
      m->throwRTE(_ox_method_name, "no export '"+result.itemNotFound+"' "
          "in package '"+fileName+"'"+(id.empty() ? string{} : " ("+id+')'));
      return;
    case PKG_ERROR_CVT_CONST_NONCONST:
      m->throwRTE(_ox_method_name, "request to import constant variable '"+
          result.itemNotFound+"' as non-constant from "
          "package '"+fileName+"'"+(id.empty() ? string{} : " ("+id+')'));
      return;
    case PKG_ERROR_NO_EXPORTER:
      m->throwRTE(_ox_method_name, "no importing function (possibly "
        "not a core package)");
      return;
    case PKG_ERROR_INITIALIZATION:
      m->throwRTE(_ox_method_name, "package initialization failed");
      return;
    case PKG_ERROR_CTOR_FAILED:
    case PKG_ERROR_UNEXPECTED:
      m->rethrow(_ox_method_name, __FUNCTION__, __LINE__);
      return;
    case PKG_ERROR_INIT_TERM_IMPORT:
      m->throwRTE(_ox_method_name, "package "+fileName+": "
          "the item '"+result.itemNotFound+"' cannot be imported");
      return;
  }
  // the classes are already registered, we treat the variables
  if (!static_cast<_O(Package)*>(this)->getClass()->isMutable()) {
    size_t n = 0;
    for (const auto& i : varImports_)
      if (i.first == "+" || i.first == "-") ++n;
    if (n != varImports_.size()) {
      const string varName = id.empty() ? string{"varName"} : id;
      m->throwRTE(_ox_method_name, "to have variables imported declare the "
          "package with the syntax '"+varName+" = \""+fileName+"\"P'");
    }
    return;
  }
  extend(m);
  OM_RETHROW_HERE_NORET();
}

PackageImportResult Package::importCorePackage(Session* m) {
  OM_ENTER_NOCHK("Package::importCorePackage");
  OM_ASSERT(importPackageInfo, m);
  // import package info
  PackageExportInfo packageExportInfo;
  importPackageInfo(&packageExportInfo);
  const auto& typeExports = *packageExportInfo.pTypeExports;
  const auto& varExports = *packageExportInfo.pVarExports;
  name = std::move(packageExportInfo.packageName);
  version = std::move(packageExportInfo.packageVersion);
  const bool all = requestedImports_.empty();
  // check imports
  const auto veEnd = varExports.end();
  for (const string typeOrVarId : requestedImports_) {
    if (typeExports.find(typeOrVarId) == typeExports.end() &&
        varExports.find(typeOrVarId) == veEnd) {
      const bool constImport = typeOrVarId[0] == '!';
      if (!constImport || varExports.find(typeOrVarId.substr(1)) == veEnd) {
        if (!constImport && varExports.find('!'+typeOrVarId) != veEnd)
          return {PKG_ERROR_CVT_CONST_NONCONST, typeOrVarId};
        else
          return {PKG_ERROR_IMPORT_NOT_FOUND, typeOrVarId};
      }
    }
  }
  // initialize package
  auto initIter = varExports.find("+");
  if (initIter != varExports.end()) {
    initIter->second.initializer(m, reinterpret_cast<ORef>(&requestedImports_));
    if (m->exceptionPending())
      return {PKG_ERROR_INITIALIZATION};
  }
  // import package
  // const string prefix{ id.empty() ? "" : id+"." };
  const string prefix = m->getTypePrefix();
  // first import types (classes)
  const auto begin = requestedImports_.begin();
  const auto end = requestedImports_.end();
  for (const auto& [typeName, ctor] : typeExports) {
    if (!isEmptyOrContains(begin, end, typeName)) // !all and not a type export
      continue;
    // proceed with the registration
    OClass* type = ctor(m, prefix + typeName);
    if (m->exceptionPending()) {
      m->rethrow(_ox_method_name);
      return {PKG_ERROR_CTOR_FAILED};
    }
    typeImports_.emplace_back(prefix + typeName, type);
  }
  // then import variables
  for (const auto& [varName, exportItem] : varExports) {
    if (varName == "+" || varName == "-") continue;
    if (!isEmptyOrContains(begin, end, varName)) {
      const bool constExport = varName[0] == '!';
      if (constExport || std::find(begin, end, '!'+varName) == end) {
        if (constExport && std::find(begin, end, varName.substr(1)) != end)
          return {PKG_ERROR_CVT_CONST_NONCONST, varName.substr(1)};
        continue;
      }
      // if we are here it's OK, it's a request to use a non-const as a const
    }
    importCorePackageVar(m, varName, exportItem);
    if (m->exceptionPending())
      return {PKG_ERROR_UNEXPECTED};
  }
  return {PKG_ERROR_NONE};
}

void Package::importCorePackageVar(Session* m,
                                   const string& varName,
                                   const PVarConstructInfo& exportItem) {
  varImports_.emplace_back(varName,
      exportItem.objectInstance ?: s_varConstructor(m,
          exportItem.type ? exportItem.type : exportItem.typeGet(m),
          exportItem.initializer));
}

ORef s_varConstructor(Session* m, OClass* type, PObjectInitializer initialize) {
  ORef o = type->createObject(m);
  if (m->exceptionPending()) return nullptr;
  initialize(m, o);
  return o;
}

//:::::::::::::::::::::::::::   Script Packages   :::::::::::::::::::::::::::://

void Package::loadScriptPackage(Session* m, const string& filePath) {
  int g_loadPackage(Session*, const string&);
  const string typePrefix{id.empty() ? id : id + '.'};
  m->beginPackage(this, typePrefix);
  packageBlock = g_loadPackage(m, filePath);
  if (packageBlock == -1) {
    m->endPackage();
    m->rethrow("Package::loadScriptPackage");
  }
}

void Package::importFromScriptPackage(Session* m) {
  OM_ENTER_NOCHK("Package::importFromScriptPackage");
  OM_ASSERT(packageBlock > 0, m);
  if (requestedImports_.size() == 1 && requestedImports_.front() == "*")
    requestedImports_.clear();
  importScriptPackage(m);
  OM_RETHROW_HERE_NORET();
  // the classes are already registered, we treat the variables
  if (!static_cast<_O(Package)*>(this)->getClass()->isMutable()) {
    if (varImports_.empty()) return;
    const string varName = id.empty() ? "varName" : id;
    OM_THROW_RTE_NORET("to have variables imported declare the package "
        "with the syntax '"+varName+" = \""+fileName+"\"P'");
  }
  extend(m);
  OM_RETHROW_HERE_NORET();
}

void Package::importScriptPackage(Session* m) {
  OM_ENTER_NOCHK("Package::importScriptPackage");
  OM_ASSERT(requestedImports_.size()  != 1 ||
            requestedImports_.front() != "*", m);
  // make block into a method and set arguments
  vector<string> argNames{"packageId"};
  m->setMethodArgNames(packageBlock, argNames);
  OM_RETHROW_HERE_NORET();
  // invoke it to import the package
  ORef const opack = static_cast<_O(Package)*>(this);
  OVar rec{opack};
  ScriptMethodAddress a{packageBlock};
  const string sel = "importFromScriptPackage:";
  // - make list of arguments
  oxString objId{BTI::getTypeClass<string>()};
  objId = id;
  OVar idVar{&objId};
  vector<OArg> argv{&idVar};
  // - run the script
  ORet result = m->execScriptMethod(a, sel, 0, &rec, argv.size(), argv.data());
  OM_RETHROW_HERE_NORET();
  if (REF(result)->getClassId() != CLSID_RTMGR) {
    const string msg = "'"+fileName+"' is not a package file or contains "
        "a misplaced exports statement";
    OM_THROW_RTE_NORET(msg);
  }
}

void Package::exportRequestedItems(Session* m) {
  std::pair<int, string> g_exportVars(Session*, int, string_object_assoc&);
  OM_ENTER_NOCHK("Package::exportRequestedItems");
  // get all exportable items
  getExportableItems(m);
  // classify requestedImports_ as types or variables
  classifyImports(m);
  OM_RETHROW_HERE_NORET();
  // export specified items
  // - make inaccessible the type exports that were not imported
  for (const auto& [typeName, type] : typeExports_)
    if (!isIn(typeImports_, type))
      type->hide();
  // - export variables
  const auto [rc, symbolName] = g_exportVars(m, packageBlock, varImports_);
  // - - handle errors
  switch (rc) {
    case EXPORT_PACKAGE_OK:
      break;
    case EXPORT_PACKAGE_UNINIT_STATIC:
    case EXPORT_PACKAGE_UNINIT_LOCAL:
    case EXPORT_PACKAGE_REF: {
      const string affix = rc == EXPORT_PACKAGE_UNINIT_STATIC ? "static " : "";
      const string msg = rc == EXPORT_PACKAGE_REF ? "cannot export reference '"
          : "cannot export uninitialized "+affix+"variable '";
      OM_THROW_RTE_NORET("package "+fileName+": "+msg+symbolName+"'");
    }
    case EXPORT_PACKAGE_VAR_NOT_FOUND: {
      const string msg = ": cannot export unknown variable '"+symbolName+"'";
      OM_THROW_RTE_NORET("package "+fileName+msg);
    }
  }
}

void Package::getExportableItems(Session* m) {
  std::pair<int, string> g_exportVars(Session*, int, string_object_assoc&);
  OM_ENTER_NOCHK("Package::getExportableItems");
  OM_ASSERT(typeExports_.empty(), m);
  OM_ASSERT(varExports_.empty(), m);
  const auto typePrefix = m->getTypePrefix();
  const auto sizePrefix = typePrefix.size();
  // build list of exporable types
  /* - reserve space for type exports */ {
    size_t sizeOfTypeExports = m->getPackageInfo().registeredTypes.size();
    for (const Package* const importee : importees_)
      sizeOfTypeExports += importee->typeImports_.size();
    typeExports_.reserve(sizeOfTypeExports);
  }
  // - add types defined in this package
  for (const auto& [typeName, type] : m->getPackageInfo().registeredTypes) {
    OM_ASSERT(typeName.size() > sizePrefix, m);
    OM_ASSERT(!sizePrefix || typeName.substr(0, sizePrefix) == typePrefix, m);
    string rawTypeName = typeName.substr(sizePrefix);
    typeExports_.emplace_back(std::move(rawTypeName), type);
  }
  // - add imported types
  for (const Package* const importee : importees_) {
    const auto& imports = importee->typeImports_;
    const string prefix = importee->id.empty() ? "" : importee->id + '.';
    string_pclass_assoc exports;
    exports.reserve(imports.size());
    for (const auto& item : imports)
      exports.emplace_back(prefix + item.first, item.second);
    typeExports_.insert(typeExports_.end(), exports.begin(), exports.end());
  }
  // build list of exportable vars
  /* - reserve space for var exports */ {
    size_t sizeOfVarExports = 0;
    for (const Package* const importee : importees_)
      sizeOfVarExports += importee->varImports_.size();
    varExports_.reserve(sizeOfVarExports);
  }
  // - add vars defined in this package
  OM_ASSERT(varExports_.empty(), m);
  g_exportVars(m, packageBlock, varExports_);
  // - add imported vars
  for (const Package* const importee : importees_) {
    const auto& imports = importee->varImports_;
    varExports_.insert(varExports_.end(), imports.begin(), imports.end());
  }
}

void Package::classifyImports(Session* m) {
  OM_ASSERT(typeImports_.empty(), m);
  OM_ASSERT(varImports_.empty(), m);
  if (requestedImports_.empty()) { // all exports are imports
    typeImports_ = typeExports_;
    varImports_ = varExports_;
    return;
  }
  // classify requested types and variables
  for (const auto& reqImport : requestedImports_) {
    const auto [found, type] = findIn(typeExports_, reqImport);
    if (!found) {
      const auto [found, o] = findIn(varExports_, reqImport);
      if (!found) {
        m->throwRTE("Package::classifyImports",
          "package "+fileName+": the import '"+reqImport+
          "' does not exist or is not an exported item");
        return;
      }
      varImports_.emplace_back(reqImport, o);
    }
    else
      typeImports_.emplace_back(reqImport, type);
  }
}

void Package::exportNothing(Session* m) {
  for (const auto& [_, type] : m->getPackageInfo().registeredTypes)
    type->hide();
}

//:::::::::::::::::::::::::::   Script Includes   :::::::::::::::::::::::::::://

void Package::importScript(Session* m) {
  OM_ENTER_NOCHK("Package::importScript");
  OM_ASSERT(requestedImports_.empty(), m);
  // make block into a method and set arguments
  vector<string> argNames{"packageId"};
  m->setMethodArgNames(packageBlock, argNames);
  OM_RETHROW_HERE_NORET();
  // invoke it to import the package
  //ORef const rec = static_cast<_O(Package)*>(this);
  OVar rec{static_cast<_O(Package)*>(this)};
  ScriptMethodAddress a{packageBlock};
  const string sel = "importScript:";
  // - make list of arguments
  oxString objId{BTI::getTypeClass<string>()};
  objId = id;
  OVar idVar{&objId};
  vector<OArg> argv{&idVar};
  // - run the script
  ORet result = m->execScriptMethod(a, sel, 0, &rec, argv.size(), argv.data());
  // clean result if disposable and check exception
  ObjectAutoCleaner resultCleaner{m, result};
  OM_RETHROW_HERE_NORET();
}

//============================================================================//
//::::::::::::::::::::::::::::   Package Object   :::::::::::::::::::::::::::://
//============================================================================//

enum : int {
  IMPEXP_OK = 0,
  IMPEXP_BAD_ARG,
  IMPEXP_EMPTY_SPEC,
  IMPEXP_NO_STRING_ARG,
  IMPEXP_UNACCEPTABLE_ITEM,
  IMPEXP_UNNAMED_VARIABLE,
};

std::pair<int, string> _O(Package)::s_getUserSpec(int argc, OArg* argv,
    vector<string>& userSpec, std::function<bool(const string&)> badItem) {
  // get vector of args
  vector<ORef> args;
  if (argc == 1) {
    ORef const arg = OM_ARG(0);
    if (arg->getClass() == BTI::getTypeClass<string>())
      args.push_back(arg);
    else if (arg->getClass() == BTI::getObjectVectorClass())
      args.assign(_PObjVector(arg)->begin(), _PObjVector(arg)->end());
    else if (arg->getClass() == BTI::getObjectSetClass())
      args.assign(_PObjSet(arg)->begin(), _PObjSet(arg)->end());
    else if (arg->getClass() == BTI::getSetClass<string>())
      userSpec.assign(_PStrSet(arg)->begin(), _PStrSet(arg)->end());
    else if (arg->getClass()->isMutable())
      return {IMPEXP_BAD_ARG, "object"};
    else
      return {IMPEXP_BAD_ARG, arg->getClassName()};
  }
  else {
    args.reserve(argc);
    for (size_t i = 0; i < static_cast<size_t>(argc); ++i)
      args.push_back(OM_ARG(i));
  }
  if (args.empty() && userSpec.empty())
    return {IMPEXP_EMPTY_SPEC, ""};
  // extract strings
  for (auto& arg : args) {
    if (arg->getRootClass() != BTI::getTypeClass<string>())
      return {IMPEXP_NO_STRING_ARG, arg->getClassName()};
    string& importExportItem = *static_cast<string*>(_PString(arg));
    if (badItem(importExportItem))
      return {IMPEXP_UNACCEPTABLE_ITEM, importExportItem};
    else if (importExportItem.empty())
      return {IMPEXP_UNNAMED_VARIABLE, ""};
    userSpec.push_back(importExportItem);
  }
  return {IMPEXP_OK, ""};
}

OM_BEGIN_METHOD_TABLE(_O(Package), OInstance)
  PUBLIC(_O(Package)::ommAs, "as:")
  PUBLIC(_O(Package)::ommAsImport, "as:import:")
  PUBLIC(_O(Package)::ommImport, "import:")
  PUBLIC(_O(Package)::ommImportAll, "import")
  PUBLIC(_O(Package)::ommImportAll, "importAll")
  PUBLIC(_O(Package)::ommSetValue, "value:")
  PUBLIC(_O(Package)::ommSetValue, "file:")
  PUBLIC(_O(Package)::ommExport, "exports:")
  PUBLIC(_O(Package)::ommExportAll, "exportAll")
  PUBLIC(_O(Package)::ommExportObjects, "objectExports:")
  PUBLIC(_O(Package)::ommExportNothing, "noexports")
OM_END_METHOD_TABLE()

ORet _O(Package)::ommSetValue(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER("package::value:", 1);
  OM_CHECK_ARG_TYPEID(0, BTI::getTypeClassId<string>());
  OM_UNPACK_VAR(String, path, 0);
  if (type != PKG_UNKNOWN)
    OM_THROW_RTE("the file name must be set before importing the package");
  if (fs::path{path}.extension().empty())
    path += ".is";
  auto* const importer = m->importing() ? m->getPackageInfo().package : nullptr;
  setFilePath(importer, path);
  if (fileName.empty())
    OM_THROW_RTE("the file '"+path+"' does not exist")
  return makeResult();
}

ORet _O(Package)::ommImport(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("package::import:");
  OM_ASSERT(requestedImports_.empty(), m);
  const auto* importer = m->importing() ? m->getPackageInfo().package : nullptr;
  // check if package id is already in use
  if (s_importsPackageWithId(importer, id))
    OM_THROW_RTE("the package identifier '"+id+"' is already in use")
  // get user spec
  auto f = [](const string& import)
  { return import == "+" || import == "!+" || import == "-" || import == "!-"; };
  auto [rc, errorItem] = s_getUserSpec(argc, argv, requestedImports_, f);
  switch (rc) {
    case IMPEXP_OK:
      break;
    case IMPEXP_BAD_ARG:
      return m->throwBadArg(_ox_method_name, 0,
          "string, set or vector", errorItem);
    case IMPEXP_EMPTY_SPEC:
      OM_THROW_RTE("empty set of imports");
    case IMPEXP_NO_STRING_ARG: {
      m->throwBadArg(_ox_method_name,
           "unexpected import type: expected string, found '"+errorItem+"'");
      return OVariable::undefinedResult();
    }
    case IMPEXP_UNACCEPTABLE_ITEM:
      OM_THROW_RTE("the variable '"+errorItem+"' cannot be imported")
    case IMPEXP_UNNAMED_VARIABLE:
      OM_THROW_RTE("request to import an unnamed variable")
  }
  // import the package
  importPackage(m);
  OM_RETHROW_HERE();
  return makeResult();
}

ORet _O(Package)::ommAs(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER("package::as:", 1);
  OM_CHECK_ARG_TYPEID(0, BTI::getTypeClassId<string>());
  OM_UNPACK_CONST_REF(String, name, 0);
  if (type != PKG_UNKNOWN)
    OM_THROW_RTE("the method '"+string{_ox_method_name}+"' must be used "
                 "before importing the package")
  id = name;
  return makeResult();
}

ORet _O(Package)::ommAsImport(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("package::as:import:");
  OM_CHECK_ARG_TYPEID(0, BTI::getTypeClassId<string>());
  OM_UNPACK_CONST_REF(String, name, 0);
  id = name;
  ORet result = ommImport(rec, argc - 1, argv + 1, m);
  OM_RETHROW_HERE();
  return result;
}

ORet _O(Package)::ommImportAll(OArg rec, int, OArg*, Session* m) {
  OM_ENTER_NOCHK("package::importAll");
  OM_ASSERT(requestedImports_.empty(), m);
  // check if package id is already in use
  const auto* importer = m->importing() ? m->getPackageInfo().package : nullptr;
  if (s_importsPackageWithId(importer, id))
    OM_THROW_RTE("the package identifier '"+id+"' is already in use")
  // import the package
  importPackage(m);
  OM_RETHROW_HERE();
  return makeResult();
}

ORet _O(Package)::ommExport(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("package::exports:");
  // check if we are importing a script package
  if (!m->importing() || type != PKG_PACKAGE ||
      m->getPackageInfo().package != static_cast<Package*>(this))
    OM_THROW_RTE("exporting declaratives can only be used from package files")
  // get user spec
  auto f = [](const string&) {return false;};
  auto [rc, errorItem] = s_getUserSpec(argc, argv, specifiedExports_, f);
  switch (rc) {
    case IMPEXP_OK: break;
    case IMPEXP_BAD_ARG:
      return m->throwBadArg(_ox_method_name, 0,
          "string, set or vector", errorItem);
    case IMPEXP_EMPTY_SPEC:
      OM_THROW_RTE("missing exports declarative or empty set of exports");
    case IMPEXP_NO_STRING_ARG: {
      m->throwBadArg(_ox_method_name,
          "unexpected export type: expected string, found '"+errorItem+"'");
      return OVariable::undefinedResult();
    }
    case IMPEXP_UNACCEPTABLE_ITEM: break;
    case IMPEXP_UNNAMED_VARIABLE:
      OM_THROW_RTE("request to export an unnamed variable");
  }
  // check for duplicates
  vector<string> exportNames{specifiedExports_};
  const auto E = exportNames.end();
  std::sort(exportNames.begin(), E);
  if (const auto it = std::adjacent_find(exportNames.begin(), E); it != E)
    OM_THROW_RTE("package "+fileName+": duplicate variable '"+*it+"'")
  // export specified items
  exportRequestedItems(m);
  OM_CHECK_EXCEPTION();
  return static_cast<ORTMgr*>(m)->makeResult();
}

ORet _O(Package)::ommExportAll(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("package::exportAll");
  // check if we are importing a script package
  if (!m->importing() || type == PKG_INCLUDE ||
      m->getPackageInfo().package != static_cast<Package*>(this))
    OM_THROW_RTE("exporting declaratives can only be used from package files")
  // export all items
  exportRequestedItems(m);
  OM_CHECK_EXCEPTION();
  return static_cast<ORTMgr*>(m)->makeResult();
}

ORet _O(Package)::ommExportObjects(OArg rec, int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("package::objectExports:");
  // check if we are importing a script package
  if (!m->importing() || type == PKG_INCLUDE ||
      m->getPackageInfo().package != static_cast<Package*>(this))
    OM_THROW_RTE("exporting declaratives can only be used from package files")
  // export specified objects
  const auto B = requestedImports_.begin();
  const auto E = requestedImports_.end();
  for (size_t i = 0; i < static_cast<size_t>(argc); i += 2) {
    const string& key = *_PString(OM_ARG(i));
    if (i + 1 == argc) {
      m->throwBadArgc(__PRETTY_FUNCTION__, _ox_method_name, argc + 1);
      return undefinedResult();
    }
    if (B == E || std::find(B, E, key) != E) {
      varImports_.emplace_back(key, nullptr);
      std::swap(varImports_.back().second, OM_ARG(i + 1));
    }
  }
  // check for extraneous requests
  if (B != E && requestedImports_.size() != varImports_.size()) {
    using t = decltype(varImports_[0]);
    const auto E = varImports_.end();
    for (const auto& reqImport : requestedImports_) {
      auto f = [&reqImport](t& p) {return p.first == reqImport;};
      if (std::find_if(varImports_.begin(), E, f) == E)
        OM_THROW_RTE("package "+fileName+": the import '"+reqImport+"' "
                     "does not exist or is not an exported item")
    }
  }
  return makeResult();
}

ORet _O(Package)::ommExportNothing(OArg, int, OArg*, Session* m) {
  OM_ENTER_NOCHK("package::noexports");
  // check if we are importing a script package
  if (!m->importing() || type == PKG_INCLUDE ||
      m->getPackageInfo().package != static_cast<Package*>(this))
    OM_THROW_RTE("exporting declaratives can only be used from package files")
  exportNothing(m);
  return static_cast<ORTMgr*>(m)->makeResult();
}

//::::::::::::::::::::::::::::   Package Class   ::::::::::::::::::::::::::::://

#if 0
OM_DEFINE_MCLASS(Package);
OM_IMPLEMENT_CLASSID_INSTALLER(Package, CLSID_PACKAGE)
#endif // 0
OM_DEFINE_META_TYPE(Package);
OM_IMPLEMENT_TYPEID_INSTALLER(Package, "package", CLSID_PACKAGE)
