#ifndef __TESTEXTCOREPACK_HPP__
#define __TESTEXTCOREPACK_HPP__

#include <string>
#include <functional>
#include "testCorePack.hpp"

struct PluginExtension : TestPlugin {
  ulong salary;
};

OM_BEGIN_CLASS_DECLARATION(PluginExtension);
  OM_METHOD(ommGetSalary);
  OM_METHOD(ommSetSalary);
  OM_METHOD(ommPrint);
OM_END_CLASS_DECLARATION()

OM_DECLARE_MCLASS(PluginExtension, "TestExt")

#endif // __TESTEXTCOREPACK_HPP__
