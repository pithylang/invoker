# Modify paths as needed

c++ -std=c++17 -fPIC -fconcepts -O0 -g -c -D__DEBUG__ -DPKG_EXPORTING \
    -I../../pithy/include -I. testCorePack.cpp
if [ "$?" == "0" ]; then
  c++ -shared -fPIC -Wl,-soname,libtestCorePack.so \
      -Wl,-rpath,/media/apostol/Gigabyte/dev/c++/i1/lib -L../lib \
      -o libtestCorePack.so testCorePack.o -lisom -lc
  if [ "$?" == "0" ]; then
    mv -v libtestCorePack.so ../lib
  fi
fi

