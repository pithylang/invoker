#ifndef __TESTCOREPACK_HPP__
#define __TESTCOREPACK_HPP__

#include <string>
#include <functional>
#include "object.hpp"
#include "odev.h"

struct TestPlugin {
  std::string name;
  unsigned age;
};

OM_BEGIN_CLASS_DECLARATION(TestPlugin);
public:
  OM_METHOD(ommGetName);
  OM_METHOD(ommSetName);
  OM_METHOD(ommGetAge);
  OM_METHOD(ommSetAge);
  OM_METHOD(ommPrint);
protected:
OM_END_CLASS_DECLARATION()

OM_DECLARE_MCLASS(TestPlugin, "Test")

#endif // __TESTCOREPACK_HPP__
