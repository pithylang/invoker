# Modify paths as needed

c++ -std=c++17 -fPIC -fconcepts -O0 -g -c -D__DEBUG__ -DPKG_EXPORTING \
    -I../../pithy/include -I. testExtCorePack.cpp
if [ "$?" == "0" ]; then
  c++ -shared -fPIC -Wl,-soname,libtestExtCorePack.so \
      -Wl,-rpath,/media/apostol/Gigabyte/dev/c++/i1/lib -L../lib \
      -o libtestExtCorePack.so testExtCorePack.o -ltestCorePack -lisom -lc
  if [ "$?" == "0" ]; then
    mv -v libtestExtCorePack.so ../lib
  fi
fi

