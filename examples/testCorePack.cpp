#include "testCorePack.hpp"
#include "ortmgr.hpp"
#include "obasicTypeInfo.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "ovector.hpp"
#include "opackdev.h"

/**
 *  How to compile:
 *  c++ -std=c++14 -fPIC -fconcepts -O0 -g -c -DPKG_EXPORTING \
 *    -I../../pithy/include -I. testCorePack.cpp
 *  c++ -shared -fPIC -Wl,-soname,libtestCorePack.so \
 *    -Wl,-rpath,/media/apostol/Gigabyte/dev/c++/i1/lib -L../lib \
 *    -o libtestCorePack.so testCorePack.o -lisom -lc
 *  Or use
 *  . make_testCorePack.sh
 */

using std::string;
using std::vector;

static vector<int> testVector{11, 12, 13};
static string testString{"testing core packages (plugins)"};

//:::::::::::::::::::::::::::::::   Exports   :::::::::::::::::::::::::::::::://

OM_BEGIN_PACKAGE_EXPORT("Test Package", "1.0.0")
  OM_EXPORT_TYPE("Test", oxTestPlugin_Class)
OM_END_PACKAGE_EXPORT()

OM_BEGIN_VAR_EXPORT()
  OM_EXPORT_VAR_TYPE("vec",
      BTI::getVectorClass<int>(),
      [](Session*, ORef o) {
        *static_cast<vector<int>*>(_PVector(int, o)) = testVector;})
  OM_EXPORT_VAR_TYPE("info",
      BTI::getTypeClass<string>(),
      [](Session*, ORef o) {*_PString(o) = testString;})
  OM_EXPORT_VAR_GET_TYPE("whatIs",
      [](Session* m) {return m->classMgr->getClass("string");},
      [](Session*, ORef o) {*_PString(o) = testString;})
OM_END_VAR_EXPORT()

OM_DEFINE_PACKAGE_EXPORTER()

//:::::::::::::::::::::::::::::   oxTestPlugin   ::::::::::::::::::::::::::::://

OM_BEGIN_METHOD_TABLE(oxTestPlugin, OInstance)
  PUBLIC(oxTestPlugin::ommGetName, "name")
  PUBLIC(oxTestPlugin::ommSetName, "name:")
  PUBLIC(oxTestPlugin::ommGetAge, "age")
  PUBLIC(oxTestPlugin::ommSetAge, "age:")
  PUBLIC(oxTestPlugin::ommPrint, "print")
OM_END_METHOD_TABLE()

ORet oxTestPlugin::ommGetName(int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("Test::name");
  ORef o = BTI::getTypeClass<string>()->createObject(m);
  OM_RETHROW_HERE();
  *_PString(o) = name;
  return o->disposableResult();
}

ORet oxTestPlugin::ommGetAge(int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("Test::age");
  ORef o = BTI::getTypeClass<unsigned>()->createObject(m);
  OM_RETHROW_HERE();
  _P(unsigned, o)->setValue(age);
  return o->disposableResult();
}

ORet oxTestPlugin::ommSetName(int argc, OArg* argv, Session* m) {
  OM_ENTER("Test::name:", 1);
  OM_CHECK_ARG_TYPEID(0, BTI::getTypeClassId<string>());
  OM_UNPACK_CONST_REF(String, s, 0);
  if (s.size() > 50)
    OM_THROW_RTE("name too long");
  name = s;
  return makeResult();
}

ORet oxTestPlugin::ommSetAge(int argc, OArg* argv, Session* m) {
  OM_ENTER("Test::age:", 1);
  unsigned x = g_castValue<unsigned>(argv, m);
  OM_RETHROW_HERE();
  if (x > 120)
    OM_THROW_RTE("incorrect age");
  age = x;
  return makeResult();
}

ORet oxTestPlugin::ommPrint(int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("Test::print");
  string msg{name + " - " + std::to_string(age)};
  m->displayMessage("Test", msg);
  return makeResult();
}

//::::::::::::::::::::::::::   oxTestPlugin_Class   :::::::::::::::::::::::::://

OM_DEFINE_MCLASS(TestPlugin)






















//
