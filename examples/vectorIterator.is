/**
 *  @brief Random access iterator for vectors
 *  @details This class is an example of a random access
 *  iterator for vectors completely implemented in script.
 */
$class: vectorIterator extends: "random access iterator" def: {
  .protected: "count_" type: "long";
  .ref: "vector";
  .method: "init: v" def: {
    vector := v;
    $returnRef: this;
  };
  .method: "value" def: {
    $returnRef: vector[count_];
  };
  .method: "incBy: r" def: {
    count_ += r;
    $returnRef: this;
  };
  .method: "decBy: r" def: {
    count_ -= r;
    $returnRef: this;
  };
  .method: "inc" def: {
    ++count_;
    $returnRef: this;
  };
  .method: "dec" def: {
    --count_;
    $returnRef: this;
  };
  .method: "sub: rhs" def: {
    $if: (rhs type: "random access iterator") then: {
      $return: count_ - rhs.count_;
    };
    count_ -= rhs;
    $returnRef: this;
  };
};

v = [1, "a string", 2.3];

// The range iteration is more efficient.
/*$auto: end type: "vectorIterator" => init: v => incBy: 3;
$for: {$auto: it type: "vectorIterator"; it init: v;} while: {it != end;}
                                                      step: {++it;} do: {
  (it value) println;
};*/

begin = "vectorIterator"a{0, v};
end = "vectorIterator"a{v size, v};
"range"a => from: begin to: end
         => do: { value println; $mergeCheck; };
"" println;

$check:
"1"
"a string"
"2.3";
