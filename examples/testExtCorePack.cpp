#include <dlfcn.h>
#include "testExtCorePack.hpp"
#include "ortmgr.hpp"
#include "obasicTypeInfo.hpp"
#include "otype.hpp"
#include "ostring.hpp"
#include "opackdev.h"

/**
 *  To compile use:
 *  . make_testExtCorePack.sh
 */

using std::string;
using std::vector;

static string testString{"testing core package extensions (plugins)"};

//:::::::::::::::::::::::::::::::   Exports   :::::::::::::::::::::::::::::::://

OM_BEGIN_PACKAGE_EXPORT("Test Extension", "1.0.0")
  OM_EXPORT_TYPE("TestExt", oxPluginExtension_Class)
OM_END_PACKAGE_EXPORT()

OM_BEGIN_VAR_EXPORT()
  OM_EXPORT_INITIALIZER([](Session* m, ORef o) {
        const vector<string>& imports = *reinterpret_cast<vector<string>*>(o);
        string l{imports.empty() ? "all" : imports.front()};
        for (size_t i = 1; i < imports.size(); ++i)
          l += ", " + imports[i];
        const string msg{"Hello from core package extension!\n"
                         "Requested imports: "+l};
        m->displayMessage("", msg);
      })
  OM_EXPORT_VAR_TYPE("info",
      BTI::getTypeClass<string>(),
      [](Session*, ORef o) {*_PString(o) = testString;})
  OM_EXPORT_VAR_GET_TYPE("whatIs",
      [](Session* m) {return m->classMgr->getClass("string");},
      [](Session*, ORef o) {*_PString(o) = testString;})
OM_END_VAR_EXPORT()

OM_DEFINE_PACKAGE_EXPORTER()

//::::::::::::::::::::::::::   oxPluginExtension   ::::::::::::::::::::::::::://

OM_BEGIN_METHOD_TABLE(oxPluginExtension, oxTestPlugin)
  PUBLIC(oxPluginExtension::ommGetSalary, "salary")
  PUBLIC(oxPluginExtension::ommSetSalary, "salary:")
  PUBLIC(oxPluginExtension::ommPrint, "print")
OM_END_METHOD_TABLE()

ORet oxPluginExtension::ommGetSalary(int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("TestExt::salary");
  ORef o = BTI::getTypeClass<ulong>()->createObject(m);
  OM_RETHROW_HERE();
  _P(ulong, o)->setValue(salary);
  return o->disposableResult();
}

ORet oxPluginExtension::ommSetSalary(int argc, OArg* argv, Session* m) {
  OM_ENTER("TestExt::salary:", 1);
  ulong x = g_castValue<ulong>(argv, m);
  OM_RETHROW_HERE();
  if (x < 500)
    OM_THROW_RTE("incorrect salary");
  salary = x;
  return makeResult();
}

ORet oxPluginExtension::ommPrint(int argc, OArg* argv, Session* m) {
  OM_ENTER_NOCHK("TestExt::print");
  auto* p = static_cast<TestPlugin*>(static_cast<PluginExtension*>(this));
  auto* baseClassThis = static_cast<oxTestPlugin*>(static_cast<ORef>(this));
  OM_ASSERT(baseClassThis == static_cast<oxTestPlugin*>(p), m);
  baseClassThis->ommPrint(argc, argv, m);
  m->mergeCheck(); // for testing mode
  m->displayMessage("Salary", std::to_string(salary));
  return makeResult();
}

//:::::::::::::::::::::::   oxPluginExtension_Class   :::::::::::::::::::::::://

OM_DEFINE_MCLASS(PluginExtension)
