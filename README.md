## What is

**Invoker** is a programming language free of
instructions/statements and keywords. **Invoker**
programs are a sequence of semicolon separated phrases,
the "program units," all of which have precisely
the same pattern:

*receiver receivable* ;

A *receiver* is an object or an expression that reduces
to an object, and *receivable* is an **ISON** definition.
An **ISON** definition is like a JSON definition without
comma separators and enclosing curly braces. For example,

```json
  employee name: "John Doe"
           age: 32
           department: 12
           salary: 40100.25;
```

**Invoker** makes extensive use of subscripted strings.
For example, to create an object `employee` of type
`Employee` one writes:

```js
  employee = "Employee"i;
```

Or, to create an employee "John Doe" with age 32, one
could write

```js
  employee = "Employee"i{"John Doe", 32};
```

Besides sequentially arranging program units, it is
possible to join them. For example, one can redirect
a unit's result to another receivable, like in the
following line:

```json
  employee name: "John Doe" => setAge: 32
                            => display;
```


