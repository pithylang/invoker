# Ranges

## Objective

We aim to incorporate the C++ ranges/views functionality into our _Object Model_ but in a simpler and more convenient coding style. The ranges package will form the basis for the framework of the algorithms.

The `C++20`'s `view`s have the following characteristics:

- Initial views are used as the first element in a pipeline of views:

    - `all` Convert range into view

    - `counted` Create view from a begin iterator and a count

    - `subrange` Create view from a begin iterator and a sentinel

    - `ref_view` Create a view that refers to a range (low level)

    - `iota(fst)` Create a view with an endless sequence of successive
      values starting from `fst`

    - `iota(fst, lst)` Create a view with an endless sequence of successive
      values starting from `fst` to `lst` (not included)

    - `single(val)` Create a view with `val` as the only element

    - `empty<type>` Creat empty view of element type `type`

    - `basic_istream_view` Create a `view` to read elements from an
      `input stream`

    - `basic_string_view` Create a read-only `view` for a character
      sequence

    - `span` Create view to a sequence of elements in _contiguous_
      memory

- Views operating on other `view`s or `range`s:

    - `take(num)` Takes the first `num` elements of a range

    - `take_while(pred)` Takes all leading elements that match a
      `pred`icate

    - `drop(num)` Drops the first `num` elements of a `range`

    - `drop_while(pred)` Drops all leading elements that match a
      `pred`icate

    - `filter(pred)` Filters elements matching a `pred`icate

    - `transform(pred)` Transforms all elements of a range

    - `elements<idx>` Use the `idx`-th member of each `tuple`-like
      element

    - `keys` Use the first member of each `tuple`-like element

    - `values` Use the second member of each `tuple`-like element

    - `reverse` Reverse the order of elements

    - `join` Joins the elements of multiple ranges

    - `split(pred)` Split a range into sub-ranges

    - `lazy_split(pred)` Split an input `const range` into sub-ranges

    - `common` Harmonizes types of `begin iterator` and `sentinel`

- The programmer can use the _pipeline syntax_ to combine views.

