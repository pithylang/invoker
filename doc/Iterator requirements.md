# Iterator Requirements

- [Iterator](https://en.cppreference.com/w/cpp/named_req/Iterator)
- [Input Iterator](https://en.cppreference.com/w/cpp/named_req/InputIterator)
- [Forward Iterator](https://en.cppreference.com/w/cpp/named_req/ForwardIterator)
- [Bidirectional Iterator](https://en.cppreference.com/w/cpp/named_req/BidirectionalIterator)
- [Random Access Iterator](https://en.cppreference.com/w/cpp/named_req/RandomAccessIterator)

## Iterator

A type satisfies the _Iterator requirement_ if it responds to the following _methods_

 - `swap:`
 - `value` (dereferencing operator)
 - `inc` (`operator++`)

## Input Iterator

A type `it` satisfies the _InputIterator requirement_ if

- It satisfies the _Iterator requirement_
- It responds to the following _methods_
    - `eq:`
    - `ne:`
    - `it.m`, `it["m"]` member access operator

The essential methods are:

- `eq:`
- `it.m`, `it["m"]` member access operator

## Forward Iterator

A type `it` satisfies the _ForwardIterator requirement_ if

- It satisfies the _InputIterator requirement_
- `postinc`

## Bidirectional Iterator

A type `it` satisfies the _BidirectionalIterator requirement_ if

- It satisfies the _ForwardIterator requirement_
- `dec`
- `postdec`

## Random Access Iterator

A type `it` satisfies the _RandomAccessIterator requirement_ if

- It satisfies the _BidirectionalIterator requirement_
- It responds to the following _methods_
    - `incBy:`
    - `decBy:`
    - `add: difference_type`
    - `sub: <type of it>`
    - `sub: difference_type`
    - All comparison operators directly, or indirectly through `sub: difference_type`

The essential methods are:

- `decBy:`
- `sub: [difference_type | <type of it>]`
- `component:`
