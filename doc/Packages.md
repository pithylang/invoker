# Packages

*Invoker Script* packages or modules are two kinds:

- Script packages
- Core packages

## Script Packages

Script packages can be

- Statically, or
- Dynamically

imported.

### Statically imported script packages

There are several ways to statically import a script package:

1. Import all package's exports:

        $ import: "package name";

2. From a package import several exports:

        $ import: { export1, ... } from: "package name";

    This is similar to the JavaScript import:

        import { export1, ... } from "package name";

3. From a package, import several exports assigning them a
   new identifier (*alias*):

        $ import: { export1: alias1,
                    export2: alias2... } from: "package name";

    This is similar to the JavaScript import:

        import { export1, export2 as alias2, … } from "package name";


### Dynamically imported script packages

1. Import exports creating a new namespace:

        $ import: packageName as: name; // or
        name = ($ import: packageName);

    This is similar to a JavaScript import:

        import * as name from "package name";

2. Import a number of exports creating a new namespace:

        $ import: { export1, ... } from: "package name" as: name; // or
        name = ($ import: { export1, ... } from: packageName);

There is no *default* import/export;


## What is imported

- Class definitions
- Variables


## Implementation


### Statically imported script packages

- The source file (*package exporter*) is compiled
- All symbols get the prefix `packageName..` (double dot)
- The `export` statement returns the list of exported items
- The calling source (package importer) incorporates the created
  blocks of code in the following way:
    - The exporter's main block is merged into the importer's main
      block
    - All other blocks are added at the end of the block table
    - References to blocks get fixed
    - The prefixes of the exported names are removed
- All changes are done before script execution


xxxcx
